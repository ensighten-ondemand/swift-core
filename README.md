# Ensighten

[![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
[![Version](https://img.shields.io/cocoapods/v/Ensighten.svg?style=flat)](http://cocoapods.org/pods/Ensighten)
[![License](https://img.shields.io/cocoapods/l/Ensighten.svg?style=flat)](http://cocoapods.org/pods/Ensighten)
[![Platform](https://img.shields.io/cocoapods/p/Ensighten.svg?style=flat)](http://cocoapods.org/pods/Ensighten)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

### Cocoapods

Ensighten is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:


```ruby
pod 'Ensighten'
```

### Carthage
Ensighten is available through Carthage. To install it simply add the following line to your Cartfile:


```ruby
binary "https://bitbucket.org/ensighten-ondemand/swift-core/raw/master/EnsightenSwift.json" ~> 2.1.1
```

## Author

Subash Luitel

## License

See the LICENSE file for more info.
