//
//  NSInvocation+Arguments.h
//  Ensighten
//
//  Created by Miles Alden on 8/28/12.
//
//

#import <Foundation/Foundation.h>

@interface NSInvocation (Arguments) 

- (int)ESWNumArgs;
- (NSMutableArray *)ESWAllArgs;
- (NSMutableArray *)ESWGetArgTypes: (int)numArgs;
- (NSMutableArray *)ESWAddArgs: (NSMutableArray *)argObjTypes;


@end
