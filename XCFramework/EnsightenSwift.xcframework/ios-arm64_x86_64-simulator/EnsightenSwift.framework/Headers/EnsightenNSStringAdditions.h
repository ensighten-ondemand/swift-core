//
//  NSString+EnsightenNSStringAdditions.h
//  EnsightenSwift
//
//  Created by Miles Alden on 9/14/16.
//  Copyright © 2017 Ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EnsightenNSStringAdditions)

- (NSString *)eswSha256;
- (NSString *)ESWBase64Decode;
- (NSString *)ESWBase64Encode;
- (NSString *)ESWEscapedJSONString;
- (NSString *)capitalizeFirstLetter;

@end
