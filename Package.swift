// swift-tools-version:5.3

import PackageDescription

let package = Package(
    name: "EnsightenSwift",
    products: [
        .library(
            name: "EnsightenSwift",
            targets: ["EnsightenSwift"]),
    ],
    targets: [
        .binaryTarget(
            name: "EnsightenSwift",
            path: "./XCFramework/EnsightenSwift.xcframework"
        )
    ]
)
