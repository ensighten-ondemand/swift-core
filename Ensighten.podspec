#
# Be sure to run `pod lib lint Ensighten.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Ensighten'
  s.version          = '2.3.6'
  s.summary          = 'Ensighten Swift framework'
  s.description      = <<-DESC
    Install the Ensighten framework to deploy analytics and other third-party technologies in your iOS app written primarily in Swift. The library must be bootstrapped using the Account ID associated with your account and the App ID associated with your app.
DESC

  s.homepage         = 'https://bitbucket.org/ensighten-ondemand/swift-core'
  s.license          = { :type => 'Proprietary', :file => 'LICENSE' }
  s.author           = { 'Ensighten, Inc.' => 'mobile@ensighten.com' }
  s.source           = { :git => 'https://bitbucket.org/ensighten-ondemand/swift-core', :tag => s.version.to_s }
  s.frameworks = 'EnsightenSwift'
  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '/Applications/Xcode.app/Contents/Developer/Library/Frameworks' }
  s.vendored_frameworks = 'EnsightenSwift.framework'
  s.ios.deployment_target = '9.0'
  s.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end
