//
//  ThemeSelectionViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 5/28/19.
//  Copyright © 2019 Ensighten. All rights reserved.
//

import UIKit

class ThemeSelectionViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThemeCell", for: indexPath)
        let theme = getThemeAtIndexPath(indexPath)
        if ThemeManager.shared.currentTheme == theme {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.textLabel?.text = "\(theme.rawValue) Theme"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedTheme = getThemeAtIndexPath(indexPath)
        ThemeManager.shared.setCurrentTheme(selectedTheme)
        tableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    
    func getThemeAtIndexPath(_ indexPath: IndexPath) -> Theme {
        switch indexPath.row {
        case 0:
            return Theme.standard
        case 1:
            return Theme.dark
        case 2:
            return Theme.largeText
        case 3:
            return Theme.custom
        default:
            return Theme.standard
        }
    }


}
