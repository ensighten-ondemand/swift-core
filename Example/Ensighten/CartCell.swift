//
//  CartCell.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet weak var thumbnailView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countContainerView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var delegate: CartCellDelegate?
    var item: Item?
    
    @IBAction func incrementCount(_ sender: UIButton) {
        guard let theItem = item else {
            return
        }
        Utility.shared.incrementCartCountFor(theItem)
        delegate?.cartCellDidUpdateCountFor(item: theItem)
    }
    @IBAction func decrementCount(_ sender: UIButton) {
        guard let theItem = item else {
            return
        }
        Utility.shared.decrementCartCountFor(theItem)
        delegate?.cartCellDidUpdateCountFor(item: theItem)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        countContainerView.layer.cornerRadius = 5
        countContainerView.layer.borderWidth = 1
        countContainerView.layer.borderColor = UIColor.lightGray.cgColor
        countContainerView.layer.masksToBounds = true
    }
    
}

protocol CartCellDelegate {
    func cartCellDidUpdateCountFor(item: Item)
}
