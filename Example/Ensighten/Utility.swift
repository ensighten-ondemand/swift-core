//
//  Utility.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    static let shared = Utility()
    
    lazy var itemsInfo: [String : Any] = {
        if let url = Bundle.main.url(forResource: "Data", withExtension: "plist") {
            do {
                let data = try Data(contentsOf: url)
                if let dictionary = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [String : Any] {
                    return dictionary
                }
            } catch {
                print(error)
            }
        }
        return [:]
    }()
    
    func getItemWith(id: String) -> Item? {
        let allProducts = fetchAllProducts()
        let allPromotions = fetchAllPromotions()
        if let productIndex = allProducts.firstIndex(where: { (product) -> Bool in
            return product.id == id
        }) {
            return allProducts[productIndex]
        }
        else if let promotionIndex = allPromotions.firstIndex(where: { (promotion) -> Bool in
            return promotion.id == id
        }) {
            return allPromotions[promotionIndex]
        }
        return nil
    }
    
    func fetchAllProducts() -> [Product] {
        var products = [Product]()
        if let productArray = itemsInfo["products"] as? [[String : Any]] {
            for productInfo in productArray {
                let product = Product(productInfo: productInfo)
                products.append(product)
            }
        }
        return products
    }
    
    func fetchAllPromotions() -> [Promotion] {
        var promotions = [Promotion]()
        if let promotionArray = itemsInfo["promotions"] as? [[String : Any]] {
            for promotionInfo in promotionArray {
                let promotion = Promotion(promotionInfo: promotionInfo)
                promotions.append(promotion)
            }
        }
        return promotions
    }
    
    
    private func getSavedCartItemsInfo() -> [[String : Any]] {
        var info = [[String : Any]]()
        if let cartItems = UserDefaults.standard.array(forKey: "cartItems") as? [[String : Any]] {
            info = cartItems
        }
        return info
    }
    
    func fetchCartItems() -> [Item] {
        var items = [Item]()
        let cartItems = getSavedCartItemsInfo()
        for cartItem in cartItems {
            if let id = cartItem["id"] as? String {
                if let item = getItemWith(id: id) {
                    items.append(item)
                }
            }
        }
        return items
    }
    
    func getCartItemCount(item: Item) -> Int {
        let cartItems = getSavedCartItemsInfo()
        if let index = cartItems.firstIndex(where: { (info) -> Bool in
            return info["id"] as? String == item.id
        }) {
            let cartItemInfo = cartItems[index]
            if let count = cartItemInfo["count"] as? Int {
                return count
            }
        }
        return 0
    }
    
    func addItemToCart(item: Item) {
        guard let itemId = item.id else {
            return
        }
        var cartItems = getSavedCartItemsInfo()
        var itemInfo: [String : Any] = ["id" : itemId, "count" : 1]
        if let index = cartItems.firstIndex(where: { (info) -> Bool in
            return info["id"] as? String == item.id
        }) {
            let cartItemInfo = cartItems[index]
            var count = 1
            if let savedCount = cartItemInfo["count"] as? Int {
                count = savedCount + 1
            }
            itemInfo["count"] = count
            cartItems.remove(at: index)
            cartItems.insert(itemInfo, at: index)
        }
        else {
            cartItems.append(itemInfo)
        }
        UserDefaults.standard.set(cartItems, forKey: "cartItems")
    }
    
    func incrementCartCountFor(_ item: Item) {
        addItemToCart(item: item)
    }
    
    func decrementCartCountFor(_ item: Item) {
        
        var cartItems = getSavedCartItemsInfo()
        if let index = cartItems.firstIndex(where: { (info) -> Bool in
            return info["id"] as? String == item.id
        }) {
            var cartItemInfo = cartItems[index]
            var count = 1
            if let savedCount = cartItemInfo["count"] as? Int {
                count = savedCount - 1
            }
            cartItemInfo["count"] = count
            cartItems.remove(at: index)
            if count > 0 {
                cartItems.insert(cartItemInfo, at: index)
            }
            UserDefaults.standard.set(cartItems, forKey: "cartItems")

        }
    }
    
    func clearCart() {
        UserDefaults.standard.removeObject(forKey: "cartItems")
    }

}
