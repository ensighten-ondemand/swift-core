//
//  PurchaseTracker.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 5/16/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import EnsightenSwift

class CheckoutTracker: EnsightenTracker {

    func trackCheckoutWith(items: [Item]) {
        let itemsInfo = items.map { (item) -> [String : Any] in
            return [
                "id": item.id ?? "",
                "name": item.name ?? "",
                "price": item.price
            ] as [String: Any]
        }
        var total: Double = 0
        for item in items {
            total += item.price
        }
        trackCustomMethod(methodName: "trackCheckout", data: total, items.count, itemsInfo)
    }
    
}
