//
//  Product.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class Product: Item {

    var productDescription: String?
    var images = [UIImage]()
    
    init(productInfo: [String : Any]) {
        super.init(info: productInfo)
        productDescription = productInfo["description"] as? String
        if let primaryImageName = productInfo["thumbnail"] as? String {
            if let primaryImage = UIImage(named: primaryImageName) {
                images.append(primaryImage)
            }
            var imagesAvailable = true
            var imageNumber = 1
            while imagesAvailable {
                let imageName = primaryImageName + "\(imageNumber)"
                if let image = UIImage(named: imageName) {
                    images.append(image)
                }
                else {
                    imagesAvailable = false
                }
                imageNumber += 1
            }
        }
    }
}
