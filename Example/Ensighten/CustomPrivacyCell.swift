//
//  CustomPrivacyCell.swift
//  Ensighten_Example
//
//  Created by Bradley Slayter on 10/25/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import EnsightenSwift

class CustomPrivacyCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: ConsentTextView!
    @IBOutlet weak var cookieSwitch: UISwitch!
    
    var cookie: ESWPrivacyCookie?
    
    var delegate: PrivacyCellDelegate?
    
    override func prepareForReuse() {
        cookieSwitch.isHidden = true
        cookieSwitch.isOn = false
    }
    
    /**
     Update the cell's contents based on the cookie configuration
     */
    func populateWith(cookie: ESWPrivacyCookie) {
        self.cookie = cookie
        titleLabel.text = cookie.title
        descriptionTextView.setHTMLText(cookie.cookieDescription, withFont: UIFont(name: "Lato-Light", size: 14)!)
        descriptionTextView.textColor = .white
        
        if cookie.value == nil {
            self.cookieSwitch.isHidden = true
            return
        }

        if cookie.isEditable {
            self.cookieSwitch.isHidden = false
        } else {
            self.cookieSwitch.isHidden = true 
        }

        if cookie.value == "1" || cookie.value == "true" {
            self.cookieSwitch.isOn = true
        } else {
            self.cookieSwitch.isOn = false
        }
    }
    
    /**
     Called when a switch is toggled
     */
    @IBAction func cookieSwitchChanged(_ sender: UISwitch) {
        delegate?.privacyModalCell(cell: self, didToggleCookie: sender)
    }

}

/**
 Protocol to update cookie states in the modal UIView
 */
protocol PrivacyCellDelegate {
    func privacyModalCell(cell: CustomPrivacyCell, didToggleCookie cookieSwitch: UISwitch)
}
