//
//  ConfirmationViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import EnsightenSwift

class ConfirmationViewController: UIViewController {
    
    @IBOutlet weak var homeButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        Utility.shared.clearCart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Ensighten.trackPageView(page: "Checkout Complete Page View")
    }

    @IBAction func homeButtonPressed(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
}
