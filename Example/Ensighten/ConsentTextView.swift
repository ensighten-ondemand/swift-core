//
//  ConsentTextView.swift
//  Ensighten_Example
//
//  Created by Bradley Slayter on 10/25/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class ConsentTextView: UITextView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
    
    func setHTMLText(_ htmlText: String?, withFont font: UIFont) {
        let attributedTextValue = attributedTextFrom(htmlText, withFont: font)
        attributedText = attributedTextValue
    }
    
    private func attributedTextFrom(_ htmlText: String?, withFont textFont: UIFont) -> NSAttributedString {
        if let theText = htmlText {
            if let htmlData = NSString(string: theText)
                .data(using: String.Encoding.unicode.rawValue) {
                let attributes: [NSAttributedString.DocumentReadingOptionKey : Any] = [
                    NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html
                ]
                if let attributedString = try? NSMutableAttributedString(data: htmlData,
                                                                         options: attributes,
                                                                         documentAttributes: nil) {
                    attributedString.addAttribute(NSAttributedString.Key.font,
                                                  value: textFont,
                                                  range: attributedString.string.range)
                    return attributedString
                }
            }
        }
        return NSAttributedString(string: "")
    }

}
