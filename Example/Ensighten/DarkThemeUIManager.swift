//
//  DarkThemeUIManager.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 5/28/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import EnsightenSwift

class DarkThemeUIManager: EnsightenPrivacyUIManager {
    
    // Modal
    func privacyModalBackgroundColor() -> UIColor {
        return .black
    }
    
    func privacyModalTitleTextColor() -> UIColor {
        return .orangePrimary
    }
    
    func privacyModalDescriptionTextColor() -> UIColor {
        return .orangePrimary
    }
    
    // Categories
    func privacyModalSeparatorColor() -> UIColor {
        return.orangePrimary
    }
    
    func privacyModalCategoryTitleTextColor() -> UIColor {
        return .orangePrimary
    }
    
    func privacyModalCategoryDescriptionTextColor() -> UIColor {
        return .orangePrimary
    }
    
    func privacyModalSwitchOnTintColor() -> UIColor {
        return .orangeDark
    }
    
    // Save Button
    func privacyModalSaveButtonTextColor() -> UIColor {
        return .white
    }
    
    func privacyModalSaveButtonBackgroundColor() -> UIColor {
        return .orangeDark
    }
    
    func privacyModalSaveButtonCornerRadius() -> CGFloat {
        return 5
    }
    
    // Cancel Button
    func privacyModalCancelButtonCornerRadius() -> CGFloat {
        return 5
    }
    
    func privacyModalCancelButtonBackgroundColor() -> UIColor {
        return .orangeDark
    }
    
    func privacyModalCancelButtonTextColor() -> UIColor {
        return .white
    }

}
