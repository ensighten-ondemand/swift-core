//
//  SideMenuViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 8/13/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import ZKDrawerController

class SideMenuViewController: ZKDrawerController {

    required init?(coder aDecoder: NSCoder) {
        super.init(center: UIViewController())
        
        if let homeNavigation = storyboard?.instantiateViewController(withIdentifier: "HomeNavigation") as? UINavigationController {
            centerViewController = homeNavigation
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let homeNavigation = storyboard?.instantiateViewController(withIdentifier: "HomeNavigation") as? UINavigationController {
            centerViewController = homeNavigation
        }
        if let leftVC = storyboard?.instantiateViewController(withIdentifier: "Left") as? LeftViewController {
            leftViewController = leftVC
            defaultLeftWidth = 280
            gestureRecognizerWidth = 40
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Touched began")
    }

}
