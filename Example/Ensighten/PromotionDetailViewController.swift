//
//  PromotionDetailViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import EnsightenSwift

class PromotionDetailViewController: UIViewController {
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var promotionImageView: UIImageView!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var dismissButton: UIButton!
    
    var promotion: Promotion?
    var delegate: PromotionDetailDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        displayPromotionDetail()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Ensighten.trackPageView(page: "Promotion Detail Page View",
                                data: getPromotionInfo())
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        
        addToCartButton.layer.cornerRadius = 10
        addToCartButton.layer.masksToBounds = true
    }
    
    func displayPromotionDetail() {
        promotionImageView.image = promotion?.largeImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addItemToCart(_ sender: UIButton) {
        guard let item = promotion else {
            return
        }
        Utility.shared.addItemToCart(item: item)
        showAddedToCartAlert()
        Ensighten.trackEvent(event: "Add to Cart",
                             data: getPromotionInfo())
        Ensighten.trackConversion(conversion: "Promotion Added to Cart",
                                  data: getPromotionInfo())
    }
    
    func showAddedToCartAlert() {
        let alert = UIAlertController(title: "Success!", message: "\(promotion?.name ?? "This item") has been added to your cart", preferredStyle: .alert)
        let continueAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.dismiss(animated: false, completion: nil)
        }
        let gotoCartAction = UIAlertAction(title: "Go to Cart", style: .default) { (action) in
            self.delegate?.promotionDetailViewControllerPressedViewCartOption(controller: self)
        }
        alert.addAction(continueAction)
        alert.addAction(gotoCartAction)
        present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func tappedView(_ sender: UITapGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }
    
    private func getPromotionInfo() -> [String : Any] {
        let promotionInfo: [String : Any] = [
            "promotionId": promotion?.id ?? "",
            "promotionName": promotion?.name ?? "",
            "promotionPrice": promotion?.price ?? 0
        ]
        return promotionInfo
    }
    
}

protocol PromotionDetailDelegate {
    func promotionDetailViewControllerPressedViewCartOption(controller: UIViewController)
}
