//
//  HomeViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import EnsightenSwift
import ZKDrawerController
import WebKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var cartButton: UIBarButtonItem!
    
    fileprivate let productCellIdentifier = "ProductCell"
    fileprivate let promotionViewIdentifier = "PromotionView"
    
    var products = [Product]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawerController?.drawerStyle = .cover
        
        products = Utility.shared.fetchAllProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Ensighten.trackPageView(page: "Home Page",
                                data: Utility.shared.itemsInfo)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func showHamburgerMenu(_ sender: UIBarButtonItem) {
        drawerController?.show(.left, animated: true)
    }
    
    
    @IBAction func gotoCart(_ sender: UIBarButtonItem) {
        showCart()
        Ensighten.trackEvent(event: "View Cart")
        let cookit = HTTPCookieStorage.shared.cookies
        if let cookies = cookit {
            for cook in cookies {
                print(cook.name)
            }
        }        
    }
    
    
    
    func showCart() {
        if let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart") as? CartViewController {
            navigationController?.pushViewController(cartVC, animated: true)
        }
    }
    
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productCellIdentifier, for: indexPath) as! ProductCell
        let product = products[indexPath.item]
        cell.productImageView.image = product.thumbnail ?? #imageLiteral(resourceName: "PlaceholderProduct")
        cell.priceLabel.text = "$\(product.price)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let promotionView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: promotionViewIdentifier, for: indexPath) as! PromotionView
        promotionView.delegate = self
        promotionView.updatePromotions()
        return promotionView
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedProduct = products[indexPath.item]
        if let productDetailVC = storyboard?.instantiateViewController(withIdentifier: "ProductDetail") as? ProductDetailViewController {
            productDetailVC.product = selectedProduct
            navigationController?.pushViewController(productDetailVC, animated: true)
        }
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        productCollectionView.layoutIfNeeded()
        productCollectionView.reloadData()
    }
}

extension HomeViewController: PromotionViewDelegate {
    func promotionViewDidSelect(promotion: Promotion) {
        if let promotionDetailVC = storyboard?.instantiateViewController(withIdentifier: "PromotionDetail") as? PromotionDetailViewController {
            promotionDetailVC.promotion = promotion
            promotionDetailVC.modalPresentationStyle = .overFullScreen
            promotionDetailVC.delegate = self
            present(promotionDetailVC, animated: false, completion: nil)
        }
    }
}

extension HomeViewController: PromotionDetailDelegate {
    func promotionDetailViewControllerPressedViewCartOption(controller: UIViewController) {
        controller.dismiss(animated: false) {
            self.showCart()
        }
    }
}


