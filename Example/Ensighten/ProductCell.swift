//
//  ProductCell.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    
}
