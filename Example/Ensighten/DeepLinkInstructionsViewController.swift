//
//  DeepLinkInstructionsViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 8/13/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class DeepLinkInstructionsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Test Deep Link"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openSafari() {
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = "ensightendemo://store"
        if let safariUrl = URL(string: "https://ensighten.com") {
            UIApplication.shared.open(safariUrl, options: [:], completionHandler: nil)
            navigationController?.popViewController(animated: false)

        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
