var PrivacyUrlProcessor =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/atob/node-atob.js":
/*!****************************************!*\
  !*** ./node_modules/atob/node-atob.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(Buffer) {

function atob(str) {
  return Buffer.from(str, 'base64').toString('binary');
}

module.exports = atob.atob = atob;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../buffer/index.js */ "./node_modules/buffer/index.js").Buffer))

/***/ }),

/***/ "./node_modules/base64-js/index.js":
/*!*****************************************!*\
  !*** ./node_modules/base64-js/index.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.byteLength = byteLength;
exports.toByteArray = toByteArray;
exports.fromByteArray = fromByteArray;
var lookup = [];
var revLookup = [];
var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array;
var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

for (var i = 0, len = code.length; i < len; ++i) {
  lookup[i] = code[i];
  revLookup[code.charCodeAt(i)] = i;
} // Support decoding URL-safe base64 strings, as Node.js does.
// See: https://en.wikipedia.org/wiki/Base64#URL_applications


revLookup['-'.charCodeAt(0)] = 62;
revLookup['_'.charCodeAt(0)] = 63;

function getLens(b64) {
  var len = b64.length;

  if (len % 4 > 0) {
    throw new Error('Invalid string. Length must be a multiple of 4');
  } // Trim off extra bytes after placeholder bytes are found
  // See: https://github.com/beatgammit/base64-js/issues/42


  var validLen = b64.indexOf('=');
  if (validLen === -1) validLen = len;
  var placeHoldersLen = validLen === len ? 0 : 4 - validLen % 4;
  return [validLen, placeHoldersLen];
} // base64 is 4/3 + up to two characters of the original data


function byteLength(b64) {
  var lens = getLens(b64);
  var validLen = lens[0];
  var placeHoldersLen = lens[1];
  return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
}

function _byteLength(b64, validLen, placeHoldersLen) {
  return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
}

function toByteArray(b64) {
  var tmp;
  var lens = getLens(b64);
  var validLen = lens[0];
  var placeHoldersLen = lens[1];
  var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen));
  var curByte = 0; // if there are placeholders, only get up to the last complete 4 chars

  var len = placeHoldersLen > 0 ? validLen - 4 : validLen;
  var i;

  for (i = 0; i < len; i += 4) {
    tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
    arr[curByte++] = tmp >> 16 & 0xFF;
    arr[curByte++] = tmp >> 8 & 0xFF;
    arr[curByte++] = tmp & 0xFF;
  }

  if (placeHoldersLen === 2) {
    tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
    arr[curByte++] = tmp & 0xFF;
  }

  if (placeHoldersLen === 1) {
    tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
    arr[curByte++] = tmp >> 8 & 0xFF;
    arr[curByte++] = tmp & 0xFF;
  }

  return arr;
}

function tripletToBase64(num) {
  return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F];
}

function encodeChunk(uint8, start, end) {
  var tmp;
  var output = [];

  for (var i = start; i < end; i += 3) {
    tmp = (uint8[i] << 16 & 0xFF0000) + (uint8[i + 1] << 8 & 0xFF00) + (uint8[i + 2] & 0xFF);
    output.push(tripletToBase64(tmp));
  }

  return output.join('');
}

function fromByteArray(uint8) {
  var tmp;
  var len = uint8.length;
  var extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes

  var parts = [];
  var maxChunkLength = 16383; // must be multiple of 3
  // go through the array every three bytes, we'll deal with trailing stuff later

  for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
    parts.push(encodeChunk(uint8, i, i + maxChunkLength > len2 ? len2 : i + maxChunkLength));
  } // pad the end with zeros, but make sure to not forget the extra bytes


  if (extraBytes === 1) {
    tmp = uint8[len - 1];
    parts.push(lookup[tmp >> 2] + lookup[tmp << 4 & 0x3F] + '==');
  } else if (extraBytes === 2) {
    tmp = (uint8[len - 2] << 8) + uint8[len - 1];
    parts.push(lookup[tmp >> 10] + lookup[tmp >> 4 & 0x3F] + lookup[tmp << 2 & 0x3F] + '=');
  }

  return parts.join('');
}

/***/ }),

/***/ "./node_modules/buffer/index.js":
/*!**************************************!*\
  !*** ./node_modules/buffer/index.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * The buffer module from node.js, for the browser.
 *
 * @author   Feross Aboukhadijeh <http://feross.org>
 * @license  MIT
 */

/* eslint-disable no-proto */


var base64 = __webpack_require__(/*! base64-js */ "./node_modules/base64-js/index.js");

var ieee754 = __webpack_require__(/*! ieee754 */ "./node_modules/ieee754/index.js");

var isArray = __webpack_require__(/*! isarray */ "./node_modules/isarray/index.js");

exports.Buffer = Buffer;
exports.SlowBuffer = SlowBuffer;
exports.INSPECT_MAX_BYTES = 50;
/**
 * If `Buffer.TYPED_ARRAY_SUPPORT`:
 *   === true    Use Uint8Array implementation (fastest)
 *   === false   Use Object implementation (most compatible, even IE6)
 *
 * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
 * Opera 11.6+, iOS 4.2+.
 *
 * Due to various browser bugs, sometimes the Object implementation will be used even
 * when the browser supports typed arrays.
 *
 * Note:
 *
 *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
 *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
 *
 *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
 *
 *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
 *     incorrect length in some situations.

 * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
 * get the Object implementation, which is slower but behaves correctly.
 */

Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined ? global.TYPED_ARRAY_SUPPORT : typedArraySupport();
/*
 * Export kMaxLength after typed array support is determined.
 */

exports.kMaxLength = kMaxLength();

function typedArraySupport() {
  try {
    var arr = new Uint8Array(1);
    arr.__proto__ = {
      __proto__: Uint8Array.prototype,
      foo: function foo() {
        return 42;
      }
    };
    return arr.foo() === 42 && // typed array instances can be augmented
    typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
    arr.subarray(1, 1).byteLength === 0; // ie10 has broken `subarray`
  } catch (e) {
    return false;
  }
}

function kMaxLength() {
  return Buffer.TYPED_ARRAY_SUPPORT ? 0x7fffffff : 0x3fffffff;
}

function createBuffer(that, length) {
  if (kMaxLength() < length) {
    throw new RangeError('Invalid typed array length');
  }

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = new Uint8Array(length);
    that.__proto__ = Buffer.prototype;
  } else {
    // Fallback: Return an object instance of the Buffer class
    if (that === null) {
      that = new Buffer(length);
    }

    that.length = length;
  }

  return that;
}
/**
 * The Buffer constructor returns instances of `Uint8Array` that have their
 * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
 * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
 * and the `Uint8Array` methods. Square bracket notation works as expected -- it
 * returns a single octet.
 *
 * The `Uint8Array` prototype remains unmodified.
 */


function Buffer(arg, encodingOrOffset, length) {
  if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) {
    return new Buffer(arg, encodingOrOffset, length);
  } // Common case.


  if (typeof arg === 'number') {
    if (typeof encodingOrOffset === 'string') {
      throw new Error('If encoding is specified then the first argument must be a string');
    }

    return allocUnsafe(this, arg);
  }

  return from(this, arg, encodingOrOffset, length);
}

Buffer.poolSize = 8192; // not used by this implementation
// TODO: Legacy, not needed anymore. Remove in next major version.

Buffer._augment = function (arr) {
  arr.__proto__ = Buffer.prototype;
  return arr;
};

function from(that, value, encodingOrOffset, length) {
  if (typeof value === 'number') {
    throw new TypeError('"value" argument must not be a number');
  }

  if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
    return fromArrayBuffer(that, value, encodingOrOffset, length);
  }

  if (typeof value === 'string') {
    return fromString(that, value, encodingOrOffset);
  }

  return fromObject(that, value);
}
/**
 * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
 * if value is a number.
 * Buffer.from(str[, encoding])
 * Buffer.from(array)
 * Buffer.from(buffer)
 * Buffer.from(arrayBuffer[, byteOffset[, length]])
 **/


Buffer.from = function (value, encodingOrOffset, length) {
  return from(null, value, encodingOrOffset, length);
};

if (Buffer.TYPED_ARRAY_SUPPORT) {
  Buffer.prototype.__proto__ = Uint8Array.prototype;
  Buffer.__proto__ = Uint8Array;

  if (typeof Symbol !== 'undefined' && Symbol.species && Buffer[Symbol.species] === Buffer) {
    // Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
    Object.defineProperty(Buffer, Symbol.species, {
      value: null,
      configurable: true
    });
  }
}

function assertSize(size) {
  if (typeof size !== 'number') {
    throw new TypeError('"size" argument must be a number');
  } else if (size < 0) {
    throw new RangeError('"size" argument must not be negative');
  }
}

function alloc(that, size, fill, encoding) {
  assertSize(size);

  if (size <= 0) {
    return createBuffer(that, size);
  }

  if (fill !== undefined) {
    // Only pay attention to encoding if it's a string. This
    // prevents accidentally sending in a number that would
    // be interpretted as a start offset.
    return typeof encoding === 'string' ? createBuffer(that, size).fill(fill, encoding) : createBuffer(that, size).fill(fill);
  }

  return createBuffer(that, size);
}
/**
 * Creates a new filled Buffer instance.
 * alloc(size[, fill[, encoding]])
 **/


Buffer.alloc = function (size, fill, encoding) {
  return alloc(null, size, fill, encoding);
};

function allocUnsafe(that, size) {
  assertSize(size);
  that = createBuffer(that, size < 0 ? 0 : checked(size) | 0);

  if (!Buffer.TYPED_ARRAY_SUPPORT) {
    for (var i = 0; i < size; ++i) {
      that[i] = 0;
    }
  }

  return that;
}
/**
 * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
 * */


Buffer.allocUnsafe = function (size) {
  return allocUnsafe(null, size);
};
/**
 * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
 */


Buffer.allocUnsafeSlow = function (size) {
  return allocUnsafe(null, size);
};

function fromString(that, string, encoding) {
  if (typeof encoding !== 'string' || encoding === '') {
    encoding = 'utf8';
  }

  if (!Buffer.isEncoding(encoding)) {
    throw new TypeError('"encoding" must be a valid string encoding');
  }

  var length = byteLength(string, encoding) | 0;
  that = createBuffer(that, length);
  var actual = that.write(string, encoding);

  if (actual !== length) {
    // Writing a hex string, for example, that contains invalid characters will
    // cause everything after the first invalid character to be ignored. (e.g.
    // 'abxxcd' will be treated as 'ab')
    that = that.slice(0, actual);
  }

  return that;
}

function fromArrayLike(that, array) {
  var length = array.length < 0 ? 0 : checked(array.length) | 0;
  that = createBuffer(that, length);

  for (var i = 0; i < length; i += 1) {
    that[i] = array[i] & 255;
  }

  return that;
}

function fromArrayBuffer(that, array, byteOffset, length) {
  array.byteLength; // this throws if `array` is not a valid ArrayBuffer

  if (byteOffset < 0 || array.byteLength < byteOffset) {
    throw new RangeError('\'offset\' is out of bounds');
  }

  if (array.byteLength < byteOffset + (length || 0)) {
    throw new RangeError('\'length\' is out of bounds');
  }

  if (byteOffset === undefined && length === undefined) {
    array = new Uint8Array(array);
  } else if (length === undefined) {
    array = new Uint8Array(array, byteOffset);
  } else {
    array = new Uint8Array(array, byteOffset, length);
  }

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    // Return an augmented `Uint8Array` instance, for best performance
    that = array;
    that.__proto__ = Buffer.prototype;
  } else {
    // Fallback: Return an object instance of the Buffer class
    that = fromArrayLike(that, array);
  }

  return that;
}

function fromObject(that, obj) {
  if (Buffer.isBuffer(obj)) {
    var len = checked(obj.length) | 0;
    that = createBuffer(that, len);

    if (that.length === 0) {
      return that;
    }

    obj.copy(that, 0, 0, len);
    return that;
  }

  if (obj) {
    if (typeof ArrayBuffer !== 'undefined' && obj.buffer instanceof ArrayBuffer || 'length' in obj) {
      if (typeof obj.length !== 'number' || isnan(obj.length)) {
        return createBuffer(that, 0);
      }

      return fromArrayLike(that, obj);
    }

    if (obj.type === 'Buffer' && isArray(obj.data)) {
      return fromArrayLike(that, obj.data);
    }
  }

  throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.');
}

function checked(length) {
  // Note: cannot use `length < kMaxLength()` here because that fails when
  // length is NaN (which is otherwise coerced to zero.)
  if (length >= kMaxLength()) {
    throw new RangeError('Attempt to allocate Buffer larger than maximum ' + 'size: 0x' + kMaxLength().toString(16) + ' bytes');
  }

  return length | 0;
}

function SlowBuffer(length) {
  if (+length != length) {
    // eslint-disable-line eqeqeq
    length = 0;
  }

  return Buffer.alloc(+length);
}

Buffer.isBuffer = function isBuffer(b) {
  return !!(b != null && b._isBuffer);
};

Buffer.compare = function compare(a, b) {
  if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
    throw new TypeError('Arguments must be Buffers');
  }

  if (a === b) return 0;
  var x = a.length;
  var y = b.length;

  for (var i = 0, len = Math.min(x, y); i < len; ++i) {
    if (a[i] !== b[i]) {
      x = a[i];
      y = b[i];
      break;
    }
  }

  if (x < y) return -1;
  if (y < x) return 1;
  return 0;
};

Buffer.isEncoding = function isEncoding(encoding) {
  switch (String(encoding).toLowerCase()) {
    case 'hex':
    case 'utf8':
    case 'utf-8':
    case 'ascii':
    case 'latin1':
    case 'binary':
    case 'base64':
    case 'ucs2':
    case 'ucs-2':
    case 'utf16le':
    case 'utf-16le':
      return true;

    default:
      return false;
  }
};

Buffer.concat = function concat(list, length) {
  if (!isArray(list)) {
    throw new TypeError('"list" argument must be an Array of Buffers');
  }

  if (list.length === 0) {
    return Buffer.alloc(0);
  }

  var i;

  if (length === undefined) {
    length = 0;

    for (i = 0; i < list.length; ++i) {
      length += list[i].length;
    }
  }

  var buffer = Buffer.allocUnsafe(length);
  var pos = 0;

  for (i = 0; i < list.length; ++i) {
    var buf = list[i];

    if (!Buffer.isBuffer(buf)) {
      throw new TypeError('"list" argument must be an Array of Buffers');
    }

    buf.copy(buffer, pos);
    pos += buf.length;
  }

  return buffer;
};

function byteLength(string, encoding) {
  if (Buffer.isBuffer(string)) {
    return string.length;
  }

  if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' && (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
    return string.byteLength;
  }

  if (typeof string !== 'string') {
    string = '' + string;
  }

  var len = string.length;
  if (len === 0) return 0; // Use a for loop to avoid recursion

  var loweredCase = false;

  for (;;) {
    switch (encoding) {
      case 'ascii':
      case 'latin1':
      case 'binary':
        return len;

      case 'utf8':
      case 'utf-8':
      case undefined:
        return utf8ToBytes(string).length;

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return len * 2;

      case 'hex':
        return len >>> 1;

      case 'base64':
        return base64ToBytes(string).length;

      default:
        if (loweredCase) return utf8ToBytes(string).length; // assume utf8

        encoding = ('' + encoding).toLowerCase();
        loweredCase = true;
    }
  }
}

Buffer.byteLength = byteLength;

function slowToString(encoding, start, end) {
  var loweredCase = false; // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
  // property of a typed array.
  // This behaves neither like String nor Uint8Array in that we set start/end
  // to their upper/lower bounds if the value passed is out of range.
  // undefined is handled specially as per ECMA-262 6th Edition,
  // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.

  if (start === undefined || start < 0) {
    start = 0;
  } // Return early if start > this.length. Done here to prevent potential uint32
  // coercion fail below.


  if (start > this.length) {
    return '';
  }

  if (end === undefined || end > this.length) {
    end = this.length;
  }

  if (end <= 0) {
    return '';
  } // Force coersion to uint32. This will also coerce falsey/NaN values to 0.


  end >>>= 0;
  start >>>= 0;

  if (end <= start) {
    return '';
  }

  if (!encoding) encoding = 'utf8';

  while (true) {
    switch (encoding) {
      case 'hex':
        return hexSlice(this, start, end);

      case 'utf8':
      case 'utf-8':
        return utf8Slice(this, start, end);

      case 'ascii':
        return asciiSlice(this, start, end);

      case 'latin1':
      case 'binary':
        return latin1Slice(this, start, end);

      case 'base64':
        return base64Slice(this, start, end);

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return utf16leSlice(this, start, end);

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding);
        encoding = (encoding + '').toLowerCase();
        loweredCase = true;
    }
  }
} // The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
// Buffer instances.


Buffer.prototype._isBuffer = true;

function swap(b, n, m) {
  var i = b[n];
  b[n] = b[m];
  b[m] = i;
}

Buffer.prototype.swap16 = function swap16() {
  var len = this.length;

  if (len % 2 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 16-bits');
  }

  for (var i = 0; i < len; i += 2) {
    swap(this, i, i + 1);
  }

  return this;
};

Buffer.prototype.swap32 = function swap32() {
  var len = this.length;

  if (len % 4 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 32-bits');
  }

  for (var i = 0; i < len; i += 4) {
    swap(this, i, i + 3);
    swap(this, i + 1, i + 2);
  }

  return this;
};

Buffer.prototype.swap64 = function swap64() {
  var len = this.length;

  if (len % 8 !== 0) {
    throw new RangeError('Buffer size must be a multiple of 64-bits');
  }

  for (var i = 0; i < len; i += 8) {
    swap(this, i, i + 7);
    swap(this, i + 1, i + 6);
    swap(this, i + 2, i + 5);
    swap(this, i + 3, i + 4);
  }

  return this;
};

Buffer.prototype.toString = function toString() {
  var length = this.length | 0;
  if (length === 0) return '';
  if (arguments.length === 0) return utf8Slice(this, 0, length);
  return slowToString.apply(this, arguments);
};

Buffer.prototype.equals = function equals(b) {
  if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer');
  if (this === b) return true;
  return Buffer.compare(this, b) === 0;
};

Buffer.prototype.inspect = function inspect() {
  var str = '';
  var max = exports.INSPECT_MAX_BYTES;

  if (this.length > 0) {
    str = this.toString('hex', 0, max).match(/.{2}/g).join(' ');
    if (this.length > max) str += ' ... ';
  }

  return '<Buffer ' + str + '>';
};

Buffer.prototype.compare = function compare(target, start, end, thisStart, thisEnd) {
  if (!Buffer.isBuffer(target)) {
    throw new TypeError('Argument must be a Buffer');
  }

  if (start === undefined) {
    start = 0;
  }

  if (end === undefined) {
    end = target ? target.length : 0;
  }

  if (thisStart === undefined) {
    thisStart = 0;
  }

  if (thisEnd === undefined) {
    thisEnd = this.length;
  }

  if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
    throw new RangeError('out of range index');
  }

  if (thisStart >= thisEnd && start >= end) {
    return 0;
  }

  if (thisStart >= thisEnd) {
    return -1;
  }

  if (start >= end) {
    return 1;
  }

  start >>>= 0;
  end >>>= 0;
  thisStart >>>= 0;
  thisEnd >>>= 0;
  if (this === target) return 0;
  var x = thisEnd - thisStart;
  var y = end - start;
  var len = Math.min(x, y);
  var thisCopy = this.slice(thisStart, thisEnd);
  var targetCopy = target.slice(start, end);

  for (var i = 0; i < len; ++i) {
    if (thisCopy[i] !== targetCopy[i]) {
      x = thisCopy[i];
      y = targetCopy[i];
      break;
    }
  }

  if (x < y) return -1;
  if (y < x) return 1;
  return 0;
}; // Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
// OR the last index of `val` in `buffer` at offset <= `byteOffset`.
//
// Arguments:
// - buffer - a Buffer to search
// - val - a string, Buffer, or number
// - byteOffset - an index into `buffer`; will be clamped to an int32
// - encoding - an optional encoding, relevant is val is a string
// - dir - true for indexOf, false for lastIndexOf


function bidirectionalIndexOf(buffer, val, byteOffset, encoding, dir) {
  // Empty buffer means no match
  if (buffer.length === 0) return -1; // Normalize byteOffset

  if (typeof byteOffset === 'string') {
    encoding = byteOffset;
    byteOffset = 0;
  } else if (byteOffset > 0x7fffffff) {
    byteOffset = 0x7fffffff;
  } else if (byteOffset < -0x80000000) {
    byteOffset = -0x80000000;
  }

  byteOffset = +byteOffset; // Coerce to Number.

  if (isNaN(byteOffset)) {
    // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
    byteOffset = dir ? 0 : buffer.length - 1;
  } // Normalize byteOffset: negative offsets start from the end of the buffer


  if (byteOffset < 0) byteOffset = buffer.length + byteOffset;

  if (byteOffset >= buffer.length) {
    if (dir) return -1;else byteOffset = buffer.length - 1;
  } else if (byteOffset < 0) {
    if (dir) byteOffset = 0;else return -1;
  } // Normalize val


  if (typeof val === 'string') {
    val = Buffer.from(val, encoding);
  } // Finally, search either indexOf (if dir is true) or lastIndexOf


  if (Buffer.isBuffer(val)) {
    // Special case: looking for empty string/buffer always fails
    if (val.length === 0) {
      return -1;
    }

    return arrayIndexOf(buffer, val, byteOffset, encoding, dir);
  } else if (typeof val === 'number') {
    val = val & 0xFF; // Search for a byte value [0-255]

    if (Buffer.TYPED_ARRAY_SUPPORT && typeof Uint8Array.prototype.indexOf === 'function') {
      if (dir) {
        return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset);
      } else {
        return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset);
      }
    }

    return arrayIndexOf(buffer, [val], byteOffset, encoding, dir);
  }

  throw new TypeError('val must be string, number or Buffer');
}

function arrayIndexOf(arr, val, byteOffset, encoding, dir) {
  var indexSize = 1;
  var arrLength = arr.length;
  var valLength = val.length;

  if (encoding !== undefined) {
    encoding = String(encoding).toLowerCase();

    if (encoding === 'ucs2' || encoding === 'ucs-2' || encoding === 'utf16le' || encoding === 'utf-16le') {
      if (arr.length < 2 || val.length < 2) {
        return -1;
      }

      indexSize = 2;
      arrLength /= 2;
      valLength /= 2;
      byteOffset /= 2;
    }
  }

  function read(buf, i) {
    if (indexSize === 1) {
      return buf[i];
    } else {
      return buf.readUInt16BE(i * indexSize);
    }
  }

  var i;

  if (dir) {
    var foundIndex = -1;

    for (i = byteOffset; i < arrLength; i++) {
      if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
        if (foundIndex === -1) foundIndex = i;
        if (i - foundIndex + 1 === valLength) return foundIndex * indexSize;
      } else {
        if (foundIndex !== -1) i -= i - foundIndex;
        foundIndex = -1;
      }
    }
  } else {
    if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength;

    for (i = byteOffset; i >= 0; i--) {
      var found = true;

      for (var j = 0; j < valLength; j++) {
        if (read(arr, i + j) !== read(val, j)) {
          found = false;
          break;
        }
      }

      if (found) return i;
    }
  }

  return -1;
}

Buffer.prototype.includes = function includes(val, byteOffset, encoding) {
  return this.indexOf(val, byteOffset, encoding) !== -1;
};

Buffer.prototype.indexOf = function indexOf(val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, true);
};

Buffer.prototype.lastIndexOf = function lastIndexOf(val, byteOffset, encoding) {
  return bidirectionalIndexOf(this, val, byteOffset, encoding, false);
};

function hexWrite(buf, string, offset, length) {
  offset = Number(offset) || 0;
  var remaining = buf.length - offset;

  if (!length) {
    length = remaining;
  } else {
    length = Number(length);

    if (length > remaining) {
      length = remaining;
    }
  } // must be an even number of digits


  var strLen = string.length;
  if (strLen % 2 !== 0) throw new TypeError('Invalid hex string');

  if (length > strLen / 2) {
    length = strLen / 2;
  }

  for (var i = 0; i < length; ++i) {
    var parsed = parseInt(string.substr(i * 2, 2), 16);
    if (isNaN(parsed)) return i;
    buf[offset + i] = parsed;
  }

  return i;
}

function utf8Write(buf, string, offset, length) {
  return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length);
}

function asciiWrite(buf, string, offset, length) {
  return blitBuffer(asciiToBytes(string), buf, offset, length);
}

function latin1Write(buf, string, offset, length) {
  return asciiWrite(buf, string, offset, length);
}

function base64Write(buf, string, offset, length) {
  return blitBuffer(base64ToBytes(string), buf, offset, length);
}

function ucs2Write(buf, string, offset, length) {
  return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length);
}

Buffer.prototype.write = function write(string, offset, length, encoding) {
  // Buffer#write(string)
  if (offset === undefined) {
    encoding = 'utf8';
    length = this.length;
    offset = 0; // Buffer#write(string, encoding)
  } else if (length === undefined && typeof offset === 'string') {
    encoding = offset;
    length = this.length;
    offset = 0; // Buffer#write(string, offset[, length][, encoding])
  } else if (isFinite(offset)) {
    offset = offset | 0;

    if (isFinite(length)) {
      length = length | 0;
      if (encoding === undefined) encoding = 'utf8';
    } else {
      encoding = length;
      length = undefined;
    } // legacy write(string, encoding, offset, length) - remove in v0.13

  } else {
    throw new Error('Buffer.write(string, encoding, offset[, length]) is no longer supported');
  }

  var remaining = this.length - offset;
  if (length === undefined || length > remaining) length = remaining;

  if (string.length > 0 && (length < 0 || offset < 0) || offset > this.length) {
    throw new RangeError('Attempt to write outside buffer bounds');
  }

  if (!encoding) encoding = 'utf8';
  var loweredCase = false;

  for (;;) {
    switch (encoding) {
      case 'hex':
        return hexWrite(this, string, offset, length);

      case 'utf8':
      case 'utf-8':
        return utf8Write(this, string, offset, length);

      case 'ascii':
        return asciiWrite(this, string, offset, length);

      case 'latin1':
      case 'binary':
        return latin1Write(this, string, offset, length);

      case 'base64':
        // Warning: maxLength not taken into account in base64Write
        return base64Write(this, string, offset, length);

      case 'ucs2':
      case 'ucs-2':
      case 'utf16le':
      case 'utf-16le':
        return ucs2Write(this, string, offset, length);

      default:
        if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding);
        encoding = ('' + encoding).toLowerCase();
        loweredCase = true;
    }
  }
};

Buffer.prototype.toJSON = function toJSON() {
  return {
    type: 'Buffer',
    data: Array.prototype.slice.call(this._arr || this, 0)
  };
};

function base64Slice(buf, start, end) {
  if (start === 0 && end === buf.length) {
    return base64.fromByteArray(buf);
  } else {
    return base64.fromByteArray(buf.slice(start, end));
  }
}

function utf8Slice(buf, start, end) {
  end = Math.min(buf.length, end);
  var res = [];
  var i = start;

  while (i < end) {
    var firstByte = buf[i];
    var codePoint = null;
    var bytesPerSequence = firstByte > 0xEF ? 4 : firstByte > 0xDF ? 3 : firstByte > 0xBF ? 2 : 1;

    if (i + bytesPerSequence <= end) {
      var secondByte, thirdByte, fourthByte, tempCodePoint;

      switch (bytesPerSequence) {
        case 1:
          if (firstByte < 0x80) {
            codePoint = firstByte;
          }

          break;

        case 2:
          secondByte = buf[i + 1];

          if ((secondByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0x1F) << 0x6 | secondByte & 0x3F;

            if (tempCodePoint > 0x7F) {
              codePoint = tempCodePoint;
            }
          }

          break;

        case 3:
          secondByte = buf[i + 1];
          thirdByte = buf[i + 2];

          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | thirdByte & 0x3F;

            if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
              codePoint = tempCodePoint;
            }
          }

          break;

        case 4:
          secondByte = buf[i + 1];
          thirdByte = buf[i + 2];
          fourthByte = buf[i + 3];

          if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
            tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | fourthByte & 0x3F;

            if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
              codePoint = tempCodePoint;
            }
          }

      }
    }

    if (codePoint === null) {
      // we did not generate a valid codePoint so insert a
      // replacement char (U+FFFD) and advance only 1 byte
      codePoint = 0xFFFD;
      bytesPerSequence = 1;
    } else if (codePoint > 0xFFFF) {
      // encode to utf16 (surrogate pair dance)
      codePoint -= 0x10000;
      res.push(codePoint >>> 10 & 0x3FF | 0xD800);
      codePoint = 0xDC00 | codePoint & 0x3FF;
    }

    res.push(codePoint);
    i += bytesPerSequence;
  }

  return decodeCodePointsArray(res);
} // Based on http://stackoverflow.com/a/22747272/680742, the browser with
// the lowest limit is Chrome, with 0x10000 args.
// We go 1 magnitude less, for safety


var MAX_ARGUMENTS_LENGTH = 0x1000;

function decodeCodePointsArray(codePoints) {
  var len = codePoints.length;

  if (len <= MAX_ARGUMENTS_LENGTH) {
    return String.fromCharCode.apply(String, codePoints); // avoid extra slice()
  } // Decode in chunks to avoid "call stack size exceeded".


  var res = '';
  var i = 0;

  while (i < len) {
    res += String.fromCharCode.apply(String, codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH));
  }

  return res;
}

function asciiSlice(buf, start, end) {
  var ret = '';
  end = Math.min(buf.length, end);

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i] & 0x7F);
  }

  return ret;
}

function latin1Slice(buf, start, end) {
  var ret = '';
  end = Math.min(buf.length, end);

  for (var i = start; i < end; ++i) {
    ret += String.fromCharCode(buf[i]);
  }

  return ret;
}

function hexSlice(buf, start, end) {
  var len = buf.length;
  if (!start || start < 0) start = 0;
  if (!end || end < 0 || end > len) end = len;
  var out = '';

  for (var i = start; i < end; ++i) {
    out += toHex(buf[i]);
  }

  return out;
}

function utf16leSlice(buf, start, end) {
  var bytes = buf.slice(start, end);
  var res = '';

  for (var i = 0; i < bytes.length; i += 2) {
    res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
  }

  return res;
}

Buffer.prototype.slice = function slice(start, end) {
  var len = this.length;
  start = ~~start;
  end = end === undefined ? len : ~~end;

  if (start < 0) {
    start += len;
    if (start < 0) start = 0;
  } else if (start > len) {
    start = len;
  }

  if (end < 0) {
    end += len;
    if (end < 0) end = 0;
  } else if (end > len) {
    end = len;
  }

  if (end < start) end = start;
  var newBuf;

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    newBuf = this.subarray(start, end);
    newBuf.__proto__ = Buffer.prototype;
  } else {
    var sliceLen = end - start;
    newBuf = new Buffer(sliceLen, undefined);

    for (var i = 0; i < sliceLen; ++i) {
      newBuf[i] = this[i + start];
    }
  }

  return newBuf;
};
/*
 * Need to make sure that buffer isn't trying to write out of bounds.
 */


function checkOffset(offset, ext, length) {
  if (offset % 1 !== 0 || offset < 0) throw new RangeError('offset is not uint');
  if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length');
}

Buffer.prototype.readUIntLE = function readUIntLE(offset, byteLength, noAssert) {
  offset = offset | 0;
  byteLength = byteLength | 0;
  if (!noAssert) checkOffset(offset, byteLength, this.length);
  var val = this[offset];
  var mul = 1;
  var i = 0;

  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul;
  }

  return val;
};

Buffer.prototype.readUIntBE = function readUIntBE(offset, byteLength, noAssert) {
  offset = offset | 0;
  byteLength = byteLength | 0;

  if (!noAssert) {
    checkOffset(offset, byteLength, this.length);
  }

  var val = this[offset + --byteLength];
  var mul = 1;

  while (byteLength > 0 && (mul *= 0x100)) {
    val += this[offset + --byteLength] * mul;
  }

  return val;
};

Buffer.prototype.readUInt8 = function readUInt8(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length);
  return this[offset];
};

Buffer.prototype.readUInt16LE = function readUInt16LE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length);
  return this[offset] | this[offset + 1] << 8;
};

Buffer.prototype.readUInt16BE = function readUInt16BE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length);
  return this[offset] << 8 | this[offset + 1];
};

Buffer.prototype.readUInt32LE = function readUInt32LE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length);
  return (this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16) + this[offset + 3] * 0x1000000;
};

Buffer.prototype.readUInt32BE = function readUInt32BE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length);
  return this[offset] * 0x1000000 + (this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3]);
};

Buffer.prototype.readIntLE = function readIntLE(offset, byteLength, noAssert) {
  offset = offset | 0;
  byteLength = byteLength | 0;
  if (!noAssert) checkOffset(offset, byteLength, this.length);
  var val = this[offset];
  var mul = 1;
  var i = 0;

  while (++i < byteLength && (mul *= 0x100)) {
    val += this[offset + i] * mul;
  }

  mul *= 0x80;
  if (val >= mul) val -= Math.pow(2, 8 * byteLength);
  return val;
};

Buffer.prototype.readIntBE = function readIntBE(offset, byteLength, noAssert) {
  offset = offset | 0;
  byteLength = byteLength | 0;
  if (!noAssert) checkOffset(offset, byteLength, this.length);
  var i = byteLength;
  var mul = 1;
  var val = this[offset + --i];

  while (i > 0 && (mul *= 0x100)) {
    val += this[offset + --i] * mul;
  }

  mul *= 0x80;
  if (val >= mul) val -= Math.pow(2, 8 * byteLength);
  return val;
};

Buffer.prototype.readInt8 = function readInt8(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 1, this.length);
  if (!(this[offset] & 0x80)) return this[offset];
  return (0xff - this[offset] + 1) * -1;
};

Buffer.prototype.readInt16LE = function readInt16LE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length);
  var val = this[offset] | this[offset + 1] << 8;
  return val & 0x8000 ? val | 0xFFFF0000 : val;
};

Buffer.prototype.readInt16BE = function readInt16BE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 2, this.length);
  var val = this[offset + 1] | this[offset] << 8;
  return val & 0x8000 ? val | 0xFFFF0000 : val;
};

Buffer.prototype.readInt32LE = function readInt32LE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length);
  return this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16 | this[offset + 3] << 24;
};

Buffer.prototype.readInt32BE = function readInt32BE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length);
  return this[offset] << 24 | this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3];
};

Buffer.prototype.readFloatLE = function readFloatLE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length);
  return ieee754.read(this, offset, true, 23, 4);
};

Buffer.prototype.readFloatBE = function readFloatBE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 4, this.length);
  return ieee754.read(this, offset, false, 23, 4);
};

Buffer.prototype.readDoubleLE = function readDoubleLE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length);
  return ieee754.read(this, offset, true, 52, 8);
};

Buffer.prototype.readDoubleBE = function readDoubleBE(offset, noAssert) {
  if (!noAssert) checkOffset(offset, 8, this.length);
  return ieee754.read(this, offset, false, 52, 8);
};

function checkInt(buf, value, offset, ext, max, min) {
  if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance');
  if (value > max || value < min) throw new RangeError('"value" argument is out of bounds');
  if (offset + ext > buf.length) throw new RangeError('Index out of range');
}

Buffer.prototype.writeUIntLE = function writeUIntLE(value, offset, byteLength, noAssert) {
  value = +value;
  offset = offset | 0;
  byteLength = byteLength | 0;

  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1;
    checkInt(this, value, offset, byteLength, maxBytes, 0);
  }

  var mul = 1;
  var i = 0;
  this[offset] = value & 0xFF;

  while (++i < byteLength && (mul *= 0x100)) {
    this[offset + i] = value / mul & 0xFF;
  }

  return offset + byteLength;
};

Buffer.prototype.writeUIntBE = function writeUIntBE(value, offset, byteLength, noAssert) {
  value = +value;
  offset = offset | 0;
  byteLength = byteLength | 0;

  if (!noAssert) {
    var maxBytes = Math.pow(2, 8 * byteLength) - 1;
    checkInt(this, value, offset, byteLength, maxBytes, 0);
  }

  var i = byteLength - 1;
  var mul = 1;
  this[offset + i] = value & 0xFF;

  while (--i >= 0 && (mul *= 0x100)) {
    this[offset + i] = value / mul & 0xFF;
  }

  return offset + byteLength;
};

Buffer.prototype.writeUInt8 = function writeUInt8(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0);
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value);
  this[offset] = value & 0xff;
  return offset + 1;
};

function objectWriteUInt16(buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffff + value + 1;

  for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
    buf[offset + i] = (value & 0xff << 8 * (littleEndian ? i : 1 - i)) >>> (littleEndian ? i : 1 - i) * 8;
  }
}

Buffer.prototype.writeUInt16LE = function writeUInt16LE(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value & 0xff;
    this[offset + 1] = value >>> 8;
  } else {
    objectWriteUInt16(this, value, offset, true);
  }

  return offset + 2;
};

Buffer.prototype.writeUInt16BE = function writeUInt16BE(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value >>> 8;
    this[offset + 1] = value & 0xff;
  } else {
    objectWriteUInt16(this, value, offset, false);
  }

  return offset + 2;
};

function objectWriteUInt32(buf, value, offset, littleEndian) {
  if (value < 0) value = 0xffffffff + value + 1;

  for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
    buf[offset + i] = value >>> (littleEndian ? i : 3 - i) * 8 & 0xff;
  }
}

Buffer.prototype.writeUInt32LE = function writeUInt32LE(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset + 3] = value >>> 24;
    this[offset + 2] = value >>> 16;
    this[offset + 1] = value >>> 8;
    this[offset] = value & 0xff;
  } else {
    objectWriteUInt32(this, value, offset, true);
  }

  return offset + 4;
};

Buffer.prototype.writeUInt32BE = function writeUInt32BE(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value >>> 24;
    this[offset + 1] = value >>> 16;
    this[offset + 2] = value >>> 8;
    this[offset + 3] = value & 0xff;
  } else {
    objectWriteUInt32(this, value, offset, false);
  }

  return offset + 4;
};

Buffer.prototype.writeIntLE = function writeIntLE(value, offset, byteLength, noAssert) {
  value = +value;
  offset = offset | 0;

  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1);
    checkInt(this, value, offset, byteLength, limit - 1, -limit);
  }

  var i = 0;
  var mul = 1;
  var sub = 0;
  this[offset] = value & 0xFF;

  while (++i < byteLength && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
      sub = 1;
    }

    this[offset + i] = (value / mul >> 0) - sub & 0xFF;
  }

  return offset + byteLength;
};

Buffer.prototype.writeIntBE = function writeIntBE(value, offset, byteLength, noAssert) {
  value = +value;
  offset = offset | 0;

  if (!noAssert) {
    var limit = Math.pow(2, 8 * byteLength - 1);
    checkInt(this, value, offset, byteLength, limit - 1, -limit);
  }

  var i = byteLength - 1;
  var mul = 1;
  var sub = 0;
  this[offset + i] = value & 0xFF;

  while (--i >= 0 && (mul *= 0x100)) {
    if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
      sub = 1;
    }

    this[offset + i] = (value / mul >> 0) - sub & 0xFF;
  }

  return offset + byteLength;
};

Buffer.prototype.writeInt8 = function writeInt8(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80);
  if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value);
  if (value < 0) value = 0xff + value + 1;
  this[offset] = value & 0xff;
  return offset + 1;
};

Buffer.prototype.writeInt16LE = function writeInt16LE(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value & 0xff;
    this[offset + 1] = value >>> 8;
  } else {
    objectWriteUInt16(this, value, offset, true);
  }

  return offset + 2;
};

Buffer.prototype.writeInt16BE = function writeInt16BE(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value >>> 8;
    this[offset + 1] = value & 0xff;
  } else {
    objectWriteUInt16(this, value, offset, false);
  }

  return offset + 2;
};

Buffer.prototype.writeInt32LE = function writeInt32LE(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value & 0xff;
    this[offset + 1] = value >>> 8;
    this[offset + 2] = value >>> 16;
    this[offset + 3] = value >>> 24;
  } else {
    objectWriteUInt32(this, value, offset, true);
  }

  return offset + 4;
};

Buffer.prototype.writeInt32BE = function writeInt32BE(value, offset, noAssert) {
  value = +value;
  offset = offset | 0;
  if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
  if (value < 0) value = 0xffffffff + value + 1;

  if (Buffer.TYPED_ARRAY_SUPPORT) {
    this[offset] = value >>> 24;
    this[offset + 1] = value >>> 16;
    this[offset + 2] = value >>> 8;
    this[offset + 3] = value & 0xff;
  } else {
    objectWriteUInt32(this, value, offset, false);
  }

  return offset + 4;
};

function checkIEEE754(buf, value, offset, ext, max, min) {
  if (offset + ext > buf.length) throw new RangeError('Index out of range');
  if (offset < 0) throw new RangeError('Index out of range');
}

function writeFloat(buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38);
  }

  ieee754.write(buf, value, offset, littleEndian, 23, 4);
  return offset + 4;
}

Buffer.prototype.writeFloatLE = function writeFloatLE(value, offset, noAssert) {
  return writeFloat(this, value, offset, true, noAssert);
};

Buffer.prototype.writeFloatBE = function writeFloatBE(value, offset, noAssert) {
  return writeFloat(this, value, offset, false, noAssert);
};

function writeDouble(buf, value, offset, littleEndian, noAssert) {
  if (!noAssert) {
    checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308);
  }

  ieee754.write(buf, value, offset, littleEndian, 52, 8);
  return offset + 8;
}

Buffer.prototype.writeDoubleLE = function writeDoubleLE(value, offset, noAssert) {
  return writeDouble(this, value, offset, true, noAssert);
};

Buffer.prototype.writeDoubleBE = function writeDoubleBE(value, offset, noAssert) {
  return writeDouble(this, value, offset, false, noAssert);
}; // copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)


Buffer.prototype.copy = function copy(target, targetStart, start, end) {
  if (!start) start = 0;
  if (!end && end !== 0) end = this.length;
  if (targetStart >= target.length) targetStart = target.length;
  if (!targetStart) targetStart = 0;
  if (end > 0 && end < start) end = start; // Copy 0 bytes; we're done

  if (end === start) return 0;
  if (target.length === 0 || this.length === 0) return 0; // Fatal error conditions

  if (targetStart < 0) {
    throw new RangeError('targetStart out of bounds');
  }

  if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds');
  if (end < 0) throw new RangeError('sourceEnd out of bounds'); // Are we oob?

  if (end > this.length) end = this.length;

  if (target.length - targetStart < end - start) {
    end = target.length - targetStart + start;
  }

  var len = end - start;
  var i;

  if (this === target && start < targetStart && targetStart < end) {
    // descending copy from end
    for (i = len - 1; i >= 0; --i) {
      target[i + targetStart] = this[i + start];
    }
  } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
    // ascending copy from start
    for (i = 0; i < len; ++i) {
      target[i + targetStart] = this[i + start];
    }
  } else {
    Uint8Array.prototype.set.call(target, this.subarray(start, start + len), targetStart);
  }

  return len;
}; // Usage:
//    buffer.fill(number[, offset[, end]])
//    buffer.fill(buffer[, offset[, end]])
//    buffer.fill(string[, offset[, end]][, encoding])


Buffer.prototype.fill = function fill(val, start, end, encoding) {
  // Handle string cases:
  if (typeof val === 'string') {
    if (typeof start === 'string') {
      encoding = start;
      start = 0;
      end = this.length;
    } else if (typeof end === 'string') {
      encoding = end;
      end = this.length;
    }

    if (val.length === 1) {
      var code = val.charCodeAt(0);

      if (code < 256) {
        val = code;
      }
    }

    if (encoding !== undefined && typeof encoding !== 'string') {
      throw new TypeError('encoding must be a string');
    }

    if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
      throw new TypeError('Unknown encoding: ' + encoding);
    }
  } else if (typeof val === 'number') {
    val = val & 255;
  } // Invalid ranges are not set to a default, so can range check early.


  if (start < 0 || this.length < start || this.length < end) {
    throw new RangeError('Out of range index');
  }

  if (end <= start) {
    return this;
  }

  start = start >>> 0;
  end = end === undefined ? this.length : end >>> 0;
  if (!val) val = 0;
  var i;

  if (typeof val === 'number') {
    for (i = start; i < end; ++i) {
      this[i] = val;
    }
  } else {
    var bytes = Buffer.isBuffer(val) ? val : utf8ToBytes(new Buffer(val, encoding).toString());
    var len = bytes.length;

    for (i = 0; i < end - start; ++i) {
      this[i + start] = bytes[i % len];
    }
  }

  return this;
}; // HELPER FUNCTIONS
// ================


var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g;

function base64clean(str) {
  // Node strips out invalid characters like \n and \t from the string, base64-js does not
  str = stringtrim(str).replace(INVALID_BASE64_RE, ''); // Node converts strings with length < 2 to ''

  if (str.length < 2) return ''; // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not

  while (str.length % 4 !== 0) {
    str = str + '=';
  }

  return str;
}

function stringtrim(str) {
  if (str.trim) return str.trim();
  return str.replace(/^\s+|\s+$/g, '');
}

function toHex(n) {
  if (n < 16) return '0' + n.toString(16);
  return n.toString(16);
}

function utf8ToBytes(string, units) {
  units = units || Infinity;
  var codePoint;
  var length = string.length;
  var leadSurrogate = null;
  var bytes = [];

  for (var i = 0; i < length; ++i) {
    codePoint = string.charCodeAt(i); // is surrogate component

    if (codePoint > 0xD7FF && codePoint < 0xE000) {
      // last char was a lead
      if (!leadSurrogate) {
        // no lead yet
        if (codePoint > 0xDBFF) {
          // unexpected trail
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
          continue;
        } else if (i + 1 === length) {
          // unpaired lead
          if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
          continue;
        } // valid lead


        leadSurrogate = codePoint;
        continue;
      } // 2 leads in a row


      if (codePoint < 0xDC00) {
        if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
        leadSurrogate = codePoint;
        continue;
      } // valid surrogate pair


      codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000;
    } else if (leadSurrogate) {
      // valid bmp char, but last char was a lead
      if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
    }

    leadSurrogate = null; // encode utf8

    if (codePoint < 0x80) {
      if ((units -= 1) < 0) break;
      bytes.push(codePoint);
    } else if (codePoint < 0x800) {
      if ((units -= 2) < 0) break;
      bytes.push(codePoint >> 0x6 | 0xC0, codePoint & 0x3F | 0x80);
    } else if (codePoint < 0x10000) {
      if ((units -= 3) < 0) break;
      bytes.push(codePoint >> 0xC | 0xE0, codePoint >> 0x6 & 0x3F | 0x80, codePoint & 0x3F | 0x80);
    } else if (codePoint < 0x110000) {
      if ((units -= 4) < 0) break;
      bytes.push(codePoint >> 0x12 | 0xF0, codePoint >> 0xC & 0x3F | 0x80, codePoint >> 0x6 & 0x3F | 0x80, codePoint & 0x3F | 0x80);
    } else {
      throw new Error('Invalid code point');
    }
  }

  return bytes;
}

function asciiToBytes(str) {
  var byteArray = [];

  for (var i = 0; i < str.length; ++i) {
    // Node's code seems to be doing this and not & 0x7F..
    byteArray.push(str.charCodeAt(i) & 0xFF);
  }

  return byteArray;
}

function utf16leToBytes(str, units) {
  var c, hi, lo;
  var byteArray = [];

  for (var i = 0; i < str.length; ++i) {
    if ((units -= 2) < 0) break;
    c = str.charCodeAt(i);
    hi = c >> 8;
    lo = c % 256;
    byteArray.push(lo);
    byteArray.push(hi);
  }

  return byteArray;
}

function base64ToBytes(str) {
  return base64.toByteArray(base64clean(str));
}

function blitBuffer(src, dst, offset, length) {
  for (var i = 0; i < length; ++i) {
    if (i + offset >= dst.length || i >= src.length) break;
    dst[i + offset] = src[i];
  }

  return i;
}

function isnan(val) {
  return val !== val; // eslint-disable-line no-self-compare
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/ieee754/index.js":
/*!***************************************!*\
  !*** ./node_modules/ieee754/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*! ieee754. BSD-3-Clause License. Feross Aboukhadijeh <https://feross.org/opensource> */
exports.read = function (buffer, offset, isLE, mLen, nBytes) {
  var e, m;
  var eLen = nBytes * 8 - mLen - 1;
  var eMax = (1 << eLen) - 1;
  var eBias = eMax >> 1;
  var nBits = -7;
  var i = isLE ? nBytes - 1 : 0;
  var d = isLE ? -1 : 1;
  var s = buffer[offset + i];
  i += d;
  e = s & (1 << -nBits) - 1;
  s >>= -nBits;
  nBits += eLen;

  for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  m = e & (1 << -nBits) - 1;
  e >>= -nBits;
  nBits += mLen;

  for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

  if (e === 0) {
    e = 1 - eBias;
  } else if (e === eMax) {
    return m ? NaN : (s ? -1 : 1) * Infinity;
  } else {
    m = m + Math.pow(2, mLen);
    e = e - eBias;
  }

  return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
};

exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
  var e, m, c;
  var eLen = nBytes * 8 - mLen - 1;
  var eMax = (1 << eLen) - 1;
  var eBias = eMax >> 1;
  var rt = mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0;
  var i = isLE ? 0 : nBytes - 1;
  var d = isLE ? 1 : -1;
  var s = value < 0 || value === 0 && 1 / value < 0 ? 1 : 0;
  value = Math.abs(value);

  if (isNaN(value) || value === Infinity) {
    m = isNaN(value) ? 1 : 0;
    e = eMax;
  } else {
    e = Math.floor(Math.log(value) / Math.LN2);

    if (value * (c = Math.pow(2, -e)) < 1) {
      e--;
      c *= 2;
    }

    if (e + eBias >= 1) {
      value += rt / c;
    } else {
      value += rt * Math.pow(2, 1 - eBias);
    }

    if (value * c >= 2) {
      e++;
      c /= 2;
    }

    if (e + eBias >= eMax) {
      m = 0;
      e = eMax;
    } else if (e + eBias >= 1) {
      m = (value * c - 1) * Math.pow(2, mLen);
      e = e + eBias;
    } else {
      m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
      e = 0;
    }
  }

  for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

  e = e << mLen | m;
  eLen += mLen;

  for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

  buffer[offset + i - d] |= s * 128;
};

/***/ }),

/***/ "./node_modules/isarray/index.js":
/*!***************************************!*\
  !*** ./node_modules/isarray/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var toString = {}.toString;

module.exports = Array.isArray || function (arr) {
  return toString.call(arr) == '[object Array]';
};

/***/ }),

/***/ "./node_modules/url-processor/lib/index.js":
/*!*************************************************!*\
  !*** ./node_modules/url-processor/lib/index.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function webpackUniversalModuleDefinition(root, factory) {
  if (( false ? undefined : _typeof(exports)) === 'object' && ( false ? undefined : _typeof(module)) === 'object') module.exports = factory();else if (true) !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else {}
})(this, function () {
  return (
    /******/
    function (modules) {
      // webpackBootstrap

      /******/
      // The module cache

      /******/
      var installedModules = {};
      /******/

      /******/
      // The require function

      /******/

      function __webpack_require__(moduleId) {
        /******/

        /******/
        // Check if module is in cache

        /******/
        if (installedModules[moduleId]) {
          /******/
          return installedModules[moduleId].exports;
          /******/
        }
        /******/
        // Create a new module (and put it into the cache)

        /******/


        var module = installedModules[moduleId] = {
          /******/
          i: moduleId,

          /******/
          l: false,

          /******/
          exports: {}
          /******/

        };
        /******/

        /******/
        // Execute the module function

        /******/

        modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
        /******/

        /******/
        // Flag the module as loaded

        /******/

        module.l = true;
        /******/

        /******/
        // Return the exports of the module

        /******/

        return module.exports;
        /******/
      }
      /******/

      /******/

      /******/
      // expose the modules object (__webpack_modules__)

      /******/


      __webpack_require__.m = modules;
      /******/

      /******/
      // expose the module cache

      /******/

      __webpack_require__.c = installedModules;
      /******/

      /******/
      // define getter function for harmony exports

      /******/

      __webpack_require__.d = function (exports, name, getter) {
        /******/
        if (!__webpack_require__.o(exports, name)) {
          /******/
          Object.defineProperty(exports, name, {
            enumerable: true,
            get: getter
          });
          /******/
        }
        /******/

      };
      /******/

      /******/
      // define __esModule on exports

      /******/


      __webpack_require__.r = function (exports) {
        /******/
        if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
          /******/
          Object.defineProperty(exports, Symbol.toStringTag, {
            value: 'Module'
          });
          /******/
        }
        /******/


        Object.defineProperty(exports, '__esModule', {
          value: true
        });
        /******/
      };
      /******/

      /******/
      // create a fake namespace object

      /******/
      // mode & 1: value is a module id, require it

      /******/
      // mode & 2: merge all properties of value into the ns

      /******/
      // mode & 4: return value when already ns object

      /******/
      // mode & 8|1: behave like require

      /******/


      __webpack_require__.t = function (value, mode) {
        /******/
        if (mode & 1) value = __webpack_require__(value);
        /******/

        if (mode & 8) return value;
        /******/

        if (mode & 4 && _typeof(value) === 'object' && value && value.__esModule) return value;
        /******/

        var ns = Object.create(null);
        /******/

        __webpack_require__.r(ns);
        /******/


        Object.defineProperty(ns, 'default', {
          enumerable: true,
          value: value
        });
        /******/

        if (mode & 2 && typeof value != 'string') for (var key in value) {
          __webpack_require__.d(ns, key, function (key) {
            return value[key];
          }.bind(null, key));
        }
        /******/

        return ns;
        /******/
      };
      /******/

      /******/
      // getDefaultExport function for compatibility with non-harmony modules

      /******/


      __webpack_require__.n = function (module) {
        /******/
        var getter = module && module.__esModule ?
        /******/
        function getDefault() {
          return module['default'];
        } :
        /******/
        function getModuleExports() {
          return module;
        };
        /******/

        __webpack_require__.d(getter, 'a', getter);
        /******/


        return getter;
        /******/
      };
      /******/

      /******/
      // Object.prototype.hasOwnProperty.call

      /******/


      __webpack_require__.o = function (object, property) {
        return Object.prototype.hasOwnProperty.call(object, property);
      };
      /******/

      /******/
      // __webpack_public_path__

      /******/


      __webpack_require__.p = "";
      /******/

      /******/

      /******/
      // Load entry module and return exports

      /******/

      return __webpack_require__(__webpack_require__.s = "./src/index.ts");
      /******/
    }(
    /************************************************************************/

    /******/
    {
      /***/
      "./node_modules/base64-js/index.js":
      /*!*****************************************!*\
        !*** ./node_modules/base64-js/index.js ***!
        \*****************************************/

      /*! no static exports found */

      /***/
      function node_modulesBase64JsIndexJs(module, exports, __webpack_require__) {
        "use strict";

        exports.byteLength = byteLength;
        exports.toByteArray = toByteArray;
        exports.fromByteArray = fromByteArray;
        var lookup = [];
        var revLookup = [];
        var Arr = typeof Uint8Array !== 'undefined' ? Uint8Array : Array;
        var code = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

        for (var i = 0, len = code.length; i < len; ++i) {
          lookup[i] = code[i];
          revLookup[code.charCodeAt(i)] = i;
        } // Support decoding URL-safe base64 strings, as Node.js does.
        // See: https://en.wikipedia.org/wiki/Base64#URL_applications


        revLookup['-'.charCodeAt(0)] = 62;
        revLookup['_'.charCodeAt(0)] = 63;

        function getLens(b64) {
          var len = b64.length;

          if (len % 4 > 0) {
            throw new Error('Invalid string. Length must be a multiple of 4');
          } // Trim off extra bytes after placeholder bytes are found
          // See: https://github.com/beatgammit/base64-js/issues/42


          var validLen = b64.indexOf('=');
          if (validLen === -1) validLen = len;
          var placeHoldersLen = validLen === len ? 0 : 4 - validLen % 4;
          return [validLen, placeHoldersLen];
        } // base64 is 4/3 + up to two characters of the original data


        function byteLength(b64) {
          var lens = getLens(b64);
          var validLen = lens[0];
          var placeHoldersLen = lens[1];
          return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
        }

        function _byteLength(b64, validLen, placeHoldersLen) {
          return (validLen + placeHoldersLen) * 3 / 4 - placeHoldersLen;
        }

        function toByteArray(b64) {
          var tmp;
          var lens = getLens(b64);
          var validLen = lens[0];
          var placeHoldersLen = lens[1];
          var arr = new Arr(_byteLength(b64, validLen, placeHoldersLen));
          var curByte = 0; // if there are placeholders, only get up to the last complete 4 chars

          var len = placeHoldersLen > 0 ? validLen - 4 : validLen;
          var i;

          for (i = 0; i < len; i += 4) {
            tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
            arr[curByte++] = tmp >> 16 & 0xFF;
            arr[curByte++] = tmp >> 8 & 0xFF;
            arr[curByte++] = tmp & 0xFF;
          }

          if (placeHoldersLen === 2) {
            tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
            arr[curByte++] = tmp & 0xFF;
          }

          if (placeHoldersLen === 1) {
            tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
            arr[curByte++] = tmp >> 8 & 0xFF;
            arr[curByte++] = tmp & 0xFF;
          }

          return arr;
        }

        function tripletToBase64(num) {
          return lookup[num >> 18 & 0x3F] + lookup[num >> 12 & 0x3F] + lookup[num >> 6 & 0x3F] + lookup[num & 0x3F];
        }

        function encodeChunk(uint8, start, end) {
          var tmp;
          var output = [];

          for (var i = start; i < end; i += 3) {
            tmp = (uint8[i] << 16 & 0xFF0000) + (uint8[i + 1] << 8 & 0xFF00) + (uint8[i + 2] & 0xFF);
            output.push(tripletToBase64(tmp));
          }

          return output.join('');
        }

        function fromByteArray(uint8) {
          var tmp;
          var len = uint8.length;
          var extraBytes = len % 3; // if we have 1 byte left, pad 2 bytes

          var parts = [];
          var maxChunkLength = 16383; // must be multiple of 3
          // go through the array every three bytes, we'll deal with trailing stuff later

          for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
            parts.push(encodeChunk(uint8, i, i + maxChunkLength > len2 ? len2 : i + maxChunkLength));
          } // pad the end with zeros, but make sure to not forget the extra bytes


          if (extraBytes === 1) {
            tmp = uint8[len - 1];
            parts.push(lookup[tmp >> 2] + lookup[tmp << 4 & 0x3F] + '==');
          } else if (extraBytes === 2) {
            tmp = (uint8[len - 2] << 8) + uint8[len - 1];
            parts.push(lookup[tmp >> 10] + lookup[tmp >> 4 & 0x3F] + lookup[tmp << 2 & 0x3F] + '=');
          }

          return parts.join('');
        }
        /***/

      },

      /***/
      "./node_modules/base64-url/index.js":
      /*!******************************************!*\
        !*** ./node_modules/base64-url/index.js ***!
        \******************************************/

      /*! no static exports found */

      /***/
      function node_modulesBase64UrlIndexJs(module, exports, __webpack_require__) {
        "use strict";
        /* WEBPACK VAR INJECTION */

        (function (Buffer) {
          module.exports = {
            unescape: unescape,
            escape: escape,
            encode: encode,
            decode: decode
          };

          function unescape(str) {
            return (str + '==='.slice((str.length + 3) % 4)).replace(/-/g, '+').replace(/_/g, '/');
          }

          function escape(str) {
            return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/=/g, '');
          }

          function encode(str, encoding) {
            return escape(Buffer.from(str, encoding || 'utf8').toString('base64'));
          }

          function decode(str, encoding) {
            return Buffer.from(unescape(str), 'base64').toString(encoding || 'utf8');
          }
          /* WEBPACK VAR INJECTION */

        }).call(this, __webpack_require__(
        /*! ./../buffer/index.js */
        "./node_modules/buffer/index.js").Buffer);
        /***/
      },

      /***/
      "./node_modules/buffer/index.js":
      /*!**************************************!*\
        !*** ./node_modules/buffer/index.js ***!
        \**************************************/

      /*! no static exports found */

      /***/
      function node_modulesBufferIndexJs(module, exports, __webpack_require__) {
        "use strict";
        /* WEBPACK VAR INJECTION */

        (function (global) {
          /*!
          * The buffer module from node.js, for the browser.
          *
          * @author   Feross Aboukhadijeh <http://feross.org>
          * @license  MIT
          */

          /* eslint-disable no-proto */
          var base64 = __webpack_require__(
          /*! base64-js */
          "./node_modules/base64-js/index.js");

          var ieee754 = __webpack_require__(
          /*! ieee754 */
          "./node_modules/ieee754/index.js");

          var isArray = __webpack_require__(
          /*! isarray */
          "./node_modules/isarray/index.js");

          exports.Buffer = Buffer;
          exports.SlowBuffer = SlowBuffer;
          exports.INSPECT_MAX_BYTES = 50;
          /**
           * If `Buffer.TYPED_ARRAY_SUPPORT`:
           *   === true    Use Uint8Array implementation (fastest)
           *   === false   Use Object implementation (most compatible, even IE6)
           *
           * Browsers that support typed arrays are IE 10+, Firefox 4+, Chrome 7+, Safari 5.1+,
           * Opera 11.6+, iOS 4.2+.
           *
           * Due to various browser bugs, sometimes the Object implementation will be used even
           * when the browser supports typed arrays.
           *
           * Note:
           *
           *   - Firefox 4-29 lacks support for adding new properties to `Uint8Array` instances,
           *     See: https://bugzilla.mozilla.org/show_bug.cgi?id=695438.
           *
           *   - Chrome 9-10 is missing the `TypedArray.prototype.subarray` function.
           *
           *   - IE10 has a broken `TypedArray.prototype.subarray` function which returns arrays of
           *     incorrect length in some situations.
          
           * We detect these buggy browsers and set `Buffer.TYPED_ARRAY_SUPPORT` to `false` so they
           * get the Object implementation, which is slower but behaves correctly.
           */

          Buffer.TYPED_ARRAY_SUPPORT = global.TYPED_ARRAY_SUPPORT !== undefined ? global.TYPED_ARRAY_SUPPORT : typedArraySupport();
          /*
           * Export kMaxLength after typed array support is determined.
           */

          exports.kMaxLength = kMaxLength();

          function typedArraySupport() {
            try {
              var arr = new Uint8Array(1);
              arr.__proto__ = {
                __proto__: Uint8Array.prototype,
                foo: function foo() {
                  return 42;
                }
              };
              return arr.foo() === 42 && // typed array instances can be augmented
              typeof arr.subarray === 'function' && // chrome 9-10 lack `subarray`
              arr.subarray(1, 1).byteLength === 0; // ie10 has broken `subarray`
            } catch (e) {
              return false;
            }
          }

          function kMaxLength() {
            return Buffer.TYPED_ARRAY_SUPPORT ? 0x7fffffff : 0x3fffffff;
          }

          function createBuffer(that, length) {
            if (kMaxLength() < length) {
              throw new RangeError('Invalid typed array length');
            }

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              // Return an augmented `Uint8Array` instance, for best performance
              that = new Uint8Array(length);
              that.__proto__ = Buffer.prototype;
            } else {
              // Fallback: Return an object instance of the Buffer class
              if (that === null) {
                that = new Buffer(length);
              }

              that.length = length;
            }

            return that;
          }
          /**
           * The Buffer constructor returns instances of `Uint8Array` that have their
           * prototype changed to `Buffer.prototype`. Furthermore, `Buffer` is a subclass of
           * `Uint8Array`, so the returned instances will have all the node `Buffer` methods
           * and the `Uint8Array` methods. Square bracket notation works as expected -- it
           * returns a single octet.
           *
           * The `Uint8Array` prototype remains unmodified.
           */


          function Buffer(arg, encodingOrOffset, length) {
            if (!Buffer.TYPED_ARRAY_SUPPORT && !(this instanceof Buffer)) {
              return new Buffer(arg, encodingOrOffset, length);
            } // Common case.


            if (typeof arg === 'number') {
              if (typeof encodingOrOffset === 'string') {
                throw new Error('If encoding is specified then the first argument must be a string');
              }

              return allocUnsafe(this, arg);
            }

            return from(this, arg, encodingOrOffset, length);
          }

          Buffer.poolSize = 8192; // not used by this implementation
          // TODO: Legacy, not needed anymore. Remove in next major version.

          Buffer._augment = function (arr) {
            arr.__proto__ = Buffer.prototype;
            return arr;
          };

          function from(that, value, encodingOrOffset, length) {
            if (typeof value === 'number') {
              throw new TypeError('"value" argument must not be a number');
            }

            if (typeof ArrayBuffer !== 'undefined' && value instanceof ArrayBuffer) {
              return fromArrayBuffer(that, value, encodingOrOffset, length);
            }

            if (typeof value === 'string') {
              return fromString(that, value, encodingOrOffset);
            }

            return fromObject(that, value);
          }
          /**
           * Functionally equivalent to Buffer(arg, encoding) but throws a TypeError
           * if value is a number.
           * Buffer.from(str[, encoding])
           * Buffer.from(array)
           * Buffer.from(buffer)
           * Buffer.from(arrayBuffer[, byteOffset[, length]])
           **/


          Buffer.from = function (value, encodingOrOffset, length) {
            return from(null, value, encodingOrOffset, length);
          };

          if (Buffer.TYPED_ARRAY_SUPPORT) {
            Buffer.prototype.__proto__ = Uint8Array.prototype;
            Buffer.__proto__ = Uint8Array;

            if (typeof Symbol !== 'undefined' && Symbol.species && Buffer[Symbol.species] === Buffer) {
              // Fix subarray() in ES2016. See: https://github.com/feross/buffer/pull/97
              Object.defineProperty(Buffer, Symbol.species, {
                value: null,
                configurable: true
              });
            }
          }

          function assertSize(size) {
            if (typeof size !== 'number') {
              throw new TypeError('"size" argument must be a number');
            } else if (size < 0) {
              throw new RangeError('"size" argument must not be negative');
            }
          }

          function alloc(that, size, fill, encoding) {
            assertSize(size);

            if (size <= 0) {
              return createBuffer(that, size);
            }

            if (fill !== undefined) {
              // Only pay attention to encoding if it's a string. This
              // prevents accidentally sending in a number that would
              // be interpretted as a start offset.
              return typeof encoding === 'string' ? createBuffer(that, size).fill(fill, encoding) : createBuffer(that, size).fill(fill);
            }

            return createBuffer(that, size);
          }
          /**
           * Creates a new filled Buffer instance.
           * alloc(size[, fill[, encoding]])
           **/


          Buffer.alloc = function (size, fill, encoding) {
            return alloc(null, size, fill, encoding);
          };

          function allocUnsafe(that, size) {
            assertSize(size);
            that = createBuffer(that, size < 0 ? 0 : checked(size) | 0);

            if (!Buffer.TYPED_ARRAY_SUPPORT) {
              for (var i = 0; i < size; ++i) {
                that[i] = 0;
              }
            }

            return that;
          }
          /**
           * Equivalent to Buffer(num), by default creates a non-zero-filled Buffer instance.
           * */


          Buffer.allocUnsafe = function (size) {
            return allocUnsafe(null, size);
          };
          /**
           * Equivalent to SlowBuffer(num), by default creates a non-zero-filled Buffer instance.
           */


          Buffer.allocUnsafeSlow = function (size) {
            return allocUnsafe(null, size);
          };

          function fromString(that, string, encoding) {
            if (typeof encoding !== 'string' || encoding === '') {
              encoding = 'utf8';
            }

            if (!Buffer.isEncoding(encoding)) {
              throw new TypeError('"encoding" must be a valid string encoding');
            }

            var length = byteLength(string, encoding) | 0;
            that = createBuffer(that, length);
            var actual = that.write(string, encoding);

            if (actual !== length) {
              // Writing a hex string, for example, that contains invalid characters will
              // cause everything after the first invalid character to be ignored. (e.g.
              // 'abxxcd' will be treated as 'ab')
              that = that.slice(0, actual);
            }

            return that;
          }

          function fromArrayLike(that, array) {
            var length = array.length < 0 ? 0 : checked(array.length) | 0;
            that = createBuffer(that, length);

            for (var i = 0; i < length; i += 1) {
              that[i] = array[i] & 255;
            }

            return that;
          }

          function fromArrayBuffer(that, array, byteOffset, length) {
            array.byteLength; // this throws if `array` is not a valid ArrayBuffer

            if (byteOffset < 0 || array.byteLength < byteOffset) {
              throw new RangeError('\'offset\' is out of bounds');
            }

            if (array.byteLength < byteOffset + (length || 0)) {
              throw new RangeError('\'length\' is out of bounds');
            }

            if (byteOffset === undefined && length === undefined) {
              array = new Uint8Array(array);
            } else if (length === undefined) {
              array = new Uint8Array(array, byteOffset);
            } else {
              array = new Uint8Array(array, byteOffset, length);
            }

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              // Return an augmented `Uint8Array` instance, for best performance
              that = array;
              that.__proto__ = Buffer.prototype;
            } else {
              // Fallback: Return an object instance of the Buffer class
              that = fromArrayLike(that, array);
            }

            return that;
          }

          function fromObject(that, obj) {
            if (Buffer.isBuffer(obj)) {
              var len = checked(obj.length) | 0;
              that = createBuffer(that, len);

              if (that.length === 0) {
                return that;
              }

              obj.copy(that, 0, 0, len);
              return that;
            }

            if (obj) {
              if (typeof ArrayBuffer !== 'undefined' && obj.buffer instanceof ArrayBuffer || 'length' in obj) {
                if (typeof obj.length !== 'number' || isnan(obj.length)) {
                  return createBuffer(that, 0);
                }

                return fromArrayLike(that, obj);
              }

              if (obj.type === 'Buffer' && isArray(obj.data)) {
                return fromArrayLike(that, obj.data);
              }
            }

            throw new TypeError('First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.');
          }

          function checked(length) {
            // Note: cannot use `length < kMaxLength()` here because that fails when
            // length is NaN (which is otherwise coerced to zero.)
            if (length >= kMaxLength()) {
              throw new RangeError('Attempt to allocate Buffer larger than maximum ' + 'size: 0x' + kMaxLength().toString(16) + ' bytes');
            }

            return length | 0;
          }

          function SlowBuffer(length) {
            if (+length != length) {
              // eslint-disable-line eqeqeq
              length = 0;
            }

            return Buffer.alloc(+length);
          }

          Buffer.isBuffer = function isBuffer(b) {
            return !!(b != null && b._isBuffer);
          };

          Buffer.compare = function compare(a, b) {
            if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
              throw new TypeError('Arguments must be Buffers');
            }

            if (a === b) return 0;
            var x = a.length;
            var y = b.length;

            for (var i = 0, len = Math.min(x, y); i < len; ++i) {
              if (a[i] !== b[i]) {
                x = a[i];
                y = b[i];
                break;
              }
            }

            if (x < y) return -1;
            if (y < x) return 1;
            return 0;
          };

          Buffer.isEncoding = function isEncoding(encoding) {
            switch (String(encoding).toLowerCase()) {
              case 'hex':
              case 'utf8':
              case 'utf-8':
              case 'ascii':
              case 'latin1':
              case 'binary':
              case 'base64':
              case 'ucs2':
              case 'ucs-2':
              case 'utf16le':
              case 'utf-16le':
                return true;

              default:
                return false;
            }
          };

          Buffer.concat = function concat(list, length) {
            if (!isArray(list)) {
              throw new TypeError('"list" argument must be an Array of Buffers');
            }

            if (list.length === 0) {
              return Buffer.alloc(0);
            }

            var i;

            if (length === undefined) {
              length = 0;

              for (i = 0; i < list.length; ++i) {
                length += list[i].length;
              }
            }

            var buffer = Buffer.allocUnsafe(length);
            var pos = 0;

            for (i = 0; i < list.length; ++i) {
              var buf = list[i];

              if (!Buffer.isBuffer(buf)) {
                throw new TypeError('"list" argument must be an Array of Buffers');
              }

              buf.copy(buffer, pos);
              pos += buf.length;
            }

            return buffer;
          };

          function byteLength(string, encoding) {
            if (Buffer.isBuffer(string)) {
              return string.length;
            }

            if (typeof ArrayBuffer !== 'undefined' && typeof ArrayBuffer.isView === 'function' && (ArrayBuffer.isView(string) || string instanceof ArrayBuffer)) {
              return string.byteLength;
            }

            if (typeof string !== 'string') {
              string = '' + string;
            }

            var len = string.length;
            if (len === 0) return 0; // Use a for loop to avoid recursion

            var loweredCase = false;

            for (;;) {
              switch (encoding) {
                case 'ascii':
                case 'latin1':
                case 'binary':
                  return len;

                case 'utf8':
                case 'utf-8':
                case undefined:
                  return utf8ToBytes(string).length;

                case 'ucs2':
                case 'ucs-2':
                case 'utf16le':
                case 'utf-16le':
                  return len * 2;

                case 'hex':
                  return len >>> 1;

                case 'base64':
                  return base64ToBytes(string).length;

                default:
                  if (loweredCase) return utf8ToBytes(string).length; // assume utf8

                  encoding = ('' + encoding).toLowerCase();
                  loweredCase = true;
              }
            }
          }

          Buffer.byteLength = byteLength;

          function slowToString(encoding, start, end) {
            var loweredCase = false; // No need to verify that "this.length <= MAX_UINT32" since it's a read-only
            // property of a typed array.
            // This behaves neither like String nor Uint8Array in that we set start/end
            // to their upper/lower bounds if the value passed is out of range.
            // undefined is handled specially as per ECMA-262 6th Edition,
            // Section 13.3.3.7 Runtime Semantics: KeyedBindingInitialization.

            if (start === undefined || start < 0) {
              start = 0;
            } // Return early if start > this.length. Done here to prevent potential uint32
            // coercion fail below.


            if (start > this.length) {
              return '';
            }

            if (end === undefined || end > this.length) {
              end = this.length;
            }

            if (end <= 0) {
              return '';
            } // Force coersion to uint32. This will also coerce falsey/NaN values to 0.


            end >>>= 0;
            start >>>= 0;

            if (end <= start) {
              return '';
            }

            if (!encoding) encoding = 'utf8';

            while (true) {
              switch (encoding) {
                case 'hex':
                  return hexSlice(this, start, end);

                case 'utf8':
                case 'utf-8':
                  return utf8Slice(this, start, end);

                case 'ascii':
                  return asciiSlice(this, start, end);

                case 'latin1':
                case 'binary':
                  return latin1Slice(this, start, end);

                case 'base64':
                  return base64Slice(this, start, end);

                case 'ucs2':
                case 'ucs-2':
                case 'utf16le':
                case 'utf-16le':
                  return utf16leSlice(this, start, end);

                default:
                  if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding);
                  encoding = (encoding + '').toLowerCase();
                  loweredCase = true;
              }
            }
          } // The property is used by `Buffer.isBuffer` and `is-buffer` (in Safari 5-7) to detect
          // Buffer instances.


          Buffer.prototype._isBuffer = true;

          function swap(b, n, m) {
            var i = b[n];
            b[n] = b[m];
            b[m] = i;
          }

          Buffer.prototype.swap16 = function swap16() {
            var len = this.length;

            if (len % 2 !== 0) {
              throw new RangeError('Buffer size must be a multiple of 16-bits');
            }

            for (var i = 0; i < len; i += 2) {
              swap(this, i, i + 1);
            }

            return this;
          };

          Buffer.prototype.swap32 = function swap32() {
            var len = this.length;

            if (len % 4 !== 0) {
              throw new RangeError('Buffer size must be a multiple of 32-bits');
            }

            for (var i = 0; i < len; i += 4) {
              swap(this, i, i + 3);
              swap(this, i + 1, i + 2);
            }

            return this;
          };

          Buffer.prototype.swap64 = function swap64() {
            var len = this.length;

            if (len % 8 !== 0) {
              throw new RangeError('Buffer size must be a multiple of 64-bits');
            }

            for (var i = 0; i < len; i += 8) {
              swap(this, i, i + 7);
              swap(this, i + 1, i + 6);
              swap(this, i + 2, i + 5);
              swap(this, i + 3, i + 4);
            }

            return this;
          };

          Buffer.prototype.toString = function toString() {
            var length = this.length | 0;
            if (length === 0) return '';
            if (arguments.length === 0) return utf8Slice(this, 0, length);
            return slowToString.apply(this, arguments);
          };

          Buffer.prototype.equals = function equals(b) {
            if (!Buffer.isBuffer(b)) throw new TypeError('Argument must be a Buffer');
            if (this === b) return true;
            return Buffer.compare(this, b) === 0;
          };

          Buffer.prototype.inspect = function inspect() {
            var str = '';
            var max = exports.INSPECT_MAX_BYTES;

            if (this.length > 0) {
              str = this.toString('hex', 0, max).match(/.{2}/g).join(' ');
              if (this.length > max) str += ' ... ';
            }

            return '<Buffer ' + str + '>';
          };

          Buffer.prototype.compare = function compare(target, start, end, thisStart, thisEnd) {
            if (!Buffer.isBuffer(target)) {
              throw new TypeError('Argument must be a Buffer');
            }

            if (start === undefined) {
              start = 0;
            }

            if (end === undefined) {
              end = target ? target.length : 0;
            }

            if (thisStart === undefined) {
              thisStart = 0;
            }

            if (thisEnd === undefined) {
              thisEnd = this.length;
            }

            if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
              throw new RangeError('out of range index');
            }

            if (thisStart >= thisEnd && start >= end) {
              return 0;
            }

            if (thisStart >= thisEnd) {
              return -1;
            }

            if (start >= end) {
              return 1;
            }

            start >>>= 0;
            end >>>= 0;
            thisStart >>>= 0;
            thisEnd >>>= 0;
            if (this === target) return 0;
            var x = thisEnd - thisStart;
            var y = end - start;
            var len = Math.min(x, y);
            var thisCopy = this.slice(thisStart, thisEnd);
            var targetCopy = target.slice(start, end);

            for (var i = 0; i < len; ++i) {
              if (thisCopy[i] !== targetCopy[i]) {
                x = thisCopy[i];
                y = targetCopy[i];
                break;
              }
            }

            if (x < y) return -1;
            if (y < x) return 1;
            return 0;
          }; // Finds either the first index of `val` in `buffer` at offset >= `byteOffset`,
          // OR the last index of `val` in `buffer` at offset <= `byteOffset`.
          //
          // Arguments:
          // - buffer - a Buffer to search
          // - val - a string, Buffer, or number
          // - byteOffset - an index into `buffer`; will be clamped to an int32
          // - encoding - an optional encoding, relevant is val is a string
          // - dir - true for indexOf, false for lastIndexOf


          function bidirectionalIndexOf(buffer, val, byteOffset, encoding, dir) {
            // Empty buffer means no match
            if (buffer.length === 0) return -1; // Normalize byteOffset

            if (typeof byteOffset === 'string') {
              encoding = byteOffset;
              byteOffset = 0;
            } else if (byteOffset > 0x7fffffff) {
              byteOffset = 0x7fffffff;
            } else if (byteOffset < -0x80000000) {
              byteOffset = -0x80000000;
            }

            byteOffset = +byteOffset; // Coerce to Number.

            if (isNaN(byteOffset)) {
              // byteOffset: it it's undefined, null, NaN, "foo", etc, search whole buffer
              byteOffset = dir ? 0 : buffer.length - 1;
            } // Normalize byteOffset: negative offsets start from the end of the buffer


            if (byteOffset < 0) byteOffset = buffer.length + byteOffset;

            if (byteOffset >= buffer.length) {
              if (dir) return -1;else byteOffset = buffer.length - 1;
            } else if (byteOffset < 0) {
              if (dir) byteOffset = 0;else return -1;
            } // Normalize val


            if (typeof val === 'string') {
              val = Buffer.from(val, encoding);
            } // Finally, search either indexOf (if dir is true) or lastIndexOf


            if (Buffer.isBuffer(val)) {
              // Special case: looking for empty string/buffer always fails
              if (val.length === 0) {
                return -1;
              }

              return arrayIndexOf(buffer, val, byteOffset, encoding, dir);
            } else if (typeof val === 'number') {
              val = val & 0xFF; // Search for a byte value [0-255]

              if (Buffer.TYPED_ARRAY_SUPPORT && typeof Uint8Array.prototype.indexOf === 'function') {
                if (dir) {
                  return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset);
                } else {
                  return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset);
                }
              }

              return arrayIndexOf(buffer, [val], byteOffset, encoding, dir);
            }

            throw new TypeError('val must be string, number or Buffer');
          }

          function arrayIndexOf(arr, val, byteOffset, encoding, dir) {
            var indexSize = 1;
            var arrLength = arr.length;
            var valLength = val.length;

            if (encoding !== undefined) {
              encoding = String(encoding).toLowerCase();

              if (encoding === 'ucs2' || encoding === 'ucs-2' || encoding === 'utf16le' || encoding === 'utf-16le') {
                if (arr.length < 2 || val.length < 2) {
                  return -1;
                }

                indexSize = 2;
                arrLength /= 2;
                valLength /= 2;
                byteOffset /= 2;
              }
            }

            function read(buf, i) {
              if (indexSize === 1) {
                return buf[i];
              } else {
                return buf.readUInt16BE(i * indexSize);
              }
            }

            var i;

            if (dir) {
              var foundIndex = -1;

              for (i = byteOffset; i < arrLength; i++) {
                if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
                  if (foundIndex === -1) foundIndex = i;
                  if (i - foundIndex + 1 === valLength) return foundIndex * indexSize;
                } else {
                  if (foundIndex !== -1) i -= i - foundIndex;
                  foundIndex = -1;
                }
              }
            } else {
              if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength;

              for (i = byteOffset; i >= 0; i--) {
                var found = true;

                for (var j = 0; j < valLength; j++) {
                  if (read(arr, i + j) !== read(val, j)) {
                    found = false;
                    break;
                  }
                }

                if (found) return i;
              }
            }

            return -1;
          }

          Buffer.prototype.includes = function includes(val, byteOffset, encoding) {
            return this.indexOf(val, byteOffset, encoding) !== -1;
          };

          Buffer.prototype.indexOf = function indexOf(val, byteOffset, encoding) {
            return bidirectionalIndexOf(this, val, byteOffset, encoding, true);
          };

          Buffer.prototype.lastIndexOf = function lastIndexOf(val, byteOffset, encoding) {
            return bidirectionalIndexOf(this, val, byteOffset, encoding, false);
          };

          function hexWrite(buf, string, offset, length) {
            offset = Number(offset) || 0;
            var remaining = buf.length - offset;

            if (!length) {
              length = remaining;
            } else {
              length = Number(length);

              if (length > remaining) {
                length = remaining;
              }
            } // must be an even number of digits


            var strLen = string.length;
            if (strLen % 2 !== 0) throw new TypeError('Invalid hex string');

            if (length > strLen / 2) {
              length = strLen / 2;
            }

            for (var i = 0; i < length; ++i) {
              var parsed = parseInt(string.substr(i * 2, 2), 16);
              if (isNaN(parsed)) return i;
              buf[offset + i] = parsed;
            }

            return i;
          }

          function utf8Write(buf, string, offset, length) {
            return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length);
          }

          function asciiWrite(buf, string, offset, length) {
            return blitBuffer(asciiToBytes(string), buf, offset, length);
          }

          function latin1Write(buf, string, offset, length) {
            return asciiWrite(buf, string, offset, length);
          }

          function base64Write(buf, string, offset, length) {
            return blitBuffer(base64ToBytes(string), buf, offset, length);
          }

          function ucs2Write(buf, string, offset, length) {
            return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length);
          }

          Buffer.prototype.write = function write(string, offset, length, encoding) {
            // Buffer#write(string)
            if (offset === undefined) {
              encoding = 'utf8';
              length = this.length;
              offset = 0; // Buffer#write(string, encoding)
            } else if (length === undefined && typeof offset === 'string') {
              encoding = offset;
              length = this.length;
              offset = 0; // Buffer#write(string, offset[, length][, encoding])
            } else if (isFinite(offset)) {
              offset = offset | 0;

              if (isFinite(length)) {
                length = length | 0;
                if (encoding === undefined) encoding = 'utf8';
              } else {
                encoding = length;
                length = undefined;
              } // legacy write(string, encoding, offset, length) - remove in v0.13

            } else {
              throw new Error('Buffer.write(string, encoding, offset[, length]) is no longer supported');
            }

            var remaining = this.length - offset;
            if (length === undefined || length > remaining) length = remaining;

            if (string.length > 0 && (length < 0 || offset < 0) || offset > this.length) {
              throw new RangeError('Attempt to write outside buffer bounds');
            }

            if (!encoding) encoding = 'utf8';
            var loweredCase = false;

            for (;;) {
              switch (encoding) {
                case 'hex':
                  return hexWrite(this, string, offset, length);

                case 'utf8':
                case 'utf-8':
                  return utf8Write(this, string, offset, length);

                case 'ascii':
                  return asciiWrite(this, string, offset, length);

                case 'latin1':
                case 'binary':
                  return latin1Write(this, string, offset, length);

                case 'base64':
                  // Warning: maxLength not taken into account in base64Write
                  return base64Write(this, string, offset, length);

                case 'ucs2':
                case 'ucs-2':
                case 'utf16le':
                case 'utf-16le':
                  return ucs2Write(this, string, offset, length);

                default:
                  if (loweredCase) throw new TypeError('Unknown encoding: ' + encoding);
                  encoding = ('' + encoding).toLowerCase();
                  loweredCase = true;
              }
            }
          };

          Buffer.prototype.toJSON = function toJSON() {
            return {
              type: 'Buffer',
              data: Array.prototype.slice.call(this._arr || this, 0)
            };
          };

          function base64Slice(buf, start, end) {
            if (start === 0 && end === buf.length) {
              return base64.fromByteArray(buf);
            } else {
              return base64.fromByteArray(buf.slice(start, end));
            }
          }

          function utf8Slice(buf, start, end) {
            end = Math.min(buf.length, end);
            var res = [];
            var i = start;

            while (i < end) {
              var firstByte = buf[i];
              var codePoint = null;
              var bytesPerSequence = firstByte > 0xEF ? 4 : firstByte > 0xDF ? 3 : firstByte > 0xBF ? 2 : 1;

              if (i + bytesPerSequence <= end) {
                var secondByte, thirdByte, fourthByte, tempCodePoint;

                switch (bytesPerSequence) {
                  case 1:
                    if (firstByte < 0x80) {
                      codePoint = firstByte;
                    }

                    break;

                  case 2:
                    secondByte = buf[i + 1];

                    if ((secondByte & 0xC0) === 0x80) {
                      tempCodePoint = (firstByte & 0x1F) << 0x6 | secondByte & 0x3F;

                      if (tempCodePoint > 0x7F) {
                        codePoint = tempCodePoint;
                      }
                    }

                    break;

                  case 3:
                    secondByte = buf[i + 1];
                    thirdByte = buf[i + 2];

                    if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80) {
                      tempCodePoint = (firstByte & 0xF) << 0xC | (secondByte & 0x3F) << 0x6 | thirdByte & 0x3F;

                      if (tempCodePoint > 0x7FF && (tempCodePoint < 0xD800 || tempCodePoint > 0xDFFF)) {
                        codePoint = tempCodePoint;
                      }
                    }

                    break;

                  case 4:
                    secondByte = buf[i + 1];
                    thirdByte = buf[i + 2];
                    fourthByte = buf[i + 3];

                    if ((secondByte & 0xC0) === 0x80 && (thirdByte & 0xC0) === 0x80 && (fourthByte & 0xC0) === 0x80) {
                      tempCodePoint = (firstByte & 0xF) << 0x12 | (secondByte & 0x3F) << 0xC | (thirdByte & 0x3F) << 0x6 | fourthByte & 0x3F;

                      if (tempCodePoint > 0xFFFF && tempCodePoint < 0x110000) {
                        codePoint = tempCodePoint;
                      }
                    }

                }
              }

              if (codePoint === null) {
                // we did not generate a valid codePoint so insert a
                // replacement char (U+FFFD) and advance only 1 byte
                codePoint = 0xFFFD;
                bytesPerSequence = 1;
              } else if (codePoint > 0xFFFF) {
                // encode to utf16 (surrogate pair dance)
                codePoint -= 0x10000;
                res.push(codePoint >>> 10 & 0x3FF | 0xD800);
                codePoint = 0xDC00 | codePoint & 0x3FF;
              }

              res.push(codePoint);
              i += bytesPerSequence;
            }

            return decodeCodePointsArray(res);
          } // Based on http://stackoverflow.com/a/22747272/680742, the browser with
          // the lowest limit is Chrome, with 0x10000 args.
          // We go 1 magnitude less, for safety


          var MAX_ARGUMENTS_LENGTH = 0x1000;

          function decodeCodePointsArray(codePoints) {
            var len = codePoints.length;

            if (len <= MAX_ARGUMENTS_LENGTH) {
              return String.fromCharCode.apply(String, codePoints); // avoid extra slice()
            } // Decode in chunks to avoid "call stack size exceeded".


            var res = '';
            var i = 0;

            while (i < len) {
              res += String.fromCharCode.apply(String, codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH));
            }

            return res;
          }

          function asciiSlice(buf, start, end) {
            var ret = '';
            end = Math.min(buf.length, end);

            for (var i = start; i < end; ++i) {
              ret += String.fromCharCode(buf[i] & 0x7F);
            }

            return ret;
          }

          function latin1Slice(buf, start, end) {
            var ret = '';
            end = Math.min(buf.length, end);

            for (var i = start; i < end; ++i) {
              ret += String.fromCharCode(buf[i]);
            }

            return ret;
          }

          function hexSlice(buf, start, end) {
            var len = buf.length;
            if (!start || start < 0) start = 0;
            if (!end || end < 0 || end > len) end = len;
            var out = '';

            for (var i = start; i < end; ++i) {
              out += toHex(buf[i]);
            }

            return out;
          }

          function utf16leSlice(buf, start, end) {
            var bytes = buf.slice(start, end);
            var res = '';

            for (var i = 0; i < bytes.length; i += 2) {
              res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
            }

            return res;
          }

          Buffer.prototype.slice = function slice(start, end) {
            var len = this.length;
            start = ~~start;
            end = end === undefined ? len : ~~end;

            if (start < 0) {
              start += len;
              if (start < 0) start = 0;
            } else if (start > len) {
              start = len;
            }

            if (end < 0) {
              end += len;
              if (end < 0) end = 0;
            } else if (end > len) {
              end = len;
            }

            if (end < start) end = start;
            var newBuf;

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              newBuf = this.subarray(start, end);
              newBuf.__proto__ = Buffer.prototype;
            } else {
              var sliceLen = end - start;
              newBuf = new Buffer(sliceLen, undefined);

              for (var i = 0; i < sliceLen; ++i) {
                newBuf[i] = this[i + start];
              }
            }

            return newBuf;
          };
          /*
           * Need to make sure that buffer isn't trying to write out of bounds.
           */


          function checkOffset(offset, ext, length) {
            if (offset % 1 !== 0 || offset < 0) throw new RangeError('offset is not uint');
            if (offset + ext > length) throw new RangeError('Trying to access beyond buffer length');
          }

          Buffer.prototype.readUIntLE = function readUIntLE(offset, byteLength, noAssert) {
            offset = offset | 0;
            byteLength = byteLength | 0;
            if (!noAssert) checkOffset(offset, byteLength, this.length);
            var val = this[offset];
            var mul = 1;
            var i = 0;

            while (++i < byteLength && (mul *= 0x100)) {
              val += this[offset + i] * mul;
            }

            return val;
          };

          Buffer.prototype.readUIntBE = function readUIntBE(offset, byteLength, noAssert) {
            offset = offset | 0;
            byteLength = byteLength | 0;

            if (!noAssert) {
              checkOffset(offset, byteLength, this.length);
            }

            var val = this[offset + --byteLength];
            var mul = 1;

            while (byteLength > 0 && (mul *= 0x100)) {
              val += this[offset + --byteLength] * mul;
            }

            return val;
          };

          Buffer.prototype.readUInt8 = function readUInt8(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 1, this.length);
            return this[offset];
          };

          Buffer.prototype.readUInt16LE = function readUInt16LE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 2, this.length);
            return this[offset] | this[offset + 1] << 8;
          };

          Buffer.prototype.readUInt16BE = function readUInt16BE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 2, this.length);
            return this[offset] << 8 | this[offset + 1];
          };

          Buffer.prototype.readUInt32LE = function readUInt32LE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 4, this.length);
            return (this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16) + this[offset + 3] * 0x1000000;
          };

          Buffer.prototype.readUInt32BE = function readUInt32BE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 4, this.length);
            return this[offset] * 0x1000000 + (this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3]);
          };

          Buffer.prototype.readIntLE = function readIntLE(offset, byteLength, noAssert) {
            offset = offset | 0;
            byteLength = byteLength | 0;
            if (!noAssert) checkOffset(offset, byteLength, this.length);
            var val = this[offset];
            var mul = 1;
            var i = 0;

            while (++i < byteLength && (mul *= 0x100)) {
              val += this[offset + i] * mul;
            }

            mul *= 0x80;
            if (val >= mul) val -= Math.pow(2, 8 * byteLength);
            return val;
          };

          Buffer.prototype.readIntBE = function readIntBE(offset, byteLength, noAssert) {
            offset = offset | 0;
            byteLength = byteLength | 0;
            if (!noAssert) checkOffset(offset, byteLength, this.length);
            var i = byteLength;
            var mul = 1;
            var val = this[offset + --i];

            while (i > 0 && (mul *= 0x100)) {
              val += this[offset + --i] * mul;
            }

            mul *= 0x80;
            if (val >= mul) val -= Math.pow(2, 8 * byteLength);
            return val;
          };

          Buffer.prototype.readInt8 = function readInt8(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 1, this.length);
            if (!(this[offset] & 0x80)) return this[offset];
            return (0xff - this[offset] + 1) * -1;
          };

          Buffer.prototype.readInt16LE = function readInt16LE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 2, this.length);
            var val = this[offset] | this[offset + 1] << 8;
            return val & 0x8000 ? val | 0xFFFF0000 : val;
          };

          Buffer.prototype.readInt16BE = function readInt16BE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 2, this.length);
            var val = this[offset + 1] | this[offset] << 8;
            return val & 0x8000 ? val | 0xFFFF0000 : val;
          };

          Buffer.prototype.readInt32LE = function readInt32LE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 4, this.length);
            return this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16 | this[offset + 3] << 24;
          };

          Buffer.prototype.readInt32BE = function readInt32BE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 4, this.length);
            return this[offset] << 24 | this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3];
          };

          Buffer.prototype.readFloatLE = function readFloatLE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 4, this.length);
            return ieee754.read(this, offset, true, 23, 4);
          };

          Buffer.prototype.readFloatBE = function readFloatBE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 4, this.length);
            return ieee754.read(this, offset, false, 23, 4);
          };

          Buffer.prototype.readDoubleLE = function readDoubleLE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 8, this.length);
            return ieee754.read(this, offset, true, 52, 8);
          };

          Buffer.prototype.readDoubleBE = function readDoubleBE(offset, noAssert) {
            if (!noAssert) checkOffset(offset, 8, this.length);
            return ieee754.read(this, offset, false, 52, 8);
          };

          function checkInt(buf, value, offset, ext, max, min) {
            if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance');
            if (value > max || value < min) throw new RangeError('"value" argument is out of bounds');
            if (offset + ext > buf.length) throw new RangeError('Index out of range');
          }

          Buffer.prototype.writeUIntLE = function writeUIntLE(value, offset, byteLength, noAssert) {
            value = +value;
            offset = offset | 0;
            byteLength = byteLength | 0;

            if (!noAssert) {
              var maxBytes = Math.pow(2, 8 * byteLength) - 1;
              checkInt(this, value, offset, byteLength, maxBytes, 0);
            }

            var mul = 1;
            var i = 0;
            this[offset] = value & 0xFF;

            while (++i < byteLength && (mul *= 0x100)) {
              this[offset + i] = value / mul & 0xFF;
            }

            return offset + byteLength;
          };

          Buffer.prototype.writeUIntBE = function writeUIntBE(value, offset, byteLength, noAssert) {
            value = +value;
            offset = offset | 0;
            byteLength = byteLength | 0;

            if (!noAssert) {
              var maxBytes = Math.pow(2, 8 * byteLength) - 1;
              checkInt(this, value, offset, byteLength, maxBytes, 0);
            }

            var i = byteLength - 1;
            var mul = 1;
            this[offset + i] = value & 0xFF;

            while (--i >= 0 && (mul *= 0x100)) {
              this[offset + i] = value / mul & 0xFF;
            }

            return offset + byteLength;
          };

          Buffer.prototype.writeUInt8 = function writeUInt8(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 1, 0xff, 0);
            if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value);
            this[offset] = value & 0xff;
            return offset + 1;
          };

          function objectWriteUInt16(buf, value, offset, littleEndian) {
            if (value < 0) value = 0xffff + value + 1;

            for (var i = 0, j = Math.min(buf.length - offset, 2); i < j; ++i) {
              buf[offset + i] = (value & 0xff << 8 * (littleEndian ? i : 1 - i)) >>> (littleEndian ? i : 1 - i) * 8;
            }
          }

          Buffer.prototype.writeUInt16LE = function writeUInt16LE(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              this[offset] = value & 0xff;
              this[offset + 1] = value >>> 8;
            } else {
              objectWriteUInt16(this, value, offset, true);
            }

            return offset + 2;
          };

          Buffer.prototype.writeUInt16BE = function writeUInt16BE(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 2, 0xffff, 0);

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              this[offset] = value >>> 8;
              this[offset + 1] = value & 0xff;
            } else {
              objectWriteUInt16(this, value, offset, false);
            }

            return offset + 2;
          };

          function objectWriteUInt32(buf, value, offset, littleEndian) {
            if (value < 0) value = 0xffffffff + value + 1;

            for (var i = 0, j = Math.min(buf.length - offset, 4); i < j; ++i) {
              buf[offset + i] = value >>> (littleEndian ? i : 3 - i) * 8 & 0xff;
            }
          }

          Buffer.prototype.writeUInt32LE = function writeUInt32LE(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              this[offset + 3] = value >>> 24;
              this[offset + 2] = value >>> 16;
              this[offset + 1] = value >>> 8;
              this[offset] = value & 0xff;
            } else {
              objectWriteUInt32(this, value, offset, true);
            }

            return offset + 4;
          };

          Buffer.prototype.writeUInt32BE = function writeUInt32BE(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 4, 0xffffffff, 0);

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              this[offset] = value >>> 24;
              this[offset + 1] = value >>> 16;
              this[offset + 2] = value >>> 8;
              this[offset + 3] = value & 0xff;
            } else {
              objectWriteUInt32(this, value, offset, false);
            }

            return offset + 4;
          };

          Buffer.prototype.writeIntLE = function writeIntLE(value, offset, byteLength, noAssert) {
            value = +value;
            offset = offset | 0;

            if (!noAssert) {
              var limit = Math.pow(2, 8 * byteLength - 1);
              checkInt(this, value, offset, byteLength, limit - 1, -limit);
            }

            var i = 0;
            var mul = 1;
            var sub = 0;
            this[offset] = value & 0xFF;

            while (++i < byteLength && (mul *= 0x100)) {
              if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
                sub = 1;
              }

              this[offset + i] = (value / mul >> 0) - sub & 0xFF;
            }

            return offset + byteLength;
          };

          Buffer.prototype.writeIntBE = function writeIntBE(value, offset, byteLength, noAssert) {
            value = +value;
            offset = offset | 0;

            if (!noAssert) {
              var limit = Math.pow(2, 8 * byteLength - 1);
              checkInt(this, value, offset, byteLength, limit - 1, -limit);
            }

            var i = byteLength - 1;
            var mul = 1;
            var sub = 0;
            this[offset + i] = value & 0xFF;

            while (--i >= 0 && (mul *= 0x100)) {
              if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
                sub = 1;
              }

              this[offset + i] = (value / mul >> 0) - sub & 0xFF;
            }

            return offset + byteLength;
          };

          Buffer.prototype.writeInt8 = function writeInt8(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 1, 0x7f, -0x80);
            if (!Buffer.TYPED_ARRAY_SUPPORT) value = Math.floor(value);
            if (value < 0) value = 0xff + value + 1;
            this[offset] = value & 0xff;
            return offset + 1;
          };

          Buffer.prototype.writeInt16LE = function writeInt16LE(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              this[offset] = value & 0xff;
              this[offset + 1] = value >>> 8;
            } else {
              objectWriteUInt16(this, value, offset, true);
            }

            return offset + 2;
          };

          Buffer.prototype.writeInt16BE = function writeInt16BE(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 2, 0x7fff, -0x8000);

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              this[offset] = value >>> 8;
              this[offset + 1] = value & 0xff;
            } else {
              objectWriteUInt16(this, value, offset, false);
            }

            return offset + 2;
          };

          Buffer.prototype.writeInt32LE = function writeInt32LE(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              this[offset] = value & 0xff;
              this[offset + 1] = value >>> 8;
              this[offset + 2] = value >>> 16;
              this[offset + 3] = value >>> 24;
            } else {
              objectWriteUInt32(this, value, offset, true);
            }

            return offset + 4;
          };

          Buffer.prototype.writeInt32BE = function writeInt32BE(value, offset, noAssert) {
            value = +value;
            offset = offset | 0;
            if (!noAssert) checkInt(this, value, offset, 4, 0x7fffffff, -0x80000000);
            if (value < 0) value = 0xffffffff + value + 1;

            if (Buffer.TYPED_ARRAY_SUPPORT) {
              this[offset] = value >>> 24;
              this[offset + 1] = value >>> 16;
              this[offset + 2] = value >>> 8;
              this[offset + 3] = value & 0xff;
            } else {
              objectWriteUInt32(this, value, offset, false);
            }

            return offset + 4;
          };

          function checkIEEE754(buf, value, offset, ext, max, min) {
            if (offset + ext > buf.length) throw new RangeError('Index out of range');
            if (offset < 0) throw new RangeError('Index out of range');
          }

          function writeFloat(buf, value, offset, littleEndian, noAssert) {
            if (!noAssert) {
              checkIEEE754(buf, value, offset, 4, 3.4028234663852886e+38, -3.4028234663852886e+38);
            }

            ieee754.write(buf, value, offset, littleEndian, 23, 4);
            return offset + 4;
          }

          Buffer.prototype.writeFloatLE = function writeFloatLE(value, offset, noAssert) {
            return writeFloat(this, value, offset, true, noAssert);
          };

          Buffer.prototype.writeFloatBE = function writeFloatBE(value, offset, noAssert) {
            return writeFloat(this, value, offset, false, noAssert);
          };

          function writeDouble(buf, value, offset, littleEndian, noAssert) {
            if (!noAssert) {
              checkIEEE754(buf, value, offset, 8, 1.7976931348623157E+308, -1.7976931348623157E+308);
            }

            ieee754.write(buf, value, offset, littleEndian, 52, 8);
            return offset + 8;
          }

          Buffer.prototype.writeDoubleLE = function writeDoubleLE(value, offset, noAssert) {
            return writeDouble(this, value, offset, true, noAssert);
          };

          Buffer.prototype.writeDoubleBE = function writeDoubleBE(value, offset, noAssert) {
            return writeDouble(this, value, offset, false, noAssert);
          }; // copy(targetBuffer, targetStart=0, sourceStart=0, sourceEnd=buffer.length)


          Buffer.prototype.copy = function copy(target, targetStart, start, end) {
            if (!start) start = 0;
            if (!end && end !== 0) end = this.length;
            if (targetStart >= target.length) targetStart = target.length;
            if (!targetStart) targetStart = 0;
            if (end > 0 && end < start) end = start; // Copy 0 bytes; we're done

            if (end === start) return 0;
            if (target.length === 0 || this.length === 0) return 0; // Fatal error conditions

            if (targetStart < 0) {
              throw new RangeError('targetStart out of bounds');
            }

            if (start < 0 || start >= this.length) throw new RangeError('sourceStart out of bounds');
            if (end < 0) throw new RangeError('sourceEnd out of bounds'); // Are we oob?

            if (end > this.length) end = this.length;

            if (target.length - targetStart < end - start) {
              end = target.length - targetStart + start;
            }

            var len = end - start;
            var i;

            if (this === target && start < targetStart && targetStart < end) {
              // descending copy from end
              for (i = len - 1; i >= 0; --i) {
                target[i + targetStart] = this[i + start];
              }
            } else if (len < 1000 || !Buffer.TYPED_ARRAY_SUPPORT) {
              // ascending copy from start
              for (i = 0; i < len; ++i) {
                target[i + targetStart] = this[i + start];
              }
            } else {
              Uint8Array.prototype.set.call(target, this.subarray(start, start + len), targetStart);
            }

            return len;
          }; // Usage:
          //    buffer.fill(number[, offset[, end]])
          //    buffer.fill(buffer[, offset[, end]])
          //    buffer.fill(string[, offset[, end]][, encoding])


          Buffer.prototype.fill = function fill(val, start, end, encoding) {
            // Handle string cases:
            if (typeof val === 'string') {
              if (typeof start === 'string') {
                encoding = start;
                start = 0;
                end = this.length;
              } else if (typeof end === 'string') {
                encoding = end;
                end = this.length;
              }

              if (val.length === 1) {
                var code = val.charCodeAt(0);

                if (code < 256) {
                  val = code;
                }
              }

              if (encoding !== undefined && typeof encoding !== 'string') {
                throw new TypeError('encoding must be a string');
              }

              if (typeof encoding === 'string' && !Buffer.isEncoding(encoding)) {
                throw new TypeError('Unknown encoding: ' + encoding);
              }
            } else if (typeof val === 'number') {
              val = val & 255;
            } // Invalid ranges are not set to a default, so can range check early.


            if (start < 0 || this.length < start || this.length < end) {
              throw new RangeError('Out of range index');
            }

            if (end <= start) {
              return this;
            }

            start = start >>> 0;
            end = end === undefined ? this.length : end >>> 0;
            if (!val) val = 0;
            var i;

            if (typeof val === 'number') {
              for (i = start; i < end; ++i) {
                this[i] = val;
              }
            } else {
              var bytes = Buffer.isBuffer(val) ? val : utf8ToBytes(new Buffer(val, encoding).toString());
              var len = bytes.length;

              for (i = 0; i < end - start; ++i) {
                this[i + start] = bytes[i % len];
              }
            }

            return this;
          }; // HELPER FUNCTIONS
          // ================


          var INVALID_BASE64_RE = /[^+\/0-9A-Za-z-_]/g;

          function base64clean(str) {
            // Node strips out invalid characters like \n and \t from the string, base64-js does not
            str = stringtrim(str).replace(INVALID_BASE64_RE, ''); // Node converts strings with length < 2 to ''

            if (str.length < 2) return ''; // Node allows for non-padded base64 strings (missing trailing ===), base64-js does not

            while (str.length % 4 !== 0) {
              str = str + '=';
            }

            return str;
          }

          function stringtrim(str) {
            if (str.trim) return str.trim();
            return str.replace(/^\s+|\s+$/g, '');
          }

          function toHex(n) {
            if (n < 16) return '0' + n.toString(16);
            return n.toString(16);
          }

          function utf8ToBytes(string, units) {
            units = units || Infinity;
            var codePoint;
            var length = string.length;
            var leadSurrogate = null;
            var bytes = [];

            for (var i = 0; i < length; ++i) {
              codePoint = string.charCodeAt(i); // is surrogate component

              if (codePoint > 0xD7FF && codePoint < 0xE000) {
                // last char was a lead
                if (!leadSurrogate) {
                  // no lead yet
                  if (codePoint > 0xDBFF) {
                    // unexpected trail
                    if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
                    continue;
                  } else if (i + 1 === length) {
                    // unpaired lead
                    if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
                    continue;
                  } // valid lead


                  leadSurrogate = codePoint;
                  continue;
                } // 2 leads in a row


                if (codePoint < 0xDC00) {
                  if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
                  leadSurrogate = codePoint;
                  continue;
                } // valid surrogate pair


                codePoint = (leadSurrogate - 0xD800 << 10 | codePoint - 0xDC00) + 0x10000;
              } else if (leadSurrogate) {
                // valid bmp char, but last char was a lead
                if ((units -= 3) > -1) bytes.push(0xEF, 0xBF, 0xBD);
              }

              leadSurrogate = null; // encode utf8

              if (codePoint < 0x80) {
                if ((units -= 1) < 0) break;
                bytes.push(codePoint);
              } else if (codePoint < 0x800) {
                if ((units -= 2) < 0) break;
                bytes.push(codePoint >> 0x6 | 0xC0, codePoint & 0x3F | 0x80);
              } else if (codePoint < 0x10000) {
                if ((units -= 3) < 0) break;
                bytes.push(codePoint >> 0xC | 0xE0, codePoint >> 0x6 & 0x3F | 0x80, codePoint & 0x3F | 0x80);
              } else if (codePoint < 0x110000) {
                if ((units -= 4) < 0) break;
                bytes.push(codePoint >> 0x12 | 0xF0, codePoint >> 0xC & 0x3F | 0x80, codePoint >> 0x6 & 0x3F | 0x80, codePoint & 0x3F | 0x80);
              } else {
                throw new Error('Invalid code point');
              }
            }

            return bytes;
          }

          function asciiToBytes(str) {
            var byteArray = [];

            for (var i = 0; i < str.length; ++i) {
              // Node's code seems to be doing this and not & 0x7F..
              byteArray.push(str.charCodeAt(i) & 0xFF);
            }

            return byteArray;
          }

          function utf16leToBytes(str, units) {
            var c, hi, lo;
            var byteArray = [];

            for (var i = 0; i < str.length; ++i) {
              if ((units -= 2) < 0) break;
              c = str.charCodeAt(i);
              hi = c >> 8;
              lo = c % 256;
              byteArray.push(lo);
              byteArray.push(hi);
            }

            return byteArray;
          }

          function base64ToBytes(str) {
            return base64.toByteArray(base64clean(str));
          }

          function blitBuffer(src, dst, offset, length) {
            for (var i = 0; i < length; ++i) {
              if (i + offset >= dst.length || i >= src.length) break;
              dst[i + offset] = src[i];
            }

            return i;
          }

          function isnan(val) {
            return val !== val; // eslint-disable-line no-self-compare
          }
          /* WEBPACK VAR INJECTION */

        }).call(this, __webpack_require__(
        /*! ./../webpack/buildin/global.js */
        "./node_modules/webpack/buildin/global.js"));
        /***/
      },

      /***/
      "./node_modules/ieee754/index.js":
      /*!***************************************!*\
        !*** ./node_modules/ieee754/index.js ***!
        \***************************************/

      /*! no static exports found */

      /***/
      function node_modulesIeee754IndexJs(module, exports) {
        /*! ieee754. BSD-3-Clause License. Feross Aboukhadijeh <https://feross.org/opensource> */
        exports.read = function (buffer, offset, isLE, mLen, nBytes) {
          var e, m;
          var eLen = nBytes * 8 - mLen - 1;
          var eMax = (1 << eLen) - 1;
          var eBias = eMax >> 1;
          var nBits = -7;
          var i = isLE ? nBytes - 1 : 0;
          var d = isLE ? -1 : 1;
          var s = buffer[offset + i];
          i += d;
          e = s & (1 << -nBits) - 1;
          s >>= -nBits;
          nBits += eLen;

          for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

          m = e & (1 << -nBits) - 1;
          e >>= -nBits;
          nBits += mLen;

          for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

          if (e === 0) {
            e = 1 - eBias;
          } else if (e === eMax) {
            return m ? NaN : (s ? -1 : 1) * Infinity;
          } else {
            m = m + Math.pow(2, mLen);
            e = e - eBias;
          }

          return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
        };

        exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
          var e, m, c;
          var eLen = nBytes * 8 - mLen - 1;
          var eMax = (1 << eLen) - 1;
          var eBias = eMax >> 1;
          var rt = mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0;
          var i = isLE ? 0 : nBytes - 1;
          var d = isLE ? 1 : -1;
          var s = value < 0 || value === 0 && 1 / value < 0 ? 1 : 0;
          value = Math.abs(value);

          if (isNaN(value) || value === Infinity) {
            m = isNaN(value) ? 1 : 0;
            e = eMax;
          } else {
            e = Math.floor(Math.log(value) / Math.LN2);

            if (value * (c = Math.pow(2, -e)) < 1) {
              e--;
              c *= 2;
            }

            if (e + eBias >= 1) {
              value += rt / c;
            } else {
              value += rt * Math.pow(2, 1 - eBias);
            }

            if (value * c >= 2) {
              e++;
              c /= 2;
            }

            if (e + eBias >= eMax) {
              m = 0;
              e = eMax;
            } else if (e + eBias >= 1) {
              m = (value * c - 1) * Math.pow(2, mLen);
              e = e + eBias;
            } else {
              m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
              e = 0;
            }
          }

          for (; mLen >= 8; buffer[offset + i] = m & 0xff, i += d, m /= 256, mLen -= 8) {}

          e = e << mLen | m;
          eLen += mLen;

          for (; eLen > 0; buffer[offset + i] = e & 0xff, i += d, e /= 256, eLen -= 8) {}

          buffer[offset + i - d] |= s * 128;
        };
        /***/

      },

      /***/
      "./node_modules/isarray/index.js":
      /*!***************************************!*\
        !*** ./node_modules/isarray/index.js ***!
        \***************************************/

      /*! no static exports found */

      /***/
      function node_modulesIsarrayIndexJs(module, exports) {
        var toString = {}.toString;

        module.exports = Array.isArray || function (arr) {
          return toString.call(arr) == '[object Array]';
        };
        /***/

      },

      /***/
      "./node_modules/webpack/buildin/global.js":
      /*!***********************************!*\
        !*** (webpack)/buildin/global.js ***!
        \***********************************/

      /*! no static exports found */

      /***/
      function node_modulesWebpackBuildinGlobalJs(module, exports) {
        var g; // This works in non-strict mode

        g = function () {
          return this;
        }();

        try {
          // This works if eval is allowed (see CSP)
          g = g || new Function("return this")();
        } catch (e) {
          // This works if the window reference is available
          if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
        } // g can still be undefined, but nothing to do about it...
        // We return undefined, instead of nothing here, so it's
        // easier to handle this case. if(!global) { ...}


        module.exports = g;
        /***/
      },

      /***/
      "./src/dataGovernanceManager.ts":
      /*!**************************************!*\
        !*** ./src/dataGovernanceManager.ts ***!
        \**************************************/

      /*! exports provided: DataGovernanceManager */

      /***/
      function srcDataGovernanceManagerTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "DataGovernanceManager", function () {
          return DataGovernanceManager;
        });
        /* harmony import */


        var _utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
        /*! ./utilities */
        "./src/utilities.ts");
        /* harmony import */


        var _urlPonyFill__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
        /*! ./urlPonyFill */
        "./src/urlPonyFill.ts");
        /* harmony import */


        var base64_url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
        /*! base64-url */
        "./node_modules/base64-url/index.js");
        /* harmony import */


        var base64_url__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(base64_url__WEBPACK_IMPORTED_MODULE_2__);

        function _classCallCheck(instance, Constructor) {
          if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
          }
        }

        function _defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        function _createClass(Constructor, protoProps, staticProps) {
          if (protoProps) _defineProperties(Constructor.prototype, protoProps);
          if (staticProps) _defineProperties(Constructor, staticProps);
          return Constructor;
        }

        function _defineProperty(obj, key, value) {
          if (key in obj) {
            Object.defineProperty(obj, key, {
              value: value,
              enumerable: true,
              configurable: true,
              writable: true
            });
          } else {
            obj[key] = value;
          }

          return obj;
        }
        /**
         * Checks a request for PII and blocks the request, masks the sensitive data, or redacts it
         */


        var DataGovernanceManager = /*#__PURE__*/function () {
          /**
           * @constructor
           * @param {boolean} enabled - if data governance is enabled
           * @param {DataMonitoringParams} dataMonitoringParams - the data governance configuration
           * @param {any} environment - the active environment
           * @param {boolean} inspectFormData - if we're allowing FormData and URLSearchParams to be processed
           */
          function DataGovernanceManager(enabled, dataMonitoringParams, environment, inspectFormData) {
            _classCallCheck(this, DataGovernanceManager);

            _defineProperty(this, "enabled", void 0);

            _defineProperty(this, "dataMonitoringParams", void 0);

            _defineProperty(this, "inspectFormData", void 0);

            _defineProperty(this, "environment", void 0);

            _defineProperty(this, "blockURLParamsRegex", void 0);

            _defineProperty(this, "activeMonitoringParams", void 0);

            _defineProperty(this, "dataMonitoringAllowedDomainsRegex", void 0);

            _defineProperty(this, "mode", void 0);

            _defineProperty(this, "maskCharacter", void 0);

            _defineProperty(this, "base64Pattern", /((?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?)/g);

            this.enabled = enabled;
            this.dataMonitoringParams = dataMonitoringParams;
            this.environment = environment;
            this.inspectFormData = inspectFormData;
            this.updateEnvironment(environment);
          }
          /**
           * @public Validates the data monitoring params by assigning fallback values if necessary
           * @param {DataMonitoringParams} activeMonitoringParams - the active data monitoring params
           */


          _createClass(DataGovernanceManager, [{
            key: "validateActivateMonitoring",
            value: function validateActivateMonitoring(activeMonitoringParams) {
              if (activeMonitoringParams) {
                activeMonitoringParams.mode = activeMonitoringParams.mode || "block";
                activeMonitoringParams.blockURLParamsRegex = activeMonitoringParams.blockURLParamsRegex || [];
                activeMonitoringParams.domainsAllowed = activeMonitoringParams.domainsAllowed || [];
                activeMonitoringParams.monitorBase64Data = activeMonitoringParams.monitorBase64Data || false;
                activeMonitoringParams.maskCharacter = activeMonitoringParams.maskCharacter || "~";
                activeMonitoringParams.processRequestBody = activeMonitoringParams.processRequestBody || false;
              }

              return activeMonitoringParams;
            }
            /**
             * @public Getter function that returns the active data monitoring params
             */

          }, {
            key: "getActiveMonitoringParams",
            value: function getActiveMonitoringParams() {
              var activeMonitoringParams;

              if (this.environment && this.environment.hasOwnProperty("dataMonitoring")) {
                activeMonitoringParams = this.validateActivateMonitoring(this.environment.dataMonitoring);
              } else {
                activeMonitoringParams = this.validateActivateMonitoring(this.dataMonitoringParams) || {};
              }

              return activeMonitoringParams;
            }
            /**
             * @public Setter function that updates the environment attached to this class, and updates the related data attached to this class
             * @param {any} env - the new environment
             */

          }, {
            key: "updateEnvironment",
            value: function updateEnvironment(env) {
              this.environment = env;
              this.activeMonitoringParams = this.getActiveMonitoringParams(); // overwrites what is in constructor from environment;

              var regexArray = this.getBlockURLParamsRegexArray(this.activeMonitoringParams);
              this.blockURLParamsRegex = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].arrayToRegex(regexArray);
              this.dataMonitoringAllowedDomainsRegex = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].arrayToRegex(this.activeMonitoringParams.domainsAllowed); // in case of empty environment

              this.mode = this.activeMonitoringParams.mode || "block";
              this.maskCharacter = this.activeMonitoringParams.maskCharacter || "~";
            }
            /**
             * @public Checks for backwards compatibility in the blockURLParamsRegex in the  activeMonitoringParams
            * @public Checks for backwards compatibility in the blockURLParamsRegex in the  activeMonitoringParams 
             * @public Checks for backwards compatibility in the blockURLParamsRegex in the  activeMonitoringParams
             * @param {DataMonitoringParams} activeMonitoringParams - the active data monitoring params to be checked
             */

          }, {
            key: "isDataPatternReported",
            value: function isDataPatternReported(activeMonitoringParams) {
              // checks if blockURLParamsRegex array contains current config with regex key + value
              return activeMonitoringParams.blockURLParamsRegex != null && activeMonitoringParams.blockURLParamsRegex.length > 0 && typeof activeMonitoringParams.blockURLParamsRegex[0].regex === "string";
            }
            /**
             * @public Checks if the request should be allowed according to the data governance rules set in the configuration
             * @param {EnsRequest} request - the request being checked
             * @param {string} hostName - the hostname of the request being checked
             * @param {any} requestBody - the body of the request
             */

          }, {
            key: "allowRequest",
            value: function allowRequest(request, hostName, requestBody) {
              // NOTE:: Hostname should param should be refactored.  This should be derived from the URL in the request
              // it is currently being passed in so that we don't parse the hostname multiple times
              var dataMonitoringCheckPasses;
              var url = request.destination;

              if (this.enabled) {
                dataMonitoringCheckPasses = true;

                var decodedURLString = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].decodeUriComponent(url);

                if (this.activeMonitoringParams.monitorBase64Data) {
                  dataMonitoringCheckPasses = !this.checkForBase64Data(url, request);
                }

                if (dataMonitoringCheckPasses) {
                  dataMonitoringCheckPasses = !this.blockURLParamsRegex.test(decodedURLString);
                }

                if (this.activeMonitoringParams.processRequestBody) {
                  if (requestBody) {
                    var body = requestBody;

                    if (body instanceof FormData) {
                      if (this.inspectFormData) {
                        body = this.formDataToString(body);
                      } else {
                        // Can't process FormData objects on IE
                        body = "";
                      }
                    } else if (this.inspectFormData && body instanceof URLSearchParams) {
                      body = body.toString();
                    }

                    if (this.activeMonitoringParams.monitorBase64Data) {
                      dataMonitoringCheckPasses = !this.checkForBase64Data(body, request);
                    }

                    if (dataMonitoringCheckPasses) {
                      dataMonitoringCheckPasses = !this.blockURLParamsRegex.test(body);
                    }

                    this.findMatchedDataPatterns(this.activeMonitoringParams.blockURLParamsRegex, request, body);
                  }
                }

                if (dataMonitoringCheckPasses === false && this.activeMonitoringParams.domainsAllowed.length > 0) {
                  dataMonitoringCheckPasses = this.dataMonitoringAllowedDomainsRegex.test(hostName);
                }

                if (!dataMonitoringCheckPasses) {
                  request.addReason("DATA_MONITORING");

                  if (this.isDataPatternReported(this.activeMonitoringParams)) {
                    this.findMatchedDataPatterns(this.activeMonitoringParams.blockURLParamsRegex, request, decodedURLString);
                  }
                }
              }

              return dataMonitoringCheckPasses;
            }
            /**
             * @public Getter function that returns a masked version of a url
             * @param {string} url - the url we're checking
             */

          }, {
            key: "getMaskedUrl",
            value: function getMaskedUrl(url) {
              if (url) {
                return this.editQueryString(url, _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].repeatString(this.maskCharacter, 10));
              }

              return url;
            }
            /**
             * @public Getter function that returns a redacted version of a url
             * @param {string} url - the url we're checking
             */

          }, {
            key: "getRedactedUrl",
            value: function getRedactedUrl(url) {
              if (url) {
                return this.editQueryString(url, "");
              }

              return url;
            }
            /**
             * @public Getter function that returns a masked version of a request body
             * @param {string} data - the request body we're checking
             */

          }, {
            key: "getMaskedData",
            value: function getMaskedData(data) {
              // cannot use type object
              if (data) {
                return this.editData(data, _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].repeatString(this.maskCharacter, 10));
              }

              return data;
            }
            /**
             * @public Getter function that returns a redacted version of a request body
             * @param {string} data - the request body we're checking
             */

          }, {
            key: "getRedactedData",
            value: function getRedactedData(data) {
              // cannot use type object
              if (data) {
                return this.editData(data, "");
              }

              return data;
            }
            /**
             * @public Checks if a request body should be masked or not
             * @param {EnsRequest} request - the request we're checking
             */

          }, {
            key: "shouldMaskData",
            value: function shouldMaskData(request) {
              var shouldMask = false;

              if (this.mode === "mask" && request.hasReason("DATA_MONITORING")) {
                shouldMask = true;
              }

              return shouldMask;
            }
            /**
             * @public Checks if a request body should be redacted or not
             * @param {EnsRequest} request - the request we're checking
             */

          }, {
            key: "shouldRedactData",
            value: function shouldRedactData(request) {
              var shouldRedact = false;

              if (this.mode === "redact" && request.hasReason("DATA_MONITORING")) {
                shouldRedact = true;
              }

              return shouldRedact;
            }
            /**
             * @public Checks if a request should be masked or not
             * @param {EnsRequest} request - the request we're checking
             */

          }, {
            key: "shouldMaskUrl",
            value: function shouldMaskUrl(request) {
              var shouldMask = false;

              if (this.mode === "mask" && !request.hasReason("Whitelist") && !request.hasReason("Blacklist") && request.hasReason("DATA_MONITORING")) {
                shouldMask = true;
              }

              return shouldMask;
            }
            /**
             * @public Checks if a request should be redacted or not
             * @param {EnsRequest} request - the request we're checking
             */

          }, {
            key: "shouldRedactUrl",
            value: function shouldRedactUrl(request) {
              var shouldRedact = false;

              if (this.mode === "redact" && !request.hasReason("Whitelist") && !request.hasReason("Blacklist") && request.hasReason("DATA_MONITORING")) {
                shouldRedact = true;
              }

              return shouldRedact;
            }
            /**
             * @private Edits the path in the request
             * @param {string} path - the path of the request being checked
             * @param {string} replacementString - the string to replace sensitive data
             * @param {boolean} isRedacted - whether or not sensitive data should be redacted
             */

          }, {
            key: "editPath",
            value: function editPath(path, replacementString, isRedacted) {
              if (path.indexOf(";") > -1) {
                path = this.editSemiColonDelimitedPath(path, replacementString, isRedacted);
              }

              var pathArray = path.split("/");

              for (var i = 0; i < pathArray.length; i++) {
                if (pathArray[i] !== "" && this.blockURLParamsRegex.test && this.blockURLParamsRegex.test(pathArray[i])) {
                  pathArray[i] = pathArray[i].replace(this.blockURLParamsRegex, replacementString);
                  pathArray[i] = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].encodeUriComponent(pathArray[i]);

                  if (this.mode === "redact" && pathArray[i] === "") {
                    // this prevents a request from becoming something like http://www.foo.com/// (if multiple values are redacted)
                    pathArray.splice(i, 1);
                    i--;
                  }
                } else if (this.activeMonitoringParams.monitorBase64Data && pathArray[i].length > 4 && this.base64Pattern.test(pathArray[i])) {
                  // decode and replace base64 data
                  try {
                    var decodedString = base64_url__WEBPACK_IMPORTED_MODULE_2___default.a.decode(pathArray[i]);

                    if (this.blockURLParamsRegex.test && this.blockURLParamsRegex.test(decodedString)) {
                      pathArray[i] = pathArray[i].replace(this.base64Pattern, replacementString);
                      pathArray[i] = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].encodeUriComponent(pathArray[i]);

                      if (this.mode === "redact" && pathArray[i] === "") {
                        // this prevents a request from becoming something like http://www.foo.com/// (if multiple values are redacted)
                        pathArray.splice(i, 1);
                        i--;
                      }
                    }
                  } catch (e) {// String not properly base64 encoded
                  }
                }
              }

              var newPath = pathArray.join("/");
              return newPath;
            }
            /**
             * @private Converts formData to a string
             * @param {any} formData - the formData we're converting
             */

          }, {
            key: "formDataToString",
            value: function formDataToString(formData) {
              var parameters = [];
              var es, e, pair;

              for (es = formData.entries(); !(e = es.next()).done && (pair = e.value);) {
                parameters.push(pair[0] + "=" + pair[1]);
              }

              return parameters.join("&");
            }
            /**
             * @private Processes a Base 64 encoded String
             * @param {string} queryString - the query string of a url
             * @param {string} replacementStringChars - the characters we'll replace any sensitive data with
             */

          }, {
            key: "editBase64String",
            value: function editBase64String(queryString, replacementStringChars) {
              var newStringVal = queryString;
              var found = queryString.match(this.base64Pattern);

              for (var i in found) {
                if (found[i].length < 4) continue;
                var decodedString = base64_url__WEBPACK_IMPORTED_MODULE_2___default.a.decode(found[i]);

                if (this.blockURLParamsRegex.test(decodedString)) {
                  newStringVal = queryString.replace(found[i], replacementStringChars);
                }
              }

              return newStringVal;
            }
            /**
             * @private Wrapper function that processes data in a request
             * @param {FormData | URLSearchParams | string} data - the data to be processed
             * @param {string} replacementString - the string we'll replace any sensitive data with
             */

          }, {
            key: "editData",
            value: function editData(data, replacementString) {
              if (this.activeMonitoringParams.processRequestBody) {
                if (data instanceof String || typeof data === "string") {
                  if (this.activeMonitoringParams.monitorBase64Data) {
                    data = this.editBase64String(data, replacementString);
                  }

                  if (this.blockURLParamsRegex.test) {
                    while (this.blockURLParamsRegex.test(data)) {
                      // replace all matches with while loop
                      // can't use g flag in blockURLParamsRegex because it makes the RegExp test method unpredictable
                      data = data.replace(this.blockURLParamsRegex, replacementString);
                    }
                  }
                } else if (this.inspectFormData && data instanceof FormData) {
                  // Can't process FormData objects on IE
                  var newData = new FormData();
                  var es, e, pair;

                  for (es = data.entries(); !(e = es.next()).done && (pair = e.value);) {
                    var key = this.editData(pair[0], replacementString);
                    var value = this.editData(pair[1], replacementString);
                    newData.append(key, value);
                  }

                  data = newData;
                } else if (this.inspectFormData && data instanceof URLSearchParams) {
                  var _newData = new URLSearchParams();

                  var _es, _e, _pair;

                  for (_es = data.entries(); !(_e = _es.next()).done && (_pair = _e.value);) {
                    var _key = this.editData(_pair[0], replacementString);

                    var _value = this.editData(_pair[1], replacementString);

                    _newData.append(_key, _value);
                  }

                  data = _newData;
                }
              }

              return data;
            }
            /**
             * @private Checks a query string for data governance violations
             * @param {string} urlString - a string of the url being checked
             * @param {string} replacementString - the string we'll replace any sensitive data with
             */

          }, {
            key: "editQueryString",
            value: function editQueryString(urlString, replacementStringChars) {
              if (!this.enabled || !_utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].isValidUrl(urlString)) return urlString;
              var isRedacted = this.mode === "redact";
              var replacementString = replacementStringChars; // protects against JS '' resolving to falsy

              if (replacementStringChars === "") {
                replacementString = "";
              } // have to use this before calling URL constructor, because it delimits strictly based on & character,
              // which has the side effect of breaking up a url in a query string


              var queryString = urlString.split("?")[1];
              var url = new _urlPonyFill__WEBPACK_IMPORTED_MODULE_1__["URL"](urlString); // check url path for sensitive data

              url.pathname = this.editPath(url.pathname, replacementString, isRedacted); //validate query string and check it for sensitive data

              if (typeof queryString === "string" && queryString !== "") {
                if (this.activeMonitoringParams.monitorBase64Data) {
                  queryString = this.editBase64String(queryString, replacementString);
                }

                var searchParams = queryString.split("&");

                if (searchParams.length > 0) {
                  url.search = this.editSearchParams(searchParams, replacementString, isRedacted);

                  if (isRedacted && url.search === "") {
                    // if the entire query string is redacted, remove the question mark from the url
                    url.href = url.href.replace("?", "");
                  }
                }
              }

              return url.href;
            }
            /**
             * @private Getter function that returns an array that contains all the regexes we're checking against
             * @param {DataMonitoringParams} activeMonitoringParams - the active monitoring params
             */

          }, {
            key: "getBlockURLParamsRegexArray",
            value: function getBlockURLParamsRegexArray(activeMonitoringParams) {
              var regexArray = []; // backwards compatibility check

              if (!this.isDataPatternReported(activeMonitoringParams)) {
                return activeMonitoringParams.blockURLParamsRegex || [];
              } else {
                for (var i = 0; i < activeMonitoringParams.blockURLParamsRegex.length; i++) {
                  regexArray.push(activeMonitoringParams.blockURLParamsRegex[i].regex);
                }
              }

              return regexArray;
            }
            /**
             * @private Attaches data patterns to a request that's blocked for reporting purposes
             * @param {Array<any>} blockURLParamsRegex - the array of regular expressions we're checking against
             * @param {EnsRequest} request - the blocked request
             * @param {string} decodedURLString - a decoded version of the url string
             */

          }, {
            key: "findMatchedDataPatterns",
            value: function findMatchedDataPatterns(blockURLParamsRegex, request, decodedURLString) {
              for (var i = 0; i < blockURLParamsRegex.length; i++) {
                var regex = new RegExp(blockURLParamsRegex[i].regex);

                if (regex.test && regex.test(decodedURLString)) {
                  var matchedDataPattern = blockURLParamsRegex[i].name;
                  request.dataPatterns.push(matchedDataPattern);
                }
              }
            }
            /**
             * @private Checks if a url contains base 64 encoded data
             * @param {string} url - the url we're checking
             * @param {EnsRequest} request - the request object that the url is associated with
             */

          }, {
            key: "checkForBase64Data",
            value: function checkForBase64Data(url, request) {
              var found = url.match(this.base64Pattern);

              for (var i in found) {
                if (found[i].length < 4) continue;
                var decodedString = base64_url__WEBPACK_IMPORTED_MODULE_2___default.a.decode(found[i]);

                if (this.blockURLParamsRegex.test(decodedString)) {
                  this.findMatchedDataPatterns(this.activeMonitoringParams.blockURLParamsRegex, request, decodedString);
                  request.dataPatterns.push("base64");
                  return true;
                }
              }

              return false;
            }
            /**
             * @private edits the path of a request that's delimited with semi colons
             * @param {string} path - the url path to be processed
             * @param {string} replacementString - the string we'll replace any matches with
             * @param {boolean} isRedacted - whether or not we're in redaction mode
             */

          }, {
            key: "editSemiColonDelimitedPath",
            value: function editSemiColonDelimitedPath(path, replacementString, isRedacted) {
              // split up each path entry into an array, can have non semi-colon delimited key value pairs
              var pathEntriesArray = path.split("/"); // remove empty values from pathEntriesArray

              pathEntriesArray = pathEntriesArray.filter(function (param) {
                return typeof param === "string" && param !== "";
              });
              var newPath = "";
              var that = this;

              _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].each(pathEntriesArray, function (pathEntry, index) {
                // within each path entry, split into key-value pairs
                var pathKeyValueArray = pathEntry.split(";");

                _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].each(pathKeyValueArray, function (param, index) {
                  var paramArray = param.split("=");
                  var key = paramArray[0];
                  var unDecodedUriComponent = paramArray[1] || "";
                  var isBlockedParams = undefined; // evaluates to true if param should be blocked, will use this to remove query params for redacted Urls;

                  var value = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].decodeUriComponent(unDecodedUriComponent);

                  var newKeyValue = ""; // check key of path param

                  if (typeof key === "string" && key !== "" && that.blockURLParamsRegex.test) {
                    while (that.blockURLParamsRegex.test(key)) {
                      isBlockedParams = isRedacted ? true : false; // replace all matches with while loop
                      // can't use g flag in blockURLParamsRegex because it makes the RegExp test method unpredictable

                      key = key.replace(that.blockURLParamsRegex, replacementString);
                    }
                  } // check value of path param


                  if (typeof value === "string" && value !== "" && that.blockURLParamsRegex.test) {
                    while (that.blockURLParamsRegex.test(value)) {
                      // replace all matches with while loop
                      // can't use g flag in blockURLParamsRegex because it makes the RegExp test method unpredictable
                      isBlockedParams = isRedacted ? true : false;
                      value = value.replace(that.blockURLParamsRegex, replacementString);
                    }
                  }

                  if (typeof key === "string" && typeof value === "string") {
                    // add results back to newPath after type validation, for actual path parameters
                    value = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].encodeUriComponent(value);

                    if (isBlockedParams && isRedacted) {
                      // Single path values without a key value pair get blocked as well
                      newKeyValue += "";
                    } else if (value === "" && !isBlockedParams) {
                      newKeyValue += key;
                    } else {
                      newKeyValue += key + "=" + value;
                    }
                  }

                  if (newKeyValue === "") {
                    pathKeyValueArray.splice(index, 1); //Remove associated value from array if not valid
                  } else {
                    pathKeyValueArray[index] = newKeyValue;
                  }
                });

                pathEntriesArray[index] = pathKeyValueArray.join(";");
              });

              newPath += pathEntriesArray.join("/");
              return newPath;
            }
            /**
             * @private Edits the query string of a url
             * @param {Array<string>} searchParams - an array containing all the search params
             * @param {string} replacementString - the string we'll replace any matches with
             * @param {boolean} isRedacted - whether or not we're in redaction mode
             */

          }, {
            key: "editSearchParams",
            value: function editSearchParams(searchParams, replacementString, isRedacted) {
              //filter for multiple params
              var newQueryString = "";
              searchParams = searchParams.filter(function (param) {
                return typeof param === "string" && param !== "";
              });
              var that = this;

              _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].each(searchParams, function (param) {
                var paramArray = param.split("=");
                var key = paramArray[0];
                var unDecodedUriComponent = paramArray[1] || "";
                var isBlockedParams; // evaluates to true if param should be blocked, will use this to remove query params for redacted Urls;

                var value = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].decodeUriComponent(unDecodedUriComponent); // check key of search param


                if (typeof key === "string" && key !== "" && that.blockURLParamsRegex.test) {
                  while (that.blockURLParamsRegex.test(key)) {
                    isBlockedParams = isRedacted ? true : false; // replace all matches with while loop
                    // can't use g flag in blockURLParamsRegex because it makes the RegExp test method unpredictable

                    key = key.replace(that.blockURLParamsRegex, replacementString);
                  }
                } // check value of search param


                if (typeof value === "string" && value !== "" && that.blockURLParamsRegex.test) {
                  while (that.blockURLParamsRegex.test(value)) {
                    // replace all matches with while loop
                    // can't use g flag in blockURLParamsRegex because it makes the RegExp test method unpredictable
                    isBlockedParams = isRedacted ? true : false;
                    value = value.replace(that.blockURLParamsRegex, replacementString);
                  }
                } // add results back to newQueryString after type validation


                if (typeof key === "string" && typeof value === "string") {
                  value = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].encodeUriComponent(value);

                  if (isBlockedParams && isRedacted) {
                    newQueryString += "";
                  } else if (value === "" && !isBlockedParams) {
                    newQueryString += key + "&";
                  } else {
                    newQueryString += key + "=" + value + "&";
                  }
                }
              }); // remove & at end of query string


              newQueryString = newQueryString.slice(0, -1);
              return newQueryString;
            }
          }]);

          return DataGovernanceManager;
        }();
        /***/

      },

      /***/
      "./src/ensRequest.ts":
      /*!***************************!*\
        !*** ./src/ensRequest.ts ***!
        \***************************/

      /*! exports provided: EnsRequest */

      /***/
      function srcEnsRequestTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "EnsRequest", function () {
          return EnsRequest;
        });

        function _classCallCheck(instance, Constructor) {
          if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
          }
        }

        function _defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        function _createClass(Constructor, protoProps, staticProps) {
          if (protoProps) _defineProperties(Constructor.prototype, protoProps);
          if (staticProps) _defineProperties(Constructor, staticProps);
          return Constructor;
        }

        function _defineProperty(obj, key, value) {
          if (key in obj) {
            Object.defineProperty(obj, key, {
              value: value,
              enumerable: true,
              configurable: true,
              writable: true
            });
          } else {
            obj[key] = value;
          }

          return obj;
        }

        var EnsRequest = /*#__PURE__*/function () {
          function EnsRequest(destination) {
            _classCallCheck(this, EnsRequest);

            this.destination = destination;

            _defineProperty(this, "type", void 0);

            _defineProperty(this, "start", +new Date());

            _defineProperty(this, "end", -1);

            _defineProperty(this, "source", null);

            _defineProperty(this, "status", null);

            _defineProperty(this, "reasons", []);

            _defineProperty(this, "dataPatterns", []);

            _defineProperty(this, "list", []);

            _defineProperty(this, "id", void 0);
          }
          /**
           * @public Setter function that attaches a block reason to the EnsRequest 
           * @param {string} reason - the reason the request was blocked
           */


          _createClass(EnsRequest, [{
            key: "addReason",
            value: function addReason(reason) {
              this.reasons.push(reason);
            }
            /**
             * @public Checks if the request contains a specified block reason
             * @param {string} reason - reason being checked
             */

          }, {
            key: "hasReason",
            value: function hasReason(reason) {
              if (this.reasons.length > 0) {
                return new RegExp(this.reasons.join("|")).test(reason);
              }

              return false;
            }
            /**
             * @public Getter function to return the number of blocked reasons attached to the EnsRequest
             */

          }, {
            key: "numReasons",
            value: function numReasons() {
              return this.reasons.length;
            }
          }]);

          return EnsRequest;
        }();
        /***/

      },

      /***/
      "./src/index.ts":
      /*!**********************!*\
        !*** ./src/index.ts ***!
        \**********************/

      /*! exports provided: UrlProcessor, utilities, EnsRequest, URLjsImpl, URL, hasNativeURL */

      /***/
      function srcIndexTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony import */


        var _urlProcessor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
        /*! ./urlProcessor */
        "./src/urlProcessor.ts");
        /* harmony reexport (safe) */


        __webpack_require__.d(__webpack_exports__, "UrlProcessor", function () {
          return _urlProcessor__WEBPACK_IMPORTED_MODULE_0__["UrlProcessor"];
        });
        /* harmony import */


        var _utilities__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
        /*! ./utilities */
        "./src/utilities.ts");
        /* harmony reexport (safe) */


        __webpack_require__.d(__webpack_exports__, "utilities", function () {
          return _utilities__WEBPACK_IMPORTED_MODULE_1__["utilities"];
        });
        /* harmony import */


        var _ensRequest__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
        /*! ./ensRequest */
        "./src/ensRequest.ts");
        /* harmony reexport (safe) */


        __webpack_require__.d(__webpack_exports__, "EnsRequest", function () {
          return _ensRequest__WEBPACK_IMPORTED_MODULE_2__["EnsRequest"];
        });
        /* harmony import */


        var _urlPonyFill__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
        /*! ./urlPonyFill */
        "./src/urlPonyFill.ts");
        /* harmony reexport (safe) */


        __webpack_require__.d(__webpack_exports__, "URLjsImpl", function () {
          return _urlPonyFill__WEBPACK_IMPORTED_MODULE_3__["URLjsImpl"];
        });
        /* harmony reexport (safe) */


        __webpack_require__.d(__webpack_exports__, "URL", function () {
          return _urlPonyFill__WEBPACK_IMPORTED_MODULE_3__["URL"];
        });
        /* harmony reexport (safe) */


        __webpack_require__.d(__webpack_exports__, "hasNativeURL", function () {
          return _urlPonyFill__WEBPACK_IMPORTED_MODULE_3__["hasNativeURL"];
        });
        /***/

      },

      /***/
      "./src/listProcessor.ts":
      /*!******************************!*\
        !*** ./src/listProcessor.ts ***!
        \******************************/

      /*! exports provided: ListProcessor */

      /***/
      function srcListProcessorTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "ListProcessor", function () {
          return ListProcessor;
        });
        /* harmony import */


        var _utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
        /*! ./utilities */
        "./src/utilities.ts");
        /* harmony import */


        var _pathFilterManager__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
        /*! ./pathFilterManager */
        "./src/pathFilterManager.ts");

        function _classCallCheck(instance, Constructor) {
          if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
          }
        }

        function _defineProperty(obj, key, value) {
          if (key in obj) {
            Object.defineProperty(obj, key, {
              value: value,
              enumerable: true,
              configurable: true,
              writable: true
            });
          } else {
            obj[key] = value;
          }

          return obj;
        }
        /** 
         * List Processor - processes requests on the tags that are set in the active environment
         */


        var ListProcessor =
        /** 
         * @constructor
         * @param {any} environment - the active environment currently being used in the gateway
         * @param {any} gatewayData - a data object referring to the user's ensClientConfig
         * @param {object} activeEnvironmentCategories - cookie values set in the active environment passed in as an object
         * @param {Array<any>} allowedUrls - allowed urls passed in from tag access manager
         * @param {Array<any>} blockedUrls - blocked urls passed in from tag access manager
         */
        function ListProcessor(environment, gatewayData, activeEnvironmentCategories) {
          var _this = this;

          var allowedUrls = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
          var blockedUrls = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [];

          _classCallCheck(this, ListProcessor);

          this.environment = environment;
          this.gatewayData = gatewayData;
          this.activeEnvironmentCategories = activeEnvironmentCategories;
          this.allowedUrls = allowedUrls;
          this.blockedUrls = blockedUrls;

          _defineProperty(this, "activeList", void 0);

          _defineProperty(this, "listRegex", new RegExp("a^"));

          _defineProperty(this, "pathFilterManager", void 0);

          _defineProperty(this, "usesAllowlist", true);

          _defineProperty(this, "usesPathFilter", false);

          _defineProperty(this, "isInitialized", false);

          _defineProperty(this, "defaultCategory", "default");

          _defineProperty(this, "allowRequest", function (hostName, request, pathName) {
            var listRegEx = _this.getListRegex();

            var targetListCheckPasses = false; // blocklist

            if (_this.usesAllowlist === false) {
              if (listRegEx.test && !listRegEx.test(hostName)) {
                targetListCheckPasses = true;
              }
            } // allowlist
            else if (listRegEx.test && listRegEx.test(hostName)) {
                targetListCheckPasses = true;
              }

            if (_this.pathFilterManager != null) {
              targetListCheckPasses = _this.pathFilterManager.allowRequest(hostName, pathName, targetListCheckPasses);
            }

            if (!targetListCheckPasses) {
              request.addReason(_this.usesAllowlist ? "Whitelist" : "Blacklist");

              var matchedCategories = _this.findMatchedCategories(hostName);

              request.list = matchedCategories;
            }

            return targetListCheckPasses;
          });

          _defineProperty(this, "updateListValues", function (activeEnvironmentCategories, allowedUrls, blockedUrls) {
            _this.buildListRegex(_this.environment, activeEnvironmentCategories, allowedUrls, blockedUrls);

            if (_this.usesPathFilter && _this.pathFilterManager == null) {
              _this.pathFilterManager = new _pathFilterManager__WEBPACK_IMPORTED_MODULE_1__["PathFilterManager"](_this.getList(), _this.shouldAddToListRegex, _this.environment, _this.gatewayData, _this.activeEnvironmentCategories, _this.allowedUrls, _this.blockedUrls);
            }

            if (_this.pathFilterManager != null) {
              _this.pathFilterManager.update(activeEnvironmentCategories, allowedUrls, blockedUrls);
            }
          });

          _defineProperty(this, "updateEnvironment", function (newEnv, activeEnvironmentCategories, allowedUrls, blockedUrls) {
            _this.environment = newEnv;

            _this.updateListValues(activeEnvironmentCategories, allowedUrls, blockedUrls);
          });

          _defineProperty(this, "getListRegex", function () {
            return _this.listRegex;
          });

          _defineProperty(this, "getList", function () {
            return _this.activeList;
          });

          _defineProperty(this, "buildListRegex", function (environment, activeEnvironmentCategories, allowedUrls, blockedUrls) {
            _this.setUsesAllowlist(environment);

            _this.setActiveList(environment);

            var list = _this.getList();

            var reg = [];

            for (var category in list) {
              var cookieValue = activeEnvironmentCategories[category];

              if (_this.shouldAddToListRegex(category, cookieValue)) {
                reg = reg.concat(_this.getObjectVals(list[category], "tag"));
              }
            }

            if (_this.usesAllowlist) {
              if (allowedUrls && allowedUrls.length > 0) {
                reg = reg.concat(_this.getObjectVals(allowedUrls, "tag"));
              }

              if (blockedUrls && blockedUrls.length > 0) {
                reg = reg.filter(function (element) {
                  return _this.getObjectVals(blockedUrls, "tag").indexOf(element) === -1;
                });
              }
            } // block list
            else {
                if (blockedUrls && blockedUrls.length > 0) {
                  reg = reg.concat(_this.getObjectVals(blockedUrls, "tag"));
                }

                if (allowedUrls && allowedUrls.length > 0) {
                  reg = reg.filter(function (element) {
                    return _this.getObjectVals(allowedUrls, "tag").indexOf(element) === -1;
                  });
                }
              }

            var flags = _this.usesAllowlist ? "i" : undefined;
            _this.listRegex = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].arrayToRegex(reg, flags);
          });

          _defineProperty(this, "shouldAddToListRegex", function (category, cookieValue) {
            var shouldAddToListRegex = false;

            if (category === _this.defaultCategory) {
              //if our default category automatically includes these items
              shouldAddToListRegex = true;
              return shouldAddToListRegex;
            } // uses allowlist


            if (_this.usesAllowlist) {
              // cookie is set to 1 -- add to list
              if (cookieValue === "1") {
                shouldAddToListRegex = true;
              } // cookie is not 0, if cookie is 0 we assume the user has set it and don't look to the opt val, otherwise check the environment default opt val
              else if ((!cookieValue || cookieValue !== "0") && _this.isEnvironmentCategoryEnabled(category, 1)) {
                  shouldAddToListRegex = true;
                }
            } // uses blocklist
            else {
                if (cookieValue === "0") {
                  shouldAddToListRegex = true;
                } else if ((!cookieValue || cookieValue !== "1") && _this.isEnvironmentCategoryEnabled(category, 0)) {
                  shouldAddToListRegex = true;
                }
              }

            return shouldAddToListRegex;
          });

          _defineProperty(this, "isEnvironmentCategoryEnabled", function (category, val) {
            return _this.environment && _this.environment.optVals[category] === val;
          });

          _defineProperty(this, "getObjectVals", function (arrayOfObjects, key) {
            var arrayOfObjectVals = [];

            for (var i = 0; i < arrayOfObjects.length; i++) {
              var val = arrayOfObjects[i][key];

              if (val !== "" && val !== undefined && val != null) {
                arrayOfObjectVals.push(val);
              } //check for path filters


              if (arrayOfObjects["filter"]) {
                _this.usesPathFilter = true;
              }
            }

            return arrayOfObjectVals;
          });

          _defineProperty(this, "setUsesAllowlist", function (environment) {
            _this.usesAllowlist = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].isWhitelist(_this.gatewayData, environment);
          });

          _defineProperty(this, "setActiveList", function (environment) {
            if (_this.usesAllowlist) {
              if (environment && environment.hasOwnProperty("whitelist")) {
                _this.activeList = environment.whitelist;
              } else {
                _this.activeList = _this.gatewayData.whitelist || {};
              }
            } else {
              if (environment && environment.hasOwnProperty("blacklist")) {
                _this.activeList = environment.blacklist;
              } else {
                _this.activeList = _this.gatewayData.blacklist || {};
              }
            } // create an empty default category if it does not exist


            if (!_this.activeList.hasOwnProperty(_this.defaultCategory) && _this.usesAllowlist === true) {
              _this.activeList[_this.defaultCategory] = [];
            }

            if (_this.usesAllowlist && !_this.isInitialized) {
              var nexusDomain = _this.gatewayData.info.nexus || "nexus.ensighten.com";

              _this.activeList[_this.defaultCategory].push({
                tag: location.hostname
              });

              _this.activeList[_this.defaultCategory].push({
                tag: nexusDomain
              });
            }
          });

          _defineProperty(this, "findMatchedCategories", function (hostName) {
            var matchedCategories = [];

            var categoryList = _this.getList();

            for (var category in categoryList) {
              // check req against every tag in each category
              for (var i = 0; i < categoryList[category].length; i++) {
                var tag = categoryList[category][i].tag; // find categories where the blocked tag exists

                if (hostName.match(tag)) {
                  matchedCategories.push(category);
                }
              }
            }

            if (matchedCategories.length === 0) {
              // if req isn't blocked from opt-in/opt-out
              matchedCategories.push(_this.usesAllowlist ? "Whitelist" : "Blacklist");
            }

            return matchedCategories;
          });

          this.buildListRegex(environment, activeEnvironmentCategories, allowedUrls, blockedUrls); // this.usesPathFilter will get set after we go thru the list and check if any of the tags are using a path filter

          if (this.usesPathFilter) {
            this.pathFilterManager = new _pathFilterManager__WEBPACK_IMPORTED_MODULE_1__["PathFilterManager"](this.getList(), this.shouldAddToListRegex, this.environment, this.gatewayData, this.activeEnvironmentCategories, this.allowedUrls, this.blockedUrls);
          }

          this.isInitialized = true;
        }
        /* 
            PUBLIC METHODS
        */

        /**
         * @public processes a request and determines whether or not it should be allowed based on the allow or block list
         * @param {string} hostName - the name of the host in the url being requested
         * @param {EnsRequest} request - the request that will be processed
         * @param {string} pathName - the path of the url being requested
         * @returns {boolean} if the request is allowed or blocked
         */
        ;
        /***/

      },

      /***/
      "./src/pathFilterManager.ts":
      /*!**********************************!*\
        !*** ./src/pathFilterManager.ts ***!
        \**********************************/

      /*! exports provided: PathFilterManager */

      /***/
      function srcPathFilterManagerTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "PathFilterManager", function () {
          return PathFilterManager;
        });
        /* harmony import */


        var _utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
        /*! ./utilities */
        "./src/utilities.ts");

        function _classCallCheck(instance, Constructor) {
          if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
          }
        }

        function _defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        function _createClass(Constructor, protoProps, staticProps) {
          if (protoProps) _defineProperties(Constructor.prototype, protoProps);
          if (staticProps) _defineProperties(Constructor, staticProps);
          return Constructor;
        }

        function _defineProperty(obj, key, value) {
          if (key in obj) {
            Object.defineProperty(obj, key, {
              value: value,
              enumerable: true,
              configurable: true,
              writable: true
            });
          } else {
            obj[key] = value;
          }

          return obj;
        }
        /**
         * Create a path filter manager object to determine, when path filters are defined, if a request should be
         * allowed or rejected.
         */


        var PathFilterManager = /*#__PURE__*/function () {
          /**
           * @constructor
           * @param {string} activeList - The active list of tag categories
           * @param {Function} shouldAddCategoryToList - a function that, given a category name, indicates whether it should be included in the list of path filtering categories considered here
           * @param {any} environment - environment object reference
           * @param {any} gatewayData - gateway data, including feature toggles
           * @param {Array<any>} allowedTagsInLocalStorage - an array containing all the tags that are marked allowed in local storage
           * @param {Array<any>} blockedTagsInLocalStorage - an array containing all the tags that are marked blocked in local storage
           */
          function PathFilterManager(activeList, shouldAddCategoryToList, environment, gatewayData, activeEnvironmentCategories, allowedTagsInLocalStorage, blockedTagsInLocalStorage) {
            var _this = this;

            _classCallCheck(this, PathFilterManager);

            this.activeList = activeList;
            this.shouldAddCategoryToList = shouldAddCategoryToList;
            this.environment = environment;
            this.gatewayData = gatewayData;
            this.activeEnvironmentCategories = activeEnvironmentCategories;
            this.allowedTagsInLocalStorage = allowedTagsInLocalStorage;
            this.blockedTagsInLocalStorage = blockedTagsInLocalStorage;

            _defineProperty(this, "tagsWithFiltersRegEx", void 0);

            _defineProperty(this, "tagsWithAllowPaths", void 0);

            _defineProperty(this, "tagsWithBlockPaths", void 0);

            _defineProperty(this, "update", function (activeEnvironmentCategories, allowedTagsInLocalStorage, blockedTagsInLocalStorage) {
              _this.setGlobals(activeEnvironmentCategories, allowedTagsInLocalStorage, blockedTagsInLocalStorage);
            });

            _defineProperty(this, "allowRequest", function (domain, path, targetListCheckPasses) {
              var allowRequest = targetListCheckPasses;

              if (path !== "" && _this.tagsWithFiltersRegEx.test(domain)) {
                for (var tagsWithAllowPathsCount = 0; tagsWithAllowPathsCount < _this.tagsWithAllowPaths.length; tagsWithAllowPathsCount++) {
                  var allowPathRegEx = new RegExp(_this.tagsWithAllowPaths[tagsWithAllowPathsCount]["tag"]);

                  if (allowPathRegEx.test(domain)) {
                    var allowRegArray = [];
                    allowRegArray = allowRegArray.concat(_this.tagsWithAllowPaths[tagsWithAllowPathsCount]["paths"]);

                    var allowReg = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].arrayToRegex(allowRegArray);

                    if (allowReg.test(path)) {
                      allowRequest = true;
                      return allowRequest;
                    } else {
                      allowRequest = false;
                    }
                  }
                }

                for (var tagsWithBlockPathsCount = 0; tagsWithBlockPathsCount < _this.tagsWithBlockPaths.length; tagsWithBlockPathsCount++) {
                  var domainRegEx = new RegExp(_this.tagsWithBlockPaths[tagsWithBlockPathsCount]["tag"]);

                  if (domainRegEx.test(domain)) {
                    var blockRegArray = [];
                    blockRegArray = blockRegArray.concat(_this.tagsWithBlockPaths[tagsWithBlockPathsCount]["paths"]);

                    var blockReg = _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].arrayToRegex(blockRegArray);

                    if (blockReg.test(path)) {
                      allowRequest = false;
                      return allowRequest;
                    } else {
                      allowRequest = true;
                    }
                  }
                }
              }

              return allowRequest;
            });

            _defineProperty(this, "buildPathFilterList", function (activeList, activeEnvironmentCategories, allowedTagsInLocalStorage, blockedTagsInLocalStorage) {
              var tagsWithFilters = []; // A list of all tag objects with path filters defined

              var tagsWithAllowPaths = [];
              var tagsWithBlockPaths = [];

              var tagsOnlyFunction = function tagsOnlyFunction(item) {
                return item["tag"];
              };

              var tagsWithPathsFunction = function tagsWithPathsFunction(item) {
                return {
                  tag: item["tag"],
                  paths: item["paths"]
                };
              };

              var updateTagListsFunction = function updateTagListsFunction(sourceList) {
                tagsWithFilters.push.apply(tagsWithFilters, _this.extractTagsWithFilters(sourceList, tagsOnlyFunction));
                tagsWithBlockPaths.push.apply(tagsWithBlockPaths, _this.extractTagsWithFilters(sourceList, tagsWithPathsFunction, "blockPaths"));
                tagsWithAllowPaths.push.apply(tagsWithAllowPaths, _this.extractTagsWithFilters(sourceList, tagsWithPathsFunction, "allowPaths"));
              };

              for (var category in activeList) {
                var cookieValue = activeEnvironmentCategories[category];

                if (_this.shouldAddCategoryToList(category, cookieValue)) {
                  updateTagListsFunction(activeList[category]);
                }

                if (allowedTagsInLocalStorage && allowedTagsInLocalStorage.length > 0) {
                  updateTagListsFunction(allowedTagsInLocalStorage);

                  if (tagsWithFilters.length > 0) {
                    tagsWithFilters = _this.pruneTagListFromLocalStorage(allowedTagsInLocalStorage, tagsWithFilters, true);
                    tagsWithAllowPaths = _this.pruneTagListFromLocalStorage(allowedTagsInLocalStorage, tagsWithAllowPaths, true, "tag");
                  }
                }

                if (blockedTagsInLocalStorage && blockedTagsInLocalStorage.length > 0) {
                  updateTagListsFunction(blockedTagsInLocalStorage);

                  if (tagsWithFilters.length > 0) {
                    tagsWithFilters = _this.pruneTagListFromLocalStorage(blockedTagsInLocalStorage, tagsWithFilters, false);
                    tagsWithBlockPaths = _this.pruneTagListFromLocalStorage(blockedTagsInLocalStorage, tagsWithBlockPaths, false, "tag");
                  }
                }
              }

              return {
                regEx: _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].arrayToRegex(tagsWithFilters),
                allowPathTags: tagsWithAllowPaths,
                blockPathTags: tagsWithBlockPaths
              };
            });

            _defineProperty(this, "pruneTagListFromLocalStorage", function (localStorageList, listToPrune, whitelistCondition, comparisonField) {
              if (_utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].isWhitelist(_this.gatewayData, _this.environment) === whitelistCondition) {
                var localTagsWithNoFilters = _this.checkForFiltersToRemove(localStorageList);

                localTagsWithNoFilters.forEach(function (removeIfPresent) {
                  listToPrune = listToPrune.filter(function (item) {
                    if (comparisonField != null) {
                      item = item[comparisonField];
                    }

                    return item !== removeIfPresent["tag"];
                  });
                });
              }

              return listToPrune;
            });

            _defineProperty(this, "checkForFiltersToRemove", function (localStorageURLs) {
              return localStorageURLs.filter(function (item) {
                return item.filter === undefined;
              });
            });

            _defineProperty(this, "setGlobals", function (activeEnvironmentCategories, allowedTagsInLocalStorage, blockedTagsInLocalStorage) {
              _this.activeEnvironmentCategories = activeEnvironmentCategories; // Reset global allow/block paths lists

              var updatedGlobals = _this.buildPathFilterList(_this.activeList, _this.activeEnvironmentCategories, allowedTagsInLocalStorage, blockedTagsInLocalStorage);

              _this.tagsWithAllowPaths = updatedGlobals.allowPathTags;
              _this.tagsWithBlockPaths = updatedGlobals.blockPathTags;
              _this.tagsWithFiltersRegEx = updatedGlobals.regEx;
            });

            this.setGlobals(activeEnvironmentCategories, allowedTagsInLocalStorage, blockedTagsInLocalStorage);
          }
          /**
           * @public Update the internal regex which captures the allowed/blocked tags
           * @param {any} activeEnvironmentCategories - cookie values set for each category in the active environment passed in as an object
           * @param {Array<any>} allowedTagsInLocalStorage - an array containing all the tags that are marked allowed in local storage
           * @param {Array<any>} blockedTagsInLocalStorage - an array containing all the tags that are marked blocked in local storage
           */


          _createClass(PathFilterManager, [{
            key: "extractTagsWithFilters",

            /**
             * @private Given a source list of tag object, a function defining what to extract, and an optional filter type,
             * return a list of tags [with that filter type].
             * @param {array} tagList - The source tag list to evaluate.
             * @param {function} extractDataFunction - Given a tag object from the source list, return the desired tag structure.
             * @param {string} filterType - Optional - If undefined, will get all tags with defined filters; if set to "allowedPaths" or "blockedPaths", will only return tags with filters of that type.
             * @return {array} The list of matching tags extracted from the source tagList.
             */
            value: function extractTagsWithFilters(tagList, extractDataFunction, filterType) {
              var filterFunction = function filterFunction(item) {
                return item["filter"] !== undefined;
              };

              if (filterType != null) {
                filterFunction = function filterFunction(item) {
                  return item["filter"] === filterType;
                };
              }

              return tagList.filter(filterFunction).map(extractDataFunction);
            }
          }]);

          return PathFilterManager;
        }();
        /***/

      },

      /***/
      "./src/protoManager.ts":
      /*!*****************************!*\
        !*** ./src/protoManager.ts ***!
        \*****************************/

      /*! exports provided: ProtoManager */

      /***/
      function srcProtoManagerTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "ProtoManager", function () {
          return ProtoManager;
        });

        function _classCallCheck(instance, Constructor) {
          if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
          }
        }

        function _defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        function _createClass(Constructor, protoProps, staticProps) {
          if (protoProps) _defineProperties(Constructor.prototype, protoProps);
          if (staticProps) _defineProperties(Constructor, staticProps);
          return Constructor;
        }

        function _defineProperty(obj, key, value) {
          if (key in obj) {
            Object.defineProperty(obj, key, {
              value: value,
              enumerable: true,
              configurable: true,
              writable: true
            });
          } else {
            obj[key] = value;
          }

          return obj;
        }
        /**
         * Allows or blocks requests based on its protocol
         */


        var ProtoManager = /*#__PURE__*/function () {
          function ProtoManager() {
            _classCallCheck(this, ProtoManager);
          }

          _createClass(ProtoManager, null, [{
            key: "allowProtocolAndBypassProcessing",

            /**
             * @public Checks if a request should be allowed or blocked based on its protocol
             * @param {string} protoString - the protocol we're checking
             * @param {EnsRequest} request - the request we're checking
             * @param {any} featureToggles - the featureToggles set in the ensClientConfig
             */
            value: function allowProtocolAndBypassProcessing(protoString, request, featureToggles) {
              var allowed = this.protoRegex.test(protoString) && this.allowedProtocols[protoString] !== undefined && (!this.allowedProtocols[protoString].additionalAllowConditions || this.allowedProtocols[protoString].additionalAllowConditions(featureToggles, request));

              if (!allowed) {
                this.addBlockReasonToRequest(protoString, request);
              }

              return allowed;
            }
            /**
             * @public Checks if a blocked request should be bypassed based on its protocol
             * @param {string} protoString - the protocol we're checking
             */

          }, {
            key: "isBlockedRequestBypassed",
            value: function isBlockedRequestBypassed(protoString) {
              return this.requestBypassRegex.test(protoString);
            }
            /**
             * @private Adds a block reason to a request
             * @param {string} protoString - the protocol we're checking
             * @param {EnsRequest} request - the request we're checking
             */

          }, {
            key: "addBlockReasonToRequest",
            value: function addBlockReasonToRequest(protoString, request) {
              if (request && typeof request.addReason === "function" && this.allowedProtocols[protoString] !== undefined) {
                var blockReason = this.allowedProtocols[protoString].blockReason;
                request.addReason(blockReason);
              }
            }
          }]);

          return ProtoManager;
        }();

        _defineProperty(ProtoManager, "allowedProtocols", {
          "about:": {
            blockReason: "ABOUT_PROTOCOL"
          },
          "blob:": {
            blockReason: "BLOB_PROTOCOL"
          },
          "chrome-extension:": {
            additionalAllowConditions: function additionalAllowConditions(featureToggles) {
              return !featureToggles.blockExtensions;
            },
            blockReason: "CHROME_BROWSER_EXTENSION"
          },
          "data:": {
            additionalAllowConditions: function additionalAllowConditions(featureToggles, request) {
              return request.destination.includes("data:image") || // right now, it appears that disableDataProtocolJavaScriptRequests actually enables those requests when set to true
              request.type === "script" && featureToggles.disableDataProtocolJavascriptRequests;
            },
            blockReason: "DATA_PROTOCOL"
          },
          "hblob:": {
            blockReason: "HBLOB_PROTOCOL"
          },
          "moz-extension:": {
            additionalAllowConditions: function additionalAllowConditions(featureToggles) {
              return !featureToggles.blockExtensions;
            },
            blockReason: "MOZ_BROWSER_EXTENSION"
          },
          "safari-extension:": {
            additionalAllowConditions: function additionalAllowConditions(featureToggles) {
              return !featureToggles.blockExtensions;
            },
            blockReason: "SAFARI_BROWSER_EXTENSION"
          },
          "tel:": {
            blockReason: "TEL_PROTOCOL"
          }
        });

        _defineProperty(ProtoManager, "protoRegex", new RegExp(Object.keys(ProtoManager.allowedProtocols).join("|"), "i"));

        _defineProperty(ProtoManager, "requestBypassRegex", new RegExp("chrome-extension:|safari-extension:|moz-extension:", "i"));
        /***/

      },

      /***/
      "./src/sslManager.ts":
      /*!***************************!*\
        !*** ./src/sslManager.ts ***!
        \***************************/

      /*! exports provided: SSLManager */

      /***/
      function srcSslManagerTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "SSLManager", function () {
          return SSLManager;
        });
        /* harmony import */


        var _utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
        /*! ./utilities */
        "./src/utilities.ts");
        /* harmony import */


        var _urlPonyFill__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
        /*! ./urlPonyFill */
        "./src/urlPonyFill.ts");

        function _classCallCheck(instance, Constructor) {
          if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
          }
        }

        function _defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        function _createClass(Constructor, protoProps, staticProps) {
          if (protoProps) _defineProperties(Constructor.prototype, protoProps);
          if (staticProps) _defineProperties(Constructor, staticProps);
          return Constructor;
        }

        function _defineProperty(obj, key, value) {
          if (key in obj) {
            Object.defineProperty(obj, key, {
              value: value,
              enumerable: true,
              configurable: true,
              writable: true
            });
          } else {
            obj[key] = value;
          }

          return obj;
        }

        var sslEnforcement;
        /**
         * Processes requests based on their protocol
         */

        (function (sslEnforcement) {
          sslEnforcement["Block"] = "Block";
          sslEnforcement["Rewrite"] = "Rewrite";
        })(sslEnforcement || (sslEnforcement = {}));

        var SSLManager = /*#__PURE__*/function () {
          //would be good to make gatewayData an object instead of any 
          //and include a gatewayData interface

          /**
           * @constructor
           * @param {any} gatewayData - the active ensClientConfig in the gateway
           * @param {any} environment - the active environment in the gateway
           */
          function SSLManager(gatewayData, environment) {
            _classCallCheck(this, SSLManager);

            _defineProperty(this, "gatewayData", void 0);

            _defineProperty(this, "environment", void 0);

            _defineProperty(this, "sslEnforcementMode", void 0);

            _defineProperty(this, "insecureProtocolRegex", /http:|ws:/i);

            this.gatewayData = gatewayData;
            this.environment = environment; // fall back to block sslEnforcementMode for backwards compatibility

            this.sslEnforcementMode = this.fallbackOrReturnEnforcementMode();
          }
          /**
           * Setter function that updates the environment in the SSL Manager
           * @param {any} env - the new environment
           */


          _createClass(SSLManager, [{
            key: "updateEnvironment",
            value: function updateEnvironment(env) {
              this.environment = env;
              this.sslEnforcementMode = this.fallbackOrReturnEnforcementMode();
            }
            /**
             * @public Checks whether or not a request should be allowed based on its protocol
             * @param {string} locationProtocol - the protocol of where the request came from
             * @param {string} protocol - the protocol of the request we're checking
             * @param {EnsRequest} request - the request object we're checking against
             */

          }, {
            key: "allowRequest",
            value: function allowRequest(locationProtocol, protocol, request) {
              var SSLCheckPasses = true;

              if (_utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].getEnvironmentOverrideOrGlobalDefault(this.gatewayData.featureToggles, this.environment, "requireSSL")) {
                if (locationProtocol === "https:" && this.insecureProtocolRegex.test(protocol)) {
                  SSLCheckPasses = false;
                }

                if (!SSLCheckPasses) {
                  request.addReason("SSL");
                }
              }

              return SSLCheckPasses;
            }
            /**
             * @public Checks whether or not we should be rewriting the protocol based on SSL enforcement mode
             * @param {EnsRequest} request - the request we're checking
             * @param {string} referrer - the web location where the request originated
             */

          }, {
            key: "shouldRewriteProtocol",
            value: function shouldRewriteProtocol(request, referrer) {
              var protocol = new _urlPonyFill__WEBPACK_IMPORTED_MODULE_1__["URL"](request.destination, referrer).protocol;
              var shouldRewrite = false;

              if (this.insecureProtocolRegex.test(protocol)) {
                if (this.sslEnforcementMode === sslEnforcement.Rewrite && !request.hasReason("Whitelist") && !request.hasReason("Blacklist") && request.hasReason("SSL")) {
                  shouldRewrite = true;
                }
              }

              return shouldRewrite;
            }
            /**
             * @public Rewrites the protocol of the request
             * @param {string} urlString - the url we're checking as a string
             */

          }, {
            key: "rewriteProtocol",
            value: function rewriteProtocol(urlString) {
              urlString = urlString.replace(/^http:\/\//i, "https://");
              urlString = urlString.replace(/^ws:\/\//i, "wss://");
              return urlString;
            }
            /**
             * @private Sets the enforcement mode in the SSL Manager or falls back to block mode by default
             */

          }, {
            key: "fallbackOrReturnEnforcementMode",
            value: function fallbackOrReturnEnforcementMode() {
              if (!this.gatewayData.sslConfig && (!this.environment || !this.environment.sslEnforcementMode)) {
                return sslEnforcement.Block;
              } else {
                return _utilities__WEBPACK_IMPORTED_MODULE_0__["utilities"].getEnvironmentOverrideOrGlobalDefault(this.gatewayData.sslConfig, this.environment, "sslEnforcementMode");
              }
            }
          }]);

          return SSLManager;
        }();
        /***/

      },

      /***/
      "./src/urlPonyFill.ts":
      /*!****************************!*\
        !*** ./src/urlPonyFill.ts ***!
        \****************************/

      /*! exports provided: URLjsImpl, URL, hasNativeURL */

      /***/
      function srcUrlPonyFillTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "URLjsImpl", function () {
          return URLjsImpl;
        });
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "URL", function () {
          return URL;
        });
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "hasNativeURL", function () {
          return hasNativeURL;
        });
        /* Any copyright is dedicated to the Public Domain.
         * http://creativecommons.org/publicdomain/zero/1.0/ */


        function URLjsImpl() {
          var relative = Object.create(null);
          relative["ftp"] = 21;
          relative["file"] = 0;
          relative["gopher"] = 70;
          relative["http"] = 80;
          relative["https"] = 443;
          relative["ws"] = 80;
          relative["wss"] = 443;
          var relativePathDotMapping = Object.create(null);
          relativePathDotMapping["%2e"] = ".";
          relativePathDotMapping[".%2e"] = "..";
          relativePathDotMapping["%2e."] = "..";
          relativePathDotMapping["%2e%2e"] = "..";

          function isRelativeScheme(scheme) {
            return relative[scheme] !== undefined;
          }

          function invalid() {
            clear.call(this);
            this._isInvalid = true;
          }

          function IDNAToASCII(h) {
            if ("" === h) {
              invalid.call(this);
            } // XXX


            return h.toLowerCase();
          }

          function percentEscape(c) {
            var unicode = c.charCodeAt(0);

            if (unicode > 0x20 && unicode < 0x7F && // " # < > ? `
            [0x22, 0x23, 0x3C, 0x3E, 0x3F, 0x60].indexOf(unicode) === -1) {
              return c;
            }

            return encodeURIComponent(c);
          }

          function percentEscapeQuery(c) {
            // XXX This actually needs to encode c using encoding and then
            // convert the bytes one-by-one.
            var unicode = c.charCodeAt(0);

            if (unicode > 0x20 && unicode < 0x7F && // " # < > ` (do not escape '?')
            [0x22, 0x23, 0x3C, 0x3E, 0x60].indexOf(unicode) === -1) {
              return c;
            }

            return encodeURIComponent(c);
          }

          var EOF = undefined,
              ALPHA = /[a-zA-Z]/,
              ALPHANUMERIC = /[a-zA-Z0-9\+\-\.]/;
          /**
           * @param {!string} input
           * @param {?string=} stateOverride
           * @param {(URL|string)=} base
           */

          function parse(input, stateOverride, base) {
            function err(message) {
              errors.push(message);
            }

            var state = stateOverride || "scheme start",
                cursor = 0,
                buffer = "",
                seenAt = false,
                seenBracket = false,
                errors = [];

            loop: while ((input[cursor - 1] !== EOF || cursor === 0) && !this._isInvalid) {
              var c = input[cursor];

              switch (state) {
                case "scheme start":
                  if (c && ALPHA.test(c)) {
                    buffer += c.toLowerCase(); // ASCII-safe

                    state = "scheme";
                  } else if (!stateOverride) {
                    buffer = "";
                    state = "no scheme";
                    continue;
                  } else {
                    err("Invalid scheme.");
                    break loop;
                  }

                  break;

                case "scheme":
                  if (c && ALPHANUMERIC.test(c)) {
                    buffer += c.toLowerCase(); // ASCII-safe
                  } else if (":" === c) {
                    this._scheme = buffer;
                    buffer = "";

                    if (stateOverride) {
                      break loop;
                    }

                    if (isRelativeScheme(this._scheme)) {
                      this._isRelative = true;
                    }

                    if ("file" === this._scheme) {
                      state = "relative";
                    } else if (this._isRelative && base && base._scheme === this._scheme) {
                      state = "relative or authority";
                    } else if (this._isRelative) {
                      state = "authority first slash";
                    } else {
                      state = "scheme data";
                    }
                  } else if (!stateOverride) {
                    buffer = "";
                    cursor = 0;
                    state = "no scheme";
                    continue;
                  } else if (EOF === c) {
                    break loop;
                  } else {
                    err("Code point not allowed in scheme: " + c);
                    break loop;
                  }

                  break;

                case "scheme data":
                  if ("?" === c) {
                    this._query = "?";
                    state = "query";
                  } else if ("#" === c) {
                    this._fragment = "#";
                    state = "fragment";
                  } else {
                    // XXX error handling
                    if (EOF !== c && "\t" !== c && "\n" !== c && "\r" !== c) {
                      this._schemeData += percentEscape(c);
                    }
                  }

                  break;

                case "no scheme":
                  if (!base || !isRelativeScheme(base._scheme)) {
                    err("Missing scheme.");
                    invalid.call(this);
                  } else {
                    state = "relative";
                    continue;
                  }

                  break;

                case "relative or authority":
                  if ("/" === c && "/" === input[cursor + 1]) {
                    state = "authority ignore slashes";
                  } else {
                    err("Expected /, got: " + c);
                    state = "relative";
                    continue;
                  }

                  break;

                case "relative":
                  this._isRelative = true;
                  if ("file" !== this._scheme) this._scheme = base._scheme;

                  if (EOF === c) {
                    this._host = base._host;
                    this._port = base._port;
                    this._path = base._path.slice();
                    this._query = base._query;
                    this._username = base._username;
                    this._password = base._password;
                    break loop;
                  } else if ("/" === c || "\\" === c) {
                    if ("\\" === c) err("\\ is an invalid code point.");
                    state = "relative slash";
                  } else if ("?" === c) {
                    this._host = base._host;
                    this._port = base._port;
                    this._path = base._path.slice();
                    this._query = "?";
                    this._username = base._username;
                    this._password = base._password;
                    state = "query";
                  } else if ("#" === c) {
                    this._host = base._host;
                    this._port = base._port;
                    this._path = base._path.slice();
                    this._query = base._query;
                    this._fragment = "#";
                    this._username = base._username;
                    this._password = base._password;
                    state = "fragment";
                  } else {
                    var nextC = input[cursor + 1];
                    var nextNextC = input[cursor + 2];

                    if ("file" !== this._scheme || !ALPHA.test(c) || nextC !== ":" && nextC !== "|" || EOF !== nextNextC && "/" !== nextNextC && "\\" !== nextNextC && "?" !== nextNextC && "#" !== nextNextC) {
                      this._host = base._host;
                      this._port = base._port;
                      this._username = base._username;
                      this._password = base._password;
                      this._path = base._path.slice();

                      this._path.pop();
                    }

                    state = "relative path";
                    continue;
                  }

                  break;

                case "relative slash":
                  if ("/" === c || "\\" === c) {
                    if ("\\" === c) {
                      err("\\ is an invalid code point.");
                    }

                    if ("file" === this._scheme) {
                      state = "file host";
                    } else {
                      state = "authority ignore slashes";
                    }
                  } else {
                    if ("file" !== this._scheme) {
                      this._host = base._host;
                      this._port = base._port;
                      this._username = base._username;
                      this._password = base._password;
                    }

                    state = "relative path";
                    continue;
                  }

                  break;

                case "authority first slash":
                  if ("/" === c) {
                    state = "authority second slash";
                  } else {
                    err("Expected '/', got: " + c);
                    state = "authority ignore slashes";
                    continue;
                  }

                  break;

                case "authority second slash":
                  state = "authority ignore slashes";

                  if ("/" !== c) {
                    err("Expected '/', got: " + c);
                    continue;
                  }

                  break;

                case "authority ignore slashes":
                  if ("/" !== c && "\\" !== c) {
                    state = "authority";
                    continue;
                  } else {
                    err("Expected authority, got: " + c);
                  }

                  break;

                case "authority":
                  if ("@" === c) {
                    if (seenAt) {
                      err("@ already seen.");
                      buffer += "%40";
                    }

                    seenAt = true;

                    for (var i = 0; i < buffer.length; i++) {
                      var cp = buffer[i];

                      if ("\t" === cp || "\n" === cp || "\r" === cp) {
                        err("Invalid whitespace in authority.");
                        continue;
                      } // XXX check URL code points


                      if (":" === cp && null === this._password) {
                        this._password = "";
                        continue;
                      }

                      var tempC = percentEscape(cp);
                      null !== this._password ? this._password += tempC : this._username += tempC;
                    }

                    buffer = "";
                  } else if (EOF === c || "/" === c || "\\" === c || "?" === c || "#" === c) {
                    cursor -= buffer.length;
                    buffer = "";
                    state = "host";
                    continue;
                  } else {
                    buffer += c;
                  }

                  break;

                case "file host":
                  if (EOF === c || "/" === c || "\\" === c || "?" === c || "#" === c) {
                    if (buffer.length === 2 && ALPHA.test(buffer[0]) && (buffer[1] === ":" || buffer[1] === "|")) {
                      state = "relative path";
                    } else if (buffer.length === 0) {
                      state = "relative path start";
                    } else {
                      this._host = IDNAToASCII.call(this, buffer);
                      buffer = "";
                      state = "relative path start";
                    }

                    continue;
                  } else if ("\t" === c || "\n" === c || "\r" === c) {
                    err("Invalid whitespace in file host.");
                  } else {
                    buffer += c;
                  }

                  break;

                case "host":
                case "hostname":
                  if (":" === c && !seenBracket) {
                    // XXX host parsing
                    this._host = IDNAToASCII.call(this, buffer);
                    buffer = "";
                    state = "port";

                    if ("hostname" === stateOverride) {
                      break loop;
                    }
                  } else if (EOF === c || "/" === c || "\\" === c || "?" === c || "#" === c) {
                    this._host = IDNAToASCII.call(this, buffer);

                    if (this._host === "") {
                      throw Error("Unable to find host");
                    }

                    buffer = "";
                    state = "relative path start";

                    if (stateOverride) {
                      break loop;
                    }

                    continue;
                  } else if ("\t" !== c && "\n" !== c && "\r" !== c) {
                    if ("[" === c) {
                      seenBracket = true;
                    } else if ("]" === c) {
                      seenBracket = false;
                    }

                    buffer += c;
                  } else {
                    err("Invalid code point in host/hostname: " + c);
                  }

                  break;

                case "port":
                  if (/[0-9]/.test(c)) {
                    buffer += c;
                  } else if (EOF === c || "/" === c || "\\" === c || "?" === c || "#" === c || stateOverride) {
                    if ("" !== buffer) {
                      var temp = parseInt(buffer, 10);

                      if (temp !== relative[this._scheme]) {
                        this._port = temp + "";
                      }

                      buffer = "";
                    }

                    if (stateOverride) {
                      break loop;
                    }

                    state = "relative path start";
                    continue;
                  } else if ("\t" === c || "\n" === c || "\r" === c) {
                    err("Invalid code point in port: " + c);
                  } else {
                    invalid.call(this);
                  }

                  break;

                case "relative path start":
                  if ("\\" === c) err("'\\' not allowed in path.");
                  state = "relative path";

                  if ("/" !== c && "\\" !== c) {
                    continue;
                  }

                  break;

                case "relative path":
                  if (EOF === c || "/" === c || "\\" === c || !stateOverride && ("?" === c || "#" === c)) {
                    if ("\\" === c) {
                      err("\\ not allowed in relative path.");
                    }

                    var tmp;

                    if (tmp = relativePathDotMapping[buffer.toLowerCase()]) {
                      // eslint-disable-line  no-cond-assign
                      buffer = tmp;
                    }

                    if (".." === buffer) {
                      this._path.pop();

                      if ("/" !== c && "\\" !== c) {
                        this._path.push("");
                      }
                    } else if ("." === buffer && "/" !== c && "\\" !== c) {
                      this._path.push("");
                    } else if ("." !== buffer) {
                      if ("file" === this._scheme && this._path.length === 0 && buffer.length === 2 && ALPHA.test(buffer[0]) && buffer[1] === "|") {
                        buffer = buffer[0] + ":";
                      }

                      this._path.push(buffer);
                    }

                    buffer = "";

                    if ("?" === c) {
                      this._query = "?";
                      state = "query";
                    } else if ("#" === c) {
                      this._fragment = "#";
                      state = "fragment";
                    }
                  } else if ("\t" !== c && "\n" !== c && "\r" !== c) {
                    buffer += percentEscape(c);
                  }

                  break;

                case "query":
                  if (!stateOverride && "#" === c) {
                    this._fragment = "#";
                    state = "fragment";
                  } else if (EOF !== c && "\t" !== c && "\n" !== c && "\r" !== c) {
                    this._query += percentEscapeQuery(c);
                  }

                  break;

                case "fragment":
                  if (EOF !== c && "\t" !== c && "\n" !== c && "\r" !== c) {
                    this._fragment += c;
                  }

                  break;
              }

              cursor++;
            }
          }

          function clear() {
            this._scheme = "";
            this._schemeData = "";
            this._username = "";
            this._password = null;
            this._host = "";
            this._port = "";
            this._path = [];
            this._query = "";
            this._fragment = "";
            this._isInvalid = false;
            this._isRelative = false;
          } // Does not process domain names or IP addresses.
          // Does not handle encoding for the query parameter.

          /**
           * @constructor
           * @extends {URL}
           * @param {!string} url
           * @param {(URL|string|Location)=} base
           */


          function jURL(url, base)
          /* , encoding */
          {
            if (base !== undefined && !(base instanceof jURL)) base = new jURL(String(base));
            this._url = "" + url;
            clear.call(this);

            var input = this._url.replace(/^[ \t\r\n\f]+|[ \t\r\n\f]+$/g, ""); // encoding = encoding || 'utf-8'


            parse.call(this, input, null, base);
          }

          jURL.prototype = {
            toString: function toString() {
              return this.href;
            },

            get href() {
              if (this._isInvalid) return this._url;
              var authority = "";

              if ("" !== this._username || null !== this._password) {
                authority = this._username + (null !== this._password ? ":" + this._password : "") + "@";
              }

              return this.protocol + (this._isRelative ? "//" + authority + this.host : "") + this.pathname + this._query + this._fragment;
            },

            set href(href) {
              clear.call(this);
              parse.call(this, href);
            },

            get protocol() {
              return this._scheme + ":";
            },

            set protocol(protocol) {
              if (this._isInvalid) return;
              parse.call(this, protocol + ":", "scheme start");
            },

            get host() {
              return this._isInvalid ? "" : this._port ? this._host + ":" + this._port : this._host;
            },

            set host(host) {
              if (this._isInvalid || !this._isRelative) return;
              parse.call(this, host, "host");
            },

            get hostname() {
              return this._host;
            },

            set hostname(hostname) {
              if (this._isInvalid || !this._isRelative) return;
              parse.call(this, hostname, "hostname");
            },

            get port() {
              return this._port;
            },

            set port(port) {
              if (this._isInvalid || !this._isRelative) return;
              parse.call(this, port, "port");
            },

            get pathname() {
              return this._isInvalid ? "" : this._isRelative ? "/" + this._path.join("/") : this._schemeData;
            },

            set pathname(pathname) {
              if (this._isInvalid || !this._isRelative) return;
              this._path = [];
              parse.call(this, pathname, "relative path start");
            },

            get search() {
              return this._isInvalid || !this._query || "?" === this._query ? "" : this._query;
            },

            set search(search) {
              if (this._isInvalid || !this._isRelative) return;
              this._query = "?";
              if ("?" === search[0]) search = search.slice(1);
              parse.call(this, search, "query");
            },

            get hash() {
              return this._isInvalid || !this._fragment || "#" === this._fragment ? "" : this._fragment;
            },

            set hash(hash) {
              if (this._isInvalid) return;

              if (!hash) {
                this._fragment = "";
                return;
              }

              this._fragment = "#";
              if ("#" === hash[0]) hash = hash.slice(1);
              parse.call(this, hash, "fragment");
            },

            get origin() {
              var host;

              if (this._isInvalid || !this._scheme) {
                return "";
              } // javascript: Gecko returns String(""), WebKit/Blink String("null")
              // Gecko throws error for "data://"
              // data: Gecko returns "", Blink returns "data://", WebKit returns "null"
              // Gecko returns String("") for file: mailto:
              // WebKit/Blink returns String("SCHEME://") for file: mailto:


              switch (this._scheme) {
                case "data":
                case "file":
                case "javascript":
                case "mailto":
                  return "null";
              }

              host = this.host;

              if (!host) {
                return "";
              }

              return this._scheme + "://" + host;
            }

          }; // Copy over the static methods

          var OriginalURL = self.URL;

          if (OriginalURL) {
            jURL["createObjectURL"] = function (blob) {
              // IE extension allows a second optional options argument.
              // http://msdn.microsoft.com/en-us/library/ie/hh772302(v=vs.85).aspx
              return OriginalURL.createObjectURL.apply(OriginalURL, arguments);
            };

            jURL["revokeObjectURL"] = function (url) {
              OriginalURL.revokeObjectURL(url);
            };
          }

          return jURL;
        }

        var URL = URLjsImpl();
        /**
           * Helper to feature detect a working native URL implementation
           * @return {bool}
           */

        function hasNativeURL() {
          // eslint-disable-line  @typescript-eslint/explicit-module-boundary-types
          var hasWorkingUrl = false;

          try {
            var u = new self.URL("b", "http://a");
            u.pathname = "c%20d";
            hasWorkingUrl = u.href === "http://a/c%20d";
          } catch (e) {
            console.log(e);
          }

          return hasWorkingUrl;
        }
        /***/

      },

      /***/
      "./src/urlProcessor.ts":
      /*!*****************************!*\
        !*** ./src/urlProcessor.ts ***!
        \*****************************/

      /*! exports provided: UrlProcessor */

      /***/
      function srcUrlProcessorTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "UrlProcessor", function () {
          return UrlProcessor;
        });
        /* harmony import */


        var _ensRequest__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
        /*! ./ensRequest */
        "./src/ensRequest.ts");
        /* harmony import */


        var _utilities__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
        /*! ./utilities */
        "./src/utilities.ts");
        /* harmony import */


        var _sslManager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
        /*! ./sslManager */
        "./src/sslManager.ts");
        /* harmony import */


        var _dataGovernanceManager__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
        /*! ./dataGovernanceManager */
        "./src/dataGovernanceManager.ts");
        /* harmony import */


        var _protoManager__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
        /*! ./protoManager */
        "./src/protoManager.ts");
        /* harmony import */


        var _listProcessor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
        /*! ./listProcessor */
        "./src/listProcessor.ts");
        /* harmony import */


        var _urlPonyFill__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
        /*! ./urlPonyFill */
        "./src/urlPonyFill.ts");

        function _classCallCheck(instance, Constructor) {
          if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
          }
        }

        function _defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        function _createClass(Constructor, protoProps, staticProps) {
          if (protoProps) _defineProperties(Constructor.prototype, protoProps);
          if (staticProps) _defineProperties(Constructor, staticProps);
          return Constructor;
        }

        function _defineProperty(obj, key, value) {
          if (key in obj) {
            Object.defineProperty(obj, key, {
              value: value,
              enumerable: true,
              configurable: true,
              writable: true
            });
          } else {
            obj[key] = value;
          }

          return obj;
        }
        /**
         * Handles url processing in an agnostic environment
         */


        var UrlProcessor = /*#__PURE__*/function () {
          /**
           * @constructor
           * @param {any} gatewayData - the gatewayData set in the ensClientConfig
           * @param {any} environment - the active environment in the gateway 
           * @param {any} historyEntryReporter - reference to the history entry reporter
           * @param {any} activeEnvironmentCategories - cookie values set in the active environment passed in as an object
           * @param {Array<any>} allowedUrls - allowed urls passed in from tag access manager
           * @param {Array<any>} blockedUrls - blocked urls passed in from tag access manager
           * @param {boolean} inspectFormData - if we're allowing FormData and URLSearchParams to be processed
           */
          function UrlProcessor(gatewayData, environment, historyEntryReporter) {
            var _this = this;

            var activeEnvironmentCategories = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];
            var allowedUrls = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : [];
            var blockedUrls = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : [];
            var inspectFormData = arguments.length > 6 ? arguments[6] : undefined;

            _classCallCheck(this, UrlProcessor);

            this.gatewayData = gatewayData;
            this.environment = environment;
            this.historyEntryReporter = historyEntryReporter;
            this.activeEnvironmentCategories = activeEnvironmentCategories;
            this.allowedUrls = allowedUrls;
            this.blockedUrls = blockedUrls;
            this.inspectFormData = inspectFormData;

            _defineProperty(this, "sslManager", void 0);

            _defineProperty(this, "dataGovernanceManager", void 0);

            _defineProperty(this, "protoManager", void 0);

            _defineProperty(this, "listProcessor", void 0);

            _defineProperty(this, "updateEnvironment", function (newEnvironment, activeEnvironmentCategories, allowedUrls, blockedUrls) {
              _this.environment = newEnvironment;

              _this.dataGovernanceManager.updateEnvironment(newEnvironment);

              _this.sslManager.updateEnvironment(newEnvironment);

              _this.listProcessor.updateEnvironment(newEnvironment, activeEnvironmentCategories, allowedUrls, blockedUrls);
            });

            _defineProperty(this, "updateListValues", function (activeEnvironmentCategories, allowedUrls, blockedUrls) {
              _this.listProcessor.updateListValues(activeEnvironmentCategories, allowedUrls, blockedUrls);
            });

            this.sslManager = new _sslManager__WEBPACK_IMPORTED_MODULE_2__["SSLManager"](gatewayData, environment);
            this.dataGovernanceManager = new _dataGovernanceManager__WEBPACK_IMPORTED_MODULE_3__["DataGovernanceManager"](gatewayData.featureToggles.enableDataMonitoring, gatewayData.dataMonitoring, environment, inspectFormData);
            this.protoManager = _protoManager__WEBPACK_IMPORTED_MODULE_4__["ProtoManager"];
            this.listProcessor = new _listProcessor__WEBPACK_IMPORTED_MODULE_5__["ListProcessor"](environment, gatewayData, this.activeEnvironmentCategories, this.allowedUrls, this.blockedUrls);
          }
          /**
           * @public Setter function which updates the environment attached to this class
           * @param {any} newEnvironment - the new environment
           * @param {any} activeEnvironmentCategories - cookie values set in the active environment passed in as an object
           * @param {Array<any>} allowedUrls - allowed urls passed in from tag access manager
           * @param {Array<any>} blockedUrls - blocked urls passed in from tag access manager
           */


          _createClass(UrlProcessor, [{
            key: "processURL",

            /**
             * @public Processes urls - calls our internal processing function
             * @param {string} url - the url to be processed
             * @param {string} requestType - the type of request being processed
             * @param {string} source - where the request originated from
             * @param {string} referrer - the web location where the request originated
             * @param {Function} onUrlModifiedFn - a callback that is invoked if the url is modified
             */
            value: function processURL(url, requestType, source, referrer, onUrlModifiedFn) {
              var _this2 = this;

              var node = {};
              return this.processURLInternal(url, null, requestType, source, node, referrer, onUrlModifiedFn, function (requestId) {
                _this2.historyEntryReporter.resourceComplete(node, requestId, "allowed");
              });
            }
            /**
             * @public Processes urls and the post bodies attached to them - calls our internal processing function
             * @param {string} url - the url to be processed
             * @param {any} requestBody - the post body attached to the request
             * @param {string} requestType - the type of request being processed
             * @param {string} source - where the request originated from
             * @param {string} referrer - the web location where the request originated
             * @param {Function} onUrlModifiedFn - a callback that is invoked if the url is modified
             */

          }, {
            key: "processURLAndBody",
            value: function processURLAndBody(url, requestBody, requestType, source, referrer, onUrlModifiedFn) {
              var _this3 = this;

              var node = {};
              this.processURLInternal(url, requestBody, requestType, source, node, referrer, onUrlModifiedFn, function (requestId) {
                _this3.historyEntryReporter.resourceComplete(node, requestId, "allowed");
              });
            }
            /**
             * @public Checks if a url is void
             * @param {string | undefined } url - the url we're checking
             */

          }, {
            key: "isVoidURL",
            value: function isVoidURL(url) {
              var voidURL = "javascript:"; // covers both //javascript:; and javascript:void(0)

              return typeof url !== "string" || url === "" || url.indexOf(voidURL) !== -1;
            }
            /**
             * @public Checks if the url is valid and can be correctly parsed
             * @param {string} url - the url we're checking
             * @param {string} referrer - the web location where the request originated
             */

          }, {
            key: "isValidURL",
            value: function isValidURL(url, referrer) {
              // if invalid url, fail gracefully, return false
              try {
                new _urlPonyFill__WEBPACK_IMPORTED_MODULE_6__["URL"](url, referrer);
              } catch (e) {
                return false;
              }

              return true;
            }
            /**
             * @public Attaches an event handler which calls resourceComplete
             * @param {number} timeStamp - the timestamp of the request represented in unix code
             * @param {string} status - the status of the request
             */

          }, {
            key: "createEventHandler",
            value: function createEventHandler(timeStamp, status) {
              var _this4 = this;

              return function () {
                _this4.historyEntryReporter.resourceComplete(_this4, timeStamp, status);
              };
            }
            /**
             * @protected Our internal function which creates the EnsRequest object and invokes the processing
             * @param {string} url - the url to be processed
             * @param {any} requestBody - the post body attached to the request
             * @param {string} requestType - the type of request being processed
             * @param {string} source - where the request originated from
             * @param {Node} node - reference to the element node associated with the request
             * @param {string} referrer - the web location where the request originated
             * @param {Function} onUrlModifiedFn - a callback that is invoked if the url is modified
             * @param {Function} onAllowedfn - a callback that is invoked if the url is allowed
             */

          }, {
            key: "processURLInternal",
            value: function processURLInternal(url, requestBody, requestType, source, node, referrer, onUrlModifiedFn, onAllowedfn) {
              var processedUrl = url;

              if (this.isVoidURL(processedUrl) || !this.isValidURL(processedUrl, referrer)) {
                return processedUrl;
              }

              var timeStamp = +new Date();
              var request = new _ensRequest__WEBPACK_IMPORTED_MODULE_0__["EnsRequest"]("");
              request.id = timeStamp;
              request.source = source;
              request.type = requestType;
              request.destination = new _urlPonyFill__WEBPACK_IMPORTED_MODULE_6__["URL"](processedUrl, referrer).href;

              if (!this.onBefore(request, requestBody, node, referrer)) {
                var modifiedUrl = this.getBlockedSrcString(request.type, request);
                var modifiedRequestBody = this.getBlockedRequestBody(request, requestBody);

                if (_utilities__WEBPACK_IMPORTED_MODULE_1__["utilities"].getEnvironmentOverrideOrGlobalDefault(this.gatewayData.featureToggles, this.environment, "enableFiltering")) {
                  if (typeof onUrlModifiedFn === "function" && onUrlModifiedFn !== undefined) {
                    onUrlModifiedFn(modifiedUrl, modifiedRequestBody);
                  }

                  processedUrl = modifiedUrl;
                }

                this.sendBlockReport(request, node, referrer);
              } else {
                onAllowedfn(request.id);
              }

              return processedUrl;
            }
            /**
             * @protected Determines the status of a blocked request and invokes resourceComplete to send a blocked report
             * @param {EnsRequest} request - the request being checked
             * @param {Node} node - the element node associated with the request
             * @param {string} referrer - the web location where the request originated
             */

          }, {
            key: "sendBlockReport",
            value: function sendBlockReport(request, node, referrer) {
              var status = "blocked";

              if (this.dataGovernanceManager.shouldMaskUrl(request)) {
                status = "masked";
              }

              if (this.dataGovernanceManager.shouldRedactUrl(request)) {
                status = "redact";
              }

              if (this.sslManager.shouldRewriteProtocol(request, referrer)) {
                status = "rewrite";
              }

              this.historyEntryReporter.resourceComplete(node, request.id, status);
            }
            /**
             * @private Invokes the history entry reporter to create an initial entry for the request, then calls allowRequest to kick off processing
             * @param {EnsRequest} request - the request to be processed
             * @param {any} requestBody - the post body associated with the request
             * @param {Node} node - the element node associated with the request
             * @param {string} referrer - the web location where the request originated
             */

          }, {
            key: "onBefore",
            value: function onBefore(request, requestBody, node, referrer) {
              //checks to see if the history object has awareness of the new node, if not it creates a new history entry and stores is as the last request
              this.historyEntryReporter.createHistoryEntry(request, node);
              return this.allowRequest(request, requestBody, referrer);
            }
            /**
             * @private Invokes our manager classes to check if a request should be allowed or not
             * @param {EnsRequest} request - the request to be processed
             * @param {any} requestBody - the post body associated with the request
             * @param {string} referrer - the web location where the request originated
             */

          }, {
            key: "allowRequest",
            value: function allowRequest(request, requestBody, referrer) {
              var shouldAllowRequest = true;
              var url = request.destination,
                  urlToParse = new _urlPonyFill__WEBPACK_IMPORTED_MODULE_6__["URL"](url, referrer),
                  locationProtocol = new _urlPonyFill__WEBPACK_IMPORTED_MODULE_6__["URL"](referrer).protocol,
                  hostName = urlToParse.hostname,
                  pathName = urlToParse.pathname,
                  protocol = urlToParse.protocol; // auto allow data protocols of images to protect against XSS
              // can allow data protocols for javascript if monitoring is disabled

              var protocolAllowed = this.protoManager.allowProtocolAndBypassProcessing(protocol, request, this.gatewayData.featureToggles);

              if (protocolAllowed) {
                return true;
              } else if (this.protoManager.isBlockedRequestBypassed(protocol)) {
                // currently, we're bypassing further processing on blocked browser extension protocols
                return false;
              }

              var SSLCheckPasses = true;

              if (url !== "") {
                SSLCheckPasses = this.sslManager.allowRequest(locationProtocol, protocol, request);
              } //checks to if the host is in the whitelist and that is using HTTP if required


              var targetListCheckPasses = true; //checks make sure not illegal params are being passed

              var dataMonitoringCheckPasses = true;
              dataMonitoringCheckPasses = this.dataGovernanceManager.allowRequest(request, hostName, requestBody);

              if (this.environment) {
                targetListCheckPasses = this.listProcessor.allowRequest(hostName, request, pathName);
              }

              if (targetListCheckPasses === false || SSLCheckPasses === false || dataMonitoringCheckPasses === false) {
                shouldAllowRequest = false;
              }

              return shouldAllowRequest;
            }
            /**
             * @private Retrieves the blocked src string for a request based on its tagName and rules set in the featureToggles
             * @param {string} tagName - the type of tag being checked
             * @param {EnsRequest} request - the request being checked
             */

          }, {
            key: "getBlockedSrcString",
            value: function getBlockedSrcString(tagName, request) {
              var blockedSrcString = request.destination;

              var getPerTagBlockString = function getPerTagBlockString() {
                if (tagName === "img" || tagName === "image") {
                  return "";
                } else {
                  return "//javascript:;";
                }
              };

              if (this.sslManager.shouldRewriteProtocol(request)) {
                blockedSrcString = this.sslManager.rewriteProtocol(request.destination);

                if (request.reasons.length === 1) {
                  return blockedSrcString;
                }
              } else if (request.hasReason("SSL")) {
                return getPerTagBlockString();
              }

              if (this.dataGovernanceManager.shouldMaskUrl(request)) {
                return this.dataGovernanceManager.getMaskedUrl(blockedSrcString);
              } else if (this.dataGovernanceManager.shouldRedactUrl(request)) {
                return this.dataGovernanceManager.getRedactedUrl(blockedSrcString);
              } else {
                return getPerTagBlockString();
              }
            }
            /**
             * @private Retrieves the blocked request body and checks against our data governance manager
             * @param {EnsRequest} request - the request to be checked
             * @param {any} requestBody - the post body associated with the request
             */

          }, {
            key: "getBlockedRequestBody",
            value: function getBlockedRequestBody(request, requestBody) {
              if (this.dataGovernanceManager.shouldMaskData(request)) {
                return this.dataGovernanceManager.getMaskedData(requestBody);
              } else if (this.dataGovernanceManager.shouldRedactData(request)) {
                return this.dataGovernanceManager.getRedactedData(requestBody);
              } else {
                return requestBody;
              }
            }
          }]);

          return UrlProcessor;
        }();

        ;
        /***/
      },

      /***/
      "./src/utilities.ts":
      /*!**************************!*\
        !*** ./src/utilities.ts ***!
        \**************************/

      /*! exports provided: utilities */

      /***/
      function srcUtilitiesTs(module, __webpack_exports__, __webpack_require__) {
        "use strict";

        __webpack_require__.r(__webpack_exports__);
        /* harmony export (binding) */


        __webpack_require__.d(__webpack_exports__, "utilities", function () {
          return utilities;
        });
        /* harmony import */


        var _urlPonyFill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
        /*! ./urlPonyFill */
        "./src/urlPonyFill.ts");

        var utilities = function () {
          return {
            /**
             * joins an array of strings in to a RegExp. if the array is empty returns an un-matchable RegExp
             * @param arr
             * @param flags - Flags to add to regex
             * @returns {RegExp}
             */
            // setting undefined as default value prevents TS errors when we call this function with one argument
            arrayToRegex: function arrayToRegex(arr) {
              var flags = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;

              if (Array.isArray(arr)) {
                if (arr && arr.length > 0 && arr.join) {
                  return new RegExp(arr.join("|"), flags);
                }

                return new RegExp("a^", flags);
              }
            },
            each: function each(list, fn, context) {
              if (list === undefined || !("length" in list)) {
                list = {
                  length: 0
                };
              }

              for (var i = 0; i < list.length; i++) {
                fn.call(context, list[i], i, list);
              }
            },
            isUriEncoded: function isUriEncoded(uri) {
              if (typeof uri === "string") {
                return uri !== decodeURIComponent(uri);
              }

              return false;
            },
            isValidUrl: function isValidUrl(string) {
              try {
                new _urlPonyFill__WEBPACK_IMPORTED_MODULE_0__["URL"](string);
                return true;
              } catch (_) {
                return false;
              }
            },
            isWhitelist: function isWhitelist(gatewayData, environment) {
              return this.getEnvironmentOverrideOrGlobalDefault(gatewayData.featureToggles, environment, "usesWhitelist");
            },
            decodeUriComponent: function decodeUriComponent(str) {
              var decodedUri = str;

              try {
                if (typeof str === "string") {
                  while (this.isUriEncoded(decodedUri)) {
                    decodedUri = decodeURIComponent(decodedUri);
                  }
                }
              } catch (e) {
                console.warn("URI: " + str + ". Could not be decoded properly. Original string was returned.");
              }

              return decodedUri;
            },
            encodeUriComponent: function encodeUriComponent(str) {
              var encodedUri = str;

              try {
                if (typeof str === "string") {
                  encodedUri = encodeURIComponent(str);
                }
              } catch (e) {
                console.warn("URI: " + str + ". Could not be encoded properly. Original string was returned.");
              }

              return encodedUri;
            },
            repeatString: function repeatString(s, n) {
              var a = [],
                  i = 0;

              for (; i < n;) {
                a[i++] = s;
              }

              return a.join("");
            },
            getEnvironmentOverrideOrGlobalDefault: function getEnvironmentOverrideOrGlobalDefault(featureToggles, environment, property) {
              if (environment && environment.hasOwnProperty(property)) {
                // tsint-disable-line no-prototype-builtins
                return environment[property];
              } else {
                return featureToggles[property];
              }
            }
          };
        }();
        /***/

      }
      /******/

    })
  );
});
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../webpack/buildin/module.js */ "./node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "./node_modules/webpack/buildin/module.js":
/*!***********************************!*\
  !*** (webpack)/buildin/module.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function (module) {
  if (!module.webpackPolyfill) {
    module.deprecate = function () {};

    module.paths = []; // module.parent = undefined by default

    if (!module.children) module.children = [];
    Object.defineProperty(module, "loaded", {
      enumerable: true,
      get: function get() {
        return module.l;
      }
    });
    Object.defineProperty(module, "id", {
      enumerable: true,
      get: function get() {
        return module.i;
      }
    });
    module.webpackPolyfill = 1;
  }

  return module;
};

/***/ }),

/***/ "./privacyUrlProcessor.ts":
/*!********************************!*\
  !*** ./privacyUrlProcessor.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

self = {};
location = {
    hostname: "nexus.ensighten.com"
};
module.exports = __webpack_require__(/*! ./node_modules/atob/node-atob */ "./node_modules/atob/node-atob.js").atob;
module.exports = __webpack_require__(/*! ./node_modules/url-processor/lib/index */ "./node_modules/url-processor/lib/index.js").UrlProcessor;


/***/ }),

/***/ 0:
/*!**************************************!*\
  !*** multi ./privacyUrlProcessor.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./privacyUrlProcessor.ts */"./privacyUrlProcessor.ts");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2F0b2Ivbm9kZS1hdG9iLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9iYXNlNjQtanMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2J1ZmZlci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvaWVlZTc1NC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvaXNhcnJheS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci93ZWJwYWNrL3VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24iLCJ3ZWJwYWNrOi8vL1ByaXZhY3lVcmxQcm9jZXNzb3Ivd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vL1ByaXZhY3lVcmxQcm9jZXNzb3Ivbm9kZV9tb2R1bGVzL2Jhc2U2NC1qcy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci9ub2RlX21vZHVsZXMvYmFzZTY0LXVybC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci9ub2RlX21vZHVsZXMvYnVmZmVyL2luZGV4LmpzIiwid2VicGFjazovLy9Qcml2YWN5VXJsUHJvY2Vzc29yL25vZGVfbW9kdWxlcy9pZWVlNzU0L2luZGV4LmpzIiwid2VicGFjazovLy9Qcml2YWN5VXJsUHJvY2Vzc29yL25vZGVfbW9kdWxlcy9pc2FycmF5L2luZGV4LmpzIiwid2VicGFjazovLy9Qcml2YWN5VXJsUHJvY2Vzc29yLyh3ZWJwYWNrKS9idWlsZGluL2dsb2JhbC5qcyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci9zcmMvZGF0YUdvdmVybmFuY2VNYW5hZ2VyLnRzIiwid2VicGFjazovLy9Qcml2YWN5VXJsUHJvY2Vzc29yL3NyYy9lbnNSZXF1ZXN0LnRzIiwid2VicGFjazovLy9Qcml2YWN5VXJsUHJvY2Vzc29yL3NyYy9pbmRleC50cyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci9zcmMvbGlzdFByb2Nlc3Nvci50cyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci9zcmMvcGF0aEZpbHRlck1hbmFnZXIudHMiLCJ3ZWJwYWNrOi8vL1ByaXZhY3lVcmxQcm9jZXNzb3Ivc3JjL3Byb3RvTWFuYWdlci50cyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci9zcmMvc3NsTWFuYWdlci50cyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci9zcmMvdXJsUG9ueUZpbGwudHMiLCJ3ZWJwYWNrOi8vL1ByaXZhY3lVcmxQcm9jZXNzb3Ivc3JjL3VybFByb2Nlc3Nvci50cyIsIndlYnBhY2s6Ly8vUHJpdmFjeVVybFByb2Nlc3Nvci9zcmMvdXRpbGl0aWVzLnRzIiwid2VicGFjazovLy8od2VicGFjaykvYnVpbGRpbi9nbG9iYWwuanMiLCJ3ZWJwYWNrOi8vLyh3ZWJwYWNrKS9idWlsZGluL21vZHVsZS5qcyIsIndlYnBhY2s6Ly8vLi9wcml2YWN5VXJsUHJvY2Vzc29yLnRzIl0sIm5hbWVzIjpbImF0b2IiLCJzdHIiLCJCdWZmZXIiLCJmcm9tIiwidG9TdHJpbmciLCJtb2R1bGUiLCJleHBvcnRzIiwiYnl0ZUxlbmd0aCIsInRvQnl0ZUFycmF5IiwiZnJvbUJ5dGVBcnJheSIsImxvb2t1cCIsInJldkxvb2t1cCIsIkFyciIsIlVpbnQ4QXJyYXkiLCJBcnJheSIsImNvZGUiLCJpIiwibGVuIiwibGVuZ3RoIiwiY2hhckNvZGVBdCIsImdldExlbnMiLCJiNjQiLCJFcnJvciIsInZhbGlkTGVuIiwiaW5kZXhPZiIsInBsYWNlSG9sZGVyc0xlbiIsImxlbnMiLCJfYnl0ZUxlbmd0aCIsInRtcCIsImFyciIsImN1ckJ5dGUiLCJ0cmlwbGV0VG9CYXNlNjQiLCJudW0iLCJlbmNvZGVDaHVuayIsInVpbnQ4Iiwic3RhcnQiLCJlbmQiLCJvdXRwdXQiLCJwdXNoIiwiam9pbiIsImV4dHJhQnl0ZXMiLCJwYXJ0cyIsIm1heENodW5rTGVuZ3RoIiwibGVuMiIsImJhc2U2NCIsInJlcXVpcmUiLCJpZWVlNzU0IiwiaXNBcnJheSIsIlNsb3dCdWZmZXIiLCJJTlNQRUNUX01BWF9CWVRFUyIsIlRZUEVEX0FSUkFZX1NVUFBPUlQiLCJnbG9iYWwiLCJ1bmRlZmluZWQiLCJ0eXBlZEFycmF5U3VwcG9ydCIsImtNYXhMZW5ndGgiLCJfX3Byb3RvX18iLCJwcm90b3R5cGUiLCJmb28iLCJzdWJhcnJheSIsImUiLCJjcmVhdGVCdWZmZXIiLCJ0aGF0IiwiUmFuZ2VFcnJvciIsImFyZyIsImVuY29kaW5nT3JPZmZzZXQiLCJhbGxvY1Vuc2FmZSIsInBvb2xTaXplIiwiX2F1Z21lbnQiLCJ2YWx1ZSIsIlR5cGVFcnJvciIsIkFycmF5QnVmZmVyIiwiZnJvbUFycmF5QnVmZmVyIiwiZnJvbVN0cmluZyIsImZyb21PYmplY3QiLCJTeW1ib2wiLCJzcGVjaWVzIiwiT2JqZWN0IiwiZGVmaW5lUHJvcGVydHkiLCJjb25maWd1cmFibGUiLCJhc3NlcnRTaXplIiwic2l6ZSIsImFsbG9jIiwiZmlsbCIsImVuY29kaW5nIiwiY2hlY2tlZCIsImFsbG9jVW5zYWZlU2xvdyIsInN0cmluZyIsImlzRW5jb2RpbmciLCJhY3R1YWwiLCJ3cml0ZSIsInNsaWNlIiwiZnJvbUFycmF5TGlrZSIsImFycmF5IiwiYnl0ZU9mZnNldCIsIm9iaiIsImlzQnVmZmVyIiwiY29weSIsImJ1ZmZlciIsImlzbmFuIiwidHlwZSIsImRhdGEiLCJiIiwiX2lzQnVmZmVyIiwiY29tcGFyZSIsImEiLCJ4IiwieSIsIk1hdGgiLCJtaW4iLCJTdHJpbmciLCJ0b0xvd2VyQ2FzZSIsImNvbmNhdCIsImxpc3QiLCJwb3MiLCJidWYiLCJpc1ZpZXciLCJsb3dlcmVkQ2FzZSIsInV0ZjhUb0J5dGVzIiwiYmFzZTY0VG9CeXRlcyIsInNsb3dUb1N0cmluZyIsImhleFNsaWNlIiwidXRmOFNsaWNlIiwiYXNjaWlTbGljZSIsImxhdGluMVNsaWNlIiwiYmFzZTY0U2xpY2UiLCJ1dGYxNmxlU2xpY2UiLCJzd2FwIiwibiIsIm0iLCJzd2FwMTYiLCJzd2FwMzIiLCJzd2FwNjQiLCJhcmd1bWVudHMiLCJhcHBseSIsImVxdWFscyIsImluc3BlY3QiLCJtYXgiLCJtYXRjaCIsInRhcmdldCIsInRoaXNTdGFydCIsInRoaXNFbmQiLCJ0aGlzQ29weSIsInRhcmdldENvcHkiLCJiaWRpcmVjdGlvbmFsSW5kZXhPZiIsInZhbCIsImRpciIsImlzTmFOIiwiYXJyYXlJbmRleE9mIiwiY2FsbCIsImxhc3RJbmRleE9mIiwiaW5kZXhTaXplIiwiYXJyTGVuZ3RoIiwidmFsTGVuZ3RoIiwicmVhZCIsInJlYWRVSW50MTZCRSIsImZvdW5kSW5kZXgiLCJmb3VuZCIsImoiLCJpbmNsdWRlcyIsImhleFdyaXRlIiwib2Zmc2V0IiwiTnVtYmVyIiwicmVtYWluaW5nIiwic3RyTGVuIiwicGFyc2VkIiwicGFyc2VJbnQiLCJzdWJzdHIiLCJ1dGY4V3JpdGUiLCJibGl0QnVmZmVyIiwiYXNjaWlXcml0ZSIsImFzY2lpVG9CeXRlcyIsImxhdGluMVdyaXRlIiwiYmFzZTY0V3JpdGUiLCJ1Y3MyV3JpdGUiLCJ1dGYxNmxlVG9CeXRlcyIsImlzRmluaXRlIiwidG9KU09OIiwiX2FyciIsInJlcyIsImZpcnN0Qnl0ZSIsImNvZGVQb2ludCIsImJ5dGVzUGVyU2VxdWVuY2UiLCJzZWNvbmRCeXRlIiwidGhpcmRCeXRlIiwiZm91cnRoQnl0ZSIsInRlbXBDb2RlUG9pbnQiLCJkZWNvZGVDb2RlUG9pbnRzQXJyYXkiLCJNQVhfQVJHVU1FTlRTX0xFTkdUSCIsImNvZGVQb2ludHMiLCJmcm9tQ2hhckNvZGUiLCJyZXQiLCJvdXQiLCJ0b0hleCIsImJ5dGVzIiwibmV3QnVmIiwic2xpY2VMZW4iLCJjaGVja09mZnNldCIsImV4dCIsInJlYWRVSW50TEUiLCJub0Fzc2VydCIsIm11bCIsInJlYWRVSW50QkUiLCJyZWFkVUludDgiLCJyZWFkVUludDE2TEUiLCJyZWFkVUludDMyTEUiLCJyZWFkVUludDMyQkUiLCJyZWFkSW50TEUiLCJwb3ciLCJyZWFkSW50QkUiLCJyZWFkSW50OCIsInJlYWRJbnQxNkxFIiwicmVhZEludDE2QkUiLCJyZWFkSW50MzJMRSIsInJlYWRJbnQzMkJFIiwicmVhZEZsb2F0TEUiLCJyZWFkRmxvYXRCRSIsInJlYWREb3VibGVMRSIsInJlYWREb3VibGVCRSIsImNoZWNrSW50Iiwid3JpdGVVSW50TEUiLCJtYXhCeXRlcyIsIndyaXRlVUludEJFIiwid3JpdGVVSW50OCIsImZsb29yIiwib2JqZWN0V3JpdGVVSW50MTYiLCJsaXR0bGVFbmRpYW4iLCJ3cml0ZVVJbnQxNkxFIiwid3JpdGVVSW50MTZCRSIsIm9iamVjdFdyaXRlVUludDMyIiwid3JpdGVVSW50MzJMRSIsIndyaXRlVUludDMyQkUiLCJ3cml0ZUludExFIiwibGltaXQiLCJzdWIiLCJ3cml0ZUludEJFIiwid3JpdGVJbnQ4Iiwid3JpdGVJbnQxNkxFIiwid3JpdGVJbnQxNkJFIiwid3JpdGVJbnQzMkxFIiwid3JpdGVJbnQzMkJFIiwiY2hlY2tJRUVFNzU0Iiwid3JpdGVGbG9hdCIsIndyaXRlRmxvYXRMRSIsIndyaXRlRmxvYXRCRSIsIndyaXRlRG91YmxlIiwid3JpdGVEb3VibGVMRSIsIndyaXRlRG91YmxlQkUiLCJ0YXJnZXRTdGFydCIsInNldCIsIklOVkFMSURfQkFTRTY0X1JFIiwiYmFzZTY0Y2xlYW4iLCJzdHJpbmd0cmltIiwicmVwbGFjZSIsInRyaW0iLCJ1bml0cyIsIkluZmluaXR5IiwibGVhZFN1cnJvZ2F0ZSIsImJ5dGVBcnJheSIsImMiLCJoaSIsImxvIiwic3JjIiwiZHN0IiwiaXNMRSIsIm1MZW4iLCJuQnl0ZXMiLCJlTGVuIiwiZU1heCIsImVCaWFzIiwibkJpdHMiLCJkIiwicyIsIk5hTiIsInJ0IiwiYWJzIiwibG9nIiwiTE4yIiwiRGF0YUdvdmVybmFuY2VNYW5hZ2VyIiwiYWN0aXZlTW9uaXRvcmluZ1BhcmFtcyIsInJlZ2V4QXJyYXkiLCJ1dGlsaXRpZXMiLCJ1cmwiLCJyZXF1ZXN0IiwiZGF0YU1vbml0b3JpbmdDaGVja1Bhc3NlcyIsImRlY29kZWRVUkxTdHJpbmciLCJib2R5Iiwic2hvdWxkTWFzayIsInNob3VsZFJlZGFjdCIsInBhdGgiLCJwYXRoQXJyYXkiLCJkZWNvZGVkU3RyaW5nIiwibmV3UGF0aCIsInBhcmFtZXRlcnMiLCJlcyIsImZvcm1EYXRhIiwicGFpciIsIm5ld1N0cmluZ1ZhbCIsInF1ZXJ5U3RyaW5nIiwibmV3RGF0YSIsImtleSIsImlzUmVkYWN0ZWQiLCJyZXBsYWNlbWVudFN0cmluZyIsInJlcGxhY2VtZW50U3RyaW5nQ2hhcnMiLCJ1cmxTdHJpbmciLCJVcmwiLCJzZWFyY2hQYXJhbXMiLCJibG9ja1VSTFBhcmFtc1JlZ2V4IiwicmVnZXgiLCJtYXRjaGVkRGF0YVBhdHRlcm4iLCJwYXRoRW50cmllc0FycmF5IiwicGFyYW0iLCJwYXRoS2V5VmFsdWVBcnJheSIsInBhdGhFbnRyeSIsInBhcmFtQXJyYXkiLCJ1bkRlY29kZWRVcmlDb21wb25lbnQiLCJpc0Jsb2NrZWRQYXJhbXMiLCJuZXdLZXlWYWx1ZSIsIm5ld1F1ZXJ5U3RyaW5nIiwiRW5zUmVxdWVzdCIsImRlc3RpbmF0aW9uIiwiTGlzdFByb2Nlc3NvciIsImFsbG93ZWRVcmxzIiwiYmxvY2tlZFVybHMiLCJlbnZpcm9ubWVudCIsImdhdGV3YXlEYXRhIiwiYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzIiwibGlzdFJlZ0V4IiwidGFyZ2V0TGlzdENoZWNrUGFzc2VzIiwibWF0Y2hlZENhdGVnb3JpZXMiLCJQYXRoRmlsdGVyTWFuYWdlciIsInJlZyIsImNvb2tpZVZhbHVlIiwiZmxhZ3MiLCJzaG91bGRBZGRUb0xpc3RSZWdleCIsImNhdGVnb3J5IiwiYXJyYXlPZk9iamVjdFZhbHMiLCJhcnJheU9mT2JqZWN0cyIsIm5leHVzRG9tYWluIiwidGFnIiwibG9jYXRpb24iLCJob3N0bmFtZSIsImNhdGVnb3J5TGlzdCIsImhvc3ROYW1lIiwiYWN0aXZlTGlzdCIsInNob3VsZEFkZENhdGVnb3J5VG9MaXN0IiwiYWxsb3dlZFRhZ3NJbkxvY2FsU3RvcmFnZSIsImJsb2NrZWRUYWdzSW5Mb2NhbFN0b3JhZ2UiLCJhbGxvd1JlcXVlc3QiLCJ0YWdzV2l0aEFsbG93UGF0aHNDb3VudCIsImFsbG93UGF0aFJlZ0V4IiwiYWxsb3dSZWdBcnJheSIsImFsbG93UmVnIiwidGFnc1dpdGhCbG9ja1BhdGhzQ291bnQiLCJkb21haW5SZWdFeCIsImJsb2NrUmVnQXJyYXkiLCJibG9ja1JlZyIsInRhZ3NXaXRoRmlsdGVycyIsInRhZ3NXaXRoQWxsb3dQYXRocyIsInRhZ3NXaXRoQmxvY2tQYXRocyIsInRhZ3NPbmx5RnVuY3Rpb24iLCJpdGVtIiwidGFnc1dpdGhQYXRoc0Z1bmN0aW9uIiwicGF0aHMiLCJ1cGRhdGVUYWdMaXN0c0Z1bmN0aW9uIiwicmVnRXgiLCJhbGxvd1BhdGhUYWdzIiwiYmxvY2tQYXRoVGFncyIsImxvY2FsVGFnc1dpdGhOb0ZpbHRlcnMiLCJsaXN0VG9QcnVuZSIsImNvbXBhcmlzb25GaWVsZCIsInJlbW92ZUlmUHJlc2VudCIsInVwZGF0ZWRHbG9iYWxzIiwiZmlsdGVyRnVuY3Rpb24iLCJmaWx0ZXJUeXBlIiwidGFnTGlzdCIsIlByb3RvTWFuYWdlciIsImFsbG93ZWQiLCJibG9ja1JlYXNvbiIsImFkZGl0aW9uYWxBbGxvd0NvbmRpdGlvbnMiLCJmZWF0dXJlVG9nZ2xlcyIsInNzbEVuZm9yY2VtZW50IiwiU1NMTWFuYWdlciIsIlNTTENoZWNrUGFzc2VzIiwibG9jYXRpb25Qcm90b2NvbCIsInByb3RvY29sIiwic2hvdWxkUmV3cml0ZSIsInJlbGF0aXZlIiwicmVsYXRpdmVQYXRoRG90TWFwcGluZyIsImNsZWFyIiwiaW52YWxpZCIsImgiLCJ1bmljb2RlIiwiZW5jb2RlVVJJQ29tcG9uZW50IiwiRU9GIiwiQUxQSEEiLCJBTFBIQU5VTUVSSUMiLCJlcnJvcnMiLCJzdGF0ZSIsInN0YXRlT3ZlcnJpZGUiLCJjdXJzb3IiLCJzZWVuQXQiLCJzZWVuQnJhY2tldCIsImxvb3AiLCJpbnB1dCIsImVyciIsImlzUmVsYXRpdmVTY2hlbWUiLCJiYXNlIiwicGVyY2VudEVzY2FwZSIsIm5leHRDIiwibmV4dE5leHRDIiwiY3AiLCJ0ZW1wQyIsIklETkFUb0FTQ0lJIiwidGVtcCIsInBlcmNlbnRFc2NhcGVRdWVyeSIsInBhcnNlIiwialVSTCIsImF1dGhvcml0eSIsInNlYXJjaCIsImhhc2giLCJob3N0IiwiT3JpZ2luYWxVUkwiLCJzZWxmIiwiVVJMIiwiVVJManNJbXBsIiwiaGFzV29ya2luZ1VybCIsInUiLCJjb25zb2xlIiwiVXJsUHJvY2Vzc29yIiwiaW5zcGVjdEZvcm1EYXRhIiwiaGlzdG9yeUVudHJ5UmVwb3J0ZXIiLCJub2RlIiwidm9pZFVSTCIsInByb2Nlc3NlZFVybCIsInRpbWVTdGFtcCIsIm1vZGlmaWVkVXJsIiwibW9kaWZpZWRSZXF1ZXN0Qm9keSIsIm9uVXJsTW9kaWZpZWRGbiIsIm9uQWxsb3dlZGZuIiwic3RhdHVzIiwic2hvdWxkQWxsb3dSZXF1ZXN0IiwidXJsVG9QYXJzZSIsInBhdGhOYW1lIiwicHJvdG9jb2xBbGxvd2VkIiwiYmxvY2tlZFNyY1N0cmluZyIsImdldFBlclRhZ0Jsb2NrU3RyaW5nIiwidGFnTmFtZSIsImFycmF5VG9SZWdleCIsImVhY2giLCJmbiIsImlzVXJpRW5jb2RlZCIsInVyaSIsImRlY29kZVVSSUNvbXBvbmVudCIsImlzVmFsaWRVcmwiLCJpc1doaXRlbGlzdCIsImRlY29kZVVyaUNvbXBvbmVudCIsImRlY29kZWRVcmkiLCJlbmNvZGVVcmlDb21wb25lbnQiLCJlbmNvZGVkVXJpIiwicmVwZWF0U3RyaW5nIiwiZ2V0RW52aXJvbm1lbnRPdmVycmlkZU9yR2xvYmFsRGVmYXVsdCIsImciLCJGdW5jdGlvbiIsIndpbmRvdyIsIndlYnBhY2tQb2x5ZmlsbCIsImRlcHJlY2F0ZSIsImNoaWxkcmVuIiwiZW51bWVyYWJsZSIsImdldCIsImwiXSwibWFwcGluZ3MiOiI7O1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBLDhDQUFhOztBQUViLFNBQVNBLElBQVQsQ0FBY0MsR0FBZCxFQUFtQjtBQUNqQixTQUFPQyxNQUFNLENBQUNDLElBQVAsQ0FBWUYsR0FBWixFQUFpQixRQUFqQixFQUEyQkcsUUFBM0IsQ0FBb0MsUUFBcEMsQ0FBUDtBQUNEOztBQUVEQyxNQUFNLENBQUNDLE9BQVAsR0FBaUJOLElBQUksQ0FBQ0EsSUFBTCxHQUFZQSxJQUE3QixDOzs7Ozs7Ozs7Ozs7O0FDTkE7O0FBRUFNLE9BQU8sQ0FBQ0MsVUFBUixHQUFxQkEsVUFBckI7QUFDQUQsT0FBTyxDQUFDRSxXQUFSLEdBQXNCQSxXQUF0QjtBQUNBRixPQUFPLENBQUNHLGFBQVIsR0FBd0JBLGFBQXhCO0FBRUEsSUFBSUMsTUFBTSxHQUFHLEVBQWI7QUFDQSxJQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQSxJQUFJQyxHQUFHLEdBQUcsT0FBT0MsVUFBUCxLQUFzQixXQUF0QixHQUFvQ0EsVUFBcEMsR0FBaURDLEtBQTNEO0FBRUEsSUFBSUMsSUFBSSxHQUFHLGtFQUFYOztBQUNBLEtBQUssSUFBSUMsQ0FBQyxHQUFHLENBQVIsRUFBV0MsR0FBRyxHQUFHRixJQUFJLENBQUNHLE1BQTNCLEVBQW1DRixDQUFDLEdBQUdDLEdBQXZDLEVBQTRDLEVBQUVELENBQTlDLEVBQWlEO0FBQy9DTixRQUFNLENBQUNNLENBQUQsQ0FBTixHQUFZRCxJQUFJLENBQUNDLENBQUQsQ0FBaEI7QUFDQUwsV0FBUyxDQUFDSSxJQUFJLENBQUNJLFVBQUwsQ0FBZ0JILENBQWhCLENBQUQsQ0FBVCxHQUFnQ0EsQ0FBaEM7QUFDRCxDLENBRUQ7QUFDQTs7O0FBQ0FMLFNBQVMsQ0FBQyxJQUFJUSxVQUFKLENBQWUsQ0FBZixDQUFELENBQVQsR0FBK0IsRUFBL0I7QUFDQVIsU0FBUyxDQUFDLElBQUlRLFVBQUosQ0FBZSxDQUFmLENBQUQsQ0FBVCxHQUErQixFQUEvQjs7QUFFQSxTQUFTQyxPQUFULENBQWtCQyxHQUFsQixFQUF1QjtBQUNyQixNQUFJSixHQUFHLEdBQUdJLEdBQUcsQ0FBQ0gsTUFBZDs7QUFFQSxNQUFJRCxHQUFHLEdBQUcsQ0FBTixHQUFVLENBQWQsRUFBaUI7QUFDZixVQUFNLElBQUlLLEtBQUosQ0FBVSxnREFBVixDQUFOO0FBQ0QsR0FMb0IsQ0FPckI7QUFDQTs7O0FBQ0EsTUFBSUMsUUFBUSxHQUFHRixHQUFHLENBQUNHLE9BQUosQ0FBWSxHQUFaLENBQWY7QUFDQSxNQUFJRCxRQUFRLEtBQUssQ0FBQyxDQUFsQixFQUFxQkEsUUFBUSxHQUFHTixHQUFYO0FBRXJCLE1BQUlRLGVBQWUsR0FBR0YsUUFBUSxLQUFLTixHQUFiLEdBQ2xCLENBRGtCLEdBRWxCLElBQUtNLFFBQVEsR0FBRyxDQUZwQjtBQUlBLFNBQU8sQ0FBQ0EsUUFBRCxFQUFXRSxlQUFYLENBQVA7QUFDRCxDLENBRUQ7OztBQUNBLFNBQVNsQixVQUFULENBQXFCYyxHQUFyQixFQUEwQjtBQUN4QixNQUFJSyxJQUFJLEdBQUdOLE9BQU8sQ0FBQ0MsR0FBRCxDQUFsQjtBQUNBLE1BQUlFLFFBQVEsR0FBR0csSUFBSSxDQUFDLENBQUQsQ0FBbkI7QUFDQSxNQUFJRCxlQUFlLEdBQUdDLElBQUksQ0FBQyxDQUFELENBQTFCO0FBQ0EsU0FBUSxDQUFDSCxRQUFRLEdBQUdFLGVBQVosSUFBK0IsQ0FBL0IsR0FBbUMsQ0FBcEMsR0FBeUNBLGVBQWhEO0FBQ0Q7O0FBRUQsU0FBU0UsV0FBVCxDQUFzQk4sR0FBdEIsRUFBMkJFLFFBQTNCLEVBQXFDRSxlQUFyQyxFQUFzRDtBQUNwRCxTQUFRLENBQUNGLFFBQVEsR0FBR0UsZUFBWixJQUErQixDQUEvQixHQUFtQyxDQUFwQyxHQUF5Q0EsZUFBaEQ7QUFDRDs7QUFFRCxTQUFTakIsV0FBVCxDQUFzQmEsR0FBdEIsRUFBMkI7QUFDekIsTUFBSU8sR0FBSjtBQUNBLE1BQUlGLElBQUksR0FBR04sT0FBTyxDQUFDQyxHQUFELENBQWxCO0FBQ0EsTUFBSUUsUUFBUSxHQUFHRyxJQUFJLENBQUMsQ0FBRCxDQUFuQjtBQUNBLE1BQUlELGVBQWUsR0FBR0MsSUFBSSxDQUFDLENBQUQsQ0FBMUI7QUFFQSxNQUFJRyxHQUFHLEdBQUcsSUFBSWpCLEdBQUosQ0FBUWUsV0FBVyxDQUFDTixHQUFELEVBQU1FLFFBQU4sRUFBZ0JFLGVBQWhCLENBQW5CLENBQVY7QUFFQSxNQUFJSyxPQUFPLEdBQUcsQ0FBZCxDQVJ5QixDQVV6Qjs7QUFDQSxNQUFJYixHQUFHLEdBQUdRLGVBQWUsR0FBRyxDQUFsQixHQUNORixRQUFRLEdBQUcsQ0FETCxHQUVOQSxRQUZKO0FBSUEsTUFBSVAsQ0FBSjs7QUFDQSxPQUFLQSxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUdDLEdBQWhCLEVBQXFCRCxDQUFDLElBQUksQ0FBMUIsRUFBNkI7QUFDM0JZLE9BQUcsR0FDQWpCLFNBQVMsQ0FBQ1UsR0FBRyxDQUFDRixVQUFKLENBQWVILENBQWYsQ0FBRCxDQUFULElBQWdDLEVBQWpDLEdBQ0NMLFNBQVMsQ0FBQ1UsR0FBRyxDQUFDRixVQUFKLENBQWVILENBQUMsR0FBRyxDQUFuQixDQUFELENBQVQsSUFBb0MsRUFEckMsR0FFQ0wsU0FBUyxDQUFDVSxHQUFHLENBQUNGLFVBQUosQ0FBZUgsQ0FBQyxHQUFHLENBQW5CLENBQUQsQ0FBVCxJQUFvQyxDQUZyQyxHQUdBTCxTQUFTLENBQUNVLEdBQUcsQ0FBQ0YsVUFBSixDQUFlSCxDQUFDLEdBQUcsQ0FBbkIsQ0FBRCxDQUpYO0FBS0FhLE9BQUcsQ0FBQ0MsT0FBTyxFQUFSLENBQUgsR0FBa0JGLEdBQUcsSUFBSSxFQUFSLEdBQWMsSUFBL0I7QUFDQUMsT0FBRyxDQUFDQyxPQUFPLEVBQVIsQ0FBSCxHQUFrQkYsR0FBRyxJQUFJLENBQVIsR0FBYSxJQUE5QjtBQUNBQyxPQUFHLENBQUNDLE9BQU8sRUFBUixDQUFILEdBQWlCRixHQUFHLEdBQUcsSUFBdkI7QUFDRDs7QUFFRCxNQUFJSCxlQUFlLEtBQUssQ0FBeEIsRUFBMkI7QUFDekJHLE9BQUcsR0FDQWpCLFNBQVMsQ0FBQ1UsR0FBRyxDQUFDRixVQUFKLENBQWVILENBQWYsQ0FBRCxDQUFULElBQWdDLENBQWpDLEdBQ0NMLFNBQVMsQ0FBQ1UsR0FBRyxDQUFDRixVQUFKLENBQWVILENBQUMsR0FBRyxDQUFuQixDQUFELENBQVQsSUFBb0MsQ0FGdkM7QUFHQWEsT0FBRyxDQUFDQyxPQUFPLEVBQVIsQ0FBSCxHQUFpQkYsR0FBRyxHQUFHLElBQXZCO0FBQ0Q7O0FBRUQsTUFBSUgsZUFBZSxLQUFLLENBQXhCLEVBQTJCO0FBQ3pCRyxPQUFHLEdBQ0FqQixTQUFTLENBQUNVLEdBQUcsQ0FBQ0YsVUFBSixDQUFlSCxDQUFmLENBQUQsQ0FBVCxJQUFnQyxFQUFqQyxHQUNDTCxTQUFTLENBQUNVLEdBQUcsQ0FBQ0YsVUFBSixDQUFlSCxDQUFDLEdBQUcsQ0FBbkIsQ0FBRCxDQUFULElBQW9DLENBRHJDLEdBRUNMLFNBQVMsQ0FBQ1UsR0FBRyxDQUFDRixVQUFKLENBQWVILENBQUMsR0FBRyxDQUFuQixDQUFELENBQVQsSUFBb0MsQ0FIdkM7QUFJQWEsT0FBRyxDQUFDQyxPQUFPLEVBQVIsQ0FBSCxHQUFrQkYsR0FBRyxJQUFJLENBQVIsR0FBYSxJQUE5QjtBQUNBQyxPQUFHLENBQUNDLE9BQU8sRUFBUixDQUFILEdBQWlCRixHQUFHLEdBQUcsSUFBdkI7QUFDRDs7QUFFRCxTQUFPQyxHQUFQO0FBQ0Q7O0FBRUQsU0FBU0UsZUFBVCxDQUEwQkMsR0FBMUIsRUFBK0I7QUFDN0IsU0FBT3RCLE1BQU0sQ0FBQ3NCLEdBQUcsSUFBSSxFQUFQLEdBQVksSUFBYixDQUFOLEdBQ0x0QixNQUFNLENBQUNzQixHQUFHLElBQUksRUFBUCxHQUFZLElBQWIsQ0FERCxHQUVMdEIsTUFBTSxDQUFDc0IsR0FBRyxJQUFJLENBQVAsR0FBVyxJQUFaLENBRkQsR0FHTHRCLE1BQU0sQ0FBQ3NCLEdBQUcsR0FBRyxJQUFQLENBSFI7QUFJRDs7QUFFRCxTQUFTQyxXQUFULENBQXNCQyxLQUF0QixFQUE2QkMsS0FBN0IsRUFBb0NDLEdBQXBDLEVBQXlDO0FBQ3ZDLE1BQUlSLEdBQUo7QUFDQSxNQUFJUyxNQUFNLEdBQUcsRUFBYjs7QUFDQSxPQUFLLElBQUlyQixDQUFDLEdBQUdtQixLQUFiLEVBQW9CbkIsQ0FBQyxHQUFHb0IsR0FBeEIsRUFBNkJwQixDQUFDLElBQUksQ0FBbEMsRUFBcUM7QUFDbkNZLE9BQUcsR0FDRCxDQUFFTSxLQUFLLENBQUNsQixDQUFELENBQUwsSUFBWSxFQUFiLEdBQW1CLFFBQXBCLEtBQ0VrQixLQUFLLENBQUNsQixDQUFDLEdBQUcsQ0FBTCxDQUFMLElBQWdCLENBQWpCLEdBQXNCLE1BRHZCLEtBRUNrQixLQUFLLENBQUNsQixDQUFDLEdBQUcsQ0FBTCxDQUFMLEdBQWUsSUFGaEIsQ0FERjtBQUlBcUIsVUFBTSxDQUFDQyxJQUFQLENBQVlQLGVBQWUsQ0FBQ0gsR0FBRCxDQUEzQjtBQUNEOztBQUNELFNBQU9TLE1BQU0sQ0FBQ0UsSUFBUCxDQUFZLEVBQVosQ0FBUDtBQUNEOztBQUVELFNBQVM5QixhQUFULENBQXdCeUIsS0FBeEIsRUFBK0I7QUFDN0IsTUFBSU4sR0FBSjtBQUNBLE1BQUlYLEdBQUcsR0FBR2lCLEtBQUssQ0FBQ2hCLE1BQWhCO0FBQ0EsTUFBSXNCLFVBQVUsR0FBR3ZCLEdBQUcsR0FBRyxDQUF2QixDQUg2QixDQUdKOztBQUN6QixNQUFJd0IsS0FBSyxHQUFHLEVBQVo7QUFDQSxNQUFJQyxjQUFjLEdBQUcsS0FBckIsQ0FMNkIsQ0FLRjtBQUUzQjs7QUFDQSxPQUFLLElBQUkxQixDQUFDLEdBQUcsQ0FBUixFQUFXMkIsSUFBSSxHQUFHMUIsR0FBRyxHQUFHdUIsVUFBN0IsRUFBeUN4QixDQUFDLEdBQUcyQixJQUE3QyxFQUFtRDNCLENBQUMsSUFBSTBCLGNBQXhELEVBQXdFO0FBQ3RFRCxTQUFLLENBQUNILElBQU4sQ0FBV0wsV0FBVyxDQUFDQyxLQUFELEVBQVFsQixDQUFSLEVBQVlBLENBQUMsR0FBRzBCLGNBQUwsR0FBdUJDLElBQXZCLEdBQThCQSxJQUE5QixHQUFzQzNCLENBQUMsR0FBRzBCLGNBQXJELENBQXRCO0FBQ0QsR0FWNEIsQ0FZN0I7OztBQUNBLE1BQUlGLFVBQVUsS0FBSyxDQUFuQixFQUFzQjtBQUNwQlosT0FBRyxHQUFHTSxLQUFLLENBQUNqQixHQUFHLEdBQUcsQ0FBUCxDQUFYO0FBQ0F3QixTQUFLLENBQUNILElBQU4sQ0FDRTVCLE1BQU0sQ0FBQ2tCLEdBQUcsSUFBSSxDQUFSLENBQU4sR0FDQWxCLE1BQU0sQ0FBRWtCLEdBQUcsSUFBSSxDQUFSLEdBQWEsSUFBZCxDQUROLEdBRUEsSUFIRjtBQUtELEdBUEQsTUFPTyxJQUFJWSxVQUFVLEtBQUssQ0FBbkIsRUFBc0I7QUFDM0JaLE9BQUcsR0FBRyxDQUFDTSxLQUFLLENBQUNqQixHQUFHLEdBQUcsQ0FBUCxDQUFMLElBQWtCLENBQW5CLElBQXdCaUIsS0FBSyxDQUFDakIsR0FBRyxHQUFHLENBQVAsQ0FBbkM7QUFDQXdCLFNBQUssQ0FBQ0gsSUFBTixDQUNFNUIsTUFBTSxDQUFDa0IsR0FBRyxJQUFJLEVBQVIsQ0FBTixHQUNBbEIsTUFBTSxDQUFFa0IsR0FBRyxJQUFJLENBQVIsR0FBYSxJQUFkLENBRE4sR0FFQWxCLE1BQU0sQ0FBRWtCLEdBQUcsSUFBSSxDQUFSLEdBQWEsSUFBZCxDQUZOLEdBR0EsR0FKRjtBQU1EOztBQUVELFNBQU9hLEtBQUssQ0FBQ0YsSUFBTixDQUFXLEVBQVgsQ0FBUDtBQUNELEM7Ozs7Ozs7Ozs7OztBQ3JKRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFFQTs7QUFFQSxJQUFJSyxNQUFNLEdBQUdDLG1CQUFPLENBQUMsb0RBQUQsQ0FBcEI7O0FBQ0EsSUFBSUMsT0FBTyxHQUFHRCxtQkFBTyxDQUFDLGdEQUFELENBQXJCOztBQUNBLElBQUlFLE9BQU8sR0FBR0YsbUJBQU8sQ0FBQyxnREFBRCxDQUFyQjs7QUFFQXZDLE9BQU8sQ0FBQ0osTUFBUixHQUFpQkEsTUFBakI7QUFDQUksT0FBTyxDQUFDMEMsVUFBUixHQUFxQkEsVUFBckI7QUFDQTFDLE9BQU8sQ0FBQzJDLGlCQUFSLEdBQTRCLEVBQTVCO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBL0MsTUFBTSxDQUFDZ0QsbUJBQVAsR0FBNkJDLE1BQU0sQ0FBQ0QsbUJBQVAsS0FBK0JFLFNBQS9CLEdBQ3pCRCxNQUFNLENBQUNELG1CQURrQixHQUV6QkcsaUJBQWlCLEVBRnJCO0FBSUE7QUFDQTtBQUNBOztBQUNBL0MsT0FBTyxDQUFDZ0QsVUFBUixHQUFxQkEsVUFBVSxFQUEvQjs7QUFFQSxTQUFTRCxpQkFBVCxHQUE4QjtBQUM1QixNQUFJO0FBQ0YsUUFBSXhCLEdBQUcsR0FBRyxJQUFJaEIsVUFBSixDQUFlLENBQWYsQ0FBVjtBQUNBZ0IsT0FBRyxDQUFDMEIsU0FBSixHQUFnQjtBQUFDQSxlQUFTLEVBQUUxQyxVQUFVLENBQUMyQyxTQUF2QjtBQUFrQ0MsU0FBRyxFQUFFLGVBQVk7QUFBRSxlQUFPLEVBQVA7QUFBVztBQUFoRSxLQUFoQjtBQUNBLFdBQU81QixHQUFHLENBQUM0QixHQUFKLE9BQWMsRUFBZCxJQUFvQjtBQUN2QixXQUFPNUIsR0FBRyxDQUFDNkIsUUFBWCxLQUF3QixVQURyQixJQUNtQztBQUN0QzdCLE9BQUcsQ0FBQzZCLFFBQUosQ0FBYSxDQUFiLEVBQWdCLENBQWhCLEVBQW1CbkQsVUFBbkIsS0FBa0MsQ0FGdEMsQ0FIRSxDQUtzQztBQUN6QyxHQU5ELENBTUUsT0FBT29ELENBQVAsRUFBVTtBQUNWLFdBQU8sS0FBUDtBQUNEO0FBQ0Y7O0FBRUQsU0FBU0wsVUFBVCxHQUF1QjtBQUNyQixTQUFPcEQsTUFBTSxDQUFDZ0QsbUJBQVAsR0FDSCxVQURHLEdBRUgsVUFGSjtBQUdEOztBQUVELFNBQVNVLFlBQVQsQ0FBdUJDLElBQXZCLEVBQTZCM0MsTUFBN0IsRUFBcUM7QUFDbkMsTUFBSW9DLFVBQVUsS0FBS3BDLE1BQW5CLEVBQTJCO0FBQ3pCLFVBQU0sSUFBSTRDLFVBQUosQ0FBZSw0QkFBZixDQUFOO0FBQ0Q7O0FBQ0QsTUFBSTVELE1BQU0sQ0FBQ2dELG1CQUFYLEVBQWdDO0FBQzlCO0FBQ0FXLFFBQUksR0FBRyxJQUFJaEQsVUFBSixDQUFlSyxNQUFmLENBQVA7QUFDQTJDLFFBQUksQ0FBQ04sU0FBTCxHQUFpQnJELE1BQU0sQ0FBQ3NELFNBQXhCO0FBQ0QsR0FKRCxNQUlPO0FBQ0w7QUFDQSxRQUFJSyxJQUFJLEtBQUssSUFBYixFQUFtQjtBQUNqQkEsVUFBSSxHQUFHLElBQUkzRCxNQUFKLENBQVdnQixNQUFYLENBQVA7QUFDRDs7QUFDRDJDLFFBQUksQ0FBQzNDLE1BQUwsR0FBY0EsTUFBZDtBQUNEOztBQUVELFNBQU8yQyxJQUFQO0FBQ0Q7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBLFNBQVMzRCxNQUFULENBQWlCNkQsR0FBakIsRUFBc0JDLGdCQUF0QixFQUF3QzlDLE1BQXhDLEVBQWdEO0FBQzlDLE1BQUksQ0FBQ2hCLE1BQU0sQ0FBQ2dELG1CQUFSLElBQStCLEVBQUUsZ0JBQWdCaEQsTUFBbEIsQ0FBbkMsRUFBOEQ7QUFDNUQsV0FBTyxJQUFJQSxNQUFKLENBQVc2RCxHQUFYLEVBQWdCQyxnQkFBaEIsRUFBa0M5QyxNQUFsQyxDQUFQO0FBQ0QsR0FINkMsQ0FLOUM7OztBQUNBLE1BQUksT0FBTzZDLEdBQVAsS0FBZSxRQUFuQixFQUE2QjtBQUMzQixRQUFJLE9BQU9DLGdCQUFQLEtBQTRCLFFBQWhDLEVBQTBDO0FBQ3hDLFlBQU0sSUFBSTFDLEtBQUosQ0FDSixtRUFESSxDQUFOO0FBR0Q7O0FBQ0QsV0FBTzJDLFdBQVcsQ0FBQyxJQUFELEVBQU9GLEdBQVAsQ0FBbEI7QUFDRDs7QUFDRCxTQUFPNUQsSUFBSSxDQUFDLElBQUQsRUFBTzRELEdBQVAsRUFBWUMsZ0JBQVosRUFBOEI5QyxNQUE5QixDQUFYO0FBQ0Q7O0FBRURoQixNQUFNLENBQUNnRSxRQUFQLEdBQWtCLElBQWxCLEMsQ0FBdUI7QUFFdkI7O0FBQ0FoRSxNQUFNLENBQUNpRSxRQUFQLEdBQWtCLFVBQVV0QyxHQUFWLEVBQWU7QUFDL0JBLEtBQUcsQ0FBQzBCLFNBQUosR0FBZ0JyRCxNQUFNLENBQUNzRCxTQUF2QjtBQUNBLFNBQU8zQixHQUFQO0FBQ0QsQ0FIRDs7QUFLQSxTQUFTMUIsSUFBVCxDQUFlMEQsSUFBZixFQUFxQk8sS0FBckIsRUFBNEJKLGdCQUE1QixFQUE4QzlDLE1BQTlDLEVBQXNEO0FBQ3BELE1BQUksT0FBT2tELEtBQVAsS0FBaUIsUUFBckIsRUFBK0I7QUFDN0IsVUFBTSxJQUFJQyxTQUFKLENBQWMsdUNBQWQsQ0FBTjtBQUNEOztBQUVELE1BQUksT0FBT0MsV0FBUCxLQUF1QixXQUF2QixJQUFzQ0YsS0FBSyxZQUFZRSxXQUEzRCxFQUF3RTtBQUN0RSxXQUFPQyxlQUFlLENBQUNWLElBQUQsRUFBT08sS0FBUCxFQUFjSixnQkFBZCxFQUFnQzlDLE1BQWhDLENBQXRCO0FBQ0Q7O0FBRUQsTUFBSSxPQUFPa0QsS0FBUCxLQUFpQixRQUFyQixFQUErQjtBQUM3QixXQUFPSSxVQUFVLENBQUNYLElBQUQsRUFBT08sS0FBUCxFQUFjSixnQkFBZCxDQUFqQjtBQUNEOztBQUVELFNBQU9TLFVBQVUsQ0FBQ1osSUFBRCxFQUFPTyxLQUFQLENBQWpCO0FBQ0Q7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQWxFLE1BQU0sQ0FBQ0MsSUFBUCxHQUFjLFVBQVVpRSxLQUFWLEVBQWlCSixnQkFBakIsRUFBbUM5QyxNQUFuQyxFQUEyQztBQUN2RCxTQUFPZixJQUFJLENBQUMsSUFBRCxFQUFPaUUsS0FBUCxFQUFjSixnQkFBZCxFQUFnQzlDLE1BQWhDLENBQVg7QUFDRCxDQUZEOztBQUlBLElBQUloQixNQUFNLENBQUNnRCxtQkFBWCxFQUFnQztBQUM5QmhELFFBQU0sQ0FBQ3NELFNBQVAsQ0FBaUJELFNBQWpCLEdBQTZCMUMsVUFBVSxDQUFDMkMsU0FBeEM7QUFDQXRELFFBQU0sQ0FBQ3FELFNBQVAsR0FBbUIxQyxVQUFuQjs7QUFDQSxNQUFJLE9BQU82RCxNQUFQLEtBQWtCLFdBQWxCLElBQWlDQSxNQUFNLENBQUNDLE9BQXhDLElBQ0F6RSxNQUFNLENBQUN3RSxNQUFNLENBQUNDLE9BQVIsQ0FBTixLQUEyQnpFLE1BRC9CLEVBQ3VDO0FBQ3JDO0FBQ0EwRSxVQUFNLENBQUNDLGNBQVAsQ0FBc0IzRSxNQUF0QixFQUE4QndFLE1BQU0sQ0FBQ0MsT0FBckMsRUFBOEM7QUFDNUNQLFdBQUssRUFBRSxJQURxQztBQUU1Q1Usa0JBQVksRUFBRTtBQUY4QixLQUE5QztBQUlEO0FBQ0Y7O0FBRUQsU0FBU0MsVUFBVCxDQUFxQkMsSUFBckIsRUFBMkI7QUFDekIsTUFBSSxPQUFPQSxJQUFQLEtBQWdCLFFBQXBCLEVBQThCO0FBQzVCLFVBQU0sSUFBSVgsU0FBSixDQUFjLGtDQUFkLENBQU47QUFDRCxHQUZELE1BRU8sSUFBSVcsSUFBSSxHQUFHLENBQVgsRUFBYztBQUNuQixVQUFNLElBQUlsQixVQUFKLENBQWUsc0NBQWYsQ0FBTjtBQUNEO0FBQ0Y7O0FBRUQsU0FBU21CLEtBQVQsQ0FBZ0JwQixJQUFoQixFQUFzQm1CLElBQXRCLEVBQTRCRSxJQUE1QixFQUFrQ0MsUUFBbEMsRUFBNEM7QUFDMUNKLFlBQVUsQ0FBQ0MsSUFBRCxDQUFWOztBQUNBLE1BQUlBLElBQUksSUFBSSxDQUFaLEVBQWU7QUFDYixXQUFPcEIsWUFBWSxDQUFDQyxJQUFELEVBQU9tQixJQUFQLENBQW5CO0FBQ0Q7O0FBQ0QsTUFBSUUsSUFBSSxLQUFLOUIsU0FBYixFQUF3QjtBQUN0QjtBQUNBO0FBQ0E7QUFDQSxXQUFPLE9BQU8rQixRQUFQLEtBQW9CLFFBQXBCLEdBQ0h2QixZQUFZLENBQUNDLElBQUQsRUFBT21CLElBQVAsQ0FBWixDQUF5QkUsSUFBekIsQ0FBOEJBLElBQTlCLEVBQW9DQyxRQUFwQyxDQURHLEdBRUh2QixZQUFZLENBQUNDLElBQUQsRUFBT21CLElBQVAsQ0FBWixDQUF5QkUsSUFBekIsQ0FBOEJBLElBQTlCLENBRko7QUFHRDs7QUFDRCxTQUFPdEIsWUFBWSxDQUFDQyxJQUFELEVBQU9tQixJQUFQLENBQW5CO0FBQ0Q7QUFFRDtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E5RSxNQUFNLENBQUMrRSxLQUFQLEdBQWUsVUFBVUQsSUFBVixFQUFnQkUsSUFBaEIsRUFBc0JDLFFBQXRCLEVBQWdDO0FBQzdDLFNBQU9GLEtBQUssQ0FBQyxJQUFELEVBQU9ELElBQVAsRUFBYUUsSUFBYixFQUFtQkMsUUFBbkIsQ0FBWjtBQUNELENBRkQ7O0FBSUEsU0FBU2xCLFdBQVQsQ0FBc0JKLElBQXRCLEVBQTRCbUIsSUFBNUIsRUFBa0M7QUFDaENELFlBQVUsQ0FBQ0MsSUFBRCxDQUFWO0FBQ0FuQixNQUFJLEdBQUdELFlBQVksQ0FBQ0MsSUFBRCxFQUFPbUIsSUFBSSxHQUFHLENBQVAsR0FBVyxDQUFYLEdBQWVJLE9BQU8sQ0FBQ0osSUFBRCxDQUFQLEdBQWdCLENBQXRDLENBQW5COztBQUNBLE1BQUksQ0FBQzlFLE1BQU0sQ0FBQ2dELG1CQUFaLEVBQWlDO0FBQy9CLFNBQUssSUFBSWxDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdnRSxJQUFwQixFQUEwQixFQUFFaEUsQ0FBNUIsRUFBK0I7QUFDN0I2QyxVQUFJLENBQUM3QyxDQUFELENBQUosR0FBVSxDQUFWO0FBQ0Q7QUFDRjs7QUFDRCxTQUFPNkMsSUFBUDtBQUNEO0FBRUQ7QUFDQTtBQUNBOzs7QUFDQTNELE1BQU0sQ0FBQytELFdBQVAsR0FBcUIsVUFBVWUsSUFBVixFQUFnQjtBQUNuQyxTQUFPZixXQUFXLENBQUMsSUFBRCxFQUFPZSxJQUFQLENBQWxCO0FBQ0QsQ0FGRDtBQUdBO0FBQ0E7QUFDQTs7O0FBQ0E5RSxNQUFNLENBQUNtRixlQUFQLEdBQXlCLFVBQVVMLElBQVYsRUFBZ0I7QUFDdkMsU0FBT2YsV0FBVyxDQUFDLElBQUQsRUFBT2UsSUFBUCxDQUFsQjtBQUNELENBRkQ7O0FBSUEsU0FBU1IsVUFBVCxDQUFxQlgsSUFBckIsRUFBMkJ5QixNQUEzQixFQUFtQ0gsUUFBbkMsRUFBNkM7QUFDM0MsTUFBSSxPQUFPQSxRQUFQLEtBQW9CLFFBQXBCLElBQWdDQSxRQUFRLEtBQUssRUFBakQsRUFBcUQ7QUFDbkRBLFlBQVEsR0FBRyxNQUFYO0FBQ0Q7O0FBRUQsTUFBSSxDQUFDakYsTUFBTSxDQUFDcUYsVUFBUCxDQUFrQkosUUFBbEIsQ0FBTCxFQUFrQztBQUNoQyxVQUFNLElBQUlkLFNBQUosQ0FBYyw0Q0FBZCxDQUFOO0FBQ0Q7O0FBRUQsTUFBSW5ELE1BQU0sR0FBR1gsVUFBVSxDQUFDK0UsTUFBRCxFQUFTSCxRQUFULENBQVYsR0FBK0IsQ0FBNUM7QUFDQXRCLE1BQUksR0FBR0QsWUFBWSxDQUFDQyxJQUFELEVBQU8zQyxNQUFQLENBQW5CO0FBRUEsTUFBSXNFLE1BQU0sR0FBRzNCLElBQUksQ0FBQzRCLEtBQUwsQ0FBV0gsTUFBWCxFQUFtQkgsUUFBbkIsQ0FBYjs7QUFFQSxNQUFJSyxNQUFNLEtBQUt0RSxNQUFmLEVBQXVCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBMkMsUUFBSSxHQUFHQSxJQUFJLENBQUM2QixLQUFMLENBQVcsQ0FBWCxFQUFjRixNQUFkLENBQVA7QUFDRDs7QUFFRCxTQUFPM0IsSUFBUDtBQUNEOztBQUVELFNBQVM4QixhQUFULENBQXdCOUIsSUFBeEIsRUFBOEIrQixLQUE5QixFQUFxQztBQUNuQyxNQUFJMUUsTUFBTSxHQUFHMEUsS0FBSyxDQUFDMUUsTUFBTixHQUFlLENBQWYsR0FBbUIsQ0FBbkIsR0FBdUJrRSxPQUFPLENBQUNRLEtBQUssQ0FBQzFFLE1BQVAsQ0FBUCxHQUF3QixDQUE1RDtBQUNBMkMsTUFBSSxHQUFHRCxZQUFZLENBQUNDLElBQUQsRUFBTzNDLE1BQVAsQ0FBbkI7O0FBQ0EsT0FBSyxJQUFJRixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHRSxNQUFwQixFQUE0QkYsQ0FBQyxJQUFJLENBQWpDLEVBQW9DO0FBQ2xDNkMsUUFBSSxDQUFDN0MsQ0FBRCxDQUFKLEdBQVU0RSxLQUFLLENBQUM1RSxDQUFELENBQUwsR0FBVyxHQUFyQjtBQUNEOztBQUNELFNBQU82QyxJQUFQO0FBQ0Q7O0FBRUQsU0FBU1UsZUFBVCxDQUEwQlYsSUFBMUIsRUFBZ0MrQixLQUFoQyxFQUF1Q0MsVUFBdkMsRUFBbUQzRSxNQUFuRCxFQUEyRDtBQUN6RDBFLE9BQUssQ0FBQ3JGLFVBQU4sQ0FEeUQsQ0FDeEM7O0FBRWpCLE1BQUlzRixVQUFVLEdBQUcsQ0FBYixJQUFrQkQsS0FBSyxDQUFDckYsVUFBTixHQUFtQnNGLFVBQXpDLEVBQXFEO0FBQ25ELFVBQU0sSUFBSS9CLFVBQUosQ0FBZSw2QkFBZixDQUFOO0FBQ0Q7O0FBRUQsTUFBSThCLEtBQUssQ0FBQ3JGLFVBQU4sR0FBbUJzRixVQUFVLElBQUkzRSxNQUFNLElBQUksQ0FBZCxDQUFqQyxFQUFtRDtBQUNqRCxVQUFNLElBQUk0QyxVQUFKLENBQWUsNkJBQWYsQ0FBTjtBQUNEOztBQUVELE1BQUkrQixVQUFVLEtBQUt6QyxTQUFmLElBQTRCbEMsTUFBTSxLQUFLa0MsU0FBM0MsRUFBc0Q7QUFDcER3QyxTQUFLLEdBQUcsSUFBSS9FLFVBQUosQ0FBZStFLEtBQWYsQ0FBUjtBQUNELEdBRkQsTUFFTyxJQUFJMUUsTUFBTSxLQUFLa0MsU0FBZixFQUEwQjtBQUMvQndDLFNBQUssR0FBRyxJQUFJL0UsVUFBSixDQUFlK0UsS0FBZixFQUFzQkMsVUFBdEIsQ0FBUjtBQUNELEdBRk0sTUFFQTtBQUNMRCxTQUFLLEdBQUcsSUFBSS9FLFVBQUosQ0FBZStFLEtBQWYsRUFBc0JDLFVBQXRCLEVBQWtDM0UsTUFBbEMsQ0FBUjtBQUNEOztBQUVELE1BQUloQixNQUFNLENBQUNnRCxtQkFBWCxFQUFnQztBQUM5QjtBQUNBVyxRQUFJLEdBQUcrQixLQUFQO0FBQ0EvQixRQUFJLENBQUNOLFNBQUwsR0FBaUJyRCxNQUFNLENBQUNzRCxTQUF4QjtBQUNELEdBSkQsTUFJTztBQUNMO0FBQ0FLLFFBQUksR0FBRzhCLGFBQWEsQ0FBQzlCLElBQUQsRUFBTytCLEtBQVAsQ0FBcEI7QUFDRDs7QUFDRCxTQUFPL0IsSUFBUDtBQUNEOztBQUVELFNBQVNZLFVBQVQsQ0FBcUJaLElBQXJCLEVBQTJCaUMsR0FBM0IsRUFBZ0M7QUFDOUIsTUFBSTVGLE1BQU0sQ0FBQzZGLFFBQVAsQ0FBZ0JELEdBQWhCLENBQUosRUFBMEI7QUFDeEIsUUFBSTdFLEdBQUcsR0FBR21FLE9BQU8sQ0FBQ1UsR0FBRyxDQUFDNUUsTUFBTCxDQUFQLEdBQXNCLENBQWhDO0FBQ0EyQyxRQUFJLEdBQUdELFlBQVksQ0FBQ0MsSUFBRCxFQUFPNUMsR0FBUCxDQUFuQjs7QUFFQSxRQUFJNEMsSUFBSSxDQUFDM0MsTUFBTCxLQUFnQixDQUFwQixFQUF1QjtBQUNyQixhQUFPMkMsSUFBUDtBQUNEOztBQUVEaUMsT0FBRyxDQUFDRSxJQUFKLENBQVNuQyxJQUFULEVBQWUsQ0FBZixFQUFrQixDQUFsQixFQUFxQjVDLEdBQXJCO0FBQ0EsV0FBTzRDLElBQVA7QUFDRDs7QUFFRCxNQUFJaUMsR0FBSixFQUFTO0FBQ1AsUUFBSyxPQUFPeEIsV0FBUCxLQUF1QixXQUF2QixJQUNEd0IsR0FBRyxDQUFDRyxNQUFKLFlBQXNCM0IsV0FEdEIsSUFDc0MsWUFBWXdCLEdBRHRELEVBQzJEO0FBQ3pELFVBQUksT0FBT0EsR0FBRyxDQUFDNUUsTUFBWCxLQUFzQixRQUF0QixJQUFrQ2dGLEtBQUssQ0FBQ0osR0FBRyxDQUFDNUUsTUFBTCxDQUEzQyxFQUF5RDtBQUN2RCxlQUFPMEMsWUFBWSxDQUFDQyxJQUFELEVBQU8sQ0FBUCxDQUFuQjtBQUNEOztBQUNELGFBQU84QixhQUFhLENBQUM5QixJQUFELEVBQU9pQyxHQUFQLENBQXBCO0FBQ0Q7O0FBRUQsUUFBSUEsR0FBRyxDQUFDSyxJQUFKLEtBQWEsUUFBYixJQUF5QnBELE9BQU8sQ0FBQytDLEdBQUcsQ0FBQ00sSUFBTCxDQUFwQyxFQUFnRDtBQUM5QyxhQUFPVCxhQUFhLENBQUM5QixJQUFELEVBQU9pQyxHQUFHLENBQUNNLElBQVgsQ0FBcEI7QUFDRDtBQUNGOztBQUVELFFBQU0sSUFBSS9CLFNBQUosQ0FBYyxvRkFBZCxDQUFOO0FBQ0Q7O0FBRUQsU0FBU2UsT0FBVCxDQUFrQmxFLE1BQWxCLEVBQTBCO0FBQ3hCO0FBQ0E7QUFDQSxNQUFJQSxNQUFNLElBQUlvQyxVQUFVLEVBQXhCLEVBQTRCO0FBQzFCLFVBQU0sSUFBSVEsVUFBSixDQUFlLG9EQUNBLFVBREEsR0FDYVIsVUFBVSxHQUFHbEQsUUFBYixDQUFzQixFQUF0QixDQURiLEdBQ3lDLFFBRHhELENBQU47QUFFRDs7QUFDRCxTQUFPYyxNQUFNLEdBQUcsQ0FBaEI7QUFDRDs7QUFFRCxTQUFTOEIsVUFBVCxDQUFxQjlCLE1BQXJCLEVBQTZCO0FBQzNCLE1BQUksQ0FBQ0EsTUFBRCxJQUFXQSxNQUFmLEVBQXVCO0FBQUU7QUFDdkJBLFVBQU0sR0FBRyxDQUFUO0FBQ0Q7O0FBQ0QsU0FBT2hCLE1BQU0sQ0FBQytFLEtBQVAsQ0FBYSxDQUFDL0QsTUFBZCxDQUFQO0FBQ0Q7O0FBRURoQixNQUFNLENBQUM2RixRQUFQLEdBQWtCLFNBQVNBLFFBQVQsQ0FBbUJNLENBQW5CLEVBQXNCO0FBQ3RDLFNBQU8sQ0FBQyxFQUFFQSxDQUFDLElBQUksSUFBTCxJQUFhQSxDQUFDLENBQUNDLFNBQWpCLENBQVI7QUFDRCxDQUZEOztBQUlBcEcsTUFBTSxDQUFDcUcsT0FBUCxHQUFpQixTQUFTQSxPQUFULENBQWtCQyxDQUFsQixFQUFxQkgsQ0FBckIsRUFBd0I7QUFDdkMsTUFBSSxDQUFDbkcsTUFBTSxDQUFDNkYsUUFBUCxDQUFnQlMsQ0FBaEIsQ0FBRCxJQUF1QixDQUFDdEcsTUFBTSxDQUFDNkYsUUFBUCxDQUFnQk0sQ0FBaEIsQ0FBNUIsRUFBZ0Q7QUFDOUMsVUFBTSxJQUFJaEMsU0FBSixDQUFjLDJCQUFkLENBQU47QUFDRDs7QUFFRCxNQUFJbUMsQ0FBQyxLQUFLSCxDQUFWLEVBQWEsT0FBTyxDQUFQO0FBRWIsTUFBSUksQ0FBQyxHQUFHRCxDQUFDLENBQUN0RixNQUFWO0FBQ0EsTUFBSXdGLENBQUMsR0FBR0wsQ0FBQyxDQUFDbkYsTUFBVjs7QUFFQSxPQUFLLElBQUlGLENBQUMsR0FBRyxDQUFSLEVBQVdDLEdBQUcsR0FBRzBGLElBQUksQ0FBQ0MsR0FBTCxDQUFTSCxDQUFULEVBQVlDLENBQVosQ0FBdEIsRUFBc0MxRixDQUFDLEdBQUdDLEdBQTFDLEVBQStDLEVBQUVELENBQWpELEVBQW9EO0FBQ2xELFFBQUl3RixDQUFDLENBQUN4RixDQUFELENBQUQsS0FBU3FGLENBQUMsQ0FBQ3JGLENBQUQsQ0FBZCxFQUFtQjtBQUNqQnlGLE9BQUMsR0FBR0QsQ0FBQyxDQUFDeEYsQ0FBRCxDQUFMO0FBQ0EwRixPQUFDLEdBQUdMLENBQUMsQ0FBQ3JGLENBQUQsQ0FBTDtBQUNBO0FBQ0Q7QUFDRjs7QUFFRCxNQUFJeUYsQ0FBQyxHQUFHQyxDQUFSLEVBQVcsT0FBTyxDQUFDLENBQVI7QUFDWCxNQUFJQSxDQUFDLEdBQUdELENBQVIsRUFBVyxPQUFPLENBQVA7QUFDWCxTQUFPLENBQVA7QUFDRCxDQXJCRDs7QUF1QkF2RyxNQUFNLENBQUNxRixVQUFQLEdBQW9CLFNBQVNBLFVBQVQsQ0FBcUJKLFFBQXJCLEVBQStCO0FBQ2pELFVBQVEwQixNQUFNLENBQUMxQixRQUFELENBQU4sQ0FBaUIyQixXQUFqQixFQUFSO0FBQ0UsU0FBSyxLQUFMO0FBQ0EsU0FBSyxNQUFMO0FBQ0EsU0FBSyxPQUFMO0FBQ0EsU0FBSyxPQUFMO0FBQ0EsU0FBSyxRQUFMO0FBQ0EsU0FBSyxRQUFMO0FBQ0EsU0FBSyxRQUFMO0FBQ0EsU0FBSyxNQUFMO0FBQ0EsU0FBSyxPQUFMO0FBQ0EsU0FBSyxTQUFMO0FBQ0EsU0FBSyxVQUFMO0FBQ0UsYUFBTyxJQUFQOztBQUNGO0FBQ0UsYUFBTyxLQUFQO0FBZEo7QUFnQkQsQ0FqQkQ7O0FBbUJBNUcsTUFBTSxDQUFDNkcsTUFBUCxHQUFnQixTQUFTQSxNQUFULENBQWlCQyxJQUFqQixFQUF1QjlGLE1BQXZCLEVBQStCO0FBQzdDLE1BQUksQ0FBQzZCLE9BQU8sQ0FBQ2lFLElBQUQsQ0FBWixFQUFvQjtBQUNsQixVQUFNLElBQUkzQyxTQUFKLENBQWMsNkNBQWQsQ0FBTjtBQUNEOztBQUVELE1BQUkyQyxJQUFJLENBQUM5RixNQUFMLEtBQWdCLENBQXBCLEVBQXVCO0FBQ3JCLFdBQU9oQixNQUFNLENBQUMrRSxLQUFQLENBQWEsQ0FBYixDQUFQO0FBQ0Q7O0FBRUQsTUFBSWpFLENBQUo7O0FBQ0EsTUFBSUUsTUFBTSxLQUFLa0MsU0FBZixFQUEwQjtBQUN4QmxDLFVBQU0sR0FBRyxDQUFUOztBQUNBLFNBQUtGLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBR2dHLElBQUksQ0FBQzlGLE1BQXJCLEVBQTZCLEVBQUVGLENBQS9CLEVBQWtDO0FBQ2hDRSxZQUFNLElBQUk4RixJQUFJLENBQUNoRyxDQUFELENBQUosQ0FBUUUsTUFBbEI7QUFDRDtBQUNGOztBQUVELE1BQUkrRSxNQUFNLEdBQUcvRixNQUFNLENBQUMrRCxXQUFQLENBQW1CL0MsTUFBbkIsQ0FBYjtBQUNBLE1BQUkrRixHQUFHLEdBQUcsQ0FBVjs7QUFDQSxPQUFLakcsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHZ0csSUFBSSxDQUFDOUYsTUFBckIsRUFBNkIsRUFBRUYsQ0FBL0IsRUFBa0M7QUFDaEMsUUFBSWtHLEdBQUcsR0FBR0YsSUFBSSxDQUFDaEcsQ0FBRCxDQUFkOztBQUNBLFFBQUksQ0FBQ2QsTUFBTSxDQUFDNkYsUUFBUCxDQUFnQm1CLEdBQWhCLENBQUwsRUFBMkI7QUFDekIsWUFBTSxJQUFJN0MsU0FBSixDQUFjLDZDQUFkLENBQU47QUFDRDs7QUFDRDZDLE9BQUcsQ0FBQ2xCLElBQUosQ0FBU0MsTUFBVCxFQUFpQmdCLEdBQWpCO0FBQ0FBLE9BQUcsSUFBSUMsR0FBRyxDQUFDaEcsTUFBWDtBQUNEOztBQUNELFNBQU8rRSxNQUFQO0FBQ0QsQ0E1QkQ7O0FBOEJBLFNBQVMxRixVQUFULENBQXFCK0UsTUFBckIsRUFBNkJILFFBQTdCLEVBQXVDO0FBQ3JDLE1BQUlqRixNQUFNLENBQUM2RixRQUFQLENBQWdCVCxNQUFoQixDQUFKLEVBQTZCO0FBQzNCLFdBQU9BLE1BQU0sQ0FBQ3BFLE1BQWQ7QUFDRDs7QUFDRCxNQUFJLE9BQU9vRCxXQUFQLEtBQXVCLFdBQXZCLElBQXNDLE9BQU9BLFdBQVcsQ0FBQzZDLE1BQW5CLEtBQThCLFVBQXBFLEtBQ0M3QyxXQUFXLENBQUM2QyxNQUFaLENBQW1CN0IsTUFBbkIsS0FBOEJBLE1BQU0sWUFBWWhCLFdBRGpELENBQUosRUFDbUU7QUFDakUsV0FBT2dCLE1BQU0sQ0FBQy9FLFVBQWQ7QUFDRDs7QUFDRCxNQUFJLE9BQU8rRSxNQUFQLEtBQWtCLFFBQXRCLEVBQWdDO0FBQzlCQSxVQUFNLEdBQUcsS0FBS0EsTUFBZDtBQUNEOztBQUVELE1BQUlyRSxHQUFHLEdBQUdxRSxNQUFNLENBQUNwRSxNQUFqQjtBQUNBLE1BQUlELEdBQUcsS0FBSyxDQUFaLEVBQWUsT0FBTyxDQUFQLENBYnNCLENBZXJDOztBQUNBLE1BQUltRyxXQUFXLEdBQUcsS0FBbEI7O0FBQ0EsV0FBUztBQUNQLFlBQVFqQyxRQUFSO0FBQ0UsV0FBSyxPQUFMO0FBQ0EsV0FBSyxRQUFMO0FBQ0EsV0FBSyxRQUFMO0FBQ0UsZUFBT2xFLEdBQVA7O0FBQ0YsV0FBSyxNQUFMO0FBQ0EsV0FBSyxPQUFMO0FBQ0EsV0FBS21DLFNBQUw7QUFDRSxlQUFPaUUsV0FBVyxDQUFDL0IsTUFBRCxDQUFYLENBQW9CcEUsTUFBM0I7O0FBQ0YsV0FBSyxNQUFMO0FBQ0EsV0FBSyxPQUFMO0FBQ0EsV0FBSyxTQUFMO0FBQ0EsV0FBSyxVQUFMO0FBQ0UsZUFBT0QsR0FBRyxHQUFHLENBQWI7O0FBQ0YsV0FBSyxLQUFMO0FBQ0UsZUFBT0EsR0FBRyxLQUFLLENBQWY7O0FBQ0YsV0FBSyxRQUFMO0FBQ0UsZUFBT3FHLGFBQWEsQ0FBQ2hDLE1BQUQsQ0FBYixDQUFzQnBFLE1BQTdCOztBQUNGO0FBQ0UsWUFBSWtHLFdBQUosRUFBaUIsT0FBT0MsV0FBVyxDQUFDL0IsTUFBRCxDQUFYLENBQW9CcEUsTUFBM0IsQ0FEbkIsQ0FDcUQ7O0FBQ25EaUUsZ0JBQVEsR0FBRyxDQUFDLEtBQUtBLFFBQU4sRUFBZ0IyQixXQUFoQixFQUFYO0FBQ0FNLG1CQUFXLEdBQUcsSUFBZDtBQXJCSjtBQXVCRDtBQUNGOztBQUNEbEgsTUFBTSxDQUFDSyxVQUFQLEdBQW9CQSxVQUFwQjs7QUFFQSxTQUFTZ0gsWUFBVCxDQUF1QnBDLFFBQXZCLEVBQWlDaEQsS0FBakMsRUFBd0NDLEdBQXhDLEVBQTZDO0FBQzNDLE1BQUlnRixXQUFXLEdBQUcsS0FBbEIsQ0FEMkMsQ0FHM0M7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLE1BQUlqRixLQUFLLEtBQUtpQixTQUFWLElBQXVCakIsS0FBSyxHQUFHLENBQW5DLEVBQXNDO0FBQ3BDQSxTQUFLLEdBQUcsQ0FBUjtBQUNELEdBWjBDLENBYTNDO0FBQ0E7OztBQUNBLE1BQUlBLEtBQUssR0FBRyxLQUFLakIsTUFBakIsRUFBeUI7QUFDdkIsV0FBTyxFQUFQO0FBQ0Q7O0FBRUQsTUFBSWtCLEdBQUcsS0FBS2dCLFNBQVIsSUFBcUJoQixHQUFHLEdBQUcsS0FBS2xCLE1BQXBDLEVBQTRDO0FBQzFDa0IsT0FBRyxHQUFHLEtBQUtsQixNQUFYO0FBQ0Q7O0FBRUQsTUFBSWtCLEdBQUcsSUFBSSxDQUFYLEVBQWM7QUFDWixXQUFPLEVBQVA7QUFDRCxHQXpCMEMsQ0EyQjNDOzs7QUFDQUEsS0FBRyxNQUFNLENBQVQ7QUFDQUQsT0FBSyxNQUFNLENBQVg7O0FBRUEsTUFBSUMsR0FBRyxJQUFJRCxLQUFYLEVBQWtCO0FBQ2hCLFdBQU8sRUFBUDtBQUNEOztBQUVELE1BQUksQ0FBQ2dELFFBQUwsRUFBZUEsUUFBUSxHQUFHLE1BQVg7O0FBRWYsU0FBTyxJQUFQLEVBQWE7QUFDWCxZQUFRQSxRQUFSO0FBQ0UsV0FBSyxLQUFMO0FBQ0UsZUFBT3FDLFFBQVEsQ0FBQyxJQUFELEVBQU9yRixLQUFQLEVBQWNDLEdBQWQsQ0FBZjs7QUFFRixXQUFLLE1BQUw7QUFDQSxXQUFLLE9BQUw7QUFDRSxlQUFPcUYsU0FBUyxDQUFDLElBQUQsRUFBT3RGLEtBQVAsRUFBY0MsR0FBZCxDQUFoQjs7QUFFRixXQUFLLE9BQUw7QUFDRSxlQUFPc0YsVUFBVSxDQUFDLElBQUQsRUFBT3ZGLEtBQVAsRUFBY0MsR0FBZCxDQUFqQjs7QUFFRixXQUFLLFFBQUw7QUFDQSxXQUFLLFFBQUw7QUFDRSxlQUFPdUYsV0FBVyxDQUFDLElBQUQsRUFBT3hGLEtBQVAsRUFBY0MsR0FBZCxDQUFsQjs7QUFFRixXQUFLLFFBQUw7QUFDRSxlQUFPd0YsV0FBVyxDQUFDLElBQUQsRUFBT3pGLEtBQVAsRUFBY0MsR0FBZCxDQUFsQjs7QUFFRixXQUFLLE1BQUw7QUFDQSxXQUFLLE9BQUw7QUFDQSxXQUFLLFNBQUw7QUFDQSxXQUFLLFVBQUw7QUFDRSxlQUFPeUYsWUFBWSxDQUFDLElBQUQsRUFBTzFGLEtBQVAsRUFBY0MsR0FBZCxDQUFuQjs7QUFFRjtBQUNFLFlBQUlnRixXQUFKLEVBQWlCLE1BQU0sSUFBSS9DLFNBQUosQ0FBYyx1QkFBdUJjLFFBQXJDLENBQU47QUFDakJBLGdCQUFRLEdBQUcsQ0FBQ0EsUUFBUSxHQUFHLEVBQVosRUFBZ0IyQixXQUFoQixFQUFYO0FBQ0FNLG1CQUFXLEdBQUcsSUFBZDtBQTNCSjtBQTZCRDtBQUNGLEMsQ0FFRDtBQUNBOzs7QUFDQWxILE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUI4QyxTQUFqQixHQUE2QixJQUE3Qjs7QUFFQSxTQUFTd0IsSUFBVCxDQUFlekIsQ0FBZixFQUFrQjBCLENBQWxCLEVBQXFCQyxDQUFyQixFQUF3QjtBQUN0QixNQUFJaEgsQ0FBQyxHQUFHcUYsQ0FBQyxDQUFDMEIsQ0FBRCxDQUFUO0FBQ0ExQixHQUFDLENBQUMwQixDQUFELENBQUQsR0FBTzFCLENBQUMsQ0FBQzJCLENBQUQsQ0FBUjtBQUNBM0IsR0FBQyxDQUFDMkIsQ0FBRCxDQUFELEdBQU9oSCxDQUFQO0FBQ0Q7O0FBRURkLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJ5RSxNQUFqQixHQUEwQixTQUFTQSxNQUFULEdBQW1CO0FBQzNDLE1BQUloSCxHQUFHLEdBQUcsS0FBS0MsTUFBZjs7QUFDQSxNQUFJRCxHQUFHLEdBQUcsQ0FBTixLQUFZLENBQWhCLEVBQW1CO0FBQ2pCLFVBQU0sSUFBSTZDLFVBQUosQ0FBZSwyQ0FBZixDQUFOO0FBQ0Q7O0FBQ0QsT0FBSyxJQUFJOUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0MsR0FBcEIsRUFBeUJELENBQUMsSUFBSSxDQUE5QixFQUFpQztBQUMvQjhHLFFBQUksQ0FBQyxJQUFELEVBQU85RyxDQUFQLEVBQVVBLENBQUMsR0FBRyxDQUFkLENBQUo7QUFDRDs7QUFDRCxTQUFPLElBQVA7QUFDRCxDQVREOztBQVdBZCxNQUFNLENBQUNzRCxTQUFQLENBQWlCMEUsTUFBakIsR0FBMEIsU0FBU0EsTUFBVCxHQUFtQjtBQUMzQyxNQUFJakgsR0FBRyxHQUFHLEtBQUtDLE1BQWY7O0FBQ0EsTUFBSUQsR0FBRyxHQUFHLENBQU4sS0FBWSxDQUFoQixFQUFtQjtBQUNqQixVQUFNLElBQUk2QyxVQUFKLENBQWUsMkNBQWYsQ0FBTjtBQUNEOztBQUNELE9BQUssSUFBSTlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdDLEdBQXBCLEVBQXlCRCxDQUFDLElBQUksQ0FBOUIsRUFBaUM7QUFDL0I4RyxRQUFJLENBQUMsSUFBRCxFQUFPOUcsQ0FBUCxFQUFVQSxDQUFDLEdBQUcsQ0FBZCxDQUFKO0FBQ0E4RyxRQUFJLENBQUMsSUFBRCxFQUFPOUcsQ0FBQyxHQUFHLENBQVgsRUFBY0EsQ0FBQyxHQUFHLENBQWxCLENBQUo7QUFDRDs7QUFDRCxTQUFPLElBQVA7QUFDRCxDQVZEOztBQVlBZCxNQUFNLENBQUNzRCxTQUFQLENBQWlCMkUsTUFBakIsR0FBMEIsU0FBU0EsTUFBVCxHQUFtQjtBQUMzQyxNQUFJbEgsR0FBRyxHQUFHLEtBQUtDLE1BQWY7O0FBQ0EsTUFBSUQsR0FBRyxHQUFHLENBQU4sS0FBWSxDQUFoQixFQUFtQjtBQUNqQixVQUFNLElBQUk2QyxVQUFKLENBQWUsMkNBQWYsQ0FBTjtBQUNEOztBQUNELE9BQUssSUFBSTlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdDLEdBQXBCLEVBQXlCRCxDQUFDLElBQUksQ0FBOUIsRUFBaUM7QUFDL0I4RyxRQUFJLENBQUMsSUFBRCxFQUFPOUcsQ0FBUCxFQUFVQSxDQUFDLEdBQUcsQ0FBZCxDQUFKO0FBQ0E4RyxRQUFJLENBQUMsSUFBRCxFQUFPOUcsQ0FBQyxHQUFHLENBQVgsRUFBY0EsQ0FBQyxHQUFHLENBQWxCLENBQUo7QUFDQThHLFFBQUksQ0FBQyxJQUFELEVBQU85RyxDQUFDLEdBQUcsQ0FBWCxFQUFjQSxDQUFDLEdBQUcsQ0FBbEIsQ0FBSjtBQUNBOEcsUUFBSSxDQUFDLElBQUQsRUFBTzlHLENBQUMsR0FBRyxDQUFYLEVBQWNBLENBQUMsR0FBRyxDQUFsQixDQUFKO0FBQ0Q7O0FBQ0QsU0FBTyxJQUFQO0FBQ0QsQ0FaRDs7QUFjQWQsTUFBTSxDQUFDc0QsU0FBUCxDQUFpQnBELFFBQWpCLEdBQTRCLFNBQVNBLFFBQVQsR0FBcUI7QUFDL0MsTUFBSWMsTUFBTSxHQUFHLEtBQUtBLE1BQUwsR0FBYyxDQUEzQjtBQUNBLE1BQUlBLE1BQU0sS0FBSyxDQUFmLEVBQWtCLE9BQU8sRUFBUDtBQUNsQixNQUFJa0gsU0FBUyxDQUFDbEgsTUFBVixLQUFxQixDQUF6QixFQUE0QixPQUFPdUcsU0FBUyxDQUFDLElBQUQsRUFBTyxDQUFQLEVBQVV2RyxNQUFWLENBQWhCO0FBQzVCLFNBQU9xRyxZQUFZLENBQUNjLEtBQWIsQ0FBbUIsSUFBbkIsRUFBeUJELFNBQXpCLENBQVA7QUFDRCxDQUxEOztBQU9BbEksTUFBTSxDQUFDc0QsU0FBUCxDQUFpQjhFLE1BQWpCLEdBQTBCLFNBQVNBLE1BQVQsQ0FBaUJqQyxDQUFqQixFQUFvQjtBQUM1QyxNQUFJLENBQUNuRyxNQUFNLENBQUM2RixRQUFQLENBQWdCTSxDQUFoQixDQUFMLEVBQXlCLE1BQU0sSUFBSWhDLFNBQUosQ0FBYywyQkFBZCxDQUFOO0FBQ3pCLE1BQUksU0FBU2dDLENBQWIsRUFBZ0IsT0FBTyxJQUFQO0FBQ2hCLFNBQU9uRyxNQUFNLENBQUNxRyxPQUFQLENBQWUsSUFBZixFQUFxQkYsQ0FBckIsTUFBNEIsQ0FBbkM7QUFDRCxDQUpEOztBQU1BbkcsTUFBTSxDQUFDc0QsU0FBUCxDQUFpQitFLE9BQWpCLEdBQTJCLFNBQVNBLE9BQVQsR0FBb0I7QUFDN0MsTUFBSXRJLEdBQUcsR0FBRyxFQUFWO0FBQ0EsTUFBSXVJLEdBQUcsR0FBR2xJLE9BQU8sQ0FBQzJDLGlCQUFsQjs7QUFDQSxNQUFJLEtBQUsvQixNQUFMLEdBQWMsQ0FBbEIsRUFBcUI7QUFDbkJqQixPQUFHLEdBQUcsS0FBS0csUUFBTCxDQUFjLEtBQWQsRUFBcUIsQ0FBckIsRUFBd0JvSSxHQUF4QixFQUE2QkMsS0FBN0IsQ0FBbUMsT0FBbkMsRUFBNENsRyxJQUE1QyxDQUFpRCxHQUFqRCxDQUFOO0FBQ0EsUUFBSSxLQUFLckIsTUFBTCxHQUFjc0gsR0FBbEIsRUFBdUJ2SSxHQUFHLElBQUksT0FBUDtBQUN4Qjs7QUFDRCxTQUFPLGFBQWFBLEdBQWIsR0FBbUIsR0FBMUI7QUFDRCxDQVJEOztBQVVBQyxNQUFNLENBQUNzRCxTQUFQLENBQWlCK0MsT0FBakIsR0FBMkIsU0FBU0EsT0FBVCxDQUFrQm1DLE1BQWxCLEVBQTBCdkcsS0FBMUIsRUFBaUNDLEdBQWpDLEVBQXNDdUcsU0FBdEMsRUFBaURDLE9BQWpELEVBQTBEO0FBQ25GLE1BQUksQ0FBQzFJLE1BQU0sQ0FBQzZGLFFBQVAsQ0FBZ0IyQyxNQUFoQixDQUFMLEVBQThCO0FBQzVCLFVBQU0sSUFBSXJFLFNBQUosQ0FBYywyQkFBZCxDQUFOO0FBQ0Q7O0FBRUQsTUFBSWxDLEtBQUssS0FBS2lCLFNBQWQsRUFBeUI7QUFDdkJqQixTQUFLLEdBQUcsQ0FBUjtBQUNEOztBQUNELE1BQUlDLEdBQUcsS0FBS2dCLFNBQVosRUFBdUI7QUFDckJoQixPQUFHLEdBQUdzRyxNQUFNLEdBQUdBLE1BQU0sQ0FBQ3hILE1BQVYsR0FBbUIsQ0FBL0I7QUFDRDs7QUFDRCxNQUFJeUgsU0FBUyxLQUFLdkYsU0FBbEIsRUFBNkI7QUFDM0J1RixhQUFTLEdBQUcsQ0FBWjtBQUNEOztBQUNELE1BQUlDLE9BQU8sS0FBS3hGLFNBQWhCLEVBQTJCO0FBQ3pCd0YsV0FBTyxHQUFHLEtBQUsxSCxNQUFmO0FBQ0Q7O0FBRUQsTUFBSWlCLEtBQUssR0FBRyxDQUFSLElBQWFDLEdBQUcsR0FBR3NHLE1BQU0sQ0FBQ3hILE1BQTFCLElBQW9DeUgsU0FBUyxHQUFHLENBQWhELElBQXFEQyxPQUFPLEdBQUcsS0FBSzFILE1BQXhFLEVBQWdGO0FBQzlFLFVBQU0sSUFBSTRDLFVBQUosQ0FBZSxvQkFBZixDQUFOO0FBQ0Q7O0FBRUQsTUFBSTZFLFNBQVMsSUFBSUMsT0FBYixJQUF3QnpHLEtBQUssSUFBSUMsR0FBckMsRUFBMEM7QUFDeEMsV0FBTyxDQUFQO0FBQ0Q7O0FBQ0QsTUFBSXVHLFNBQVMsSUFBSUMsT0FBakIsRUFBMEI7QUFDeEIsV0FBTyxDQUFDLENBQVI7QUFDRDs7QUFDRCxNQUFJekcsS0FBSyxJQUFJQyxHQUFiLEVBQWtCO0FBQ2hCLFdBQU8sQ0FBUDtBQUNEOztBQUVERCxPQUFLLE1BQU0sQ0FBWDtBQUNBQyxLQUFHLE1BQU0sQ0FBVDtBQUNBdUcsV0FBUyxNQUFNLENBQWY7QUFDQUMsU0FBTyxNQUFNLENBQWI7QUFFQSxNQUFJLFNBQVNGLE1BQWIsRUFBcUIsT0FBTyxDQUFQO0FBRXJCLE1BQUlqQyxDQUFDLEdBQUdtQyxPQUFPLEdBQUdELFNBQWxCO0FBQ0EsTUFBSWpDLENBQUMsR0FBR3RFLEdBQUcsR0FBR0QsS0FBZDtBQUNBLE1BQUlsQixHQUFHLEdBQUcwRixJQUFJLENBQUNDLEdBQUwsQ0FBU0gsQ0FBVCxFQUFZQyxDQUFaLENBQVY7QUFFQSxNQUFJbUMsUUFBUSxHQUFHLEtBQUtuRCxLQUFMLENBQVdpRCxTQUFYLEVBQXNCQyxPQUF0QixDQUFmO0FBQ0EsTUFBSUUsVUFBVSxHQUFHSixNQUFNLENBQUNoRCxLQUFQLENBQWF2RCxLQUFiLEVBQW9CQyxHQUFwQixDQUFqQjs7QUFFQSxPQUFLLElBQUlwQixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHQyxHQUFwQixFQUF5QixFQUFFRCxDQUEzQixFQUE4QjtBQUM1QixRQUFJNkgsUUFBUSxDQUFDN0gsQ0FBRCxDQUFSLEtBQWdCOEgsVUFBVSxDQUFDOUgsQ0FBRCxDQUE5QixFQUFtQztBQUNqQ3lGLE9BQUMsR0FBR29DLFFBQVEsQ0FBQzdILENBQUQsQ0FBWjtBQUNBMEYsT0FBQyxHQUFHb0MsVUFBVSxDQUFDOUgsQ0FBRCxDQUFkO0FBQ0E7QUFDRDtBQUNGOztBQUVELE1BQUl5RixDQUFDLEdBQUdDLENBQVIsRUFBVyxPQUFPLENBQUMsQ0FBUjtBQUNYLE1BQUlBLENBQUMsR0FBR0QsQ0FBUixFQUFXLE9BQU8sQ0FBUDtBQUNYLFNBQU8sQ0FBUDtBQUNELENBekRELEMsQ0EyREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFTc0Msb0JBQVQsQ0FBK0I5QyxNQUEvQixFQUF1QytDLEdBQXZDLEVBQTRDbkQsVUFBNUMsRUFBd0RWLFFBQXhELEVBQWtFOEQsR0FBbEUsRUFBdUU7QUFDckU7QUFDQSxNQUFJaEQsTUFBTSxDQUFDL0UsTUFBUCxLQUFrQixDQUF0QixFQUF5QixPQUFPLENBQUMsQ0FBUixDQUY0QyxDQUlyRTs7QUFDQSxNQUFJLE9BQU8yRSxVQUFQLEtBQXNCLFFBQTFCLEVBQW9DO0FBQ2xDVixZQUFRLEdBQUdVLFVBQVg7QUFDQUEsY0FBVSxHQUFHLENBQWI7QUFDRCxHQUhELE1BR08sSUFBSUEsVUFBVSxHQUFHLFVBQWpCLEVBQTZCO0FBQ2xDQSxjQUFVLEdBQUcsVUFBYjtBQUNELEdBRk0sTUFFQSxJQUFJQSxVQUFVLEdBQUcsQ0FBQyxVQUFsQixFQUE4QjtBQUNuQ0EsY0FBVSxHQUFHLENBQUMsVUFBZDtBQUNEOztBQUNEQSxZQUFVLEdBQUcsQ0FBQ0EsVUFBZCxDQWJxRSxDQWEzQzs7QUFDMUIsTUFBSXFELEtBQUssQ0FBQ3JELFVBQUQsQ0FBVCxFQUF1QjtBQUNyQjtBQUNBQSxjQUFVLEdBQUdvRCxHQUFHLEdBQUcsQ0FBSCxHQUFRaEQsTUFBTSxDQUFDL0UsTUFBUCxHQUFnQixDQUF4QztBQUNELEdBakJvRSxDQW1CckU7OztBQUNBLE1BQUkyRSxVQUFVLEdBQUcsQ0FBakIsRUFBb0JBLFVBQVUsR0FBR0ksTUFBTSxDQUFDL0UsTUFBUCxHQUFnQjJFLFVBQTdCOztBQUNwQixNQUFJQSxVQUFVLElBQUlJLE1BQU0sQ0FBQy9FLE1BQXpCLEVBQWlDO0FBQy9CLFFBQUkrSCxHQUFKLEVBQVMsT0FBTyxDQUFDLENBQVIsQ0FBVCxLQUNLcEQsVUFBVSxHQUFHSSxNQUFNLENBQUMvRSxNQUFQLEdBQWdCLENBQTdCO0FBQ04sR0FIRCxNQUdPLElBQUkyRSxVQUFVLEdBQUcsQ0FBakIsRUFBb0I7QUFDekIsUUFBSW9ELEdBQUosRUFBU3BELFVBQVUsR0FBRyxDQUFiLENBQVQsS0FDSyxPQUFPLENBQUMsQ0FBUjtBQUNOLEdBM0JvRSxDQTZCckU7OztBQUNBLE1BQUksT0FBT21ELEdBQVAsS0FBZSxRQUFuQixFQUE2QjtBQUMzQkEsT0FBRyxHQUFHOUksTUFBTSxDQUFDQyxJQUFQLENBQVk2SSxHQUFaLEVBQWlCN0QsUUFBakIsQ0FBTjtBQUNELEdBaENvRSxDQWtDckU7OztBQUNBLE1BQUlqRixNQUFNLENBQUM2RixRQUFQLENBQWdCaUQsR0FBaEIsQ0FBSixFQUEwQjtBQUN4QjtBQUNBLFFBQUlBLEdBQUcsQ0FBQzlILE1BQUosS0FBZSxDQUFuQixFQUFzQjtBQUNwQixhQUFPLENBQUMsQ0FBUjtBQUNEOztBQUNELFdBQU9pSSxZQUFZLENBQUNsRCxNQUFELEVBQVMrQyxHQUFULEVBQWNuRCxVQUFkLEVBQTBCVixRQUExQixFQUFvQzhELEdBQXBDLENBQW5CO0FBQ0QsR0FORCxNQU1PLElBQUksT0FBT0QsR0FBUCxLQUFlLFFBQW5CLEVBQTZCO0FBQ2xDQSxPQUFHLEdBQUdBLEdBQUcsR0FBRyxJQUFaLENBRGtDLENBQ2pCOztBQUNqQixRQUFJOUksTUFBTSxDQUFDZ0QsbUJBQVAsSUFDQSxPQUFPckMsVUFBVSxDQUFDMkMsU0FBWCxDQUFxQmhDLE9BQTVCLEtBQXdDLFVBRDVDLEVBQ3dEO0FBQ3RELFVBQUl5SCxHQUFKLEVBQVM7QUFDUCxlQUFPcEksVUFBVSxDQUFDMkMsU0FBWCxDQUFxQmhDLE9BQXJCLENBQTZCNEgsSUFBN0IsQ0FBa0NuRCxNQUFsQyxFQUEwQytDLEdBQTFDLEVBQStDbkQsVUFBL0MsQ0FBUDtBQUNELE9BRkQsTUFFTztBQUNMLGVBQU9oRixVQUFVLENBQUMyQyxTQUFYLENBQXFCNkYsV0FBckIsQ0FBaUNELElBQWpDLENBQXNDbkQsTUFBdEMsRUFBOEMrQyxHQUE5QyxFQUFtRG5ELFVBQW5ELENBQVA7QUFDRDtBQUNGOztBQUNELFdBQU9zRCxZQUFZLENBQUNsRCxNQUFELEVBQVMsQ0FBRStDLEdBQUYsQ0FBVCxFQUFrQm5ELFVBQWxCLEVBQThCVixRQUE5QixFQUF3QzhELEdBQXhDLENBQW5CO0FBQ0Q7O0FBRUQsUUFBTSxJQUFJNUUsU0FBSixDQUFjLHNDQUFkLENBQU47QUFDRDs7QUFFRCxTQUFTOEUsWUFBVCxDQUF1QnRILEdBQXZCLEVBQTRCbUgsR0FBNUIsRUFBaUNuRCxVQUFqQyxFQUE2Q1YsUUFBN0MsRUFBdUQ4RCxHQUF2RCxFQUE0RDtBQUMxRCxNQUFJSyxTQUFTLEdBQUcsQ0FBaEI7QUFDQSxNQUFJQyxTQUFTLEdBQUcxSCxHQUFHLENBQUNYLE1BQXBCO0FBQ0EsTUFBSXNJLFNBQVMsR0FBR1IsR0FBRyxDQUFDOUgsTUFBcEI7O0FBRUEsTUFBSWlFLFFBQVEsS0FBSy9CLFNBQWpCLEVBQTRCO0FBQzFCK0IsWUFBUSxHQUFHMEIsTUFBTSxDQUFDMUIsUUFBRCxDQUFOLENBQWlCMkIsV0FBakIsRUFBWDs7QUFDQSxRQUFJM0IsUUFBUSxLQUFLLE1BQWIsSUFBdUJBLFFBQVEsS0FBSyxPQUFwQyxJQUNBQSxRQUFRLEtBQUssU0FEYixJQUMwQkEsUUFBUSxLQUFLLFVBRDNDLEVBQ3VEO0FBQ3JELFVBQUl0RCxHQUFHLENBQUNYLE1BQUosR0FBYSxDQUFiLElBQWtCOEgsR0FBRyxDQUFDOUgsTUFBSixHQUFhLENBQW5DLEVBQXNDO0FBQ3BDLGVBQU8sQ0FBQyxDQUFSO0FBQ0Q7O0FBQ0RvSSxlQUFTLEdBQUcsQ0FBWjtBQUNBQyxlQUFTLElBQUksQ0FBYjtBQUNBQyxlQUFTLElBQUksQ0FBYjtBQUNBM0QsZ0JBQVUsSUFBSSxDQUFkO0FBQ0Q7QUFDRjs7QUFFRCxXQUFTNEQsSUFBVCxDQUFldkMsR0FBZixFQUFvQmxHLENBQXBCLEVBQXVCO0FBQ3JCLFFBQUlzSSxTQUFTLEtBQUssQ0FBbEIsRUFBcUI7QUFDbkIsYUFBT3BDLEdBQUcsQ0FBQ2xHLENBQUQsQ0FBVjtBQUNELEtBRkQsTUFFTztBQUNMLGFBQU9rRyxHQUFHLENBQUN3QyxZQUFKLENBQWlCMUksQ0FBQyxHQUFHc0ksU0FBckIsQ0FBUDtBQUNEO0FBQ0Y7O0FBRUQsTUFBSXRJLENBQUo7O0FBQ0EsTUFBSWlJLEdBQUosRUFBUztBQUNQLFFBQUlVLFVBQVUsR0FBRyxDQUFDLENBQWxCOztBQUNBLFNBQUszSSxDQUFDLEdBQUc2RSxVQUFULEVBQXFCN0UsQ0FBQyxHQUFHdUksU0FBekIsRUFBb0N2SSxDQUFDLEVBQXJDLEVBQXlDO0FBQ3ZDLFVBQUl5SSxJQUFJLENBQUM1SCxHQUFELEVBQU1iLENBQU4sQ0FBSixLQUFpQnlJLElBQUksQ0FBQ1QsR0FBRCxFQUFNVyxVQUFVLEtBQUssQ0FBQyxDQUFoQixHQUFvQixDQUFwQixHQUF3QjNJLENBQUMsR0FBRzJJLFVBQWxDLENBQXpCLEVBQXdFO0FBQ3RFLFlBQUlBLFVBQVUsS0FBSyxDQUFDLENBQXBCLEVBQXVCQSxVQUFVLEdBQUczSSxDQUFiO0FBQ3ZCLFlBQUlBLENBQUMsR0FBRzJJLFVBQUosR0FBaUIsQ0FBakIsS0FBdUJILFNBQTNCLEVBQXNDLE9BQU9HLFVBQVUsR0FBR0wsU0FBcEI7QUFDdkMsT0FIRCxNQUdPO0FBQ0wsWUFBSUssVUFBVSxLQUFLLENBQUMsQ0FBcEIsRUFBdUIzSSxDQUFDLElBQUlBLENBQUMsR0FBRzJJLFVBQVQ7QUFDdkJBLGtCQUFVLEdBQUcsQ0FBQyxDQUFkO0FBQ0Q7QUFDRjtBQUNGLEdBWEQsTUFXTztBQUNMLFFBQUk5RCxVQUFVLEdBQUcyRCxTQUFiLEdBQXlCRCxTQUE3QixFQUF3QzFELFVBQVUsR0FBRzBELFNBQVMsR0FBR0MsU0FBekI7O0FBQ3hDLFNBQUt4SSxDQUFDLEdBQUc2RSxVQUFULEVBQXFCN0UsQ0FBQyxJQUFJLENBQTFCLEVBQTZCQSxDQUFDLEVBQTlCLEVBQWtDO0FBQ2hDLFVBQUk0SSxLQUFLLEdBQUcsSUFBWjs7QUFDQSxXQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdMLFNBQXBCLEVBQStCSyxDQUFDLEVBQWhDLEVBQW9DO0FBQ2xDLFlBQUlKLElBQUksQ0FBQzVILEdBQUQsRUFBTWIsQ0FBQyxHQUFHNkksQ0FBVixDQUFKLEtBQXFCSixJQUFJLENBQUNULEdBQUQsRUFBTWEsQ0FBTixDQUE3QixFQUF1QztBQUNyQ0QsZUFBSyxHQUFHLEtBQVI7QUFDQTtBQUNEO0FBQ0Y7O0FBQ0QsVUFBSUEsS0FBSixFQUFXLE9BQU81SSxDQUFQO0FBQ1o7QUFDRjs7QUFFRCxTQUFPLENBQUMsQ0FBUjtBQUNEOztBQUVEZCxNQUFNLENBQUNzRCxTQUFQLENBQWlCc0csUUFBakIsR0FBNEIsU0FBU0EsUUFBVCxDQUFtQmQsR0FBbkIsRUFBd0JuRCxVQUF4QixFQUFvQ1YsUUFBcEMsRUFBOEM7QUFDeEUsU0FBTyxLQUFLM0QsT0FBTCxDQUFhd0gsR0FBYixFQUFrQm5ELFVBQWxCLEVBQThCVixRQUE5QixNQUE0QyxDQUFDLENBQXBEO0FBQ0QsQ0FGRDs7QUFJQWpGLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJoQyxPQUFqQixHQUEyQixTQUFTQSxPQUFULENBQWtCd0gsR0FBbEIsRUFBdUJuRCxVQUF2QixFQUFtQ1YsUUFBbkMsRUFBNkM7QUFDdEUsU0FBTzRELG9CQUFvQixDQUFDLElBQUQsRUFBT0MsR0FBUCxFQUFZbkQsVUFBWixFQUF3QlYsUUFBeEIsRUFBa0MsSUFBbEMsQ0FBM0I7QUFDRCxDQUZEOztBQUlBakYsTUFBTSxDQUFDc0QsU0FBUCxDQUFpQjZGLFdBQWpCLEdBQStCLFNBQVNBLFdBQVQsQ0FBc0JMLEdBQXRCLEVBQTJCbkQsVUFBM0IsRUFBdUNWLFFBQXZDLEVBQWlEO0FBQzlFLFNBQU80RCxvQkFBb0IsQ0FBQyxJQUFELEVBQU9DLEdBQVAsRUFBWW5ELFVBQVosRUFBd0JWLFFBQXhCLEVBQWtDLEtBQWxDLENBQTNCO0FBQ0QsQ0FGRDs7QUFJQSxTQUFTNEUsUUFBVCxDQUFtQjdDLEdBQW5CLEVBQXdCNUIsTUFBeEIsRUFBZ0MwRSxNQUFoQyxFQUF3QzlJLE1BQXhDLEVBQWdEO0FBQzlDOEksUUFBTSxHQUFHQyxNQUFNLENBQUNELE1BQUQsQ0FBTixJQUFrQixDQUEzQjtBQUNBLE1BQUlFLFNBQVMsR0FBR2hELEdBQUcsQ0FBQ2hHLE1BQUosR0FBYThJLE1BQTdCOztBQUNBLE1BQUksQ0FBQzlJLE1BQUwsRUFBYTtBQUNYQSxVQUFNLEdBQUdnSixTQUFUO0FBQ0QsR0FGRCxNQUVPO0FBQ0xoSixVQUFNLEdBQUcrSSxNQUFNLENBQUMvSSxNQUFELENBQWY7O0FBQ0EsUUFBSUEsTUFBTSxHQUFHZ0osU0FBYixFQUF3QjtBQUN0QmhKLFlBQU0sR0FBR2dKLFNBQVQ7QUFDRDtBQUNGLEdBVjZDLENBWTlDOzs7QUFDQSxNQUFJQyxNQUFNLEdBQUc3RSxNQUFNLENBQUNwRSxNQUFwQjtBQUNBLE1BQUlpSixNQUFNLEdBQUcsQ0FBVCxLQUFlLENBQW5CLEVBQXNCLE1BQU0sSUFBSTlGLFNBQUosQ0FBYyxvQkFBZCxDQUFOOztBQUV0QixNQUFJbkQsTUFBTSxHQUFHaUosTUFBTSxHQUFHLENBQXRCLEVBQXlCO0FBQ3ZCakosVUFBTSxHQUFHaUosTUFBTSxHQUFHLENBQWxCO0FBQ0Q7O0FBQ0QsT0FBSyxJQUFJbkosQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0UsTUFBcEIsRUFBNEIsRUFBRUYsQ0FBOUIsRUFBaUM7QUFDL0IsUUFBSW9KLE1BQU0sR0FBR0MsUUFBUSxDQUFDL0UsTUFBTSxDQUFDZ0YsTUFBUCxDQUFjdEosQ0FBQyxHQUFHLENBQWxCLEVBQXFCLENBQXJCLENBQUQsRUFBMEIsRUFBMUIsQ0FBckI7QUFDQSxRQUFJa0ksS0FBSyxDQUFDa0IsTUFBRCxDQUFULEVBQW1CLE9BQU9wSixDQUFQO0FBQ25Ca0csT0FBRyxDQUFDOEMsTUFBTSxHQUFHaEosQ0FBVixDQUFILEdBQWtCb0osTUFBbEI7QUFDRDs7QUFDRCxTQUFPcEosQ0FBUDtBQUNEOztBQUVELFNBQVN1SixTQUFULENBQW9CckQsR0FBcEIsRUFBeUI1QixNQUF6QixFQUFpQzBFLE1BQWpDLEVBQXlDOUksTUFBekMsRUFBaUQ7QUFDL0MsU0FBT3NKLFVBQVUsQ0FBQ25ELFdBQVcsQ0FBQy9CLE1BQUQsRUFBUzRCLEdBQUcsQ0FBQ2hHLE1BQUosR0FBYThJLE1BQXRCLENBQVosRUFBMkM5QyxHQUEzQyxFQUFnRDhDLE1BQWhELEVBQXdEOUksTUFBeEQsQ0FBakI7QUFDRDs7QUFFRCxTQUFTdUosVUFBVCxDQUFxQnZELEdBQXJCLEVBQTBCNUIsTUFBMUIsRUFBa0MwRSxNQUFsQyxFQUEwQzlJLE1BQTFDLEVBQWtEO0FBQ2hELFNBQU9zSixVQUFVLENBQUNFLFlBQVksQ0FBQ3BGLE1BQUQsQ0FBYixFQUF1QjRCLEdBQXZCLEVBQTRCOEMsTUFBNUIsRUFBb0M5SSxNQUFwQyxDQUFqQjtBQUNEOztBQUVELFNBQVN5SixXQUFULENBQXNCekQsR0FBdEIsRUFBMkI1QixNQUEzQixFQUFtQzBFLE1BQW5DLEVBQTJDOUksTUFBM0MsRUFBbUQ7QUFDakQsU0FBT3VKLFVBQVUsQ0FBQ3ZELEdBQUQsRUFBTTVCLE1BQU4sRUFBYzBFLE1BQWQsRUFBc0I5SSxNQUF0QixDQUFqQjtBQUNEOztBQUVELFNBQVMwSixXQUFULENBQXNCMUQsR0FBdEIsRUFBMkI1QixNQUEzQixFQUFtQzBFLE1BQW5DLEVBQTJDOUksTUFBM0MsRUFBbUQ7QUFDakQsU0FBT3NKLFVBQVUsQ0FBQ2xELGFBQWEsQ0FBQ2hDLE1BQUQsQ0FBZCxFQUF3QjRCLEdBQXhCLEVBQTZCOEMsTUFBN0IsRUFBcUM5SSxNQUFyQyxDQUFqQjtBQUNEOztBQUVELFNBQVMySixTQUFULENBQW9CM0QsR0FBcEIsRUFBeUI1QixNQUF6QixFQUFpQzBFLE1BQWpDLEVBQXlDOUksTUFBekMsRUFBaUQ7QUFDL0MsU0FBT3NKLFVBQVUsQ0FBQ00sY0FBYyxDQUFDeEYsTUFBRCxFQUFTNEIsR0FBRyxDQUFDaEcsTUFBSixHQUFhOEksTUFBdEIsQ0FBZixFQUE4QzlDLEdBQTlDLEVBQW1EOEMsTUFBbkQsRUFBMkQ5SSxNQUEzRCxDQUFqQjtBQUNEOztBQUVEaEIsTUFBTSxDQUFDc0QsU0FBUCxDQUFpQmlDLEtBQWpCLEdBQXlCLFNBQVNBLEtBQVQsQ0FBZ0JILE1BQWhCLEVBQXdCMEUsTUFBeEIsRUFBZ0M5SSxNQUFoQyxFQUF3Q2lFLFFBQXhDLEVBQWtEO0FBQ3pFO0FBQ0EsTUFBSTZFLE1BQU0sS0FBSzVHLFNBQWYsRUFBMEI7QUFDeEIrQixZQUFRLEdBQUcsTUFBWDtBQUNBakUsVUFBTSxHQUFHLEtBQUtBLE1BQWQ7QUFDQThJLFVBQU0sR0FBRyxDQUFULENBSHdCLENBSTFCO0FBQ0MsR0FMRCxNQUtPLElBQUk5SSxNQUFNLEtBQUtrQyxTQUFYLElBQXdCLE9BQU80RyxNQUFQLEtBQWtCLFFBQTlDLEVBQXdEO0FBQzdEN0UsWUFBUSxHQUFHNkUsTUFBWDtBQUNBOUksVUFBTSxHQUFHLEtBQUtBLE1BQWQ7QUFDQThJLFVBQU0sR0FBRyxDQUFULENBSDZELENBSS9EO0FBQ0MsR0FMTSxNQUtBLElBQUllLFFBQVEsQ0FBQ2YsTUFBRCxDQUFaLEVBQXNCO0FBQzNCQSxVQUFNLEdBQUdBLE1BQU0sR0FBRyxDQUFsQjs7QUFDQSxRQUFJZSxRQUFRLENBQUM3SixNQUFELENBQVosRUFBc0I7QUFDcEJBLFlBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCO0FBQ0EsVUFBSWlFLFFBQVEsS0FBSy9CLFNBQWpCLEVBQTRCK0IsUUFBUSxHQUFHLE1BQVg7QUFDN0IsS0FIRCxNQUdPO0FBQ0xBLGNBQVEsR0FBR2pFLE1BQVg7QUFDQUEsWUFBTSxHQUFHa0MsU0FBVDtBQUNELEtBUjBCLENBUzdCOztBQUNDLEdBVk0sTUFVQTtBQUNMLFVBQU0sSUFBSTlCLEtBQUosQ0FDSix5RUFESSxDQUFOO0FBR0Q7O0FBRUQsTUFBSTRJLFNBQVMsR0FBRyxLQUFLaEosTUFBTCxHQUFjOEksTUFBOUI7QUFDQSxNQUFJOUksTUFBTSxLQUFLa0MsU0FBWCxJQUF3QmxDLE1BQU0sR0FBR2dKLFNBQXJDLEVBQWdEaEosTUFBTSxHQUFHZ0osU0FBVDs7QUFFaEQsTUFBSzVFLE1BQU0sQ0FBQ3BFLE1BQVAsR0FBZ0IsQ0FBaEIsS0FBc0JBLE1BQU0sR0FBRyxDQUFULElBQWM4SSxNQUFNLEdBQUcsQ0FBN0MsQ0FBRCxJQUFxREEsTUFBTSxHQUFHLEtBQUs5SSxNQUF2RSxFQUErRTtBQUM3RSxVQUFNLElBQUk0QyxVQUFKLENBQWUsd0NBQWYsQ0FBTjtBQUNEOztBQUVELE1BQUksQ0FBQ3FCLFFBQUwsRUFBZUEsUUFBUSxHQUFHLE1BQVg7QUFFZixNQUFJaUMsV0FBVyxHQUFHLEtBQWxCOztBQUNBLFdBQVM7QUFDUCxZQUFRakMsUUFBUjtBQUNFLFdBQUssS0FBTDtBQUNFLGVBQU80RSxRQUFRLENBQUMsSUFBRCxFQUFPekUsTUFBUCxFQUFlMEUsTUFBZixFQUF1QjlJLE1BQXZCLENBQWY7O0FBRUYsV0FBSyxNQUFMO0FBQ0EsV0FBSyxPQUFMO0FBQ0UsZUFBT3FKLFNBQVMsQ0FBQyxJQUFELEVBQU9qRixNQUFQLEVBQWUwRSxNQUFmLEVBQXVCOUksTUFBdkIsQ0FBaEI7O0FBRUYsV0FBSyxPQUFMO0FBQ0UsZUFBT3VKLFVBQVUsQ0FBQyxJQUFELEVBQU9uRixNQUFQLEVBQWUwRSxNQUFmLEVBQXVCOUksTUFBdkIsQ0FBakI7O0FBRUYsV0FBSyxRQUFMO0FBQ0EsV0FBSyxRQUFMO0FBQ0UsZUFBT3lKLFdBQVcsQ0FBQyxJQUFELEVBQU9yRixNQUFQLEVBQWUwRSxNQUFmLEVBQXVCOUksTUFBdkIsQ0FBbEI7O0FBRUYsV0FBSyxRQUFMO0FBQ0U7QUFDQSxlQUFPMEosV0FBVyxDQUFDLElBQUQsRUFBT3RGLE1BQVAsRUFBZTBFLE1BQWYsRUFBdUI5SSxNQUF2QixDQUFsQjs7QUFFRixXQUFLLE1BQUw7QUFDQSxXQUFLLE9BQUw7QUFDQSxXQUFLLFNBQUw7QUFDQSxXQUFLLFVBQUw7QUFDRSxlQUFPMkosU0FBUyxDQUFDLElBQUQsRUFBT3ZGLE1BQVAsRUFBZTBFLE1BQWYsRUFBdUI5SSxNQUF2QixDQUFoQjs7QUFFRjtBQUNFLFlBQUlrRyxXQUFKLEVBQWlCLE1BQU0sSUFBSS9DLFNBQUosQ0FBYyx1QkFBdUJjLFFBQXJDLENBQU47QUFDakJBLGdCQUFRLEdBQUcsQ0FBQyxLQUFLQSxRQUFOLEVBQWdCMkIsV0FBaEIsRUFBWDtBQUNBTSxtQkFBVyxHQUFHLElBQWQ7QUE1Qko7QUE4QkQ7QUFDRixDQXRFRDs7QUF3RUFsSCxNQUFNLENBQUNzRCxTQUFQLENBQWlCd0gsTUFBakIsR0FBMEIsU0FBU0EsTUFBVCxHQUFtQjtBQUMzQyxTQUFPO0FBQ0w3RSxRQUFJLEVBQUUsUUFERDtBQUVMQyxRQUFJLEVBQUV0RixLQUFLLENBQUMwQyxTQUFOLENBQWdCa0MsS0FBaEIsQ0FBc0IwRCxJQUF0QixDQUEyQixLQUFLNkIsSUFBTCxJQUFhLElBQXhDLEVBQThDLENBQTlDO0FBRkQsR0FBUDtBQUlELENBTEQ7O0FBT0EsU0FBU3JELFdBQVQsQ0FBc0JWLEdBQXRCLEVBQTJCL0UsS0FBM0IsRUFBa0NDLEdBQWxDLEVBQXVDO0FBQ3JDLE1BQUlELEtBQUssS0FBSyxDQUFWLElBQWVDLEdBQUcsS0FBSzhFLEdBQUcsQ0FBQ2hHLE1BQS9CLEVBQXVDO0FBQ3JDLFdBQU8wQixNQUFNLENBQUNuQyxhQUFQLENBQXFCeUcsR0FBckIsQ0FBUDtBQUNELEdBRkQsTUFFTztBQUNMLFdBQU90RSxNQUFNLENBQUNuQyxhQUFQLENBQXFCeUcsR0FBRyxDQUFDeEIsS0FBSixDQUFVdkQsS0FBVixFQUFpQkMsR0FBakIsQ0FBckIsQ0FBUDtBQUNEO0FBQ0Y7O0FBRUQsU0FBU3FGLFNBQVQsQ0FBb0JQLEdBQXBCLEVBQXlCL0UsS0FBekIsRUFBZ0NDLEdBQWhDLEVBQXFDO0FBQ25DQSxLQUFHLEdBQUd1RSxJQUFJLENBQUNDLEdBQUwsQ0FBU00sR0FBRyxDQUFDaEcsTUFBYixFQUFxQmtCLEdBQXJCLENBQU47QUFDQSxNQUFJOEksR0FBRyxHQUFHLEVBQVY7QUFFQSxNQUFJbEssQ0FBQyxHQUFHbUIsS0FBUjs7QUFDQSxTQUFPbkIsQ0FBQyxHQUFHb0IsR0FBWCxFQUFnQjtBQUNkLFFBQUkrSSxTQUFTLEdBQUdqRSxHQUFHLENBQUNsRyxDQUFELENBQW5CO0FBQ0EsUUFBSW9LLFNBQVMsR0FBRyxJQUFoQjtBQUNBLFFBQUlDLGdCQUFnQixHQUFJRixTQUFTLEdBQUcsSUFBYixHQUFxQixDQUFyQixHQUNsQkEsU0FBUyxHQUFHLElBQWIsR0FBcUIsQ0FBckIsR0FDQ0EsU0FBUyxHQUFHLElBQWIsR0FBcUIsQ0FBckIsR0FDQSxDQUhKOztBQUtBLFFBQUluSyxDQUFDLEdBQUdxSyxnQkFBSixJQUF3QmpKLEdBQTVCLEVBQWlDO0FBQy9CLFVBQUlrSixVQUFKLEVBQWdCQyxTQUFoQixFQUEyQkMsVUFBM0IsRUFBdUNDLGFBQXZDOztBQUVBLGNBQVFKLGdCQUFSO0FBQ0UsYUFBSyxDQUFMO0FBQ0UsY0FBSUYsU0FBUyxHQUFHLElBQWhCLEVBQXNCO0FBQ3BCQyxxQkFBUyxHQUFHRCxTQUFaO0FBQ0Q7O0FBQ0Q7O0FBQ0YsYUFBSyxDQUFMO0FBQ0VHLG9CQUFVLEdBQUdwRSxHQUFHLENBQUNsRyxDQUFDLEdBQUcsQ0FBTCxDQUFoQjs7QUFDQSxjQUFJLENBQUNzSyxVQUFVLEdBQUcsSUFBZCxNQUF3QixJQUE1QixFQUFrQztBQUNoQ0cseUJBQWEsR0FBRyxDQUFDTixTQUFTLEdBQUcsSUFBYixLQUFzQixHQUF0QixHQUE2QkcsVUFBVSxHQUFHLElBQTFEOztBQUNBLGdCQUFJRyxhQUFhLEdBQUcsSUFBcEIsRUFBMEI7QUFDeEJMLHVCQUFTLEdBQUdLLGFBQVo7QUFDRDtBQUNGOztBQUNEOztBQUNGLGFBQUssQ0FBTDtBQUNFSCxvQkFBVSxHQUFHcEUsR0FBRyxDQUFDbEcsQ0FBQyxHQUFHLENBQUwsQ0FBaEI7QUFDQXVLLG1CQUFTLEdBQUdyRSxHQUFHLENBQUNsRyxDQUFDLEdBQUcsQ0FBTCxDQUFmOztBQUNBLGNBQUksQ0FBQ3NLLFVBQVUsR0FBRyxJQUFkLE1BQXdCLElBQXhCLElBQWdDLENBQUNDLFNBQVMsR0FBRyxJQUFiLE1BQXVCLElBQTNELEVBQWlFO0FBQy9ERSx5QkFBYSxHQUFHLENBQUNOLFNBQVMsR0FBRyxHQUFiLEtBQXFCLEdBQXJCLEdBQTJCLENBQUNHLFVBQVUsR0FBRyxJQUFkLEtBQXVCLEdBQWxELEdBQXlEQyxTQUFTLEdBQUcsSUFBckY7O0FBQ0EsZ0JBQUlFLGFBQWEsR0FBRyxLQUFoQixLQUEwQkEsYUFBYSxHQUFHLE1BQWhCLElBQTBCQSxhQUFhLEdBQUcsTUFBcEUsQ0FBSixFQUFpRjtBQUMvRUwsdUJBQVMsR0FBR0ssYUFBWjtBQUNEO0FBQ0Y7O0FBQ0Q7O0FBQ0YsYUFBSyxDQUFMO0FBQ0VILG9CQUFVLEdBQUdwRSxHQUFHLENBQUNsRyxDQUFDLEdBQUcsQ0FBTCxDQUFoQjtBQUNBdUssbUJBQVMsR0FBR3JFLEdBQUcsQ0FBQ2xHLENBQUMsR0FBRyxDQUFMLENBQWY7QUFDQXdLLG9CQUFVLEdBQUd0RSxHQUFHLENBQUNsRyxDQUFDLEdBQUcsQ0FBTCxDQUFoQjs7QUFDQSxjQUFJLENBQUNzSyxVQUFVLEdBQUcsSUFBZCxNQUF3QixJQUF4QixJQUFnQyxDQUFDQyxTQUFTLEdBQUcsSUFBYixNQUF1QixJQUF2RCxJQUErRCxDQUFDQyxVQUFVLEdBQUcsSUFBZCxNQUF3QixJQUEzRixFQUFpRztBQUMvRkMseUJBQWEsR0FBRyxDQUFDTixTQUFTLEdBQUcsR0FBYixLQUFxQixJQUFyQixHQUE0QixDQUFDRyxVQUFVLEdBQUcsSUFBZCxLQUF1QixHQUFuRCxHQUF5RCxDQUFDQyxTQUFTLEdBQUcsSUFBYixLQUFzQixHQUEvRSxHQUFzRkMsVUFBVSxHQUFHLElBQW5IOztBQUNBLGdCQUFJQyxhQUFhLEdBQUcsTUFBaEIsSUFBMEJBLGFBQWEsR0FBRyxRQUE5QyxFQUF3RDtBQUN0REwsdUJBQVMsR0FBR0ssYUFBWjtBQUNEO0FBQ0Y7O0FBbENMO0FBb0NEOztBQUVELFFBQUlMLFNBQVMsS0FBSyxJQUFsQixFQUF3QjtBQUN0QjtBQUNBO0FBQ0FBLGVBQVMsR0FBRyxNQUFaO0FBQ0FDLHNCQUFnQixHQUFHLENBQW5CO0FBQ0QsS0FMRCxNQUtPLElBQUlELFNBQVMsR0FBRyxNQUFoQixFQUF3QjtBQUM3QjtBQUNBQSxlQUFTLElBQUksT0FBYjtBQUNBRixTQUFHLENBQUM1SSxJQUFKLENBQVM4SSxTQUFTLEtBQUssRUFBZCxHQUFtQixLQUFuQixHQUEyQixNQUFwQztBQUNBQSxlQUFTLEdBQUcsU0FBU0EsU0FBUyxHQUFHLEtBQWpDO0FBQ0Q7O0FBRURGLE9BQUcsQ0FBQzVJLElBQUosQ0FBUzhJLFNBQVQ7QUFDQXBLLEtBQUMsSUFBSXFLLGdCQUFMO0FBQ0Q7O0FBRUQsU0FBT0sscUJBQXFCLENBQUNSLEdBQUQsQ0FBNUI7QUFDRCxDLENBRUQ7QUFDQTtBQUNBOzs7QUFDQSxJQUFJUyxvQkFBb0IsR0FBRyxNQUEzQjs7QUFFQSxTQUFTRCxxQkFBVCxDQUFnQ0UsVUFBaEMsRUFBNEM7QUFDMUMsTUFBSTNLLEdBQUcsR0FBRzJLLFVBQVUsQ0FBQzFLLE1BQXJCOztBQUNBLE1BQUlELEdBQUcsSUFBSTBLLG9CQUFYLEVBQWlDO0FBQy9CLFdBQU85RSxNQUFNLENBQUNnRixZQUFQLENBQW9CeEQsS0FBcEIsQ0FBMEJ4QixNQUExQixFQUFrQytFLFVBQWxDLENBQVAsQ0FEK0IsQ0FDc0I7QUFDdEQsR0FKeUMsQ0FNMUM7OztBQUNBLE1BQUlWLEdBQUcsR0FBRyxFQUFWO0FBQ0EsTUFBSWxLLENBQUMsR0FBRyxDQUFSOztBQUNBLFNBQU9BLENBQUMsR0FBR0MsR0FBWCxFQUFnQjtBQUNkaUssT0FBRyxJQUFJckUsTUFBTSxDQUFDZ0YsWUFBUCxDQUFvQnhELEtBQXBCLENBQ0x4QixNQURLLEVBRUwrRSxVQUFVLENBQUNsRyxLQUFYLENBQWlCMUUsQ0FBakIsRUFBb0JBLENBQUMsSUFBSTJLLG9CQUF6QixDQUZLLENBQVA7QUFJRDs7QUFDRCxTQUFPVCxHQUFQO0FBQ0Q7O0FBRUQsU0FBU3hELFVBQVQsQ0FBcUJSLEdBQXJCLEVBQTBCL0UsS0FBMUIsRUFBaUNDLEdBQWpDLEVBQXNDO0FBQ3BDLE1BQUkwSixHQUFHLEdBQUcsRUFBVjtBQUNBMUosS0FBRyxHQUFHdUUsSUFBSSxDQUFDQyxHQUFMLENBQVNNLEdBQUcsQ0FBQ2hHLE1BQWIsRUFBcUJrQixHQUFyQixDQUFOOztBQUVBLE9BQUssSUFBSXBCLENBQUMsR0FBR21CLEtBQWIsRUFBb0JuQixDQUFDLEdBQUdvQixHQUF4QixFQUE2QixFQUFFcEIsQ0FBL0IsRUFBa0M7QUFDaEM4SyxPQUFHLElBQUlqRixNQUFNLENBQUNnRixZQUFQLENBQW9CM0UsR0FBRyxDQUFDbEcsQ0FBRCxDQUFILEdBQVMsSUFBN0IsQ0FBUDtBQUNEOztBQUNELFNBQU84SyxHQUFQO0FBQ0Q7O0FBRUQsU0FBU25FLFdBQVQsQ0FBc0JULEdBQXRCLEVBQTJCL0UsS0FBM0IsRUFBa0NDLEdBQWxDLEVBQXVDO0FBQ3JDLE1BQUkwSixHQUFHLEdBQUcsRUFBVjtBQUNBMUosS0FBRyxHQUFHdUUsSUFBSSxDQUFDQyxHQUFMLENBQVNNLEdBQUcsQ0FBQ2hHLE1BQWIsRUFBcUJrQixHQUFyQixDQUFOOztBQUVBLE9BQUssSUFBSXBCLENBQUMsR0FBR21CLEtBQWIsRUFBb0JuQixDQUFDLEdBQUdvQixHQUF4QixFQUE2QixFQUFFcEIsQ0FBL0IsRUFBa0M7QUFDaEM4SyxPQUFHLElBQUlqRixNQUFNLENBQUNnRixZQUFQLENBQW9CM0UsR0FBRyxDQUFDbEcsQ0FBRCxDQUF2QixDQUFQO0FBQ0Q7O0FBQ0QsU0FBTzhLLEdBQVA7QUFDRDs7QUFFRCxTQUFTdEUsUUFBVCxDQUFtQk4sR0FBbkIsRUFBd0IvRSxLQUF4QixFQUErQkMsR0FBL0IsRUFBb0M7QUFDbEMsTUFBSW5CLEdBQUcsR0FBR2lHLEdBQUcsQ0FBQ2hHLE1BQWQ7QUFFQSxNQUFJLENBQUNpQixLQUFELElBQVVBLEtBQUssR0FBRyxDQUF0QixFQUF5QkEsS0FBSyxHQUFHLENBQVI7QUFDekIsTUFBSSxDQUFDQyxHQUFELElBQVFBLEdBQUcsR0FBRyxDQUFkLElBQW1CQSxHQUFHLEdBQUduQixHQUE3QixFQUFrQ21CLEdBQUcsR0FBR25CLEdBQU47QUFFbEMsTUFBSThLLEdBQUcsR0FBRyxFQUFWOztBQUNBLE9BQUssSUFBSS9LLENBQUMsR0FBR21CLEtBQWIsRUFBb0JuQixDQUFDLEdBQUdvQixHQUF4QixFQUE2QixFQUFFcEIsQ0FBL0IsRUFBa0M7QUFDaEMrSyxPQUFHLElBQUlDLEtBQUssQ0FBQzlFLEdBQUcsQ0FBQ2xHLENBQUQsQ0FBSixDQUFaO0FBQ0Q7O0FBQ0QsU0FBTytLLEdBQVA7QUFDRDs7QUFFRCxTQUFTbEUsWUFBVCxDQUF1QlgsR0FBdkIsRUFBNEIvRSxLQUE1QixFQUFtQ0MsR0FBbkMsRUFBd0M7QUFDdEMsTUFBSTZKLEtBQUssR0FBRy9FLEdBQUcsQ0FBQ3hCLEtBQUosQ0FBVXZELEtBQVYsRUFBaUJDLEdBQWpCLENBQVo7QUFDQSxNQUFJOEksR0FBRyxHQUFHLEVBQVY7O0FBQ0EsT0FBSyxJQUFJbEssQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR2lMLEtBQUssQ0FBQy9LLE1BQTFCLEVBQWtDRixDQUFDLElBQUksQ0FBdkMsRUFBMEM7QUFDeENrSyxPQUFHLElBQUlyRSxNQUFNLENBQUNnRixZQUFQLENBQW9CSSxLQUFLLENBQUNqTCxDQUFELENBQUwsR0FBV2lMLEtBQUssQ0FBQ2pMLENBQUMsR0FBRyxDQUFMLENBQUwsR0FBZSxHQUE5QyxDQUFQO0FBQ0Q7O0FBQ0QsU0FBT2tLLEdBQVA7QUFDRDs7QUFFRGhMLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJrQyxLQUFqQixHQUF5QixTQUFTQSxLQUFULENBQWdCdkQsS0FBaEIsRUFBdUJDLEdBQXZCLEVBQTRCO0FBQ25ELE1BQUluQixHQUFHLEdBQUcsS0FBS0MsTUFBZjtBQUNBaUIsT0FBSyxHQUFHLENBQUMsQ0FBQ0EsS0FBVjtBQUNBQyxLQUFHLEdBQUdBLEdBQUcsS0FBS2dCLFNBQVIsR0FBb0JuQyxHQUFwQixHQUEwQixDQUFDLENBQUNtQixHQUFsQzs7QUFFQSxNQUFJRCxLQUFLLEdBQUcsQ0FBWixFQUFlO0FBQ2JBLFNBQUssSUFBSWxCLEdBQVQ7QUFDQSxRQUFJa0IsS0FBSyxHQUFHLENBQVosRUFBZUEsS0FBSyxHQUFHLENBQVI7QUFDaEIsR0FIRCxNQUdPLElBQUlBLEtBQUssR0FBR2xCLEdBQVosRUFBaUI7QUFDdEJrQixTQUFLLEdBQUdsQixHQUFSO0FBQ0Q7O0FBRUQsTUFBSW1CLEdBQUcsR0FBRyxDQUFWLEVBQWE7QUFDWEEsT0FBRyxJQUFJbkIsR0FBUDtBQUNBLFFBQUltQixHQUFHLEdBQUcsQ0FBVixFQUFhQSxHQUFHLEdBQUcsQ0FBTjtBQUNkLEdBSEQsTUFHTyxJQUFJQSxHQUFHLEdBQUduQixHQUFWLEVBQWU7QUFDcEJtQixPQUFHLEdBQUduQixHQUFOO0FBQ0Q7O0FBRUQsTUFBSW1CLEdBQUcsR0FBR0QsS0FBVixFQUFpQkMsR0FBRyxHQUFHRCxLQUFOO0FBRWpCLE1BQUkrSixNQUFKOztBQUNBLE1BQUloTSxNQUFNLENBQUNnRCxtQkFBWCxFQUFnQztBQUM5QmdKLFVBQU0sR0FBRyxLQUFLeEksUUFBTCxDQUFjdkIsS0FBZCxFQUFxQkMsR0FBckIsQ0FBVDtBQUNBOEosVUFBTSxDQUFDM0ksU0FBUCxHQUFtQnJELE1BQU0sQ0FBQ3NELFNBQTFCO0FBQ0QsR0FIRCxNQUdPO0FBQ0wsUUFBSTJJLFFBQVEsR0FBRy9KLEdBQUcsR0FBR0QsS0FBckI7QUFDQStKLFVBQU0sR0FBRyxJQUFJaE0sTUFBSixDQUFXaU0sUUFBWCxFQUFxQi9JLFNBQXJCLENBQVQ7O0FBQ0EsU0FBSyxJQUFJcEMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR21MLFFBQXBCLEVBQThCLEVBQUVuTCxDQUFoQyxFQUFtQztBQUNqQ2tMLFlBQU0sQ0FBQ2xMLENBQUQsQ0FBTixHQUFZLEtBQUtBLENBQUMsR0FBR21CLEtBQVQsQ0FBWjtBQUNEO0FBQ0Y7O0FBRUQsU0FBTytKLE1BQVA7QUFDRCxDQWxDRDtBQW9DQTtBQUNBO0FBQ0E7OztBQUNBLFNBQVNFLFdBQVQsQ0FBc0JwQyxNQUF0QixFQUE4QnFDLEdBQTlCLEVBQW1DbkwsTUFBbkMsRUFBMkM7QUFDekMsTUFBSzhJLE1BQU0sR0FBRyxDQUFWLEtBQWlCLENBQWpCLElBQXNCQSxNQUFNLEdBQUcsQ0FBbkMsRUFBc0MsTUFBTSxJQUFJbEcsVUFBSixDQUFlLG9CQUFmLENBQU47QUFDdEMsTUFBSWtHLE1BQU0sR0FBR3FDLEdBQVQsR0FBZW5MLE1BQW5CLEVBQTJCLE1BQU0sSUFBSTRDLFVBQUosQ0FBZSx1Q0FBZixDQUFOO0FBQzVCOztBQUVENUQsTUFBTSxDQUFDc0QsU0FBUCxDQUFpQjhJLFVBQWpCLEdBQThCLFNBQVNBLFVBQVQsQ0FBcUJ0QyxNQUFyQixFQUE2QnpKLFVBQTdCLEVBQXlDZ00sUUFBekMsRUFBbUQ7QUFDL0V2QyxRQUFNLEdBQUdBLE1BQU0sR0FBRyxDQUFsQjtBQUNBekosWUFBVSxHQUFHQSxVQUFVLEdBQUcsQ0FBMUI7QUFDQSxNQUFJLENBQUNnTSxRQUFMLEVBQWVILFdBQVcsQ0FBQ3BDLE1BQUQsRUFBU3pKLFVBQVQsRUFBcUIsS0FBS1csTUFBMUIsQ0FBWDtBQUVmLE1BQUk4SCxHQUFHLEdBQUcsS0FBS2dCLE1BQUwsQ0FBVjtBQUNBLE1BQUl3QyxHQUFHLEdBQUcsQ0FBVjtBQUNBLE1BQUl4TCxDQUFDLEdBQUcsQ0FBUjs7QUFDQSxTQUFPLEVBQUVBLENBQUYsR0FBTVQsVUFBTixLQUFxQmlNLEdBQUcsSUFBSSxLQUE1QixDQUFQLEVBQTJDO0FBQ3pDeEQsT0FBRyxJQUFJLEtBQUtnQixNQUFNLEdBQUdoSixDQUFkLElBQW1Cd0wsR0FBMUI7QUFDRDs7QUFFRCxTQUFPeEQsR0FBUDtBQUNELENBYkQ7O0FBZUE5SSxNQUFNLENBQUNzRCxTQUFQLENBQWlCaUosVUFBakIsR0FBOEIsU0FBU0EsVUFBVCxDQUFxQnpDLE1BQXJCLEVBQTZCekosVUFBN0IsRUFBeUNnTSxRQUF6QyxFQUFtRDtBQUMvRXZDLFFBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCO0FBQ0F6SixZQUFVLEdBQUdBLFVBQVUsR0FBRyxDQUExQjs7QUFDQSxNQUFJLENBQUNnTSxRQUFMLEVBQWU7QUFDYkgsZUFBVyxDQUFDcEMsTUFBRCxFQUFTekosVUFBVCxFQUFxQixLQUFLVyxNQUExQixDQUFYO0FBQ0Q7O0FBRUQsTUFBSThILEdBQUcsR0FBRyxLQUFLZ0IsTUFBTSxHQUFHLEVBQUV6SixVQUFoQixDQUFWO0FBQ0EsTUFBSWlNLEdBQUcsR0FBRyxDQUFWOztBQUNBLFNBQU9qTSxVQUFVLEdBQUcsQ0FBYixLQUFtQmlNLEdBQUcsSUFBSSxLQUExQixDQUFQLEVBQXlDO0FBQ3ZDeEQsT0FBRyxJQUFJLEtBQUtnQixNQUFNLEdBQUcsRUFBRXpKLFVBQWhCLElBQThCaU0sR0FBckM7QUFDRDs7QUFFRCxTQUFPeEQsR0FBUDtBQUNELENBZEQ7O0FBZ0JBOUksTUFBTSxDQUFDc0QsU0FBUCxDQUFpQmtKLFNBQWpCLEdBQTZCLFNBQVNBLFNBQVQsQ0FBb0IxQyxNQUFwQixFQUE0QnVDLFFBQTVCLEVBQXNDO0FBQ2pFLE1BQUksQ0FBQ0EsUUFBTCxFQUFlSCxXQUFXLENBQUNwQyxNQUFELEVBQVMsQ0FBVCxFQUFZLEtBQUs5SSxNQUFqQixDQUFYO0FBQ2YsU0FBTyxLQUFLOEksTUFBTCxDQUFQO0FBQ0QsQ0FIRDs7QUFLQTlKLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJtSixZQUFqQixHQUFnQyxTQUFTQSxZQUFULENBQXVCM0MsTUFBdkIsRUFBK0J1QyxRQUEvQixFQUF5QztBQUN2RSxNQUFJLENBQUNBLFFBQUwsRUFBZUgsV0FBVyxDQUFDcEMsTUFBRCxFQUFTLENBQVQsRUFBWSxLQUFLOUksTUFBakIsQ0FBWDtBQUNmLFNBQU8sS0FBSzhJLE1BQUwsSUFBZ0IsS0FBS0EsTUFBTSxHQUFHLENBQWQsS0FBb0IsQ0FBM0M7QUFDRCxDQUhEOztBQUtBOUosTUFBTSxDQUFDc0QsU0FBUCxDQUFpQmtHLFlBQWpCLEdBQWdDLFNBQVNBLFlBQVQsQ0FBdUJNLE1BQXZCLEVBQStCdUMsUUFBL0IsRUFBeUM7QUFDdkUsTUFBSSxDQUFDQSxRQUFMLEVBQWVILFdBQVcsQ0FBQ3BDLE1BQUQsRUFBUyxDQUFULEVBQVksS0FBSzlJLE1BQWpCLENBQVg7QUFDZixTQUFRLEtBQUs4SSxNQUFMLEtBQWdCLENBQWpCLEdBQXNCLEtBQUtBLE1BQU0sR0FBRyxDQUFkLENBQTdCO0FBQ0QsQ0FIRDs7QUFLQTlKLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJvSixZQUFqQixHQUFnQyxTQUFTQSxZQUFULENBQXVCNUMsTUFBdkIsRUFBK0J1QyxRQUEvQixFQUF5QztBQUN2RSxNQUFJLENBQUNBLFFBQUwsRUFBZUgsV0FBVyxDQUFDcEMsTUFBRCxFQUFTLENBQVQsRUFBWSxLQUFLOUksTUFBakIsQ0FBWDtBQUVmLFNBQU8sQ0FBRSxLQUFLOEksTUFBTCxDQUFELEdBQ0gsS0FBS0EsTUFBTSxHQUFHLENBQWQsS0FBb0IsQ0FEakIsR0FFSCxLQUFLQSxNQUFNLEdBQUcsQ0FBZCxLQUFvQixFQUZsQixJQUdGLEtBQUtBLE1BQU0sR0FBRyxDQUFkLElBQW1CLFNBSHhCO0FBSUQsQ0FQRDs7QUFTQTlKLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJxSixZQUFqQixHQUFnQyxTQUFTQSxZQUFULENBQXVCN0MsTUFBdkIsRUFBK0J1QyxRQUEvQixFQUF5QztBQUN2RSxNQUFJLENBQUNBLFFBQUwsRUFBZUgsV0FBVyxDQUFDcEMsTUFBRCxFQUFTLENBQVQsRUFBWSxLQUFLOUksTUFBakIsQ0FBWDtBQUVmLFNBQVEsS0FBSzhJLE1BQUwsSUFBZSxTQUFoQixJQUNILEtBQUtBLE1BQU0sR0FBRyxDQUFkLEtBQW9CLEVBQXJCLEdBQ0EsS0FBS0EsTUFBTSxHQUFHLENBQWQsS0FBb0IsQ0FEcEIsR0FFRCxLQUFLQSxNQUFNLEdBQUcsQ0FBZCxDQUhLLENBQVA7QUFJRCxDQVBEOztBQVNBOUosTUFBTSxDQUFDc0QsU0FBUCxDQUFpQnNKLFNBQWpCLEdBQTZCLFNBQVNBLFNBQVQsQ0FBb0I5QyxNQUFwQixFQUE0QnpKLFVBQTVCLEVBQXdDZ00sUUFBeEMsRUFBa0Q7QUFDN0V2QyxRQUFNLEdBQUdBLE1BQU0sR0FBRyxDQUFsQjtBQUNBekosWUFBVSxHQUFHQSxVQUFVLEdBQUcsQ0FBMUI7QUFDQSxNQUFJLENBQUNnTSxRQUFMLEVBQWVILFdBQVcsQ0FBQ3BDLE1BQUQsRUFBU3pKLFVBQVQsRUFBcUIsS0FBS1csTUFBMUIsQ0FBWDtBQUVmLE1BQUk4SCxHQUFHLEdBQUcsS0FBS2dCLE1BQUwsQ0FBVjtBQUNBLE1BQUl3QyxHQUFHLEdBQUcsQ0FBVjtBQUNBLE1BQUl4TCxDQUFDLEdBQUcsQ0FBUjs7QUFDQSxTQUFPLEVBQUVBLENBQUYsR0FBTVQsVUFBTixLQUFxQmlNLEdBQUcsSUFBSSxLQUE1QixDQUFQLEVBQTJDO0FBQ3pDeEQsT0FBRyxJQUFJLEtBQUtnQixNQUFNLEdBQUdoSixDQUFkLElBQW1Cd0wsR0FBMUI7QUFDRDs7QUFDREEsS0FBRyxJQUFJLElBQVA7QUFFQSxNQUFJeEQsR0FBRyxJQUFJd0QsR0FBWCxFQUFnQnhELEdBQUcsSUFBSXJDLElBQUksQ0FBQ29HLEdBQUwsQ0FBUyxDQUFULEVBQVksSUFBSXhNLFVBQWhCLENBQVA7QUFFaEIsU0FBT3lJLEdBQVA7QUFDRCxDQWhCRDs7QUFrQkE5SSxNQUFNLENBQUNzRCxTQUFQLENBQWlCd0osU0FBakIsR0FBNkIsU0FBU0EsU0FBVCxDQUFvQmhELE1BQXBCLEVBQTRCekosVUFBNUIsRUFBd0NnTSxRQUF4QyxFQUFrRDtBQUM3RXZDLFFBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCO0FBQ0F6SixZQUFVLEdBQUdBLFVBQVUsR0FBRyxDQUExQjtBQUNBLE1BQUksQ0FBQ2dNLFFBQUwsRUFBZUgsV0FBVyxDQUFDcEMsTUFBRCxFQUFTekosVUFBVCxFQUFxQixLQUFLVyxNQUExQixDQUFYO0FBRWYsTUFBSUYsQ0FBQyxHQUFHVCxVQUFSO0FBQ0EsTUFBSWlNLEdBQUcsR0FBRyxDQUFWO0FBQ0EsTUFBSXhELEdBQUcsR0FBRyxLQUFLZ0IsTUFBTSxHQUFHLEVBQUVoSixDQUFoQixDQUFWOztBQUNBLFNBQU9BLENBQUMsR0FBRyxDQUFKLEtBQVV3TCxHQUFHLElBQUksS0FBakIsQ0FBUCxFQUFnQztBQUM5QnhELE9BQUcsSUFBSSxLQUFLZ0IsTUFBTSxHQUFHLEVBQUVoSixDQUFoQixJQUFxQndMLEdBQTVCO0FBQ0Q7O0FBQ0RBLEtBQUcsSUFBSSxJQUFQO0FBRUEsTUFBSXhELEdBQUcsSUFBSXdELEdBQVgsRUFBZ0J4RCxHQUFHLElBQUlyQyxJQUFJLENBQUNvRyxHQUFMLENBQVMsQ0FBVCxFQUFZLElBQUl4TSxVQUFoQixDQUFQO0FBRWhCLFNBQU95SSxHQUFQO0FBQ0QsQ0FoQkQ7O0FBa0JBOUksTUFBTSxDQUFDc0QsU0FBUCxDQUFpQnlKLFFBQWpCLEdBQTRCLFNBQVNBLFFBQVQsQ0FBbUJqRCxNQUFuQixFQUEyQnVDLFFBQTNCLEVBQXFDO0FBQy9ELE1BQUksQ0FBQ0EsUUFBTCxFQUFlSCxXQUFXLENBQUNwQyxNQUFELEVBQVMsQ0FBVCxFQUFZLEtBQUs5SSxNQUFqQixDQUFYO0FBQ2YsTUFBSSxFQUFFLEtBQUs4SSxNQUFMLElBQWUsSUFBakIsQ0FBSixFQUE0QixPQUFRLEtBQUtBLE1BQUwsQ0FBUjtBQUM1QixTQUFRLENBQUMsT0FBTyxLQUFLQSxNQUFMLENBQVAsR0FBc0IsQ0FBdkIsSUFBNEIsQ0FBQyxDQUFyQztBQUNELENBSkQ7O0FBTUE5SixNQUFNLENBQUNzRCxTQUFQLENBQWlCMEosV0FBakIsR0FBK0IsU0FBU0EsV0FBVCxDQUFzQmxELE1BQXRCLEVBQThCdUMsUUFBOUIsRUFBd0M7QUFDckUsTUFBSSxDQUFDQSxRQUFMLEVBQWVILFdBQVcsQ0FBQ3BDLE1BQUQsRUFBUyxDQUFULEVBQVksS0FBSzlJLE1BQWpCLENBQVg7QUFDZixNQUFJOEgsR0FBRyxHQUFHLEtBQUtnQixNQUFMLElBQWdCLEtBQUtBLE1BQU0sR0FBRyxDQUFkLEtBQW9CLENBQTlDO0FBQ0EsU0FBUWhCLEdBQUcsR0FBRyxNQUFQLEdBQWlCQSxHQUFHLEdBQUcsVUFBdkIsR0FBb0NBLEdBQTNDO0FBQ0QsQ0FKRDs7QUFNQTlJLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUIySixXQUFqQixHQUErQixTQUFTQSxXQUFULENBQXNCbkQsTUFBdEIsRUFBOEJ1QyxRQUE5QixFQUF3QztBQUNyRSxNQUFJLENBQUNBLFFBQUwsRUFBZUgsV0FBVyxDQUFDcEMsTUFBRCxFQUFTLENBQVQsRUFBWSxLQUFLOUksTUFBakIsQ0FBWDtBQUNmLE1BQUk4SCxHQUFHLEdBQUcsS0FBS2dCLE1BQU0sR0FBRyxDQUFkLElBQW9CLEtBQUtBLE1BQUwsS0FBZ0IsQ0FBOUM7QUFDQSxTQUFRaEIsR0FBRyxHQUFHLE1BQVAsR0FBaUJBLEdBQUcsR0FBRyxVQUF2QixHQUFvQ0EsR0FBM0M7QUFDRCxDQUpEOztBQU1BOUksTUFBTSxDQUFDc0QsU0FBUCxDQUFpQjRKLFdBQWpCLEdBQStCLFNBQVNBLFdBQVQsQ0FBc0JwRCxNQUF0QixFQUE4QnVDLFFBQTlCLEVBQXdDO0FBQ3JFLE1BQUksQ0FBQ0EsUUFBTCxFQUFlSCxXQUFXLENBQUNwQyxNQUFELEVBQVMsQ0FBVCxFQUFZLEtBQUs5SSxNQUFqQixDQUFYO0FBRWYsU0FBUSxLQUFLOEksTUFBTCxDQUFELEdBQ0osS0FBS0EsTUFBTSxHQUFHLENBQWQsS0FBb0IsQ0FEaEIsR0FFSixLQUFLQSxNQUFNLEdBQUcsQ0FBZCxLQUFvQixFQUZoQixHQUdKLEtBQUtBLE1BQU0sR0FBRyxDQUFkLEtBQW9CLEVBSHZCO0FBSUQsQ0FQRDs7QUFTQTlKLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUI2SixXQUFqQixHQUErQixTQUFTQSxXQUFULENBQXNCckQsTUFBdEIsRUFBOEJ1QyxRQUE5QixFQUF3QztBQUNyRSxNQUFJLENBQUNBLFFBQUwsRUFBZUgsV0FBVyxDQUFDcEMsTUFBRCxFQUFTLENBQVQsRUFBWSxLQUFLOUksTUFBakIsQ0FBWDtBQUVmLFNBQVEsS0FBSzhJLE1BQUwsS0FBZ0IsRUFBakIsR0FDSixLQUFLQSxNQUFNLEdBQUcsQ0FBZCxLQUFvQixFQURoQixHQUVKLEtBQUtBLE1BQU0sR0FBRyxDQUFkLEtBQW9CLENBRmhCLEdBR0osS0FBS0EsTUFBTSxHQUFHLENBQWQsQ0FISDtBQUlELENBUEQ7O0FBU0E5SixNQUFNLENBQUNzRCxTQUFQLENBQWlCOEosV0FBakIsR0FBK0IsU0FBU0EsV0FBVCxDQUFzQnRELE1BQXRCLEVBQThCdUMsUUFBOUIsRUFBd0M7QUFDckUsTUFBSSxDQUFDQSxRQUFMLEVBQWVILFdBQVcsQ0FBQ3BDLE1BQUQsRUFBUyxDQUFULEVBQVksS0FBSzlJLE1BQWpCLENBQVg7QUFDZixTQUFPNEIsT0FBTyxDQUFDMkcsSUFBUixDQUFhLElBQWIsRUFBbUJPLE1BQW5CLEVBQTJCLElBQTNCLEVBQWlDLEVBQWpDLEVBQXFDLENBQXJDLENBQVA7QUFDRCxDQUhEOztBQUtBOUosTUFBTSxDQUFDc0QsU0FBUCxDQUFpQitKLFdBQWpCLEdBQStCLFNBQVNBLFdBQVQsQ0FBc0J2RCxNQUF0QixFQUE4QnVDLFFBQTlCLEVBQXdDO0FBQ3JFLE1BQUksQ0FBQ0EsUUFBTCxFQUFlSCxXQUFXLENBQUNwQyxNQUFELEVBQVMsQ0FBVCxFQUFZLEtBQUs5SSxNQUFqQixDQUFYO0FBQ2YsU0FBTzRCLE9BQU8sQ0FBQzJHLElBQVIsQ0FBYSxJQUFiLEVBQW1CTyxNQUFuQixFQUEyQixLQUEzQixFQUFrQyxFQUFsQyxFQUFzQyxDQUF0QyxDQUFQO0FBQ0QsQ0FIRDs7QUFLQTlKLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJnSyxZQUFqQixHQUFnQyxTQUFTQSxZQUFULENBQXVCeEQsTUFBdkIsRUFBK0J1QyxRQUEvQixFQUF5QztBQUN2RSxNQUFJLENBQUNBLFFBQUwsRUFBZUgsV0FBVyxDQUFDcEMsTUFBRCxFQUFTLENBQVQsRUFBWSxLQUFLOUksTUFBakIsQ0FBWDtBQUNmLFNBQU80QixPQUFPLENBQUMyRyxJQUFSLENBQWEsSUFBYixFQUFtQk8sTUFBbkIsRUFBMkIsSUFBM0IsRUFBaUMsRUFBakMsRUFBcUMsQ0FBckMsQ0FBUDtBQUNELENBSEQ7O0FBS0E5SixNQUFNLENBQUNzRCxTQUFQLENBQWlCaUssWUFBakIsR0FBZ0MsU0FBU0EsWUFBVCxDQUF1QnpELE1BQXZCLEVBQStCdUMsUUFBL0IsRUFBeUM7QUFDdkUsTUFBSSxDQUFDQSxRQUFMLEVBQWVILFdBQVcsQ0FBQ3BDLE1BQUQsRUFBUyxDQUFULEVBQVksS0FBSzlJLE1BQWpCLENBQVg7QUFDZixTQUFPNEIsT0FBTyxDQUFDMkcsSUFBUixDQUFhLElBQWIsRUFBbUJPLE1BQW5CLEVBQTJCLEtBQTNCLEVBQWtDLEVBQWxDLEVBQXNDLENBQXRDLENBQVA7QUFDRCxDQUhEOztBQUtBLFNBQVMwRCxRQUFULENBQW1CeEcsR0FBbkIsRUFBd0I5QyxLQUF4QixFQUErQjRGLE1BQS9CLEVBQXVDcUMsR0FBdkMsRUFBNEM3RCxHQUE1QyxFQUFpRDVCLEdBQWpELEVBQXNEO0FBQ3BELE1BQUksQ0FBQzFHLE1BQU0sQ0FBQzZGLFFBQVAsQ0FBZ0JtQixHQUFoQixDQUFMLEVBQTJCLE1BQU0sSUFBSTdDLFNBQUosQ0FBYyw2Q0FBZCxDQUFOO0FBQzNCLE1BQUlELEtBQUssR0FBR29FLEdBQVIsSUFBZXBFLEtBQUssR0FBR3dDLEdBQTNCLEVBQWdDLE1BQU0sSUFBSTlDLFVBQUosQ0FBZSxtQ0FBZixDQUFOO0FBQ2hDLE1BQUlrRyxNQUFNLEdBQUdxQyxHQUFULEdBQWVuRixHQUFHLENBQUNoRyxNQUF2QixFQUErQixNQUFNLElBQUk0QyxVQUFKLENBQWUsb0JBQWYsQ0FBTjtBQUNoQzs7QUFFRDVELE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJtSyxXQUFqQixHQUErQixTQUFTQSxXQUFULENBQXNCdkosS0FBdEIsRUFBNkI0RixNQUE3QixFQUFxQ3pKLFVBQXJDLEVBQWlEZ00sUUFBakQsRUFBMkQ7QUFDeEZuSSxPQUFLLEdBQUcsQ0FBQ0EsS0FBVDtBQUNBNEYsUUFBTSxHQUFHQSxNQUFNLEdBQUcsQ0FBbEI7QUFDQXpKLFlBQVUsR0FBR0EsVUFBVSxHQUFHLENBQTFCOztBQUNBLE1BQUksQ0FBQ2dNLFFBQUwsRUFBZTtBQUNiLFFBQUlxQixRQUFRLEdBQUdqSCxJQUFJLENBQUNvRyxHQUFMLENBQVMsQ0FBVCxFQUFZLElBQUl4TSxVQUFoQixJQUE4QixDQUE3QztBQUNBbU4sWUFBUSxDQUFDLElBQUQsRUFBT3RKLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0J6SixVQUF0QixFQUFrQ3FOLFFBQWxDLEVBQTRDLENBQTVDLENBQVI7QUFDRDs7QUFFRCxNQUFJcEIsR0FBRyxHQUFHLENBQVY7QUFDQSxNQUFJeEwsQ0FBQyxHQUFHLENBQVI7QUFDQSxPQUFLZ0osTUFBTCxJQUFlNUYsS0FBSyxHQUFHLElBQXZCOztBQUNBLFNBQU8sRUFBRXBELENBQUYsR0FBTVQsVUFBTixLQUFxQmlNLEdBQUcsSUFBSSxLQUE1QixDQUFQLEVBQTJDO0FBQ3pDLFNBQUt4QyxNQUFNLEdBQUdoSixDQUFkLElBQW9Cb0QsS0FBSyxHQUFHb0ksR0FBVCxHQUFnQixJQUFuQztBQUNEOztBQUVELFNBQU94QyxNQUFNLEdBQUd6SixVQUFoQjtBQUNELENBakJEOztBQW1CQUwsTUFBTSxDQUFDc0QsU0FBUCxDQUFpQnFLLFdBQWpCLEdBQStCLFNBQVNBLFdBQVQsQ0FBc0J6SixLQUF0QixFQUE2QjRGLE1BQTdCLEVBQXFDekosVUFBckMsRUFBaURnTSxRQUFqRCxFQUEyRDtBQUN4Rm5JLE9BQUssR0FBRyxDQUFDQSxLQUFUO0FBQ0E0RixRQUFNLEdBQUdBLE1BQU0sR0FBRyxDQUFsQjtBQUNBekosWUFBVSxHQUFHQSxVQUFVLEdBQUcsQ0FBMUI7O0FBQ0EsTUFBSSxDQUFDZ00sUUFBTCxFQUFlO0FBQ2IsUUFBSXFCLFFBQVEsR0FBR2pILElBQUksQ0FBQ29HLEdBQUwsQ0FBUyxDQUFULEVBQVksSUFBSXhNLFVBQWhCLElBQThCLENBQTdDO0FBQ0FtTixZQUFRLENBQUMsSUFBRCxFQUFPdEosS0FBUCxFQUFjNEYsTUFBZCxFQUFzQnpKLFVBQXRCLEVBQWtDcU4sUUFBbEMsRUFBNEMsQ0FBNUMsQ0FBUjtBQUNEOztBQUVELE1BQUk1TSxDQUFDLEdBQUdULFVBQVUsR0FBRyxDQUFyQjtBQUNBLE1BQUlpTSxHQUFHLEdBQUcsQ0FBVjtBQUNBLE9BQUt4QyxNQUFNLEdBQUdoSixDQUFkLElBQW1Cb0QsS0FBSyxHQUFHLElBQTNCOztBQUNBLFNBQU8sRUFBRXBELENBQUYsSUFBTyxDQUFQLEtBQWF3TCxHQUFHLElBQUksS0FBcEIsQ0FBUCxFQUFtQztBQUNqQyxTQUFLeEMsTUFBTSxHQUFHaEosQ0FBZCxJQUFvQm9ELEtBQUssR0FBR29JLEdBQVQsR0FBZ0IsSUFBbkM7QUFDRDs7QUFFRCxTQUFPeEMsTUFBTSxHQUFHekosVUFBaEI7QUFDRCxDQWpCRDs7QUFtQkFMLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJzSyxVQUFqQixHQUE4QixTQUFTQSxVQUFULENBQXFCMUosS0FBckIsRUFBNEI0RixNQUE1QixFQUFvQ3VDLFFBQXBDLEVBQThDO0FBQzFFbkksT0FBSyxHQUFHLENBQUNBLEtBQVQ7QUFDQTRGLFFBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCO0FBQ0EsTUFBSSxDQUFDdUMsUUFBTCxFQUFlbUIsUUFBUSxDQUFDLElBQUQsRUFBT3RKLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0IsQ0FBdEIsRUFBeUIsSUFBekIsRUFBK0IsQ0FBL0IsQ0FBUjtBQUNmLE1BQUksQ0FBQzlKLE1BQU0sQ0FBQ2dELG1CQUFaLEVBQWlDa0IsS0FBSyxHQUFHdUMsSUFBSSxDQUFDb0gsS0FBTCxDQUFXM0osS0FBWCxDQUFSO0FBQ2pDLE9BQUs0RixNQUFMLElBQWdCNUYsS0FBSyxHQUFHLElBQXhCO0FBQ0EsU0FBTzRGLE1BQU0sR0FBRyxDQUFoQjtBQUNELENBUEQ7O0FBU0EsU0FBU2dFLGlCQUFULENBQTRCOUcsR0FBNUIsRUFBaUM5QyxLQUFqQyxFQUF3QzRGLE1BQXhDLEVBQWdEaUUsWUFBaEQsRUFBOEQ7QUFDNUQsTUFBSTdKLEtBQUssR0FBRyxDQUFaLEVBQWVBLEtBQUssR0FBRyxTQUFTQSxLQUFULEdBQWlCLENBQXpCOztBQUNmLE9BQUssSUFBSXBELENBQUMsR0FBRyxDQUFSLEVBQVc2SSxDQUFDLEdBQUdsRCxJQUFJLENBQUNDLEdBQUwsQ0FBU00sR0FBRyxDQUFDaEcsTUFBSixHQUFhOEksTUFBdEIsRUFBOEIsQ0FBOUIsQ0FBcEIsRUFBc0RoSixDQUFDLEdBQUc2SSxDQUExRCxFQUE2RCxFQUFFN0ksQ0FBL0QsRUFBa0U7QUFDaEVrRyxPQUFHLENBQUM4QyxNQUFNLEdBQUdoSixDQUFWLENBQUgsR0FBa0IsQ0FBQ29ELEtBQUssR0FBSSxRQUFTLEtBQUs2SixZQUFZLEdBQUdqTixDQUFILEdBQU8sSUFBSUEsQ0FBNUIsQ0FBbkIsTUFDaEIsQ0FBQ2lOLFlBQVksR0FBR2pOLENBQUgsR0FBTyxJQUFJQSxDQUF4QixJQUE2QixDQUQvQjtBQUVEO0FBQ0Y7O0FBRURkLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUIwSyxhQUFqQixHQUFpQyxTQUFTQSxhQUFULENBQXdCOUosS0FBeEIsRUFBK0I0RixNQUEvQixFQUF1Q3VDLFFBQXZDLEVBQWlEO0FBQ2hGbkksT0FBSyxHQUFHLENBQUNBLEtBQVQ7QUFDQTRGLFFBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCO0FBQ0EsTUFBSSxDQUFDdUMsUUFBTCxFQUFlbUIsUUFBUSxDQUFDLElBQUQsRUFBT3RKLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0IsQ0FBdEIsRUFBeUIsTUFBekIsRUFBaUMsQ0FBakMsQ0FBUjs7QUFDZixNQUFJOUosTUFBTSxDQUFDZ0QsbUJBQVgsRUFBZ0M7QUFDOUIsU0FBSzhHLE1BQUwsSUFBZ0I1RixLQUFLLEdBQUcsSUFBeEI7QUFDQSxTQUFLNEYsTUFBTSxHQUFHLENBQWQsSUFBb0I1RixLQUFLLEtBQUssQ0FBOUI7QUFDRCxHQUhELE1BR087QUFDTDRKLHFCQUFpQixDQUFDLElBQUQsRUFBTzVKLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0IsSUFBdEIsQ0FBakI7QUFDRDs7QUFDRCxTQUFPQSxNQUFNLEdBQUcsQ0FBaEI7QUFDRCxDQVhEOztBQWFBOUosTUFBTSxDQUFDc0QsU0FBUCxDQUFpQjJLLGFBQWpCLEdBQWlDLFNBQVNBLGFBQVQsQ0FBd0IvSixLQUF4QixFQUErQjRGLE1BQS9CLEVBQXVDdUMsUUFBdkMsRUFBaUQ7QUFDaEZuSSxPQUFLLEdBQUcsQ0FBQ0EsS0FBVDtBQUNBNEYsUUFBTSxHQUFHQSxNQUFNLEdBQUcsQ0FBbEI7QUFDQSxNQUFJLENBQUN1QyxRQUFMLEVBQWVtQixRQUFRLENBQUMsSUFBRCxFQUFPdEosS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixDQUF0QixFQUF5QixNQUF6QixFQUFpQyxDQUFqQyxDQUFSOztBQUNmLE1BQUk5SixNQUFNLENBQUNnRCxtQkFBWCxFQUFnQztBQUM5QixTQUFLOEcsTUFBTCxJQUFnQjVGLEtBQUssS0FBSyxDQUExQjtBQUNBLFNBQUs0RixNQUFNLEdBQUcsQ0FBZCxJQUFvQjVGLEtBQUssR0FBRyxJQUE1QjtBQUNELEdBSEQsTUFHTztBQUNMNEoscUJBQWlCLENBQUMsSUFBRCxFQUFPNUosS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixLQUF0QixDQUFqQjtBQUNEOztBQUNELFNBQU9BLE1BQU0sR0FBRyxDQUFoQjtBQUNELENBWEQ7O0FBYUEsU0FBU29FLGlCQUFULENBQTRCbEgsR0FBNUIsRUFBaUM5QyxLQUFqQyxFQUF3QzRGLE1BQXhDLEVBQWdEaUUsWUFBaEQsRUFBOEQ7QUFDNUQsTUFBSTdKLEtBQUssR0FBRyxDQUFaLEVBQWVBLEtBQUssR0FBRyxhQUFhQSxLQUFiLEdBQXFCLENBQTdCOztBQUNmLE9BQUssSUFBSXBELENBQUMsR0FBRyxDQUFSLEVBQVc2SSxDQUFDLEdBQUdsRCxJQUFJLENBQUNDLEdBQUwsQ0FBU00sR0FBRyxDQUFDaEcsTUFBSixHQUFhOEksTUFBdEIsRUFBOEIsQ0FBOUIsQ0FBcEIsRUFBc0RoSixDQUFDLEdBQUc2SSxDQUExRCxFQUE2RCxFQUFFN0ksQ0FBL0QsRUFBa0U7QUFDaEVrRyxPQUFHLENBQUM4QyxNQUFNLEdBQUdoSixDQUFWLENBQUgsR0FBbUJvRCxLQUFLLEtBQUssQ0FBQzZKLFlBQVksR0FBR2pOLENBQUgsR0FBTyxJQUFJQSxDQUF4QixJQUE2QixDQUF4QyxHQUE2QyxJQUEvRDtBQUNEO0FBQ0Y7O0FBRURkLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUI2SyxhQUFqQixHQUFpQyxTQUFTQSxhQUFULENBQXdCakssS0FBeEIsRUFBK0I0RixNQUEvQixFQUF1Q3VDLFFBQXZDLEVBQWlEO0FBQ2hGbkksT0FBSyxHQUFHLENBQUNBLEtBQVQ7QUFDQTRGLFFBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCO0FBQ0EsTUFBSSxDQUFDdUMsUUFBTCxFQUFlbUIsUUFBUSxDQUFDLElBQUQsRUFBT3RKLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0IsQ0FBdEIsRUFBeUIsVUFBekIsRUFBcUMsQ0FBckMsQ0FBUjs7QUFDZixNQUFJOUosTUFBTSxDQUFDZ0QsbUJBQVgsRUFBZ0M7QUFDOUIsU0FBSzhHLE1BQU0sR0FBRyxDQUFkLElBQW9CNUYsS0FBSyxLQUFLLEVBQTlCO0FBQ0EsU0FBSzRGLE1BQU0sR0FBRyxDQUFkLElBQW9CNUYsS0FBSyxLQUFLLEVBQTlCO0FBQ0EsU0FBSzRGLE1BQU0sR0FBRyxDQUFkLElBQW9CNUYsS0FBSyxLQUFLLENBQTlCO0FBQ0EsU0FBSzRGLE1BQUwsSUFBZ0I1RixLQUFLLEdBQUcsSUFBeEI7QUFDRCxHQUxELE1BS087QUFDTGdLLHFCQUFpQixDQUFDLElBQUQsRUFBT2hLLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0IsSUFBdEIsQ0FBakI7QUFDRDs7QUFDRCxTQUFPQSxNQUFNLEdBQUcsQ0FBaEI7QUFDRCxDQWJEOztBQWVBOUosTUFBTSxDQUFDc0QsU0FBUCxDQUFpQjhLLGFBQWpCLEdBQWlDLFNBQVNBLGFBQVQsQ0FBd0JsSyxLQUF4QixFQUErQjRGLE1BQS9CLEVBQXVDdUMsUUFBdkMsRUFBaUQ7QUFDaEZuSSxPQUFLLEdBQUcsQ0FBQ0EsS0FBVDtBQUNBNEYsUUFBTSxHQUFHQSxNQUFNLEdBQUcsQ0FBbEI7QUFDQSxNQUFJLENBQUN1QyxRQUFMLEVBQWVtQixRQUFRLENBQUMsSUFBRCxFQUFPdEosS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixDQUF0QixFQUF5QixVQUF6QixFQUFxQyxDQUFyQyxDQUFSOztBQUNmLE1BQUk5SixNQUFNLENBQUNnRCxtQkFBWCxFQUFnQztBQUM5QixTQUFLOEcsTUFBTCxJQUFnQjVGLEtBQUssS0FBSyxFQUExQjtBQUNBLFNBQUs0RixNQUFNLEdBQUcsQ0FBZCxJQUFvQjVGLEtBQUssS0FBSyxFQUE5QjtBQUNBLFNBQUs0RixNQUFNLEdBQUcsQ0FBZCxJQUFvQjVGLEtBQUssS0FBSyxDQUE5QjtBQUNBLFNBQUs0RixNQUFNLEdBQUcsQ0FBZCxJQUFvQjVGLEtBQUssR0FBRyxJQUE1QjtBQUNELEdBTEQsTUFLTztBQUNMZ0sscUJBQWlCLENBQUMsSUFBRCxFQUFPaEssS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixLQUF0QixDQUFqQjtBQUNEOztBQUNELFNBQU9BLE1BQU0sR0FBRyxDQUFoQjtBQUNELENBYkQ7O0FBZUE5SixNQUFNLENBQUNzRCxTQUFQLENBQWlCK0ssVUFBakIsR0FBOEIsU0FBU0EsVUFBVCxDQUFxQm5LLEtBQXJCLEVBQTRCNEYsTUFBNUIsRUFBb0N6SixVQUFwQyxFQUFnRGdNLFFBQWhELEVBQTBEO0FBQ3RGbkksT0FBSyxHQUFHLENBQUNBLEtBQVQ7QUFDQTRGLFFBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCOztBQUNBLE1BQUksQ0FBQ3VDLFFBQUwsRUFBZTtBQUNiLFFBQUlpQyxLQUFLLEdBQUc3SCxJQUFJLENBQUNvRyxHQUFMLENBQVMsQ0FBVCxFQUFZLElBQUl4TSxVQUFKLEdBQWlCLENBQTdCLENBQVo7QUFFQW1OLFlBQVEsQ0FBQyxJQUFELEVBQU90SixLQUFQLEVBQWM0RixNQUFkLEVBQXNCekosVUFBdEIsRUFBa0NpTyxLQUFLLEdBQUcsQ0FBMUMsRUFBNkMsQ0FBQ0EsS0FBOUMsQ0FBUjtBQUNEOztBQUVELE1BQUl4TixDQUFDLEdBQUcsQ0FBUjtBQUNBLE1BQUl3TCxHQUFHLEdBQUcsQ0FBVjtBQUNBLE1BQUlpQyxHQUFHLEdBQUcsQ0FBVjtBQUNBLE9BQUt6RSxNQUFMLElBQWU1RixLQUFLLEdBQUcsSUFBdkI7O0FBQ0EsU0FBTyxFQUFFcEQsQ0FBRixHQUFNVCxVQUFOLEtBQXFCaU0sR0FBRyxJQUFJLEtBQTVCLENBQVAsRUFBMkM7QUFDekMsUUFBSXBJLEtBQUssR0FBRyxDQUFSLElBQWFxSyxHQUFHLEtBQUssQ0FBckIsSUFBMEIsS0FBS3pFLE1BQU0sR0FBR2hKLENBQVQsR0FBYSxDQUFsQixNQUF5QixDQUF2RCxFQUEwRDtBQUN4RHlOLFNBQUcsR0FBRyxDQUFOO0FBQ0Q7O0FBQ0QsU0FBS3pFLE1BQU0sR0FBR2hKLENBQWQsSUFBbUIsQ0FBRW9ELEtBQUssR0FBR29JLEdBQVQsSUFBaUIsQ0FBbEIsSUFBdUJpQyxHQUF2QixHQUE2QixJQUFoRDtBQUNEOztBQUVELFNBQU96RSxNQUFNLEdBQUd6SixVQUFoQjtBQUNELENBckJEOztBQXVCQUwsTUFBTSxDQUFDc0QsU0FBUCxDQUFpQmtMLFVBQWpCLEdBQThCLFNBQVNBLFVBQVQsQ0FBcUJ0SyxLQUFyQixFQUE0QjRGLE1BQTVCLEVBQW9DekosVUFBcEMsRUFBZ0RnTSxRQUFoRCxFQUEwRDtBQUN0Rm5JLE9BQUssR0FBRyxDQUFDQSxLQUFUO0FBQ0E0RixRQUFNLEdBQUdBLE1BQU0sR0FBRyxDQUFsQjs7QUFDQSxNQUFJLENBQUN1QyxRQUFMLEVBQWU7QUFDYixRQUFJaUMsS0FBSyxHQUFHN0gsSUFBSSxDQUFDb0csR0FBTCxDQUFTLENBQVQsRUFBWSxJQUFJeE0sVUFBSixHQUFpQixDQUE3QixDQUFaO0FBRUFtTixZQUFRLENBQUMsSUFBRCxFQUFPdEosS0FBUCxFQUFjNEYsTUFBZCxFQUFzQnpKLFVBQXRCLEVBQWtDaU8sS0FBSyxHQUFHLENBQTFDLEVBQTZDLENBQUNBLEtBQTlDLENBQVI7QUFDRDs7QUFFRCxNQUFJeE4sQ0FBQyxHQUFHVCxVQUFVLEdBQUcsQ0FBckI7QUFDQSxNQUFJaU0sR0FBRyxHQUFHLENBQVY7QUFDQSxNQUFJaUMsR0FBRyxHQUFHLENBQVY7QUFDQSxPQUFLekUsTUFBTSxHQUFHaEosQ0FBZCxJQUFtQm9ELEtBQUssR0FBRyxJQUEzQjs7QUFDQSxTQUFPLEVBQUVwRCxDQUFGLElBQU8sQ0FBUCxLQUFhd0wsR0FBRyxJQUFJLEtBQXBCLENBQVAsRUFBbUM7QUFDakMsUUFBSXBJLEtBQUssR0FBRyxDQUFSLElBQWFxSyxHQUFHLEtBQUssQ0FBckIsSUFBMEIsS0FBS3pFLE1BQU0sR0FBR2hKLENBQVQsR0FBYSxDQUFsQixNQUF5QixDQUF2RCxFQUEwRDtBQUN4RHlOLFNBQUcsR0FBRyxDQUFOO0FBQ0Q7O0FBQ0QsU0FBS3pFLE1BQU0sR0FBR2hKLENBQWQsSUFBbUIsQ0FBRW9ELEtBQUssR0FBR29JLEdBQVQsSUFBaUIsQ0FBbEIsSUFBdUJpQyxHQUF2QixHQUE2QixJQUFoRDtBQUNEOztBQUVELFNBQU96RSxNQUFNLEdBQUd6SixVQUFoQjtBQUNELENBckJEOztBQXVCQUwsTUFBTSxDQUFDc0QsU0FBUCxDQUFpQm1MLFNBQWpCLEdBQTZCLFNBQVNBLFNBQVQsQ0FBb0J2SyxLQUFwQixFQUEyQjRGLE1BQTNCLEVBQW1DdUMsUUFBbkMsRUFBNkM7QUFDeEVuSSxPQUFLLEdBQUcsQ0FBQ0EsS0FBVDtBQUNBNEYsUUFBTSxHQUFHQSxNQUFNLEdBQUcsQ0FBbEI7QUFDQSxNQUFJLENBQUN1QyxRQUFMLEVBQWVtQixRQUFRLENBQUMsSUFBRCxFQUFPdEosS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixDQUF0QixFQUF5QixJQUF6QixFQUErQixDQUFDLElBQWhDLENBQVI7QUFDZixNQUFJLENBQUM5SixNQUFNLENBQUNnRCxtQkFBWixFQUFpQ2tCLEtBQUssR0FBR3VDLElBQUksQ0FBQ29ILEtBQUwsQ0FBVzNKLEtBQVgsQ0FBUjtBQUNqQyxNQUFJQSxLQUFLLEdBQUcsQ0FBWixFQUFlQSxLQUFLLEdBQUcsT0FBT0EsS0FBUCxHQUFlLENBQXZCO0FBQ2YsT0FBSzRGLE1BQUwsSUFBZ0I1RixLQUFLLEdBQUcsSUFBeEI7QUFDQSxTQUFPNEYsTUFBTSxHQUFHLENBQWhCO0FBQ0QsQ0FSRDs7QUFVQTlKLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJvTCxZQUFqQixHQUFnQyxTQUFTQSxZQUFULENBQXVCeEssS0FBdkIsRUFBOEI0RixNQUE5QixFQUFzQ3VDLFFBQXRDLEVBQWdEO0FBQzlFbkksT0FBSyxHQUFHLENBQUNBLEtBQVQ7QUFDQTRGLFFBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCO0FBQ0EsTUFBSSxDQUFDdUMsUUFBTCxFQUFlbUIsUUFBUSxDQUFDLElBQUQsRUFBT3RKLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0IsQ0FBdEIsRUFBeUIsTUFBekIsRUFBaUMsQ0FBQyxNQUFsQyxDQUFSOztBQUNmLE1BQUk5SixNQUFNLENBQUNnRCxtQkFBWCxFQUFnQztBQUM5QixTQUFLOEcsTUFBTCxJQUFnQjVGLEtBQUssR0FBRyxJQUF4QjtBQUNBLFNBQUs0RixNQUFNLEdBQUcsQ0FBZCxJQUFvQjVGLEtBQUssS0FBSyxDQUE5QjtBQUNELEdBSEQsTUFHTztBQUNMNEoscUJBQWlCLENBQUMsSUFBRCxFQUFPNUosS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixJQUF0QixDQUFqQjtBQUNEOztBQUNELFNBQU9BLE1BQU0sR0FBRyxDQUFoQjtBQUNELENBWEQ7O0FBYUE5SixNQUFNLENBQUNzRCxTQUFQLENBQWlCcUwsWUFBakIsR0FBZ0MsU0FBU0EsWUFBVCxDQUF1QnpLLEtBQXZCLEVBQThCNEYsTUFBOUIsRUFBc0N1QyxRQUF0QyxFQUFnRDtBQUM5RW5JLE9BQUssR0FBRyxDQUFDQSxLQUFUO0FBQ0E0RixRQUFNLEdBQUdBLE1BQU0sR0FBRyxDQUFsQjtBQUNBLE1BQUksQ0FBQ3VDLFFBQUwsRUFBZW1CLFFBQVEsQ0FBQyxJQUFELEVBQU90SixLQUFQLEVBQWM0RixNQUFkLEVBQXNCLENBQXRCLEVBQXlCLE1BQXpCLEVBQWlDLENBQUMsTUFBbEMsQ0FBUjs7QUFDZixNQUFJOUosTUFBTSxDQUFDZ0QsbUJBQVgsRUFBZ0M7QUFDOUIsU0FBSzhHLE1BQUwsSUFBZ0I1RixLQUFLLEtBQUssQ0FBMUI7QUFDQSxTQUFLNEYsTUFBTSxHQUFHLENBQWQsSUFBb0I1RixLQUFLLEdBQUcsSUFBNUI7QUFDRCxHQUhELE1BR087QUFDTDRKLHFCQUFpQixDQUFDLElBQUQsRUFBTzVKLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0IsS0FBdEIsQ0FBakI7QUFDRDs7QUFDRCxTQUFPQSxNQUFNLEdBQUcsQ0FBaEI7QUFDRCxDQVhEOztBQWFBOUosTUFBTSxDQUFDc0QsU0FBUCxDQUFpQnNMLFlBQWpCLEdBQWdDLFNBQVNBLFlBQVQsQ0FBdUIxSyxLQUF2QixFQUE4QjRGLE1BQTlCLEVBQXNDdUMsUUFBdEMsRUFBZ0Q7QUFDOUVuSSxPQUFLLEdBQUcsQ0FBQ0EsS0FBVDtBQUNBNEYsUUFBTSxHQUFHQSxNQUFNLEdBQUcsQ0FBbEI7QUFDQSxNQUFJLENBQUN1QyxRQUFMLEVBQWVtQixRQUFRLENBQUMsSUFBRCxFQUFPdEosS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixDQUF0QixFQUF5QixVQUF6QixFQUFxQyxDQUFDLFVBQXRDLENBQVI7O0FBQ2YsTUFBSTlKLE1BQU0sQ0FBQ2dELG1CQUFYLEVBQWdDO0FBQzlCLFNBQUs4RyxNQUFMLElBQWdCNUYsS0FBSyxHQUFHLElBQXhCO0FBQ0EsU0FBSzRGLE1BQU0sR0FBRyxDQUFkLElBQW9CNUYsS0FBSyxLQUFLLENBQTlCO0FBQ0EsU0FBSzRGLE1BQU0sR0FBRyxDQUFkLElBQW9CNUYsS0FBSyxLQUFLLEVBQTlCO0FBQ0EsU0FBSzRGLE1BQU0sR0FBRyxDQUFkLElBQW9CNUYsS0FBSyxLQUFLLEVBQTlCO0FBQ0QsR0FMRCxNQUtPO0FBQ0xnSyxxQkFBaUIsQ0FBQyxJQUFELEVBQU9oSyxLQUFQLEVBQWM0RixNQUFkLEVBQXNCLElBQXRCLENBQWpCO0FBQ0Q7O0FBQ0QsU0FBT0EsTUFBTSxHQUFHLENBQWhCO0FBQ0QsQ0FiRDs7QUFlQTlKLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUJ1TCxZQUFqQixHQUFnQyxTQUFTQSxZQUFULENBQXVCM0ssS0FBdkIsRUFBOEI0RixNQUE5QixFQUFzQ3VDLFFBQXRDLEVBQWdEO0FBQzlFbkksT0FBSyxHQUFHLENBQUNBLEtBQVQ7QUFDQTRGLFFBQU0sR0FBR0EsTUFBTSxHQUFHLENBQWxCO0FBQ0EsTUFBSSxDQUFDdUMsUUFBTCxFQUFlbUIsUUFBUSxDQUFDLElBQUQsRUFBT3RKLEtBQVAsRUFBYzRGLE1BQWQsRUFBc0IsQ0FBdEIsRUFBeUIsVUFBekIsRUFBcUMsQ0FBQyxVQUF0QyxDQUFSO0FBQ2YsTUFBSTVGLEtBQUssR0FBRyxDQUFaLEVBQWVBLEtBQUssR0FBRyxhQUFhQSxLQUFiLEdBQXFCLENBQTdCOztBQUNmLE1BQUlsRSxNQUFNLENBQUNnRCxtQkFBWCxFQUFnQztBQUM5QixTQUFLOEcsTUFBTCxJQUFnQjVGLEtBQUssS0FBSyxFQUExQjtBQUNBLFNBQUs0RixNQUFNLEdBQUcsQ0FBZCxJQUFvQjVGLEtBQUssS0FBSyxFQUE5QjtBQUNBLFNBQUs0RixNQUFNLEdBQUcsQ0FBZCxJQUFvQjVGLEtBQUssS0FBSyxDQUE5QjtBQUNBLFNBQUs0RixNQUFNLEdBQUcsQ0FBZCxJQUFvQjVGLEtBQUssR0FBRyxJQUE1QjtBQUNELEdBTEQsTUFLTztBQUNMZ0sscUJBQWlCLENBQUMsSUFBRCxFQUFPaEssS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixLQUF0QixDQUFqQjtBQUNEOztBQUNELFNBQU9BLE1BQU0sR0FBRyxDQUFoQjtBQUNELENBZEQ7O0FBZ0JBLFNBQVNnRixZQUFULENBQXVCOUgsR0FBdkIsRUFBNEI5QyxLQUE1QixFQUFtQzRGLE1BQW5DLEVBQTJDcUMsR0FBM0MsRUFBZ0Q3RCxHQUFoRCxFQUFxRDVCLEdBQXJELEVBQTBEO0FBQ3hELE1BQUlvRCxNQUFNLEdBQUdxQyxHQUFULEdBQWVuRixHQUFHLENBQUNoRyxNQUF2QixFQUErQixNQUFNLElBQUk0QyxVQUFKLENBQWUsb0JBQWYsQ0FBTjtBQUMvQixNQUFJa0csTUFBTSxHQUFHLENBQWIsRUFBZ0IsTUFBTSxJQUFJbEcsVUFBSixDQUFlLG9CQUFmLENBQU47QUFDakI7O0FBRUQsU0FBU21MLFVBQVQsQ0FBcUIvSCxHQUFyQixFQUEwQjlDLEtBQTFCLEVBQWlDNEYsTUFBakMsRUFBeUNpRSxZQUF6QyxFQUF1RDFCLFFBQXZELEVBQWlFO0FBQy9ELE1BQUksQ0FBQ0EsUUFBTCxFQUFlO0FBQ2J5QyxnQkFBWSxDQUFDOUgsR0FBRCxFQUFNOUMsS0FBTixFQUFhNEYsTUFBYixFQUFxQixDQUFyQixFQUF3QixzQkFBeEIsRUFBZ0QsQ0FBQyxzQkFBakQsQ0FBWjtBQUNEOztBQUNEbEgsU0FBTyxDQUFDMkMsS0FBUixDQUFjeUIsR0FBZCxFQUFtQjlDLEtBQW5CLEVBQTBCNEYsTUFBMUIsRUFBa0NpRSxZQUFsQyxFQUFnRCxFQUFoRCxFQUFvRCxDQUFwRDtBQUNBLFNBQU9qRSxNQUFNLEdBQUcsQ0FBaEI7QUFDRDs7QUFFRDlKLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUIwTCxZQUFqQixHQUFnQyxTQUFTQSxZQUFULENBQXVCOUssS0FBdkIsRUFBOEI0RixNQUE5QixFQUFzQ3VDLFFBQXRDLEVBQWdEO0FBQzlFLFNBQU8wQyxVQUFVLENBQUMsSUFBRCxFQUFPN0ssS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixJQUF0QixFQUE0QnVDLFFBQTVCLENBQWpCO0FBQ0QsQ0FGRDs7QUFJQXJNLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUIyTCxZQUFqQixHQUFnQyxTQUFTQSxZQUFULENBQXVCL0ssS0FBdkIsRUFBOEI0RixNQUE5QixFQUFzQ3VDLFFBQXRDLEVBQWdEO0FBQzlFLFNBQU8wQyxVQUFVLENBQUMsSUFBRCxFQUFPN0ssS0FBUCxFQUFjNEYsTUFBZCxFQUFzQixLQUF0QixFQUE2QnVDLFFBQTdCLENBQWpCO0FBQ0QsQ0FGRDs7QUFJQSxTQUFTNkMsV0FBVCxDQUFzQmxJLEdBQXRCLEVBQTJCOUMsS0FBM0IsRUFBa0M0RixNQUFsQyxFQUEwQ2lFLFlBQTFDLEVBQXdEMUIsUUFBeEQsRUFBa0U7QUFDaEUsTUFBSSxDQUFDQSxRQUFMLEVBQWU7QUFDYnlDLGdCQUFZLENBQUM5SCxHQUFELEVBQU05QyxLQUFOLEVBQWE0RixNQUFiLEVBQXFCLENBQXJCLEVBQXdCLHVCQUF4QixFQUFpRCxDQUFDLHVCQUFsRCxDQUFaO0FBQ0Q7O0FBQ0RsSCxTQUFPLENBQUMyQyxLQUFSLENBQWN5QixHQUFkLEVBQW1COUMsS0FBbkIsRUFBMEI0RixNQUExQixFQUFrQ2lFLFlBQWxDLEVBQWdELEVBQWhELEVBQW9ELENBQXBEO0FBQ0EsU0FBT2pFLE1BQU0sR0FBRyxDQUFoQjtBQUNEOztBQUVEOUosTUFBTSxDQUFDc0QsU0FBUCxDQUFpQjZMLGFBQWpCLEdBQWlDLFNBQVNBLGFBQVQsQ0FBd0JqTCxLQUF4QixFQUErQjRGLE1BQS9CLEVBQXVDdUMsUUFBdkMsRUFBaUQ7QUFDaEYsU0FBTzZDLFdBQVcsQ0FBQyxJQUFELEVBQU9oTCxLQUFQLEVBQWM0RixNQUFkLEVBQXNCLElBQXRCLEVBQTRCdUMsUUFBNUIsQ0FBbEI7QUFDRCxDQUZEOztBQUlBck0sTUFBTSxDQUFDc0QsU0FBUCxDQUFpQjhMLGFBQWpCLEdBQWlDLFNBQVNBLGFBQVQsQ0FBd0JsTCxLQUF4QixFQUErQjRGLE1BQS9CLEVBQXVDdUMsUUFBdkMsRUFBaUQ7QUFDaEYsU0FBTzZDLFdBQVcsQ0FBQyxJQUFELEVBQU9oTCxLQUFQLEVBQWM0RixNQUFkLEVBQXNCLEtBQXRCLEVBQTZCdUMsUUFBN0IsQ0FBbEI7QUFDRCxDQUZELEMsQ0FJQTs7O0FBQ0FyTSxNQUFNLENBQUNzRCxTQUFQLENBQWlCd0MsSUFBakIsR0FBd0IsU0FBU0EsSUFBVCxDQUFlMEMsTUFBZixFQUF1QjZHLFdBQXZCLEVBQW9DcE4sS0FBcEMsRUFBMkNDLEdBQTNDLEVBQWdEO0FBQ3RFLE1BQUksQ0FBQ0QsS0FBTCxFQUFZQSxLQUFLLEdBQUcsQ0FBUjtBQUNaLE1BQUksQ0FBQ0MsR0FBRCxJQUFRQSxHQUFHLEtBQUssQ0FBcEIsRUFBdUJBLEdBQUcsR0FBRyxLQUFLbEIsTUFBWDtBQUN2QixNQUFJcU8sV0FBVyxJQUFJN0csTUFBTSxDQUFDeEgsTUFBMUIsRUFBa0NxTyxXQUFXLEdBQUc3RyxNQUFNLENBQUN4SCxNQUFyQjtBQUNsQyxNQUFJLENBQUNxTyxXQUFMLEVBQWtCQSxXQUFXLEdBQUcsQ0FBZDtBQUNsQixNQUFJbk4sR0FBRyxHQUFHLENBQU4sSUFBV0EsR0FBRyxHQUFHRCxLQUFyQixFQUE0QkMsR0FBRyxHQUFHRCxLQUFOLENBTDBDLENBT3RFOztBQUNBLE1BQUlDLEdBQUcsS0FBS0QsS0FBWixFQUFtQixPQUFPLENBQVA7QUFDbkIsTUFBSXVHLE1BQU0sQ0FBQ3hILE1BQVAsS0FBa0IsQ0FBbEIsSUFBdUIsS0FBS0EsTUFBTCxLQUFnQixDQUEzQyxFQUE4QyxPQUFPLENBQVAsQ0FUd0IsQ0FXdEU7O0FBQ0EsTUFBSXFPLFdBQVcsR0FBRyxDQUFsQixFQUFxQjtBQUNuQixVQUFNLElBQUl6TCxVQUFKLENBQWUsMkJBQWYsQ0FBTjtBQUNEOztBQUNELE1BQUkzQixLQUFLLEdBQUcsQ0FBUixJQUFhQSxLQUFLLElBQUksS0FBS2pCLE1BQS9CLEVBQXVDLE1BQU0sSUFBSTRDLFVBQUosQ0FBZSwyQkFBZixDQUFOO0FBQ3ZDLE1BQUkxQixHQUFHLEdBQUcsQ0FBVixFQUFhLE1BQU0sSUFBSTBCLFVBQUosQ0FBZSx5QkFBZixDQUFOLENBaEJ5RCxDQWtCdEU7O0FBQ0EsTUFBSTFCLEdBQUcsR0FBRyxLQUFLbEIsTUFBZixFQUF1QmtCLEdBQUcsR0FBRyxLQUFLbEIsTUFBWDs7QUFDdkIsTUFBSXdILE1BQU0sQ0FBQ3hILE1BQVAsR0FBZ0JxTyxXQUFoQixHQUE4Qm5OLEdBQUcsR0FBR0QsS0FBeEMsRUFBK0M7QUFDN0NDLE9BQUcsR0FBR3NHLE1BQU0sQ0FBQ3hILE1BQVAsR0FBZ0JxTyxXQUFoQixHQUE4QnBOLEtBQXBDO0FBQ0Q7O0FBRUQsTUFBSWxCLEdBQUcsR0FBR21CLEdBQUcsR0FBR0QsS0FBaEI7QUFDQSxNQUFJbkIsQ0FBSjs7QUFFQSxNQUFJLFNBQVMwSCxNQUFULElBQW1CdkcsS0FBSyxHQUFHb04sV0FBM0IsSUFBMENBLFdBQVcsR0FBR25OLEdBQTVELEVBQWlFO0FBQy9EO0FBQ0EsU0FBS3BCLENBQUMsR0FBR0MsR0FBRyxHQUFHLENBQWYsRUFBa0JELENBQUMsSUFBSSxDQUF2QixFQUEwQixFQUFFQSxDQUE1QixFQUErQjtBQUM3QjBILFlBQU0sQ0FBQzFILENBQUMsR0FBR3VPLFdBQUwsQ0FBTixHQUEwQixLQUFLdk8sQ0FBQyxHQUFHbUIsS0FBVCxDQUExQjtBQUNEO0FBQ0YsR0FMRCxNQUtPLElBQUlsQixHQUFHLEdBQUcsSUFBTixJQUFjLENBQUNmLE1BQU0sQ0FBQ2dELG1CQUExQixFQUErQztBQUNwRDtBQUNBLFNBQUtsQyxDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUdDLEdBQWhCLEVBQXFCLEVBQUVELENBQXZCLEVBQTBCO0FBQ3hCMEgsWUFBTSxDQUFDMUgsQ0FBQyxHQUFHdU8sV0FBTCxDQUFOLEdBQTBCLEtBQUt2TyxDQUFDLEdBQUdtQixLQUFULENBQTFCO0FBQ0Q7QUFDRixHQUxNLE1BS0E7QUFDTHRCLGNBQVUsQ0FBQzJDLFNBQVgsQ0FBcUJnTSxHQUFyQixDQUF5QnBHLElBQXpCLENBQ0VWLE1BREYsRUFFRSxLQUFLaEYsUUFBTCxDQUFjdkIsS0FBZCxFQUFxQkEsS0FBSyxHQUFHbEIsR0FBN0IsQ0FGRixFQUdFc08sV0FIRjtBQUtEOztBQUVELFNBQU90TyxHQUFQO0FBQ0QsQ0E5Q0QsQyxDQWdEQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0FmLE1BQU0sQ0FBQ3NELFNBQVAsQ0FBaUIwQixJQUFqQixHQUF3QixTQUFTQSxJQUFULENBQWU4RCxHQUFmLEVBQW9CN0csS0FBcEIsRUFBMkJDLEdBQTNCLEVBQWdDK0MsUUFBaEMsRUFBMEM7QUFDaEU7QUFDQSxNQUFJLE9BQU82RCxHQUFQLEtBQWUsUUFBbkIsRUFBNkI7QUFDM0IsUUFBSSxPQUFPN0csS0FBUCxLQUFpQixRQUFyQixFQUErQjtBQUM3QmdELGNBQVEsR0FBR2hELEtBQVg7QUFDQUEsV0FBSyxHQUFHLENBQVI7QUFDQUMsU0FBRyxHQUFHLEtBQUtsQixNQUFYO0FBQ0QsS0FKRCxNQUlPLElBQUksT0FBT2tCLEdBQVAsS0FBZSxRQUFuQixFQUE2QjtBQUNsQytDLGNBQVEsR0FBRy9DLEdBQVg7QUFDQUEsU0FBRyxHQUFHLEtBQUtsQixNQUFYO0FBQ0Q7O0FBQ0QsUUFBSThILEdBQUcsQ0FBQzlILE1BQUosS0FBZSxDQUFuQixFQUFzQjtBQUNwQixVQUFJSCxJQUFJLEdBQUdpSSxHQUFHLENBQUM3SCxVQUFKLENBQWUsQ0FBZixDQUFYOztBQUNBLFVBQUlKLElBQUksR0FBRyxHQUFYLEVBQWdCO0FBQ2RpSSxXQUFHLEdBQUdqSSxJQUFOO0FBQ0Q7QUFDRjs7QUFDRCxRQUFJb0UsUUFBUSxLQUFLL0IsU0FBYixJQUEwQixPQUFPK0IsUUFBUCxLQUFvQixRQUFsRCxFQUE0RDtBQUMxRCxZQUFNLElBQUlkLFNBQUosQ0FBYywyQkFBZCxDQUFOO0FBQ0Q7O0FBQ0QsUUFBSSxPQUFPYyxRQUFQLEtBQW9CLFFBQXBCLElBQWdDLENBQUNqRixNQUFNLENBQUNxRixVQUFQLENBQWtCSixRQUFsQixDQUFyQyxFQUFrRTtBQUNoRSxZQUFNLElBQUlkLFNBQUosQ0FBYyx1QkFBdUJjLFFBQXJDLENBQU47QUFDRDtBQUNGLEdBckJELE1BcUJPLElBQUksT0FBTzZELEdBQVAsS0FBZSxRQUFuQixFQUE2QjtBQUNsQ0EsT0FBRyxHQUFHQSxHQUFHLEdBQUcsR0FBWjtBQUNELEdBekIrRCxDQTJCaEU7OztBQUNBLE1BQUk3RyxLQUFLLEdBQUcsQ0FBUixJQUFhLEtBQUtqQixNQUFMLEdBQWNpQixLQUEzQixJQUFvQyxLQUFLakIsTUFBTCxHQUFja0IsR0FBdEQsRUFBMkQ7QUFDekQsVUFBTSxJQUFJMEIsVUFBSixDQUFlLG9CQUFmLENBQU47QUFDRDs7QUFFRCxNQUFJMUIsR0FBRyxJQUFJRCxLQUFYLEVBQWtCO0FBQ2hCLFdBQU8sSUFBUDtBQUNEOztBQUVEQSxPQUFLLEdBQUdBLEtBQUssS0FBSyxDQUFsQjtBQUNBQyxLQUFHLEdBQUdBLEdBQUcsS0FBS2dCLFNBQVIsR0FBb0IsS0FBS2xDLE1BQXpCLEdBQWtDa0IsR0FBRyxLQUFLLENBQWhEO0FBRUEsTUFBSSxDQUFDNEcsR0FBTCxFQUFVQSxHQUFHLEdBQUcsQ0FBTjtBQUVWLE1BQUloSSxDQUFKOztBQUNBLE1BQUksT0FBT2dJLEdBQVAsS0FBZSxRQUFuQixFQUE2QjtBQUMzQixTQUFLaEksQ0FBQyxHQUFHbUIsS0FBVCxFQUFnQm5CLENBQUMsR0FBR29CLEdBQXBCLEVBQXlCLEVBQUVwQixDQUEzQixFQUE4QjtBQUM1QixXQUFLQSxDQUFMLElBQVVnSSxHQUFWO0FBQ0Q7QUFDRixHQUpELE1BSU87QUFDTCxRQUFJaUQsS0FBSyxHQUFHL0wsTUFBTSxDQUFDNkYsUUFBUCxDQUFnQmlELEdBQWhCLElBQ1JBLEdBRFEsR0FFUjNCLFdBQVcsQ0FBQyxJQUFJbkgsTUFBSixDQUFXOEksR0FBWCxFQUFnQjdELFFBQWhCLEVBQTBCL0UsUUFBMUIsRUFBRCxDQUZmO0FBR0EsUUFBSWEsR0FBRyxHQUFHZ0wsS0FBSyxDQUFDL0ssTUFBaEI7O0FBQ0EsU0FBS0YsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHb0IsR0FBRyxHQUFHRCxLQUF0QixFQUE2QixFQUFFbkIsQ0FBL0IsRUFBa0M7QUFDaEMsV0FBS0EsQ0FBQyxHQUFHbUIsS0FBVCxJQUFrQjhKLEtBQUssQ0FBQ2pMLENBQUMsR0FBR0MsR0FBTCxDQUF2QjtBQUNEO0FBQ0Y7O0FBRUQsU0FBTyxJQUFQO0FBQ0QsQ0F6REQsQyxDQTJEQTtBQUNBOzs7QUFFQSxJQUFJd08saUJBQWlCLEdBQUcsb0JBQXhCOztBQUVBLFNBQVNDLFdBQVQsQ0FBc0J6UCxHQUF0QixFQUEyQjtBQUN6QjtBQUNBQSxLQUFHLEdBQUcwUCxVQUFVLENBQUMxUCxHQUFELENBQVYsQ0FBZ0IyUCxPQUFoQixDQUF3QkgsaUJBQXhCLEVBQTJDLEVBQTNDLENBQU4sQ0FGeUIsQ0FHekI7O0FBQ0EsTUFBSXhQLEdBQUcsQ0FBQ2lCLE1BQUosR0FBYSxDQUFqQixFQUFvQixPQUFPLEVBQVAsQ0FKSyxDQUt6Qjs7QUFDQSxTQUFPakIsR0FBRyxDQUFDaUIsTUFBSixHQUFhLENBQWIsS0FBbUIsQ0FBMUIsRUFBNkI7QUFDM0JqQixPQUFHLEdBQUdBLEdBQUcsR0FBRyxHQUFaO0FBQ0Q7O0FBQ0QsU0FBT0EsR0FBUDtBQUNEOztBQUVELFNBQVMwUCxVQUFULENBQXFCMVAsR0FBckIsRUFBMEI7QUFDeEIsTUFBSUEsR0FBRyxDQUFDNFAsSUFBUixFQUFjLE9BQU81UCxHQUFHLENBQUM0UCxJQUFKLEVBQVA7QUFDZCxTQUFPNVAsR0FBRyxDQUFDMlAsT0FBSixDQUFZLFlBQVosRUFBMEIsRUFBMUIsQ0FBUDtBQUNEOztBQUVELFNBQVM1RCxLQUFULENBQWdCakUsQ0FBaEIsRUFBbUI7QUFDakIsTUFBSUEsQ0FBQyxHQUFHLEVBQVIsRUFBWSxPQUFPLE1BQU1BLENBQUMsQ0FBQzNILFFBQUYsQ0FBVyxFQUFYLENBQWI7QUFDWixTQUFPMkgsQ0FBQyxDQUFDM0gsUUFBRixDQUFXLEVBQVgsQ0FBUDtBQUNEOztBQUVELFNBQVNpSCxXQUFULENBQXNCL0IsTUFBdEIsRUFBOEJ3SyxLQUE5QixFQUFxQztBQUNuQ0EsT0FBSyxHQUFHQSxLQUFLLElBQUlDLFFBQWpCO0FBQ0EsTUFBSTNFLFNBQUo7QUFDQSxNQUFJbEssTUFBTSxHQUFHb0UsTUFBTSxDQUFDcEUsTUFBcEI7QUFDQSxNQUFJOE8sYUFBYSxHQUFHLElBQXBCO0FBQ0EsTUFBSS9ELEtBQUssR0FBRyxFQUFaOztBQUVBLE9BQUssSUFBSWpMLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdFLE1BQXBCLEVBQTRCLEVBQUVGLENBQTlCLEVBQWlDO0FBQy9Cb0ssYUFBUyxHQUFHOUYsTUFBTSxDQUFDbkUsVUFBUCxDQUFrQkgsQ0FBbEIsQ0FBWixDQUQrQixDQUcvQjs7QUFDQSxRQUFJb0ssU0FBUyxHQUFHLE1BQVosSUFBc0JBLFNBQVMsR0FBRyxNQUF0QyxFQUE4QztBQUM1QztBQUNBLFVBQUksQ0FBQzRFLGFBQUwsRUFBb0I7QUFDbEI7QUFDQSxZQUFJNUUsU0FBUyxHQUFHLE1BQWhCLEVBQXdCO0FBQ3RCO0FBQ0EsY0FBSSxDQUFDMEUsS0FBSyxJQUFJLENBQVYsSUFBZSxDQUFDLENBQXBCLEVBQXVCN0QsS0FBSyxDQUFDM0osSUFBTixDQUFXLElBQVgsRUFBaUIsSUFBakIsRUFBdUIsSUFBdkI7QUFDdkI7QUFDRCxTQUpELE1BSU8sSUFBSXRCLENBQUMsR0FBRyxDQUFKLEtBQVVFLE1BQWQsRUFBc0I7QUFDM0I7QUFDQSxjQUFJLENBQUM0TyxLQUFLLElBQUksQ0FBVixJQUFlLENBQUMsQ0FBcEIsRUFBdUI3RCxLQUFLLENBQUMzSixJQUFOLENBQVcsSUFBWCxFQUFpQixJQUFqQixFQUF1QixJQUF2QjtBQUN2QjtBQUNELFNBVmlCLENBWWxCOzs7QUFDQTBOLHFCQUFhLEdBQUc1RSxTQUFoQjtBQUVBO0FBQ0QsT0FsQjJDLENBb0I1Qzs7O0FBQ0EsVUFBSUEsU0FBUyxHQUFHLE1BQWhCLEVBQXdCO0FBQ3RCLFlBQUksQ0FBQzBFLEtBQUssSUFBSSxDQUFWLElBQWUsQ0FBQyxDQUFwQixFQUF1QjdELEtBQUssQ0FBQzNKLElBQU4sQ0FBVyxJQUFYLEVBQWlCLElBQWpCLEVBQXVCLElBQXZCO0FBQ3ZCME4scUJBQWEsR0FBRzVFLFNBQWhCO0FBQ0E7QUFDRCxPQXpCMkMsQ0EyQjVDOzs7QUFDQUEsZUFBUyxHQUFHLENBQUM0RSxhQUFhLEdBQUcsTUFBaEIsSUFBMEIsRUFBMUIsR0FBK0I1RSxTQUFTLEdBQUcsTUFBNUMsSUFBc0QsT0FBbEU7QUFDRCxLQTdCRCxNQTZCTyxJQUFJNEUsYUFBSixFQUFtQjtBQUN4QjtBQUNBLFVBQUksQ0FBQ0YsS0FBSyxJQUFJLENBQVYsSUFBZSxDQUFDLENBQXBCLEVBQXVCN0QsS0FBSyxDQUFDM0osSUFBTixDQUFXLElBQVgsRUFBaUIsSUFBakIsRUFBdUIsSUFBdkI7QUFDeEI7O0FBRUQwTixpQkFBYSxHQUFHLElBQWhCLENBdEMrQixDQXdDL0I7O0FBQ0EsUUFBSTVFLFNBQVMsR0FBRyxJQUFoQixFQUFzQjtBQUNwQixVQUFJLENBQUMwRSxLQUFLLElBQUksQ0FBVixJQUFlLENBQW5CLEVBQXNCO0FBQ3RCN0QsV0FBSyxDQUFDM0osSUFBTixDQUFXOEksU0FBWDtBQUNELEtBSEQsTUFHTyxJQUFJQSxTQUFTLEdBQUcsS0FBaEIsRUFBdUI7QUFDNUIsVUFBSSxDQUFDMEUsS0FBSyxJQUFJLENBQVYsSUFBZSxDQUFuQixFQUFzQjtBQUN0QjdELFdBQUssQ0FBQzNKLElBQU4sQ0FDRThJLFNBQVMsSUFBSSxHQUFiLEdBQW1CLElBRHJCLEVBRUVBLFNBQVMsR0FBRyxJQUFaLEdBQW1CLElBRnJCO0FBSUQsS0FOTSxNQU1BLElBQUlBLFNBQVMsR0FBRyxPQUFoQixFQUF5QjtBQUM5QixVQUFJLENBQUMwRSxLQUFLLElBQUksQ0FBVixJQUFlLENBQW5CLEVBQXNCO0FBQ3RCN0QsV0FBSyxDQUFDM0osSUFBTixDQUNFOEksU0FBUyxJQUFJLEdBQWIsR0FBbUIsSUFEckIsRUFFRUEsU0FBUyxJQUFJLEdBQWIsR0FBbUIsSUFBbkIsR0FBMEIsSUFGNUIsRUFHRUEsU0FBUyxHQUFHLElBQVosR0FBbUIsSUFIckI7QUFLRCxLQVBNLE1BT0EsSUFBSUEsU0FBUyxHQUFHLFFBQWhCLEVBQTBCO0FBQy9CLFVBQUksQ0FBQzBFLEtBQUssSUFBSSxDQUFWLElBQWUsQ0FBbkIsRUFBc0I7QUFDdEI3RCxXQUFLLENBQUMzSixJQUFOLENBQ0U4SSxTQUFTLElBQUksSUFBYixHQUFvQixJQUR0QixFQUVFQSxTQUFTLElBQUksR0FBYixHQUFtQixJQUFuQixHQUEwQixJQUY1QixFQUdFQSxTQUFTLElBQUksR0FBYixHQUFtQixJQUFuQixHQUEwQixJQUg1QixFQUlFQSxTQUFTLEdBQUcsSUFBWixHQUFtQixJQUpyQjtBQU1ELEtBUk0sTUFRQTtBQUNMLFlBQU0sSUFBSTlKLEtBQUosQ0FBVSxvQkFBVixDQUFOO0FBQ0Q7QUFDRjs7QUFFRCxTQUFPMkssS0FBUDtBQUNEOztBQUVELFNBQVN2QixZQUFULENBQXVCekssR0FBdkIsRUFBNEI7QUFDMUIsTUFBSWdRLFNBQVMsR0FBRyxFQUFoQjs7QUFDQSxPQUFLLElBQUlqUCxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHZixHQUFHLENBQUNpQixNQUF4QixFQUFnQyxFQUFFRixDQUFsQyxFQUFxQztBQUNuQztBQUNBaVAsYUFBUyxDQUFDM04sSUFBVixDQUFlckMsR0FBRyxDQUFDa0IsVUFBSixDQUFlSCxDQUFmLElBQW9CLElBQW5DO0FBQ0Q7O0FBQ0QsU0FBT2lQLFNBQVA7QUFDRDs7QUFFRCxTQUFTbkYsY0FBVCxDQUF5QjdLLEdBQXpCLEVBQThCNlAsS0FBOUIsRUFBcUM7QUFDbkMsTUFBSUksQ0FBSixFQUFPQyxFQUFQLEVBQVdDLEVBQVg7QUFDQSxNQUFJSCxTQUFTLEdBQUcsRUFBaEI7O0FBQ0EsT0FBSyxJQUFJalAsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR2YsR0FBRyxDQUFDaUIsTUFBeEIsRUFBZ0MsRUFBRUYsQ0FBbEMsRUFBcUM7QUFDbkMsUUFBSSxDQUFDOE8sS0FBSyxJQUFJLENBQVYsSUFBZSxDQUFuQixFQUFzQjtBQUV0QkksS0FBQyxHQUFHalEsR0FBRyxDQUFDa0IsVUFBSixDQUFlSCxDQUFmLENBQUo7QUFDQW1QLE1BQUUsR0FBR0QsQ0FBQyxJQUFJLENBQVY7QUFDQUUsTUFBRSxHQUFHRixDQUFDLEdBQUcsR0FBVDtBQUNBRCxhQUFTLENBQUMzTixJQUFWLENBQWU4TixFQUFmO0FBQ0FILGFBQVMsQ0FBQzNOLElBQVYsQ0FBZTZOLEVBQWY7QUFDRDs7QUFFRCxTQUFPRixTQUFQO0FBQ0Q7O0FBRUQsU0FBUzNJLGFBQVQsQ0FBd0JySCxHQUF4QixFQUE2QjtBQUMzQixTQUFPMkMsTUFBTSxDQUFDcEMsV0FBUCxDQUFtQmtQLFdBQVcsQ0FBQ3pQLEdBQUQsQ0FBOUIsQ0FBUDtBQUNEOztBQUVELFNBQVN1SyxVQUFULENBQXFCNkYsR0FBckIsRUFBMEJDLEdBQTFCLEVBQStCdEcsTUFBL0IsRUFBdUM5SSxNQUF2QyxFQUErQztBQUM3QyxPQUFLLElBQUlGLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdFLE1BQXBCLEVBQTRCLEVBQUVGLENBQTlCLEVBQWlDO0FBQy9CLFFBQUtBLENBQUMsR0FBR2dKLE1BQUosSUFBY3NHLEdBQUcsQ0FBQ3BQLE1BQW5CLElBQStCRixDQUFDLElBQUlxUCxHQUFHLENBQUNuUCxNQUE1QyxFQUFxRDtBQUNyRG9QLE9BQUcsQ0FBQ3RQLENBQUMsR0FBR2dKLE1BQUwsQ0FBSCxHQUFrQnFHLEdBQUcsQ0FBQ3JQLENBQUQsQ0FBckI7QUFDRDs7QUFDRCxTQUFPQSxDQUFQO0FBQ0Q7O0FBRUQsU0FBU2tGLEtBQVQsQ0FBZ0I4QyxHQUFoQixFQUFxQjtBQUNuQixTQUFPQSxHQUFHLEtBQUtBLEdBQWYsQ0FEbUIsQ0FDQTtBQUNwQixDOzs7Ozs7Ozs7Ozs7QUM1dkREO0FBQ0ExSSxPQUFPLENBQUNtSixJQUFSLEdBQWUsVUFBVXhELE1BQVYsRUFBa0IrRCxNQUFsQixFQUEwQnVHLElBQTFCLEVBQWdDQyxJQUFoQyxFQUFzQ0MsTUFBdEMsRUFBOEM7QUFDM0QsTUFBSTlNLENBQUosRUFBT3FFLENBQVA7QUFDQSxNQUFJMEksSUFBSSxHQUFJRCxNQUFNLEdBQUcsQ0FBVixHQUFlRCxJQUFmLEdBQXNCLENBQWpDO0FBQ0EsTUFBSUcsSUFBSSxHQUFHLENBQUMsS0FBS0QsSUFBTixJQUFjLENBQXpCO0FBQ0EsTUFBSUUsS0FBSyxHQUFHRCxJQUFJLElBQUksQ0FBcEI7QUFDQSxNQUFJRSxLQUFLLEdBQUcsQ0FBQyxDQUFiO0FBQ0EsTUFBSTdQLENBQUMsR0FBR3VQLElBQUksR0FBSUUsTUFBTSxHQUFHLENBQWIsR0FBa0IsQ0FBOUI7QUFDQSxNQUFJSyxDQUFDLEdBQUdQLElBQUksR0FBRyxDQUFDLENBQUosR0FBUSxDQUFwQjtBQUNBLE1BQUlRLENBQUMsR0FBRzlLLE1BQU0sQ0FBQytELE1BQU0sR0FBR2hKLENBQVYsQ0FBZDtBQUVBQSxHQUFDLElBQUk4UCxDQUFMO0FBRUFuTixHQUFDLEdBQUdvTixDQUFDLEdBQUksQ0FBQyxLQUFNLENBQUNGLEtBQVIsSUFBa0IsQ0FBM0I7QUFDQUUsR0FBQyxLQUFNLENBQUNGLEtBQVI7QUFDQUEsT0FBSyxJQUFJSCxJQUFUOztBQUNBLFNBQU9HLEtBQUssR0FBRyxDQUFmLEVBQWtCbE4sQ0FBQyxHQUFJQSxDQUFDLEdBQUcsR0FBTCxHQUFZc0MsTUFBTSxDQUFDK0QsTUFBTSxHQUFHaEosQ0FBVixDQUF0QixFQUFvQ0EsQ0FBQyxJQUFJOFAsQ0FBekMsRUFBNENELEtBQUssSUFBSSxDQUF2RSxFQUEwRSxDQUFFOztBQUU1RTdJLEdBQUMsR0FBR3JFLENBQUMsR0FBSSxDQUFDLEtBQU0sQ0FBQ2tOLEtBQVIsSUFBa0IsQ0FBM0I7QUFDQWxOLEdBQUMsS0FBTSxDQUFDa04sS0FBUjtBQUNBQSxPQUFLLElBQUlMLElBQVQ7O0FBQ0EsU0FBT0ssS0FBSyxHQUFHLENBQWYsRUFBa0I3SSxDQUFDLEdBQUlBLENBQUMsR0FBRyxHQUFMLEdBQVkvQixNQUFNLENBQUMrRCxNQUFNLEdBQUdoSixDQUFWLENBQXRCLEVBQW9DQSxDQUFDLElBQUk4UCxDQUF6QyxFQUE0Q0QsS0FBSyxJQUFJLENBQXZFLEVBQTBFLENBQUU7O0FBRTVFLE1BQUlsTixDQUFDLEtBQUssQ0FBVixFQUFhO0FBQ1hBLEtBQUMsR0FBRyxJQUFJaU4sS0FBUjtBQUNELEdBRkQsTUFFTyxJQUFJak4sQ0FBQyxLQUFLZ04sSUFBVixFQUFnQjtBQUNyQixXQUFPM0ksQ0FBQyxHQUFHZ0osR0FBSCxHQUFVLENBQUNELENBQUMsR0FBRyxDQUFDLENBQUosR0FBUSxDQUFWLElBQWVoQixRQUFqQztBQUNELEdBRk0sTUFFQTtBQUNML0gsS0FBQyxHQUFHQSxDQUFDLEdBQUdyQixJQUFJLENBQUNvRyxHQUFMLENBQVMsQ0FBVCxFQUFZeUQsSUFBWixDQUFSO0FBQ0E3TSxLQUFDLEdBQUdBLENBQUMsR0FBR2lOLEtBQVI7QUFDRDs7QUFDRCxTQUFPLENBQUNHLENBQUMsR0FBRyxDQUFDLENBQUosR0FBUSxDQUFWLElBQWUvSSxDQUFmLEdBQW1CckIsSUFBSSxDQUFDb0csR0FBTCxDQUFTLENBQVQsRUFBWXBKLENBQUMsR0FBRzZNLElBQWhCLENBQTFCO0FBQ0QsQ0EvQkQ7O0FBaUNBbFEsT0FBTyxDQUFDbUYsS0FBUixHQUFnQixVQUFVUSxNQUFWLEVBQWtCN0IsS0FBbEIsRUFBeUI0RixNQUF6QixFQUFpQ3VHLElBQWpDLEVBQXVDQyxJQUF2QyxFQUE2Q0MsTUFBN0MsRUFBcUQ7QUFDbkUsTUFBSTlNLENBQUosRUFBT3FFLENBQVAsRUFBVWtJLENBQVY7QUFDQSxNQUFJUSxJQUFJLEdBQUlELE1BQU0sR0FBRyxDQUFWLEdBQWVELElBQWYsR0FBc0IsQ0FBakM7QUFDQSxNQUFJRyxJQUFJLEdBQUcsQ0FBQyxLQUFLRCxJQUFOLElBQWMsQ0FBekI7QUFDQSxNQUFJRSxLQUFLLEdBQUdELElBQUksSUFBSSxDQUFwQjtBQUNBLE1BQUlNLEVBQUUsR0FBSVQsSUFBSSxLQUFLLEVBQVQsR0FBYzdKLElBQUksQ0FBQ29HLEdBQUwsQ0FBUyxDQUFULEVBQVksQ0FBQyxFQUFiLElBQW1CcEcsSUFBSSxDQUFDb0csR0FBTCxDQUFTLENBQVQsRUFBWSxDQUFDLEVBQWIsQ0FBakMsR0FBb0QsQ0FBOUQ7QUFDQSxNQUFJL0wsQ0FBQyxHQUFHdVAsSUFBSSxHQUFHLENBQUgsR0FBUUUsTUFBTSxHQUFHLENBQTdCO0FBQ0EsTUFBSUssQ0FBQyxHQUFHUCxJQUFJLEdBQUcsQ0FBSCxHQUFPLENBQUMsQ0FBcEI7QUFDQSxNQUFJUSxDQUFDLEdBQUczTSxLQUFLLEdBQUcsQ0FBUixJQUFjQSxLQUFLLEtBQUssQ0FBVixJQUFlLElBQUlBLEtBQUosR0FBWSxDQUF6QyxHQUE4QyxDQUE5QyxHQUFrRCxDQUExRDtBQUVBQSxPQUFLLEdBQUd1QyxJQUFJLENBQUN1SyxHQUFMLENBQVM5TSxLQUFULENBQVI7O0FBRUEsTUFBSThFLEtBQUssQ0FBQzlFLEtBQUQsQ0FBTCxJQUFnQkEsS0FBSyxLQUFLMkwsUUFBOUIsRUFBd0M7QUFDdEMvSCxLQUFDLEdBQUdrQixLQUFLLENBQUM5RSxLQUFELENBQUwsR0FBZSxDQUFmLEdBQW1CLENBQXZCO0FBQ0FULEtBQUMsR0FBR2dOLElBQUo7QUFDRCxHQUhELE1BR087QUFDTGhOLEtBQUMsR0FBR2dELElBQUksQ0FBQ29ILEtBQUwsQ0FBV3BILElBQUksQ0FBQ3dLLEdBQUwsQ0FBUy9NLEtBQVQsSUFBa0J1QyxJQUFJLENBQUN5SyxHQUFsQyxDQUFKOztBQUNBLFFBQUloTixLQUFLLElBQUk4TCxDQUFDLEdBQUd2SixJQUFJLENBQUNvRyxHQUFMLENBQVMsQ0FBVCxFQUFZLENBQUNwSixDQUFiLENBQVIsQ0FBTCxHQUFnQyxDQUFwQyxFQUF1QztBQUNyQ0EsT0FBQztBQUNEdU0sT0FBQyxJQUFJLENBQUw7QUFDRDs7QUFDRCxRQUFJdk0sQ0FBQyxHQUFHaU4sS0FBSixJQUFhLENBQWpCLEVBQW9CO0FBQ2xCeE0sV0FBSyxJQUFJNk0sRUFBRSxHQUFHZixDQUFkO0FBQ0QsS0FGRCxNQUVPO0FBQ0w5TCxXQUFLLElBQUk2TSxFQUFFLEdBQUd0SyxJQUFJLENBQUNvRyxHQUFMLENBQVMsQ0FBVCxFQUFZLElBQUk2RCxLQUFoQixDQUFkO0FBQ0Q7O0FBQ0QsUUFBSXhNLEtBQUssR0FBRzhMLENBQVIsSUFBYSxDQUFqQixFQUFvQjtBQUNsQnZNLE9BQUM7QUFDRHVNLE9BQUMsSUFBSSxDQUFMO0FBQ0Q7O0FBRUQsUUFBSXZNLENBQUMsR0FBR2lOLEtBQUosSUFBYUQsSUFBakIsRUFBdUI7QUFDckIzSSxPQUFDLEdBQUcsQ0FBSjtBQUNBckUsT0FBQyxHQUFHZ04sSUFBSjtBQUNELEtBSEQsTUFHTyxJQUFJaE4sQ0FBQyxHQUFHaU4sS0FBSixJQUFhLENBQWpCLEVBQW9CO0FBQ3pCNUksT0FBQyxHQUFHLENBQUU1RCxLQUFLLEdBQUc4TCxDQUFULEdBQWMsQ0FBZixJQUFvQnZKLElBQUksQ0FBQ29HLEdBQUwsQ0FBUyxDQUFULEVBQVl5RCxJQUFaLENBQXhCO0FBQ0E3TSxPQUFDLEdBQUdBLENBQUMsR0FBR2lOLEtBQVI7QUFDRCxLQUhNLE1BR0E7QUFDTDVJLE9BQUMsR0FBRzVELEtBQUssR0FBR3VDLElBQUksQ0FBQ29HLEdBQUwsQ0FBUyxDQUFULEVBQVk2RCxLQUFLLEdBQUcsQ0FBcEIsQ0FBUixHQUFpQ2pLLElBQUksQ0FBQ29HLEdBQUwsQ0FBUyxDQUFULEVBQVl5RCxJQUFaLENBQXJDO0FBQ0E3TSxPQUFDLEdBQUcsQ0FBSjtBQUNEO0FBQ0Y7O0FBRUQsU0FBTzZNLElBQUksSUFBSSxDQUFmLEVBQWtCdkssTUFBTSxDQUFDK0QsTUFBTSxHQUFHaEosQ0FBVixDQUFOLEdBQXFCZ0gsQ0FBQyxHQUFHLElBQXpCLEVBQStCaEgsQ0FBQyxJQUFJOFAsQ0FBcEMsRUFBdUM5SSxDQUFDLElBQUksR0FBNUMsRUFBaUR3SSxJQUFJLElBQUksQ0FBM0UsRUFBOEUsQ0FBRTs7QUFFaEY3TSxHQUFDLEdBQUlBLENBQUMsSUFBSTZNLElBQU4sR0FBY3hJLENBQWxCO0FBQ0EwSSxNQUFJLElBQUlGLElBQVI7O0FBQ0EsU0FBT0UsSUFBSSxHQUFHLENBQWQsRUFBaUJ6SyxNQUFNLENBQUMrRCxNQUFNLEdBQUdoSixDQUFWLENBQU4sR0FBcUIyQyxDQUFDLEdBQUcsSUFBekIsRUFBK0IzQyxDQUFDLElBQUk4UCxDQUFwQyxFQUF1Q25OLENBQUMsSUFBSSxHQUE1QyxFQUFpRCtNLElBQUksSUFBSSxDQUExRSxFQUE2RSxDQUFFOztBQUUvRXpLLFFBQU0sQ0FBQytELE1BQU0sR0FBR2hKLENBQVQsR0FBYThQLENBQWQsQ0FBTixJQUEwQkMsQ0FBQyxHQUFHLEdBQTlCO0FBQ0QsQ0FsREQsQzs7Ozs7Ozs7Ozs7QUNsQ0EsSUFBSTNRLFFBQVEsR0FBRyxHQUFHQSxRQUFsQjs7QUFFQUMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCUSxLQUFLLENBQUNpQyxPQUFOLElBQWlCLFVBQVVsQixHQUFWLEVBQWU7QUFDL0MsU0FBT3pCLFFBQVEsQ0FBQ2dKLElBQVQsQ0FBY3ZILEdBQWQsS0FBc0IsZ0JBQTdCO0FBQ0QsQ0FGRCxDOzs7Ozs7Ozs7Ozs7O0FDRkE7QUFDQSxtSEFDQSwyQkFEQSxLQUVBLFVBQ0E7QUFBQTtBQUFBO0FBQUEscUdBREEsS0FFQSxFQUdBO0FBQ0MsQ0FURCxFQVNDLElBVEQsRUFTQztBQUNEOzs7OztBQ1ZBO0FBQUE7O0FBQ0E7QUFBQTs7O0FBRUE7QUFBQTs7QUFDQTs7QUFBQTs7O0FBRUE7QUFBQTs7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBOztBQUNBOzs7QUFBQTtBQUNBO0FBQUEscUJBREE7O0FBRUE7QUFBQSxrQkFGQTs7QUFHQTtBQUFBO0FBQ0E7O0FBSkE7OztBQU1BO0FBQUE7O0FBQ0E7O0FBQUE7OztBQUVBO0FBQUE7O0FBQ0E7O0FBQUE7OztBQUVBO0FBQUE7O0FBQ0E7O0FBQUE7QUFDQTtBQUFBOzs7OztBQUdBO0FBQUE7O0FBQ0E7OztBQUFBOzs7QUFFQTtBQUFBOztBQUNBOztBQUFBOzs7QUFFQTtBQUFBOztBQUNBOztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBMEMsNEJBQTFDO0FBQTBDO0FBQTFDO0FBQ0E7QUFBQTtBQUNBOztBQUFBLE9BSkE7OztBQU1BO0FBQUE7O0FBQ0E7OztBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBd0Q7QUFBeEQ7QUFDQTtBQUFBO0FBQ0E7OztBQUFBO0FBQWlEO0FBQWpEO0FBQ0E7QUFBQSxPQUxBOzs7QUFPQTtBQUFBOztBQUNBO0FBQUE7O0FBQ0E7QUFBQTs7QUFDQTtBQUFBOztBQUNBO0FBQUE7O0FBQ0E7OztBQUFBO0FBQ0E7QUFBQTtBQUNBOztBQUFBO0FBQ0E7O0FBQUE7QUFDQTs7QUFBQTtBQUNBOztBQUFBO0FBQ0E7OztBQUFBO0FBQXlDLDBCQUF6QztBQUF5QztBQUF6QztBQUNBOztBQUFBO0FBQUE7QUFBZ0g7QUFBcUIsV0FBckksQ0FBcUksSUFBckksQ0FBcUksSUFBckksRUFBcUksR0FBckk7QUFBQTtBQUNBOztBQUFBO0FBQ0E7QUFBQSxPQVRBOzs7QUFXQTtBQUFBOztBQUNBOzs7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQTJCO0FBQTRCLFNBRHZEO0FBRUE7QUFBQTtBQUFpQztBQUFlLFNBRmhEO0FBR0E7O0FBQUE7QUFDQTs7O0FBQUE7QUFDQTtBQUFBLE9BTkE7OztBQVFBO0FBQUE7O0FBQ0E7OztBQUFBO0FBQXNEO0FBQStELE9BQXJIOzs7QUFFQTtBQUFBOztBQUNBOzs7QUFBQTs7Ozs7QUFHQTtBQUFBOztBQUNBOztBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEZBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUVBOztBQUNBLDJDQUFrQyxPQUFsQyxFQUEyQyxHQUEzQyxFQUEyQztBQUMzQztBQUNBO0FBQ0EsUyxDQUVBO0FBQ0E7OztBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsV0FMQSxDQU9BO0FBQ0E7OztBQUNBO0FBQ0E7QUFFQSxtREFDQSxDQURBLEdBRUEsZ0JBRkE7QUFJQTtBQUNBLFMsQ0FFQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFFQSwwQkFSQSxDQVVBOztBQUNBLDBDQUNBLFlBREEsR0FFQSxRQUZBO0FBSUE7O0FBQ0Esc0JBQWEsT0FBYixFQUFzQixNQUF0QixFQUFzQjtBQUN0QixrQkFDQSxxQ0FDQSxzQ0FEQSxHQUVBLHFDQUZBLEdBR0EsZ0NBSkE7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGtCQUNBLG9DQUNBLHFDQUZBO0FBR0E7QUFDQTs7QUFFQTtBQUNBLGtCQUNBLHFDQUNBLHFDQURBLEdBRUEscUNBSEE7QUFJQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLDRDQUNBLHdCQURBLEdBRUEsdUJBRkEsR0FHQSxrQkFIQTtBQUlBOztBQUVBO0FBQ0E7QUFDQTs7QUFDQSw4QkFBcUIsT0FBckIsRUFBOEIsTUFBOUIsRUFBOEI7QUFDOUIsa0JBQ0EsK0JBQ0EsMEJBREEsS0FFQSxtQkFGQSxDQURBO0FBSUE7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUhBLENBR0E7O0FBQ0E7QUFDQSxxQ0FMQSxDQUtBO0FBRUE7O0FBQ0EsbURBQTBDLFFBQTFDLEVBQW9ELG1CQUFwRCxFQUFvRDtBQUNwRDtBQUNBLFdBVkEsQ0FZQTs7O0FBQ0E7QUFDQTtBQUNBLHVCQUNBLG1CQUNBLHVCQURBLEdBRUEsSUFIQTtBQUtHLFdBUEgsTUFPRztBQUNIO0FBQ0EsdUJBQ0Esb0JBQ0EsdUJBREEsR0FFQSx1QkFGQSxHQUdBLEdBSkE7QUFNQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7O0FDckpBOztBQUFBO0FBRUE7QUFDQSw4QkFEQTtBQUVBLDBCQUZBO0FBR0EsMEJBSEE7QUFJQTtBQUpBOztBQU9BO0FBQ0EsNkRBQ0EsT0FEQSxDQUNBLElBREEsRUFDQSxHQURBLEVBRUEsT0FGQSxDQUVBLElBRkEsRUFFQSxHQUZBO0FBR0E7O0FBRUE7QUFDQSwyQ0FDQSxPQURBLENBQ0EsS0FEQSxFQUNBLEdBREEsRUFFQSxPQUZBLENBRUEsSUFGQSxFQUVBLEVBRkE7QUFHQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7U0EzQkEsRSxJQUFBLEMsSUFBQSxFOztnREFBQTs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7O0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFJQSx1QkFBYSxtQkFBTztBQUFDO0FBQUEsNkNBQUQsQ0FBcEI7O0FBQ0Esd0JBQWMsbUJBQU87QUFBQztBQUFBLDJDQUFELENBQXJCOztBQUNBLHdCQUFjLG1CQUFPO0FBQUM7QUFBQSwyQ0FBRCxDQUFyQjs7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFDQSxrRkFDQSwwQkFEQSxHQUVBLG1CQUZBO0FBSUE7QUFDQTtBQUNBOztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQXFCLCtDQUFyQjtBQUFxQjtBQUFtRDtBQUFBO0FBQXhFO0FBQ0E7QUFDQSxnREFEQSxJQUNBO0FBQ0EsaURBRkEsQ0FIQSxDQUtBO0FBQ0csYUFOSCxDQU1HO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0RBQ0EsVUFEQSxHQUVBLFVBRkE7QUFHQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNHLGFBSkgsTUFJRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFIQSxDQUtBOzs7QUFDQTtBQUNBO0FBQ0EsZ0NBQ0EsbUVBREE7QUFHQTs7QUFDQTtBQUNBOztBQUNBO0FBQ0E7O0FBRUEsaUNBbkhBLENBbUhBO0FBRUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FIQTs7QUFLQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0EsV0FGQTs7QUFJQTtBQUNBO0FBQ0E7O0FBQ0EsbUVBQ0EsaUNBREEsRUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFEQTtBQUVBO0FBRkE7QUFJQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNHLGFBRkgsTUFFRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9EQUNBLDZDQURBLEdBRUEsbUNBRkE7QUFHQTs7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQSxXQUZBOztBQUlBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBLDhCQUFtQixRQUFuQixFQUE2QixHQUE3QixFQUE2QjtBQUM3QjtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBLFdBRkE7QUFHQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQSxXQUZBOztBQUlBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUNBLDRCQUFpQixVQUFqQixFQUE2QixNQUE3QixFQUE2QjtBQUM3QjtBQUNBOztBQUNBO0FBQ0E7O0FBRUE7QUFDQSw2QkFEQSxDQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNHLGFBRkgsTUFFRztBQUNIO0FBQ0csYUFGQSxNQUVBO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNHLGFBSkgsTUFJRztBQUNIO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx3REFDQSxpQ0FEQSxJQUNBLGVBREEsRUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1RkFDQSxVQURBLEdBQ0EseUJBREEsR0FDQSxRQURBO0FBRUE7O0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQTBCO0FBQzFCO0FBQ0E7O0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsV0FGQTs7QUFJQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUVBO0FBQ0E7O0FBRUEsa0RBQXVDLE9BQXZDLEVBQWdELEdBQWhELEVBQWdEO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQXJCQTs7QUF1QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBZEE7QUFnQkEsV0FqQkE7O0FBbUJBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFDQTtBQUNBOztBQUNBLDBCQUFlLGVBQWYsRUFBZ0MsR0FBaEMsRUFBZ0M7QUFDaEM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBQ0Esd0JBQWEsZUFBYixFQUE4QixHQUE5QixFQUE4QjtBQUM5Qjs7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0EsV0E1QkE7O0FBOEJBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLG1HQUNBLDJEQURBLEdBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLG9DQWJBLENBZUE7O0FBQ0E7O0FBQ0EscUJBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUNBO0FBQ0EscUVBREEsQ0FDQTs7QUFDQTtBQUNBO0FBckJBO0FBdUJBO0FBQ0E7O0FBQ0E7O0FBRUE7QUFDQSxvQ0FEQSxDQUdBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0EsYUFaQSxDQWFBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBekJBLENBMkJBOzs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBM0JBO0FBNkJBO0FBQ0EsV0F2Z0JBLENBeWdCQTtBQUNBOzs7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUNBLDRCQUFpQixPQUFqQixFQUEwQixNQUExQixFQUEwQjtBQUMxQjtBQUNBOztBQUNBO0FBQ0EsV0FUQTs7QUFXQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFDQSw0QkFBaUIsT0FBakIsRUFBMEIsTUFBMUIsRUFBMEI7QUFDMUI7QUFDQTtBQUNBOztBQUNBO0FBQ0EsV0FWQTs7QUFZQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFDQSw0QkFBaUIsT0FBakIsRUFBMEIsTUFBMUIsRUFBMEI7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBLFdBWkE7O0FBY0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBTEE7O0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUpBOztBQU1BO0FBQ0E7QUFDQTs7QUFDQTtBQUNBLGdFQUFrRCxJQUFsRCxDQUFrRCxHQUFsRDtBQUNBO0FBQ0E7O0FBQ0E7QUFDQSxXQVJBOztBQVVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7O0FBRUEsNEJBQWlCLE9BQWpCLEVBQTBCLEdBQTFCLEVBQTBCO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQXpEQSxDQS9rQkEsQ0Ewb0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBLCtDQUZBLENBSUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0csYUFISCxNQUdHO0FBQ0g7QUFDRyxhQUZBLE1BRUE7QUFDSDtBQUNBOztBQUNBLHFDQWJBLENBYUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFqQkEsQ0FtQkE7OztBQUNBOztBQUNBO0FBQ0Esc0NBQ0E7QUFDRyxhQUhILE1BR0c7QUFDSCwyQ0FDQTtBQUNBLGFBM0JBLENBNkJBOzs7QUFDQTtBQUNBO0FBQ0EsYUFoQ0EsQ0FrQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDRyxhQU5ILE1BTUc7QUFDSCwrQkFERyxDQUNIOztBQUNBLGdEQUNBLGtEQURBLEVBQ0E7QUFDQTtBQUNBO0FBQ08saUJBRlAsTUFFTztBQUNQO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFDQSxpRUFDQSxzQkFEQSxJQUNBLHVCQURBLEVBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNLLGVBRkwsTUFFSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTs7QUFDQTtBQUNBOztBQUNBLG1DQUF3QixhQUF4QixFQUF1QyxHQUF2QyxFQUF1QztBQUN2QztBQUNBO0FBQ0E7QUFDTyxpQkFIUCxNQUdPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDRyxhQVhILE1BV0c7QUFDSDs7QUFDQSxtQ0FBd0IsTUFBeEIsRUFBZ0MsR0FBaEMsRUFBZ0M7QUFDaEM7O0FBQ0EsZ0NBQXFCLGFBQXJCLEVBQW9DLEdBQXBDLEVBQW9DO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFdBRkE7O0FBSUE7QUFDQTtBQUNBLFdBRkE7O0FBSUE7QUFDQTtBQUNBLFdBRkE7O0FBSUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDRyxhQUZILE1BRUc7QUFDSDs7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQVZBLENBWUE7OztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUNBLDRCQUFpQixVQUFqQixFQUE2QixHQUE3QixFQUE2QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUhBLENBSUE7QUFDRyxhQUxILE1BS0c7QUFDSDtBQUNBO0FBQ0EseUJBSEcsQ0FJSDtBQUNHLGFBTEEsTUFLQTtBQUNIOztBQUNBO0FBQ0E7QUFDQTtBQUNLLGVBSEwsTUFHSztBQUNMO0FBQ0E7QUFDQSxlQVJHLENBU0g7O0FBQ0csYUFWQSxNQVVBO0FBQ0gsOEJBQ0EseUVBREE7QUFHQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUVBOztBQUNBLHFCQUFTO0FBQ1Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQTVCQTtBQThCQTtBQUNBLFdBdEVBOztBQXdFQTtBQUNBO0FBQ0EsNEJBREE7QUFFQTtBQUZBO0FBSUEsV0FMQTs7QUFPQTtBQUNBO0FBQ0E7QUFDRyxhQUZILE1BRUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBRUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNERBQ0EsdUJBQ0EsdUJBQ0EsQ0FIQTs7QUFLQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7O0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBbENBO0FBb0NBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSyxlQUxMLE1BS0s7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBNzlCQSxDQSs5QkE7QUFDQTtBQUNBOzs7QUFDQTs7QUFFQTtBQUNBOztBQUNBO0FBQ0EsbUVBREEsQ0FDQTtBQUNBLGFBSkEsQ0FNQTs7O0FBQ0E7QUFDQTs7QUFDQTtBQUNBLCtDQUNBLE1BREEsRUFFQSw4Q0FGQTtBQUlBOztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGdDQUFxQixPQUFyQixFQUE4QixHQUE5QixFQUE4QjtBQUM5QjtBQUNBOztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGdDQUFxQixPQUFyQixFQUE4QixHQUE5QixFQUE4QjtBQUM5QjtBQUNBOztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTs7QUFDQSxnQ0FBcUIsT0FBckIsRUFBOEIsR0FBOUIsRUFBOEI7QUFDOUI7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFDQSw0QkFBaUIsZ0JBQWpCLEVBQW1DLE1BQW5DLEVBQW1DO0FBQ25DO0FBQ0E7O0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDRyxhQUhILE1BR0c7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNHLGFBSEgsTUFHRztBQUNIO0FBQ0E7O0FBRUE7QUFFQTs7QUFDQTtBQUNBO0FBQ0E7QUFDRyxhQUhILE1BR0c7QUFDSDtBQUNBOztBQUNBLDhCQUFtQixZQUFuQixFQUFpQyxHQUFqQyxFQUFpQztBQUNqQztBQUNBO0FBQ0E7O0FBRUE7QUFDQSxXQWxDQTtBQW9DQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBYkE7O0FBZUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBZEE7O0FBZ0JBO0FBQ0E7QUFDQTtBQUNBLFdBSEE7O0FBS0E7QUFDQTtBQUNBO0FBQ0EsV0FIQTs7QUFLQTtBQUNBO0FBQ0E7QUFDQSxXQUhBOztBQUtBO0FBQ0E7QUFFQSxtQ0FDQSxxQkFEQSxHQUVBLHNCQUZBLElBR0EsNEJBSEE7QUFJQSxXQVBBOztBQVNBO0FBQ0E7QUFFQSwrQ0FDQSx5QkFDQSxxQkFEQSxHQUVBLGdCQUhBO0FBSUEsV0FQQTs7QUFTQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFFQTtBQUVBO0FBQ0EsV0FoQkE7O0FBa0JBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUVBO0FBRUE7QUFDQSxXQWhCQTs7QUFrQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUpBOztBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FKQTs7QUFNQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBSkE7O0FBTUE7QUFDQTtBQUVBLGtDQUNBLHFCQURBLEdBRUEsc0JBRkEsR0FHQSxzQkFIQTtBQUlBLFdBUEE7O0FBU0E7QUFDQTtBQUVBLHdDQUNBLHNCQURBLEdBRUEscUJBRkEsR0FHQSxnQkFIQTtBQUlBLFdBUEE7O0FBU0E7QUFDQTtBQUNBO0FBQ0EsV0FIQTs7QUFLQTtBQUNBO0FBQ0E7QUFDQSxXQUhBOztBQUtBO0FBQ0E7QUFDQTtBQUNBLFdBSEE7O0FBS0E7QUFDQTtBQUNBO0FBQ0EsV0FIQTs7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxXQWpCQTs7QUFtQkE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBakJBOztBQW1CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBUEE7O0FBU0E7QUFDQTs7QUFDQSxrRUFBdUQsS0FBdkQsRUFBOEQsR0FBOUQsRUFBOEQ7QUFDOUQscUZBQ0EsOEJBREE7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNHLGFBSEgsTUFHRztBQUNIO0FBQ0E7O0FBQ0E7QUFDQSxXQVhBOztBQWFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNHLGFBSEgsTUFHRztBQUNIO0FBQ0E7O0FBQ0E7QUFDQSxXQVhBOztBQWFBO0FBQ0E7O0FBQ0Esa0VBQXVELEtBQXZELEVBQThELEdBQTlELEVBQThEO0FBQzlEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0csYUFMSCxNQUtHO0FBQ0g7QUFDQTs7QUFDQTtBQUNBLFdBYkE7O0FBZUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNHLGFBTEgsTUFLRztBQUNIO0FBQ0E7O0FBQ0E7QUFDQSxXQWJBOztBQWVBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0EsV0FyQkE7O0FBdUJBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0EsV0FyQkE7O0FBdUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQVJBOztBQVVBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNHLGFBSEgsTUFHRztBQUNIO0FBQ0E7O0FBQ0E7QUFDQSxXQVhBOztBQWFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNHLGFBSEgsTUFHRztBQUNIO0FBQ0E7O0FBQ0E7QUFDQSxXQVhBOztBQWFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRyxhQUxILE1BS0c7QUFDSDtBQUNBOztBQUNBO0FBQ0EsV0FiQTs7QUFlQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRyxhQUxILE1BS0c7QUFDSDtBQUNBOztBQUNBO0FBQ0EsV0FkQTs7QUFnQkE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxXQUZBOztBQUlBO0FBQ0E7QUFDQSxXQUZBOztBQUlBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsV0FGQTs7QUFJQTtBQUNBO0FBQ0EsV0FGQSxDQXQvQ0EsQ0EwL0NBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0RBTEEsQ0FPQTs7QUFDQTtBQUNBLG1FQVRBLENBV0E7O0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0EseUVBaEJBLENBa0JBOztBQUNBOztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxnQ0FBcUIsTUFBckIsRUFBNkIsR0FBN0IsRUFBNkI7QUFDN0I7QUFDQTtBQUNHLGFBTEgsTUFLRztBQUNIO0FBQ0EsMEJBQWUsT0FBZixFQUF3QixHQUF4QixFQUF3QjtBQUN4QjtBQUNBO0FBQ0csYUFMQSxNQUtBO0FBQ0gsNENBQ0EsTUFEQSxFQUVBLGlDQUZBLEVBR0EsV0FIQTtBQUtBOztBQUVBO0FBQ0EsV0E5Q0EsQ0EzL0NBLENBMmlEQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSyxlQUpMLE1BSUs7QUFDTDtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0csYUFyQkgsTUFxQkc7QUFDSDtBQUNBLGFBekJBLENBMkJBOzs7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFFQTtBQUVBOztBQUNBO0FBQ0EsOEJBQW1CLE9BQW5CLEVBQTRCLEdBQTVCLEVBQTRCO0FBQzVCO0FBQ0E7QUFDRyxhQUpILE1BSUc7QUFDSCxpREFDQSxHQURBLEdBRUEsaURBRkE7QUFHQTs7QUFDQSwwQkFBZSxlQUFmLEVBQWdDLEdBQWhDLEVBQWdDO0FBQ2hDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFdBekRBLENBL2lEQSxDQTBtREE7QUFDQTs7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLGlFQUZBLENBR0E7O0FBQ0EsMENBSkEsQ0FLQTs7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEJBQWlCLFVBQWpCLEVBQTZCLEdBQTdCLEVBQTZCO0FBQzdCLCtDQUQ2QixDQUc3Qjs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ1MsbUJBSlQsTUFJUztBQUNUO0FBQ0E7QUFDQTtBQUNBLG1CQVZBLENBWUE7OztBQUNBO0FBRUE7QUFDQSxpQkFsQkEsQ0FvQkE7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBekJBLENBMkJBOzs7QUFDQTtBQUNLLGVBN0JMLE1BNkJLO0FBQ0w7QUFDQTtBQUNBOztBQUVBLG1DQXRDNkIsQ0F3QzdCOztBQUNBO0FBQ0E7QUFDQTtBQUNLLGVBSEwsTUFHSztBQUNMO0FBQ0EsMkJBQ0EsdUJBREEsRUFFQSx1QkFGQTtBQUlLLGVBTkEsTUFNQTtBQUNMO0FBQ0EsMkJBQ0EsdUJBREEsRUFFQSw4QkFGQSxFQUdBLHVCQUhBO0FBS0ssZUFQQSxNQU9BO0FBQ0w7QUFDQSwyQkFDQSx3QkFEQSxFQUVBLDhCQUZBLEVBR0EsOEJBSEEsRUFJQSx1QkFKQTtBQU1LLGVBUkEsTUFRQTtBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBQ0EsNEJBQWlCLGNBQWpCLEVBQWlDLEdBQWpDLEVBQWlDO0FBQ2pDO0FBQ0E7QUFDQTs7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFDQSw0QkFBaUIsY0FBakIsRUFBaUMsR0FBakMsRUFBaUM7QUFDakM7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw0QkFBaUIsVUFBakIsRUFBNkIsR0FBN0IsRUFBNkI7QUFDN0I7QUFDQTtBQUNBOztBQUNBO0FBQ0E7O0FBRUE7QUFDQSwrQkFEQSxDQUNBO0FBQ0E7OztTQTV2REEsRSxJQUFBLEMsSUFBQSxFOzttREFBQTs7Ozs7Ozs7Ozs7Ozs7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBOztBQUNBLGlCQUFRLFNBQVIsRUFBbUIsb0RBQW5CLEVBQW1COztBQUVuQjtBQUNBO0FBQ0E7O0FBQ0EsaUJBQVEsU0FBUixFQUFtQixvREFBbkIsRUFBbUI7O0FBRW5CO0FBQ0E7QUFDRyxXQUZILE1BRUc7QUFDSDtBQUNHLFdBRkEsTUFFQTtBQUNIO0FBQ0E7QUFDQTs7QUFDQTtBQUNBLFNBL0JBOztBQWlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDRyxXQUhILE1BR0c7QUFDSDs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0ssYUFGTCxNQUVLO0FBQ0w7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDSyxhQUhMLE1BR0s7QUFDTDtBQUNBO0FBQ0ssYUFIQSxNQUdBO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsaUJBQVEsU0FBUixFQUFtQiwwREFBbkIsRUFBbUI7O0FBRW5CO0FBQ0E7O0FBQ0EsaUJBQVEsUUFBUixFQUFrQiwwREFBbEIsRUFBa0I7O0FBRWxCO0FBQ0EsU0FsREE7Ozs7Ozs7Ozs7Ozs7OztBQ2xDQSwwQkFBaUIsUUFBakI7O0FBRUE7QUFDQTtBQUNBLFNBRkE7Ozs7Ozs7Ozs7Ozs7OztBQ0ZBLGMsQ0FFQTs7QUFDQTtBQUNBO0FBQ0MsU0FGRDs7QUFJQTtBQUNBO0FBQ0E7QUFDQyxTQUhELENBR0M7QUFDRDtBQUNBO0FBQ0EsUyxDQUVBO0FBQ0E7QUFDQTs7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNiQTs7Ozs7QUFHTyxZQUFNd1AscUJBQWI7QUFZSTs7Ozs7OztBQU9BLHNHQUtFO0FBQUE7O0FBQUE7O0FBQUE7O0FBQUE7O0FBQUE7O0FBQUE7O0FBQUE7O0FBQUE7O0FBQUE7O0FBQUE7O0FBQUEsbURBZCtCLG1FQWMvQjs7QUFDRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0g7QUFFRDs7Ozs7O0FBaENKO0FBQUE7QUFBQSwrRUFvQ3lGO0FBQ2pGLDBDQUEyQjtBQUN2QkMsc0NBQXNCLENBQXRCQSxPQUErQkEsc0JBQXNCLENBQXRCQSxRQUEvQkE7QUFDQUEsc0NBQXNCLENBQXRCQSxzQkFBOENBLHNCQUFzQixDQUF0QkEsdUJBQTlDQTtBQUNBQSxzQ0FBc0IsQ0FBdEJBLGlCQUF5Q0Esc0JBQXNCLENBQXRCQSxrQkFBekNBO0FBQ0FBLHNDQUFzQixDQUF0QkEsb0JBQTRDQSxzQkFBc0IsQ0FBdEJBLHFCQUE1Q0E7QUFDQUEsc0NBQXNCLENBQXRCQSxnQkFBd0NBLHNCQUFzQixDQUF0QkEsaUJBQXhDQTtBQUNBQSxzQ0FBc0IsQ0FBdEJBLHFCQUE2Q0Esc0JBQXNCLENBQXRCQSxzQkFBN0NBO0FBQ0g7O0FBQ0Q7QUFDSDtBQUVEOzs7O0FBaERKO0FBQUE7QUFBQSx3REFtRDZEO0FBQ3JEOztBQUNBLGtCQUNJLG9CQUNBLGdDQUZKLGdCQUVJLENBRkosRUFHRTtBQUNFQSxzQ0FBc0IsR0FBRyxnQ0FDckIsaUJBREpBLGNBQXlCLENBQXpCQTtBQUpKLHFCQU9PO0FBQ0hBLHNDQUFzQixHQUNsQixnQ0FBZ0MsS0FBaEMseUJBREpBO0FBR0g7O0FBQ0Q7QUFDSDtBQUVEOzs7OztBQXBFSjtBQUFBO0FBQUEsbURBd0U2QztBQUNyQztBQUNBLDRDQUE4QixLQUZPLHlCQUVQLEVBQTlCLENBRnFDLENBRTJCOztBQUVoRSxrQkFBTUMsVUFBVSxHQUFHLGlDQUNmLEtBREosc0JBQW1CLENBQW5CO0FBR0EseUNBQTJCQyxrRUFBM0IsVUFBMkJBLENBQTNCO0FBQ0EsdURBQXlDQSxrRUFBdUIsNEJBUjNCLGNBUUlBLENBQXpDLENBUnFDLENBVXJDOztBQUNBLDBCQUFZLG9DQUFaO0FBQ0EsbUNBQXFCLDZDQUFyQjtBQUNIO0FBRUQ7Ozs7Ozs7QUF2Rko7QUFBQTtBQUFBLDBFQStGZTtBQUNQO0FBQ0EscUJBQ0lGLHNCQUFzQixDQUF0QkEsK0JBQ0FBLHNCQUFzQixDQUF0QkEsNkJBREFBLEtBRUEsT0FBT0Esc0JBQXNCLENBQXRCQSx1QkFBUCxVQUhKO0FBTUg7QUFFRDs7Ozs7OztBQXpHSjtBQUFBO0FBQUEseUVBbUgwQjtBQUNsQjtBQUNBO0FBRUE7QUFDQSxrQkFBTUcsR0FBVyxHQUFHQyxPQUFPLENBQTNCOztBQUVBLGtCQUFJLEtBQUosU0FBa0I7QUFDZEMseUNBQXlCLEdBQXpCQTs7QUFFQSxvQkFBTUMsZ0JBQWdCLEdBQUdKLHdFQUF6QixHQUF5QkEsQ0FBekI7O0FBRUEsb0JBQUksNEJBQUosbUJBQW1EO0FBQy9DRywyQ0FBeUIsR0FBRyxDQUFDLDZCQUE3QkEsT0FBNkIsQ0FBN0JBO0FBSUg7O0FBRUQsK0NBQStCO0FBQzNCQSwyQ0FBeUIsR0FBRyxDQUFDLDhCQUE3QkEsZ0JBQTZCLENBQTdCQTtBQUdIOztBQUVELG9CQUFJLDRCQUFKLG9CQUFvRDtBQUNoRCxtQ0FBaUI7QUFDYix3QkFBSUUsSUFBSSxHQUFSOztBQUNBLHdCQUFJQSxJQUFJLFlBQVIsVUFBOEI7QUFDMUIsMEJBQUksS0FBSixpQkFBMEI7QUFDdEJBLDRCQUFJLEdBQUcsc0JBQVBBLElBQU8sQ0FBUEE7QUFESiw2QkFFTztBQUNIO0FBQ0FBLDRCQUFJLEdBQUpBO0FBQ0g7QUFOTCwyQkFPTyxJQUNILHdCQUNBQSxJQUFJLFlBRkQsaUJBR0w7QUFDRUEsMEJBQUksR0FBR0EsSUFBSSxDQUFYQSxRQUFPQSxFQUFQQTtBQUNIOztBQUVELHdCQUFJLDRCQUFKLG1CQUFtRDtBQUMvQ0YsK0NBQXlCLEdBQUcsQ0FBQyw4QkFBN0JBLE9BQTZCLENBQTdCQTtBQUlIOztBQUVELG1EQUErQjtBQUMzQkEsK0NBQXlCLEdBQUcsQ0FBQyw4QkFBN0JBLElBQTZCLENBQTdCQTtBQUdIOztBQUVELGlEQUNJLDRCQURKO0FBS0g7QUFDSjs7QUFFRCxvQkFDSUEseUJBQXlCLEtBQXpCQSxTQUNBLG9EQUZKLEdBR0U7QUFDRUEsMkNBQXlCLEdBQUcsNENBQTVCQSxRQUE0QixDQUE1QkE7QUFHSDs7QUFFRCxvQkFBSSxDQUFKLDJCQUFnQztBQUM1QkQseUJBQU8sQ0FBUEE7O0FBQ0Esc0JBQUksMkJBQTJCLEtBQS9CLHNCQUFJLENBQUosRUFBNkQ7QUFDekQsaURBQ0ksNEJBREo7QUFLSDtBQUNKO0FBQ0o7O0FBRUQ7QUFDSDtBQUVEOzs7OztBQTFNSjtBQUFBO0FBQUEsOENBOE1xQztBQUM3Qix1QkFBUztBQUNMLHVCQUFPLDBCQUVIRixrRUFBdUIsS0FBdkJBLGVBRkosRUFFSUEsQ0FGRyxDQUFQO0FBSUg7O0FBQ0Q7QUFDSDtBQUVEOzs7OztBQXhOSjtBQUFBO0FBQUEsZ0RBNE51QztBQUMvQix1QkFBUztBQUNMLHVCQUFPLDBCQUFQLEVBQU8sQ0FBUDtBQUNIOztBQUNEO0FBQ0g7QUFFRDs7Ozs7QUFuT0o7QUFBQTtBQUFBLGdEQXVPOEQ7QUFBRTtBQUN4RCx3QkFBVTtBQUNOLHVCQUFPLG9CQUVIQSxrRUFBdUIsS0FBdkJBLGVBRkosRUFFSUEsQ0FGRyxDQUFQO0FBSUg7O0FBQ0Q7QUFDSDtBQUVEOzs7OztBQWpQSjtBQUFBO0FBQUEsa0RBcVBnRTtBQUFFO0FBQzFELHdCQUFVO0FBQ04sdUJBQU8sb0JBQVAsRUFBTyxDQUFQO0FBQ0g7O0FBQ0Q7QUFDSDtBQUVEOzs7OztBQTVQSjtBQUFBO0FBQUEsb0RBZ1F3RDtBQUNoRCxrQkFBSU0sVUFBVSxHQUFkOztBQUVBLGtCQUFJLHdCQUF3QkosT0FBTyxDQUFQQSxVQUE1QixpQkFBNEJBLENBQTVCLEVBQWtFO0FBQzlESSwwQkFBVSxHQUFWQTtBQUNIOztBQUNEO0FBQ0g7QUFFRDs7Ozs7QUF6UUo7QUFBQTtBQUFBLHNEQTZRMEQ7QUFDbEQsa0JBQUlDLFlBQVksR0FBaEI7O0FBRUEsa0JBQUksMEJBQTBCTCxPQUFPLENBQVBBLFVBQTlCLGlCQUE4QkEsQ0FBOUIsRUFBb0U7QUFDaEVLLDRCQUFZLEdBQVpBO0FBQ0g7O0FBQ0Q7QUFDSDtBQUVEOzs7OztBQXRSSjtBQUFBO0FBQUEsbURBMFJ1RDtBQUMvQyxrQkFBSUQsVUFBVSxHQUFkOztBQUVBLGtCQUNJLHdCQUNBLENBQUNKLE9BQU8sQ0FBUEEsVUFERCxXQUNDQSxDQURELElBRUEsQ0FBQ0EsT0FBTyxDQUFQQSxVQUZELFdBRUNBLENBRkQsSUFHQUEsT0FBTyxDQUFQQSxVQUpKLGlCQUlJQSxDQUpKLEVBS0U7QUFDRUksMEJBQVUsR0FBVkE7QUFDSDs7QUFDRDtBQUNIO0FBRUQ7Ozs7O0FBeFNKO0FBQUE7QUFBQSxxREE0U3lEO0FBQ2pELGtCQUFJQyxZQUFZLEdBQWhCOztBQUVBLGtCQUNJLDBCQUNBLENBQUNMLE9BQU8sQ0FBUEEsVUFERCxXQUNDQSxDQURELElBRUEsQ0FBQ0EsT0FBTyxDQUFQQSxVQUZELFdBRUNBLENBRkQsSUFHQUEsT0FBTyxDQUFQQSxVQUpKLGlCQUlJQSxDQUpKLEVBS0U7QUFDRUssNEJBQVksR0FBWkE7QUFDSDs7QUFDRDtBQUNIO0FBRUQ7Ozs7Ozs7QUExVEo7QUFBQTtBQUFBLDBFQW9VYztBQUNOLGtCQUFJQyxJQUFJLENBQUpBLGVBQW9CLENBQXhCLEdBQTRCO0FBQ3hCQSxvQkFBSSxHQUFHLHlEQUFQQSxVQUFPLENBQVBBO0FBS0g7O0FBQ0Qsa0JBQU1DLFNBQVMsR0FBR0QsSUFBSSxDQUFKQSxNQUFsQixHQUFrQkEsQ0FBbEI7O0FBRUEsbUJBQUssSUFBSWhSLENBQUMsR0FBVixHQUFnQkEsQ0FBQyxHQUFHaVIsU0FBUyxDQUE3QixRQUFzQ2pSLENBQXRDLElBQTJDO0FBQ3ZDLG9CQUNJaVIsU0FBUyxDQUFUQSxDQUFTLENBQVRBLFdBQ0EseUJBREFBLFFBRUEsOEJBQThCQSxTQUFTLENBSDNDLENBRzJDLENBQXZDLENBSEosRUFJRTtBQUNFQSwyQkFBUyxDQUFUQSxDQUFTLENBQVRBLEdBQWVBLFNBQVMsQ0FBVEEsQ0FBUyxDQUFUQSxTQUNYLEtBRFdBLHFCQUFmQSxpQkFBZUEsQ0FBZkE7QUFJQUEsMkJBQVMsQ0FBVEEsQ0FBUyxDQUFUQSxHQUFlVCx3RUFBNkJTLFNBQVMsQ0FBckRBLENBQXFELENBQXRDVCxDQUFmUzs7QUFDQSxzQkFBSSwwQkFBMEJBLFNBQVMsQ0FBVEEsQ0FBUyxDQUFUQSxLQUE5QixJQUFtRDtBQUMvQztBQUNBQSw2QkFBUyxDQUFUQTtBQUNBalIscUJBQUM7QUFDSjtBQWRMLHVCQWVPLElBQ0gsaURBQ0FpUixTQUFTLENBQVRBLENBQVMsQ0FBVEEsVUFEQSxLQUVBLHdCQUF3QkEsU0FBUyxDQUg5QixDQUc4QixDQUFqQyxDQUhHLEVBSUw7QUFDRTtBQUNBLHNCQUFJO0FBQ0Esd0JBQU1DLGFBQWEsR0FBR3RQLHlEQUFjcVAsU0FBUyxDQUE3QyxDQUE2QyxDQUF2QnJQLENBQXRCOztBQUNBLHdCQUNJLGlDQUNBLDhCQUZKLGFBRUksQ0FGSixFQUdFO0FBQ0VxUCwrQkFBUyxDQUFUQSxDQUFTLENBQVRBLEdBQWVBLFNBQVMsQ0FBVEEsQ0FBUyxDQUFUQSxTQUNYLEtBRFdBLGVBQWZBLGlCQUFlQSxDQUFmQTtBQUlBQSwrQkFBUyxDQUFUQSxDQUFTLENBQVRBLEdBQWVULHdFQUE2QlMsU0FBUyxDQUFyREEsQ0FBcUQsQ0FBdENULENBQWZTOztBQUNBLDBCQUFJLDBCQUEwQkEsU0FBUyxDQUFUQSxDQUFTLENBQVRBLEtBQTlCLElBQW1EO0FBQy9DO0FBQ0FBLGlDQUFTLENBQVRBO0FBQ0FqUix5QkFBQztBQUNKO0FBQ0o7QUFoQkwsb0JBaUJFLFVBQVUsQ0FDUjtBQUNIO0FBQ0o7QUFDSjs7QUFDRCxrQkFBTW1SLE9BQU8sR0FBR0YsU0FBUyxDQUFUQSxLQUFoQixHQUFnQkEsQ0FBaEI7QUFDQTtBQUNIO0FBRUQ7Ozs7O0FBOVhKO0FBQUE7QUFBQSx1REFrWW9EO0FBQzVDLGtCQUFNRyxVQUFVLEdBQWhCO0FBQ0E7O0FBQ0EsbUJBQUtDLEVBQUUsR0FBR0MsUUFBUSxDQUFsQixPQUFVQSxFQUFWLEVBQThCLENBQUMsQ0FBQzNPLENBQUMsR0FBRzBPLEVBQUUsQ0FBUCxJQUFLQSxFQUFMLEVBQUQsU0FBMEJFLElBQUksR0FBRzVPLENBQUMsQ0FBaEUsS0FBOEIsQ0FBOUIsR0FBMkU7QUFDdkV5TywwQkFBVSxDQUFWQSxLQUFnQkcsSUFBSSxDQUFKQSxDQUFJLENBQUpBLFNBQWdCQSxJQUFJLENBQXBDSCxDQUFvQyxDQUFwQ0E7QUFDSDs7QUFDRCxxQkFBT0EsVUFBVSxDQUFWQSxLQUFQLEdBQU9BLENBQVA7QUFDSDtBQUVEOzs7Ozs7QUEzWUo7QUFBQTtBQUFBLGtGQWdadUY7QUFDL0Usa0JBQUlJLFlBQVksR0FBaEI7QUFDQSxrQkFBTTVJLEtBQUssR0FBRzZJLFdBQVcsQ0FBWEEsTUFBa0IsS0FBaEMsYUFBY0EsQ0FBZDs7QUFDQSxtQkFBSyxJQUFMLFlBQXVCO0FBQ25CLG9CQUFJN0ksS0FBSyxDQUFMQSxDQUFLLENBQUxBLFVBQUosR0FBeUI7QUFFekIsb0JBQU1zSSxhQUFhLEdBQUd0UCx5REFBY2dILEtBQUssQ0FBekMsQ0FBeUMsQ0FBbkJoSCxDQUF0Qjs7QUFDQSxvQkFBSSw4QkFBSixhQUFJLENBQUosRUFBa0Q7QUFDOUM0UCw4QkFBWSxHQUFHQyxXQUFXLENBQVhBLFFBQW9CN0ksS0FBSyxDQUF6QjZJLENBQXlCLENBQXpCQSxFQUFmRCxzQkFBZUMsQ0FBZkQ7QUFDSDtBQUNKOztBQUVEO0FBQ0g7QUFFRDs7Ozs7O0FBL1pKO0FBQUE7QUFBQSw4REF1YStCO0FBQ3ZCLGtCQUFJLDRCQUFKLG9CQUFvRDtBQUNoRCxvQkFBSXBNLElBQUksWUFBSkEsVUFBMEIsZ0JBQTlCLFVBQXdEO0FBQ3BELHNCQUFJLDRCQUFKLG1CQUFtRDtBQUMvQ0Esd0JBQUksR0FBRyw0QkFBUEEsaUJBQU8sQ0FBUEE7QUFDSDs7QUFFRCxzQkFBSSx5QkFBSixNQUFtQztBQUMvQiwyQkFBTyw4QkFBUCxJQUFPLENBQVAsRUFBNEM7QUFDeEM7QUFDQTtBQUNBQSwwQkFBSSxHQUFHQSxJQUFJLENBQUpBLFFBQWEsS0FBYkEscUJBQVBBLGlCQUFPQSxDQUFQQTtBQUNIO0FBQ0o7QUFYTCx1QkFZTyxJQUFJLHdCQUF3QkEsSUFBSSxZQUFoQyxVQUFzRDtBQUN6RDtBQUNBLHNCQUFNc00sT0FBTyxHQUFHLElBQWhCLFFBQWdCLEVBQWhCO0FBQ0E7O0FBQ0EsdUJBQ0lMLEVBQUUsR0FBR2pNLElBQUksQ0FEYixPQUNTQSxFQURULEVBRUksQ0FBQyxDQUFDekMsQ0FBQyxHQUFHME8sRUFBRSxDQUFQLElBQUtBLEVBQUwsRUFBRCxTQUEwQkUsSUFBSSxHQUFHNU8sQ0FBQyxDQUZ0QyxLQUVJLENBRkosR0FJRTtBQUNFLHdCQUFNZ1AsR0FBRyxHQUFHLGNBQWNKLElBQUksQ0FBbEIsQ0FBa0IsQ0FBbEIsRUFBWixpQkFBWSxDQUFaO0FBQ0Esd0JBQU1uTyxLQUFLLEdBQUcsY0FBY21PLElBQUksQ0FBbEIsQ0FBa0IsQ0FBbEIsRUFBZCxpQkFBYyxDQUFkO0FBQ0FHLDJCQUFPLENBQVBBO0FBQ0g7O0FBRUR0TSxzQkFBSSxHQUFKQTtBQWRHLHVCQWVBLElBQUksd0JBQXdCQSxJQUFJLFlBQWhDLGlCQUE2RDtBQUNoRSxzQkFBTXNNLFFBQU8sR0FBRyxJQUFoQixlQUFnQixFQUFoQjs7QUFDQTs7QUFDQSx1QkFBS0wsR0FBRSxHQUFHak0sSUFBSSxDQUFkLE9BQVVBLEVBQVYsRUFBMEIsQ0FBQyxDQUFDekMsRUFBQyxHQUFHME8sR0FBRSxDQUFQLElBQUtBLEVBQUwsRUFBRCxTQUEwQkUsS0FBSSxHQUFHNU8sRUFBQyxDQUE1RCxLQUEwQixDQUExQixHQUF1RTtBQUNuRSx3QkFBTWdQLElBQUcsR0FBRyxjQUFjSixLQUFJLENBQWxCLENBQWtCLENBQWxCLEVBQVosaUJBQVksQ0FBWjs7QUFDQSx3QkFBTW5PLE1BQUssR0FBRyxjQUFjbU8sS0FBSSxDQUFsQixDQUFrQixDQUFsQixFQUFkLGlCQUFjLENBQWQ7O0FBQ0FHLDRCQUFPLENBQVBBO0FBQ0g7O0FBQ0R0TSxzQkFBSSxHQUFKQTtBQUNIO0FBQ0o7O0FBRUQ7QUFDSDtBQUVEOzs7Ozs7QUFuZEo7QUFBQTtBQUFBLCtFQXdkdUY7QUFDL0Usa0JBQUksQ0FBQyxLQUFELFdBQWlCLENBQUNvTCxnRUFBdEIsU0FBc0JBLENBQXRCLEVBQXVEO0FBRXZELGtCQUFNb0IsVUFBVSxHQUFHLGNBQW5CO0FBQ0Esa0JBQUlDLGlCQUFpQixHQUowRCxzQkFJL0UsQ0FKK0UsQ0FNL0U7O0FBQ0Esa0JBQUlDLHNCQUFzQixLQUExQixJQUFtQztBQUMvQkQsaUNBQWlCLEdBQWpCQTtBQVIyRSxnQkFXL0U7QUFDQTs7O0FBRUEsa0JBQUlKLFdBQVcsR0FBR00sU0FBUyxDQUFUQSxXQUFsQixDQUFrQkEsQ0FBbEI7QUFFQSxrQkFBTXRCLEdBQUcsR0FBRyxJQUFJdUIsMENBQUosS0FBSUEsQ0FBSixDQWhCbUUsU0FnQm5FLENBQVosQ0FoQitFLENBa0IvRTs7QUFDQXZCLGlCQUFHLENBQUhBLFdBQWUsY0FBY0EsR0FBRyxDQUFqQiw2QkFuQmdFLFVBbUJoRSxDQUFmQSxDQW5CK0UsQ0FxQi9FOztBQUNBLGtCQUFJLG1DQUFtQ2dCLFdBQVcsS0FBbEQsSUFBMkQ7QUFDdkQsb0JBQUksNEJBQUosbUJBQW1EO0FBQy9DQSw2QkFBVyxHQUFHLG1DQUFkQSxpQkFBYyxDQUFkQTtBQUNIOztBQUVELG9CQUFNUSxZQUFZLEdBQUdSLFdBQVcsQ0FBWEEsTUFBckIsR0FBcUJBLENBQXJCOztBQUNBLG9CQUFJUSxZQUFZLENBQVpBLFNBQUosR0FBNkI7QUFDekJ4QixxQkFBRyxDQUFIQSxTQUFhLHVEQUFiQSxVQUFhLENBQWJBOztBQUtBLHNCQUFJbUIsVUFBVSxJQUFJbkIsR0FBRyxDQUFIQSxXQUFsQixJQUFxQztBQUNqQztBQUNBQSx1QkFBRyxDQUFIQSxPQUFXQSxHQUFHLENBQUhBLGtCQUFYQSxFQUFXQSxDQUFYQTtBQUNIO0FBQ0o7QUFDSjs7QUFFRCxxQkFBT0EsR0FBRyxDQUFWO0FBQ0g7QUFFRDs7Ozs7QUFwZ0JKO0FBQUE7QUFBQSxnRkEwZ0JxQjtBQUNiLGtCQUFNRixVQUFVLEdBREgsRUFDYixDQURhLENBRWI7O0FBQ0Esa0JBQUksQ0FBQywyQkFBTCxzQkFBSyxDQUFMLEVBQXlEO0FBQ3JELHVCQUFPRCxzQkFBc0IsQ0FBdEJBLHVCQUFQO0FBREoscUJBRU87QUFDSCxxQkFDSSxJQUFJdFEsQ0FBQyxHQURULEdBRUlBLENBQUMsR0FBR3NRLHNCQUFzQixDQUF0QkEsb0JBRlIsUUFHSXRRLENBSEosSUFJRTtBQUNFdVEsNEJBQVUsQ0FBVkEsS0FBZ0JELHNCQUFzQixDQUF0QkEsdUJBQWhCQztBQUNIO0FBQ0o7O0FBQ0Q7QUFDSDtBQUVEOzs7Ozs7O0FBM2hCSjtBQUFBO0FBQUEsb0dBcWlCWTtBQUNKLG1CQUFLLElBQUl2USxDQUFDLEdBQVYsR0FBZ0JBLENBQUMsR0FBR2tTLG1CQUFtQixDQUF2QyxRQUFnRGxTLENBQWhELElBQXFEO0FBQ2pELG9CQUFNbVMsS0FBSyxHQUFHLFdBQVdELG1CQUFtQixDQUFuQkEsQ0FBbUIsQ0FBbkJBLENBQXpCLEtBQWMsQ0FBZDs7QUFDQSxvQkFBSUMsS0FBSyxDQUFMQSxRQUFjQSxLQUFLLENBQUxBLEtBQWxCLGdCQUFrQkEsQ0FBbEIsRUFBZ0Q7QUFDNUMsc0JBQU1DLGtCQUFrQixHQUFHRixtQkFBbUIsQ0FBbkJBLENBQW1CLENBQW5CQSxDQUEzQjtBQUNBeEIseUJBQU8sQ0FBUEE7QUFDSDtBQUNKO0FBQ0o7QUFFRDs7Ozs7O0FBL2lCSjtBQUFBO0FBQUEsNkRBb2pCMEU7QUFDbEUsa0JBQU05SCxLQUFLLEdBQUc2SCxHQUFHLENBQUhBLE1BQVUsS0FBeEIsYUFBY0EsQ0FBZDs7QUFDQSxtQkFBSyxJQUFMLFlBQXVCO0FBQ25CLG9CQUFJN0gsS0FBSyxDQUFMQSxDQUFLLENBQUxBLFVBQUosR0FBeUI7QUFFekIsb0JBQU1zSSxhQUFhLEdBQUd0UCx5REFBY2dILEtBQUssQ0FBekMsQ0FBeUMsQ0FBbkJoSCxDQUF0Qjs7QUFDQSxvQkFBSSw4QkFBSixhQUFJLENBQUosRUFBa0Q7QUFDOUMsK0NBQ0ksNEJBREo7QUFLQThPLHlCQUFPLENBQVBBO0FBQ0E7QUFDSDtBQUNKOztBQUVEO0FBQ0g7QUFFRDs7Ozs7OztBQXhrQko7QUFBQTtBQUFBLDRGQWtsQmM7QUFDTjtBQUNBLGtCQUFJMkIsZ0JBQStCLEdBQUdyQixJQUFJLENBQUpBLE1BRmhDLEdBRWdDQSxDQUF0QyxDQUZNLENBSU47O0FBQ0FxQiw4QkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBaEIsT0FBd0IsaUJBQWlCO0FBQ3hELHVCQUFPLDZCQUE2QkMsS0FBSyxLQUF6QztBQURKRCxlQUFtQixDQUFuQkE7QUFJQSxrQkFBSWxCLE9BQU8sR0FBWDtBQUVBLGtCQUFNdE8sSUFBSSxHQUFWOztBQUVBMk4sMEZBQWlDLDRCQUE0QjtBQUN6RDtBQUNBLG9CQUFNK0IsaUJBQWlCLEdBQUdDLFNBQVMsQ0FBVEEsTUFBMUIsR0FBMEJBLENBQTFCOztBQUNBaEMsNkZBQWtDLHdCQUF3QjtBQUV0RCxzQkFBTWlDLFVBQVUsR0FBR0gsS0FBSyxDQUFMQSxNQUFuQixHQUFtQkEsQ0FBbkI7QUFDQSxzQkFBSVgsR0FBRyxHQUFHYyxVQUFVLENBQXBCLENBQW9CLENBQXBCO0FBQ0Esc0JBQU1DLHFCQUFxQixHQUFHRCxVQUFVLENBQVZBLENBQVUsQ0FBVkEsSUFBOUI7QUFDQSxzQkFBSUUsZUFBZSxHQUxtQyxTQUt0RCxDQUxzRCxDQUtyQjs7QUFDakMsc0JBQUl2UCxLQUFLLEdBQUdvTix3RUFBWixxQkFBWUEsQ0FBWjs7QUFDQSxzQkFBSW9DLFdBQVcsR0FQdUMsRUFPdEQsQ0FQc0QsQ0FRdEQ7O0FBQ0Esc0JBQ0ksMkJBQ0FqQixHQUFHLEtBREgsTUFFQTlPLElBQUksQ0FBSkEsb0JBSEosTUFJRTtBQUNFLDJCQUFPQSxJQUFJLENBQUpBLHlCQUFQLEdBQU9BLENBQVAsRUFBMkM7QUFDdkM4UCxxQ0FBZSxHQUFHZixVQUFVLFVBRFcsS0FDdkNlLENBRHVDLENBRXZDO0FBQ0E7O0FBQ0FoQix5QkFBRyxHQUFHQSxHQUFHLENBQUhBLFFBQVk5TyxJQUFJLENBQWhCOE8scUJBQU5BLGlCQUFNQSxDQUFOQTtBQUNIO0FBbkJpRCxvQkFxQnREOzs7QUFDQSxzQkFDSSw2QkFDQXZPLEtBQUssS0FETCxNQUVBUCxJQUFJLENBQUpBLG9CQUhKLE1BSUU7QUFDRSwyQkFBT0EsSUFBSSxDQUFKQSx5QkFBUCxLQUFPQSxDQUFQLEVBQTZDO0FBQ3pDO0FBQ0E7QUFDQThQLHFDQUFlLEdBQUdmLFVBQVUsVUFBNUJlO0FBQ0F2UCwyQkFBSyxHQUFHQSxLQUFLLENBQUxBLFFBQWNQLElBQUksQ0FBbEJPLHFCQUFSQSxpQkFBUUEsQ0FBUkE7QUFDSDtBQUNKOztBQUVELHNCQUFJLDJCQUEyQixpQkFBL0IsVUFBMEQ7QUFBRTtBQUN4REEseUJBQUssR0FBR29OLHdFQUFScE4sS0FBUW9OLENBQVJwTjs7QUFDQSx3QkFBSXVQLGVBQWUsSUFBbkIsWUFBbUM7QUFBRTtBQUNqQ0MsaUNBQVcsSUFBWEE7QUFESiwyQkFFTyxJQUFJeFAsS0FBSyxLQUFMQSxNQUFnQixDQUFwQixpQkFBc0M7QUFDekN3UCxpQ0FBVyxJQUFYQTtBQURHLDJCQUVBO0FBQ0hBLGlDQUFXLElBQUlqQixHQUFHLEdBQUhBLE1BQWZpQjtBQUNIO0FBQ0o7O0FBRUQsc0JBQUdBLFdBQVcsS0FBZCxJQUFzQjtBQUNsQkwscUNBQWlCLENBQWpCQSxjQURrQixDQUNsQkEsRUFEa0IsQ0FDa0I7QUFEeEMseUJBRU87QUFDSEEscUNBQWlCLENBQWpCQSxLQUFpQixDQUFqQkE7QUFDSDtBQWxETC9COztBQXFEQTZCLGdDQUFnQixDQUFoQkEsS0FBZ0IsQ0FBaEJBLEdBQTBCRSxpQkFBaUIsQ0FBakJBLEtBQTFCRixHQUEwQkUsQ0FBMUJGO0FBeERKN0I7O0FBNERBVyxxQkFBTyxJQUFJa0IsZ0JBQWdCLENBQWhCQSxLQUFYbEIsR0FBV2tCLENBQVhsQjtBQUVBO0FBQ0g7QUFFRDs7Ozs7OztBQWhxQko7QUFBQTtBQUFBLDBGQTBxQmM7QUFDTjtBQUNBLGtCQUFJMEIsY0FBYyxHQUFsQjtBQUNBWiwwQkFBWSxHQUFHLFlBQVksQ0FBWixPQUFvQixpQkFBaUI7QUFDaEQsdUJBQU8sNkJBQTZCSyxLQUFLLEtBQXpDO0FBREpMLGVBQWUsQ0FBZkE7QUFHQSxrQkFBTXBQLElBQUksR0FBVjs7QUFFQTJOLHNGQUE2QixpQkFBaUI7QUFDMUMsb0JBQU1pQyxVQUFVLEdBQUdILEtBQUssQ0FBTEEsTUFBbkIsR0FBbUJBLENBQW5CO0FBQ0Esb0JBQUlYLEdBQUcsR0FBR2MsVUFBVSxDQUFwQixDQUFvQixDQUFwQjtBQUNBLG9CQUFNQyxxQkFBcUIsR0FBR0QsVUFBVSxDQUFWQSxDQUFVLENBQVZBLElBQTlCO0FBQ0Esb0JBSjBDLGVBSTFDLENBSjBDLENBSXJCOztBQUNyQixvQkFBSXJQLEtBQUssR0FBR29OLHdFQUw4QixxQkFLOUJBLENBQVosQ0FMMEMsQ0FNMUM7OztBQUNBLG9CQUNJLDJCQUNBbUIsR0FBRyxLQURILE1BRUE5TyxJQUFJLENBQUpBLG9CQUhKLE1BSUU7QUFDRSx5QkFBT0EsSUFBSSxDQUFKQSx5QkFBUCxHQUFPQSxDQUFQLEVBQTJDO0FBQ3ZDOFAsbUNBQWUsR0FBR2YsVUFBVSxVQURXLEtBQ3ZDZSxDQUR1QyxDQUV2QztBQUNBOztBQUNBaEIsdUJBQUcsR0FBR0EsR0FBRyxDQUFIQSxRQUNGOU8sSUFBSSxDQURGOE8scUJBQU5BLGlCQUFNQSxDQUFOQTtBQUlIO0FBcEJxQyxrQkF1QjFDOzs7QUFDQSxvQkFDSSw2QkFDQXZPLEtBQUssS0FETCxNQUVBUCxJQUFJLENBQUpBLG9CQUhKLE1BSUU7QUFDRSx5QkFBT0EsSUFBSSxDQUFKQSx5QkFBUCxLQUFPQSxDQUFQLEVBQTZDO0FBQ3pDO0FBQ0E7QUFDQThQLG1DQUFlLEdBQUdmLFVBQVUsVUFBNUJlO0FBQ0F2UCx5QkFBSyxHQUFHQSxLQUFLLENBQUxBLFFBQWNQLElBQUksQ0FBbEJPLHFCQUFSQSxpQkFBUUEsQ0FBUkE7QUFDSDtBQWxDcUMsa0JBcUMxQzs7O0FBQ0Esb0JBQUksMkJBQTJCLGlCQUEvQixVQUEwRDtBQUN0REEsdUJBQUssR0FBR29OLHdFQUFScE4sS0FBUW9OLENBQVJwTjs7QUFDQSxzQkFBSXVQLGVBQWUsSUFBbkIsWUFBbUM7QUFDL0JFLGtDQUFjLElBQWRBO0FBREoseUJBRU8sSUFBSXpQLEtBQUssS0FBTEEsTUFBZ0IsQ0FBcEIsaUJBQXNDO0FBQ3pDeVAsa0NBQWMsSUFBSWxCLEdBQUcsR0FBckJrQjtBQURHLHlCQUVBO0FBQ0hBLGtDQUFjLElBQUlsQixHQUFHLEdBQUhBLGNBQWxCa0I7QUFDSDtBQUNKO0FBdkRDLGVBUU5yQyxFQVJNLENBMEROOzs7QUFDQXFDLDRCQUFjLEdBQUdBLGNBQWMsQ0FBZEEsU0FBd0IsQ0FBekNBLENBQWlCQSxDQUFqQkE7QUFDQTtBQUNIO0FBdnVCTDs7QUFBQTtBQUFBLFdBQU87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVEEsWUFBTUMsVUFBYjtBQVdFLDJDQUF5QztBQUFBOztBQUFBLGlCQUF0QkMsV0FBc0IsR0FBdEJBLFdBQXNCOztBQUFBOztBQUFBLDJDQVRsQixDQUFDLFVBU2lCOztBQUFBLHlDQVI1QixDQUFDLENBUTJCOztBQUFBLDRDQVBqQixJQU9pQjs7QUFBQSw0Q0FOakIsSUFNaUI7O0FBQUEsNkNBTGQsRUFLYzs7QUFBQSxrREFKVCxFQUlTOztBQUFBLDBDQUhqQixFQUdpQjs7QUFBQTtBQUN4QztBQUVEOzs7Ozs7QUFkRjtBQUFBO0FBQUEsOENBa0IwQztBQUNwQztBQUNIO0FBRUQ7Ozs7O0FBdEJGO0FBQUE7QUFBQSw4Q0EwQjZDO0FBQ3ZDLGtCQUFJLHNCQUFKLEdBQTZCO0FBQ3pCLHVCQUFPLFdBQVcsa0JBQVgsR0FBVyxDQUFYLE9BQVAsTUFBTyxDQUFQO0FBQ0g7O0FBQ0Q7QUFDSDtBQUVEOzs7O0FBakNGO0FBQUE7QUFBQSx5Q0FvQytCO0FBQ3pCLHFCQUFPLGFBQVA7QUFDSDtBQXRDSDs7QUFBQTtBQUFBLFdBQU87Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQVA7QUFBQTs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7OztBQUFBO0FBQUE7QUFBQTtBQUFBOzs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7OztBQUFBO0FBQUE7QUFBQTtBQUFBOzs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7OztBQUFBO0FBQUE7QUFBQTtBQUFBOzs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7O0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNJQTs7Ozs7QUFHTyxZQUFNQyxhQUFiO0FBVUk7Ozs7Ozs7O0FBUUEsc0ZBTUU7QUFBQTs7QUFBQSxjQUZVQyxXQUVWLHVFQUZvQyxFQUVwQztBQUFBLGNBRFVDLFdBQ1YsdUVBRG9DLEVBQ3BDOztBQUFBOztBQUFBLGVBTFVDLFdBS1YsR0FMVUEsV0FLVjtBQUFBLGVBSlVDLFdBSVYsR0FKVUEsV0FJVjtBQUFBLGVBSFVDLDJCQUdWLEdBSFVBLDJCQUdWO0FBQUEsZUFGVUosV0FFVixHQUZVQSxXQUVWO0FBQUEsZUFEVUMsV0FDVixHQURVQSxXQUNWOztBQUFBOztBQUFBLDZDQXJCa0IsZ0JBcUJsQjs7QUFBQTs7QUFBQSxpREFuQnNCLElBbUJ0Qjs7QUFBQSxrREFsQnVCLEtBa0J2Qjs7QUFBQSxpREFqQnNCLEtBaUJ0Qjs7QUFBQSxtREFoQmlDLFNBZ0JqQzs7QUFBQSxnREFvQm9CLHVDQUF1RTtBQUV6RixnQkFBTUksU0FBUyxHQUFHLEtBQUksQ0FBdEIsWUFBa0IsRUFBbEI7O0FBQ0EsZ0JBQUlDLHFCQUFxQixHQUhnRSxLQUd6RixDQUh5RixDQUl6Rjs7QUFDQSxnQkFBSSxLQUFJLENBQUosa0JBQUosT0FBa0M7QUFFOUIsa0JBQUlELFNBQVMsQ0FBVEEsUUFBa0IsQ0FBQ0EsU0FBUyxDQUFUQSxLQUF2QixRQUF1QkEsQ0FBdkIsRUFBaUQ7QUFFN0NDLHFDQUFxQixHQUFyQkE7QUFDSDtBQUxMLGNBUUE7QUFSQSxpQkFTSyxJQUFJRCxTQUFTLENBQVRBLFFBQWtCQSxTQUFTLENBQVRBLEtBQXRCLFFBQXNCQSxDQUF0QixFQUFnRDtBQUNqREMscUNBQXFCLEdBQXJCQTtBQUNIOztBQUVELGdCQUFJLEtBQUksQ0FBSixxQkFBSixNQUFvQztBQUNoQ0EsbUNBQXFCLEdBQUcsS0FBSSxDQUFKLG1EQUF4QkEscUJBQXdCLENBQXhCQTtBQUNIOztBQUNELGdCQUFJLENBQUosdUJBQTRCO0FBQ3hCN0MscUJBQU8sQ0FBUEEsVUFBa0IsS0FBSSxDQUFKLDhCQUFsQkE7O0FBQ0Esa0JBQU04QyxpQkFBaUIsR0FBRyxLQUFJLENBQUosc0JBQTFCLFFBQTBCLENBQTFCOztBQUNBOUMscUJBQU8sQ0FBUEE7QUFDSDs7QUFDRDtBQTlDRjs7QUFBQSxvREF1RHdCLGlFQUFrRztBQUN4SCxpQkFBSSxDQUFKLGVBQW9CLEtBQUksQ0FBeEI7O0FBQ0EsZ0JBQUksS0FBSSxDQUFKLGtCQUF1QixLQUFJLENBQUoscUJBQTNCLE1BQTJEO0FBQ3ZELG1CQUFJLENBQUosb0JBQXlCLElBQUkrQyxnREFBSixtQkFBSUEsQ0FBSixDQUFzQixLQUFJLENBQTFCLE9BQXNCLEVBQXRCLEVBQXNDLEtBQUksQ0FBMUMsc0JBQWlFLEtBQUksQ0FBckUsYUFBbUYsS0FBSSxDQUF2RixhQUFxRyxLQUFJLENBQXpHLDZCQUF1SSxLQUFJLENBQTNJLGFBQXlKLEtBQUksQ0FBdEwsV0FBeUIsQ0FBekI7QUFDSDs7QUFDRCxnQkFBSSxLQUFJLENBQUoscUJBQUosTUFBb0M7QUFDaEMsbUJBQUksQ0FBSjtBQUNIO0FBOURIOztBQUFBLHFEQXdFeUIseUVBQStHO0FBQ3RJLGlCQUFJLENBQUo7O0FBQ0EsaUJBQUksQ0FBSjtBQTFFRjs7QUFBQSxnREFpRm9CLFlBQWU7QUFDakMsbUJBQU8sS0FBSSxDQUFYO0FBbEZGOztBQUFBLDJDQTZGZ0IsWUFBWTtBQUMxQixtQkFBTyxLQUFJLENBQVg7QUE5RkY7O0FBQUEsa0RBd0d1Qiw4RUFBb0g7QUFFekksaUJBQUksQ0FBSjs7QUFDQSxpQkFBSSxDQUFKOztBQUVBLGdCQUFNek4sSUFBSSxHQUFHLEtBQUksQ0FBakIsT0FBYSxFQUFiOztBQUNBLGdCQUFJME4sR0FBRyxHQUFQOztBQUVBLGlCQUFLLElBQUwsa0JBQTZCO0FBQ3pCLGtCQUFNQyxXQUFXLEdBQUdOLDJCQUEyQixDQUEvQyxRQUErQyxDQUEvQzs7QUFDQSxrQkFBSSxLQUFJLENBQUosK0JBQUosV0FBSSxDQUFKLEVBQXNEO0FBQ2xESyxtQkFBRyxHQUFHQSxHQUFHLENBQUhBLE9BQVcsS0FBSSxDQUFKLGNBQW1CMU4sSUFBSSxDQUF2QixRQUF1QixDQUF2QixFQUFqQjBOLEtBQWlCLENBQVhBLENBQU5BO0FBQ0g7QUFDSjs7QUFFRCxnQkFBSSxLQUFJLENBQVIsZUFBd0I7QUFDcEIsa0JBQUlULFdBQVcsSUFBSUEsV0FBVyxDQUFYQSxTQUFuQixHQUEwQztBQUN0Q1MsbUJBQUcsR0FBR0EsR0FBRyxDQUFIQSxPQUFXLEtBQUksQ0FBSiwyQkFBakJBLEtBQWlCLENBQVhBLENBQU5BO0FBQ0g7O0FBQ0Qsa0JBQUlSLFdBQVcsSUFBSUEsV0FBVyxDQUFYQSxTQUFuQixHQUEwQztBQUN0Q1EsbUJBQUcsR0FBRyxHQUFHLENBQUgsT0FBVyxtQkFBYTtBQUMxQix5QkFBTyxLQUFJLENBQUosdURBQTJELENBQWxFO0FBREpBLGlCQUFNLENBQU5BO0FBR0g7QUFSTCxjQVVBO0FBVkEsaUJBV0s7QUFDRCxvQkFBSVIsV0FBVyxJQUFJQSxXQUFXLENBQVhBLFNBQW5CLEdBQTJDO0FBQ3ZDUSxxQkFBRyxHQUFHQSxHQUFHLENBQUhBLE9BQVcsS0FBSSxDQUFKLDJCQUFqQkEsS0FBaUIsQ0FBWEEsQ0FBTkE7QUFDSDs7QUFDRCxvQkFBSVQsV0FBVyxJQUFJQSxXQUFXLENBQVhBLFNBQW5CLEdBQTJDO0FBQ3ZDUyxxQkFBRyxHQUFHLEdBQUcsQ0FBSCxPQUFXLG1CQUFhO0FBQzFCLDJCQUFPLEtBQUksQ0FBSix1REFBMkQsQ0FBbEU7QUFESkEsbUJBQU0sQ0FBTkE7QUFHSDtBQUNKOztBQUVELGdCQUFNRSxLQUFLLEdBQUksS0FBSSxDQUFMLGFBQUMsR0FBRCxHQUFDLEdBQWY7QUFFQSxpQkFBSSxDQUFKLFlBQWlCcEQsdUVBQWpCLEtBQWlCQSxDQUFqQjtBQS9JRjs7QUFBQSx3REF3SjZCLGlDQUFpRTtBQUM1RixnQkFBSXFELG9CQUFvQixHQUF4Qjs7QUFFQSxnQkFBSUMsUUFBUSxLQUFLLEtBQUksQ0FBckIsaUJBQXVDO0FBQ25DO0FBQ0FELGtDQUFvQixHQUFwQkE7QUFDQTtBQU53RixjQVM1Rjs7O0FBQ0EsZ0JBQUksS0FBSSxDQUFSLGVBQXdCO0FBQ3BCO0FBQ0Esa0JBQUlGLFdBQVcsS0FBZixLQUF5QjtBQUNyQkUsb0NBQW9CLEdBQXBCQTtBQURKLGdCQUdBO0FBSEEsbUJBSUssSUFBSSxDQUFDLGdCQUFnQkYsV0FBVyxLQUE1QixRQUF5QyxLQUFJLENBQUosdUNBQTdDLENBQTZDLENBQTdDLEVBQTZGO0FBQzlGRSxzQ0FBb0IsR0FBcEJBO0FBQ0g7QUFSTCxjQVVBO0FBVkEsaUJBV0s7QUFDRCxvQkFBSUYsV0FBVyxLQUFmLEtBQXlCO0FBQ3JCRSxzQ0FBb0IsR0FBcEJBO0FBREosdUJBR0ssSUFBSSxDQUFDLGdCQUFnQkYsV0FBVyxLQUE1QixRQUF5QyxLQUFJLENBQUosdUNBQTdDLENBQTZDLENBQTdDLEVBQTZGO0FBQzlGRSxzQ0FBb0IsR0FBcEJBO0FBQ0g7QUFDSjs7QUFFRDtBQXRMRjs7QUFBQSxnRUErTHFDLHlCQUE0QztBQUMvRSxtQkFBTyxLQUFJLENBQUosZUFBb0IsS0FBSSxDQUFKLGtDQUEzQjtBQWhNRjs7QUFBQSxpREF5TXNCLCtCQUFrRztBQUN0SCxnQkFBTUUsaUJBQWlCLEdBQXZCOztBQUVBLGlCQUFLLElBQUkvVCxDQUFDLEdBQVYsR0FBZ0JBLENBQUMsR0FBR2dVLGNBQWMsQ0FBbEMsUUFBMkNoVSxDQUEzQyxJQUFnRDtBQUM1QyxrQkFBTWdJLEdBQUcsR0FBR2dNLGNBQWMsQ0FBZEEsQ0FBYyxDQUFkQSxDQUFaLEdBQVlBLENBQVo7O0FBRUEsa0JBQUloTSxHQUFHLEtBQUhBLE1BQWNBLEdBQUcsS0FBakJBLGFBQW1DQSxHQUFHLElBQTFDLE1BQW9EO0FBQ2hEK0wsaUNBQWlCLENBQWpCQTtBQUp3QyxnQkFPNUM7OztBQUNBLGtCQUFJQyxjQUFjLENBQWxCLFFBQWtCLENBQWxCLEVBQThCO0FBQzFCLHFCQUFJLENBQUo7QUFDSDtBQUVKOztBQUVEO0FBMU5GOztBQUFBLG9EQWlPeUIsdUJBQTZCO0FBQ3BELGlCQUFJLENBQUosZ0JBQXFCeEQsaUVBQXNCLEtBQUksQ0FBMUJBLGFBQXJCLFdBQXFCQSxDQUFyQjtBQWxPRjs7QUFBQSxpREF5T3NCLHVCQUE2QjtBQUVqRCxnQkFBSSxLQUFJLENBQVIsZUFBd0I7QUFDcEIsa0JBQUkyQyxXQUFXLElBQUlBLFdBQVcsQ0FBWEEsZUFBbkIsV0FBbUJBLENBQW5CLEVBQTREO0FBQ3hELHFCQUFJLENBQUosYUFBa0JBLFdBQVcsQ0FBN0I7QUFESixxQkFFTztBQUNILHFCQUFJLENBQUosYUFBa0IsS0FBSSxDQUFKLHlCQUFsQjtBQUNIO0FBTEwsbUJBTU87QUFDSCxrQkFBSUEsV0FBVyxJQUFJQSxXQUFXLENBQVhBLGVBQW5CLFdBQW1CQSxDQUFuQixFQUE0RDtBQUN4RCxxQkFBSSxDQUFKLGFBQWtCQSxXQUFXLENBQTdCO0FBREoscUJBRU87QUFDSCxxQkFBSSxDQUFKLGFBQWtCLEtBQUksQ0FBSix5QkFBbEI7QUFDSDtBQWI0QyxjQWdCakQ7OztBQUNBLGdCQUFJLENBQUMsS0FBSSxDQUFKLDBCQUErQixLQUFJLENBQXBDLGVBQUMsQ0FBRCxJQUF5RCxLQUFJLENBQUosa0JBQTdELE1BQTBGO0FBQ3RGLG1CQUFJLENBQUosV0FBZ0IsS0FBSSxDQUFwQjtBQUNIOztBQUVELGdCQUFJLEtBQUksQ0FBSixpQkFBc0IsQ0FBQyxLQUFJLENBQS9CLGVBQStDO0FBQzNDLGtCQUFNYyxXQUFXLEdBQUcsS0FBSSxDQUFKLDBCQUFwQjs7QUFDQSxtQkFBSSxDQUFKLFdBQWdCLEtBQUksQ0FBcEIsc0JBQTJDO0FBQUVDLG1CQUFHLEVBQUVDLFFBQVEsQ0FBQ0M7QUFBaEIsZUFBM0M7O0FBQ0EsbUJBQUksQ0FBSixXQUFnQixLQUFJLENBQXBCLHNCQUEyQztBQUFFRixtQkFBRyxFQUFFRDtBQUFQLGVBQTNDO0FBQ0g7QUFsUUg7O0FBQUEseURBMFE4QixvQkFBc0M7QUFDbEUsZ0JBQU1ULGlCQUFpQixHQUF2Qjs7QUFDQSxnQkFBTWEsWUFBWSxHQUFHLEtBQUksQ0FBekIsT0FBcUIsRUFBckI7O0FBRUEsaUJBQUssSUFBTCwwQkFBcUM7QUFDakM7QUFDQSxtQkFBSyxJQUFJclUsQ0FBQyxHQUFWLEdBQWdCQSxDQUFDLEdBQUdxVSxZQUFZLENBQVpBLFFBQVksQ0FBWkEsQ0FBcEIsUUFBbURyVSxDQUFuRCxJQUF3RDtBQUNwRCxvQkFBTWtVLEdBQUcsR0FBR0csWUFBWSxDQUFaQSxRQUFZLENBQVpBLElBRHdDLEdBQ3BELENBRG9ELENBRXBEOztBQUNBLG9CQUFJQyxRQUFRLENBQVJBLE1BQUosR0FBSUEsQ0FBSixFQUF5QjtBQUNyQmQsbUNBQWlCLENBQWpCQTtBQUNIO0FBQ0o7QUFDSjs7QUFFRCxnQkFBSUEsaUJBQWlCLENBQWpCQSxXQUFKLEdBQW9DO0FBQUU7QUFDbENBLCtCQUFpQixDQUFqQkEsS0FBdUIsS0FBSSxDQUFKLDhCQUF2QkE7QUFDSDs7QUFDRDtBQTVSRjs7QUFDRSxxRkFERixXQUNFLEVBREYsQ0FFRTs7QUFDQSxjQUFJLEtBQUosZ0JBQXlCO0FBQ3JCLHFDQUF5QixJQUFJQyxnREFBSixtQkFBSUEsQ0FBSixDQUFzQixLQUF0QixPQUFzQixFQUF0QixFQUFzQyxLQUF0QyxzQkFBaUUsS0FBakUsYUFBbUYsS0FBbkYsYUFBcUcsS0FBckcsNkJBQXVJLEtBQXZJLGFBQXlKLEtBQWxMLFdBQXlCLENBQXpCO0FBQ0g7O0FBQ0Q7QUFDSDtBQUVEOzs7O0FBSUE7Ozs7Ozs7QUFyQ0c7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMUDs7Ozs7O0FBSU8sWUFBTUEsaUJBQWI7QUFLSTs7Ozs7Ozs7O0FBU0EsdUxBUUU7QUFBQTs7QUFBQTs7QUFBQSxpQkFQVWMsVUFPVixHQVBVQSxVQU9WO0FBQUEsaUJBTlVDLHVCQU1WLEdBTlVBLHVCQU1WO0FBQUEsaUJBTFVyQixXQUtWLEdBTFVBLFdBS1Y7QUFBQSxpQkFKVUMsV0FJVixHQUpVQSxXQUlWO0FBQUEsaUJBSFVDLDJCQUdWLEdBSFVBLDJCQUdWO0FBQUEsaUJBRlVvQix5QkFFVixHQUZVQSx5QkFFVjtBQUFBLGlCQURVQyx5QkFDVixHQURVQSx5QkFDVjs7QUFBQTs7QUFBQTs7QUFBQTs7QUFBQSw0Q0FjYyw2RkFJTDtBQUNQLG1CQUFJLENBQUo7QUFuQkY7O0FBQUEsa0RBaUNvQiwrQ0FJUjtBQUNWLGtCQUFJQyxZQUFZLEdBQWhCOztBQUVBLGtCQUFJM0QsSUFBSSxLQUFKQSxNQUFlLEtBQUksQ0FBSiwwQkFBbkIsTUFBbUIsQ0FBbkIsRUFBMkQ7QUFDdkQscUJBQ0ksSUFBSTRELHVCQUF1QixHQUQvQixHQUVJQSx1QkFBdUIsR0FBRyxLQUFJLENBQUosbUJBRjlCLFFBR0lBLHVCQUhKLElBSUU7QUFDRSxzQkFBTUMsY0FBYyxHQUFHLFdBQ25CLEtBQUksQ0FBSiw0Q0FESixLQUNJLENBRG1CLENBQXZCOztBQUdBLHNCQUFJQSxjQUFjLENBQWRBLEtBQUosTUFBSUEsQ0FBSixFQUFpQztBQUM3Qix3QkFBSUMsYUFBa0IsR0FBdEI7QUFFQUEsaUNBQWEsR0FBR0EsYUFBYSxDQUFiQSxPQUNaLEtBQUksQ0FBSiw0Q0FESkEsT0FDSSxDQURZQSxDQUFoQkE7O0FBS0Esd0JBQU1DLFFBQVEsR0FBR3ZFLGtFQUFqQixhQUFpQkEsQ0FBakI7O0FBQ0Esd0JBQUl1RSxRQUFRLENBQVJBLEtBQUosSUFBSUEsQ0FBSixFQUF5QjtBQUNyQkosa0NBQVksR0FBWkE7QUFDQTtBQUZKLDJCQUdPO0FBQ0hBLGtDQUFZLEdBQVpBO0FBQ0g7QUFDSjtBQUNKOztBQUVELHFCQUNJLElBQUlLLHVCQUF1QixHQUQvQixHQUVJQSx1QkFBdUIsR0FBRyxLQUFJLENBQUosbUJBRjlCLFFBR0lBLHVCQUhKLElBSUU7QUFDRSxzQkFBTUMsV0FBVyxHQUFHLFdBQ2hCLEtBQUksQ0FBSiw0Q0FESixLQUNJLENBRGdCLENBQXBCOztBQUlBLHNCQUFJQSxXQUFXLENBQVhBLEtBQUosTUFBSUEsQ0FBSixFQUE4QjtBQUMxQix3QkFBSUMsYUFBa0IsR0FBdEI7QUFFQUEsaUNBQWEsR0FBR0EsYUFBYSxDQUFiQSxPQUNaLEtBQUksQ0FBSiw0Q0FESkEsT0FDSSxDQURZQSxDQUFoQkE7O0FBTUEsd0JBQU1DLFFBQVEsR0FBRzNFLGtFQUFqQixhQUFpQkEsQ0FBakI7O0FBRUEsd0JBQUkyRSxRQUFRLENBQVJBLEtBQUosSUFBSUEsQ0FBSixFQUF5QjtBQUNyQlIsa0NBQVksR0FBWkE7QUFDQTtBQUZKLDJCQUdPO0FBQ0hBLGtDQUFZLEdBQVpBO0FBQ0g7QUFDSjtBQUNKO0FBQ0o7O0FBQ0Q7QUFoR0Y7O0FBQUEseURBMkc0Qix5R0FLcEI7QUFDTixrQkFBSVMsZUFBZSxHQURiLEVBQ04sQ0FETSxDQUNvQjs7QUFDMUIsa0JBQUlDLGtCQUFrQixHQUF0QjtBQUNBLGtCQUFJQyxrQkFBa0IsR0FBdEI7O0FBQ0Esa0JBQU1DLGdCQUFnQixHQUFHLFNBQW5CQSxnQkFBbUIsT0FBb0I7QUFDekMsdUJBQU9DLElBQUksQ0FBWCxLQUFXLENBQVg7QUFESjs7QUFHQSxrQkFBTUMscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixPQUFvQjtBQUM5Qyx1QkFBTztBQUFFdkIscUJBQUcsRUFBRXNCLElBQUksQ0FBWCxLQUFXLENBQVg7QUFBb0JFLHVCQUFLLEVBQUVGLElBQUk7QUFBL0IsaUJBQVA7QUFESjs7QUFHQSxrQkFBTUcsc0JBQXNCLEdBQUcsU0FBekJBLHNCQUF5QixhQUFrQztBQUM3RFAsK0JBQWUsQ0FBZkEsNEJBRUksS0FBSSxDQUFKLG1DQUZKQSxnQkFFSSxDQUZKQTtBQUlBRSxrQ0FBa0IsQ0FBbEJBLCtCQUVJLEtBQUksQ0FBSiwwREFGSkEsWUFFSSxDQUZKQTtBQVFBRCxrQ0FBa0IsQ0FBbEJBLCtCQUVJLEtBQUksQ0FBSiwwREFGSkEsWUFFSSxDQUZKQTtBQWJKOztBQXVCQSxtQkFBSyxJQUFMLHdCQUFtQztBQUMvQixvQkFBTTFCLFdBQVcsR0FBR04sMkJBQTJCLENBQS9DLFFBQStDLENBQS9DOztBQUVBLG9CQUFJLEtBQUksQ0FBSixrQ0FBSixXQUFJLENBQUosRUFBeUQ7QUFDckRzQyx3Q0FBc0IsQ0FBQ3BCLFVBQVUsQ0FBakNvQixRQUFpQyxDQUFYLENBQXRCQTtBQUNIOztBQUVELG9CQUNJbEIseUJBQXlCLElBQ3pCQSx5QkFBeUIsQ0FBekJBLFNBRkosR0FHRTtBQUNFa0Isd0NBQXNCLENBQXRCQSx5QkFBc0IsQ0FBdEJBOztBQUNBLHNCQUFJUCxlQUFlLENBQWZBLFNBQUosR0FBZ0M7QUFDNUJBLG1DQUFlLEdBQUcsS0FBSSxDQUFKLHlFQUFsQkEsSUFBa0IsQ0FBbEJBO0FBS0FDLHNDQUFrQixHQUFHLEtBQUksQ0FBSixrRkFBckJBLEtBQXFCLENBQXJCQTtBQU1IO0FBQ0o7O0FBRUQsb0JBQ0lYLHlCQUF5QixJQUN6QkEseUJBQXlCLENBQXpCQSxTQUZKLEdBR0U7QUFDRWlCLHdDQUFzQixDQUF0QkEseUJBQXNCLENBQXRCQTs7QUFDQSxzQkFBSVAsZUFBZSxDQUFmQSxTQUFKLEdBQWdDO0FBQzVCQSxtQ0FBZSxHQUFHLEtBQUksQ0FBSix5RUFBbEJBLEtBQWtCLENBQWxCQTtBQUtBRSxzQ0FBa0IsR0FBRyxLQUFJLENBQUosbUZBQXJCQSxLQUFxQixDQUFyQkE7QUFNSDtBQUNKO0FBQ0o7O0FBRUQscUJBQU87QUFDSE0scUJBQUssRUFBRXBGLGtFQURKLGVBQ0lBLENBREo7QUFFSHFGLDZCQUFhLEVBRlY7QUFHSEMsNkJBQWEsRUFBRVI7QUFIWixlQUFQO0FBak1GOztBQUFBLGtFQWdOcUMsOEVBS3RCO0FBQ2Isa0JBQ0k5RSxpRUFBc0IsS0FBSSxDQUExQkEsYUFBd0MsS0FBSSxDQUE1Q0EsaUJBREosb0JBR0U7QUFDRSxvQkFBTXVGLHNCQUFzQixHQUFHLEtBQUksQ0FBSix3QkFBL0IsZ0JBQStCLENBQS9COztBQUdBQSxzQ0FBc0IsQ0FBdEJBLFFBQStCLDJCQUFxQjtBQUNoREMsNkJBQVcsR0FBRyxXQUFXLENBQVgsT0FBbUIsZ0JBQXdCO0FBQ3JELHdCQUFJQyxlQUFlLElBQW5CLE1BQTZCO0FBQ3pCVCwwQkFBSSxHQUFHQSxJQUFJLENBQVhBLGVBQVcsQ0FBWEE7QUFDSDs7QUFDRCwyQkFBT0EsSUFBSSxLQUFLVSxlQUFlLENBQS9CLEtBQStCLENBQS9CO0FBSkpGLG1CQUFjLENBQWRBO0FBREpEO0FBUUg7O0FBQ0Q7QUF0T0Y7O0FBQUEsNkRBOE9nQyw0QkFFakI7QUFDYixxQkFBTyxnQkFBZ0IsQ0FBaEIsT0FBd0IsZ0JBQXdCO0FBQ25ELHVCQUFPUCxJQUFJLENBQUpBLFdBQVA7QUFESixlQUFPLENBQVA7QUFqUEY7O0FBQUEsZ0RBNFBtQiw2RkFJVjtBQUNQLG1CQUFJLENBQUosOEJBRE8sMkJBQ1AsQ0FETyxDQUVQOztBQUNBLGtCQUFNVyxjQUFjLEdBQUcsS0FBSSxDQUFKLG9CQUNuQixLQUFJLENBRGUsWUFFbkIsS0FBSSxDQUZlLHdEQUF2Qix5QkFBdUIsQ0FBdkI7O0FBT0EsbUJBQUksQ0FBSixxQkFBMEJBLGNBQWMsQ0FBeEM7QUFDQSxtQkFBSSxDQUFKLHFCQUEwQkEsY0FBYyxDQUF4QztBQUNBLG1CQUFJLENBQUosdUJBQTRCQSxjQUFjLENBQTFDO0FBNVFGOztBQUNFO0FBS0g7QUFFRDs7Ozs7Ozs7QUE5Qko7QUFBQTs7QUFxU0k7Ozs7Ozs7O0FBclNKLDZGQWlUa0I7QUFDVixrQkFBSUMsY0FBYyxHQUFHLDhCQUF3QjtBQUN6Qyx1QkFBT1osSUFBSSxDQUFKQSxRQUFJLENBQUpBLEtBQVA7QUFESjs7QUFHQSxrQkFBSWEsVUFBVSxJQUFkLE1BQXdCO0FBQ3BCRCw4QkFBYyxHQUFHLDhCQUF3QjtBQUNyQyx5QkFBT1osSUFBSSxDQUFKQSxRQUFJLENBQUpBLEtBQVA7QUFESlk7QUFHSDs7QUFDRCxxQkFBT0UsT0FBTyxDQUFQQSwyQkFBUCxtQkFBT0EsQ0FBUDtBQUNIO0FBM1RMOztBQUFBO0FBQUEsV0FBTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0pQOzs7OztBQUdPLFlBQU1DLFlBQWI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFBQTs7QUErQ0k7Ozs7OztBQS9DSixtR0FxRDRIO0FBQ3BILGtCQUFNQyxPQUFpQixHQUFHLHFDQUNGLHVDQURFLGNBRUQsQ0FBQyxtQ0FBRCw2QkFDRyw2RUFINUIsT0FHNEIsQ0FIRixDQUExQjs7QUFJQSxrQkFBRyxDQUFILFNBQVk7QUFDUjtBQUNIOztBQUVEO0FBQ0g7QUFFRDs7Ozs7QUFqRUo7QUFBQTtBQUFBLGtFQXFFMkU7QUFDbkUscUJBQU8sNkJBQVAsV0FBTyxDQUFQO0FBQ0g7QUFHRDs7Ozs7O0FBMUVKO0FBQUE7QUFBQSwwRUErRTRGO0FBQ3BGLGtCQUFHOUYsT0FBTyxJQUFJLE9BQU9BLE9BQU8sQ0FBZCxjQUFYQSxjQUNJLHVDQURQLFdBQ3dEO0FBQ3BELG9CQUFNK0YsV0FBb0IsR0FBRyxtQ0FBN0I7QUFDQS9GLHVCQUFPLENBQVBBO0FBQ0g7QUFDSjtBQXJGTDs7QUFBQTtBQUFBLFdBQU87O3dCQUFNNkYsWSxzQkFDa0M7QUFDdkMsb0JBQVU7QUFDTkUsdUJBQVcsRUFBRTtBQURQLFdBRDZCO0FBSXZDLG1CQUFTO0FBQ0xBLHVCQUFXLEVBQUU7QUFEUixXQUo4QjtBQU92QywrQkFBcUI7QUFDakJDLHFDQUF5QixFQUFFLG1EQUF5QjtBQUNoRCxxQkFBTyxDQUFDQyxjQUFjLENBQXRCO0FBRmE7QUFJakJGLHVCQUFXLEVBQUU7QUFKSSxXQVBrQjtBQWF2QyxtQkFBUztBQUNMQyxxQ0FBeUIsRUFBRSw0REFBa0M7QUFDekQscUJBQ0toRyxPQUFPLENBQVBBLHFCQUFELFlBQUNBLEtBQ1g7QUFDQ0EscUJBQU8sQ0FBUEEscUJBQTZCaUcsY0FBYyxDQUh0QztBQUZDO0FBUUxGLHVCQUFXLEVBQUU7QUFSUixXQWI4QjtBQXVCdkMsb0JBQVU7QUFDTkEsdUJBQVcsRUFBRTtBQURQLFdBdkI2QjtBQTBCdkMsNEJBQWtCO0FBQ2RDLHFDQUF5QixFQUFFLG1EQUF5QjtBQUNoRCxxQkFBTyxDQUFDQyxjQUFjLENBQXRCO0FBRlU7QUFJZEYsdUJBQVcsRUFBRTtBQUpDLFdBMUJxQjtBQWdDdkMsK0JBQXFCO0FBQ2pCQyxxQ0FBeUIsRUFBRSxtREFBeUI7QUFDaEQscUJBQU8sQ0FBQ0MsY0FBYyxDQUF0QjtBQUZhO0FBSWpCRix1QkFBVyxFQUFFO0FBSkksV0FoQ2tCO0FBc0N2QyxrQkFBUTtBQUNKQSx1QkFBVyxFQUFFO0FBRFQ7QUF0QytCLFM7O3dCQURsQ0YsWSxnQkE0QzRCLFdBQVczUyxNQUFNLENBQU5BLEtBQVkyUyxZQUFZLENBQXhCM1MsdUJBQVgsR0FBV0EsQ0FBWCxNOzt3QkE1QzVCMlMsWSx3QkE2Q3NDLHFFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQzlDOUNLLGM7QUFLTDs7OzttQkFMS0EsYztBQUFBQSx3QixTQUFBQSxHLE9BQUFBO0FBQUFBLHdCLFdBQUFBLEcsU0FBQUE7V0FBQUEsYyxLQUFBQSxjOztBQVFFLFlBQU1DLFVBQWI7QUFDSTtBQUNBOztBQU1BOzs7OztBQUtBLHdEQUdFO0FBQUE7O0FBQUE7O0FBQUE7O0FBQUE7O0FBQUEsMkRBVndDLFlBVXhDOztBQUNFO0FBQ0EsK0JBRkYsV0FFRSxDQUZGLENBR0U7O0FBQ0Esc0NBQTBCLEtBQTFCLCtCQUEwQixFQUExQjtBQUNIO0FBRUQ7Ozs7OztBQXZCSjtBQUFBO0FBQUEsbURBMkI2QztBQUNyQztBQUNBLHdDQUEwQixLQUExQiwrQkFBMEIsRUFBMUI7QUFDSDtBQUVEOzs7Ozs7O0FBaENKO0FBQUE7QUFBQSw4RUFzQ2tHO0FBRTFGLGtCQUFJQyxjQUFjLEdBQWxCOztBQUNBLGtCQUFHdEcsMkZBQWdELGlCQUFoREEsZ0JBQWlGLEtBQWpGQSxhQUFILFlBQUdBLENBQUgsRUFBb0g7QUFDaEgsb0JBQUt1RyxnQkFBZ0IsS0FBakIsUUFBQ0EsSUFBa0MsZ0NBQXZDLFFBQXVDLENBQXZDLEVBQWtGO0FBQzlFRCxnQ0FBYyxHQUFkQTtBQUNIOztBQUVELG9CQUFJLENBQUosZ0JBQXFCO0FBQ2pCcEcseUJBQU8sQ0FBUEE7QUFDSDtBQUNKOztBQUNEO0FBQ0g7QUFFRDs7Ozs7O0FBckRKO0FBQUE7QUFBQSxxRUEwRGlGO0FBQ3pFLGtCQUFNc0csUUFBUSxHQUFHLElBQUloRiwwQ0FBSixLQUFJQSxDQUFKLENBQVF0QixPQUFPLENBQWYsdUJBQWpCO0FBRUEsa0JBQUl1RyxhQUFhLEdBQWpCOztBQUVBLGtCQUFJLGdDQUFKLFFBQUksQ0FBSixFQUErQztBQUMzQyxvQkFBSSw0QkFBNEJMLGNBQWMsQ0FBMUMsV0FBc0QsQ0FBQ2xHLE9BQU8sQ0FBUEEsVUFBdkQsV0FBdURBLENBQXZELElBQXlGLENBQUNBLE9BQU8sQ0FBUEEsVUFBMUYsV0FBMEZBLENBQTFGLElBQTRIQSxPQUFPLENBQVBBLFVBQWhJLEtBQWdJQSxDQUFoSSxFQUEwSjtBQUN0SnVHLCtCQUFhLEdBQWJBO0FBQ0g7QUFDSjs7QUFFRDtBQUNIO0FBRUQ7Ozs7O0FBeEVKO0FBQUE7QUFBQSx1REE0RXNEO0FBQzlDbEYsdUJBQVMsR0FBR0EsU0FBUyxDQUFUQSx1QkFBWkEsVUFBWUEsQ0FBWkE7QUFDQUEsdUJBQVMsR0FBR0EsU0FBUyxDQUFUQSxxQkFBWkEsUUFBWUEsQ0FBWkE7QUFDRDtBQUNGO0FBRUQ7Ozs7QUFsRko7QUFBQTtBQUFBLDhEQXFGOEQ7QUFDdEQsa0JBQUksQ0FBQyxpQkFBRCxjQUFnQyxDQUFDLEtBQUQsZUFBcUIsQ0FBQyxpQkFBMUQsa0JBQUksQ0FBSixFQUFnRztBQUM1Rix1QkFBTzZFLGNBQWMsQ0FBckI7QUFESixxQkFFTztBQUNILHVCQUFPcEcsMkZBQWdELGlCQUFoREEsV0FBNEUsS0FBNUVBLGFBQVAsb0JBQU9BLENBQVA7QUFDSDtBQUNKO0FBM0ZMOztBQUFBO0FBQUEsV0FBTzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNaUDtBQUFBOzs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7O0FBQUE7QUFBQTtBQUFBO0FBQUE7OztBQUFBO0FBQUE7QUFBQTtBQUFBOzs7O0FBR08sNkJBQW9CO0FBRXZCLGNBQUkwRyxRQUFRLEdBQUd0VCxNQUFNLENBQU5BLE9BQWYsSUFBZUEsQ0FBZjtBQUNBc1Qsa0JBQVEsQ0FBUkEsS0FBUSxDQUFSQTtBQUNBQSxrQkFBUSxDQUFSQSxNQUFRLENBQVJBO0FBQ0FBLGtCQUFRLENBQVJBLFFBQVEsQ0FBUkE7QUFDQUEsa0JBQVEsQ0FBUkEsTUFBUSxDQUFSQTtBQUNBQSxrQkFBUSxDQUFSQSxPQUFRLENBQVJBO0FBQ0FBLGtCQUFRLENBQVJBLElBQVEsQ0FBUkE7QUFDQUEsa0JBQVEsQ0FBUkEsS0FBUSxDQUFSQTtBQUVBLGNBQUlDLHNCQUFzQixHQUFHdlQsTUFBTSxDQUFOQSxPQUE3QixJQUE2QkEsQ0FBN0I7QUFDQXVULGdDQUFzQixDQUF0QkEsS0FBc0IsQ0FBdEJBO0FBQ0FBLGdDQUFzQixDQUF0QkEsTUFBc0IsQ0FBdEJBO0FBQ0FBLGdDQUFzQixDQUF0QkEsTUFBc0IsQ0FBdEJBO0FBQ0FBLGdDQUFzQixDQUF0QkEsUUFBc0IsQ0FBdEJBOztBQUVBLDRDQUFrQztBQUM5QixtQkFBT0QsUUFBUSxDQUFSQSxNQUFRLENBQVJBLEtBQVA7QUFDSDs7QUFFRCw2QkFBbUI7QUFDZkUsaUJBQUssQ0FBTEE7QUFDQTtBQUNIOztBQUVELGtDQUF3QjtBQUNwQixnQkFBSSxPQUFKLEdBQWM7QUFDVkMscUJBQU8sQ0FBUEE7QUFGZ0IsY0FJcEI7OztBQUNBLG1CQUFPQyxDQUFDLENBQVIsV0FBT0EsRUFBUDtBQUNIOztBQUVELG9DQUEwQjtBQUN0QixnQkFBSUMsT0FBTyxHQUFHckksQ0FBQyxDQUFEQSxXQUFkLENBQWNBLENBQWQ7O0FBQ0EsZ0JBQUlxSSxPQUFPLEdBQVBBLFFBQ0pBLE9BQU8sR0FESEEsUUFFSjtBQUNBLHNFQUEwRCxDQUgxRCxHQUlFO0FBQ0U7QUFDSDs7QUFDRCxtQkFBT0Msa0JBQWtCLENBQXpCLENBQXlCLENBQXpCO0FBQ0g7O0FBRUQseUNBQStCO0FBQzNCO0FBQ0E7QUFFQSxnQkFBSUQsT0FBTyxHQUFHckksQ0FBQyxDQUFEQSxXQUFkLENBQWNBLENBQWQ7O0FBQ0EsZ0JBQUlxSSxPQUFPLEdBQVBBLFFBQ0pBLE9BQU8sR0FESEEsUUFFSjtBQUNBLGdFQUFvRCxDQUhwRCxHQUlFO0FBQ0U7QUFDSDs7QUFDRCxtQkFBT0Msa0JBQWtCLENBQXpCLENBQXlCLENBQXpCO0FBQ0g7O0FBRUQsY0FBSUMsR0FBRyxHQUFQO0FBQUEsY0FDSUMsS0FBSyxHQURUO0FBQUEsY0FFSUMsWUFBWSxHQUZoQjtBQUlBOzs7Ozs7QUFLQSxxREFBMkM7QUFDdkMsa0NBQXNCO0FBQ2xCQyxvQkFBTSxDQUFOQTtBQUNIOztBQUNELGdCQUFJQyxLQUFLLEdBQUdDLGFBQWEsSUFBekI7QUFBQSxnQkFDSUMsTUFBTSxHQURWO0FBQUEsZ0JBRUk5UyxNQUFNLEdBRlY7QUFBQSxnQkFHSStTLE1BQU0sR0FIVjtBQUFBLGdCQUlJQyxXQUFXLEdBSmY7QUFBQSxnQkFLSUwsTUFBTSxHQUxWOztBQU9BTSxnQkFBSSxFQUFFLE9BQU8sQ0FBQ0MsS0FBSyxDQUFDSixNQUFNLEdBQVpJLENBQUssQ0FBTEEsWUFBNkJKLE1BQU0sS0FBcEMsTUFBK0MsQ0FBQyxLQUF2RCxZQUF3RTtBQUMxRSxrQkFBSTdJLENBQUMsR0FBR2lKLEtBQUssQ0FBYixNQUFhLENBQWI7O0FBQ0E7QUFDQTtBQUNJLHNCQUFJakosQ0FBQyxJQUFJd0ksS0FBSyxDQUFMQSxLQUFULENBQVNBLENBQVQsRUFBd0I7QUFDcEJ6UywwQkFBTSxJQUFJaUssQ0FBQyxDQURTLFdBQ1ZBLEVBQVZqSyxDQURvQixDQUNPOztBQUMzQjRTLHlCQUFLLEdBQUxBO0FBRkoseUJBR08sSUFBSSxDQUFKLGVBQW9CO0FBQ3ZCNVMsMEJBQU0sR0FBTkE7QUFDQTRTLHlCQUFLLEdBQUxBO0FBQ0E7QUFIRyx5QkFJQTtBQUNITyx1QkFBRyxDQUFIQSxpQkFBRyxDQUFIQTtBQUNBO0FBQ0g7O0FBQ0Q7O0FBRUo7QUFDSSxzQkFBSWxKLENBQUMsSUFBSXlJLFlBQVksQ0FBWkEsS0FBVCxDQUFTQSxDQUFULEVBQStCO0FBQzNCMVMsMEJBQU0sSUFBSWlLLENBQUMsQ0FEZ0IsV0FDakJBLEVBQVZqSyxDQUQyQixDQUNBO0FBRC9CLHlCQUdPLElBQUksUUFBSixHQUFlO0FBQ2xCO0FBQ0FBLDBCQUFNLEdBQU5BOztBQUNBLHVDQUFtQjtBQUNmO0FBQ0g7O0FBQ0Qsd0JBQUlvVCxnQkFBZ0IsQ0FBQyxLQUFyQixPQUFvQixDQUFwQixFQUFvQztBQUNoQztBQUNIOztBQUNELHdCQUFJLFdBQVcsS0FBZixTQUE2QjtBQUN6QlIsMkJBQUssR0FBTEE7QUFESiwyQkFFTyxJQUFJLDRCQUE0QlMsSUFBSSxDQUFKQSxZQUFpQixLQUFqRCxTQUErRDtBQUNsRVQsMkJBQUssR0FBTEE7QUFERywyQkFFQSxJQUFJLEtBQUosYUFBc0I7QUFDekJBLDJCQUFLLEdBQUxBO0FBREcsMkJBRUE7QUFDSEEsMkJBQUssR0FBTEE7QUFDSDtBQWpCRSx5QkFrQkEsSUFBSSxDQUFKLGVBQW9CO0FBQ3ZCNVMsMEJBQU0sR0FBTkE7QUFDQThTLDBCQUFNLEdBQU5BO0FBQ0FGLHlCQUFLLEdBQUxBO0FBQ0E7QUFKRyx5QkFLQSxJQUFJSixHQUFHLEtBQVAsR0FBZTtBQUNsQjtBQURHLHlCQUVBO0FBQ0hXLHVCQUFHLENBQUMsdUNBQUpBLENBQUcsQ0FBSEE7QUFDQTtBQUNIOztBQUNEOztBQUVKO0FBQ0ksc0JBQUksUUFBSixHQUFlO0FBQ1g7QUFDQVAseUJBQUssR0FBTEE7QUFGSix5QkFHTyxJQUFJLFFBQUosR0FBZTtBQUNsQjtBQUNBQSx5QkFBSyxHQUFMQTtBQUZHLHlCQUdBO0FBQ1A7QUFDSSx3QkFBSUosR0FBRyxLQUFIQSxLQUFhLFNBQWJBLEtBQTJCLFNBQTNCQSxLQUF5QyxTQUE3QyxHQUF5RDtBQUNyRCwwQ0FBb0JjLGFBQWEsQ0FBakMsQ0FBaUMsQ0FBakM7QUFDSDtBQUNKOztBQUNEOztBQUVKO0FBQ0ksc0JBQUksU0FBUyxDQUFFRixnQkFBZ0IsQ0FBQ0MsSUFBSSxDQUFwQyxPQUErQixDQUEvQixFQUFnRDtBQUM1Q0YsdUJBQUcsQ0FBSEEsaUJBQUcsQ0FBSEE7QUFDQWYsMkJBQU8sQ0FBUEE7QUFGSix5QkFHTztBQUNIUSx5QkFBSyxHQUFMQTtBQUNBO0FBQ0g7O0FBQ0Q7O0FBRUo7QUFDSSxzQkFBSSxhQUFhLFFBQVFNLEtBQUssQ0FBQ0osTUFBTSxHQUFyQyxDQUE4QixDQUE5QixFQUEwQztBQUN0Q0YseUJBQUssR0FBTEE7QUFESix5QkFFTztBQUNITyx1QkFBRyxDQUFDLHNCQUFKQSxDQUFHLENBQUhBO0FBQ0FQLHlCQUFLLEdBQUxBO0FBQ0E7QUFDSDs7QUFDRDs7QUFFSjtBQUNJO0FBQ0Esc0JBQUksV0FBVyxLQUFmLFNBQ0ksZUFBZVMsSUFBSSxDQUFuQjs7QUFDSixzQkFBSWIsR0FBRyxLQUFQLEdBQWU7QUFDWCxpQ0FBYWEsSUFBSSxDQUFqQjtBQUNBLGlDQUFhQSxJQUFJLENBQWpCO0FBQ0EsaUNBQWFBLElBQUksQ0FBSkEsTUFBYixLQUFhQSxFQUFiO0FBQ0Esa0NBQWNBLElBQUksQ0FBbEI7QUFDQSxxQ0FBaUJBLElBQUksQ0FBckI7QUFDQSxxQ0FBaUJBLElBQUksQ0FBckI7QUFDQTtBQVBKLHlCQVFPLElBQUksYUFBYSxTQUFqQixHQUE2QjtBQUNoQyx3QkFBSSxTQUFKLEdBQ0lGLEdBQUcsQ0FBSEEsOEJBQUcsQ0FBSEE7QUFDSlAseUJBQUssR0FBTEE7QUFIRyx5QkFJQSxJQUFJLFFBQUosR0FBZTtBQUNsQixpQ0FBYVMsSUFBSSxDQUFqQjtBQUNBLGlDQUFhQSxJQUFJLENBQWpCO0FBQ0EsaUNBQWFBLElBQUksQ0FBSkEsTUFBYixLQUFhQSxFQUFiO0FBQ0E7QUFDQSxxQ0FBaUJBLElBQUksQ0FBckI7QUFDQSxxQ0FBaUJBLElBQUksQ0FBckI7QUFDQVQseUJBQUssR0FBTEE7QUFQRyx5QkFRQSxJQUFJLFFBQUosR0FBZTtBQUNsQixpQ0FBYVMsSUFBSSxDQUFqQjtBQUNBLGlDQUFhQSxJQUFJLENBQWpCO0FBQ0EsaUNBQWFBLElBQUksQ0FBSkEsTUFBYixLQUFhQSxFQUFiO0FBQ0Esa0NBQWNBLElBQUksQ0FBbEI7QUFDQTtBQUNBLHFDQUFpQkEsSUFBSSxDQUFyQjtBQUNBLHFDQUFpQkEsSUFBSSxDQUFyQjtBQUNBVCx5QkFBSyxHQUFMQTtBQVJHLHlCQVNBO0FBQ0gsd0JBQUlXLEtBQUssR0FBR0wsS0FBSyxDQUFDSixNQUFNLEdBQXhCLENBQWlCLENBQWpCO0FBQ0Esd0JBQUlVLFNBQVMsR0FBR04sS0FBSyxDQUFDSixNQUFNLEdBQTVCLENBQXFCLENBQXJCOztBQUNBLHdCQUNJLFdBQVcsS0FBWCxXQUEyQixDQUFDTCxLQUFLLENBQUxBLEtBQTVCLENBQTRCQSxDQUE1QixJQUNQYyxLQUFLLEtBQUxBLE9BQWlCQSxLQUFLLEtBRGYsT0FFUGYsR0FBRyxLQUFIQSxhQUFxQixRQUFyQkEsYUFBMEMsU0FBMUNBLGFBQWdFLFFBQWhFQSxhQUFxRixRQUhsRixXQUdzRztBQUNsRyxtQ0FBYWEsSUFBSSxDQUFqQjtBQUNBLG1DQUFhQSxJQUFJLENBQWpCO0FBQ0EsdUNBQWlCQSxJQUFJLENBQXJCO0FBQ0EsdUNBQWlCQSxJQUFJLENBQXJCO0FBQ0EsbUNBQWFBLElBQUksQ0FBSkEsTUFBYixLQUFhQSxFQUFiOztBQUNBO0FBQ0g7O0FBQ0RULHlCQUFLLEdBQUxBO0FBQ0E7QUFDSDs7QUFDRDs7QUFFSjtBQUNJLHNCQUFJLGFBQWEsU0FBakIsR0FBNkI7QUFDekIsd0JBQUksU0FBSixHQUFnQjtBQUNaTyx5QkFBRyxDQUFIQSw4QkFBRyxDQUFIQTtBQUNIOztBQUNELHdCQUFJLFdBQVcsS0FBZixTQUE2QjtBQUN6QlAsMkJBQUssR0FBTEE7QUFESiwyQkFFTztBQUNIQSwyQkFBSyxHQUFMQTtBQUNIO0FBUkwseUJBU087QUFDSCx3QkFBSSxXQUFXLEtBQWYsU0FBNkI7QUFDekIsbUNBQWFTLElBQUksQ0FBakI7QUFDQSxtQ0FBYUEsSUFBSSxDQUFqQjtBQUNBLHVDQUFpQkEsSUFBSSxDQUFyQjtBQUNBLHVDQUFpQkEsSUFBSSxDQUFyQjtBQUNIOztBQUNEVCx5QkFBSyxHQUFMQTtBQUNBO0FBQ0g7O0FBQ0Q7O0FBRUo7QUFDSSxzQkFBSSxRQUFKLEdBQWU7QUFDWEEseUJBQUssR0FBTEE7QUFESix5QkFFTztBQUNITyx1QkFBRyxDQUFDLHdCQUFKQSxDQUFHLENBQUhBO0FBQ0FQLHlCQUFLLEdBQUxBO0FBQ0E7QUFDSDs7QUFDRDs7QUFFSjtBQUNJQSx1QkFBSyxHQUFMQTs7QUFDQSxzQkFBSSxRQUFKLEdBQWU7QUFDWE8sdUJBQUcsQ0FBQyx3QkFBSkEsQ0FBRyxDQUFIQTtBQUNBO0FBQ0g7O0FBQ0Q7O0FBRUo7QUFDSSxzQkFBSSxhQUFhLFNBQWpCLEdBQTZCO0FBQ3pCUCx5QkFBSyxHQUFMQTtBQUNBO0FBRkoseUJBR087QUFDSE8sdUJBQUcsQ0FBQyw4QkFBSkEsQ0FBRyxDQUFIQTtBQUNIOztBQUNEOztBQUVKO0FBQ0ksc0JBQUksUUFBSixHQUFlO0FBQ1gsZ0NBQVk7QUFDUkEseUJBQUcsQ0FBSEEsaUJBQUcsQ0FBSEE7QUFDQW5ULDRCQUFNLElBQU5BO0FBQ0g7O0FBQ0QrUywwQkFBTSxHQUFOQTs7QUFDQSx5QkFBSyxJQUFJaFksQ0FBQyxHQUFWLEdBQWdCQSxDQUFDLEdBQUdpRixNQUFNLENBQTFCLFFBQW1DakYsQ0FBbkMsSUFBd0M7QUFDcEMsMEJBQUkwWSxFQUFFLEdBQUd6VCxNQUFNLENBQWYsQ0FBZSxDQUFmOztBQUNBLDBCQUFJLGVBQWUsU0FBZixNQUE4QixTQUFsQyxJQUErQztBQUMzQ21ULDJCQUFHLENBQUhBLGtDQUFHLENBQUhBO0FBQ0E7QUFKZ0Msd0JBTXBDOzs7QUFDQSwwQkFBSSxjQUFjLFNBQVMsS0FBM0IsV0FBMkM7QUFDdkM7QUFDQTtBQUNIOztBQUNELDBCQUFJTyxLQUFLLEdBQUdKLGFBQWEsQ0FBekIsRUFBeUIsQ0FBekI7QUFDQywrQkFBUyxLQUFWLFNBQUMsR0FBMkIsa0JBQTVCLEtBQUMsR0FBcUQsa0JBQXRELEtBQUM7QUFDSjs7QUFDRHRULDBCQUFNLEdBQU5BO0FBcEJKLHlCQXFCTyxJQUFJd1MsR0FBRyxLQUFIQSxLQUFhLFFBQWJBLEtBQTBCLFNBQTFCQSxLQUF3QyxRQUF4Q0EsS0FBcUQsUUFBekQsR0FBb0U7QUFDdkVNLDBCQUFNLElBQUk5UyxNQUFNLENBQWhCOFM7QUFDQTlTLDBCQUFNLEdBQU5BO0FBQ0E0Uyx5QkFBSyxHQUFMQTtBQUNBO0FBSkcseUJBS0E7QUFDSDVTLDBCQUFNLElBQU5BO0FBQ0g7O0FBQ0Q7O0FBRUo7QUFDSSxzQkFBSXdTLEdBQUcsS0FBSEEsS0FBYSxRQUFiQSxLQUEwQixTQUExQkEsS0FBd0MsUUFBeENBLEtBQXFELFFBQXpELEdBQW9FO0FBQ2hFLHdCQUFJeFMsTUFBTSxDQUFOQSxnQkFBdUJ5UyxLQUFLLENBQUxBLEtBQVd6UyxNQUFNLENBQXhDQSxDQUF3QyxDQUFqQnlTLENBQXZCelMsS0FBaURBLE1BQU0sQ0FBTkEsQ0FBTSxDQUFOQSxZQUFxQkEsTUFBTSxDQUFOQSxDQUFNLENBQU5BLEtBQTFFLEdBQUlBLENBQUosRUFBOEY7QUFDMUY0UywyQkFBSyxHQUFMQTtBQURKLDJCQUVPLElBQUk1UyxNQUFNLENBQU5BLFdBQUosR0FBeUI7QUFDNUI0UywyQkFBSyxHQUFMQTtBQURHLDJCQUVBO0FBQ0gsbUNBQWFlLFdBQVcsQ0FBWEEsV0FBYixNQUFhQSxDQUFiO0FBQ0EzVCw0QkFBTSxHQUFOQTtBQUNBNFMsMkJBQUssR0FBTEE7QUFDSDs7QUFDRDtBQVZKLHlCQVdPLElBQUksY0FBYyxTQUFkLEtBQTRCLFNBQWhDLEdBQTRDO0FBQy9DTyx1QkFBRyxDQUFIQSxrQ0FBRyxDQUFIQTtBQURHLHlCQUVBO0FBQ0huVCwwQkFBTSxJQUFOQTtBQUNIOztBQUNEOztBQUVKO0FBQ0E7QUFDSSxzQkFBSSxhQUFhLENBQWpCLGFBQStCO0FBQy9CO0FBQ0ksaUNBQWEyVCxXQUFXLENBQVhBLFdBQWIsTUFBYUEsQ0FBYjtBQUNBM1QsMEJBQU0sR0FBTkE7QUFDQTRTLHlCQUFLLEdBQUxBOztBQUNBLHdCQUFJLGVBQUosZUFBa0M7QUFDOUI7QUFDSDtBQVBMLHlCQVFPLElBQUlKLEdBQUcsS0FBSEEsS0FBYSxRQUFiQSxLQUEwQixTQUExQkEsS0FBd0MsUUFBeENBLEtBQXFELFFBQXpELEdBQW9FO0FBQ3ZFLGlDQUFhbUIsV0FBVyxDQUFYQSxXQUFiLE1BQWFBLENBQWI7O0FBQ0Esd0JBQUcsZUFBSCxJQUFxQjtBQUNqQiw0QkFBTXRZLEtBQUssQ0FBWCxxQkFBVyxDQUFYO0FBQ0g7O0FBQ0QyRSwwQkFBTSxHQUFOQTtBQUNBNFMseUJBQUssR0FBTEE7O0FBQ0EsdUNBQW1CO0FBQ2Y7QUFDSDs7QUFDRDtBQVZHLHlCQVdBLElBQUksY0FBYyxTQUFkLEtBQTRCLFNBQWhDLEdBQTRDO0FBQy9DLHdCQUFJLFFBQUosR0FBZTtBQUNYSSxpQ0FBVyxHQUFYQTtBQURKLDJCQUVPLElBQUksUUFBSixHQUFlO0FBQ2xCQSxpQ0FBVyxHQUFYQTtBQUNIOztBQUNEaFQsMEJBQU0sSUFBTkE7QUFORyx5QkFPQTtBQUNIbVQsdUJBQUcsQ0FBQywwQ0FBSkEsQ0FBRyxDQUFIQTtBQUNIOztBQUNEOztBQUVKO0FBQ0ksc0JBQUksYUFBSixDQUFJLENBQUosRUFBcUI7QUFDakJuVCwwQkFBTSxJQUFOQTtBQURKLHlCQUVPLElBQUl3UyxHQUFHLEtBQUhBLEtBQWEsUUFBYkEsS0FBMEIsU0FBMUJBLEtBQXdDLFFBQXhDQSxLQUFxRCxRQUFyREEsS0FBSixlQUFxRjtBQUN4Rix3QkFBSSxPQUFKLFFBQW1CO0FBQ2YsMEJBQUlvQixJQUFJLEdBQUd4UCxRQUFRLFNBQW5CLEVBQW1CLENBQW5COztBQUNBLDBCQUFJd1AsSUFBSSxLQUFLM0IsUUFBUSxDQUFDLEtBQXRCLE9BQXFCLENBQXJCLEVBQXFDO0FBQ2pDLHFDQUFhMkIsSUFBSSxHQUFqQjtBQUNIOztBQUNENVQsNEJBQU0sR0FBTkE7QUFDSDs7QUFDRCx1Q0FBbUI7QUFDZjtBQUNIOztBQUNENFMseUJBQUssR0FBTEE7QUFDQTtBQVpHLHlCQWFBLElBQUksY0FBYyxTQUFkLEtBQTRCLFNBQWhDLEdBQTRDO0FBQy9DTyx1QkFBRyxDQUFDLGlDQUFKQSxDQUFHLENBQUhBO0FBREcseUJBRUE7QUFDSGYsMkJBQU8sQ0FBUEE7QUFDSDs7QUFDRDs7QUFFSjtBQUNJLHNCQUFJLFNBQUosR0FDSWUsR0FBRyxDQUFIQSwyQkFBRyxDQUFIQTtBQUNKUCx1QkFBSyxHQUFMQTs7QUFDQSxzQkFBSSxhQUFhLFNBQWpCLEdBQTZCO0FBQ3pCO0FBQ0g7O0FBQ0Q7O0FBRUo7QUFDSSxzQkFBSUosR0FBRyxLQUFIQSxLQUFhLFFBQWJBLEtBQTBCLFNBQTFCQSxLQUF5QyxtQkFBbUIsYUFBYSxRQUE3RSxDQUE2QyxDQUE3QyxFQUEwRjtBQUN0Rix3QkFBSSxTQUFKLEdBQWdCO0FBQ1pXLHlCQUFHLENBQUhBLGtDQUFHLENBQUhBO0FBQ0g7O0FBQ0Q7O0FBQ0Esd0JBQUl4WCxHQUFHLEdBQUd1VyxzQkFBc0IsQ0FBQ2xTLE1BQU0sQ0FBdkMsV0FBaUNBLEVBQUQsQ0FBaEMsRUFBd0Q7QUFBRztBQUN2REEsNEJBQU0sR0FBTkE7QUFDSDs7QUFDRCx3QkFBSSxTQUFKLFFBQXFCO0FBQ2pCOztBQUNBLDBCQUFJLGFBQWEsU0FBakIsR0FBNkI7QUFDekI7QUFDSDtBQUpMLDJCQUtPLElBQUksa0JBQWtCLFFBQWxCLEtBQStCLFNBQW5DLEdBQStDO0FBQ2xEO0FBREcsMkJBRUEsSUFBSSxRQUFKLFFBQW9CO0FBQ3ZCLDBCQUFJLFdBQVcsS0FBWCxXQUEyQixzQkFBM0IsS0FBc0RBLE1BQU0sQ0FBTkEsV0FBdEQsS0FBNkV5UyxLQUFLLENBQUxBLEtBQVd6UyxNQUFNLENBQTlGLENBQThGLENBQWpCeVMsQ0FBN0UsSUFBc0d6UyxNQUFNLENBQU5BLENBQU0sQ0FBTkEsS0FBMUcsS0FBNkg7QUFDekhBLDhCQUFNLEdBQUdBLE1BQU0sQ0FBTkEsQ0FBTSxDQUFOQSxHQUFUQTtBQUNIOztBQUNEO0FBQ0g7O0FBQ0RBLDBCQUFNLEdBQU5BOztBQUNBLHdCQUFJLFFBQUosR0FBZTtBQUNYO0FBQ0E0UywyQkFBSyxHQUFMQTtBQUZKLDJCQUdPLElBQUksUUFBSixHQUFlO0FBQ2xCO0FBQ0FBLDJCQUFLLEdBQUxBO0FBQ0g7QUE1QkwseUJBNkJPLElBQUksY0FBYyxTQUFkLEtBQTRCLFNBQWhDLEdBQTRDO0FBQy9DNVMsMEJBQU0sSUFBSXNULGFBQWEsQ0FBdkJ0VCxDQUF1QixDQUF2QkE7QUFDSDs7QUFDRDs7QUFFSjtBQUNJLHNCQUFJLGtCQUFrQixRQUF0QixHQUFpQztBQUM3QjtBQUNBNFMseUJBQUssR0FBTEE7QUFGSix5QkFHTyxJQUFJSixHQUFHLEtBQUhBLEtBQWEsU0FBYkEsS0FBMkIsU0FBM0JBLEtBQXlDLFNBQTdDLEdBQXlEO0FBQzVELG1DQUFlcUIsa0JBQWtCLENBQWpDLENBQWlDLENBQWpDO0FBQ0g7O0FBQ0Q7O0FBRUo7QUFDSSxzQkFBSXJCLEdBQUcsS0FBSEEsS0FBYSxTQUFiQSxLQUEyQixTQUEzQkEsS0FBeUMsU0FBN0MsR0FBeUQ7QUFDckQ7QUFDSDs7QUFDRDtBQTlWSjs7QUFpV0FNLG9CQUFNO0FBQ1Q7QUFDSjs7QUFFRCwyQkFBaUI7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBbmNtQixZQXNjdkI7QUFDQTs7QUFDQTs7Ozs7Ozs7QUFNQTtBQUF5QjtBQUFrQjtBQUN2QyxnQkFBSU8sSUFBSSxLQUFKQSxhQUFzQixFQUFFQSxJQUFJLFlBQWhDLElBQTBCLENBQTFCLEVBQ0lBLElBQUksR0FBRyxTQUFTelMsTUFBTSxDQUF0QnlTLElBQXNCLENBQWYsQ0FBUEE7QUFFSix3QkFBWSxLQUFaO0FBQ0FsQixpQkFBSyxDQUFMQTs7QUFFQSxnQkFBSWUsS0FBSyxHQUFHLGtEQVAyQixFQU8zQixDQUFaLENBUHVDLENBUXZDOzs7QUFFQVksaUJBQUssQ0FBTEE7QUFDSDs7QUFFREMsY0FBSSxDQUFKQSxZQUFpQjtBQUNiNVosb0JBQVEsRUFBRSxvQkFBVztBQUNqQixxQkFBTyxLQUFQO0FBRlM7O0FBSWIsdUJBQVc7QUFDUCxrQkFBSSxLQUFKLFlBQ0ksT0FBTyxLQUFQO0FBRUosa0JBQUk2WixTQUFTLEdBQWI7O0FBQ0Esa0JBQUksT0FBTyxLQUFQLGFBQXlCLFNBQVMsS0FBdEMsV0FBc0Q7QUFDbERBLHlCQUFTLEdBQUcsa0JBQ1gsU0FBUyxLQUFULFlBQTBCLE1BQU0sS0FBaEMsWUFEVyxNQUFaQTtBQUVIOztBQUVELHFCQUFPLGlCQUNOLG1CQUFtQixtQkFBbUIsS0FBdEMsT0FETSxNQUVQLEtBRk8sV0FFUyxLQUZULFNBRXVCLEtBRjlCO0FBZFM7O0FBa0JiLDJCQUFlO0FBQ1g3QixtQkFBSyxDQUFMQTtBQUNBMkIsbUJBQUssQ0FBTEE7QUFwQlM7O0FBdUJiLDJCQUFlO0FBQ1gscUJBQU8sZUFBUDtBQXhCUzs7QUEwQmIsbUNBQXVCO0FBQ25CLGtCQUFJLEtBQUosWUFDSTtBQUNKQSxtQkFBSyxDQUFMQSxXQUFpQi9CLFFBQVEsR0FBekIrQjtBQTdCUzs7QUFnQ2IsdUJBQVc7QUFDUCxxQkFBTyx1QkFBdUIsYUFDMUIsbUJBQW1CLEtBRE8sUUFDTSxLQURwQztBQWpDUzs7QUFvQ2IsMkJBQWU7QUFDWCxrQkFBSSxtQkFBbUIsQ0FBQyxLQUF4QixhQUNJO0FBQ0pBLG1CQUFLLENBQUxBO0FBdkNTOztBQTBDYiwyQkFBZTtBQUNYLHFCQUFPLEtBQVA7QUEzQ1M7O0FBNkNiLG1DQUF1QjtBQUNuQixrQkFBSSxtQkFBbUIsQ0FBQyxLQUF4QixhQUNJO0FBQ0pBLG1CQUFLLENBQUxBO0FBaERTOztBQW1EYix1QkFBVztBQUNQLHFCQUFPLEtBQVA7QUFwRFM7O0FBc0RiLDJCQUFlO0FBQ1gsa0JBQUksbUJBQW1CLENBQUMsS0FBeEIsYUFDSTtBQUNKQSxtQkFBSyxDQUFMQTtBQXpEUzs7QUE0RGIsMkJBQWU7QUFDWCxxQkFBTyx1QkFBdUIsbUJBQzFCLE1BQU0sZ0JBRG9CLEdBQ3BCLENBRG9CLEdBQ0csS0FEakM7QUE3RFM7O0FBZ0ViLG1DQUF1QjtBQUNuQixrQkFBSSxtQkFBbUIsQ0FBQyxLQUF4QixhQUNJO0FBQ0o7QUFDQUEsbUJBQUssQ0FBTEE7QUFwRVM7O0FBdUViLHlCQUFhO0FBQ1QscUJBQU8sbUJBQW1CLENBQUMsS0FBcEIsVUFBbUMsUUFBUSxLQUEzQyxjQUNFLEtBRFQ7QUF4RVM7O0FBMkViLCtCQUFtQjtBQUNmLGtCQUFJLG1CQUFtQixDQUFDLEtBQXhCLGFBQ0k7QUFDSjtBQUNBLGtCQUFJLFFBQVFHLE1BQU0sQ0FBbEIsQ0FBa0IsQ0FBbEIsRUFDSUEsTUFBTSxHQUFHQSxNQUFNLENBQU5BLE1BQVRBLENBQVNBLENBQVRBO0FBQ0pILG1CQUFLLENBQUxBO0FBakZTOztBQW9GYix1QkFBVztBQUNQLHFCQUFPLG1CQUFtQixDQUFDLEtBQXBCLGFBQXNDLFFBQVEsS0FBOUMsaUJBQ0UsS0FEVDtBQXJGUzs7QUF3RmIsMkJBQWU7QUFDWCxrQkFBSSxLQUFKLFlBQ0k7O0FBQ0osa0JBQUcsQ0FBSCxNQUFVO0FBQ047QUFDQTtBQUNIOztBQUNEO0FBQ0Esa0JBQUksUUFBUUksSUFBSSxDQUFoQixDQUFnQixDQUFoQixFQUNJQSxJQUFJLEdBQUdBLElBQUksQ0FBSkEsTUFBUEEsQ0FBT0EsQ0FBUEE7QUFDSkosbUJBQUssQ0FBTEE7QUFsR1M7O0FBcUdiLHlCQUFhO0FBQ1Q7O0FBQ0Esa0JBQUksbUJBQW1CLENBQUMsS0FBeEIsU0FBc0M7QUFDbEM7QUFISyxnQkFLVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxzQkFBUSxLQUFSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDSTtBQUxKOztBQU9BSyxrQkFBSSxHQUFHLEtBQVBBOztBQUNBLGtCQUFJLENBQUosTUFBVztBQUNQO0FBQ0g7O0FBQ0QscUJBQU8sdUJBQVA7QUFDSDs7QUEzSFksV0FBakJKLENBM2R1QixDQXlsQnZCOztBQUNBLGNBQUlLLFdBQVcsR0FBR0MsSUFBSSxDQUF0Qjs7QUFDQSwyQkFBaUI7QUFDYk4sZ0JBQUksQ0FBSkEsaUJBQUksQ0FBSkEsR0FBMEIsZ0JBQWU7QUFDekM7QUFDQTtBQUNJLHFCQUFPSyxXQUFXLENBQVhBLG1DQUFQLFNBQU9BLENBQVA7QUFISkw7O0FBS0FBLGdCQUFJLENBQUpBLGlCQUFJLENBQUpBLEdBQTBCLGVBQWM7QUFDcENLLHlCQUFXLENBQVhBO0FBREpMO0FBR0g7O0FBRUQ7QUFDSDs7QUFFTSxZQUFNTyxHQUFHLEdBQUdDLFNBQVo7QUFFUDs7Ozs7QUFJTyxnQ0FBd0I7QUFBRTtBQUM3QixjQUFJQyxhQUFhLEdBQWpCOztBQUVBLGNBQUk7QUFDQSxnQkFBSUMsQ0FBQyxHQUFHLElBQUlKLElBQUksQ0FBUixTQUFSLFVBQVEsQ0FBUjtBQUNBSSxhQUFDLENBQURBO0FBQ0FELHlCQUFhLEdBQUdDLENBQUMsQ0FBREEsU0FBaEJEO0FBSEosWUFJRSxVQUFTO0FBQ1BFLG1CQUFPLENBQVBBO0FBQ0g7O0FBRUQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcm5CSjs7Ozs7QUFHTyxZQUFNQyxZQUFiO0FBTUk7Ozs7Ozs7Ozs7QUFVQSxnRkFRRTtBQUFBOztBQUFBLGdCQUpVdkcsMkJBSVYsdUVBSm9ELEVBSXBEO0FBQUEsZ0JBSFVKLFdBR1YsdUVBSG9DLEVBR3BDO0FBQUEsZ0JBRlVDLFdBRVYsdUVBRm9DLEVBRXBDO0FBQUEsZ0JBRFUyRyxlQUNWOztBQUFBOztBQUFBLGlCQVBZekcsV0FPWixHQVBZQSxXQU9aO0FBQUEsaUJBTllELFdBTVosR0FOWUEsV0FNWjtBQUFBLGlCQUxVMkcsb0JBS1YsR0FMVUEsb0JBS1Y7QUFBQSxpQkFKVXpHLDJCQUlWLEdBSlVBLDJCQUlWO0FBQUEsaUJBSFVKLFdBR1YsR0FIVUEsV0FHVjtBQUFBLGlCQUZVQyxXQUVWLEdBRlVBLFdBRVY7QUFBQSxpQkFEVTJHLGVBQ1YsR0FEVUEsZUFDVjs7QUFBQTs7QUFBQTs7QUFBQTs7QUFBQTs7QUFBQSx1REF5QnlCLGlGQUFtSDtBQUMxSSxtQkFBSSxDQUFKOztBQUNBLG1CQUFJLENBQUo7O0FBQ0EsbUJBQUksQ0FBSjs7QUFDQSxtQkFBSSxDQUFKO0FBN0JGOztBQUFBLHNEQXNDd0IsaUVBQThGO0FBQ3BILG1CQUFJLENBQUo7QUF2Q0Y7O0FBQ0UsOEJBQWtCLElBQUloRCx5Q0FBSixZQUFJQSxDQUFKLGNBQWxCLFdBQWtCLENBQWxCO0FBQ0EseUNBQTZCLElBQUl4RyxvREFBSix1QkFBSUEsQ0FBSixDQUN6QitDLFdBQVcsQ0FBWEEsZUFEeUIsc0JBRXpCQSxXQUFXLENBRmMsNkJBQTdCLGVBQTZCLENBQTdCO0FBTUEsZ0NBQW9CbUQsMkNBQXBCLGNBQW9CQSxDQUFwQjtBQUNBLGlDQUFxQixJQUFJdkQsNENBQUosZUFBSUEsQ0FBSiwyQkFHakIsS0FIaUIsNkJBSWpCLEtBSmlCLGFBS2pCLEtBTEosV0FBcUIsQ0FBckI7QUFPSDtBQUVEOzs7Ozs7Ozs7QUExQ0o7QUFBQTs7QUFrRUk7Ozs7Ozs7O0FBbEVKLDRGQTBFOEg7QUFBQTs7QUFDdEgsa0JBQU0rRyxJQUFJLEdBQVY7QUFDQSxxQkFBTyx5RkFBeUYscUJBQWU7QUFDM0csc0JBQUksQ0FBSjtBQURKLGVBQU8sQ0FBUDtBQUdIO0FBRUQ7Ozs7Ozs7Ozs7QUFqRko7QUFBQTtBQUFBLGdIQTBGcUo7QUFBQTs7QUFDN0ksa0JBQU1BLElBQUksR0FBVjtBQUNBLDhHQUFnRyxxQkFBZTtBQUMzRyxzQkFBSSxDQUFKO0FBREo7QUFHSDtBQUVEOzs7OztBQWpHSjtBQUFBO0FBQUEsMkNBcUd1RDtBQUMvQyxrQkFBSUMsT0FBTyxHQURvQyxhQUMvQyxDQUQrQyxDQUNsQjs7QUFDN0IscUJBQU8sMkJBQTZCdkosR0FBRyxLQUFoQyxNQUEyQ0EsR0FBRyxDQUFIQSxxQkFBeUIsQ0FBM0U7QUFDSDtBQUVEOzs7Ozs7QUExR0o7QUFBQTtBQUFBLHNEQStHOEQ7QUFDdEQ7QUFDQSxrQkFBSTtBQUNBLG9CQUFJdUIsMENBQUosS0FBSUEsQ0FBSjtBQURKLGdCQUVFLFVBQVU7QUFDUjtBQUNIOztBQUNEO0FBQ0g7QUFFRDs7Ozs7O0FBekhKO0FBQUE7QUFBQSxrRUE4SDJFO0FBQUE7O0FBQ25FLHFCQUFPLFlBQU07QUFDVCxzQkFBSSxDQUFKO0FBREo7QUFHSDtBQUVEOzs7Ozs7Ozs7Ozs7QUFwSUo7QUFBQTtBQUFBLG9JQStJOEg7QUFDdEgsa0JBQUlpSSxZQUFZLEdBQWhCOztBQUNBLGtCQUFJLGdDQUFnQyxDQUFDLDhCQUFyQyxRQUFxQyxDQUFyQyxFQUE4RTtBQUMxRTtBQUNIOztBQUNELGtCQUFNQyxTQUFTLEdBQUcsQ0FBQyxJQUFuQixJQUFtQixFQUFuQjtBQUNBLGtCQUFNeEosT0FBTyxHQUFHLElBQUlvQyx5Q0FBSixZQUFJQSxDQUFKLENBQWhCLEVBQWdCLENBQWhCO0FBQ0FwQyxxQkFBTyxDQUFQQTtBQUNBQSxxQkFBTyxDQUFQQTtBQUNBQSxxQkFBTyxDQUFQQTtBQUNBQSxxQkFBTyxDQUFQQSxjQUFzQixJQUFJc0IsMENBQUosS0FBSUEsQ0FBSix5QkFBdEJ0Qjs7QUFFQSxrQkFBSSxDQUFDLDBDQUFMLFFBQUssQ0FBTCxFQUEwRDtBQUN0RCxvQkFBTXlKLFdBQVcsR0FBRyx5QkFBeUJ6SixPQUFPLENBQWhDLE1BQXBCLE9BQW9CLENBQXBCO0FBQ0Esb0JBQU0wSixtQkFBbUIsR0FBRyxvQ0FBNUIsV0FBNEIsQ0FBNUI7O0FBQ0Esb0JBQUk1SiwyRkFBZ0QsaUJBQWhEQSxnQkFBaUYsS0FBakZBLGFBQUosaUJBQUlBLENBQUosRUFBMkg7QUFDdkgsc0JBQUkseUNBQXlDNkosZUFBZSxLQUE1RCxXQUE0RTtBQUN4RUEsbUNBQWUsY0FBZkEsbUJBQWUsQ0FBZkE7QUFDSDs7QUFDREosOEJBQVksR0FBWkE7QUFDSDs7QUFDRDtBQVRKLHFCQVdLO0FBQ0RLLDJCQUFXLENBQUM1SixPQUFPLENBQW5CNEosRUFBVyxDQUFYQTtBQUNIOztBQUNEO0FBQ0g7QUFFRDs7Ozs7OztBQTVLSjtBQUFBO0FBQUEscUVBa0x1RjtBQUMvRSxrQkFBSUMsTUFBTSxHQUFWOztBQUVBLGtCQUFJLHlDQUFKLE9BQUksQ0FBSixFQUF1RDtBQUNuREEsc0JBQU0sR0FBTkE7QUFDSDs7QUFFRCxrQkFBSSwyQ0FBSixPQUFJLENBQUosRUFBeUQ7QUFDckRBLHNCQUFNLEdBQU5BO0FBQ0g7O0FBRUQsa0JBQUksK0NBQUosUUFBSSxDQUFKLEVBQThEO0FBQzFEQSxzQkFBTSxHQUFOQTtBQUNIOztBQUVELCtEQUFpRDdKLE9BQU8sQ0FBeEQ7QUFDSDtBQUVEOzs7Ozs7OztBQXBNSjtBQUFBO0FBQUEsMkVBMk1tRztBQUMzRjtBQUNBO0FBQ0EscUJBQU8sd0NBQVAsUUFBTyxDQUFQO0FBQ0g7QUFFRDs7Ozs7OztBQWpOSjtBQUFBO0FBQUEseUVBdU4yRjtBQUNuRixrQkFBSThKLGtCQUFrQixHQUF0QjtBQUNBLGtCQUFNL0osR0FBRyxHQUFHQyxPQUFPLENBQW5CO0FBQUEsa0JBQ0krSixVQUFVLEdBQUcsSUFBSXpJLDBDQUFKLEtBQUlBLENBQUosTUFEakIsUUFDaUIsQ0FEakI7QUFBQSxrQkFFSStFLGdCQUFnQixHQUFHLElBQUkvRSwwQ0FBSixLQUFJQSxDQUFKLFdBRnZCO0FBQUEsa0JBR0lzQyxRQUFRLEdBQUdtRyxVQUFVLENBSHpCO0FBQUEsa0JBSUlDLFFBQVEsR0FBR0QsVUFBVSxDQUp6QjtBQUFBLGtCQUtJekQsUUFBUSxHQUFHeUQsVUFBVSxDQVAwRCxRQUVuRixDQUZtRixDQVFuRjtBQUNBOztBQUNBLGtCQUFNRSxlQUFlLEdBQUcsc0VBQXNFLGlCQUE5RixjQUF3QixDQUF4Qjs7QUFDQSxtQ0FBcUI7QUFDakI7QUFESixxQkFFTyxJQUFJLDJDQUFKLFFBQUksQ0FBSixFQUEwRDtBQUM3RDtBQUNBO0FBQ0g7O0FBQ0Qsa0JBQUk3RCxjQUFjLEdBQWxCOztBQUNBLGtCQUFJckcsR0FBRyxLQUFQLElBQWdCO0FBQ1pxRyw4QkFBYyxHQUFHLHlEQUFqQkEsT0FBaUIsQ0FBakJBO0FBbkIrRSxnQkFxQm5GOzs7QUFDQSxrQkFBSXZELHFCQUFxQixHQXRCMEQsSUFzQm5GLENBdEJtRixDQXVCbkY7O0FBQ0Esa0JBQUk1Qyx5QkFBeUIsR0FBN0I7QUFFQUEsdUNBQXlCLEdBQUcsMkRBQTVCQSxXQUE0QixDQUE1QkE7O0FBQ0Esa0JBQUksS0FBSixhQUFzQjtBQUNsQjRDLHFDQUFxQixHQUFHLG1EQUF4QkEsUUFBd0IsQ0FBeEJBO0FBQ0g7O0FBRUQsa0JBQUlBLHFCQUFxQixLQUFyQkEsU0FDQXVELGNBQWMsS0FEZHZELFNBRUE1Qyx5QkFBeUIsS0FGN0IsT0FFeUM7QUFFckM2SixrQ0FBa0IsR0FBbEJBO0FBQ0g7O0FBQ0Q7QUFDSDtBQUVEOzs7Ozs7QUEvUEo7QUFBQTtBQUFBLGtFQW9ROEU7QUFDdEUsa0JBQUlJLGdCQUFnQixHQUFHbEssT0FBTyxDQUE5Qjs7QUFDQSxrQkFBSW1LLG9CQUFvQixHQUFHLFNBQXZCQSxvQkFBdUIsR0FBWTtBQUNuQyxvQkFBSUMsT0FBTyxLQUFQQSxTQUFxQkEsT0FBTyxLQUFoQyxTQUE4QztBQUMxQztBQURKLHVCQUdLO0FBQ0Q7QUFDSDtBQU5MOztBQVNBLGtCQUFJLHNDQUFKLE9BQUksQ0FBSixFQUFvRDtBQUNoREYsZ0NBQWdCLEdBQUcsZ0NBQWdDbEssT0FBTyxDQUExRGtLLFdBQW1CLENBQW5CQTs7QUFDQSxvQkFBSWxLLE9BQU8sQ0FBUEEsbUJBQUosR0FBa0M7QUFDOUI7QUFDSDtBQUpMLHFCQUtPLElBQUlBLE9BQU8sQ0FBUEEsVUFBSixLQUFJQSxDQUFKLEVBQThCO0FBQ2pDLHVCQUFPbUssb0JBQVA7QUFDSDs7QUFFRCxrQkFBSSx5Q0FBSixPQUFJLENBQUosRUFBdUQ7QUFDbkQsdUJBQU8sd0NBQVAsZ0JBQU8sQ0FBUDtBQURKLHFCQUdLLElBQUksMkNBQUosT0FBSSxDQUFKLEVBQXlEO0FBQzFELHVCQUFPLDBDQUFQLGdCQUFPLENBQVA7QUFEQyxxQkFHQTtBQUNELHVCQUFPQSxvQkFBUDtBQUNIO0FBQ0o7QUFFRDs7Ozs7O0FBblNKO0FBQUE7QUFBQSx3RUF3U2lGO0FBQ3pFLGtCQUFJLDBDQUFKLE9BQUksQ0FBSixFQUF3RDtBQUNwRCx1QkFBTyx5Q0FBUCxXQUFPLENBQVA7QUFESixxQkFHSyxJQUFJLDRDQUFKLE9BQUksQ0FBSixFQUEwRDtBQUMzRCx1QkFBTywyQ0FBUCxXQUFPLENBQVA7QUFEQyxxQkFHQTtBQUNEO0FBQ0g7QUFDSjtBQWxUTDs7QUFBQTtBQUFBLFdBQU87O0FBbVROOzs7Ozs7Ozs7Ozs7Ozs7O0FDOVREO0FBQUE7OztBQUFBO0FBQUE7QUFBQTtBQUFBOzs7QUFBQTtBQUFBO0FBQUE7O0FBRU8sWUFBTXJLLFNBQVMsR0FBSSxZQUFZO0FBRWxDLGlCQUFPO0FBQ0g7Ozs7OztBQU1BO0FBQ0F1Syx3QkFBWSxFQUFFLDJCQUEwQztBQUFBLGtCQUEzQm5ILEtBQTJCLHVFQUFuQnhSLFNBQW1COztBQUNwRCxrQkFBSXRDLEtBQUssQ0FBTEEsUUFBSixHQUFJQSxDQUFKLEVBQXdCO0FBQ3BCLG9CQUFJZSxHQUFHLElBQUlBLEdBQUcsQ0FBSEEsU0FBUEEsS0FBeUJBLEdBQUcsQ0FBaEMsTUFBdUM7QUFFbkMseUJBQU8sV0FBV0EsR0FBRyxDQUFIQSxLQUFYLEdBQVdBLENBQVgsRUFBUCxLQUFPLENBQVA7QUFDSDs7QUFFRCx1QkFBTyxpQkFBUCxLQUFPLENBQVA7QUFDSDtBQWhCRjtBQW9CSG1hLGdCQUFJLEVBQUUsaUNBQW9DO0FBQ3RDLGtCQUFJaFYsSUFBSSxLQUFKQSxhQUFzQixFQUFFLFlBQTVCLElBQTBCLENBQTFCLEVBQStDO0FBQzNDQSxvQkFBSSxHQUFHO0FBQ0g5Rix3QkFBTSxFQUFFO0FBREwsaUJBQVA4RjtBQUdIOztBQUNELG1CQUFLLElBQUloRyxDQUFDLEdBQVYsR0FBZ0JBLENBQUMsR0FBR2dHLElBQUksQ0FBeEIsUUFBaUNoRyxDQUFqQyxJQUFzQztBQUNsQ2liLGtCQUFFLENBQUZBLGNBQWlCalYsSUFBSSxDQUFyQmlWLENBQXFCLENBQXJCQTtBQUNIO0FBNUJGO0FBK0JIQyx3QkFBWSxFQUFFLDJCQUFnQztBQUMxQyxrQkFBSSxlQUFKLFVBQTZCO0FBQ3pCLHVCQUFPQyxHQUFHLEtBQUtDLGtCQUFrQixDQUFqQyxHQUFpQyxDQUFqQztBQUNIOztBQUNEO0FBbkNEO0FBc0NIQyxzQkFBVSxFQUFFLDRCQUEwQjtBQUNsQyxrQkFBSTtBQUNBLG9CQUFJckosMENBQUosS0FBSUEsQ0FBSjtBQUNBO0FBRkosZ0JBR0UsVUFBVTtBQUNSO0FBQ0g7QUE1Q0Y7QUErQ0hzSix1QkFBVyxFQUFFLCtDQUE2QztBQUN0RCxxQkFBTywyQ0FBMkNsSSxXQUFXLENBQXRELDZCQUFQLGVBQU8sQ0FBUDtBQWhERDtBQW1ESG1JLDhCQUFrQixFQUFFLGlDQUFjO0FBQzlCLGtCQUFJQyxVQUFVLEdBQWQ7O0FBQ0Esa0JBQUk7QUFDQSxvQkFBSSxlQUFKLFVBQTZCO0FBQ3pCLHlCQUFPLGtCQUFQLFVBQU8sQ0FBUCxFQUFzQztBQUNsQ0EsOEJBQVUsR0FBR0osa0JBQWtCLENBQS9CSSxVQUErQixDQUEvQkE7QUFDSDtBQUNKO0FBTEwsZ0JBTUUsVUFBUztBQUNQN0IsdUJBQU8sQ0FBUEEsS0FBYSxnQkFBYkE7QUFDSDs7QUFDRDtBQTlERDtBQWlFSDhCLDhCQUFrQixFQUFFLGlDQUFtQjtBQUNuQyxrQkFBSUMsVUFBVSxHQUFkOztBQUNBLGtCQUFJO0FBQ0Esb0JBQUksZUFBSixVQUE2QjtBQUN6QkEsNEJBQVUsR0FBR2xFLGtCQUFrQixDQUEvQmtFLEdBQStCLENBQS9CQTtBQUNIO0FBSEwsZ0JBSUUsVUFBUztBQUNQL0IsdUJBQU8sQ0FBUEEsS0FBYSxnQkFBYkE7QUFDSDs7QUFDRDtBQTFFRDtBQTZFSGdDLHdCQUFZLEVBQUUsNEJBQXVCO0FBQ2pDLGtCQUFJblcsQ0FBQyxHQUFMO0FBQUEsa0JBQVN4RixDQUFDLEdBQVY7O0FBRUEscUJBQUtBLENBQUMsR0FBTjtBQUNJd0YsaUJBQUMsQ0FBQ3hGLENBQUZ3RixFQUFDLENBQURBO0FBREo7O0FBR0EscUJBQU9BLENBQUMsQ0FBREEsS0FBUCxFQUFPQSxDQUFQO0FBbkZEO0FBc0ZIb1csaURBQXFDLEVBQUUsc0ZBQXFEO0FBQ3hGLGtCQUFJekksV0FBVyxJQUFJQSxXQUFXLENBQVhBLGVBQW5CLFFBQW1CQSxDQUFuQixFQUF5RDtBQUFFO0FBQ3ZELHVCQUFPQSxXQUFXLENBQWxCLFFBQWtCLENBQWxCO0FBREoscUJBRU87QUFDSCx1QkFBT3dELGNBQWMsQ0FBckIsUUFBcUIsQ0FBckI7QUFDSDtBQUNKO0FBNUZFLFdBQVA7QUFGRyxTQUFtQixFQUFuQjs7Ozs7OztBakJRUDtDQVZBLEU7Ozs7Ozs7Ozs7Ozs7O0FrQkFBLElBQUlrRixDQUFKLEMsQ0FFQTs7QUFDQUEsQ0FBQyxHQUFJLFlBQVc7QUFDZixTQUFPLElBQVA7QUFDQSxDQUZHLEVBQUo7O0FBSUEsSUFBSTtBQUNIO0FBQ0FBLEdBQUMsR0FBR0EsQ0FBQyxJQUFJLElBQUlDLFFBQUosQ0FBYSxhQUFiLEdBQVQ7QUFDQSxDQUhELENBR0UsT0FBT25aLENBQVAsRUFBVTtBQUNYO0FBQ0EsTUFBSSxRQUFPb1osTUFBUCx5Q0FBT0EsTUFBUCxPQUFrQixRQUF0QixFQUFnQ0YsQ0FBQyxHQUFHRSxNQUFKO0FBQ2hDLEMsQ0FFRDtBQUNBO0FBQ0E7OztBQUVBMWMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCdWMsQ0FBakIsQzs7Ozs7Ozs7Ozs7QUNuQkF4YyxNQUFNLENBQUNDLE9BQVAsR0FBaUIsVUFBU0QsTUFBVCxFQUFpQjtBQUNqQyxNQUFJLENBQUNBLE1BQU0sQ0FBQzJjLGVBQVosRUFBNkI7QUFDNUIzYyxVQUFNLENBQUM0YyxTQUFQLEdBQW1CLFlBQVcsQ0FBRSxDQUFoQzs7QUFDQTVjLFVBQU0sQ0FBQ3FXLEtBQVAsR0FBZSxFQUFmLENBRjRCLENBRzVCOztBQUNBLFFBQUksQ0FBQ3JXLE1BQU0sQ0FBQzZjLFFBQVosRUFBc0I3YyxNQUFNLENBQUM2YyxRQUFQLEdBQWtCLEVBQWxCO0FBQ3RCdFksVUFBTSxDQUFDQyxjQUFQLENBQXNCeEUsTUFBdEIsRUFBOEIsUUFBOUIsRUFBd0M7QUFDdkM4YyxnQkFBVSxFQUFFLElBRDJCO0FBRXZDQyxTQUFHLEVBQUUsZUFBVztBQUNmLGVBQU8vYyxNQUFNLENBQUNnZCxDQUFkO0FBQ0E7QUFKc0MsS0FBeEM7QUFNQXpZLFVBQU0sQ0FBQ0MsY0FBUCxDQUFzQnhFLE1BQXRCLEVBQThCLElBQTlCLEVBQW9DO0FBQ25DOGMsZ0JBQVUsRUFBRSxJQUR1QjtBQUVuQ0MsU0FBRyxFQUFFLGVBQVc7QUFDZixlQUFPL2MsTUFBTSxDQUFDVyxDQUFkO0FBQ0E7QUFKa0MsS0FBcEM7QUFNQVgsVUFBTSxDQUFDMmMsZUFBUCxHQUF5QixDQUF6QjtBQUNBOztBQUNELFNBQU8zYyxNQUFQO0FBQ0EsQ0FyQkQsQzs7Ozs7Ozs7Ozs7QUNBQSxJQUFJLEdBQUcsRUFBRSxDQUFDO0FBQ1YsUUFBUSxHQUFHO0lBQ1QsUUFBUSxFQUFFLHFCQUFxQjtDQUNoQyxDQUFDO0FBQ0YsTUFBTSxDQUFDLE9BQU8sR0FBRyxtQkFBTyxDQUFDLHVFQUErQixDQUFDLENBQUMsSUFBSSxDQUFDO0FBQy9ELE1BQU0sQ0FBQyxPQUFPLEdBQUcsbUJBQU8sQ0FBQyx5RkFBd0MsQ0FBQyxDQUFDLFlBQVksQ0FBQyIsImZpbGUiOiJwcml2YWN5VXJsUHJvY2Vzc29yLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDApO1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmZ1bmN0aW9uIGF0b2Ioc3RyKSB7XG4gIHJldHVybiBCdWZmZXIuZnJvbShzdHIsICdiYXNlNjQnKS50b1N0cmluZygnYmluYXJ5Jyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYXRvYi5hdG9iID0gYXRvYjtcbiIsIid1c2Ugc3RyaWN0J1xuXG5leHBvcnRzLmJ5dGVMZW5ndGggPSBieXRlTGVuZ3RoXG5leHBvcnRzLnRvQnl0ZUFycmF5ID0gdG9CeXRlQXJyYXlcbmV4cG9ydHMuZnJvbUJ5dGVBcnJheSA9IGZyb21CeXRlQXJyYXlcblxudmFyIGxvb2t1cCA9IFtdXG52YXIgcmV2TG9va3VwID0gW11cbnZhciBBcnIgPSB0eXBlb2YgVWludDhBcnJheSAhPT0gJ3VuZGVmaW5lZCcgPyBVaW50OEFycmF5IDogQXJyYXlcblxudmFyIGNvZGUgPSAnQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVphYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ejAxMjM0NTY3ODkrLydcbmZvciAodmFyIGkgPSAwLCBsZW4gPSBjb2RlLmxlbmd0aDsgaSA8IGxlbjsgKytpKSB7XG4gIGxvb2t1cFtpXSA9IGNvZGVbaV1cbiAgcmV2TG9va3VwW2NvZGUuY2hhckNvZGVBdChpKV0gPSBpXG59XG5cbi8vIFN1cHBvcnQgZGVjb2RpbmcgVVJMLXNhZmUgYmFzZTY0IHN0cmluZ3MsIGFzIE5vZGUuanMgZG9lcy5cbi8vIFNlZTogaHR0cHM6Ly9lbi53aWtpcGVkaWEub3JnL3dpa2kvQmFzZTY0I1VSTF9hcHBsaWNhdGlvbnNcbnJldkxvb2t1cFsnLScuY2hhckNvZGVBdCgwKV0gPSA2MlxucmV2TG9va3VwWydfJy5jaGFyQ29kZUF0KDApXSA9IDYzXG5cbmZ1bmN0aW9uIGdldExlbnMgKGI2NCkge1xuICB2YXIgbGVuID0gYjY0Lmxlbmd0aFxuXG4gIGlmIChsZW4gJSA0ID4gMCkge1xuICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBzdHJpbmcuIExlbmd0aCBtdXN0IGJlIGEgbXVsdGlwbGUgb2YgNCcpXG4gIH1cblxuICAvLyBUcmltIG9mZiBleHRyYSBieXRlcyBhZnRlciBwbGFjZWhvbGRlciBieXRlcyBhcmUgZm91bmRcbiAgLy8gU2VlOiBodHRwczovL2dpdGh1Yi5jb20vYmVhdGdhbW1pdC9iYXNlNjQtanMvaXNzdWVzLzQyXG4gIHZhciB2YWxpZExlbiA9IGI2NC5pbmRleE9mKCc9JylcbiAgaWYgKHZhbGlkTGVuID09PSAtMSkgdmFsaWRMZW4gPSBsZW5cblxuICB2YXIgcGxhY2VIb2xkZXJzTGVuID0gdmFsaWRMZW4gPT09IGxlblxuICAgID8gMFxuICAgIDogNCAtICh2YWxpZExlbiAlIDQpXG5cbiAgcmV0dXJuIFt2YWxpZExlbiwgcGxhY2VIb2xkZXJzTGVuXVxufVxuXG4vLyBiYXNlNjQgaXMgNC8zICsgdXAgdG8gdHdvIGNoYXJhY3RlcnMgb2YgdGhlIG9yaWdpbmFsIGRhdGFcbmZ1bmN0aW9uIGJ5dGVMZW5ndGggKGI2NCkge1xuICB2YXIgbGVucyA9IGdldExlbnMoYjY0KVxuICB2YXIgdmFsaWRMZW4gPSBsZW5zWzBdXG4gIHZhciBwbGFjZUhvbGRlcnNMZW4gPSBsZW5zWzFdXG4gIHJldHVybiAoKHZhbGlkTGVuICsgcGxhY2VIb2xkZXJzTGVuKSAqIDMgLyA0KSAtIHBsYWNlSG9sZGVyc0xlblxufVxuXG5mdW5jdGlvbiBfYnl0ZUxlbmd0aCAoYjY0LCB2YWxpZExlbiwgcGxhY2VIb2xkZXJzTGVuKSB7XG4gIHJldHVybiAoKHZhbGlkTGVuICsgcGxhY2VIb2xkZXJzTGVuKSAqIDMgLyA0KSAtIHBsYWNlSG9sZGVyc0xlblxufVxuXG5mdW5jdGlvbiB0b0J5dGVBcnJheSAoYjY0KSB7XG4gIHZhciB0bXBcbiAgdmFyIGxlbnMgPSBnZXRMZW5zKGI2NClcbiAgdmFyIHZhbGlkTGVuID0gbGVuc1swXVxuICB2YXIgcGxhY2VIb2xkZXJzTGVuID0gbGVuc1sxXVxuXG4gIHZhciBhcnIgPSBuZXcgQXJyKF9ieXRlTGVuZ3RoKGI2NCwgdmFsaWRMZW4sIHBsYWNlSG9sZGVyc0xlbikpXG5cbiAgdmFyIGN1ckJ5dGUgPSAwXG5cbiAgLy8gaWYgdGhlcmUgYXJlIHBsYWNlaG9sZGVycywgb25seSBnZXQgdXAgdG8gdGhlIGxhc3QgY29tcGxldGUgNCBjaGFyc1xuICB2YXIgbGVuID0gcGxhY2VIb2xkZXJzTGVuID4gMFxuICAgID8gdmFsaWRMZW4gLSA0XG4gICAgOiB2YWxpZExlblxuXG4gIHZhciBpXG4gIGZvciAoaSA9IDA7IGkgPCBsZW47IGkgKz0gNCkge1xuICAgIHRtcCA9XG4gICAgICAocmV2TG9va3VwW2I2NC5jaGFyQ29kZUF0KGkpXSA8PCAxOCkgfFxuICAgICAgKHJldkxvb2t1cFtiNjQuY2hhckNvZGVBdChpICsgMSldIDw8IDEyKSB8XG4gICAgICAocmV2TG9va3VwW2I2NC5jaGFyQ29kZUF0KGkgKyAyKV0gPDwgNikgfFxuICAgICAgcmV2TG9va3VwW2I2NC5jaGFyQ29kZUF0KGkgKyAzKV1cbiAgICBhcnJbY3VyQnl0ZSsrXSA9ICh0bXAgPj4gMTYpICYgMHhGRlxuICAgIGFycltjdXJCeXRlKytdID0gKHRtcCA+PiA4KSAmIDB4RkZcbiAgICBhcnJbY3VyQnl0ZSsrXSA9IHRtcCAmIDB4RkZcbiAgfVxuXG4gIGlmIChwbGFjZUhvbGRlcnNMZW4gPT09IDIpIHtcbiAgICB0bXAgPVxuICAgICAgKHJldkxvb2t1cFtiNjQuY2hhckNvZGVBdChpKV0gPDwgMikgfFxuICAgICAgKHJldkxvb2t1cFtiNjQuY2hhckNvZGVBdChpICsgMSldID4+IDQpXG4gICAgYXJyW2N1ckJ5dGUrK10gPSB0bXAgJiAweEZGXG4gIH1cblxuICBpZiAocGxhY2VIb2xkZXJzTGVuID09PSAxKSB7XG4gICAgdG1wID1cbiAgICAgIChyZXZMb29rdXBbYjY0LmNoYXJDb2RlQXQoaSldIDw8IDEwKSB8XG4gICAgICAocmV2TG9va3VwW2I2NC5jaGFyQ29kZUF0KGkgKyAxKV0gPDwgNCkgfFxuICAgICAgKHJldkxvb2t1cFtiNjQuY2hhckNvZGVBdChpICsgMildID4+IDIpXG4gICAgYXJyW2N1ckJ5dGUrK10gPSAodG1wID4+IDgpICYgMHhGRlxuICAgIGFycltjdXJCeXRlKytdID0gdG1wICYgMHhGRlxuICB9XG5cbiAgcmV0dXJuIGFyclxufVxuXG5mdW5jdGlvbiB0cmlwbGV0VG9CYXNlNjQgKG51bSkge1xuICByZXR1cm4gbG9va3VwW251bSA+PiAxOCAmIDB4M0ZdICtcbiAgICBsb29rdXBbbnVtID4+IDEyICYgMHgzRl0gK1xuICAgIGxvb2t1cFtudW0gPj4gNiAmIDB4M0ZdICtcbiAgICBsb29rdXBbbnVtICYgMHgzRl1cbn1cblxuZnVuY3Rpb24gZW5jb2RlQ2h1bmsgKHVpbnQ4LCBzdGFydCwgZW5kKSB7XG4gIHZhciB0bXBcbiAgdmFyIG91dHB1dCA9IFtdXG4gIGZvciAodmFyIGkgPSBzdGFydDsgaSA8IGVuZDsgaSArPSAzKSB7XG4gICAgdG1wID1cbiAgICAgICgodWludDhbaV0gPDwgMTYpICYgMHhGRjAwMDApICtcbiAgICAgICgodWludDhbaSArIDFdIDw8IDgpICYgMHhGRjAwKSArXG4gICAgICAodWludDhbaSArIDJdICYgMHhGRilcbiAgICBvdXRwdXQucHVzaCh0cmlwbGV0VG9CYXNlNjQodG1wKSlcbiAgfVxuICByZXR1cm4gb3V0cHV0LmpvaW4oJycpXG59XG5cbmZ1bmN0aW9uIGZyb21CeXRlQXJyYXkgKHVpbnQ4KSB7XG4gIHZhciB0bXBcbiAgdmFyIGxlbiA9IHVpbnQ4Lmxlbmd0aFxuICB2YXIgZXh0cmFCeXRlcyA9IGxlbiAlIDMgLy8gaWYgd2UgaGF2ZSAxIGJ5dGUgbGVmdCwgcGFkIDIgYnl0ZXNcbiAgdmFyIHBhcnRzID0gW11cbiAgdmFyIG1heENodW5rTGVuZ3RoID0gMTYzODMgLy8gbXVzdCBiZSBtdWx0aXBsZSBvZiAzXG5cbiAgLy8gZ28gdGhyb3VnaCB0aGUgYXJyYXkgZXZlcnkgdGhyZWUgYnl0ZXMsIHdlJ2xsIGRlYWwgd2l0aCB0cmFpbGluZyBzdHVmZiBsYXRlclxuICBmb3IgKHZhciBpID0gMCwgbGVuMiA9IGxlbiAtIGV4dHJhQnl0ZXM7IGkgPCBsZW4yOyBpICs9IG1heENodW5rTGVuZ3RoKSB7XG4gICAgcGFydHMucHVzaChlbmNvZGVDaHVuayh1aW50OCwgaSwgKGkgKyBtYXhDaHVua0xlbmd0aCkgPiBsZW4yID8gbGVuMiA6IChpICsgbWF4Q2h1bmtMZW5ndGgpKSlcbiAgfVxuXG4gIC8vIHBhZCB0aGUgZW5kIHdpdGggemVyb3MsIGJ1dCBtYWtlIHN1cmUgdG8gbm90IGZvcmdldCB0aGUgZXh0cmEgYnl0ZXNcbiAgaWYgKGV4dHJhQnl0ZXMgPT09IDEpIHtcbiAgICB0bXAgPSB1aW50OFtsZW4gLSAxXVxuICAgIHBhcnRzLnB1c2goXG4gICAgICBsb29rdXBbdG1wID4+IDJdICtcbiAgICAgIGxvb2t1cFsodG1wIDw8IDQpICYgMHgzRl0gK1xuICAgICAgJz09J1xuICAgIClcbiAgfSBlbHNlIGlmIChleHRyYUJ5dGVzID09PSAyKSB7XG4gICAgdG1wID0gKHVpbnQ4W2xlbiAtIDJdIDw8IDgpICsgdWludDhbbGVuIC0gMV1cbiAgICBwYXJ0cy5wdXNoKFxuICAgICAgbG9va3VwW3RtcCA+PiAxMF0gK1xuICAgICAgbG9va3VwWyh0bXAgPj4gNCkgJiAweDNGXSArXG4gICAgICBsb29rdXBbKHRtcCA8PCAyKSAmIDB4M0ZdICtcbiAgICAgICc9J1xuICAgIClcbiAgfVxuXG4gIHJldHVybiBwYXJ0cy5qb2luKCcnKVxufVxuIiwiLyohXG4gKiBUaGUgYnVmZmVyIG1vZHVsZSBmcm9tIG5vZGUuanMsIGZvciB0aGUgYnJvd3Nlci5cbiAqXG4gKiBAYXV0aG9yICAgRmVyb3NzIEFib3VraGFkaWplaCA8aHR0cDovL2Zlcm9zcy5vcmc+XG4gKiBAbGljZW5zZSAgTUlUXG4gKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLXByb3RvICovXG5cbid1c2Ugc3RyaWN0J1xuXG52YXIgYmFzZTY0ID0gcmVxdWlyZSgnYmFzZTY0LWpzJylcbnZhciBpZWVlNzU0ID0gcmVxdWlyZSgnaWVlZTc1NCcpXG52YXIgaXNBcnJheSA9IHJlcXVpcmUoJ2lzYXJyYXknKVxuXG5leHBvcnRzLkJ1ZmZlciA9IEJ1ZmZlclxuZXhwb3J0cy5TbG93QnVmZmVyID0gU2xvd0J1ZmZlclxuZXhwb3J0cy5JTlNQRUNUX01BWF9CWVRFUyA9IDUwXG5cbi8qKlxuICogSWYgYEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUYDpcbiAqICAgPT09IHRydWUgICAgVXNlIFVpbnQ4QXJyYXkgaW1wbGVtZW50YXRpb24gKGZhc3Rlc3QpXG4gKiAgID09PSBmYWxzZSAgIFVzZSBPYmplY3QgaW1wbGVtZW50YXRpb24gKG1vc3QgY29tcGF0aWJsZSwgZXZlbiBJRTYpXG4gKlxuICogQnJvd3NlcnMgdGhhdCBzdXBwb3J0IHR5cGVkIGFycmF5cyBhcmUgSUUgMTArLCBGaXJlZm94IDQrLCBDaHJvbWUgNyssIFNhZmFyaSA1LjErLFxuICogT3BlcmEgMTEuNissIGlPUyA0LjIrLlxuICpcbiAqIER1ZSB0byB2YXJpb3VzIGJyb3dzZXIgYnVncywgc29tZXRpbWVzIHRoZSBPYmplY3QgaW1wbGVtZW50YXRpb24gd2lsbCBiZSB1c2VkIGV2ZW5cbiAqIHdoZW4gdGhlIGJyb3dzZXIgc3VwcG9ydHMgdHlwZWQgYXJyYXlzLlxuICpcbiAqIE5vdGU6XG4gKlxuICogICAtIEZpcmVmb3ggNC0yOSBsYWNrcyBzdXBwb3J0IGZvciBhZGRpbmcgbmV3IHByb3BlcnRpZXMgdG8gYFVpbnQ4QXJyYXlgIGluc3RhbmNlcyxcbiAqICAgICBTZWU6IGh0dHBzOi8vYnVnemlsbGEubW96aWxsYS5vcmcvc2hvd19idWcuY2dpP2lkPTY5NTQzOC5cbiAqXG4gKiAgIC0gQ2hyb21lIDktMTAgaXMgbWlzc2luZyB0aGUgYFR5cGVkQXJyYXkucHJvdG90eXBlLnN1YmFycmF5YCBmdW5jdGlvbi5cbiAqXG4gKiAgIC0gSUUxMCBoYXMgYSBicm9rZW4gYFR5cGVkQXJyYXkucHJvdG90eXBlLnN1YmFycmF5YCBmdW5jdGlvbiB3aGljaCByZXR1cm5zIGFycmF5cyBvZlxuICogICAgIGluY29ycmVjdCBsZW5ndGggaW4gc29tZSBzaXR1YXRpb25zLlxuXG4gKiBXZSBkZXRlY3QgdGhlc2UgYnVnZ3kgYnJvd3NlcnMgYW5kIHNldCBgQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlRgIHRvIGBmYWxzZWAgc28gdGhleVxuICogZ2V0IHRoZSBPYmplY3QgaW1wbGVtZW50YXRpb24sIHdoaWNoIGlzIHNsb3dlciBidXQgYmVoYXZlcyBjb3JyZWN0bHkuXG4gKi9cbkJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUID0gZ2xvYmFsLlRZUEVEX0FSUkFZX1NVUFBPUlQgIT09IHVuZGVmaW5lZFxuICA/IGdsb2JhbC5UWVBFRF9BUlJBWV9TVVBQT1JUXG4gIDogdHlwZWRBcnJheVN1cHBvcnQoKVxuXG4vKlxuICogRXhwb3J0IGtNYXhMZW5ndGggYWZ0ZXIgdHlwZWQgYXJyYXkgc3VwcG9ydCBpcyBkZXRlcm1pbmVkLlxuICovXG5leHBvcnRzLmtNYXhMZW5ndGggPSBrTWF4TGVuZ3RoKClcblxuZnVuY3Rpb24gdHlwZWRBcnJheVN1cHBvcnQgKCkge1xuICB0cnkge1xuICAgIHZhciBhcnIgPSBuZXcgVWludDhBcnJheSgxKVxuICAgIGFyci5fX3Byb3RvX18gPSB7X19wcm90b19fOiBVaW50OEFycmF5LnByb3RvdHlwZSwgZm9vOiBmdW5jdGlvbiAoKSB7IHJldHVybiA0MiB9fVxuICAgIHJldHVybiBhcnIuZm9vKCkgPT09IDQyICYmIC8vIHR5cGVkIGFycmF5IGluc3RhbmNlcyBjYW4gYmUgYXVnbWVudGVkXG4gICAgICAgIHR5cGVvZiBhcnIuc3ViYXJyYXkgPT09ICdmdW5jdGlvbicgJiYgLy8gY2hyb21lIDktMTAgbGFjayBgc3ViYXJyYXlgXG4gICAgICAgIGFyci5zdWJhcnJheSgxLCAxKS5ieXRlTGVuZ3RoID09PSAwIC8vIGllMTAgaGFzIGJyb2tlbiBgc3ViYXJyYXlgXG4gIH0gY2F0Y2ggKGUpIHtcbiAgICByZXR1cm4gZmFsc2VcbiAgfVxufVxuXG5mdW5jdGlvbiBrTWF4TGVuZ3RoICgpIHtcbiAgcmV0dXJuIEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUXG4gICAgPyAweDdmZmZmZmZmXG4gICAgOiAweDNmZmZmZmZmXG59XG5cbmZ1bmN0aW9uIGNyZWF0ZUJ1ZmZlciAodGhhdCwgbGVuZ3RoKSB7XG4gIGlmIChrTWF4TGVuZ3RoKCkgPCBsZW5ndGgpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignSW52YWxpZCB0eXBlZCBhcnJheSBsZW5ndGgnKVxuICB9XG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIC8vIFJldHVybiBhbiBhdWdtZW50ZWQgYFVpbnQ4QXJyYXlgIGluc3RhbmNlLCBmb3IgYmVzdCBwZXJmb3JtYW5jZVxuICAgIHRoYXQgPSBuZXcgVWludDhBcnJheShsZW5ndGgpXG4gICAgdGhhdC5fX3Byb3RvX18gPSBCdWZmZXIucHJvdG90eXBlXG4gIH0gZWxzZSB7XG4gICAgLy8gRmFsbGJhY2s6IFJldHVybiBhbiBvYmplY3QgaW5zdGFuY2Ugb2YgdGhlIEJ1ZmZlciBjbGFzc1xuICAgIGlmICh0aGF0ID09PSBudWxsKSB7XG4gICAgICB0aGF0ID0gbmV3IEJ1ZmZlcihsZW5ndGgpXG4gICAgfVxuICAgIHRoYXQubGVuZ3RoID0gbGVuZ3RoXG4gIH1cblxuICByZXR1cm4gdGhhdFxufVxuXG4vKipcbiAqIFRoZSBCdWZmZXIgY29uc3RydWN0b3IgcmV0dXJucyBpbnN0YW5jZXMgb2YgYFVpbnQ4QXJyYXlgIHRoYXQgaGF2ZSB0aGVpclxuICogcHJvdG90eXBlIGNoYW5nZWQgdG8gYEJ1ZmZlci5wcm90b3R5cGVgLiBGdXJ0aGVybW9yZSwgYEJ1ZmZlcmAgaXMgYSBzdWJjbGFzcyBvZlxuICogYFVpbnQ4QXJyYXlgLCBzbyB0aGUgcmV0dXJuZWQgaW5zdGFuY2VzIHdpbGwgaGF2ZSBhbGwgdGhlIG5vZGUgYEJ1ZmZlcmAgbWV0aG9kc1xuICogYW5kIHRoZSBgVWludDhBcnJheWAgbWV0aG9kcy4gU3F1YXJlIGJyYWNrZXQgbm90YXRpb24gd29ya3MgYXMgZXhwZWN0ZWQgLS0gaXRcbiAqIHJldHVybnMgYSBzaW5nbGUgb2N0ZXQuXG4gKlxuICogVGhlIGBVaW50OEFycmF5YCBwcm90b3R5cGUgcmVtYWlucyB1bm1vZGlmaWVkLlxuICovXG5cbmZ1bmN0aW9uIEJ1ZmZlciAoYXJnLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpIHtcbiAgaWYgKCFCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCAmJiAhKHRoaXMgaW5zdGFuY2VvZiBCdWZmZXIpKSB7XG4gICAgcmV0dXJuIG5ldyBCdWZmZXIoYXJnLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpXG4gIH1cblxuICAvLyBDb21tb24gY2FzZS5cbiAgaWYgKHR5cGVvZiBhcmcgPT09ICdudW1iZXInKSB7XG4gICAgaWYgKHR5cGVvZiBlbmNvZGluZ09yT2Zmc2V0ID09PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICAnSWYgZW5jb2RpbmcgaXMgc3BlY2lmaWVkIHRoZW4gdGhlIGZpcnN0IGFyZ3VtZW50IG11c3QgYmUgYSBzdHJpbmcnXG4gICAgICApXG4gICAgfVxuICAgIHJldHVybiBhbGxvY1Vuc2FmZSh0aGlzLCBhcmcpXG4gIH1cbiAgcmV0dXJuIGZyb20odGhpcywgYXJnLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpXG59XG5cbkJ1ZmZlci5wb29sU2l6ZSA9IDgxOTIgLy8gbm90IHVzZWQgYnkgdGhpcyBpbXBsZW1lbnRhdGlvblxuXG4vLyBUT0RPOiBMZWdhY3ksIG5vdCBuZWVkZWQgYW55bW9yZS4gUmVtb3ZlIGluIG5leHQgbWFqb3IgdmVyc2lvbi5cbkJ1ZmZlci5fYXVnbWVudCA9IGZ1bmN0aW9uIChhcnIpIHtcbiAgYXJyLl9fcHJvdG9fXyA9IEJ1ZmZlci5wcm90b3R5cGVcbiAgcmV0dXJuIGFyclxufVxuXG5mdW5jdGlvbiBmcm9tICh0aGF0LCB2YWx1ZSwgZW5jb2RpbmdPck9mZnNldCwgbGVuZ3RoKSB7XG4gIGlmICh0eXBlb2YgdmFsdWUgPT09ICdudW1iZXInKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcignXCJ2YWx1ZVwiIGFyZ3VtZW50IG11c3Qgbm90IGJlIGEgbnVtYmVyJylcbiAgfVxuXG4gIGlmICh0eXBlb2YgQXJyYXlCdWZmZXIgIT09ICd1bmRlZmluZWQnICYmIHZhbHVlIGluc3RhbmNlb2YgQXJyYXlCdWZmZXIpIHtcbiAgICByZXR1cm4gZnJvbUFycmF5QnVmZmVyKHRoYXQsIHZhbHVlLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpXG4gIH1cblxuICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBmcm9tU3RyaW5nKHRoYXQsIHZhbHVlLCBlbmNvZGluZ09yT2Zmc2V0KVxuICB9XG5cbiAgcmV0dXJuIGZyb21PYmplY3QodGhhdCwgdmFsdWUpXG59XG5cbi8qKlxuICogRnVuY3Rpb25hbGx5IGVxdWl2YWxlbnQgdG8gQnVmZmVyKGFyZywgZW5jb2RpbmcpIGJ1dCB0aHJvd3MgYSBUeXBlRXJyb3JcbiAqIGlmIHZhbHVlIGlzIGEgbnVtYmVyLlxuICogQnVmZmVyLmZyb20oc3RyWywgZW5jb2RpbmddKVxuICogQnVmZmVyLmZyb20oYXJyYXkpXG4gKiBCdWZmZXIuZnJvbShidWZmZXIpXG4gKiBCdWZmZXIuZnJvbShhcnJheUJ1ZmZlclssIGJ5dGVPZmZzZXRbLCBsZW5ndGhdXSlcbiAqKi9cbkJ1ZmZlci5mcm9tID0gZnVuY3Rpb24gKHZhbHVlLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpIHtcbiAgcmV0dXJuIGZyb20obnVsbCwgdmFsdWUsIGVuY29kaW5nT3JPZmZzZXQsIGxlbmd0aClcbn1cblxuaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gIEJ1ZmZlci5wcm90b3R5cGUuX19wcm90b19fID0gVWludDhBcnJheS5wcm90b3R5cGVcbiAgQnVmZmVyLl9fcHJvdG9fXyA9IFVpbnQ4QXJyYXlcbiAgaWYgKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC5zcGVjaWVzICYmXG4gICAgICBCdWZmZXJbU3ltYm9sLnNwZWNpZXNdID09PSBCdWZmZXIpIHtcbiAgICAvLyBGaXggc3ViYXJyYXkoKSBpbiBFUzIwMTYuIFNlZTogaHR0cHM6Ly9naXRodWIuY29tL2Zlcm9zcy9idWZmZXIvcHVsbC85N1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShCdWZmZXIsIFN5bWJvbC5zcGVjaWVzLCB7XG4gICAgICB2YWx1ZTogbnVsbCxcbiAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgIH0pXG4gIH1cbn1cblxuZnVuY3Rpb24gYXNzZXJ0U2l6ZSAoc2l6ZSkge1xuICBpZiAodHlwZW9mIHNpemUgIT09ICdudW1iZXInKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcignXCJzaXplXCIgYXJndW1lbnQgbXVzdCBiZSBhIG51bWJlcicpXG4gIH0gZWxzZSBpZiAoc2l6ZSA8IDApIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignXCJzaXplXCIgYXJndW1lbnQgbXVzdCBub3QgYmUgbmVnYXRpdmUnKVxuICB9XG59XG5cbmZ1bmN0aW9uIGFsbG9jICh0aGF0LCBzaXplLCBmaWxsLCBlbmNvZGluZykge1xuICBhc3NlcnRTaXplKHNpemUpXG4gIGlmIChzaXplIDw9IDApIHtcbiAgICByZXR1cm4gY3JlYXRlQnVmZmVyKHRoYXQsIHNpemUpXG4gIH1cbiAgaWYgKGZpbGwgIT09IHVuZGVmaW5lZCkge1xuICAgIC8vIE9ubHkgcGF5IGF0dGVudGlvbiB0byBlbmNvZGluZyBpZiBpdCdzIGEgc3RyaW5nLiBUaGlzXG4gICAgLy8gcHJldmVudHMgYWNjaWRlbnRhbGx5IHNlbmRpbmcgaW4gYSBudW1iZXIgdGhhdCB3b3VsZFxuICAgIC8vIGJlIGludGVycHJldHRlZCBhcyBhIHN0YXJ0IG9mZnNldC5cbiAgICByZXR1cm4gdHlwZW9mIGVuY29kaW5nID09PSAnc3RyaW5nJ1xuICAgICAgPyBjcmVhdGVCdWZmZXIodGhhdCwgc2l6ZSkuZmlsbChmaWxsLCBlbmNvZGluZylcbiAgICAgIDogY3JlYXRlQnVmZmVyKHRoYXQsIHNpemUpLmZpbGwoZmlsbClcbiAgfVxuICByZXR1cm4gY3JlYXRlQnVmZmVyKHRoYXQsIHNpemUpXG59XG5cbi8qKlxuICogQ3JlYXRlcyBhIG5ldyBmaWxsZWQgQnVmZmVyIGluc3RhbmNlLlxuICogYWxsb2Moc2l6ZVssIGZpbGxbLCBlbmNvZGluZ11dKVxuICoqL1xuQnVmZmVyLmFsbG9jID0gZnVuY3Rpb24gKHNpemUsIGZpbGwsIGVuY29kaW5nKSB7XG4gIHJldHVybiBhbGxvYyhudWxsLCBzaXplLCBmaWxsLCBlbmNvZGluZylcbn1cblxuZnVuY3Rpb24gYWxsb2NVbnNhZmUgKHRoYXQsIHNpemUpIHtcbiAgYXNzZXJ0U2l6ZShzaXplKVxuICB0aGF0ID0gY3JlYXRlQnVmZmVyKHRoYXQsIHNpemUgPCAwID8gMCA6IGNoZWNrZWQoc2l6ZSkgfCAwKVxuICBpZiAoIUJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzaXplOyArK2kpIHtcbiAgICAgIHRoYXRbaV0gPSAwXG4gICAgfVxuICB9XG4gIHJldHVybiB0aGF0XG59XG5cbi8qKlxuICogRXF1aXZhbGVudCB0byBCdWZmZXIobnVtKSwgYnkgZGVmYXVsdCBjcmVhdGVzIGEgbm9uLXplcm8tZmlsbGVkIEJ1ZmZlciBpbnN0YW5jZS5cbiAqICovXG5CdWZmZXIuYWxsb2NVbnNhZmUgPSBmdW5jdGlvbiAoc2l6ZSkge1xuICByZXR1cm4gYWxsb2NVbnNhZmUobnVsbCwgc2l6ZSlcbn1cbi8qKlxuICogRXF1aXZhbGVudCB0byBTbG93QnVmZmVyKG51bSksIGJ5IGRlZmF1bHQgY3JlYXRlcyBhIG5vbi16ZXJvLWZpbGxlZCBCdWZmZXIgaW5zdGFuY2UuXG4gKi9cbkJ1ZmZlci5hbGxvY1Vuc2FmZVNsb3cgPSBmdW5jdGlvbiAoc2l6ZSkge1xuICByZXR1cm4gYWxsb2NVbnNhZmUobnVsbCwgc2l6ZSlcbn1cblxuZnVuY3Rpb24gZnJvbVN0cmluZyAodGhhdCwgc3RyaW5nLCBlbmNvZGluZykge1xuICBpZiAodHlwZW9mIGVuY29kaW5nICE9PSAnc3RyaW5nJyB8fCBlbmNvZGluZyA9PT0gJycpIHtcbiAgICBlbmNvZGluZyA9ICd1dGY4J1xuICB9XG5cbiAgaWYgKCFCdWZmZXIuaXNFbmNvZGluZyhlbmNvZGluZykpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdcImVuY29kaW5nXCIgbXVzdCBiZSBhIHZhbGlkIHN0cmluZyBlbmNvZGluZycpXG4gIH1cblxuICB2YXIgbGVuZ3RoID0gYnl0ZUxlbmd0aChzdHJpbmcsIGVuY29kaW5nKSB8IDBcbiAgdGhhdCA9IGNyZWF0ZUJ1ZmZlcih0aGF0LCBsZW5ndGgpXG5cbiAgdmFyIGFjdHVhbCA9IHRoYXQud3JpdGUoc3RyaW5nLCBlbmNvZGluZylcblxuICBpZiAoYWN0dWFsICE9PSBsZW5ndGgpIHtcbiAgICAvLyBXcml0aW5nIGEgaGV4IHN0cmluZywgZm9yIGV4YW1wbGUsIHRoYXQgY29udGFpbnMgaW52YWxpZCBjaGFyYWN0ZXJzIHdpbGxcbiAgICAvLyBjYXVzZSBldmVyeXRoaW5nIGFmdGVyIHRoZSBmaXJzdCBpbnZhbGlkIGNoYXJhY3RlciB0byBiZSBpZ25vcmVkLiAoZS5nLlxuICAgIC8vICdhYnh4Y2QnIHdpbGwgYmUgdHJlYXRlZCBhcyAnYWInKVxuICAgIHRoYXQgPSB0aGF0LnNsaWNlKDAsIGFjdHVhbClcbiAgfVxuXG4gIHJldHVybiB0aGF0XG59XG5cbmZ1bmN0aW9uIGZyb21BcnJheUxpa2UgKHRoYXQsIGFycmF5KSB7XG4gIHZhciBsZW5ndGggPSBhcnJheS5sZW5ndGggPCAwID8gMCA6IGNoZWNrZWQoYXJyYXkubGVuZ3RoKSB8IDBcbiAgdGhhdCA9IGNyZWF0ZUJ1ZmZlcih0aGF0LCBsZW5ndGgpXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyBpICs9IDEpIHtcbiAgICB0aGF0W2ldID0gYXJyYXlbaV0gJiAyNTVcbiAgfVxuICByZXR1cm4gdGhhdFxufVxuXG5mdW5jdGlvbiBmcm9tQXJyYXlCdWZmZXIgKHRoYXQsIGFycmF5LCBieXRlT2Zmc2V0LCBsZW5ndGgpIHtcbiAgYXJyYXkuYnl0ZUxlbmd0aCAvLyB0aGlzIHRocm93cyBpZiBgYXJyYXlgIGlzIG5vdCBhIHZhbGlkIEFycmF5QnVmZmVyXG5cbiAgaWYgKGJ5dGVPZmZzZXQgPCAwIHx8IGFycmF5LmJ5dGVMZW5ndGggPCBieXRlT2Zmc2V0KSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1xcJ29mZnNldFxcJyBpcyBvdXQgb2YgYm91bmRzJylcbiAgfVxuXG4gIGlmIChhcnJheS5ieXRlTGVuZ3RoIDwgYnl0ZU9mZnNldCArIChsZW5ndGggfHwgMCkpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignXFwnbGVuZ3RoXFwnIGlzIG91dCBvZiBib3VuZHMnKVxuICB9XG5cbiAgaWYgKGJ5dGVPZmZzZXQgPT09IHVuZGVmaW5lZCAmJiBsZW5ndGggPT09IHVuZGVmaW5lZCkge1xuICAgIGFycmF5ID0gbmV3IFVpbnQ4QXJyYXkoYXJyYXkpXG4gIH0gZWxzZSBpZiAobGVuZ3RoID09PSB1bmRlZmluZWQpIHtcbiAgICBhcnJheSA9IG5ldyBVaW50OEFycmF5KGFycmF5LCBieXRlT2Zmc2V0KVxuICB9IGVsc2Uge1xuICAgIGFycmF5ID0gbmV3IFVpbnQ4QXJyYXkoYXJyYXksIGJ5dGVPZmZzZXQsIGxlbmd0aClcbiAgfVxuXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIC8vIFJldHVybiBhbiBhdWdtZW50ZWQgYFVpbnQ4QXJyYXlgIGluc3RhbmNlLCBmb3IgYmVzdCBwZXJmb3JtYW5jZVxuICAgIHRoYXQgPSBhcnJheVxuICAgIHRoYXQuX19wcm90b19fID0gQnVmZmVyLnByb3RvdHlwZVxuICB9IGVsc2Uge1xuICAgIC8vIEZhbGxiYWNrOiBSZXR1cm4gYW4gb2JqZWN0IGluc3RhbmNlIG9mIHRoZSBCdWZmZXIgY2xhc3NcbiAgICB0aGF0ID0gZnJvbUFycmF5TGlrZSh0aGF0LCBhcnJheSlcbiAgfVxuICByZXR1cm4gdGhhdFxufVxuXG5mdW5jdGlvbiBmcm9tT2JqZWN0ICh0aGF0LCBvYmopIHtcbiAgaWYgKEJ1ZmZlci5pc0J1ZmZlcihvYmopKSB7XG4gICAgdmFyIGxlbiA9IGNoZWNrZWQob2JqLmxlbmd0aCkgfCAwXG4gICAgdGhhdCA9IGNyZWF0ZUJ1ZmZlcih0aGF0LCBsZW4pXG5cbiAgICBpZiAodGhhdC5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiB0aGF0XG4gICAgfVxuXG4gICAgb2JqLmNvcHkodGhhdCwgMCwgMCwgbGVuKVxuICAgIHJldHVybiB0aGF0XG4gIH1cblxuICBpZiAob2JqKSB7XG4gICAgaWYgKCh0eXBlb2YgQXJyYXlCdWZmZXIgIT09ICd1bmRlZmluZWQnICYmXG4gICAgICAgIG9iai5idWZmZXIgaW5zdGFuY2VvZiBBcnJheUJ1ZmZlcikgfHwgJ2xlbmd0aCcgaW4gb2JqKSB7XG4gICAgICBpZiAodHlwZW9mIG9iai5sZW5ndGggIT09ICdudW1iZXInIHx8IGlzbmFuKG9iai5sZW5ndGgpKSB7XG4gICAgICAgIHJldHVybiBjcmVhdGVCdWZmZXIodGhhdCwgMClcbiAgICAgIH1cbiAgICAgIHJldHVybiBmcm9tQXJyYXlMaWtlKHRoYXQsIG9iailcbiAgICB9XG5cbiAgICBpZiAob2JqLnR5cGUgPT09ICdCdWZmZXInICYmIGlzQXJyYXkob2JqLmRhdGEpKSB7XG4gICAgICByZXR1cm4gZnJvbUFycmF5TGlrZSh0aGF0LCBvYmouZGF0YSlcbiAgICB9XG4gIH1cblxuICB0aHJvdyBuZXcgVHlwZUVycm9yKCdGaXJzdCBhcmd1bWVudCBtdXN0IGJlIGEgc3RyaW5nLCBCdWZmZXIsIEFycmF5QnVmZmVyLCBBcnJheSwgb3IgYXJyYXktbGlrZSBvYmplY3QuJylcbn1cblxuZnVuY3Rpb24gY2hlY2tlZCAobGVuZ3RoKSB7XG4gIC8vIE5vdGU6IGNhbm5vdCB1c2UgYGxlbmd0aCA8IGtNYXhMZW5ndGgoKWAgaGVyZSBiZWNhdXNlIHRoYXQgZmFpbHMgd2hlblxuICAvLyBsZW5ndGggaXMgTmFOICh3aGljaCBpcyBvdGhlcndpc2UgY29lcmNlZCB0byB6ZXJvLilcbiAgaWYgKGxlbmd0aCA+PSBrTWF4TGVuZ3RoKCkpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignQXR0ZW1wdCB0byBhbGxvY2F0ZSBCdWZmZXIgbGFyZ2VyIHRoYW4gbWF4aW11bSAnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAnc2l6ZTogMHgnICsga01heExlbmd0aCgpLnRvU3RyaW5nKDE2KSArICcgYnl0ZXMnKVxuICB9XG4gIHJldHVybiBsZW5ndGggfCAwXG59XG5cbmZ1bmN0aW9uIFNsb3dCdWZmZXIgKGxlbmd0aCkge1xuICBpZiAoK2xlbmd0aCAhPSBsZW5ndGgpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBlcWVxZXFcbiAgICBsZW5ndGggPSAwXG4gIH1cbiAgcmV0dXJuIEJ1ZmZlci5hbGxvYygrbGVuZ3RoKVxufVxuXG5CdWZmZXIuaXNCdWZmZXIgPSBmdW5jdGlvbiBpc0J1ZmZlciAoYikge1xuICByZXR1cm4gISEoYiAhPSBudWxsICYmIGIuX2lzQnVmZmVyKVxufVxuXG5CdWZmZXIuY29tcGFyZSA9IGZ1bmN0aW9uIGNvbXBhcmUgKGEsIGIpIHtcbiAgaWYgKCFCdWZmZXIuaXNCdWZmZXIoYSkgfHwgIUJ1ZmZlci5pc0J1ZmZlcihiKSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0FyZ3VtZW50cyBtdXN0IGJlIEJ1ZmZlcnMnKVxuICB9XG5cbiAgaWYgKGEgPT09IGIpIHJldHVybiAwXG5cbiAgdmFyIHggPSBhLmxlbmd0aFxuICB2YXIgeSA9IGIubGVuZ3RoXG5cbiAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IE1hdGgubWluKHgsIHkpOyBpIDwgbGVuOyArK2kpIHtcbiAgICBpZiAoYVtpXSAhPT0gYltpXSkge1xuICAgICAgeCA9IGFbaV1cbiAgICAgIHkgPSBiW2ldXG4gICAgICBicmVha1xuICAgIH1cbiAgfVxuXG4gIGlmICh4IDwgeSkgcmV0dXJuIC0xXG4gIGlmICh5IDwgeCkgcmV0dXJuIDFcbiAgcmV0dXJuIDBcbn1cblxuQnVmZmVyLmlzRW5jb2RpbmcgPSBmdW5jdGlvbiBpc0VuY29kaW5nIChlbmNvZGluZykge1xuICBzd2l0Y2ggKFN0cmluZyhlbmNvZGluZykudG9Mb3dlckNhc2UoKSkge1xuICAgIGNhc2UgJ2hleCc6XG4gICAgY2FzZSAndXRmOCc6XG4gICAgY2FzZSAndXRmLTgnOlxuICAgIGNhc2UgJ2FzY2lpJzpcbiAgICBjYXNlICdsYXRpbjEnOlxuICAgIGNhc2UgJ2JpbmFyeSc6XG4gICAgY2FzZSAnYmFzZTY0JzpcbiAgICBjYXNlICd1Y3MyJzpcbiAgICBjYXNlICd1Y3MtMic6XG4gICAgY2FzZSAndXRmMTZsZSc6XG4gICAgY2FzZSAndXRmLTE2bGUnOlxuICAgICAgcmV0dXJuIHRydWVcbiAgICBkZWZhdWx0OlxuICAgICAgcmV0dXJuIGZhbHNlXG4gIH1cbn1cblxuQnVmZmVyLmNvbmNhdCA9IGZ1bmN0aW9uIGNvbmNhdCAobGlzdCwgbGVuZ3RoKSB7XG4gIGlmICghaXNBcnJheShsaXN0KSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1wibGlzdFwiIGFyZ3VtZW50IG11c3QgYmUgYW4gQXJyYXkgb2YgQnVmZmVycycpXG4gIH1cblxuICBpZiAobGlzdC5sZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gQnVmZmVyLmFsbG9jKDApXG4gIH1cblxuICB2YXIgaVxuICBpZiAobGVuZ3RoID09PSB1bmRlZmluZWQpIHtcbiAgICBsZW5ndGggPSAwXG4gICAgZm9yIChpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyArK2kpIHtcbiAgICAgIGxlbmd0aCArPSBsaXN0W2ldLmxlbmd0aFxuICAgIH1cbiAgfVxuXG4gIHZhciBidWZmZXIgPSBCdWZmZXIuYWxsb2NVbnNhZmUobGVuZ3RoKVxuICB2YXIgcG9zID0gMFxuICBmb3IgKGkgPSAwOyBpIDwgbGlzdC5sZW5ndGg7ICsraSkge1xuICAgIHZhciBidWYgPSBsaXN0W2ldXG4gICAgaWYgKCFCdWZmZXIuaXNCdWZmZXIoYnVmKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignXCJsaXN0XCIgYXJndW1lbnQgbXVzdCBiZSBhbiBBcnJheSBvZiBCdWZmZXJzJylcbiAgICB9XG4gICAgYnVmLmNvcHkoYnVmZmVyLCBwb3MpXG4gICAgcG9zICs9IGJ1Zi5sZW5ndGhcbiAgfVxuICByZXR1cm4gYnVmZmVyXG59XG5cbmZ1bmN0aW9uIGJ5dGVMZW5ndGggKHN0cmluZywgZW5jb2RpbmcpIHtcbiAgaWYgKEJ1ZmZlci5pc0J1ZmZlcihzdHJpbmcpKSB7XG4gICAgcmV0dXJuIHN0cmluZy5sZW5ndGhcbiAgfVxuICBpZiAodHlwZW9mIEFycmF5QnVmZmVyICE9PSAndW5kZWZpbmVkJyAmJiB0eXBlb2YgQXJyYXlCdWZmZXIuaXNWaWV3ID09PSAnZnVuY3Rpb24nICYmXG4gICAgICAoQXJyYXlCdWZmZXIuaXNWaWV3KHN0cmluZykgfHwgc3RyaW5nIGluc3RhbmNlb2YgQXJyYXlCdWZmZXIpKSB7XG4gICAgcmV0dXJuIHN0cmluZy5ieXRlTGVuZ3RoXG4gIH1cbiAgaWYgKHR5cGVvZiBzdHJpbmcgIT09ICdzdHJpbmcnKSB7XG4gICAgc3RyaW5nID0gJycgKyBzdHJpbmdcbiAgfVxuXG4gIHZhciBsZW4gPSBzdHJpbmcubGVuZ3RoXG4gIGlmIChsZW4gPT09IDApIHJldHVybiAwXG5cbiAgLy8gVXNlIGEgZm9yIGxvb3AgdG8gYXZvaWQgcmVjdXJzaW9uXG4gIHZhciBsb3dlcmVkQ2FzZSA9IGZhbHNlXG4gIGZvciAoOzspIHtcbiAgICBzd2l0Y2ggKGVuY29kaW5nKSB7XG4gICAgICBjYXNlICdhc2NpaSc6XG4gICAgICBjYXNlICdsYXRpbjEnOlxuICAgICAgY2FzZSAnYmluYXJ5JzpcbiAgICAgICAgcmV0dXJuIGxlblxuICAgICAgY2FzZSAndXRmOCc6XG4gICAgICBjYXNlICd1dGYtOCc6XG4gICAgICBjYXNlIHVuZGVmaW5lZDpcbiAgICAgICAgcmV0dXJuIHV0ZjhUb0J5dGVzKHN0cmluZykubGVuZ3RoXG4gICAgICBjYXNlICd1Y3MyJzpcbiAgICAgIGNhc2UgJ3Vjcy0yJzpcbiAgICAgIGNhc2UgJ3V0ZjE2bGUnOlxuICAgICAgY2FzZSAndXRmLTE2bGUnOlxuICAgICAgICByZXR1cm4gbGVuICogMlxuICAgICAgY2FzZSAnaGV4JzpcbiAgICAgICAgcmV0dXJuIGxlbiA+Pj4gMVxuICAgICAgY2FzZSAnYmFzZTY0JzpcbiAgICAgICAgcmV0dXJuIGJhc2U2NFRvQnl0ZXMoc3RyaW5nKS5sZW5ndGhcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGlmIChsb3dlcmVkQ2FzZSkgcmV0dXJuIHV0ZjhUb0J5dGVzKHN0cmluZykubGVuZ3RoIC8vIGFzc3VtZSB1dGY4XG4gICAgICAgIGVuY29kaW5nID0gKCcnICsgZW5jb2RpbmcpLnRvTG93ZXJDYXNlKClcbiAgICAgICAgbG93ZXJlZENhc2UgPSB0cnVlXG4gICAgfVxuICB9XG59XG5CdWZmZXIuYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGhcblxuZnVuY3Rpb24gc2xvd1RvU3RyaW5nIChlbmNvZGluZywgc3RhcnQsIGVuZCkge1xuICB2YXIgbG93ZXJlZENhc2UgPSBmYWxzZVxuXG4gIC8vIE5vIG5lZWQgdG8gdmVyaWZ5IHRoYXQgXCJ0aGlzLmxlbmd0aCA8PSBNQVhfVUlOVDMyXCIgc2luY2UgaXQncyBhIHJlYWQtb25seVxuICAvLyBwcm9wZXJ0eSBvZiBhIHR5cGVkIGFycmF5LlxuXG4gIC8vIFRoaXMgYmVoYXZlcyBuZWl0aGVyIGxpa2UgU3RyaW5nIG5vciBVaW50OEFycmF5IGluIHRoYXQgd2Ugc2V0IHN0YXJ0L2VuZFxuICAvLyB0byB0aGVpciB1cHBlci9sb3dlciBib3VuZHMgaWYgdGhlIHZhbHVlIHBhc3NlZCBpcyBvdXQgb2YgcmFuZ2UuXG4gIC8vIHVuZGVmaW5lZCBpcyBoYW5kbGVkIHNwZWNpYWxseSBhcyBwZXIgRUNNQS0yNjIgNnRoIEVkaXRpb24sXG4gIC8vIFNlY3Rpb24gMTMuMy4zLjcgUnVudGltZSBTZW1hbnRpY3M6IEtleWVkQmluZGluZ0luaXRpYWxpemF0aW9uLlxuICBpZiAoc3RhcnQgPT09IHVuZGVmaW5lZCB8fCBzdGFydCA8IDApIHtcbiAgICBzdGFydCA9IDBcbiAgfVxuICAvLyBSZXR1cm4gZWFybHkgaWYgc3RhcnQgPiB0aGlzLmxlbmd0aC4gRG9uZSBoZXJlIHRvIHByZXZlbnQgcG90ZW50aWFsIHVpbnQzMlxuICAvLyBjb2VyY2lvbiBmYWlsIGJlbG93LlxuICBpZiAoc3RhcnQgPiB0aGlzLmxlbmd0aCkge1xuICAgIHJldHVybiAnJ1xuICB9XG5cbiAgaWYgKGVuZCA9PT0gdW5kZWZpbmVkIHx8IGVuZCA+IHRoaXMubGVuZ3RoKSB7XG4gICAgZW5kID0gdGhpcy5sZW5ndGhcbiAgfVxuXG4gIGlmIChlbmQgPD0gMCkge1xuICAgIHJldHVybiAnJ1xuICB9XG5cbiAgLy8gRm9yY2UgY29lcnNpb24gdG8gdWludDMyLiBUaGlzIHdpbGwgYWxzbyBjb2VyY2UgZmFsc2V5L05hTiB2YWx1ZXMgdG8gMC5cbiAgZW5kID4+Pj0gMFxuICBzdGFydCA+Pj49IDBcblxuICBpZiAoZW5kIDw9IHN0YXJ0KSB7XG4gICAgcmV0dXJuICcnXG4gIH1cblxuICBpZiAoIWVuY29kaW5nKSBlbmNvZGluZyA9ICd1dGY4J1xuXG4gIHdoaWxlICh0cnVlKSB7XG4gICAgc3dpdGNoIChlbmNvZGluZykge1xuICAgICAgY2FzZSAnaGV4JzpcbiAgICAgICAgcmV0dXJuIGhleFNsaWNlKHRoaXMsIHN0YXJ0LCBlbmQpXG5cbiAgICAgIGNhc2UgJ3V0ZjgnOlxuICAgICAgY2FzZSAndXRmLTgnOlxuICAgICAgICByZXR1cm4gdXRmOFNsaWNlKHRoaXMsIHN0YXJ0LCBlbmQpXG5cbiAgICAgIGNhc2UgJ2FzY2lpJzpcbiAgICAgICAgcmV0dXJuIGFzY2lpU2xpY2UodGhpcywgc3RhcnQsIGVuZClcblxuICAgICAgY2FzZSAnbGF0aW4xJzpcbiAgICAgIGNhc2UgJ2JpbmFyeSc6XG4gICAgICAgIHJldHVybiBsYXRpbjFTbGljZSh0aGlzLCBzdGFydCwgZW5kKVxuXG4gICAgICBjYXNlICdiYXNlNjQnOlxuICAgICAgICByZXR1cm4gYmFzZTY0U2xpY2UodGhpcywgc3RhcnQsIGVuZClcblxuICAgICAgY2FzZSAndWNzMic6XG4gICAgICBjYXNlICd1Y3MtMic6XG4gICAgICBjYXNlICd1dGYxNmxlJzpcbiAgICAgIGNhc2UgJ3V0Zi0xNmxlJzpcbiAgICAgICAgcmV0dXJuIHV0ZjE2bGVTbGljZSh0aGlzLCBzdGFydCwgZW5kKVxuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICBpZiAobG93ZXJlZENhc2UpIHRocm93IG5ldyBUeXBlRXJyb3IoJ1Vua25vd24gZW5jb2Rpbmc6ICcgKyBlbmNvZGluZylcbiAgICAgICAgZW5jb2RpbmcgPSAoZW5jb2RpbmcgKyAnJykudG9Mb3dlckNhc2UoKVxuICAgICAgICBsb3dlcmVkQ2FzZSA9IHRydWVcbiAgICB9XG4gIH1cbn1cblxuLy8gVGhlIHByb3BlcnR5IGlzIHVzZWQgYnkgYEJ1ZmZlci5pc0J1ZmZlcmAgYW5kIGBpcy1idWZmZXJgIChpbiBTYWZhcmkgNS03KSB0byBkZXRlY3Rcbi8vIEJ1ZmZlciBpbnN0YW5jZXMuXG5CdWZmZXIucHJvdG90eXBlLl9pc0J1ZmZlciA9IHRydWVcblxuZnVuY3Rpb24gc3dhcCAoYiwgbiwgbSkge1xuICB2YXIgaSA9IGJbbl1cbiAgYltuXSA9IGJbbV1cbiAgYlttXSA9IGlcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5zd2FwMTYgPSBmdW5jdGlvbiBzd2FwMTYgKCkge1xuICB2YXIgbGVuID0gdGhpcy5sZW5ndGhcbiAgaWYgKGxlbiAlIDIgIT09IDApIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignQnVmZmVyIHNpemUgbXVzdCBiZSBhIG11bHRpcGxlIG9mIDE2LWJpdHMnKVxuICB9XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyBpICs9IDIpIHtcbiAgICBzd2FwKHRoaXMsIGksIGkgKyAxKVxuICB9XG4gIHJldHVybiB0aGlzXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuc3dhcDMyID0gZnVuY3Rpb24gc3dhcDMyICgpIHtcbiAgdmFyIGxlbiA9IHRoaXMubGVuZ3RoXG4gIGlmIChsZW4gJSA0ICE9PSAwKSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ0J1ZmZlciBzaXplIG11c3QgYmUgYSBtdWx0aXBsZSBvZiAzMi1iaXRzJylcbiAgfVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSArPSA0KSB7XG4gICAgc3dhcCh0aGlzLCBpLCBpICsgMylcbiAgICBzd2FwKHRoaXMsIGkgKyAxLCBpICsgMilcbiAgfVxuICByZXR1cm4gdGhpc1xufVxuXG5CdWZmZXIucHJvdG90eXBlLnN3YXA2NCA9IGZ1bmN0aW9uIHN3YXA2NCAoKSB7XG4gIHZhciBsZW4gPSB0aGlzLmxlbmd0aFxuICBpZiAobGVuICUgOCAhPT0gMCkge1xuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdCdWZmZXIgc2l6ZSBtdXN0IGJlIGEgbXVsdGlwbGUgb2YgNjQtYml0cycpXG4gIH1cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkgKz0gOCkge1xuICAgIHN3YXAodGhpcywgaSwgaSArIDcpXG4gICAgc3dhcCh0aGlzLCBpICsgMSwgaSArIDYpXG4gICAgc3dhcCh0aGlzLCBpICsgMiwgaSArIDUpXG4gICAgc3dhcCh0aGlzLCBpICsgMywgaSArIDQpXG4gIH1cbiAgcmV0dXJuIHRoaXNcbn1cblxuQnVmZmVyLnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uIHRvU3RyaW5nICgpIHtcbiAgdmFyIGxlbmd0aCA9IHRoaXMubGVuZ3RoIHwgMFxuICBpZiAobGVuZ3RoID09PSAwKSByZXR1cm4gJydcbiAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHJldHVybiB1dGY4U2xpY2UodGhpcywgMCwgbGVuZ3RoKVxuICByZXR1cm4gc2xvd1RvU3RyaW5nLmFwcGx5KHRoaXMsIGFyZ3VtZW50cylcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5lcXVhbHMgPSBmdW5jdGlvbiBlcXVhbHMgKGIpIHtcbiAgaWYgKCFCdWZmZXIuaXNCdWZmZXIoYikpIHRocm93IG5ldyBUeXBlRXJyb3IoJ0FyZ3VtZW50IG11c3QgYmUgYSBCdWZmZXInKVxuICBpZiAodGhpcyA9PT0gYikgcmV0dXJuIHRydWVcbiAgcmV0dXJuIEJ1ZmZlci5jb21wYXJlKHRoaXMsIGIpID09PSAwXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuaW5zcGVjdCA9IGZ1bmN0aW9uIGluc3BlY3QgKCkge1xuICB2YXIgc3RyID0gJydcbiAgdmFyIG1heCA9IGV4cG9ydHMuSU5TUEVDVF9NQVhfQllURVNcbiAgaWYgKHRoaXMubGVuZ3RoID4gMCkge1xuICAgIHN0ciA9IHRoaXMudG9TdHJpbmcoJ2hleCcsIDAsIG1heCkubWF0Y2goLy57Mn0vZykuam9pbignICcpXG4gICAgaWYgKHRoaXMubGVuZ3RoID4gbWF4KSBzdHIgKz0gJyAuLi4gJ1xuICB9XG4gIHJldHVybiAnPEJ1ZmZlciAnICsgc3RyICsgJz4nXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuY29tcGFyZSA9IGZ1bmN0aW9uIGNvbXBhcmUgKHRhcmdldCwgc3RhcnQsIGVuZCwgdGhpc1N0YXJ0LCB0aGlzRW5kKSB7XG4gIGlmICghQnVmZmVyLmlzQnVmZmVyKHRhcmdldCkpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdBcmd1bWVudCBtdXN0IGJlIGEgQnVmZmVyJylcbiAgfVxuXG4gIGlmIChzdGFydCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgc3RhcnQgPSAwXG4gIH1cbiAgaWYgKGVuZCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgZW5kID0gdGFyZ2V0ID8gdGFyZ2V0Lmxlbmd0aCA6IDBcbiAgfVxuICBpZiAodGhpc1N0YXJ0ID09PSB1bmRlZmluZWQpIHtcbiAgICB0aGlzU3RhcnQgPSAwXG4gIH1cbiAgaWYgKHRoaXNFbmQgPT09IHVuZGVmaW5lZCkge1xuICAgIHRoaXNFbmQgPSB0aGlzLmxlbmd0aFxuICB9XG5cbiAgaWYgKHN0YXJ0IDwgMCB8fCBlbmQgPiB0YXJnZXQubGVuZ3RoIHx8IHRoaXNTdGFydCA8IDAgfHwgdGhpc0VuZCA+IHRoaXMubGVuZ3RoKSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ291dCBvZiByYW5nZSBpbmRleCcpXG4gIH1cblxuICBpZiAodGhpc1N0YXJ0ID49IHRoaXNFbmQgJiYgc3RhcnQgPj0gZW5kKSB7XG4gICAgcmV0dXJuIDBcbiAgfVxuICBpZiAodGhpc1N0YXJ0ID49IHRoaXNFbmQpIHtcbiAgICByZXR1cm4gLTFcbiAgfVxuICBpZiAoc3RhcnQgPj0gZW5kKSB7XG4gICAgcmV0dXJuIDFcbiAgfVxuXG4gIHN0YXJ0ID4+Pj0gMFxuICBlbmQgPj4+PSAwXG4gIHRoaXNTdGFydCA+Pj49IDBcbiAgdGhpc0VuZCA+Pj49IDBcblxuICBpZiAodGhpcyA9PT0gdGFyZ2V0KSByZXR1cm4gMFxuXG4gIHZhciB4ID0gdGhpc0VuZCAtIHRoaXNTdGFydFxuICB2YXIgeSA9IGVuZCAtIHN0YXJ0XG4gIHZhciBsZW4gPSBNYXRoLm1pbih4LCB5KVxuXG4gIHZhciB0aGlzQ29weSA9IHRoaXMuc2xpY2UodGhpc1N0YXJ0LCB0aGlzRW5kKVxuICB2YXIgdGFyZ2V0Q29weSA9IHRhcmdldC5zbGljZShzdGFydCwgZW5kKVxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyArK2kpIHtcbiAgICBpZiAodGhpc0NvcHlbaV0gIT09IHRhcmdldENvcHlbaV0pIHtcbiAgICAgIHggPSB0aGlzQ29weVtpXVxuICAgICAgeSA9IHRhcmdldENvcHlbaV1cbiAgICAgIGJyZWFrXG4gICAgfVxuICB9XG5cbiAgaWYgKHggPCB5KSByZXR1cm4gLTFcbiAgaWYgKHkgPCB4KSByZXR1cm4gMVxuICByZXR1cm4gMFxufVxuXG4vLyBGaW5kcyBlaXRoZXIgdGhlIGZpcnN0IGluZGV4IG9mIGB2YWxgIGluIGBidWZmZXJgIGF0IG9mZnNldCA+PSBgYnl0ZU9mZnNldGAsXG4vLyBPUiB0aGUgbGFzdCBpbmRleCBvZiBgdmFsYCBpbiBgYnVmZmVyYCBhdCBvZmZzZXQgPD0gYGJ5dGVPZmZzZXRgLlxuLy9cbi8vIEFyZ3VtZW50czpcbi8vIC0gYnVmZmVyIC0gYSBCdWZmZXIgdG8gc2VhcmNoXG4vLyAtIHZhbCAtIGEgc3RyaW5nLCBCdWZmZXIsIG9yIG51bWJlclxuLy8gLSBieXRlT2Zmc2V0IC0gYW4gaW5kZXggaW50byBgYnVmZmVyYDsgd2lsbCBiZSBjbGFtcGVkIHRvIGFuIGludDMyXG4vLyAtIGVuY29kaW5nIC0gYW4gb3B0aW9uYWwgZW5jb2RpbmcsIHJlbGV2YW50IGlzIHZhbCBpcyBhIHN0cmluZ1xuLy8gLSBkaXIgLSB0cnVlIGZvciBpbmRleE9mLCBmYWxzZSBmb3IgbGFzdEluZGV4T2ZcbmZ1bmN0aW9uIGJpZGlyZWN0aW9uYWxJbmRleE9mIChidWZmZXIsIHZhbCwgYnl0ZU9mZnNldCwgZW5jb2RpbmcsIGRpcikge1xuICAvLyBFbXB0eSBidWZmZXIgbWVhbnMgbm8gbWF0Y2hcbiAgaWYgKGJ1ZmZlci5sZW5ndGggPT09IDApIHJldHVybiAtMVxuXG4gIC8vIE5vcm1hbGl6ZSBieXRlT2Zmc2V0XG4gIGlmICh0eXBlb2YgYnl0ZU9mZnNldCA9PT0gJ3N0cmluZycpIHtcbiAgICBlbmNvZGluZyA9IGJ5dGVPZmZzZXRcbiAgICBieXRlT2Zmc2V0ID0gMFxuICB9IGVsc2UgaWYgKGJ5dGVPZmZzZXQgPiAweDdmZmZmZmZmKSB7XG4gICAgYnl0ZU9mZnNldCA9IDB4N2ZmZmZmZmZcbiAgfSBlbHNlIGlmIChieXRlT2Zmc2V0IDwgLTB4ODAwMDAwMDApIHtcbiAgICBieXRlT2Zmc2V0ID0gLTB4ODAwMDAwMDBcbiAgfVxuICBieXRlT2Zmc2V0ID0gK2J5dGVPZmZzZXQgIC8vIENvZXJjZSB0byBOdW1iZXIuXG4gIGlmIChpc05hTihieXRlT2Zmc2V0KSkge1xuICAgIC8vIGJ5dGVPZmZzZXQ6IGl0IGl0J3MgdW5kZWZpbmVkLCBudWxsLCBOYU4sIFwiZm9vXCIsIGV0Yywgc2VhcmNoIHdob2xlIGJ1ZmZlclxuICAgIGJ5dGVPZmZzZXQgPSBkaXIgPyAwIDogKGJ1ZmZlci5sZW5ndGggLSAxKVxuICB9XG5cbiAgLy8gTm9ybWFsaXplIGJ5dGVPZmZzZXQ6IG5lZ2F0aXZlIG9mZnNldHMgc3RhcnQgZnJvbSB0aGUgZW5kIG9mIHRoZSBidWZmZXJcbiAgaWYgKGJ5dGVPZmZzZXQgPCAwKSBieXRlT2Zmc2V0ID0gYnVmZmVyLmxlbmd0aCArIGJ5dGVPZmZzZXRcbiAgaWYgKGJ5dGVPZmZzZXQgPj0gYnVmZmVyLmxlbmd0aCkge1xuICAgIGlmIChkaXIpIHJldHVybiAtMVxuICAgIGVsc2UgYnl0ZU9mZnNldCA9IGJ1ZmZlci5sZW5ndGggLSAxXG4gIH0gZWxzZSBpZiAoYnl0ZU9mZnNldCA8IDApIHtcbiAgICBpZiAoZGlyKSBieXRlT2Zmc2V0ID0gMFxuICAgIGVsc2UgcmV0dXJuIC0xXG4gIH1cblxuICAvLyBOb3JtYWxpemUgdmFsXG4gIGlmICh0eXBlb2YgdmFsID09PSAnc3RyaW5nJykge1xuICAgIHZhbCA9IEJ1ZmZlci5mcm9tKHZhbCwgZW5jb2RpbmcpXG4gIH1cblxuICAvLyBGaW5hbGx5LCBzZWFyY2ggZWl0aGVyIGluZGV4T2YgKGlmIGRpciBpcyB0cnVlKSBvciBsYXN0SW5kZXhPZlxuICBpZiAoQnVmZmVyLmlzQnVmZmVyKHZhbCkpIHtcbiAgICAvLyBTcGVjaWFsIGNhc2U6IGxvb2tpbmcgZm9yIGVtcHR5IHN0cmluZy9idWZmZXIgYWx3YXlzIGZhaWxzXG4gICAgaWYgKHZhbC5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiAtMVxuICAgIH1cbiAgICByZXR1cm4gYXJyYXlJbmRleE9mKGJ1ZmZlciwgdmFsLCBieXRlT2Zmc2V0LCBlbmNvZGluZywgZGlyKVxuICB9IGVsc2UgaWYgKHR5cGVvZiB2YWwgPT09ICdudW1iZXInKSB7XG4gICAgdmFsID0gdmFsICYgMHhGRiAvLyBTZWFyY2ggZm9yIGEgYnl0ZSB2YWx1ZSBbMC0yNTVdXG4gICAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUICYmXG4gICAgICAgIHR5cGVvZiBVaW50OEFycmF5LnByb3RvdHlwZS5pbmRleE9mID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBpZiAoZGlyKSB7XG4gICAgICAgIHJldHVybiBVaW50OEFycmF5LnByb3RvdHlwZS5pbmRleE9mLmNhbGwoYnVmZmVyLCB2YWwsIGJ5dGVPZmZzZXQpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gVWludDhBcnJheS5wcm90b3R5cGUubGFzdEluZGV4T2YuY2FsbChidWZmZXIsIHZhbCwgYnl0ZU9mZnNldClcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGFycmF5SW5kZXhPZihidWZmZXIsIFsgdmFsIF0sIGJ5dGVPZmZzZXQsIGVuY29kaW5nLCBkaXIpXG4gIH1cblxuICB0aHJvdyBuZXcgVHlwZUVycm9yKCd2YWwgbXVzdCBiZSBzdHJpbmcsIG51bWJlciBvciBCdWZmZXInKVxufVxuXG5mdW5jdGlvbiBhcnJheUluZGV4T2YgKGFyciwgdmFsLCBieXRlT2Zmc2V0LCBlbmNvZGluZywgZGlyKSB7XG4gIHZhciBpbmRleFNpemUgPSAxXG4gIHZhciBhcnJMZW5ndGggPSBhcnIubGVuZ3RoXG4gIHZhciB2YWxMZW5ndGggPSB2YWwubGVuZ3RoXG5cbiAgaWYgKGVuY29kaW5nICE9PSB1bmRlZmluZWQpIHtcbiAgICBlbmNvZGluZyA9IFN0cmluZyhlbmNvZGluZykudG9Mb3dlckNhc2UoKVxuICAgIGlmIChlbmNvZGluZyA9PT0gJ3VjczInIHx8IGVuY29kaW5nID09PSAndWNzLTInIHx8XG4gICAgICAgIGVuY29kaW5nID09PSAndXRmMTZsZScgfHwgZW5jb2RpbmcgPT09ICd1dGYtMTZsZScpIHtcbiAgICAgIGlmIChhcnIubGVuZ3RoIDwgMiB8fCB2YWwubGVuZ3RoIDwgMikge1xuICAgICAgICByZXR1cm4gLTFcbiAgICAgIH1cbiAgICAgIGluZGV4U2l6ZSA9IDJcbiAgICAgIGFyckxlbmd0aCAvPSAyXG4gICAgICB2YWxMZW5ndGggLz0gMlxuICAgICAgYnl0ZU9mZnNldCAvPSAyXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gcmVhZCAoYnVmLCBpKSB7XG4gICAgaWYgKGluZGV4U2l6ZSA9PT0gMSkge1xuICAgICAgcmV0dXJuIGJ1ZltpXVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gYnVmLnJlYWRVSW50MTZCRShpICogaW5kZXhTaXplKVxuICAgIH1cbiAgfVxuXG4gIHZhciBpXG4gIGlmIChkaXIpIHtcbiAgICB2YXIgZm91bmRJbmRleCA9IC0xXG4gICAgZm9yIChpID0gYnl0ZU9mZnNldDsgaSA8IGFyckxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAocmVhZChhcnIsIGkpID09PSByZWFkKHZhbCwgZm91bmRJbmRleCA9PT0gLTEgPyAwIDogaSAtIGZvdW5kSW5kZXgpKSB7XG4gICAgICAgIGlmIChmb3VuZEluZGV4ID09PSAtMSkgZm91bmRJbmRleCA9IGlcbiAgICAgICAgaWYgKGkgLSBmb3VuZEluZGV4ICsgMSA9PT0gdmFsTGVuZ3RoKSByZXR1cm4gZm91bmRJbmRleCAqIGluZGV4U2l6ZVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKGZvdW5kSW5kZXggIT09IC0xKSBpIC09IGkgLSBmb3VuZEluZGV4XG4gICAgICAgIGZvdW5kSW5kZXggPSAtMVxuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIHtcbiAgICBpZiAoYnl0ZU9mZnNldCArIHZhbExlbmd0aCA+IGFyckxlbmd0aCkgYnl0ZU9mZnNldCA9IGFyckxlbmd0aCAtIHZhbExlbmd0aFxuICAgIGZvciAoaSA9IGJ5dGVPZmZzZXQ7IGkgPj0gMDsgaS0tKSB7XG4gICAgICB2YXIgZm91bmQgPSB0cnVlXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IHZhbExlbmd0aDsgaisrKSB7XG4gICAgICAgIGlmIChyZWFkKGFyciwgaSArIGopICE9PSByZWFkKHZhbCwgaikpIHtcbiAgICAgICAgICBmb3VuZCA9IGZhbHNlXG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKGZvdW5kKSByZXR1cm4gaVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiAtMVxufVxuXG5CdWZmZXIucHJvdG90eXBlLmluY2x1ZGVzID0gZnVuY3Rpb24gaW5jbHVkZXMgKHZhbCwgYnl0ZU9mZnNldCwgZW5jb2RpbmcpIHtcbiAgcmV0dXJuIHRoaXMuaW5kZXhPZih2YWwsIGJ5dGVPZmZzZXQsIGVuY29kaW5nKSAhPT0gLTFcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5pbmRleE9mID0gZnVuY3Rpb24gaW5kZXhPZiAodmFsLCBieXRlT2Zmc2V0LCBlbmNvZGluZykge1xuICByZXR1cm4gYmlkaXJlY3Rpb25hbEluZGV4T2YodGhpcywgdmFsLCBieXRlT2Zmc2V0LCBlbmNvZGluZywgdHJ1ZSlcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5sYXN0SW5kZXhPZiA9IGZ1bmN0aW9uIGxhc3RJbmRleE9mICh2YWwsIGJ5dGVPZmZzZXQsIGVuY29kaW5nKSB7XG4gIHJldHVybiBiaWRpcmVjdGlvbmFsSW5kZXhPZih0aGlzLCB2YWwsIGJ5dGVPZmZzZXQsIGVuY29kaW5nLCBmYWxzZSlcbn1cblxuZnVuY3Rpb24gaGV4V3JpdGUgKGJ1Ziwgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aCkge1xuICBvZmZzZXQgPSBOdW1iZXIob2Zmc2V0KSB8fCAwXG4gIHZhciByZW1haW5pbmcgPSBidWYubGVuZ3RoIC0gb2Zmc2V0XG4gIGlmICghbGVuZ3RoKSB7XG4gICAgbGVuZ3RoID0gcmVtYWluaW5nXG4gIH0gZWxzZSB7XG4gICAgbGVuZ3RoID0gTnVtYmVyKGxlbmd0aClcbiAgICBpZiAobGVuZ3RoID4gcmVtYWluaW5nKSB7XG4gICAgICBsZW5ndGggPSByZW1haW5pbmdcbiAgICB9XG4gIH1cblxuICAvLyBtdXN0IGJlIGFuIGV2ZW4gbnVtYmVyIG9mIGRpZ2l0c1xuICB2YXIgc3RyTGVuID0gc3RyaW5nLmxlbmd0aFxuICBpZiAoc3RyTGVuICUgMiAhPT0gMCkgdGhyb3cgbmV3IFR5cGVFcnJvcignSW52YWxpZCBoZXggc3RyaW5nJylcblxuICBpZiAobGVuZ3RoID4gc3RyTGVuIC8gMikge1xuICAgIGxlbmd0aCA9IHN0ckxlbiAvIDJcbiAgfVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbmd0aDsgKytpKSB7XG4gICAgdmFyIHBhcnNlZCA9IHBhcnNlSW50KHN0cmluZy5zdWJzdHIoaSAqIDIsIDIpLCAxNilcbiAgICBpZiAoaXNOYU4ocGFyc2VkKSkgcmV0dXJuIGlcbiAgICBidWZbb2Zmc2V0ICsgaV0gPSBwYXJzZWRcbiAgfVxuICByZXR1cm4gaVxufVxuXG5mdW5jdGlvbiB1dGY4V3JpdGUgKGJ1Ziwgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aCkge1xuICByZXR1cm4gYmxpdEJ1ZmZlcih1dGY4VG9CeXRlcyhzdHJpbmcsIGJ1Zi5sZW5ndGggLSBvZmZzZXQpLCBidWYsIG9mZnNldCwgbGVuZ3RoKVxufVxuXG5mdW5jdGlvbiBhc2NpaVdyaXRlIChidWYsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpIHtcbiAgcmV0dXJuIGJsaXRCdWZmZXIoYXNjaWlUb0J5dGVzKHN0cmluZyksIGJ1Ziwgb2Zmc2V0LCBsZW5ndGgpXG59XG5cbmZ1bmN0aW9uIGxhdGluMVdyaXRlIChidWYsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpIHtcbiAgcmV0dXJuIGFzY2lpV3JpdGUoYnVmLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKVxufVxuXG5mdW5jdGlvbiBiYXNlNjRXcml0ZSAoYnVmLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKSB7XG4gIHJldHVybiBibGl0QnVmZmVyKGJhc2U2NFRvQnl0ZXMoc3RyaW5nKSwgYnVmLCBvZmZzZXQsIGxlbmd0aClcbn1cblxuZnVuY3Rpb24gdWNzMldyaXRlIChidWYsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpIHtcbiAgcmV0dXJuIGJsaXRCdWZmZXIodXRmMTZsZVRvQnl0ZXMoc3RyaW5nLCBidWYubGVuZ3RoIC0gb2Zmc2V0KSwgYnVmLCBvZmZzZXQsIGxlbmd0aClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZSA9IGZ1bmN0aW9uIHdyaXRlIChzdHJpbmcsIG9mZnNldCwgbGVuZ3RoLCBlbmNvZGluZykge1xuICAvLyBCdWZmZXIjd3JpdGUoc3RyaW5nKVxuICBpZiAob2Zmc2V0ID09PSB1bmRlZmluZWQpIHtcbiAgICBlbmNvZGluZyA9ICd1dGY4J1xuICAgIGxlbmd0aCA9IHRoaXMubGVuZ3RoXG4gICAgb2Zmc2V0ID0gMFxuICAvLyBCdWZmZXIjd3JpdGUoc3RyaW5nLCBlbmNvZGluZylcbiAgfSBlbHNlIGlmIChsZW5ndGggPT09IHVuZGVmaW5lZCAmJiB0eXBlb2Ygb2Zmc2V0ID09PSAnc3RyaW5nJykge1xuICAgIGVuY29kaW5nID0gb2Zmc2V0XG4gICAgbGVuZ3RoID0gdGhpcy5sZW5ndGhcbiAgICBvZmZzZXQgPSAwXG4gIC8vIEJ1ZmZlciN3cml0ZShzdHJpbmcsIG9mZnNldFssIGxlbmd0aF1bLCBlbmNvZGluZ10pXG4gIH0gZWxzZSBpZiAoaXNGaW5pdGUob2Zmc2V0KSkge1xuICAgIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgICBpZiAoaXNGaW5pdGUobGVuZ3RoKSkge1xuICAgICAgbGVuZ3RoID0gbGVuZ3RoIHwgMFxuICAgICAgaWYgKGVuY29kaW5nID09PSB1bmRlZmluZWQpIGVuY29kaW5nID0gJ3V0ZjgnXG4gICAgfSBlbHNlIHtcbiAgICAgIGVuY29kaW5nID0gbGVuZ3RoXG4gICAgICBsZW5ndGggPSB1bmRlZmluZWRcbiAgICB9XG4gIC8vIGxlZ2FjeSB3cml0ZShzdHJpbmcsIGVuY29kaW5nLCBvZmZzZXQsIGxlbmd0aCkgLSByZW1vdmUgaW4gdjAuMTNcbiAgfSBlbHNlIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAnQnVmZmVyLndyaXRlKHN0cmluZywgZW5jb2RpbmcsIG9mZnNldFssIGxlbmd0aF0pIGlzIG5vIGxvbmdlciBzdXBwb3J0ZWQnXG4gICAgKVxuICB9XG5cbiAgdmFyIHJlbWFpbmluZyA9IHRoaXMubGVuZ3RoIC0gb2Zmc2V0XG4gIGlmIChsZW5ndGggPT09IHVuZGVmaW5lZCB8fCBsZW5ndGggPiByZW1haW5pbmcpIGxlbmd0aCA9IHJlbWFpbmluZ1xuXG4gIGlmICgoc3RyaW5nLmxlbmd0aCA+IDAgJiYgKGxlbmd0aCA8IDAgfHwgb2Zmc2V0IDwgMCkpIHx8IG9mZnNldCA+IHRoaXMubGVuZ3RoKSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ0F0dGVtcHQgdG8gd3JpdGUgb3V0c2lkZSBidWZmZXIgYm91bmRzJylcbiAgfVxuXG4gIGlmICghZW5jb2RpbmcpIGVuY29kaW5nID0gJ3V0ZjgnXG5cbiAgdmFyIGxvd2VyZWRDYXNlID0gZmFsc2VcbiAgZm9yICg7Oykge1xuICAgIHN3aXRjaCAoZW5jb2RpbmcpIHtcbiAgICAgIGNhc2UgJ2hleCc6XG4gICAgICAgIHJldHVybiBoZXhXcml0ZSh0aGlzLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKVxuXG4gICAgICBjYXNlICd1dGY4JzpcbiAgICAgIGNhc2UgJ3V0Zi04JzpcbiAgICAgICAgcmV0dXJuIHV0ZjhXcml0ZSh0aGlzLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKVxuXG4gICAgICBjYXNlICdhc2NpaSc6XG4gICAgICAgIHJldHVybiBhc2NpaVdyaXRlKHRoaXMsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpXG5cbiAgICAgIGNhc2UgJ2xhdGluMSc6XG4gICAgICBjYXNlICdiaW5hcnknOlxuICAgICAgICByZXR1cm4gbGF0aW4xV3JpdGUodGhpcywgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aClcblxuICAgICAgY2FzZSAnYmFzZTY0JzpcbiAgICAgICAgLy8gV2FybmluZzogbWF4TGVuZ3RoIG5vdCB0YWtlbiBpbnRvIGFjY291bnQgaW4gYmFzZTY0V3JpdGVcbiAgICAgICAgcmV0dXJuIGJhc2U2NFdyaXRlKHRoaXMsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpXG5cbiAgICAgIGNhc2UgJ3VjczInOlxuICAgICAgY2FzZSAndWNzLTInOlxuICAgICAgY2FzZSAndXRmMTZsZSc6XG4gICAgICBjYXNlICd1dGYtMTZsZSc6XG4gICAgICAgIHJldHVybiB1Y3MyV3JpdGUodGhpcywgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aClcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgaWYgKGxvd2VyZWRDYXNlKSB0aHJvdyBuZXcgVHlwZUVycm9yKCdVbmtub3duIGVuY29kaW5nOiAnICsgZW5jb2RpbmcpXG4gICAgICAgIGVuY29kaW5nID0gKCcnICsgZW5jb2RpbmcpLnRvTG93ZXJDYXNlKClcbiAgICAgICAgbG93ZXJlZENhc2UgPSB0cnVlXG4gICAgfVxuICB9XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OICgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiAnQnVmZmVyJyxcbiAgICBkYXRhOiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0aGlzLl9hcnIgfHwgdGhpcywgMClcbiAgfVxufVxuXG5mdW5jdGlvbiBiYXNlNjRTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIGlmIChzdGFydCA9PT0gMCAmJiBlbmQgPT09IGJ1Zi5sZW5ndGgpIHtcbiAgICByZXR1cm4gYmFzZTY0LmZyb21CeXRlQXJyYXkoYnVmKVxuICB9IGVsc2Uge1xuICAgIHJldHVybiBiYXNlNjQuZnJvbUJ5dGVBcnJheShidWYuc2xpY2Uoc3RhcnQsIGVuZCkpXG4gIH1cbn1cblxuZnVuY3Rpb24gdXRmOFNsaWNlIChidWYsIHN0YXJ0LCBlbmQpIHtcbiAgZW5kID0gTWF0aC5taW4oYnVmLmxlbmd0aCwgZW5kKVxuICB2YXIgcmVzID0gW11cblxuICB2YXIgaSA9IHN0YXJ0XG4gIHdoaWxlIChpIDwgZW5kKSB7XG4gICAgdmFyIGZpcnN0Qnl0ZSA9IGJ1ZltpXVxuICAgIHZhciBjb2RlUG9pbnQgPSBudWxsXG4gICAgdmFyIGJ5dGVzUGVyU2VxdWVuY2UgPSAoZmlyc3RCeXRlID4gMHhFRikgPyA0XG4gICAgICA6IChmaXJzdEJ5dGUgPiAweERGKSA/IDNcbiAgICAgIDogKGZpcnN0Qnl0ZSA+IDB4QkYpID8gMlxuICAgICAgOiAxXG5cbiAgICBpZiAoaSArIGJ5dGVzUGVyU2VxdWVuY2UgPD0gZW5kKSB7XG4gICAgICB2YXIgc2Vjb25kQnl0ZSwgdGhpcmRCeXRlLCBmb3VydGhCeXRlLCB0ZW1wQ29kZVBvaW50XG5cbiAgICAgIHN3aXRjaCAoYnl0ZXNQZXJTZXF1ZW5jZSkge1xuICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgaWYgKGZpcnN0Qnl0ZSA8IDB4ODApIHtcbiAgICAgICAgICAgIGNvZGVQb2ludCA9IGZpcnN0Qnl0ZVxuICAgICAgICAgIH1cbiAgICAgICAgICBicmVha1xuICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgc2Vjb25kQnl0ZSA9IGJ1ZltpICsgMV1cbiAgICAgICAgICBpZiAoKHNlY29uZEJ5dGUgJiAweEMwKSA9PT0gMHg4MCkge1xuICAgICAgICAgICAgdGVtcENvZGVQb2ludCA9IChmaXJzdEJ5dGUgJiAweDFGKSA8PCAweDYgfCAoc2Vjb25kQnl0ZSAmIDB4M0YpXG4gICAgICAgICAgICBpZiAodGVtcENvZGVQb2ludCA+IDB4N0YpIHtcbiAgICAgICAgICAgICAgY29kZVBvaW50ID0gdGVtcENvZGVQb2ludFxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBicmVha1xuICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgc2Vjb25kQnl0ZSA9IGJ1ZltpICsgMV1cbiAgICAgICAgICB0aGlyZEJ5dGUgPSBidWZbaSArIDJdXG4gICAgICAgICAgaWYgKChzZWNvbmRCeXRlICYgMHhDMCkgPT09IDB4ODAgJiYgKHRoaXJkQnl0ZSAmIDB4QzApID09PSAweDgwKSB7XG4gICAgICAgICAgICB0ZW1wQ29kZVBvaW50ID0gKGZpcnN0Qnl0ZSAmIDB4RikgPDwgMHhDIHwgKHNlY29uZEJ5dGUgJiAweDNGKSA8PCAweDYgfCAodGhpcmRCeXRlICYgMHgzRilcbiAgICAgICAgICAgIGlmICh0ZW1wQ29kZVBvaW50ID4gMHg3RkYgJiYgKHRlbXBDb2RlUG9pbnQgPCAweEQ4MDAgfHwgdGVtcENvZGVQb2ludCA+IDB4REZGRikpIHtcbiAgICAgICAgICAgICAgY29kZVBvaW50ID0gdGVtcENvZGVQb2ludFxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBicmVha1xuICAgICAgICBjYXNlIDQ6XG4gICAgICAgICAgc2Vjb25kQnl0ZSA9IGJ1ZltpICsgMV1cbiAgICAgICAgICB0aGlyZEJ5dGUgPSBidWZbaSArIDJdXG4gICAgICAgICAgZm91cnRoQnl0ZSA9IGJ1ZltpICsgM11cbiAgICAgICAgICBpZiAoKHNlY29uZEJ5dGUgJiAweEMwKSA9PT0gMHg4MCAmJiAodGhpcmRCeXRlICYgMHhDMCkgPT09IDB4ODAgJiYgKGZvdXJ0aEJ5dGUgJiAweEMwKSA9PT0gMHg4MCkge1xuICAgICAgICAgICAgdGVtcENvZGVQb2ludCA9IChmaXJzdEJ5dGUgJiAweEYpIDw8IDB4MTIgfCAoc2Vjb25kQnl0ZSAmIDB4M0YpIDw8IDB4QyB8ICh0aGlyZEJ5dGUgJiAweDNGKSA8PCAweDYgfCAoZm91cnRoQnl0ZSAmIDB4M0YpXG4gICAgICAgICAgICBpZiAodGVtcENvZGVQb2ludCA+IDB4RkZGRiAmJiB0ZW1wQ29kZVBvaW50IDwgMHgxMTAwMDApIHtcbiAgICAgICAgICAgICAgY29kZVBvaW50ID0gdGVtcENvZGVQb2ludFxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoY29kZVBvaW50ID09PSBudWxsKSB7XG4gICAgICAvLyB3ZSBkaWQgbm90IGdlbmVyYXRlIGEgdmFsaWQgY29kZVBvaW50IHNvIGluc2VydCBhXG4gICAgICAvLyByZXBsYWNlbWVudCBjaGFyIChVK0ZGRkQpIGFuZCBhZHZhbmNlIG9ubHkgMSBieXRlXG4gICAgICBjb2RlUG9pbnQgPSAweEZGRkRcbiAgICAgIGJ5dGVzUGVyU2VxdWVuY2UgPSAxXG4gICAgfSBlbHNlIGlmIChjb2RlUG9pbnQgPiAweEZGRkYpIHtcbiAgICAgIC8vIGVuY29kZSB0byB1dGYxNiAoc3Vycm9nYXRlIHBhaXIgZGFuY2UpXG4gICAgICBjb2RlUG9pbnQgLT0gMHgxMDAwMFxuICAgICAgcmVzLnB1c2goY29kZVBvaW50ID4+PiAxMCAmIDB4M0ZGIHwgMHhEODAwKVxuICAgICAgY29kZVBvaW50ID0gMHhEQzAwIHwgY29kZVBvaW50ICYgMHgzRkZcbiAgICB9XG5cbiAgICByZXMucHVzaChjb2RlUG9pbnQpXG4gICAgaSArPSBieXRlc1BlclNlcXVlbmNlXG4gIH1cblxuICByZXR1cm4gZGVjb2RlQ29kZVBvaW50c0FycmF5KHJlcylcbn1cblxuLy8gQmFzZWQgb24gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMjI3NDcyNzIvNjgwNzQyLCB0aGUgYnJvd3NlciB3aXRoXG4vLyB0aGUgbG93ZXN0IGxpbWl0IGlzIENocm9tZSwgd2l0aCAweDEwMDAwIGFyZ3MuXG4vLyBXZSBnbyAxIG1hZ25pdHVkZSBsZXNzLCBmb3Igc2FmZXR5XG52YXIgTUFYX0FSR1VNRU5UU19MRU5HVEggPSAweDEwMDBcblxuZnVuY3Rpb24gZGVjb2RlQ29kZVBvaW50c0FycmF5IChjb2RlUG9pbnRzKSB7XG4gIHZhciBsZW4gPSBjb2RlUG9pbnRzLmxlbmd0aFxuICBpZiAobGVuIDw9IE1BWF9BUkdVTUVOVFNfTEVOR1RIKSB7XG4gICAgcmV0dXJuIFN0cmluZy5mcm9tQ2hhckNvZGUuYXBwbHkoU3RyaW5nLCBjb2RlUG9pbnRzKSAvLyBhdm9pZCBleHRyYSBzbGljZSgpXG4gIH1cblxuICAvLyBEZWNvZGUgaW4gY2h1bmtzIHRvIGF2b2lkIFwiY2FsbCBzdGFjayBzaXplIGV4Y2VlZGVkXCIuXG4gIHZhciByZXMgPSAnJ1xuICB2YXIgaSA9IDBcbiAgd2hpbGUgKGkgPCBsZW4pIHtcbiAgICByZXMgKz0gU3RyaW5nLmZyb21DaGFyQ29kZS5hcHBseShcbiAgICAgIFN0cmluZyxcbiAgICAgIGNvZGVQb2ludHMuc2xpY2UoaSwgaSArPSBNQVhfQVJHVU1FTlRTX0xFTkdUSClcbiAgICApXG4gIH1cbiAgcmV0dXJuIHJlc1xufVxuXG5mdW5jdGlvbiBhc2NpaVNsaWNlIChidWYsIHN0YXJ0LCBlbmQpIHtcbiAgdmFyIHJldCA9ICcnXG4gIGVuZCA9IE1hdGgubWluKGJ1Zi5sZW5ndGgsIGVuZClcblxuICBmb3IgKHZhciBpID0gc3RhcnQ7IGkgPCBlbmQ7ICsraSkge1xuICAgIHJldCArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGJ1ZltpXSAmIDB4N0YpXG4gIH1cbiAgcmV0dXJuIHJldFxufVxuXG5mdW5jdGlvbiBsYXRpbjFTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIHZhciByZXQgPSAnJ1xuICBlbmQgPSBNYXRoLm1pbihidWYubGVuZ3RoLCBlbmQpXG5cbiAgZm9yICh2YXIgaSA9IHN0YXJ0OyBpIDwgZW5kOyArK2kpIHtcbiAgICByZXQgKz0gU3RyaW5nLmZyb21DaGFyQ29kZShidWZbaV0pXG4gIH1cbiAgcmV0dXJuIHJldFxufVxuXG5mdW5jdGlvbiBoZXhTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIHZhciBsZW4gPSBidWYubGVuZ3RoXG5cbiAgaWYgKCFzdGFydCB8fCBzdGFydCA8IDApIHN0YXJ0ID0gMFxuICBpZiAoIWVuZCB8fCBlbmQgPCAwIHx8IGVuZCA+IGxlbikgZW5kID0gbGVuXG5cbiAgdmFyIG91dCA9ICcnXG4gIGZvciAodmFyIGkgPSBzdGFydDsgaSA8IGVuZDsgKytpKSB7XG4gICAgb3V0ICs9IHRvSGV4KGJ1ZltpXSlcbiAgfVxuICByZXR1cm4gb3V0XG59XG5cbmZ1bmN0aW9uIHV0ZjE2bGVTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIHZhciBieXRlcyA9IGJ1Zi5zbGljZShzdGFydCwgZW5kKVxuICB2YXIgcmVzID0gJydcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBieXRlcy5sZW5ndGg7IGkgKz0gMikge1xuICAgIHJlcyArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGJ5dGVzW2ldICsgYnl0ZXNbaSArIDFdICogMjU2KVxuICB9XG4gIHJldHVybiByZXNcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5zbGljZSA9IGZ1bmN0aW9uIHNsaWNlIChzdGFydCwgZW5kKSB7XG4gIHZhciBsZW4gPSB0aGlzLmxlbmd0aFxuICBzdGFydCA9IH5+c3RhcnRcbiAgZW5kID0gZW5kID09PSB1bmRlZmluZWQgPyBsZW4gOiB+fmVuZFxuXG4gIGlmIChzdGFydCA8IDApIHtcbiAgICBzdGFydCArPSBsZW5cbiAgICBpZiAoc3RhcnQgPCAwKSBzdGFydCA9IDBcbiAgfSBlbHNlIGlmIChzdGFydCA+IGxlbikge1xuICAgIHN0YXJ0ID0gbGVuXG4gIH1cblxuICBpZiAoZW5kIDwgMCkge1xuICAgIGVuZCArPSBsZW5cbiAgICBpZiAoZW5kIDwgMCkgZW5kID0gMFxuICB9IGVsc2UgaWYgKGVuZCA+IGxlbikge1xuICAgIGVuZCA9IGxlblxuICB9XG5cbiAgaWYgKGVuZCA8IHN0YXJ0KSBlbmQgPSBzdGFydFxuXG4gIHZhciBuZXdCdWZcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgbmV3QnVmID0gdGhpcy5zdWJhcnJheShzdGFydCwgZW5kKVxuICAgIG5ld0J1Zi5fX3Byb3RvX18gPSBCdWZmZXIucHJvdG90eXBlXG4gIH0gZWxzZSB7XG4gICAgdmFyIHNsaWNlTGVuID0gZW5kIC0gc3RhcnRcbiAgICBuZXdCdWYgPSBuZXcgQnVmZmVyKHNsaWNlTGVuLCB1bmRlZmluZWQpXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzbGljZUxlbjsgKytpKSB7XG4gICAgICBuZXdCdWZbaV0gPSB0aGlzW2kgKyBzdGFydF1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gbmV3QnVmXG59XG5cbi8qXG4gKiBOZWVkIHRvIG1ha2Ugc3VyZSB0aGF0IGJ1ZmZlciBpc24ndCB0cnlpbmcgdG8gd3JpdGUgb3V0IG9mIGJvdW5kcy5cbiAqL1xuZnVuY3Rpb24gY2hlY2tPZmZzZXQgKG9mZnNldCwgZXh0LCBsZW5ndGgpIHtcbiAgaWYgKChvZmZzZXQgJSAxKSAhPT0gMCB8fCBvZmZzZXQgPCAwKSB0aHJvdyBuZXcgUmFuZ2VFcnJvcignb2Zmc2V0IGlzIG5vdCB1aW50JylcbiAgaWYgKG9mZnNldCArIGV4dCA+IGxlbmd0aCkgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1RyeWluZyB0byBhY2Nlc3MgYmV5b25kIGJ1ZmZlciBsZW5ndGgnKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRVSW50TEUgPSBmdW5jdGlvbiByZWFkVUludExFIChvZmZzZXQsIGJ5dGVMZW5ndGgsIG5vQXNzZXJ0KSB7XG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGggfCAwXG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrT2Zmc2V0KG9mZnNldCwgYnl0ZUxlbmd0aCwgdGhpcy5sZW5ndGgpXG5cbiAgdmFyIHZhbCA9IHRoaXNbb2Zmc2V0XVxuICB2YXIgbXVsID0gMVxuICB2YXIgaSA9IDBcbiAgd2hpbGUgKCsraSA8IGJ5dGVMZW5ndGggJiYgKG11bCAqPSAweDEwMCkpIHtcbiAgICB2YWwgKz0gdGhpc1tvZmZzZXQgKyBpXSAqIG11bFxuICB9XG5cbiAgcmV0dXJuIHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRVSW50QkUgPSBmdW5jdGlvbiByZWFkVUludEJFIChvZmZzZXQsIGJ5dGVMZW5ndGgsIG5vQXNzZXJ0KSB7XG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGggfCAwXG4gIGlmICghbm9Bc3NlcnQpIHtcbiAgICBjaGVja09mZnNldChvZmZzZXQsIGJ5dGVMZW5ndGgsIHRoaXMubGVuZ3RoKVxuICB9XG5cbiAgdmFyIHZhbCA9IHRoaXNbb2Zmc2V0ICsgLS1ieXRlTGVuZ3RoXVxuICB2YXIgbXVsID0gMVxuICB3aGlsZSAoYnl0ZUxlbmd0aCA+IDAgJiYgKG11bCAqPSAweDEwMCkpIHtcbiAgICB2YWwgKz0gdGhpc1tvZmZzZXQgKyAtLWJ5dGVMZW5ndGhdICogbXVsXG4gIH1cblxuICByZXR1cm4gdmFsXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUucmVhZFVJbnQ4ID0gZnVuY3Rpb24gcmVhZFVJbnQ4IChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrT2Zmc2V0KG9mZnNldCwgMSwgdGhpcy5sZW5ndGgpXG4gIHJldHVybiB0aGlzW29mZnNldF1cbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkVUludDE2TEUgPSBmdW5jdGlvbiByZWFkVUludDE2TEUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCAyLCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIHRoaXNbb2Zmc2V0XSB8ICh0aGlzW29mZnNldCArIDFdIDw8IDgpXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUucmVhZFVJbnQxNkJFID0gZnVuY3Rpb24gcmVhZFVJbnQxNkJFIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrT2Zmc2V0KG9mZnNldCwgMiwgdGhpcy5sZW5ndGgpXG4gIHJldHVybiAodGhpc1tvZmZzZXRdIDw8IDgpIHwgdGhpc1tvZmZzZXQgKyAxXVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRVSW50MzJMRSA9IGZ1bmN0aW9uIHJlYWRVSW50MzJMRSAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIDQsIHRoaXMubGVuZ3RoKVxuXG4gIHJldHVybiAoKHRoaXNbb2Zmc2V0XSkgfFxuICAgICAgKHRoaXNbb2Zmc2V0ICsgMV0gPDwgOCkgfFxuICAgICAgKHRoaXNbb2Zmc2V0ICsgMl0gPDwgMTYpKSArXG4gICAgICAodGhpc1tvZmZzZXQgKyAzXSAqIDB4MTAwMDAwMClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkVUludDMyQkUgPSBmdW5jdGlvbiByZWFkVUludDMyQkUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcblxuICByZXR1cm4gKHRoaXNbb2Zmc2V0XSAqIDB4MTAwMDAwMCkgK1xuICAgICgodGhpc1tvZmZzZXQgKyAxXSA8PCAxNikgfFxuICAgICh0aGlzW29mZnNldCArIDJdIDw8IDgpIHxcbiAgICB0aGlzW29mZnNldCArIDNdKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnRMRSA9IGZ1bmN0aW9uIHJlYWRJbnRMRSAob2Zmc2V0LCBieXRlTGVuZ3RoLCBub0Fzc2VydCkge1xuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGJ5dGVMZW5ndGggPSBieXRlTGVuZ3RoIHwgMFxuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIGJ5dGVMZW5ndGgsIHRoaXMubGVuZ3RoKVxuXG4gIHZhciB2YWwgPSB0aGlzW29mZnNldF1cbiAgdmFyIG11bCA9IDFcbiAgdmFyIGkgPSAwXG4gIHdoaWxlICgrK2kgPCBieXRlTGVuZ3RoICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgdmFsICs9IHRoaXNbb2Zmc2V0ICsgaV0gKiBtdWxcbiAgfVxuICBtdWwgKj0gMHg4MFxuXG4gIGlmICh2YWwgPj0gbXVsKSB2YWwgLT0gTWF0aC5wb3coMiwgOCAqIGJ5dGVMZW5ndGgpXG5cbiAgcmV0dXJuIHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnRCRSA9IGZ1bmN0aW9uIHJlYWRJbnRCRSAob2Zmc2V0LCBieXRlTGVuZ3RoLCBub0Fzc2VydCkge1xuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGJ5dGVMZW5ndGggPSBieXRlTGVuZ3RoIHwgMFxuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIGJ5dGVMZW5ndGgsIHRoaXMubGVuZ3RoKVxuXG4gIHZhciBpID0gYnl0ZUxlbmd0aFxuICB2YXIgbXVsID0gMVxuICB2YXIgdmFsID0gdGhpc1tvZmZzZXQgKyAtLWldXG4gIHdoaWxlIChpID4gMCAmJiAobXVsICo9IDB4MTAwKSkge1xuICAgIHZhbCArPSB0aGlzW29mZnNldCArIC0taV0gKiBtdWxcbiAgfVxuICBtdWwgKj0gMHg4MFxuXG4gIGlmICh2YWwgPj0gbXVsKSB2YWwgLT0gTWF0aC5wb3coMiwgOCAqIGJ5dGVMZW5ndGgpXG5cbiAgcmV0dXJuIHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQ4ID0gZnVuY3Rpb24gcmVhZEludDggKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCAxLCB0aGlzLmxlbmd0aClcbiAgaWYgKCEodGhpc1tvZmZzZXRdICYgMHg4MCkpIHJldHVybiAodGhpc1tvZmZzZXRdKVxuICByZXR1cm4gKCgweGZmIC0gdGhpc1tvZmZzZXRdICsgMSkgKiAtMSlcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkSW50MTZMRSA9IGZ1bmN0aW9uIHJlYWRJbnQxNkxFIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrT2Zmc2V0KG9mZnNldCwgMiwgdGhpcy5sZW5ndGgpXG4gIHZhciB2YWwgPSB0aGlzW29mZnNldF0gfCAodGhpc1tvZmZzZXQgKyAxXSA8PCA4KVxuICByZXR1cm4gKHZhbCAmIDB4ODAwMCkgPyB2YWwgfCAweEZGRkYwMDAwIDogdmFsXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUucmVhZEludDE2QkUgPSBmdW5jdGlvbiByZWFkSW50MTZCRSAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIDIsIHRoaXMubGVuZ3RoKVxuICB2YXIgdmFsID0gdGhpc1tvZmZzZXQgKyAxXSB8ICh0aGlzW29mZnNldF0gPDwgOClcbiAgcmV0dXJuICh2YWwgJiAweDgwMDApID8gdmFsIHwgMHhGRkZGMDAwMCA6IHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQzMkxFID0gZnVuY3Rpb24gcmVhZEludDMyTEUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcblxuICByZXR1cm4gKHRoaXNbb2Zmc2V0XSkgfFxuICAgICh0aGlzW29mZnNldCArIDFdIDw8IDgpIHxcbiAgICAodGhpc1tvZmZzZXQgKyAyXSA8PCAxNikgfFxuICAgICh0aGlzW29mZnNldCArIDNdIDw8IDI0KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQzMkJFID0gZnVuY3Rpb24gcmVhZEludDMyQkUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcblxuICByZXR1cm4gKHRoaXNbb2Zmc2V0XSA8PCAyNCkgfFxuICAgICh0aGlzW29mZnNldCArIDFdIDw8IDE2KSB8XG4gICAgKHRoaXNbb2Zmc2V0ICsgMl0gPDwgOCkgfFxuICAgICh0aGlzW29mZnNldCArIDNdKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRGbG9hdExFID0gZnVuY3Rpb24gcmVhZEZsb2F0TEUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIHRydWUsIDIzLCA0KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRGbG9hdEJFID0gZnVuY3Rpb24gcmVhZEZsb2F0QkUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIGZhbHNlLCAyMywgNClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkRG91YmxlTEUgPSBmdW5jdGlvbiByZWFkRG91YmxlTEUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA4LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIHRydWUsIDUyLCA4KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWREb3VibGVCRSA9IGZ1bmN0aW9uIHJlYWREb3VibGVCRSAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIDgsIHRoaXMubGVuZ3RoKVxuICByZXR1cm4gaWVlZTc1NC5yZWFkKHRoaXMsIG9mZnNldCwgZmFsc2UsIDUyLCA4KVxufVxuXG5mdW5jdGlvbiBjaGVja0ludCAoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBleHQsIG1heCwgbWluKSB7XG4gIGlmICghQnVmZmVyLmlzQnVmZmVyKGJ1ZikpIHRocm93IG5ldyBUeXBlRXJyb3IoJ1wiYnVmZmVyXCIgYXJndW1lbnQgbXVzdCBiZSBhIEJ1ZmZlciBpbnN0YW5jZScpXG4gIGlmICh2YWx1ZSA+IG1heCB8fCB2YWx1ZSA8IG1pbikgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1widmFsdWVcIiBhcmd1bWVudCBpcyBvdXQgb2YgYm91bmRzJylcbiAgaWYgKG9mZnNldCArIGV4dCA+IGJ1Zi5sZW5ndGgpIHRocm93IG5ldyBSYW5nZUVycm9yKCdJbmRleCBvdXQgb2YgcmFuZ2UnKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludExFID0gZnVuY3Rpb24gd3JpdGVVSW50TEUgKHZhbHVlLCBvZmZzZXQsIGJ5dGVMZW5ndGgsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGggfCAwXG4gIGlmICghbm9Bc3NlcnQpIHtcbiAgICB2YXIgbWF4Qnl0ZXMgPSBNYXRoLnBvdygyLCA4ICogYnl0ZUxlbmd0aCkgLSAxXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbWF4Qnl0ZXMsIDApXG4gIH1cblxuICB2YXIgbXVsID0gMVxuICB2YXIgaSA9IDBcbiAgdGhpc1tvZmZzZXRdID0gdmFsdWUgJiAweEZGXG4gIHdoaWxlICgrK2kgPCBieXRlTGVuZ3RoICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgdGhpc1tvZmZzZXQgKyBpXSA9ICh2YWx1ZSAvIG11bCkgJiAweEZGXG4gIH1cblxuICByZXR1cm4gb2Zmc2V0ICsgYnl0ZUxlbmd0aFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludEJFID0gZnVuY3Rpb24gd3JpdGVVSW50QkUgKHZhbHVlLCBvZmZzZXQsIGJ5dGVMZW5ndGgsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGggfCAwXG4gIGlmICghbm9Bc3NlcnQpIHtcbiAgICB2YXIgbWF4Qnl0ZXMgPSBNYXRoLnBvdygyLCA4ICogYnl0ZUxlbmd0aCkgLSAxXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbWF4Qnl0ZXMsIDApXG4gIH1cblxuICB2YXIgaSA9IGJ5dGVMZW5ndGggLSAxXG4gIHZhciBtdWwgPSAxXG4gIHRoaXNbb2Zmc2V0ICsgaV0gPSB2YWx1ZSAmIDB4RkZcbiAgd2hpbGUgKC0taSA+PSAwICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgdGhpc1tvZmZzZXQgKyBpXSA9ICh2YWx1ZSAvIG11bCkgJiAweEZGXG4gIH1cblxuICByZXR1cm4gb2Zmc2V0ICsgYnl0ZUxlbmd0aFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludDggPSBmdW5jdGlvbiB3cml0ZVVJbnQ4ICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDEsIDB4ZmYsIDApXG4gIGlmICghQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHZhbHVlID0gTWF0aC5mbG9vcih2YWx1ZSlcbiAgdGhpc1tvZmZzZXRdID0gKHZhbHVlICYgMHhmZilcbiAgcmV0dXJuIG9mZnNldCArIDFcbn1cblxuZnVuY3Rpb24gb2JqZWN0V3JpdGVVSW50MTYgKGJ1ZiwgdmFsdWUsIG9mZnNldCwgbGl0dGxlRW5kaWFuKSB7XG4gIGlmICh2YWx1ZSA8IDApIHZhbHVlID0gMHhmZmZmICsgdmFsdWUgKyAxXG4gIGZvciAodmFyIGkgPSAwLCBqID0gTWF0aC5taW4oYnVmLmxlbmd0aCAtIG9mZnNldCwgMik7IGkgPCBqOyArK2kpIHtcbiAgICBidWZbb2Zmc2V0ICsgaV0gPSAodmFsdWUgJiAoMHhmZiA8PCAoOCAqIChsaXR0bGVFbmRpYW4gPyBpIDogMSAtIGkpKSkpID4+PlxuICAgICAgKGxpdHRsZUVuZGlhbiA/IGkgOiAxIC0gaSkgKiA4XG4gIH1cbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZVVJbnQxNkxFID0gZnVuY3Rpb24gd3JpdGVVSW50MTZMRSAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0IHwgMFxuICBpZiAoIW5vQXNzZXJ0KSBjaGVja0ludCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCAyLCAweGZmZmYsIDApXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIHRoaXNbb2Zmc2V0XSA9ICh2YWx1ZSAmIDB4ZmYpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSA+Pj4gOClcbiAgfSBlbHNlIHtcbiAgICBvYmplY3RXcml0ZVVJbnQxNih0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlKVxuICB9XG4gIHJldHVybiBvZmZzZXQgKyAyXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVVSW50MTZCRSA9IGZ1bmN0aW9uIHdyaXRlVUludDE2QkUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMiwgMHhmZmZmLCAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgPj4+IDgpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSAmIDB4ZmYpXG4gIH0gZWxzZSB7XG4gICAgb2JqZWN0V3JpdGVVSW50MTYodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDJcbn1cblxuZnVuY3Rpb24gb2JqZWN0V3JpdGVVSW50MzIgKGJ1ZiwgdmFsdWUsIG9mZnNldCwgbGl0dGxlRW5kaWFuKSB7XG4gIGlmICh2YWx1ZSA8IDApIHZhbHVlID0gMHhmZmZmZmZmZiArIHZhbHVlICsgMVxuICBmb3IgKHZhciBpID0gMCwgaiA9IE1hdGgubWluKGJ1Zi5sZW5ndGggLSBvZmZzZXQsIDQpOyBpIDwgajsgKytpKSB7XG4gICAgYnVmW29mZnNldCArIGldID0gKHZhbHVlID4+PiAobGl0dGxlRW5kaWFuID8gaSA6IDMgLSBpKSAqIDgpICYgMHhmZlxuICB9XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVVSW50MzJMRSA9IGZ1bmN0aW9uIHdyaXRlVUludDMyTEUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgNCwgMHhmZmZmZmZmZiwgMClcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgdGhpc1tvZmZzZXQgKyAzXSA9ICh2YWx1ZSA+Pj4gMjQpXG4gICAgdGhpc1tvZmZzZXQgKyAyXSA9ICh2YWx1ZSA+Pj4gMTYpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSA+Pj4gOClcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgJiAweGZmKVxuICB9IGVsc2Uge1xuICAgIG9iamVjdFdyaXRlVUludDMyKHRoaXMsIHZhbHVlLCBvZmZzZXQsIHRydWUpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDRcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZVVJbnQzMkJFID0gZnVuY3Rpb24gd3JpdGVVSW50MzJCRSAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0IHwgMFxuICBpZiAoIW5vQXNzZXJ0KSBjaGVja0ludCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCA0LCAweGZmZmZmZmZmLCAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgPj4+IDI0KVxuICAgIHRoaXNbb2Zmc2V0ICsgMV0gPSAodmFsdWUgPj4+IDE2KVxuICAgIHRoaXNbb2Zmc2V0ICsgMl0gPSAodmFsdWUgPj4+IDgpXG4gICAgdGhpc1tvZmZzZXQgKyAzXSA9ICh2YWx1ZSAmIDB4ZmYpXG4gIH0gZWxzZSB7XG4gICAgb2JqZWN0V3JpdGVVSW50MzIodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDRcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludExFID0gZnVuY3Rpb24gd3JpdGVJbnRMRSAodmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0IHwgMFxuICBpZiAoIW5vQXNzZXJ0KSB7XG4gICAgdmFyIGxpbWl0ID0gTWF0aC5wb3coMiwgOCAqIGJ5dGVMZW5ndGggLSAxKVxuXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbGltaXQgLSAxLCAtbGltaXQpXG4gIH1cblxuICB2YXIgaSA9IDBcbiAgdmFyIG11bCA9IDFcbiAgdmFyIHN1YiA9IDBcbiAgdGhpc1tvZmZzZXRdID0gdmFsdWUgJiAweEZGXG4gIHdoaWxlICgrK2kgPCBieXRlTGVuZ3RoICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgaWYgKHZhbHVlIDwgMCAmJiBzdWIgPT09IDAgJiYgdGhpc1tvZmZzZXQgKyBpIC0gMV0gIT09IDApIHtcbiAgICAgIHN1YiA9IDFcbiAgICB9XG4gICAgdGhpc1tvZmZzZXQgKyBpXSA9ICgodmFsdWUgLyBtdWwpID4+IDApIC0gc3ViICYgMHhGRlxuICB9XG5cbiAgcmV0dXJuIG9mZnNldCArIGJ5dGVMZW5ndGhcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludEJFID0gZnVuY3Rpb24gd3JpdGVJbnRCRSAodmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0IHwgMFxuICBpZiAoIW5vQXNzZXJ0KSB7XG4gICAgdmFyIGxpbWl0ID0gTWF0aC5wb3coMiwgOCAqIGJ5dGVMZW5ndGggLSAxKVxuXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbGltaXQgLSAxLCAtbGltaXQpXG4gIH1cblxuICB2YXIgaSA9IGJ5dGVMZW5ndGggLSAxXG4gIHZhciBtdWwgPSAxXG4gIHZhciBzdWIgPSAwXG4gIHRoaXNbb2Zmc2V0ICsgaV0gPSB2YWx1ZSAmIDB4RkZcbiAgd2hpbGUgKC0taSA+PSAwICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgaWYgKHZhbHVlIDwgMCAmJiBzdWIgPT09IDAgJiYgdGhpc1tvZmZzZXQgKyBpICsgMV0gIT09IDApIHtcbiAgICAgIHN1YiA9IDFcbiAgICB9XG4gICAgdGhpc1tvZmZzZXQgKyBpXSA9ICgodmFsdWUgLyBtdWwpID4+IDApIC0gc3ViICYgMHhGRlxuICB9XG5cbiAgcmV0dXJuIG9mZnNldCArIGJ5dGVMZW5ndGhcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludDggPSBmdW5jdGlvbiB3cml0ZUludDggKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMSwgMHg3ZiwgLTB4ODApXG4gIGlmICghQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHZhbHVlID0gTWF0aC5mbG9vcih2YWx1ZSlcbiAgaWYgKHZhbHVlIDwgMCkgdmFsdWUgPSAweGZmICsgdmFsdWUgKyAxXG4gIHRoaXNbb2Zmc2V0XSA9ICh2YWx1ZSAmIDB4ZmYpXG4gIHJldHVybiBvZmZzZXQgKyAxXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVJbnQxNkxFID0gZnVuY3Rpb24gd3JpdGVJbnQxNkxFICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDIsIDB4N2ZmZiwgLTB4ODAwMClcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgdGhpc1tvZmZzZXRdID0gKHZhbHVlICYgMHhmZilcbiAgICB0aGlzW29mZnNldCArIDFdID0gKHZhbHVlID4+PiA4KVxuICB9IGVsc2Uge1xuICAgIG9iamVjdFdyaXRlVUludDE2KHRoaXMsIHZhbHVlLCBvZmZzZXQsIHRydWUpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDJcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludDE2QkUgPSBmdW5jdGlvbiB3cml0ZUludDE2QkUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMiwgMHg3ZmZmLCAtMHg4MDAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgPj4+IDgpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSAmIDB4ZmYpXG4gIH0gZWxzZSB7XG4gICAgb2JqZWN0V3JpdGVVSW50MTYodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDJcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludDMyTEUgPSBmdW5jdGlvbiB3cml0ZUludDMyTEUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgNCwgMHg3ZmZmZmZmZiwgLTB4ODAwMDAwMDApXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIHRoaXNbb2Zmc2V0XSA9ICh2YWx1ZSAmIDB4ZmYpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSA+Pj4gOClcbiAgICB0aGlzW29mZnNldCArIDJdID0gKHZhbHVlID4+PiAxNilcbiAgICB0aGlzW29mZnNldCArIDNdID0gKHZhbHVlID4+PiAyNClcbiAgfSBlbHNlIHtcbiAgICBvYmplY3RXcml0ZVVJbnQzMih0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlKVxuICB9XG4gIHJldHVybiBvZmZzZXQgKyA0XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVJbnQzMkJFID0gZnVuY3Rpb24gd3JpdGVJbnQzMkJFICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDQsIDB4N2ZmZmZmZmYsIC0weDgwMDAwMDAwKVxuICBpZiAodmFsdWUgPCAwKSB2YWx1ZSA9IDB4ZmZmZmZmZmYgKyB2YWx1ZSArIDFcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgdGhpc1tvZmZzZXRdID0gKHZhbHVlID4+PiAyNClcbiAgICB0aGlzW29mZnNldCArIDFdID0gKHZhbHVlID4+PiAxNilcbiAgICB0aGlzW29mZnNldCArIDJdID0gKHZhbHVlID4+PiA4KVxuICAgIHRoaXNbb2Zmc2V0ICsgM10gPSAodmFsdWUgJiAweGZmKVxuICB9IGVsc2Uge1xuICAgIG9iamVjdFdyaXRlVUludDMyKHRoaXMsIHZhbHVlLCBvZmZzZXQsIGZhbHNlKVxuICB9XG4gIHJldHVybiBvZmZzZXQgKyA0XG59XG5cbmZ1bmN0aW9uIGNoZWNrSUVFRTc1NCAoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBleHQsIG1heCwgbWluKSB7XG4gIGlmIChvZmZzZXQgKyBleHQgPiBidWYubGVuZ3RoKSB0aHJvdyBuZXcgUmFuZ2VFcnJvcignSW5kZXggb3V0IG9mIHJhbmdlJylcbiAgaWYgKG9mZnNldCA8IDApIHRocm93IG5ldyBSYW5nZUVycm9yKCdJbmRleCBvdXQgb2YgcmFuZ2UnKVxufVxuXG5mdW5jdGlvbiB3cml0ZUZsb2F0IChidWYsIHZhbHVlLCBvZmZzZXQsIGxpdHRsZUVuZGlhbiwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkge1xuICAgIGNoZWNrSUVFRTc1NChidWYsIHZhbHVlLCBvZmZzZXQsIDQsIDMuNDAyODIzNDY2Mzg1Mjg4NmUrMzgsIC0zLjQwMjgyMzQ2NjM4NTI4ODZlKzM4KVxuICB9XG4gIGllZWU3NTQud3JpdGUoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBsaXR0bGVFbmRpYW4sIDIzLCA0KVxuICByZXR1cm4gb2Zmc2V0ICsgNFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlRmxvYXRMRSA9IGZ1bmN0aW9uIHdyaXRlRmxvYXRMRSAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgcmV0dXJuIHdyaXRlRmxvYXQodGhpcywgdmFsdWUsIG9mZnNldCwgdHJ1ZSwgbm9Bc3NlcnQpXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVGbG9hdEJFID0gZnVuY3Rpb24gd3JpdGVGbG9hdEJFICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICByZXR1cm4gd3JpdGVGbG9hdCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCBmYWxzZSwgbm9Bc3NlcnQpXG59XG5cbmZ1bmN0aW9uIHdyaXRlRG91YmxlIChidWYsIHZhbHVlLCBvZmZzZXQsIGxpdHRsZUVuZGlhbiwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkge1xuICAgIGNoZWNrSUVFRTc1NChidWYsIHZhbHVlLCBvZmZzZXQsIDgsIDEuNzk3NjkzMTM0ODYyMzE1N0UrMzA4LCAtMS43OTc2OTMxMzQ4NjIzMTU3RSszMDgpXG4gIH1cbiAgaWVlZTc1NC53cml0ZShidWYsIHZhbHVlLCBvZmZzZXQsIGxpdHRsZUVuZGlhbiwgNTIsIDgpXG4gIHJldHVybiBvZmZzZXQgKyA4XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVEb3VibGVMRSA9IGZ1bmN0aW9uIHdyaXRlRG91YmxlTEUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHJldHVybiB3cml0ZURvdWJsZSh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlLCBub0Fzc2VydClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZURvdWJsZUJFID0gZnVuY3Rpb24gd3JpdGVEb3VibGVCRSAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgcmV0dXJuIHdyaXRlRG91YmxlKHRoaXMsIHZhbHVlLCBvZmZzZXQsIGZhbHNlLCBub0Fzc2VydClcbn1cblxuLy8gY29weSh0YXJnZXRCdWZmZXIsIHRhcmdldFN0YXJ0PTAsIHNvdXJjZVN0YXJ0PTAsIHNvdXJjZUVuZD1idWZmZXIubGVuZ3RoKVxuQnVmZmVyLnByb3RvdHlwZS5jb3B5ID0gZnVuY3Rpb24gY29weSAodGFyZ2V0LCB0YXJnZXRTdGFydCwgc3RhcnQsIGVuZCkge1xuICBpZiAoIXN0YXJ0KSBzdGFydCA9IDBcbiAgaWYgKCFlbmQgJiYgZW5kICE9PSAwKSBlbmQgPSB0aGlzLmxlbmd0aFxuICBpZiAodGFyZ2V0U3RhcnQgPj0gdGFyZ2V0Lmxlbmd0aCkgdGFyZ2V0U3RhcnQgPSB0YXJnZXQubGVuZ3RoXG4gIGlmICghdGFyZ2V0U3RhcnQpIHRhcmdldFN0YXJ0ID0gMFxuICBpZiAoZW5kID4gMCAmJiBlbmQgPCBzdGFydCkgZW5kID0gc3RhcnRcblxuICAvLyBDb3B5IDAgYnl0ZXM7IHdlJ3JlIGRvbmVcbiAgaWYgKGVuZCA9PT0gc3RhcnQpIHJldHVybiAwXG4gIGlmICh0YXJnZXQubGVuZ3RoID09PSAwIHx8IHRoaXMubGVuZ3RoID09PSAwKSByZXR1cm4gMFxuXG4gIC8vIEZhdGFsIGVycm9yIGNvbmRpdGlvbnNcbiAgaWYgKHRhcmdldFN0YXJ0IDwgMCkge1xuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCd0YXJnZXRTdGFydCBvdXQgb2YgYm91bmRzJylcbiAgfVxuICBpZiAoc3RhcnQgPCAwIHx8IHN0YXJ0ID49IHRoaXMubGVuZ3RoKSB0aHJvdyBuZXcgUmFuZ2VFcnJvcignc291cmNlU3RhcnQgb3V0IG9mIGJvdW5kcycpXG4gIGlmIChlbmQgPCAwKSB0aHJvdyBuZXcgUmFuZ2VFcnJvcignc291cmNlRW5kIG91dCBvZiBib3VuZHMnKVxuXG4gIC8vIEFyZSB3ZSBvb2I/XG4gIGlmIChlbmQgPiB0aGlzLmxlbmd0aCkgZW5kID0gdGhpcy5sZW5ndGhcbiAgaWYgKHRhcmdldC5sZW5ndGggLSB0YXJnZXRTdGFydCA8IGVuZCAtIHN0YXJ0KSB7XG4gICAgZW5kID0gdGFyZ2V0Lmxlbmd0aCAtIHRhcmdldFN0YXJ0ICsgc3RhcnRcbiAgfVxuXG4gIHZhciBsZW4gPSBlbmQgLSBzdGFydFxuICB2YXIgaVxuXG4gIGlmICh0aGlzID09PSB0YXJnZXQgJiYgc3RhcnQgPCB0YXJnZXRTdGFydCAmJiB0YXJnZXRTdGFydCA8IGVuZCkge1xuICAgIC8vIGRlc2NlbmRpbmcgY29weSBmcm9tIGVuZFxuICAgIGZvciAoaSA9IGxlbiAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICB0YXJnZXRbaSArIHRhcmdldFN0YXJ0XSA9IHRoaXNbaSArIHN0YXJ0XVxuICAgIH1cbiAgfSBlbHNlIGlmIChsZW4gPCAxMDAwIHx8ICFCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIC8vIGFzY2VuZGluZyBjb3B5IGZyb20gc3RhcnRcbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyArK2kpIHtcbiAgICAgIHRhcmdldFtpICsgdGFyZ2V0U3RhcnRdID0gdGhpc1tpICsgc3RhcnRdXG4gICAgfVxuICB9IGVsc2Uge1xuICAgIFVpbnQ4QXJyYXkucHJvdG90eXBlLnNldC5jYWxsKFxuICAgICAgdGFyZ2V0LFxuICAgICAgdGhpcy5zdWJhcnJheShzdGFydCwgc3RhcnQgKyBsZW4pLFxuICAgICAgdGFyZ2V0U3RhcnRcbiAgICApXG4gIH1cblxuICByZXR1cm4gbGVuXG59XG5cbi8vIFVzYWdlOlxuLy8gICAgYnVmZmVyLmZpbGwobnVtYmVyWywgb2Zmc2V0WywgZW5kXV0pXG4vLyAgICBidWZmZXIuZmlsbChidWZmZXJbLCBvZmZzZXRbLCBlbmRdXSlcbi8vICAgIGJ1ZmZlci5maWxsKHN0cmluZ1ssIG9mZnNldFssIGVuZF1dWywgZW5jb2RpbmddKVxuQnVmZmVyLnByb3RvdHlwZS5maWxsID0gZnVuY3Rpb24gZmlsbCAodmFsLCBzdGFydCwgZW5kLCBlbmNvZGluZykge1xuICAvLyBIYW5kbGUgc3RyaW5nIGNhc2VzOlxuICBpZiAodHlwZW9mIHZhbCA9PT0gJ3N0cmluZycpIHtcbiAgICBpZiAodHlwZW9mIHN0YXJ0ID09PSAnc3RyaW5nJykge1xuICAgICAgZW5jb2RpbmcgPSBzdGFydFxuICAgICAgc3RhcnQgPSAwXG4gICAgICBlbmQgPSB0aGlzLmxlbmd0aFxuICAgIH0gZWxzZSBpZiAodHlwZW9mIGVuZCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGVuY29kaW5nID0gZW5kXG4gICAgICBlbmQgPSB0aGlzLmxlbmd0aFxuICAgIH1cbiAgICBpZiAodmFsLmxlbmd0aCA9PT0gMSkge1xuICAgICAgdmFyIGNvZGUgPSB2YWwuY2hhckNvZGVBdCgwKVxuICAgICAgaWYgKGNvZGUgPCAyNTYpIHtcbiAgICAgICAgdmFsID0gY29kZVxuICAgICAgfVxuICAgIH1cbiAgICBpZiAoZW5jb2RpbmcgIT09IHVuZGVmaW5lZCAmJiB0eXBlb2YgZW5jb2RpbmcgIT09ICdzdHJpbmcnKSB7XG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdlbmNvZGluZyBtdXN0IGJlIGEgc3RyaW5nJylcbiAgICB9XG4gICAgaWYgKHR5cGVvZiBlbmNvZGluZyA9PT0gJ3N0cmluZycgJiYgIUJ1ZmZlci5pc0VuY29kaW5nKGVuY29kaW5nKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignVW5rbm93biBlbmNvZGluZzogJyArIGVuY29kaW5nKVxuICAgIH1cbiAgfSBlbHNlIGlmICh0eXBlb2YgdmFsID09PSAnbnVtYmVyJykge1xuICAgIHZhbCA9IHZhbCAmIDI1NVxuICB9XG5cbiAgLy8gSW52YWxpZCByYW5nZXMgYXJlIG5vdCBzZXQgdG8gYSBkZWZhdWx0LCBzbyBjYW4gcmFuZ2UgY2hlY2sgZWFybHkuXG4gIGlmIChzdGFydCA8IDAgfHwgdGhpcy5sZW5ndGggPCBzdGFydCB8fCB0aGlzLmxlbmd0aCA8IGVuZCkge1xuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdPdXQgb2YgcmFuZ2UgaW5kZXgnKVxuICB9XG5cbiAgaWYgKGVuZCA8PSBzdGFydCkge1xuICAgIHJldHVybiB0aGlzXG4gIH1cblxuICBzdGFydCA9IHN0YXJ0ID4+PiAwXG4gIGVuZCA9IGVuZCA9PT0gdW5kZWZpbmVkID8gdGhpcy5sZW5ndGggOiBlbmQgPj4+IDBcblxuICBpZiAoIXZhbCkgdmFsID0gMFxuXG4gIHZhciBpXG4gIGlmICh0eXBlb2YgdmFsID09PSAnbnVtYmVyJykge1xuICAgIGZvciAoaSA9IHN0YXJ0OyBpIDwgZW5kOyArK2kpIHtcbiAgICAgIHRoaXNbaV0gPSB2YWxcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgdmFyIGJ5dGVzID0gQnVmZmVyLmlzQnVmZmVyKHZhbClcbiAgICAgID8gdmFsXG4gICAgICA6IHV0ZjhUb0J5dGVzKG5ldyBCdWZmZXIodmFsLCBlbmNvZGluZykudG9TdHJpbmcoKSlcbiAgICB2YXIgbGVuID0gYnl0ZXMubGVuZ3RoXG4gICAgZm9yIChpID0gMDsgaSA8IGVuZCAtIHN0YXJ0OyArK2kpIHtcbiAgICAgIHRoaXNbaSArIHN0YXJ0XSA9IGJ5dGVzW2kgJSBsZW5dXG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRoaXNcbn1cblxuLy8gSEVMUEVSIEZVTkNUSU9OU1xuLy8gPT09PT09PT09PT09PT09PVxuXG52YXIgSU5WQUxJRF9CQVNFNjRfUkUgPSAvW14rXFwvMC05QS1aYS16LV9dL2dcblxuZnVuY3Rpb24gYmFzZTY0Y2xlYW4gKHN0cikge1xuICAvLyBOb2RlIHN0cmlwcyBvdXQgaW52YWxpZCBjaGFyYWN0ZXJzIGxpa2UgXFxuIGFuZCBcXHQgZnJvbSB0aGUgc3RyaW5nLCBiYXNlNjQtanMgZG9lcyBub3RcbiAgc3RyID0gc3RyaW5ndHJpbShzdHIpLnJlcGxhY2UoSU5WQUxJRF9CQVNFNjRfUkUsICcnKVxuICAvLyBOb2RlIGNvbnZlcnRzIHN0cmluZ3Mgd2l0aCBsZW5ndGggPCAyIHRvICcnXG4gIGlmIChzdHIubGVuZ3RoIDwgMikgcmV0dXJuICcnXG4gIC8vIE5vZGUgYWxsb3dzIGZvciBub24tcGFkZGVkIGJhc2U2NCBzdHJpbmdzIChtaXNzaW5nIHRyYWlsaW5nID09PSksIGJhc2U2NC1qcyBkb2VzIG5vdFxuICB3aGlsZSAoc3RyLmxlbmd0aCAlIDQgIT09IDApIHtcbiAgICBzdHIgPSBzdHIgKyAnPSdcbiAgfVxuICByZXR1cm4gc3RyXG59XG5cbmZ1bmN0aW9uIHN0cmluZ3RyaW0gKHN0cikge1xuICBpZiAoc3RyLnRyaW0pIHJldHVybiBzdHIudHJpbSgpXG4gIHJldHVybiBzdHIucmVwbGFjZSgvXlxccyt8XFxzKyQvZywgJycpXG59XG5cbmZ1bmN0aW9uIHRvSGV4IChuKSB7XG4gIGlmIChuIDwgMTYpIHJldHVybiAnMCcgKyBuLnRvU3RyaW5nKDE2KVxuICByZXR1cm4gbi50b1N0cmluZygxNilcbn1cblxuZnVuY3Rpb24gdXRmOFRvQnl0ZXMgKHN0cmluZywgdW5pdHMpIHtcbiAgdW5pdHMgPSB1bml0cyB8fCBJbmZpbml0eVxuICB2YXIgY29kZVBvaW50XG4gIHZhciBsZW5ndGggPSBzdHJpbmcubGVuZ3RoXG4gIHZhciBsZWFkU3Vycm9nYXRlID0gbnVsbFxuICB2YXIgYnl0ZXMgPSBbXVxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyArK2kpIHtcbiAgICBjb2RlUG9pbnQgPSBzdHJpbmcuY2hhckNvZGVBdChpKVxuXG4gICAgLy8gaXMgc3Vycm9nYXRlIGNvbXBvbmVudFxuICAgIGlmIChjb2RlUG9pbnQgPiAweEQ3RkYgJiYgY29kZVBvaW50IDwgMHhFMDAwKSB7XG4gICAgICAvLyBsYXN0IGNoYXIgd2FzIGEgbGVhZFxuICAgICAgaWYgKCFsZWFkU3Vycm9nYXRlKSB7XG4gICAgICAgIC8vIG5vIGxlYWQgeWV0XG4gICAgICAgIGlmIChjb2RlUG9pbnQgPiAweERCRkYpIHtcbiAgICAgICAgICAvLyB1bmV4cGVjdGVkIHRyYWlsXG4gICAgICAgICAgaWYgKCh1bml0cyAtPSAzKSA+IC0xKSBieXRlcy5wdXNoKDB4RUYsIDB4QkYsIDB4QkQpXG4gICAgICAgICAgY29udGludWVcbiAgICAgICAgfSBlbHNlIGlmIChpICsgMSA9PT0gbGVuZ3RoKSB7XG4gICAgICAgICAgLy8gdW5wYWlyZWQgbGVhZFxuICAgICAgICAgIGlmICgodW5pdHMgLT0gMykgPiAtMSkgYnl0ZXMucHVzaCgweEVGLCAweEJGLCAweEJEKVxuICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgIH1cblxuICAgICAgICAvLyB2YWxpZCBsZWFkXG4gICAgICAgIGxlYWRTdXJyb2dhdGUgPSBjb2RlUG9pbnRcblxuICAgICAgICBjb250aW51ZVxuICAgICAgfVxuXG4gICAgICAvLyAyIGxlYWRzIGluIGEgcm93XG4gICAgICBpZiAoY29kZVBvaW50IDwgMHhEQzAwKSB7XG4gICAgICAgIGlmICgodW5pdHMgLT0gMykgPiAtMSkgYnl0ZXMucHVzaCgweEVGLCAweEJGLCAweEJEKVxuICAgICAgICBsZWFkU3Vycm9nYXRlID0gY29kZVBvaW50XG4gICAgICAgIGNvbnRpbnVlXG4gICAgICB9XG5cbiAgICAgIC8vIHZhbGlkIHN1cnJvZ2F0ZSBwYWlyXG4gICAgICBjb2RlUG9pbnQgPSAobGVhZFN1cnJvZ2F0ZSAtIDB4RDgwMCA8PCAxMCB8IGNvZGVQb2ludCAtIDB4REMwMCkgKyAweDEwMDAwXG4gICAgfSBlbHNlIGlmIChsZWFkU3Vycm9nYXRlKSB7XG4gICAgICAvLyB2YWxpZCBibXAgY2hhciwgYnV0IGxhc3QgY2hhciB3YXMgYSBsZWFkXG4gICAgICBpZiAoKHVuaXRzIC09IDMpID4gLTEpIGJ5dGVzLnB1c2goMHhFRiwgMHhCRiwgMHhCRClcbiAgICB9XG5cbiAgICBsZWFkU3Vycm9nYXRlID0gbnVsbFxuXG4gICAgLy8gZW5jb2RlIHV0ZjhcbiAgICBpZiAoY29kZVBvaW50IDwgMHg4MCkge1xuICAgICAgaWYgKCh1bml0cyAtPSAxKSA8IDApIGJyZWFrXG4gICAgICBieXRlcy5wdXNoKGNvZGVQb2ludClcbiAgICB9IGVsc2UgaWYgKGNvZGVQb2ludCA8IDB4ODAwKSB7XG4gICAgICBpZiAoKHVuaXRzIC09IDIpIDwgMCkgYnJlYWtcbiAgICAgIGJ5dGVzLnB1c2goXG4gICAgICAgIGNvZGVQb2ludCA+PiAweDYgfCAweEMwLFxuICAgICAgICBjb2RlUG9pbnQgJiAweDNGIHwgMHg4MFxuICAgICAgKVxuICAgIH0gZWxzZSBpZiAoY29kZVBvaW50IDwgMHgxMDAwMCkge1xuICAgICAgaWYgKCh1bml0cyAtPSAzKSA8IDApIGJyZWFrXG4gICAgICBieXRlcy5wdXNoKFxuICAgICAgICBjb2RlUG9pbnQgPj4gMHhDIHwgMHhFMCxcbiAgICAgICAgY29kZVBvaW50ID4+IDB4NiAmIDB4M0YgfCAweDgwLFxuICAgICAgICBjb2RlUG9pbnQgJiAweDNGIHwgMHg4MFxuICAgICAgKVxuICAgIH0gZWxzZSBpZiAoY29kZVBvaW50IDwgMHgxMTAwMDApIHtcbiAgICAgIGlmICgodW5pdHMgLT0gNCkgPCAwKSBicmVha1xuICAgICAgYnl0ZXMucHVzaChcbiAgICAgICAgY29kZVBvaW50ID4+IDB4MTIgfCAweEYwLFxuICAgICAgICBjb2RlUG9pbnQgPj4gMHhDICYgMHgzRiB8IDB4ODAsXG4gICAgICAgIGNvZGVQb2ludCA+PiAweDYgJiAweDNGIHwgMHg4MCxcbiAgICAgICAgY29kZVBvaW50ICYgMHgzRiB8IDB4ODBcbiAgICAgIClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGNvZGUgcG9pbnQnKVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBieXRlc1xufVxuXG5mdW5jdGlvbiBhc2NpaVRvQnl0ZXMgKHN0cikge1xuICB2YXIgYnl0ZUFycmF5ID0gW11cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyArK2kpIHtcbiAgICAvLyBOb2RlJ3MgY29kZSBzZWVtcyB0byBiZSBkb2luZyB0aGlzIGFuZCBub3QgJiAweDdGLi5cbiAgICBieXRlQXJyYXkucHVzaChzdHIuY2hhckNvZGVBdChpKSAmIDB4RkYpXG4gIH1cbiAgcmV0dXJuIGJ5dGVBcnJheVxufVxuXG5mdW5jdGlvbiB1dGYxNmxlVG9CeXRlcyAoc3RyLCB1bml0cykge1xuICB2YXIgYywgaGksIGxvXG4gIHZhciBieXRlQXJyYXkgPSBbXVxuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0ci5sZW5ndGg7ICsraSkge1xuICAgIGlmICgodW5pdHMgLT0gMikgPCAwKSBicmVha1xuXG4gICAgYyA9IHN0ci5jaGFyQ29kZUF0KGkpXG4gICAgaGkgPSBjID4+IDhcbiAgICBsbyA9IGMgJSAyNTZcbiAgICBieXRlQXJyYXkucHVzaChsbylcbiAgICBieXRlQXJyYXkucHVzaChoaSlcbiAgfVxuXG4gIHJldHVybiBieXRlQXJyYXlcbn1cblxuZnVuY3Rpb24gYmFzZTY0VG9CeXRlcyAoc3RyKSB7XG4gIHJldHVybiBiYXNlNjQudG9CeXRlQXJyYXkoYmFzZTY0Y2xlYW4oc3RyKSlcbn1cblxuZnVuY3Rpb24gYmxpdEJ1ZmZlciAoc3JjLCBkc3QsIG9mZnNldCwgbGVuZ3RoKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyArK2kpIHtcbiAgICBpZiAoKGkgKyBvZmZzZXQgPj0gZHN0Lmxlbmd0aCkgfHwgKGkgPj0gc3JjLmxlbmd0aCkpIGJyZWFrXG4gICAgZHN0W2kgKyBvZmZzZXRdID0gc3JjW2ldXG4gIH1cbiAgcmV0dXJuIGlcbn1cblxuZnVuY3Rpb24gaXNuYW4gKHZhbCkge1xuICByZXR1cm4gdmFsICE9PSB2YWwgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1zZWxmLWNvbXBhcmVcbn1cbiIsIi8qISBpZWVlNzU0LiBCU0QtMy1DbGF1c2UgTGljZW5zZS4gRmVyb3NzIEFib3VraGFkaWplaCA8aHR0cHM6Ly9mZXJvc3Mub3JnL29wZW5zb3VyY2U+ICovXG5leHBvcnRzLnJlYWQgPSBmdW5jdGlvbiAoYnVmZmVyLCBvZmZzZXQsIGlzTEUsIG1MZW4sIG5CeXRlcykge1xuICB2YXIgZSwgbVxuICB2YXIgZUxlbiA9IChuQnl0ZXMgKiA4KSAtIG1MZW4gLSAxXG4gIHZhciBlTWF4ID0gKDEgPDwgZUxlbikgLSAxXG4gIHZhciBlQmlhcyA9IGVNYXggPj4gMVxuICB2YXIgbkJpdHMgPSAtN1xuICB2YXIgaSA9IGlzTEUgPyAobkJ5dGVzIC0gMSkgOiAwXG4gIHZhciBkID0gaXNMRSA/IC0xIDogMVxuICB2YXIgcyA9IGJ1ZmZlcltvZmZzZXQgKyBpXVxuXG4gIGkgKz0gZFxuXG4gIGUgPSBzICYgKCgxIDw8ICgtbkJpdHMpKSAtIDEpXG4gIHMgPj49ICgtbkJpdHMpXG4gIG5CaXRzICs9IGVMZW5cbiAgZm9yICg7IG5CaXRzID4gMDsgZSA9IChlICogMjU2KSArIGJ1ZmZlcltvZmZzZXQgKyBpXSwgaSArPSBkLCBuQml0cyAtPSA4KSB7fVxuXG4gIG0gPSBlICYgKCgxIDw8ICgtbkJpdHMpKSAtIDEpXG4gIGUgPj49ICgtbkJpdHMpXG4gIG5CaXRzICs9IG1MZW5cbiAgZm9yICg7IG5CaXRzID4gMDsgbSA9IChtICogMjU2KSArIGJ1ZmZlcltvZmZzZXQgKyBpXSwgaSArPSBkLCBuQml0cyAtPSA4KSB7fVxuXG4gIGlmIChlID09PSAwKSB7XG4gICAgZSA9IDEgLSBlQmlhc1xuICB9IGVsc2UgaWYgKGUgPT09IGVNYXgpIHtcbiAgICByZXR1cm4gbSA/IE5hTiA6ICgocyA/IC0xIDogMSkgKiBJbmZpbml0eSlcbiAgfSBlbHNlIHtcbiAgICBtID0gbSArIE1hdGgucG93KDIsIG1MZW4pXG4gICAgZSA9IGUgLSBlQmlhc1xuICB9XG4gIHJldHVybiAocyA/IC0xIDogMSkgKiBtICogTWF0aC5wb3coMiwgZSAtIG1MZW4pXG59XG5cbmV4cG9ydHMud3JpdGUgPSBmdW5jdGlvbiAoYnVmZmVyLCB2YWx1ZSwgb2Zmc2V0LCBpc0xFLCBtTGVuLCBuQnl0ZXMpIHtcbiAgdmFyIGUsIG0sIGNcbiAgdmFyIGVMZW4gPSAobkJ5dGVzICogOCkgLSBtTGVuIC0gMVxuICB2YXIgZU1heCA9ICgxIDw8IGVMZW4pIC0gMVxuICB2YXIgZUJpYXMgPSBlTWF4ID4+IDFcbiAgdmFyIHJ0ID0gKG1MZW4gPT09IDIzID8gTWF0aC5wb3coMiwgLTI0KSAtIE1hdGgucG93KDIsIC03NykgOiAwKVxuICB2YXIgaSA9IGlzTEUgPyAwIDogKG5CeXRlcyAtIDEpXG4gIHZhciBkID0gaXNMRSA/IDEgOiAtMVxuICB2YXIgcyA9IHZhbHVlIDwgMCB8fCAodmFsdWUgPT09IDAgJiYgMSAvIHZhbHVlIDwgMCkgPyAxIDogMFxuXG4gIHZhbHVlID0gTWF0aC5hYnModmFsdWUpXG5cbiAgaWYgKGlzTmFOKHZhbHVlKSB8fCB2YWx1ZSA9PT0gSW5maW5pdHkpIHtcbiAgICBtID0gaXNOYU4odmFsdWUpID8gMSA6IDBcbiAgICBlID0gZU1heFxuICB9IGVsc2Uge1xuICAgIGUgPSBNYXRoLmZsb29yKE1hdGgubG9nKHZhbHVlKSAvIE1hdGguTE4yKVxuICAgIGlmICh2YWx1ZSAqIChjID0gTWF0aC5wb3coMiwgLWUpKSA8IDEpIHtcbiAgICAgIGUtLVxuICAgICAgYyAqPSAyXG4gICAgfVxuICAgIGlmIChlICsgZUJpYXMgPj0gMSkge1xuICAgICAgdmFsdWUgKz0gcnQgLyBjXG4gICAgfSBlbHNlIHtcbiAgICAgIHZhbHVlICs9IHJ0ICogTWF0aC5wb3coMiwgMSAtIGVCaWFzKVxuICAgIH1cbiAgICBpZiAodmFsdWUgKiBjID49IDIpIHtcbiAgICAgIGUrK1xuICAgICAgYyAvPSAyXG4gICAgfVxuXG4gICAgaWYgKGUgKyBlQmlhcyA+PSBlTWF4KSB7XG4gICAgICBtID0gMFxuICAgICAgZSA9IGVNYXhcbiAgICB9IGVsc2UgaWYgKGUgKyBlQmlhcyA+PSAxKSB7XG4gICAgICBtID0gKCh2YWx1ZSAqIGMpIC0gMSkgKiBNYXRoLnBvdygyLCBtTGVuKVxuICAgICAgZSA9IGUgKyBlQmlhc1xuICAgIH0gZWxzZSB7XG4gICAgICBtID0gdmFsdWUgKiBNYXRoLnBvdygyLCBlQmlhcyAtIDEpICogTWF0aC5wb3coMiwgbUxlbilcbiAgICAgIGUgPSAwXG4gICAgfVxuICB9XG5cbiAgZm9yICg7IG1MZW4gPj0gODsgYnVmZmVyW29mZnNldCArIGldID0gbSAmIDB4ZmYsIGkgKz0gZCwgbSAvPSAyNTYsIG1MZW4gLT0gOCkge31cblxuICBlID0gKGUgPDwgbUxlbikgfCBtXG4gIGVMZW4gKz0gbUxlblxuICBmb3IgKDsgZUxlbiA+IDA7IGJ1ZmZlcltvZmZzZXQgKyBpXSA9IGUgJiAweGZmLCBpICs9IGQsIGUgLz0gMjU2LCBlTGVuIC09IDgpIHt9XG5cbiAgYnVmZmVyW29mZnNldCArIGkgLSBkXSB8PSBzICogMTI4XG59XG4iLCJ2YXIgdG9TdHJpbmcgPSB7fS50b1N0cmluZztcblxubW9kdWxlLmV4cG9ydHMgPSBBcnJheS5pc0FycmF5IHx8IGZ1bmN0aW9uIChhcnIpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwoYXJyKSA9PSAnW29iamVjdCBBcnJheV0nO1xufTtcbiIsIihmdW5jdGlvbiB3ZWJwYWNrVW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbihyb290LCBmYWN0b3J5KSB7XG5cdGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0JyAmJiB0eXBlb2YgbW9kdWxlID09PSAnb2JqZWN0Jylcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGZhY3RvcnkoKTtcblx0ZWxzZSBpZih0eXBlb2YgZGVmaW5lID09PSAnZnVuY3Rpb24nICYmIGRlZmluZS5hbWQpXG5cdFx0ZGVmaW5lKFtdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcIlByaXZhY3lVcmxQcm9jZXNzb3JcIl0gPSBmYWN0b3J5KCk7XG5cdGVsc2Vcblx0XHRyb290W1wiUHJpdmFjeVVybFByb2Nlc3NvclwiXSA9IGZhY3RvcnkoKTtcbn0pKHRoaXMsIGZ1bmN0aW9uKCkge1xucmV0dXJuICIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2luZGV4LnRzXCIpO1xuIiwiJ3VzZSBzdHJpY3QnXG5cbmV4cG9ydHMuYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGhcbmV4cG9ydHMudG9CeXRlQXJyYXkgPSB0b0J5dGVBcnJheVxuZXhwb3J0cy5mcm9tQnl0ZUFycmF5ID0gZnJvbUJ5dGVBcnJheVxuXG52YXIgbG9va3VwID0gW11cbnZhciByZXZMb29rdXAgPSBbXVxudmFyIEFyciA9IHR5cGVvZiBVaW50OEFycmF5ICE9PSAndW5kZWZpbmVkJyA/IFVpbnQ4QXJyYXkgOiBBcnJheVxuXG52YXIgY29kZSA9ICdBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWmFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6MDEyMzQ1Njc4OSsvJ1xuZm9yICh2YXIgaSA9IDAsIGxlbiA9IGNvZGUubGVuZ3RoOyBpIDwgbGVuOyArK2kpIHtcbiAgbG9va3VwW2ldID0gY29kZVtpXVxuICByZXZMb29rdXBbY29kZS5jaGFyQ29kZUF0KGkpXSA9IGlcbn1cblxuLy8gU3VwcG9ydCBkZWNvZGluZyBVUkwtc2FmZSBiYXNlNjQgc3RyaW5ncywgYXMgTm9kZS5qcyBkb2VzLlxuLy8gU2VlOiBodHRwczovL2VuLndpa2lwZWRpYS5vcmcvd2lraS9CYXNlNjQjVVJMX2FwcGxpY2F0aW9uc1xucmV2TG9va3VwWyctJy5jaGFyQ29kZUF0KDApXSA9IDYyXG5yZXZMb29rdXBbJ18nLmNoYXJDb2RlQXQoMCldID0gNjNcblxuZnVuY3Rpb24gZ2V0TGVucyAoYjY0KSB7XG4gIHZhciBsZW4gPSBiNjQubGVuZ3RoXG5cbiAgaWYgKGxlbiAlIDQgPiAwKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIHN0cmluZy4gTGVuZ3RoIG11c3QgYmUgYSBtdWx0aXBsZSBvZiA0JylcbiAgfVxuXG4gIC8vIFRyaW0gb2ZmIGV4dHJhIGJ5dGVzIGFmdGVyIHBsYWNlaG9sZGVyIGJ5dGVzIGFyZSBmb3VuZFxuICAvLyBTZWU6IGh0dHBzOi8vZ2l0aHViLmNvbS9iZWF0Z2FtbWl0L2Jhc2U2NC1qcy9pc3N1ZXMvNDJcbiAgdmFyIHZhbGlkTGVuID0gYjY0LmluZGV4T2YoJz0nKVxuICBpZiAodmFsaWRMZW4gPT09IC0xKSB2YWxpZExlbiA9IGxlblxuXG4gIHZhciBwbGFjZUhvbGRlcnNMZW4gPSB2YWxpZExlbiA9PT0gbGVuXG4gICAgPyAwXG4gICAgOiA0IC0gKHZhbGlkTGVuICUgNClcblxuICByZXR1cm4gW3ZhbGlkTGVuLCBwbGFjZUhvbGRlcnNMZW5dXG59XG5cbi8vIGJhc2U2NCBpcyA0LzMgKyB1cCB0byB0d28gY2hhcmFjdGVycyBvZiB0aGUgb3JpZ2luYWwgZGF0YVxuZnVuY3Rpb24gYnl0ZUxlbmd0aCAoYjY0KSB7XG4gIHZhciBsZW5zID0gZ2V0TGVucyhiNjQpXG4gIHZhciB2YWxpZExlbiA9IGxlbnNbMF1cbiAgdmFyIHBsYWNlSG9sZGVyc0xlbiA9IGxlbnNbMV1cbiAgcmV0dXJuICgodmFsaWRMZW4gKyBwbGFjZUhvbGRlcnNMZW4pICogMyAvIDQpIC0gcGxhY2VIb2xkZXJzTGVuXG59XG5cbmZ1bmN0aW9uIF9ieXRlTGVuZ3RoIChiNjQsIHZhbGlkTGVuLCBwbGFjZUhvbGRlcnNMZW4pIHtcbiAgcmV0dXJuICgodmFsaWRMZW4gKyBwbGFjZUhvbGRlcnNMZW4pICogMyAvIDQpIC0gcGxhY2VIb2xkZXJzTGVuXG59XG5cbmZ1bmN0aW9uIHRvQnl0ZUFycmF5IChiNjQpIHtcbiAgdmFyIHRtcFxuICB2YXIgbGVucyA9IGdldExlbnMoYjY0KVxuICB2YXIgdmFsaWRMZW4gPSBsZW5zWzBdXG4gIHZhciBwbGFjZUhvbGRlcnNMZW4gPSBsZW5zWzFdXG5cbiAgdmFyIGFyciA9IG5ldyBBcnIoX2J5dGVMZW5ndGgoYjY0LCB2YWxpZExlbiwgcGxhY2VIb2xkZXJzTGVuKSlcblxuICB2YXIgY3VyQnl0ZSA9IDBcblxuICAvLyBpZiB0aGVyZSBhcmUgcGxhY2Vob2xkZXJzLCBvbmx5IGdldCB1cCB0byB0aGUgbGFzdCBjb21wbGV0ZSA0IGNoYXJzXG4gIHZhciBsZW4gPSBwbGFjZUhvbGRlcnNMZW4gPiAwXG4gICAgPyB2YWxpZExlbiAtIDRcbiAgICA6IHZhbGlkTGVuXG5cbiAgdmFyIGlcbiAgZm9yIChpID0gMDsgaSA8IGxlbjsgaSArPSA0KSB7XG4gICAgdG1wID1cbiAgICAgIChyZXZMb29rdXBbYjY0LmNoYXJDb2RlQXQoaSldIDw8IDE4KSB8XG4gICAgICAocmV2TG9va3VwW2I2NC5jaGFyQ29kZUF0KGkgKyAxKV0gPDwgMTIpIHxcbiAgICAgIChyZXZMb29rdXBbYjY0LmNoYXJDb2RlQXQoaSArIDIpXSA8PCA2KSB8XG4gICAgICByZXZMb29rdXBbYjY0LmNoYXJDb2RlQXQoaSArIDMpXVxuICAgIGFycltjdXJCeXRlKytdID0gKHRtcCA+PiAxNikgJiAweEZGXG4gICAgYXJyW2N1ckJ5dGUrK10gPSAodG1wID4+IDgpICYgMHhGRlxuICAgIGFycltjdXJCeXRlKytdID0gdG1wICYgMHhGRlxuICB9XG5cbiAgaWYgKHBsYWNlSG9sZGVyc0xlbiA9PT0gMikge1xuICAgIHRtcCA9XG4gICAgICAocmV2TG9va3VwW2I2NC5jaGFyQ29kZUF0KGkpXSA8PCAyKSB8XG4gICAgICAocmV2TG9va3VwW2I2NC5jaGFyQ29kZUF0KGkgKyAxKV0gPj4gNClcbiAgICBhcnJbY3VyQnl0ZSsrXSA9IHRtcCAmIDB4RkZcbiAgfVxuXG4gIGlmIChwbGFjZUhvbGRlcnNMZW4gPT09IDEpIHtcbiAgICB0bXAgPVxuICAgICAgKHJldkxvb2t1cFtiNjQuY2hhckNvZGVBdChpKV0gPDwgMTApIHxcbiAgICAgIChyZXZMb29rdXBbYjY0LmNoYXJDb2RlQXQoaSArIDEpXSA8PCA0KSB8XG4gICAgICAocmV2TG9va3VwW2I2NC5jaGFyQ29kZUF0KGkgKyAyKV0gPj4gMilcbiAgICBhcnJbY3VyQnl0ZSsrXSA9ICh0bXAgPj4gOCkgJiAweEZGXG4gICAgYXJyW2N1ckJ5dGUrK10gPSB0bXAgJiAweEZGXG4gIH1cblxuICByZXR1cm4gYXJyXG59XG5cbmZ1bmN0aW9uIHRyaXBsZXRUb0Jhc2U2NCAobnVtKSB7XG4gIHJldHVybiBsb29rdXBbbnVtID4+IDE4ICYgMHgzRl0gK1xuICAgIGxvb2t1cFtudW0gPj4gMTIgJiAweDNGXSArXG4gICAgbG9va3VwW251bSA+PiA2ICYgMHgzRl0gK1xuICAgIGxvb2t1cFtudW0gJiAweDNGXVxufVxuXG5mdW5jdGlvbiBlbmNvZGVDaHVuayAodWludDgsIHN0YXJ0LCBlbmQpIHtcbiAgdmFyIHRtcFxuICB2YXIgb3V0cHV0ID0gW11cbiAgZm9yICh2YXIgaSA9IHN0YXJ0OyBpIDwgZW5kOyBpICs9IDMpIHtcbiAgICB0bXAgPVxuICAgICAgKCh1aW50OFtpXSA8PCAxNikgJiAweEZGMDAwMCkgK1xuICAgICAgKCh1aW50OFtpICsgMV0gPDwgOCkgJiAweEZGMDApICtcbiAgICAgICh1aW50OFtpICsgMl0gJiAweEZGKVxuICAgIG91dHB1dC5wdXNoKHRyaXBsZXRUb0Jhc2U2NCh0bXApKVxuICB9XG4gIHJldHVybiBvdXRwdXQuam9pbignJylcbn1cblxuZnVuY3Rpb24gZnJvbUJ5dGVBcnJheSAodWludDgpIHtcbiAgdmFyIHRtcFxuICB2YXIgbGVuID0gdWludDgubGVuZ3RoXG4gIHZhciBleHRyYUJ5dGVzID0gbGVuICUgMyAvLyBpZiB3ZSBoYXZlIDEgYnl0ZSBsZWZ0LCBwYWQgMiBieXRlc1xuICB2YXIgcGFydHMgPSBbXVxuICB2YXIgbWF4Q2h1bmtMZW5ndGggPSAxNjM4MyAvLyBtdXN0IGJlIG11bHRpcGxlIG9mIDNcblxuICAvLyBnbyB0aHJvdWdoIHRoZSBhcnJheSBldmVyeSB0aHJlZSBieXRlcywgd2UnbGwgZGVhbCB3aXRoIHRyYWlsaW5nIHN0dWZmIGxhdGVyXG4gIGZvciAodmFyIGkgPSAwLCBsZW4yID0gbGVuIC0gZXh0cmFCeXRlczsgaSA8IGxlbjI7IGkgKz0gbWF4Q2h1bmtMZW5ndGgpIHtcbiAgICBwYXJ0cy5wdXNoKGVuY29kZUNodW5rKHVpbnQ4LCBpLCAoaSArIG1heENodW5rTGVuZ3RoKSA+IGxlbjIgPyBsZW4yIDogKGkgKyBtYXhDaHVua0xlbmd0aCkpKVxuICB9XG5cbiAgLy8gcGFkIHRoZSBlbmQgd2l0aCB6ZXJvcywgYnV0IG1ha2Ugc3VyZSB0byBub3QgZm9yZ2V0IHRoZSBleHRyYSBieXRlc1xuICBpZiAoZXh0cmFCeXRlcyA9PT0gMSkge1xuICAgIHRtcCA9IHVpbnQ4W2xlbiAtIDFdXG4gICAgcGFydHMucHVzaChcbiAgICAgIGxvb2t1cFt0bXAgPj4gMl0gK1xuICAgICAgbG9va3VwWyh0bXAgPDwgNCkgJiAweDNGXSArXG4gICAgICAnPT0nXG4gICAgKVxuICB9IGVsc2UgaWYgKGV4dHJhQnl0ZXMgPT09IDIpIHtcbiAgICB0bXAgPSAodWludDhbbGVuIC0gMl0gPDwgOCkgKyB1aW50OFtsZW4gLSAxXVxuICAgIHBhcnRzLnB1c2goXG4gICAgICBsb29rdXBbdG1wID4+IDEwXSArXG4gICAgICBsb29rdXBbKHRtcCA+PiA0KSAmIDB4M0ZdICtcbiAgICAgIGxvb2t1cFsodG1wIDw8IDIpICYgMHgzRl0gK1xuICAgICAgJz0nXG4gICAgKVxuICB9XG5cbiAgcmV0dXJuIHBhcnRzLmpvaW4oJycpXG59XG4iLCIndXNlIHN0cmljdCdcblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIHVuZXNjYXBlOiB1bmVzY2FwZSxcbiAgZXNjYXBlOiBlc2NhcGUsXG4gIGVuY29kZTogZW5jb2RlLFxuICBkZWNvZGU6IGRlY29kZVxufVxuXG5mdW5jdGlvbiB1bmVzY2FwZSAoc3RyKSB7XG4gIHJldHVybiAoc3RyICsgJz09PScuc2xpY2UoKHN0ci5sZW5ndGggKyAzKSAlIDQpKVxuICAgIC5yZXBsYWNlKC8tL2csICcrJylcbiAgICAucmVwbGFjZSgvXy9nLCAnLycpXG59XG5cbmZ1bmN0aW9uIGVzY2FwZSAoc3RyKSB7XG4gIHJldHVybiBzdHIucmVwbGFjZSgvXFwrL2csICctJylcbiAgICAucmVwbGFjZSgvXFwvL2csICdfJylcbiAgICAucmVwbGFjZSgvPS9nLCAnJylcbn1cblxuZnVuY3Rpb24gZW5jb2RlIChzdHIsIGVuY29kaW5nKSB7XG4gIHJldHVybiBlc2NhcGUoQnVmZmVyLmZyb20oc3RyLCBlbmNvZGluZyB8fCAndXRmOCcpLnRvU3RyaW5nKCdiYXNlNjQnKSlcbn1cblxuZnVuY3Rpb24gZGVjb2RlIChzdHIsIGVuY29kaW5nKSB7XG4gIHJldHVybiBCdWZmZXIuZnJvbSh1bmVzY2FwZShzdHIpLCAnYmFzZTY0JykudG9TdHJpbmcoZW5jb2RpbmcgfHwgJ3V0ZjgnKVxufVxuIiwiLyohXG4gKiBUaGUgYnVmZmVyIG1vZHVsZSBmcm9tIG5vZGUuanMsIGZvciB0aGUgYnJvd3Nlci5cbiAqXG4gKiBAYXV0aG9yICAgRmVyb3NzIEFib3VraGFkaWplaCA8aHR0cDovL2Zlcm9zcy5vcmc+XG4gKiBAbGljZW5zZSAgTUlUXG4gKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLXByb3RvICovXG5cbid1c2Ugc3RyaWN0J1xuXG52YXIgYmFzZTY0ID0gcmVxdWlyZSgnYmFzZTY0LWpzJylcbnZhciBpZWVlNzU0ID0gcmVxdWlyZSgnaWVlZTc1NCcpXG52YXIgaXNBcnJheSA9IHJlcXVpcmUoJ2lzYXJyYXknKVxuXG5leHBvcnRzLkJ1ZmZlciA9IEJ1ZmZlclxuZXhwb3J0cy5TbG93QnVmZmVyID0gU2xvd0J1ZmZlclxuZXhwb3J0cy5JTlNQRUNUX01BWF9CWVRFUyA9IDUwXG5cbi8qKlxuICogSWYgYEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUYDpcbiAqICAgPT09IHRydWUgICAgVXNlIFVpbnQ4QXJyYXkgaW1wbGVtZW50YXRpb24gKGZhc3Rlc3QpXG4gKiAgID09PSBmYWxzZSAgIFVzZSBPYmplY3QgaW1wbGVtZW50YXRpb24gKG1vc3QgY29tcGF0aWJsZSwgZXZlbiBJRTYpXG4gKlxuICogQnJvd3NlcnMgdGhhdCBzdXBwb3J0IHR5cGVkIGFycmF5cyBhcmUgSUUgMTArLCBGaXJlZm94IDQrLCBDaHJvbWUgNyssIFNhZmFyaSA1LjErLFxuICogT3BlcmEgMTEuNissIGlPUyA0LjIrLlxuICpcbiAqIER1ZSB0byB2YXJpb3VzIGJyb3dzZXIgYnVncywgc29tZXRpbWVzIHRoZSBPYmplY3QgaW1wbGVtZW50YXRpb24gd2lsbCBiZSB1c2VkIGV2ZW5cbiAqIHdoZW4gdGhlIGJyb3dzZXIgc3VwcG9ydHMgdHlwZWQgYXJyYXlzLlxuICpcbiAqIE5vdGU6XG4gKlxuICogICAtIEZpcmVmb3ggNC0yOSBsYWNrcyBzdXBwb3J0IGZvciBhZGRpbmcgbmV3IHByb3BlcnRpZXMgdG8gYFVpbnQ4QXJyYXlgIGluc3RhbmNlcyxcbiAqICAgICBTZWU6IGh0dHBzOi8vYnVnemlsbGEubW96aWxsYS5vcmcvc2hvd19idWcuY2dpP2lkPTY5NTQzOC5cbiAqXG4gKiAgIC0gQ2hyb21lIDktMTAgaXMgbWlzc2luZyB0aGUgYFR5cGVkQXJyYXkucHJvdG90eXBlLnN1YmFycmF5YCBmdW5jdGlvbi5cbiAqXG4gKiAgIC0gSUUxMCBoYXMgYSBicm9rZW4gYFR5cGVkQXJyYXkucHJvdG90eXBlLnN1YmFycmF5YCBmdW5jdGlvbiB3aGljaCByZXR1cm5zIGFycmF5cyBvZlxuICogICAgIGluY29ycmVjdCBsZW5ndGggaW4gc29tZSBzaXR1YXRpb25zLlxuXG4gKiBXZSBkZXRlY3QgdGhlc2UgYnVnZ3kgYnJvd3NlcnMgYW5kIHNldCBgQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlRgIHRvIGBmYWxzZWAgc28gdGhleVxuICogZ2V0IHRoZSBPYmplY3QgaW1wbGVtZW50YXRpb24sIHdoaWNoIGlzIHNsb3dlciBidXQgYmVoYXZlcyBjb3JyZWN0bHkuXG4gKi9cbkJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUID0gZ2xvYmFsLlRZUEVEX0FSUkFZX1NVUFBPUlQgIT09IHVuZGVmaW5lZFxuICA/IGdsb2JhbC5UWVBFRF9BUlJBWV9TVVBQT1JUXG4gIDogdHlwZWRBcnJheVN1cHBvcnQoKVxuXG4vKlxuICogRXhwb3J0IGtNYXhMZW5ndGggYWZ0ZXIgdHlwZWQgYXJyYXkgc3VwcG9ydCBpcyBkZXRlcm1pbmVkLlxuICovXG5leHBvcnRzLmtNYXhMZW5ndGggPSBrTWF4TGVuZ3RoKClcblxuZnVuY3Rpb24gdHlwZWRBcnJheVN1cHBvcnQgKCkge1xuICB0cnkge1xuICAgIHZhciBhcnIgPSBuZXcgVWludDhBcnJheSgxKVxuICAgIGFyci5fX3Byb3RvX18gPSB7X19wcm90b19fOiBVaW50OEFycmF5LnByb3RvdHlwZSwgZm9vOiBmdW5jdGlvbiAoKSB7IHJldHVybiA0MiB9fVxuICAgIHJldHVybiBhcnIuZm9vKCkgPT09IDQyICYmIC8vIHR5cGVkIGFycmF5IGluc3RhbmNlcyBjYW4gYmUgYXVnbWVudGVkXG4gICAgICAgIHR5cGVvZiBhcnIuc3ViYXJyYXkgPT09ICdmdW5jdGlvbicgJiYgLy8gY2hyb21lIDktMTAgbGFjayBgc3ViYXJyYXlgXG4gICAgICAgIGFyci5zdWJhcnJheSgxLCAxKS5ieXRlTGVuZ3RoID09PSAwIC8vIGllMTAgaGFzIGJyb2tlbiBgc3ViYXJyYXlgXG4gIH0gY2F0Y2ggKGUpIHtcbiAgICByZXR1cm4gZmFsc2VcbiAgfVxufVxuXG5mdW5jdGlvbiBrTWF4TGVuZ3RoICgpIHtcbiAgcmV0dXJuIEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUXG4gICAgPyAweDdmZmZmZmZmXG4gICAgOiAweDNmZmZmZmZmXG59XG5cbmZ1bmN0aW9uIGNyZWF0ZUJ1ZmZlciAodGhhdCwgbGVuZ3RoKSB7XG4gIGlmIChrTWF4TGVuZ3RoKCkgPCBsZW5ndGgpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignSW52YWxpZCB0eXBlZCBhcnJheSBsZW5ndGgnKVxuICB9XG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIC8vIFJldHVybiBhbiBhdWdtZW50ZWQgYFVpbnQ4QXJyYXlgIGluc3RhbmNlLCBmb3IgYmVzdCBwZXJmb3JtYW5jZVxuICAgIHRoYXQgPSBuZXcgVWludDhBcnJheShsZW5ndGgpXG4gICAgdGhhdC5fX3Byb3RvX18gPSBCdWZmZXIucHJvdG90eXBlXG4gIH0gZWxzZSB7XG4gICAgLy8gRmFsbGJhY2s6IFJldHVybiBhbiBvYmplY3QgaW5zdGFuY2Ugb2YgdGhlIEJ1ZmZlciBjbGFzc1xuICAgIGlmICh0aGF0ID09PSBudWxsKSB7XG4gICAgICB0aGF0ID0gbmV3IEJ1ZmZlcihsZW5ndGgpXG4gICAgfVxuICAgIHRoYXQubGVuZ3RoID0gbGVuZ3RoXG4gIH1cblxuICByZXR1cm4gdGhhdFxufVxuXG4vKipcbiAqIFRoZSBCdWZmZXIgY29uc3RydWN0b3IgcmV0dXJucyBpbnN0YW5jZXMgb2YgYFVpbnQ4QXJyYXlgIHRoYXQgaGF2ZSB0aGVpclxuICogcHJvdG90eXBlIGNoYW5nZWQgdG8gYEJ1ZmZlci5wcm90b3R5cGVgLiBGdXJ0aGVybW9yZSwgYEJ1ZmZlcmAgaXMgYSBzdWJjbGFzcyBvZlxuICogYFVpbnQ4QXJyYXlgLCBzbyB0aGUgcmV0dXJuZWQgaW5zdGFuY2VzIHdpbGwgaGF2ZSBhbGwgdGhlIG5vZGUgYEJ1ZmZlcmAgbWV0aG9kc1xuICogYW5kIHRoZSBgVWludDhBcnJheWAgbWV0aG9kcy4gU3F1YXJlIGJyYWNrZXQgbm90YXRpb24gd29ya3MgYXMgZXhwZWN0ZWQgLS0gaXRcbiAqIHJldHVybnMgYSBzaW5nbGUgb2N0ZXQuXG4gKlxuICogVGhlIGBVaW50OEFycmF5YCBwcm90b3R5cGUgcmVtYWlucyB1bm1vZGlmaWVkLlxuICovXG5cbmZ1bmN0aW9uIEJ1ZmZlciAoYXJnLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpIHtcbiAgaWYgKCFCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCAmJiAhKHRoaXMgaW5zdGFuY2VvZiBCdWZmZXIpKSB7XG4gICAgcmV0dXJuIG5ldyBCdWZmZXIoYXJnLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpXG4gIH1cblxuICAvLyBDb21tb24gY2FzZS5cbiAgaWYgKHR5cGVvZiBhcmcgPT09ICdudW1iZXInKSB7XG4gICAgaWYgKHR5cGVvZiBlbmNvZGluZ09yT2Zmc2V0ID09PSAnc3RyaW5nJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICAnSWYgZW5jb2RpbmcgaXMgc3BlY2lmaWVkIHRoZW4gdGhlIGZpcnN0IGFyZ3VtZW50IG11c3QgYmUgYSBzdHJpbmcnXG4gICAgICApXG4gICAgfVxuICAgIHJldHVybiBhbGxvY1Vuc2FmZSh0aGlzLCBhcmcpXG4gIH1cbiAgcmV0dXJuIGZyb20odGhpcywgYXJnLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpXG59XG5cbkJ1ZmZlci5wb29sU2l6ZSA9IDgxOTIgLy8gbm90IHVzZWQgYnkgdGhpcyBpbXBsZW1lbnRhdGlvblxuXG4vLyBUT0RPOiBMZWdhY3ksIG5vdCBuZWVkZWQgYW55bW9yZS4gUmVtb3ZlIGluIG5leHQgbWFqb3IgdmVyc2lvbi5cbkJ1ZmZlci5fYXVnbWVudCA9IGZ1bmN0aW9uIChhcnIpIHtcbiAgYXJyLl9fcHJvdG9fXyA9IEJ1ZmZlci5wcm90b3R5cGVcbiAgcmV0dXJuIGFyclxufVxuXG5mdW5jdGlvbiBmcm9tICh0aGF0LCB2YWx1ZSwgZW5jb2RpbmdPck9mZnNldCwgbGVuZ3RoKSB7XG4gIGlmICh0eXBlb2YgdmFsdWUgPT09ICdudW1iZXInKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcignXCJ2YWx1ZVwiIGFyZ3VtZW50IG11c3Qgbm90IGJlIGEgbnVtYmVyJylcbiAgfVxuXG4gIGlmICh0eXBlb2YgQXJyYXlCdWZmZXIgIT09ICd1bmRlZmluZWQnICYmIHZhbHVlIGluc3RhbmNlb2YgQXJyYXlCdWZmZXIpIHtcbiAgICByZXR1cm4gZnJvbUFycmF5QnVmZmVyKHRoYXQsIHZhbHVlLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpXG4gIH1cblxuICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBmcm9tU3RyaW5nKHRoYXQsIHZhbHVlLCBlbmNvZGluZ09yT2Zmc2V0KVxuICB9XG5cbiAgcmV0dXJuIGZyb21PYmplY3QodGhhdCwgdmFsdWUpXG59XG5cbi8qKlxuICogRnVuY3Rpb25hbGx5IGVxdWl2YWxlbnQgdG8gQnVmZmVyKGFyZywgZW5jb2RpbmcpIGJ1dCB0aHJvd3MgYSBUeXBlRXJyb3JcbiAqIGlmIHZhbHVlIGlzIGEgbnVtYmVyLlxuICogQnVmZmVyLmZyb20oc3RyWywgZW5jb2RpbmddKVxuICogQnVmZmVyLmZyb20oYXJyYXkpXG4gKiBCdWZmZXIuZnJvbShidWZmZXIpXG4gKiBCdWZmZXIuZnJvbShhcnJheUJ1ZmZlclssIGJ5dGVPZmZzZXRbLCBsZW5ndGhdXSlcbiAqKi9cbkJ1ZmZlci5mcm9tID0gZnVuY3Rpb24gKHZhbHVlLCBlbmNvZGluZ09yT2Zmc2V0LCBsZW5ndGgpIHtcbiAgcmV0dXJuIGZyb20obnVsbCwgdmFsdWUsIGVuY29kaW5nT3JPZmZzZXQsIGxlbmd0aClcbn1cblxuaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gIEJ1ZmZlci5wcm90b3R5cGUuX19wcm90b19fID0gVWludDhBcnJheS5wcm90b3R5cGVcbiAgQnVmZmVyLl9fcHJvdG9fXyA9IFVpbnQ4QXJyYXlcbiAgaWYgKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC5zcGVjaWVzICYmXG4gICAgICBCdWZmZXJbU3ltYm9sLnNwZWNpZXNdID09PSBCdWZmZXIpIHtcbiAgICAvLyBGaXggc3ViYXJyYXkoKSBpbiBFUzIwMTYuIFNlZTogaHR0cHM6Ly9naXRodWIuY29tL2Zlcm9zcy9idWZmZXIvcHVsbC85N1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShCdWZmZXIsIFN5bWJvbC5zcGVjaWVzLCB7XG4gICAgICB2YWx1ZTogbnVsbCxcbiAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgIH0pXG4gIH1cbn1cblxuZnVuY3Rpb24gYXNzZXJ0U2l6ZSAoc2l6ZSkge1xuICBpZiAodHlwZW9mIHNpemUgIT09ICdudW1iZXInKSB7XG4gICAgdGhyb3cgbmV3IFR5cGVFcnJvcignXCJzaXplXCIgYXJndW1lbnQgbXVzdCBiZSBhIG51bWJlcicpXG4gIH0gZWxzZSBpZiAoc2l6ZSA8IDApIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignXCJzaXplXCIgYXJndW1lbnQgbXVzdCBub3QgYmUgbmVnYXRpdmUnKVxuICB9XG59XG5cbmZ1bmN0aW9uIGFsbG9jICh0aGF0LCBzaXplLCBmaWxsLCBlbmNvZGluZykge1xuICBhc3NlcnRTaXplKHNpemUpXG4gIGlmIChzaXplIDw9IDApIHtcbiAgICByZXR1cm4gY3JlYXRlQnVmZmVyKHRoYXQsIHNpemUpXG4gIH1cbiAgaWYgKGZpbGwgIT09IHVuZGVmaW5lZCkge1xuICAgIC8vIE9ubHkgcGF5IGF0dGVudGlvbiB0byBlbmNvZGluZyBpZiBpdCdzIGEgc3RyaW5nLiBUaGlzXG4gICAgLy8gcHJldmVudHMgYWNjaWRlbnRhbGx5IHNlbmRpbmcgaW4gYSBudW1iZXIgdGhhdCB3b3VsZFxuICAgIC8vIGJlIGludGVycHJldHRlZCBhcyBhIHN0YXJ0IG9mZnNldC5cbiAgICByZXR1cm4gdHlwZW9mIGVuY29kaW5nID09PSAnc3RyaW5nJ1xuICAgICAgPyBjcmVhdGVCdWZmZXIodGhhdCwgc2l6ZSkuZmlsbChmaWxsLCBlbmNvZGluZylcbiAgICAgIDogY3JlYXRlQnVmZmVyKHRoYXQsIHNpemUpLmZpbGwoZmlsbClcbiAgfVxuICByZXR1cm4gY3JlYXRlQnVmZmVyKHRoYXQsIHNpemUpXG59XG5cbi8qKlxuICogQ3JlYXRlcyBhIG5ldyBmaWxsZWQgQnVmZmVyIGluc3RhbmNlLlxuICogYWxsb2Moc2l6ZVssIGZpbGxbLCBlbmNvZGluZ11dKVxuICoqL1xuQnVmZmVyLmFsbG9jID0gZnVuY3Rpb24gKHNpemUsIGZpbGwsIGVuY29kaW5nKSB7XG4gIHJldHVybiBhbGxvYyhudWxsLCBzaXplLCBmaWxsLCBlbmNvZGluZylcbn1cblxuZnVuY3Rpb24gYWxsb2NVbnNhZmUgKHRoYXQsIHNpemUpIHtcbiAgYXNzZXJ0U2l6ZShzaXplKVxuICB0aGF0ID0gY3JlYXRlQnVmZmVyKHRoYXQsIHNpemUgPCAwID8gMCA6IGNoZWNrZWQoc2l6ZSkgfCAwKVxuICBpZiAoIUJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzaXplOyArK2kpIHtcbiAgICAgIHRoYXRbaV0gPSAwXG4gICAgfVxuICB9XG4gIHJldHVybiB0aGF0XG59XG5cbi8qKlxuICogRXF1aXZhbGVudCB0byBCdWZmZXIobnVtKSwgYnkgZGVmYXVsdCBjcmVhdGVzIGEgbm9uLXplcm8tZmlsbGVkIEJ1ZmZlciBpbnN0YW5jZS5cbiAqICovXG5CdWZmZXIuYWxsb2NVbnNhZmUgPSBmdW5jdGlvbiAoc2l6ZSkge1xuICByZXR1cm4gYWxsb2NVbnNhZmUobnVsbCwgc2l6ZSlcbn1cbi8qKlxuICogRXF1aXZhbGVudCB0byBTbG93QnVmZmVyKG51bSksIGJ5IGRlZmF1bHQgY3JlYXRlcyBhIG5vbi16ZXJvLWZpbGxlZCBCdWZmZXIgaW5zdGFuY2UuXG4gKi9cbkJ1ZmZlci5hbGxvY1Vuc2FmZVNsb3cgPSBmdW5jdGlvbiAoc2l6ZSkge1xuICByZXR1cm4gYWxsb2NVbnNhZmUobnVsbCwgc2l6ZSlcbn1cblxuZnVuY3Rpb24gZnJvbVN0cmluZyAodGhhdCwgc3RyaW5nLCBlbmNvZGluZykge1xuICBpZiAodHlwZW9mIGVuY29kaW5nICE9PSAnc3RyaW5nJyB8fCBlbmNvZGluZyA9PT0gJycpIHtcbiAgICBlbmNvZGluZyA9ICd1dGY4J1xuICB9XG5cbiAgaWYgKCFCdWZmZXIuaXNFbmNvZGluZyhlbmNvZGluZykpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdcImVuY29kaW5nXCIgbXVzdCBiZSBhIHZhbGlkIHN0cmluZyBlbmNvZGluZycpXG4gIH1cblxuICB2YXIgbGVuZ3RoID0gYnl0ZUxlbmd0aChzdHJpbmcsIGVuY29kaW5nKSB8IDBcbiAgdGhhdCA9IGNyZWF0ZUJ1ZmZlcih0aGF0LCBsZW5ndGgpXG5cbiAgdmFyIGFjdHVhbCA9IHRoYXQud3JpdGUoc3RyaW5nLCBlbmNvZGluZylcblxuICBpZiAoYWN0dWFsICE9PSBsZW5ndGgpIHtcbiAgICAvLyBXcml0aW5nIGEgaGV4IHN0cmluZywgZm9yIGV4YW1wbGUsIHRoYXQgY29udGFpbnMgaW52YWxpZCBjaGFyYWN0ZXJzIHdpbGxcbiAgICAvLyBjYXVzZSBldmVyeXRoaW5nIGFmdGVyIHRoZSBmaXJzdCBpbnZhbGlkIGNoYXJhY3RlciB0byBiZSBpZ25vcmVkLiAoZS5nLlxuICAgIC8vICdhYnh4Y2QnIHdpbGwgYmUgdHJlYXRlZCBhcyAnYWInKVxuICAgIHRoYXQgPSB0aGF0LnNsaWNlKDAsIGFjdHVhbClcbiAgfVxuXG4gIHJldHVybiB0aGF0XG59XG5cbmZ1bmN0aW9uIGZyb21BcnJheUxpa2UgKHRoYXQsIGFycmF5KSB7XG4gIHZhciBsZW5ndGggPSBhcnJheS5sZW5ndGggPCAwID8gMCA6IGNoZWNrZWQoYXJyYXkubGVuZ3RoKSB8IDBcbiAgdGhhdCA9IGNyZWF0ZUJ1ZmZlcih0aGF0LCBsZW5ndGgpXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyBpICs9IDEpIHtcbiAgICB0aGF0W2ldID0gYXJyYXlbaV0gJiAyNTVcbiAgfVxuICByZXR1cm4gdGhhdFxufVxuXG5mdW5jdGlvbiBmcm9tQXJyYXlCdWZmZXIgKHRoYXQsIGFycmF5LCBieXRlT2Zmc2V0LCBsZW5ndGgpIHtcbiAgYXJyYXkuYnl0ZUxlbmd0aCAvLyB0aGlzIHRocm93cyBpZiBgYXJyYXlgIGlzIG5vdCBhIHZhbGlkIEFycmF5QnVmZmVyXG5cbiAgaWYgKGJ5dGVPZmZzZXQgPCAwIHx8IGFycmF5LmJ5dGVMZW5ndGggPCBieXRlT2Zmc2V0KSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1xcJ29mZnNldFxcJyBpcyBvdXQgb2YgYm91bmRzJylcbiAgfVxuXG4gIGlmIChhcnJheS5ieXRlTGVuZ3RoIDwgYnl0ZU9mZnNldCArIChsZW5ndGggfHwgMCkpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignXFwnbGVuZ3RoXFwnIGlzIG91dCBvZiBib3VuZHMnKVxuICB9XG5cbiAgaWYgKGJ5dGVPZmZzZXQgPT09IHVuZGVmaW5lZCAmJiBsZW5ndGggPT09IHVuZGVmaW5lZCkge1xuICAgIGFycmF5ID0gbmV3IFVpbnQ4QXJyYXkoYXJyYXkpXG4gIH0gZWxzZSBpZiAobGVuZ3RoID09PSB1bmRlZmluZWQpIHtcbiAgICBhcnJheSA9IG5ldyBVaW50OEFycmF5KGFycmF5LCBieXRlT2Zmc2V0KVxuICB9IGVsc2Uge1xuICAgIGFycmF5ID0gbmV3IFVpbnQ4QXJyYXkoYXJyYXksIGJ5dGVPZmZzZXQsIGxlbmd0aClcbiAgfVxuXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIC8vIFJldHVybiBhbiBhdWdtZW50ZWQgYFVpbnQ4QXJyYXlgIGluc3RhbmNlLCBmb3IgYmVzdCBwZXJmb3JtYW5jZVxuICAgIHRoYXQgPSBhcnJheVxuICAgIHRoYXQuX19wcm90b19fID0gQnVmZmVyLnByb3RvdHlwZVxuICB9IGVsc2Uge1xuICAgIC8vIEZhbGxiYWNrOiBSZXR1cm4gYW4gb2JqZWN0IGluc3RhbmNlIG9mIHRoZSBCdWZmZXIgY2xhc3NcbiAgICB0aGF0ID0gZnJvbUFycmF5TGlrZSh0aGF0LCBhcnJheSlcbiAgfVxuICByZXR1cm4gdGhhdFxufVxuXG5mdW5jdGlvbiBmcm9tT2JqZWN0ICh0aGF0LCBvYmopIHtcbiAgaWYgKEJ1ZmZlci5pc0J1ZmZlcihvYmopKSB7XG4gICAgdmFyIGxlbiA9IGNoZWNrZWQob2JqLmxlbmd0aCkgfCAwXG4gICAgdGhhdCA9IGNyZWF0ZUJ1ZmZlcih0aGF0LCBsZW4pXG5cbiAgICBpZiAodGhhdC5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiB0aGF0XG4gICAgfVxuXG4gICAgb2JqLmNvcHkodGhhdCwgMCwgMCwgbGVuKVxuICAgIHJldHVybiB0aGF0XG4gIH1cblxuICBpZiAob2JqKSB7XG4gICAgaWYgKCh0eXBlb2YgQXJyYXlCdWZmZXIgIT09ICd1bmRlZmluZWQnICYmXG4gICAgICAgIG9iai5idWZmZXIgaW5zdGFuY2VvZiBBcnJheUJ1ZmZlcikgfHwgJ2xlbmd0aCcgaW4gb2JqKSB7XG4gICAgICBpZiAodHlwZW9mIG9iai5sZW5ndGggIT09ICdudW1iZXInIHx8IGlzbmFuKG9iai5sZW5ndGgpKSB7XG4gICAgICAgIHJldHVybiBjcmVhdGVCdWZmZXIodGhhdCwgMClcbiAgICAgIH1cbiAgICAgIHJldHVybiBmcm9tQXJyYXlMaWtlKHRoYXQsIG9iailcbiAgICB9XG5cbiAgICBpZiAob2JqLnR5cGUgPT09ICdCdWZmZXInICYmIGlzQXJyYXkob2JqLmRhdGEpKSB7XG4gICAgICByZXR1cm4gZnJvbUFycmF5TGlrZSh0aGF0LCBvYmouZGF0YSlcbiAgICB9XG4gIH1cblxuICB0aHJvdyBuZXcgVHlwZUVycm9yKCdGaXJzdCBhcmd1bWVudCBtdXN0IGJlIGEgc3RyaW5nLCBCdWZmZXIsIEFycmF5QnVmZmVyLCBBcnJheSwgb3IgYXJyYXktbGlrZSBvYmplY3QuJylcbn1cblxuZnVuY3Rpb24gY2hlY2tlZCAobGVuZ3RoKSB7XG4gIC8vIE5vdGU6IGNhbm5vdCB1c2UgYGxlbmd0aCA8IGtNYXhMZW5ndGgoKWAgaGVyZSBiZWNhdXNlIHRoYXQgZmFpbHMgd2hlblxuICAvLyBsZW5ndGggaXMgTmFOICh3aGljaCBpcyBvdGhlcndpc2UgY29lcmNlZCB0byB6ZXJvLilcbiAgaWYgKGxlbmd0aCA+PSBrTWF4TGVuZ3RoKCkpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignQXR0ZW1wdCB0byBhbGxvY2F0ZSBCdWZmZXIgbGFyZ2VyIHRoYW4gbWF4aW11bSAnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAnc2l6ZTogMHgnICsga01heExlbmd0aCgpLnRvU3RyaW5nKDE2KSArICcgYnl0ZXMnKVxuICB9XG4gIHJldHVybiBsZW5ndGggfCAwXG59XG5cbmZ1bmN0aW9uIFNsb3dCdWZmZXIgKGxlbmd0aCkge1xuICBpZiAoK2xlbmd0aCAhPSBsZW5ndGgpIHsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBlcWVxZXFcbiAgICBsZW5ndGggPSAwXG4gIH1cbiAgcmV0dXJuIEJ1ZmZlci5hbGxvYygrbGVuZ3RoKVxufVxuXG5CdWZmZXIuaXNCdWZmZXIgPSBmdW5jdGlvbiBpc0J1ZmZlciAoYikge1xuICByZXR1cm4gISEoYiAhPSBudWxsICYmIGIuX2lzQnVmZmVyKVxufVxuXG5CdWZmZXIuY29tcGFyZSA9IGZ1bmN0aW9uIGNvbXBhcmUgKGEsIGIpIHtcbiAgaWYgKCFCdWZmZXIuaXNCdWZmZXIoYSkgfHwgIUJ1ZmZlci5pc0J1ZmZlcihiKSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ0FyZ3VtZW50cyBtdXN0IGJlIEJ1ZmZlcnMnKVxuICB9XG5cbiAgaWYgKGEgPT09IGIpIHJldHVybiAwXG5cbiAgdmFyIHggPSBhLmxlbmd0aFxuICB2YXIgeSA9IGIubGVuZ3RoXG5cbiAgZm9yICh2YXIgaSA9IDAsIGxlbiA9IE1hdGgubWluKHgsIHkpOyBpIDwgbGVuOyArK2kpIHtcbiAgICBpZiAoYVtpXSAhPT0gYltpXSkge1xuICAgICAgeCA9IGFbaV1cbiAgICAgIHkgPSBiW2ldXG4gICAgICBicmVha1xuICAgIH1cbiAgfVxuXG4gIGlmICh4IDwgeSkgcmV0dXJuIC0xXG4gIGlmICh5IDwgeCkgcmV0dXJuIDFcbiAgcmV0dXJuIDBcbn1cblxuQnVmZmVyLmlzRW5jb2RpbmcgPSBmdW5jdGlvbiBpc0VuY29kaW5nIChlbmNvZGluZykge1xuICBzd2l0Y2ggKFN0cmluZyhlbmNvZGluZykudG9Mb3dlckNhc2UoKSkge1xuICAgIGNhc2UgJ2hleCc6XG4gICAgY2FzZSAndXRmOCc6XG4gICAgY2FzZSAndXRmLTgnOlxuICAgIGNhc2UgJ2FzY2lpJzpcbiAgICBjYXNlICdsYXRpbjEnOlxuICAgIGNhc2UgJ2JpbmFyeSc6XG4gICAgY2FzZSAnYmFzZTY0JzpcbiAgICBjYXNlICd1Y3MyJzpcbiAgICBjYXNlICd1Y3MtMic6XG4gICAgY2FzZSAndXRmMTZsZSc6XG4gICAgY2FzZSAndXRmLTE2bGUnOlxuICAgICAgcmV0dXJuIHRydWVcbiAgICBkZWZhdWx0OlxuICAgICAgcmV0dXJuIGZhbHNlXG4gIH1cbn1cblxuQnVmZmVyLmNvbmNhdCA9IGZ1bmN0aW9uIGNvbmNhdCAobGlzdCwgbGVuZ3RoKSB7XG4gIGlmICghaXNBcnJheShsaXN0KSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1wibGlzdFwiIGFyZ3VtZW50IG11c3QgYmUgYW4gQXJyYXkgb2YgQnVmZmVycycpXG4gIH1cblxuICBpZiAobGlzdC5sZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gQnVmZmVyLmFsbG9jKDApXG4gIH1cblxuICB2YXIgaVxuICBpZiAobGVuZ3RoID09PSB1bmRlZmluZWQpIHtcbiAgICBsZW5ndGggPSAwXG4gICAgZm9yIChpID0gMDsgaSA8IGxpc3QubGVuZ3RoOyArK2kpIHtcbiAgICAgIGxlbmd0aCArPSBsaXN0W2ldLmxlbmd0aFxuICAgIH1cbiAgfVxuXG4gIHZhciBidWZmZXIgPSBCdWZmZXIuYWxsb2NVbnNhZmUobGVuZ3RoKVxuICB2YXIgcG9zID0gMFxuICBmb3IgKGkgPSAwOyBpIDwgbGlzdC5sZW5ndGg7ICsraSkge1xuICAgIHZhciBidWYgPSBsaXN0W2ldXG4gICAgaWYgKCFCdWZmZXIuaXNCdWZmZXIoYnVmKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignXCJsaXN0XCIgYXJndW1lbnQgbXVzdCBiZSBhbiBBcnJheSBvZiBCdWZmZXJzJylcbiAgICB9XG4gICAgYnVmLmNvcHkoYnVmZmVyLCBwb3MpXG4gICAgcG9zICs9IGJ1Zi5sZW5ndGhcbiAgfVxuICByZXR1cm4gYnVmZmVyXG59XG5cbmZ1bmN0aW9uIGJ5dGVMZW5ndGggKHN0cmluZywgZW5jb2RpbmcpIHtcbiAgaWYgKEJ1ZmZlci5pc0J1ZmZlcihzdHJpbmcpKSB7XG4gICAgcmV0dXJuIHN0cmluZy5sZW5ndGhcbiAgfVxuICBpZiAodHlwZW9mIEFycmF5QnVmZmVyICE9PSAndW5kZWZpbmVkJyAmJiB0eXBlb2YgQXJyYXlCdWZmZXIuaXNWaWV3ID09PSAnZnVuY3Rpb24nICYmXG4gICAgICAoQXJyYXlCdWZmZXIuaXNWaWV3KHN0cmluZykgfHwgc3RyaW5nIGluc3RhbmNlb2YgQXJyYXlCdWZmZXIpKSB7XG4gICAgcmV0dXJuIHN0cmluZy5ieXRlTGVuZ3RoXG4gIH1cbiAgaWYgKHR5cGVvZiBzdHJpbmcgIT09ICdzdHJpbmcnKSB7XG4gICAgc3RyaW5nID0gJycgKyBzdHJpbmdcbiAgfVxuXG4gIHZhciBsZW4gPSBzdHJpbmcubGVuZ3RoXG4gIGlmIChsZW4gPT09IDApIHJldHVybiAwXG5cbiAgLy8gVXNlIGEgZm9yIGxvb3AgdG8gYXZvaWQgcmVjdXJzaW9uXG4gIHZhciBsb3dlcmVkQ2FzZSA9IGZhbHNlXG4gIGZvciAoOzspIHtcbiAgICBzd2l0Y2ggKGVuY29kaW5nKSB7XG4gICAgICBjYXNlICdhc2NpaSc6XG4gICAgICBjYXNlICdsYXRpbjEnOlxuICAgICAgY2FzZSAnYmluYXJ5JzpcbiAgICAgICAgcmV0dXJuIGxlblxuICAgICAgY2FzZSAndXRmOCc6XG4gICAgICBjYXNlICd1dGYtOCc6XG4gICAgICBjYXNlIHVuZGVmaW5lZDpcbiAgICAgICAgcmV0dXJuIHV0ZjhUb0J5dGVzKHN0cmluZykubGVuZ3RoXG4gICAgICBjYXNlICd1Y3MyJzpcbiAgICAgIGNhc2UgJ3Vjcy0yJzpcbiAgICAgIGNhc2UgJ3V0ZjE2bGUnOlxuICAgICAgY2FzZSAndXRmLTE2bGUnOlxuICAgICAgICByZXR1cm4gbGVuICogMlxuICAgICAgY2FzZSAnaGV4JzpcbiAgICAgICAgcmV0dXJuIGxlbiA+Pj4gMVxuICAgICAgY2FzZSAnYmFzZTY0JzpcbiAgICAgICAgcmV0dXJuIGJhc2U2NFRvQnl0ZXMoc3RyaW5nKS5sZW5ndGhcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGlmIChsb3dlcmVkQ2FzZSkgcmV0dXJuIHV0ZjhUb0J5dGVzKHN0cmluZykubGVuZ3RoIC8vIGFzc3VtZSB1dGY4XG4gICAgICAgIGVuY29kaW5nID0gKCcnICsgZW5jb2RpbmcpLnRvTG93ZXJDYXNlKClcbiAgICAgICAgbG93ZXJlZENhc2UgPSB0cnVlXG4gICAgfVxuICB9XG59XG5CdWZmZXIuYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGhcblxuZnVuY3Rpb24gc2xvd1RvU3RyaW5nIChlbmNvZGluZywgc3RhcnQsIGVuZCkge1xuICB2YXIgbG93ZXJlZENhc2UgPSBmYWxzZVxuXG4gIC8vIE5vIG5lZWQgdG8gdmVyaWZ5IHRoYXQgXCJ0aGlzLmxlbmd0aCA8PSBNQVhfVUlOVDMyXCIgc2luY2UgaXQncyBhIHJlYWQtb25seVxuICAvLyBwcm9wZXJ0eSBvZiBhIHR5cGVkIGFycmF5LlxuXG4gIC8vIFRoaXMgYmVoYXZlcyBuZWl0aGVyIGxpa2UgU3RyaW5nIG5vciBVaW50OEFycmF5IGluIHRoYXQgd2Ugc2V0IHN0YXJ0L2VuZFxuICAvLyB0byB0aGVpciB1cHBlci9sb3dlciBib3VuZHMgaWYgdGhlIHZhbHVlIHBhc3NlZCBpcyBvdXQgb2YgcmFuZ2UuXG4gIC8vIHVuZGVmaW5lZCBpcyBoYW5kbGVkIHNwZWNpYWxseSBhcyBwZXIgRUNNQS0yNjIgNnRoIEVkaXRpb24sXG4gIC8vIFNlY3Rpb24gMTMuMy4zLjcgUnVudGltZSBTZW1hbnRpY3M6IEtleWVkQmluZGluZ0luaXRpYWxpemF0aW9uLlxuICBpZiAoc3RhcnQgPT09IHVuZGVmaW5lZCB8fCBzdGFydCA8IDApIHtcbiAgICBzdGFydCA9IDBcbiAgfVxuICAvLyBSZXR1cm4gZWFybHkgaWYgc3RhcnQgPiB0aGlzLmxlbmd0aC4gRG9uZSBoZXJlIHRvIHByZXZlbnQgcG90ZW50aWFsIHVpbnQzMlxuICAvLyBjb2VyY2lvbiBmYWlsIGJlbG93LlxuICBpZiAoc3RhcnQgPiB0aGlzLmxlbmd0aCkge1xuICAgIHJldHVybiAnJ1xuICB9XG5cbiAgaWYgKGVuZCA9PT0gdW5kZWZpbmVkIHx8IGVuZCA+IHRoaXMubGVuZ3RoKSB7XG4gICAgZW5kID0gdGhpcy5sZW5ndGhcbiAgfVxuXG4gIGlmIChlbmQgPD0gMCkge1xuICAgIHJldHVybiAnJ1xuICB9XG5cbiAgLy8gRm9yY2UgY29lcnNpb24gdG8gdWludDMyLiBUaGlzIHdpbGwgYWxzbyBjb2VyY2UgZmFsc2V5L05hTiB2YWx1ZXMgdG8gMC5cbiAgZW5kID4+Pj0gMFxuICBzdGFydCA+Pj49IDBcblxuICBpZiAoZW5kIDw9IHN0YXJ0KSB7XG4gICAgcmV0dXJuICcnXG4gIH1cblxuICBpZiAoIWVuY29kaW5nKSBlbmNvZGluZyA9ICd1dGY4J1xuXG4gIHdoaWxlICh0cnVlKSB7XG4gICAgc3dpdGNoIChlbmNvZGluZykge1xuICAgICAgY2FzZSAnaGV4JzpcbiAgICAgICAgcmV0dXJuIGhleFNsaWNlKHRoaXMsIHN0YXJ0LCBlbmQpXG5cbiAgICAgIGNhc2UgJ3V0ZjgnOlxuICAgICAgY2FzZSAndXRmLTgnOlxuICAgICAgICByZXR1cm4gdXRmOFNsaWNlKHRoaXMsIHN0YXJ0LCBlbmQpXG5cbiAgICAgIGNhc2UgJ2FzY2lpJzpcbiAgICAgICAgcmV0dXJuIGFzY2lpU2xpY2UodGhpcywgc3RhcnQsIGVuZClcblxuICAgICAgY2FzZSAnbGF0aW4xJzpcbiAgICAgIGNhc2UgJ2JpbmFyeSc6XG4gICAgICAgIHJldHVybiBsYXRpbjFTbGljZSh0aGlzLCBzdGFydCwgZW5kKVxuXG4gICAgICBjYXNlICdiYXNlNjQnOlxuICAgICAgICByZXR1cm4gYmFzZTY0U2xpY2UodGhpcywgc3RhcnQsIGVuZClcblxuICAgICAgY2FzZSAndWNzMic6XG4gICAgICBjYXNlICd1Y3MtMic6XG4gICAgICBjYXNlICd1dGYxNmxlJzpcbiAgICAgIGNhc2UgJ3V0Zi0xNmxlJzpcbiAgICAgICAgcmV0dXJuIHV0ZjE2bGVTbGljZSh0aGlzLCBzdGFydCwgZW5kKVxuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICBpZiAobG93ZXJlZENhc2UpIHRocm93IG5ldyBUeXBlRXJyb3IoJ1Vua25vd24gZW5jb2Rpbmc6ICcgKyBlbmNvZGluZylcbiAgICAgICAgZW5jb2RpbmcgPSAoZW5jb2RpbmcgKyAnJykudG9Mb3dlckNhc2UoKVxuICAgICAgICBsb3dlcmVkQ2FzZSA9IHRydWVcbiAgICB9XG4gIH1cbn1cblxuLy8gVGhlIHByb3BlcnR5IGlzIHVzZWQgYnkgYEJ1ZmZlci5pc0J1ZmZlcmAgYW5kIGBpcy1idWZmZXJgIChpbiBTYWZhcmkgNS03KSB0byBkZXRlY3Rcbi8vIEJ1ZmZlciBpbnN0YW5jZXMuXG5CdWZmZXIucHJvdG90eXBlLl9pc0J1ZmZlciA9IHRydWVcblxuZnVuY3Rpb24gc3dhcCAoYiwgbiwgbSkge1xuICB2YXIgaSA9IGJbbl1cbiAgYltuXSA9IGJbbV1cbiAgYlttXSA9IGlcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5zd2FwMTYgPSBmdW5jdGlvbiBzd2FwMTYgKCkge1xuICB2YXIgbGVuID0gdGhpcy5sZW5ndGhcbiAgaWYgKGxlbiAlIDIgIT09IDApIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignQnVmZmVyIHNpemUgbXVzdCBiZSBhIG11bHRpcGxlIG9mIDE2LWJpdHMnKVxuICB9XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyBpICs9IDIpIHtcbiAgICBzd2FwKHRoaXMsIGksIGkgKyAxKVxuICB9XG4gIHJldHVybiB0aGlzXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuc3dhcDMyID0gZnVuY3Rpb24gc3dhcDMyICgpIHtcbiAgdmFyIGxlbiA9IHRoaXMubGVuZ3RoXG4gIGlmIChsZW4gJSA0ICE9PSAwKSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ0J1ZmZlciBzaXplIG11c3QgYmUgYSBtdWx0aXBsZSBvZiAzMi1iaXRzJylcbiAgfVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgaSArPSA0KSB7XG4gICAgc3dhcCh0aGlzLCBpLCBpICsgMylcbiAgICBzd2FwKHRoaXMsIGkgKyAxLCBpICsgMilcbiAgfVxuICByZXR1cm4gdGhpc1xufVxuXG5CdWZmZXIucHJvdG90eXBlLnN3YXA2NCA9IGZ1bmN0aW9uIHN3YXA2NCAoKSB7XG4gIHZhciBsZW4gPSB0aGlzLmxlbmd0aFxuICBpZiAobGVuICUgOCAhPT0gMCkge1xuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdCdWZmZXIgc2l6ZSBtdXN0IGJlIGEgbXVsdGlwbGUgb2YgNjQtYml0cycpXG4gIH1cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsZW47IGkgKz0gOCkge1xuICAgIHN3YXAodGhpcywgaSwgaSArIDcpXG4gICAgc3dhcCh0aGlzLCBpICsgMSwgaSArIDYpXG4gICAgc3dhcCh0aGlzLCBpICsgMiwgaSArIDUpXG4gICAgc3dhcCh0aGlzLCBpICsgMywgaSArIDQpXG4gIH1cbiAgcmV0dXJuIHRoaXNcbn1cblxuQnVmZmVyLnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uIHRvU3RyaW5nICgpIHtcbiAgdmFyIGxlbmd0aCA9IHRoaXMubGVuZ3RoIHwgMFxuICBpZiAobGVuZ3RoID09PSAwKSByZXR1cm4gJydcbiAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPT09IDApIHJldHVybiB1dGY4U2xpY2UodGhpcywgMCwgbGVuZ3RoKVxuICByZXR1cm4gc2xvd1RvU3RyaW5nLmFwcGx5KHRoaXMsIGFyZ3VtZW50cylcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5lcXVhbHMgPSBmdW5jdGlvbiBlcXVhbHMgKGIpIHtcbiAgaWYgKCFCdWZmZXIuaXNCdWZmZXIoYikpIHRocm93IG5ldyBUeXBlRXJyb3IoJ0FyZ3VtZW50IG11c3QgYmUgYSBCdWZmZXInKVxuICBpZiAodGhpcyA9PT0gYikgcmV0dXJuIHRydWVcbiAgcmV0dXJuIEJ1ZmZlci5jb21wYXJlKHRoaXMsIGIpID09PSAwXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuaW5zcGVjdCA9IGZ1bmN0aW9uIGluc3BlY3QgKCkge1xuICB2YXIgc3RyID0gJydcbiAgdmFyIG1heCA9IGV4cG9ydHMuSU5TUEVDVF9NQVhfQllURVNcbiAgaWYgKHRoaXMubGVuZ3RoID4gMCkge1xuICAgIHN0ciA9IHRoaXMudG9TdHJpbmcoJ2hleCcsIDAsIG1heCkubWF0Y2goLy57Mn0vZykuam9pbignICcpXG4gICAgaWYgKHRoaXMubGVuZ3RoID4gbWF4KSBzdHIgKz0gJyAuLi4gJ1xuICB9XG4gIHJldHVybiAnPEJ1ZmZlciAnICsgc3RyICsgJz4nXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUuY29tcGFyZSA9IGZ1bmN0aW9uIGNvbXBhcmUgKHRhcmdldCwgc3RhcnQsIGVuZCwgdGhpc1N0YXJ0LCB0aGlzRW5kKSB7XG4gIGlmICghQnVmZmVyLmlzQnVmZmVyKHRhcmdldCkpIHtcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdBcmd1bWVudCBtdXN0IGJlIGEgQnVmZmVyJylcbiAgfVxuXG4gIGlmIChzdGFydCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgc3RhcnQgPSAwXG4gIH1cbiAgaWYgKGVuZCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgZW5kID0gdGFyZ2V0ID8gdGFyZ2V0Lmxlbmd0aCA6IDBcbiAgfVxuICBpZiAodGhpc1N0YXJ0ID09PSB1bmRlZmluZWQpIHtcbiAgICB0aGlzU3RhcnQgPSAwXG4gIH1cbiAgaWYgKHRoaXNFbmQgPT09IHVuZGVmaW5lZCkge1xuICAgIHRoaXNFbmQgPSB0aGlzLmxlbmd0aFxuICB9XG5cbiAgaWYgKHN0YXJ0IDwgMCB8fCBlbmQgPiB0YXJnZXQubGVuZ3RoIHx8IHRoaXNTdGFydCA8IDAgfHwgdGhpc0VuZCA+IHRoaXMubGVuZ3RoKSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ291dCBvZiByYW5nZSBpbmRleCcpXG4gIH1cblxuICBpZiAodGhpc1N0YXJ0ID49IHRoaXNFbmQgJiYgc3RhcnQgPj0gZW5kKSB7XG4gICAgcmV0dXJuIDBcbiAgfVxuICBpZiAodGhpc1N0YXJ0ID49IHRoaXNFbmQpIHtcbiAgICByZXR1cm4gLTFcbiAgfVxuICBpZiAoc3RhcnQgPj0gZW5kKSB7XG4gICAgcmV0dXJuIDFcbiAgfVxuXG4gIHN0YXJ0ID4+Pj0gMFxuICBlbmQgPj4+PSAwXG4gIHRoaXNTdGFydCA+Pj49IDBcbiAgdGhpc0VuZCA+Pj49IDBcblxuICBpZiAodGhpcyA9PT0gdGFyZ2V0KSByZXR1cm4gMFxuXG4gIHZhciB4ID0gdGhpc0VuZCAtIHRoaXNTdGFydFxuICB2YXIgeSA9IGVuZCAtIHN0YXJ0XG4gIHZhciBsZW4gPSBNYXRoLm1pbih4LCB5KVxuXG4gIHZhciB0aGlzQ29weSA9IHRoaXMuc2xpY2UodGhpc1N0YXJ0LCB0aGlzRW5kKVxuICB2YXIgdGFyZ2V0Q29weSA9IHRhcmdldC5zbGljZShzdGFydCwgZW5kKVxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuOyArK2kpIHtcbiAgICBpZiAodGhpc0NvcHlbaV0gIT09IHRhcmdldENvcHlbaV0pIHtcbiAgICAgIHggPSB0aGlzQ29weVtpXVxuICAgICAgeSA9IHRhcmdldENvcHlbaV1cbiAgICAgIGJyZWFrXG4gICAgfVxuICB9XG5cbiAgaWYgKHggPCB5KSByZXR1cm4gLTFcbiAgaWYgKHkgPCB4KSByZXR1cm4gMVxuICByZXR1cm4gMFxufVxuXG4vLyBGaW5kcyBlaXRoZXIgdGhlIGZpcnN0IGluZGV4IG9mIGB2YWxgIGluIGBidWZmZXJgIGF0IG9mZnNldCA+PSBgYnl0ZU9mZnNldGAsXG4vLyBPUiB0aGUgbGFzdCBpbmRleCBvZiBgdmFsYCBpbiBgYnVmZmVyYCBhdCBvZmZzZXQgPD0gYGJ5dGVPZmZzZXRgLlxuLy9cbi8vIEFyZ3VtZW50czpcbi8vIC0gYnVmZmVyIC0gYSBCdWZmZXIgdG8gc2VhcmNoXG4vLyAtIHZhbCAtIGEgc3RyaW5nLCBCdWZmZXIsIG9yIG51bWJlclxuLy8gLSBieXRlT2Zmc2V0IC0gYW4gaW5kZXggaW50byBgYnVmZmVyYDsgd2lsbCBiZSBjbGFtcGVkIHRvIGFuIGludDMyXG4vLyAtIGVuY29kaW5nIC0gYW4gb3B0aW9uYWwgZW5jb2RpbmcsIHJlbGV2YW50IGlzIHZhbCBpcyBhIHN0cmluZ1xuLy8gLSBkaXIgLSB0cnVlIGZvciBpbmRleE9mLCBmYWxzZSBmb3IgbGFzdEluZGV4T2ZcbmZ1bmN0aW9uIGJpZGlyZWN0aW9uYWxJbmRleE9mIChidWZmZXIsIHZhbCwgYnl0ZU9mZnNldCwgZW5jb2RpbmcsIGRpcikge1xuICAvLyBFbXB0eSBidWZmZXIgbWVhbnMgbm8gbWF0Y2hcbiAgaWYgKGJ1ZmZlci5sZW5ndGggPT09IDApIHJldHVybiAtMVxuXG4gIC8vIE5vcm1hbGl6ZSBieXRlT2Zmc2V0XG4gIGlmICh0eXBlb2YgYnl0ZU9mZnNldCA9PT0gJ3N0cmluZycpIHtcbiAgICBlbmNvZGluZyA9IGJ5dGVPZmZzZXRcbiAgICBieXRlT2Zmc2V0ID0gMFxuICB9IGVsc2UgaWYgKGJ5dGVPZmZzZXQgPiAweDdmZmZmZmZmKSB7XG4gICAgYnl0ZU9mZnNldCA9IDB4N2ZmZmZmZmZcbiAgfSBlbHNlIGlmIChieXRlT2Zmc2V0IDwgLTB4ODAwMDAwMDApIHtcbiAgICBieXRlT2Zmc2V0ID0gLTB4ODAwMDAwMDBcbiAgfVxuICBieXRlT2Zmc2V0ID0gK2J5dGVPZmZzZXQgIC8vIENvZXJjZSB0byBOdW1iZXIuXG4gIGlmIChpc05hTihieXRlT2Zmc2V0KSkge1xuICAgIC8vIGJ5dGVPZmZzZXQ6IGl0IGl0J3MgdW5kZWZpbmVkLCBudWxsLCBOYU4sIFwiZm9vXCIsIGV0Yywgc2VhcmNoIHdob2xlIGJ1ZmZlclxuICAgIGJ5dGVPZmZzZXQgPSBkaXIgPyAwIDogKGJ1ZmZlci5sZW5ndGggLSAxKVxuICB9XG5cbiAgLy8gTm9ybWFsaXplIGJ5dGVPZmZzZXQ6IG5lZ2F0aXZlIG9mZnNldHMgc3RhcnQgZnJvbSB0aGUgZW5kIG9mIHRoZSBidWZmZXJcbiAgaWYgKGJ5dGVPZmZzZXQgPCAwKSBieXRlT2Zmc2V0ID0gYnVmZmVyLmxlbmd0aCArIGJ5dGVPZmZzZXRcbiAgaWYgKGJ5dGVPZmZzZXQgPj0gYnVmZmVyLmxlbmd0aCkge1xuICAgIGlmIChkaXIpIHJldHVybiAtMVxuICAgIGVsc2UgYnl0ZU9mZnNldCA9IGJ1ZmZlci5sZW5ndGggLSAxXG4gIH0gZWxzZSBpZiAoYnl0ZU9mZnNldCA8IDApIHtcbiAgICBpZiAoZGlyKSBieXRlT2Zmc2V0ID0gMFxuICAgIGVsc2UgcmV0dXJuIC0xXG4gIH1cblxuICAvLyBOb3JtYWxpemUgdmFsXG4gIGlmICh0eXBlb2YgdmFsID09PSAnc3RyaW5nJykge1xuICAgIHZhbCA9IEJ1ZmZlci5mcm9tKHZhbCwgZW5jb2RpbmcpXG4gIH1cblxuICAvLyBGaW5hbGx5LCBzZWFyY2ggZWl0aGVyIGluZGV4T2YgKGlmIGRpciBpcyB0cnVlKSBvciBsYXN0SW5kZXhPZlxuICBpZiAoQnVmZmVyLmlzQnVmZmVyKHZhbCkpIHtcbiAgICAvLyBTcGVjaWFsIGNhc2U6IGxvb2tpbmcgZm9yIGVtcHR5IHN0cmluZy9idWZmZXIgYWx3YXlzIGZhaWxzXG4gICAgaWYgKHZhbC5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiAtMVxuICAgIH1cbiAgICByZXR1cm4gYXJyYXlJbmRleE9mKGJ1ZmZlciwgdmFsLCBieXRlT2Zmc2V0LCBlbmNvZGluZywgZGlyKVxuICB9IGVsc2UgaWYgKHR5cGVvZiB2YWwgPT09ICdudW1iZXInKSB7XG4gICAgdmFsID0gdmFsICYgMHhGRiAvLyBTZWFyY2ggZm9yIGEgYnl0ZSB2YWx1ZSBbMC0yNTVdXG4gICAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUICYmXG4gICAgICAgIHR5cGVvZiBVaW50OEFycmF5LnByb3RvdHlwZS5pbmRleE9mID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBpZiAoZGlyKSB7XG4gICAgICAgIHJldHVybiBVaW50OEFycmF5LnByb3RvdHlwZS5pbmRleE9mLmNhbGwoYnVmZmVyLCB2YWwsIGJ5dGVPZmZzZXQpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICByZXR1cm4gVWludDhBcnJheS5wcm90b3R5cGUubGFzdEluZGV4T2YuY2FsbChidWZmZXIsIHZhbCwgYnl0ZU9mZnNldClcbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGFycmF5SW5kZXhPZihidWZmZXIsIFsgdmFsIF0sIGJ5dGVPZmZzZXQsIGVuY29kaW5nLCBkaXIpXG4gIH1cblxuICB0aHJvdyBuZXcgVHlwZUVycm9yKCd2YWwgbXVzdCBiZSBzdHJpbmcsIG51bWJlciBvciBCdWZmZXInKVxufVxuXG5mdW5jdGlvbiBhcnJheUluZGV4T2YgKGFyciwgdmFsLCBieXRlT2Zmc2V0LCBlbmNvZGluZywgZGlyKSB7XG4gIHZhciBpbmRleFNpemUgPSAxXG4gIHZhciBhcnJMZW5ndGggPSBhcnIubGVuZ3RoXG4gIHZhciB2YWxMZW5ndGggPSB2YWwubGVuZ3RoXG5cbiAgaWYgKGVuY29kaW5nICE9PSB1bmRlZmluZWQpIHtcbiAgICBlbmNvZGluZyA9IFN0cmluZyhlbmNvZGluZykudG9Mb3dlckNhc2UoKVxuICAgIGlmIChlbmNvZGluZyA9PT0gJ3VjczInIHx8IGVuY29kaW5nID09PSAndWNzLTInIHx8XG4gICAgICAgIGVuY29kaW5nID09PSAndXRmMTZsZScgfHwgZW5jb2RpbmcgPT09ICd1dGYtMTZsZScpIHtcbiAgICAgIGlmIChhcnIubGVuZ3RoIDwgMiB8fCB2YWwubGVuZ3RoIDwgMikge1xuICAgICAgICByZXR1cm4gLTFcbiAgICAgIH1cbiAgICAgIGluZGV4U2l6ZSA9IDJcbiAgICAgIGFyckxlbmd0aCAvPSAyXG4gICAgICB2YWxMZW5ndGggLz0gMlxuICAgICAgYnl0ZU9mZnNldCAvPSAyXG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gcmVhZCAoYnVmLCBpKSB7XG4gICAgaWYgKGluZGV4U2l6ZSA9PT0gMSkge1xuICAgICAgcmV0dXJuIGJ1ZltpXVxuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gYnVmLnJlYWRVSW50MTZCRShpICogaW5kZXhTaXplKVxuICAgIH1cbiAgfVxuXG4gIHZhciBpXG4gIGlmIChkaXIpIHtcbiAgICB2YXIgZm91bmRJbmRleCA9IC0xXG4gICAgZm9yIChpID0gYnl0ZU9mZnNldDsgaSA8IGFyckxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAocmVhZChhcnIsIGkpID09PSByZWFkKHZhbCwgZm91bmRJbmRleCA9PT0gLTEgPyAwIDogaSAtIGZvdW5kSW5kZXgpKSB7XG4gICAgICAgIGlmIChmb3VuZEluZGV4ID09PSAtMSkgZm91bmRJbmRleCA9IGlcbiAgICAgICAgaWYgKGkgLSBmb3VuZEluZGV4ICsgMSA9PT0gdmFsTGVuZ3RoKSByZXR1cm4gZm91bmRJbmRleCAqIGluZGV4U2l6ZVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKGZvdW5kSW5kZXggIT09IC0xKSBpIC09IGkgLSBmb3VuZEluZGV4XG4gICAgICAgIGZvdW5kSW5kZXggPSAtMVxuICAgICAgfVxuICAgIH1cbiAgfSBlbHNlIHtcbiAgICBpZiAoYnl0ZU9mZnNldCArIHZhbExlbmd0aCA+IGFyckxlbmd0aCkgYnl0ZU9mZnNldCA9IGFyckxlbmd0aCAtIHZhbExlbmd0aFxuICAgIGZvciAoaSA9IGJ5dGVPZmZzZXQ7IGkgPj0gMDsgaS0tKSB7XG4gICAgICB2YXIgZm91bmQgPSB0cnVlXG4gICAgICBmb3IgKHZhciBqID0gMDsgaiA8IHZhbExlbmd0aDsgaisrKSB7XG4gICAgICAgIGlmIChyZWFkKGFyciwgaSArIGopICE9PSByZWFkKHZhbCwgaikpIHtcbiAgICAgICAgICBmb3VuZCA9IGZhbHNlXG4gICAgICAgICAgYnJlYWtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKGZvdW5kKSByZXR1cm4gaVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiAtMVxufVxuXG5CdWZmZXIucHJvdG90eXBlLmluY2x1ZGVzID0gZnVuY3Rpb24gaW5jbHVkZXMgKHZhbCwgYnl0ZU9mZnNldCwgZW5jb2RpbmcpIHtcbiAgcmV0dXJuIHRoaXMuaW5kZXhPZih2YWwsIGJ5dGVPZmZzZXQsIGVuY29kaW5nKSAhPT0gLTFcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5pbmRleE9mID0gZnVuY3Rpb24gaW5kZXhPZiAodmFsLCBieXRlT2Zmc2V0LCBlbmNvZGluZykge1xuICByZXR1cm4gYmlkaXJlY3Rpb25hbEluZGV4T2YodGhpcywgdmFsLCBieXRlT2Zmc2V0LCBlbmNvZGluZywgdHJ1ZSlcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5sYXN0SW5kZXhPZiA9IGZ1bmN0aW9uIGxhc3RJbmRleE9mICh2YWwsIGJ5dGVPZmZzZXQsIGVuY29kaW5nKSB7XG4gIHJldHVybiBiaWRpcmVjdGlvbmFsSW5kZXhPZih0aGlzLCB2YWwsIGJ5dGVPZmZzZXQsIGVuY29kaW5nLCBmYWxzZSlcbn1cblxuZnVuY3Rpb24gaGV4V3JpdGUgKGJ1Ziwgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aCkge1xuICBvZmZzZXQgPSBOdW1iZXIob2Zmc2V0KSB8fCAwXG4gIHZhciByZW1haW5pbmcgPSBidWYubGVuZ3RoIC0gb2Zmc2V0XG4gIGlmICghbGVuZ3RoKSB7XG4gICAgbGVuZ3RoID0gcmVtYWluaW5nXG4gIH0gZWxzZSB7XG4gICAgbGVuZ3RoID0gTnVtYmVyKGxlbmd0aClcbiAgICBpZiAobGVuZ3RoID4gcmVtYWluaW5nKSB7XG4gICAgICBsZW5ndGggPSByZW1haW5pbmdcbiAgICB9XG4gIH1cblxuICAvLyBtdXN0IGJlIGFuIGV2ZW4gbnVtYmVyIG9mIGRpZ2l0c1xuICB2YXIgc3RyTGVuID0gc3RyaW5nLmxlbmd0aFxuICBpZiAoc3RyTGVuICUgMiAhPT0gMCkgdGhyb3cgbmV3IFR5cGVFcnJvcignSW52YWxpZCBoZXggc3RyaW5nJylcblxuICBpZiAobGVuZ3RoID4gc3RyTGVuIC8gMikge1xuICAgIGxlbmd0aCA9IHN0ckxlbiAvIDJcbiAgfVxuICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbmd0aDsgKytpKSB7XG4gICAgdmFyIHBhcnNlZCA9IHBhcnNlSW50KHN0cmluZy5zdWJzdHIoaSAqIDIsIDIpLCAxNilcbiAgICBpZiAoaXNOYU4ocGFyc2VkKSkgcmV0dXJuIGlcbiAgICBidWZbb2Zmc2V0ICsgaV0gPSBwYXJzZWRcbiAgfVxuICByZXR1cm4gaVxufVxuXG5mdW5jdGlvbiB1dGY4V3JpdGUgKGJ1Ziwgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aCkge1xuICByZXR1cm4gYmxpdEJ1ZmZlcih1dGY4VG9CeXRlcyhzdHJpbmcsIGJ1Zi5sZW5ndGggLSBvZmZzZXQpLCBidWYsIG9mZnNldCwgbGVuZ3RoKVxufVxuXG5mdW5jdGlvbiBhc2NpaVdyaXRlIChidWYsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpIHtcbiAgcmV0dXJuIGJsaXRCdWZmZXIoYXNjaWlUb0J5dGVzKHN0cmluZyksIGJ1Ziwgb2Zmc2V0LCBsZW5ndGgpXG59XG5cbmZ1bmN0aW9uIGxhdGluMVdyaXRlIChidWYsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpIHtcbiAgcmV0dXJuIGFzY2lpV3JpdGUoYnVmLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKVxufVxuXG5mdW5jdGlvbiBiYXNlNjRXcml0ZSAoYnVmLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKSB7XG4gIHJldHVybiBibGl0QnVmZmVyKGJhc2U2NFRvQnl0ZXMoc3RyaW5nKSwgYnVmLCBvZmZzZXQsIGxlbmd0aClcbn1cblxuZnVuY3Rpb24gdWNzMldyaXRlIChidWYsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpIHtcbiAgcmV0dXJuIGJsaXRCdWZmZXIodXRmMTZsZVRvQnl0ZXMoc3RyaW5nLCBidWYubGVuZ3RoIC0gb2Zmc2V0KSwgYnVmLCBvZmZzZXQsIGxlbmd0aClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZSA9IGZ1bmN0aW9uIHdyaXRlIChzdHJpbmcsIG9mZnNldCwgbGVuZ3RoLCBlbmNvZGluZykge1xuICAvLyBCdWZmZXIjd3JpdGUoc3RyaW5nKVxuICBpZiAob2Zmc2V0ID09PSB1bmRlZmluZWQpIHtcbiAgICBlbmNvZGluZyA9ICd1dGY4J1xuICAgIGxlbmd0aCA9IHRoaXMubGVuZ3RoXG4gICAgb2Zmc2V0ID0gMFxuICAvLyBCdWZmZXIjd3JpdGUoc3RyaW5nLCBlbmNvZGluZylcbiAgfSBlbHNlIGlmIChsZW5ndGggPT09IHVuZGVmaW5lZCAmJiB0eXBlb2Ygb2Zmc2V0ID09PSAnc3RyaW5nJykge1xuICAgIGVuY29kaW5nID0gb2Zmc2V0XG4gICAgbGVuZ3RoID0gdGhpcy5sZW5ndGhcbiAgICBvZmZzZXQgPSAwXG4gIC8vIEJ1ZmZlciN3cml0ZShzdHJpbmcsIG9mZnNldFssIGxlbmd0aF1bLCBlbmNvZGluZ10pXG4gIH0gZWxzZSBpZiAoaXNGaW5pdGUob2Zmc2V0KSkge1xuICAgIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgICBpZiAoaXNGaW5pdGUobGVuZ3RoKSkge1xuICAgICAgbGVuZ3RoID0gbGVuZ3RoIHwgMFxuICAgICAgaWYgKGVuY29kaW5nID09PSB1bmRlZmluZWQpIGVuY29kaW5nID0gJ3V0ZjgnXG4gICAgfSBlbHNlIHtcbiAgICAgIGVuY29kaW5nID0gbGVuZ3RoXG4gICAgICBsZW5ndGggPSB1bmRlZmluZWRcbiAgICB9XG4gIC8vIGxlZ2FjeSB3cml0ZShzdHJpbmcsIGVuY29kaW5nLCBvZmZzZXQsIGxlbmd0aCkgLSByZW1vdmUgaW4gdjAuMTNcbiAgfSBlbHNlIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAnQnVmZmVyLndyaXRlKHN0cmluZywgZW5jb2RpbmcsIG9mZnNldFssIGxlbmd0aF0pIGlzIG5vIGxvbmdlciBzdXBwb3J0ZWQnXG4gICAgKVxuICB9XG5cbiAgdmFyIHJlbWFpbmluZyA9IHRoaXMubGVuZ3RoIC0gb2Zmc2V0XG4gIGlmIChsZW5ndGggPT09IHVuZGVmaW5lZCB8fCBsZW5ndGggPiByZW1haW5pbmcpIGxlbmd0aCA9IHJlbWFpbmluZ1xuXG4gIGlmICgoc3RyaW5nLmxlbmd0aCA+IDAgJiYgKGxlbmd0aCA8IDAgfHwgb2Zmc2V0IDwgMCkpIHx8IG9mZnNldCA+IHRoaXMubGVuZ3RoKSB7XG4gICAgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ0F0dGVtcHQgdG8gd3JpdGUgb3V0c2lkZSBidWZmZXIgYm91bmRzJylcbiAgfVxuXG4gIGlmICghZW5jb2RpbmcpIGVuY29kaW5nID0gJ3V0ZjgnXG5cbiAgdmFyIGxvd2VyZWRDYXNlID0gZmFsc2VcbiAgZm9yICg7Oykge1xuICAgIHN3aXRjaCAoZW5jb2RpbmcpIHtcbiAgICAgIGNhc2UgJ2hleCc6XG4gICAgICAgIHJldHVybiBoZXhXcml0ZSh0aGlzLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKVxuXG4gICAgICBjYXNlICd1dGY4JzpcbiAgICAgIGNhc2UgJ3V0Zi04JzpcbiAgICAgICAgcmV0dXJuIHV0ZjhXcml0ZSh0aGlzLCBzdHJpbmcsIG9mZnNldCwgbGVuZ3RoKVxuXG4gICAgICBjYXNlICdhc2NpaSc6XG4gICAgICAgIHJldHVybiBhc2NpaVdyaXRlKHRoaXMsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpXG5cbiAgICAgIGNhc2UgJ2xhdGluMSc6XG4gICAgICBjYXNlICdiaW5hcnknOlxuICAgICAgICByZXR1cm4gbGF0aW4xV3JpdGUodGhpcywgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aClcblxuICAgICAgY2FzZSAnYmFzZTY0JzpcbiAgICAgICAgLy8gV2FybmluZzogbWF4TGVuZ3RoIG5vdCB0YWtlbiBpbnRvIGFjY291bnQgaW4gYmFzZTY0V3JpdGVcbiAgICAgICAgcmV0dXJuIGJhc2U2NFdyaXRlKHRoaXMsIHN0cmluZywgb2Zmc2V0LCBsZW5ndGgpXG5cbiAgICAgIGNhc2UgJ3VjczInOlxuICAgICAgY2FzZSAndWNzLTInOlxuICAgICAgY2FzZSAndXRmMTZsZSc6XG4gICAgICBjYXNlICd1dGYtMTZsZSc6XG4gICAgICAgIHJldHVybiB1Y3MyV3JpdGUodGhpcywgc3RyaW5nLCBvZmZzZXQsIGxlbmd0aClcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgaWYgKGxvd2VyZWRDYXNlKSB0aHJvdyBuZXcgVHlwZUVycm9yKCdVbmtub3duIGVuY29kaW5nOiAnICsgZW5jb2RpbmcpXG4gICAgICAgIGVuY29kaW5nID0gKCcnICsgZW5jb2RpbmcpLnRvTG93ZXJDYXNlKClcbiAgICAgICAgbG93ZXJlZENhc2UgPSB0cnVlXG4gICAgfVxuICB9XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUudG9KU09OID0gZnVuY3Rpb24gdG9KU09OICgpIHtcbiAgcmV0dXJuIHtcbiAgICB0eXBlOiAnQnVmZmVyJyxcbiAgICBkYXRhOiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0aGlzLl9hcnIgfHwgdGhpcywgMClcbiAgfVxufVxuXG5mdW5jdGlvbiBiYXNlNjRTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIGlmIChzdGFydCA9PT0gMCAmJiBlbmQgPT09IGJ1Zi5sZW5ndGgpIHtcbiAgICByZXR1cm4gYmFzZTY0LmZyb21CeXRlQXJyYXkoYnVmKVxuICB9IGVsc2Uge1xuICAgIHJldHVybiBiYXNlNjQuZnJvbUJ5dGVBcnJheShidWYuc2xpY2Uoc3RhcnQsIGVuZCkpXG4gIH1cbn1cblxuZnVuY3Rpb24gdXRmOFNsaWNlIChidWYsIHN0YXJ0LCBlbmQpIHtcbiAgZW5kID0gTWF0aC5taW4oYnVmLmxlbmd0aCwgZW5kKVxuICB2YXIgcmVzID0gW11cblxuICB2YXIgaSA9IHN0YXJ0XG4gIHdoaWxlIChpIDwgZW5kKSB7XG4gICAgdmFyIGZpcnN0Qnl0ZSA9IGJ1ZltpXVxuICAgIHZhciBjb2RlUG9pbnQgPSBudWxsXG4gICAgdmFyIGJ5dGVzUGVyU2VxdWVuY2UgPSAoZmlyc3RCeXRlID4gMHhFRikgPyA0XG4gICAgICA6IChmaXJzdEJ5dGUgPiAweERGKSA/IDNcbiAgICAgIDogKGZpcnN0Qnl0ZSA+IDB4QkYpID8gMlxuICAgICAgOiAxXG5cbiAgICBpZiAoaSArIGJ5dGVzUGVyU2VxdWVuY2UgPD0gZW5kKSB7XG4gICAgICB2YXIgc2Vjb25kQnl0ZSwgdGhpcmRCeXRlLCBmb3VydGhCeXRlLCB0ZW1wQ29kZVBvaW50XG5cbiAgICAgIHN3aXRjaCAoYnl0ZXNQZXJTZXF1ZW5jZSkge1xuICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgaWYgKGZpcnN0Qnl0ZSA8IDB4ODApIHtcbiAgICAgICAgICAgIGNvZGVQb2ludCA9IGZpcnN0Qnl0ZVxuICAgICAgICAgIH1cbiAgICAgICAgICBicmVha1xuICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgc2Vjb25kQnl0ZSA9IGJ1ZltpICsgMV1cbiAgICAgICAgICBpZiAoKHNlY29uZEJ5dGUgJiAweEMwKSA9PT0gMHg4MCkge1xuICAgICAgICAgICAgdGVtcENvZGVQb2ludCA9IChmaXJzdEJ5dGUgJiAweDFGKSA8PCAweDYgfCAoc2Vjb25kQnl0ZSAmIDB4M0YpXG4gICAgICAgICAgICBpZiAodGVtcENvZGVQb2ludCA+IDB4N0YpIHtcbiAgICAgICAgICAgICAgY29kZVBvaW50ID0gdGVtcENvZGVQb2ludFxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBicmVha1xuICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgc2Vjb25kQnl0ZSA9IGJ1ZltpICsgMV1cbiAgICAgICAgICB0aGlyZEJ5dGUgPSBidWZbaSArIDJdXG4gICAgICAgICAgaWYgKChzZWNvbmRCeXRlICYgMHhDMCkgPT09IDB4ODAgJiYgKHRoaXJkQnl0ZSAmIDB4QzApID09PSAweDgwKSB7XG4gICAgICAgICAgICB0ZW1wQ29kZVBvaW50ID0gKGZpcnN0Qnl0ZSAmIDB4RikgPDwgMHhDIHwgKHNlY29uZEJ5dGUgJiAweDNGKSA8PCAweDYgfCAodGhpcmRCeXRlICYgMHgzRilcbiAgICAgICAgICAgIGlmICh0ZW1wQ29kZVBvaW50ID4gMHg3RkYgJiYgKHRlbXBDb2RlUG9pbnQgPCAweEQ4MDAgfHwgdGVtcENvZGVQb2ludCA+IDB4REZGRikpIHtcbiAgICAgICAgICAgICAgY29kZVBvaW50ID0gdGVtcENvZGVQb2ludFxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBicmVha1xuICAgICAgICBjYXNlIDQ6XG4gICAgICAgICAgc2Vjb25kQnl0ZSA9IGJ1ZltpICsgMV1cbiAgICAgICAgICB0aGlyZEJ5dGUgPSBidWZbaSArIDJdXG4gICAgICAgICAgZm91cnRoQnl0ZSA9IGJ1ZltpICsgM11cbiAgICAgICAgICBpZiAoKHNlY29uZEJ5dGUgJiAweEMwKSA9PT0gMHg4MCAmJiAodGhpcmRCeXRlICYgMHhDMCkgPT09IDB4ODAgJiYgKGZvdXJ0aEJ5dGUgJiAweEMwKSA9PT0gMHg4MCkge1xuICAgICAgICAgICAgdGVtcENvZGVQb2ludCA9IChmaXJzdEJ5dGUgJiAweEYpIDw8IDB4MTIgfCAoc2Vjb25kQnl0ZSAmIDB4M0YpIDw8IDB4QyB8ICh0aGlyZEJ5dGUgJiAweDNGKSA8PCAweDYgfCAoZm91cnRoQnl0ZSAmIDB4M0YpXG4gICAgICAgICAgICBpZiAodGVtcENvZGVQb2ludCA+IDB4RkZGRiAmJiB0ZW1wQ29kZVBvaW50IDwgMHgxMTAwMDApIHtcbiAgICAgICAgICAgICAgY29kZVBvaW50ID0gdGVtcENvZGVQb2ludFxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoY29kZVBvaW50ID09PSBudWxsKSB7XG4gICAgICAvLyB3ZSBkaWQgbm90IGdlbmVyYXRlIGEgdmFsaWQgY29kZVBvaW50IHNvIGluc2VydCBhXG4gICAgICAvLyByZXBsYWNlbWVudCBjaGFyIChVK0ZGRkQpIGFuZCBhZHZhbmNlIG9ubHkgMSBieXRlXG4gICAgICBjb2RlUG9pbnQgPSAweEZGRkRcbiAgICAgIGJ5dGVzUGVyU2VxdWVuY2UgPSAxXG4gICAgfSBlbHNlIGlmIChjb2RlUG9pbnQgPiAweEZGRkYpIHtcbiAgICAgIC8vIGVuY29kZSB0byB1dGYxNiAoc3Vycm9nYXRlIHBhaXIgZGFuY2UpXG4gICAgICBjb2RlUG9pbnQgLT0gMHgxMDAwMFxuICAgICAgcmVzLnB1c2goY29kZVBvaW50ID4+PiAxMCAmIDB4M0ZGIHwgMHhEODAwKVxuICAgICAgY29kZVBvaW50ID0gMHhEQzAwIHwgY29kZVBvaW50ICYgMHgzRkZcbiAgICB9XG5cbiAgICByZXMucHVzaChjb2RlUG9pbnQpXG4gICAgaSArPSBieXRlc1BlclNlcXVlbmNlXG4gIH1cblxuICByZXR1cm4gZGVjb2RlQ29kZVBvaW50c0FycmF5KHJlcylcbn1cblxuLy8gQmFzZWQgb24gaHR0cDovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMjI3NDcyNzIvNjgwNzQyLCB0aGUgYnJvd3NlciB3aXRoXG4vLyB0aGUgbG93ZXN0IGxpbWl0IGlzIENocm9tZSwgd2l0aCAweDEwMDAwIGFyZ3MuXG4vLyBXZSBnbyAxIG1hZ25pdHVkZSBsZXNzLCBmb3Igc2FmZXR5XG52YXIgTUFYX0FSR1VNRU5UU19MRU5HVEggPSAweDEwMDBcblxuZnVuY3Rpb24gZGVjb2RlQ29kZVBvaW50c0FycmF5IChjb2RlUG9pbnRzKSB7XG4gIHZhciBsZW4gPSBjb2RlUG9pbnRzLmxlbmd0aFxuICBpZiAobGVuIDw9IE1BWF9BUkdVTUVOVFNfTEVOR1RIKSB7XG4gICAgcmV0dXJuIFN0cmluZy5mcm9tQ2hhckNvZGUuYXBwbHkoU3RyaW5nLCBjb2RlUG9pbnRzKSAvLyBhdm9pZCBleHRyYSBzbGljZSgpXG4gIH1cblxuICAvLyBEZWNvZGUgaW4gY2h1bmtzIHRvIGF2b2lkIFwiY2FsbCBzdGFjayBzaXplIGV4Y2VlZGVkXCIuXG4gIHZhciByZXMgPSAnJ1xuICB2YXIgaSA9IDBcbiAgd2hpbGUgKGkgPCBsZW4pIHtcbiAgICByZXMgKz0gU3RyaW5nLmZyb21DaGFyQ29kZS5hcHBseShcbiAgICAgIFN0cmluZyxcbiAgICAgIGNvZGVQb2ludHMuc2xpY2UoaSwgaSArPSBNQVhfQVJHVU1FTlRTX0xFTkdUSClcbiAgICApXG4gIH1cbiAgcmV0dXJuIHJlc1xufVxuXG5mdW5jdGlvbiBhc2NpaVNsaWNlIChidWYsIHN0YXJ0LCBlbmQpIHtcbiAgdmFyIHJldCA9ICcnXG4gIGVuZCA9IE1hdGgubWluKGJ1Zi5sZW5ndGgsIGVuZClcblxuICBmb3IgKHZhciBpID0gc3RhcnQ7IGkgPCBlbmQ7ICsraSkge1xuICAgIHJldCArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGJ1ZltpXSAmIDB4N0YpXG4gIH1cbiAgcmV0dXJuIHJldFxufVxuXG5mdW5jdGlvbiBsYXRpbjFTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIHZhciByZXQgPSAnJ1xuICBlbmQgPSBNYXRoLm1pbihidWYubGVuZ3RoLCBlbmQpXG5cbiAgZm9yICh2YXIgaSA9IHN0YXJ0OyBpIDwgZW5kOyArK2kpIHtcbiAgICByZXQgKz0gU3RyaW5nLmZyb21DaGFyQ29kZShidWZbaV0pXG4gIH1cbiAgcmV0dXJuIHJldFxufVxuXG5mdW5jdGlvbiBoZXhTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIHZhciBsZW4gPSBidWYubGVuZ3RoXG5cbiAgaWYgKCFzdGFydCB8fCBzdGFydCA8IDApIHN0YXJ0ID0gMFxuICBpZiAoIWVuZCB8fCBlbmQgPCAwIHx8IGVuZCA+IGxlbikgZW5kID0gbGVuXG5cbiAgdmFyIG91dCA9ICcnXG4gIGZvciAodmFyIGkgPSBzdGFydDsgaSA8IGVuZDsgKytpKSB7XG4gICAgb3V0ICs9IHRvSGV4KGJ1ZltpXSlcbiAgfVxuICByZXR1cm4gb3V0XG59XG5cbmZ1bmN0aW9uIHV0ZjE2bGVTbGljZSAoYnVmLCBzdGFydCwgZW5kKSB7XG4gIHZhciBieXRlcyA9IGJ1Zi5zbGljZShzdGFydCwgZW5kKVxuICB2YXIgcmVzID0gJydcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBieXRlcy5sZW5ndGg7IGkgKz0gMikge1xuICAgIHJlcyArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGJ5dGVzW2ldICsgYnl0ZXNbaSArIDFdICogMjU2KVxuICB9XG4gIHJldHVybiByZXNcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5zbGljZSA9IGZ1bmN0aW9uIHNsaWNlIChzdGFydCwgZW5kKSB7XG4gIHZhciBsZW4gPSB0aGlzLmxlbmd0aFxuICBzdGFydCA9IH5+c3RhcnRcbiAgZW5kID0gZW5kID09PSB1bmRlZmluZWQgPyBsZW4gOiB+fmVuZFxuXG4gIGlmIChzdGFydCA8IDApIHtcbiAgICBzdGFydCArPSBsZW5cbiAgICBpZiAoc3RhcnQgPCAwKSBzdGFydCA9IDBcbiAgfSBlbHNlIGlmIChzdGFydCA+IGxlbikge1xuICAgIHN0YXJ0ID0gbGVuXG4gIH1cblxuICBpZiAoZW5kIDwgMCkge1xuICAgIGVuZCArPSBsZW5cbiAgICBpZiAoZW5kIDwgMCkgZW5kID0gMFxuICB9IGVsc2UgaWYgKGVuZCA+IGxlbikge1xuICAgIGVuZCA9IGxlblxuICB9XG5cbiAgaWYgKGVuZCA8IHN0YXJ0KSBlbmQgPSBzdGFydFxuXG4gIHZhciBuZXdCdWZcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgbmV3QnVmID0gdGhpcy5zdWJhcnJheShzdGFydCwgZW5kKVxuICAgIG5ld0J1Zi5fX3Byb3RvX18gPSBCdWZmZXIucHJvdG90eXBlXG4gIH0gZWxzZSB7XG4gICAgdmFyIHNsaWNlTGVuID0gZW5kIC0gc3RhcnRcbiAgICBuZXdCdWYgPSBuZXcgQnVmZmVyKHNsaWNlTGVuLCB1bmRlZmluZWQpXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBzbGljZUxlbjsgKytpKSB7XG4gICAgICBuZXdCdWZbaV0gPSB0aGlzW2kgKyBzdGFydF1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gbmV3QnVmXG59XG5cbi8qXG4gKiBOZWVkIHRvIG1ha2Ugc3VyZSB0aGF0IGJ1ZmZlciBpc24ndCB0cnlpbmcgdG8gd3JpdGUgb3V0IG9mIGJvdW5kcy5cbiAqL1xuZnVuY3Rpb24gY2hlY2tPZmZzZXQgKG9mZnNldCwgZXh0LCBsZW5ndGgpIHtcbiAgaWYgKChvZmZzZXQgJSAxKSAhPT0gMCB8fCBvZmZzZXQgPCAwKSB0aHJvdyBuZXcgUmFuZ2VFcnJvcignb2Zmc2V0IGlzIG5vdCB1aW50JylcbiAgaWYgKG9mZnNldCArIGV4dCA+IGxlbmd0aCkgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1RyeWluZyB0byBhY2Nlc3MgYmV5b25kIGJ1ZmZlciBsZW5ndGgnKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRVSW50TEUgPSBmdW5jdGlvbiByZWFkVUludExFIChvZmZzZXQsIGJ5dGVMZW5ndGgsIG5vQXNzZXJ0KSB7XG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGggfCAwXG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrT2Zmc2V0KG9mZnNldCwgYnl0ZUxlbmd0aCwgdGhpcy5sZW5ndGgpXG5cbiAgdmFyIHZhbCA9IHRoaXNbb2Zmc2V0XVxuICB2YXIgbXVsID0gMVxuICB2YXIgaSA9IDBcbiAgd2hpbGUgKCsraSA8IGJ5dGVMZW5ndGggJiYgKG11bCAqPSAweDEwMCkpIHtcbiAgICB2YWwgKz0gdGhpc1tvZmZzZXQgKyBpXSAqIG11bFxuICB9XG5cbiAgcmV0dXJuIHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRVSW50QkUgPSBmdW5jdGlvbiByZWFkVUludEJFIChvZmZzZXQsIGJ5dGVMZW5ndGgsIG5vQXNzZXJ0KSB7XG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGggfCAwXG4gIGlmICghbm9Bc3NlcnQpIHtcbiAgICBjaGVja09mZnNldChvZmZzZXQsIGJ5dGVMZW5ndGgsIHRoaXMubGVuZ3RoKVxuICB9XG5cbiAgdmFyIHZhbCA9IHRoaXNbb2Zmc2V0ICsgLS1ieXRlTGVuZ3RoXVxuICB2YXIgbXVsID0gMVxuICB3aGlsZSAoYnl0ZUxlbmd0aCA+IDAgJiYgKG11bCAqPSAweDEwMCkpIHtcbiAgICB2YWwgKz0gdGhpc1tvZmZzZXQgKyAtLWJ5dGVMZW5ndGhdICogbXVsXG4gIH1cblxuICByZXR1cm4gdmFsXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUucmVhZFVJbnQ4ID0gZnVuY3Rpb24gcmVhZFVJbnQ4IChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrT2Zmc2V0KG9mZnNldCwgMSwgdGhpcy5sZW5ndGgpXG4gIHJldHVybiB0aGlzW29mZnNldF1cbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkVUludDE2TEUgPSBmdW5jdGlvbiByZWFkVUludDE2TEUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCAyLCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIHRoaXNbb2Zmc2V0XSB8ICh0aGlzW29mZnNldCArIDFdIDw8IDgpXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUucmVhZFVJbnQxNkJFID0gZnVuY3Rpb24gcmVhZFVJbnQxNkJFIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrT2Zmc2V0KG9mZnNldCwgMiwgdGhpcy5sZW5ndGgpXG4gIHJldHVybiAodGhpc1tvZmZzZXRdIDw8IDgpIHwgdGhpc1tvZmZzZXQgKyAxXVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRVSW50MzJMRSA9IGZ1bmN0aW9uIHJlYWRVSW50MzJMRSAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIDQsIHRoaXMubGVuZ3RoKVxuXG4gIHJldHVybiAoKHRoaXNbb2Zmc2V0XSkgfFxuICAgICAgKHRoaXNbb2Zmc2V0ICsgMV0gPDwgOCkgfFxuICAgICAgKHRoaXNbb2Zmc2V0ICsgMl0gPDwgMTYpKSArXG4gICAgICAodGhpc1tvZmZzZXQgKyAzXSAqIDB4MTAwMDAwMClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkVUludDMyQkUgPSBmdW5jdGlvbiByZWFkVUludDMyQkUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcblxuICByZXR1cm4gKHRoaXNbb2Zmc2V0XSAqIDB4MTAwMDAwMCkgK1xuICAgICgodGhpc1tvZmZzZXQgKyAxXSA8PCAxNikgfFxuICAgICh0aGlzW29mZnNldCArIDJdIDw8IDgpIHxcbiAgICB0aGlzW29mZnNldCArIDNdKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnRMRSA9IGZ1bmN0aW9uIHJlYWRJbnRMRSAob2Zmc2V0LCBieXRlTGVuZ3RoLCBub0Fzc2VydCkge1xuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGJ5dGVMZW5ndGggPSBieXRlTGVuZ3RoIHwgMFxuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIGJ5dGVMZW5ndGgsIHRoaXMubGVuZ3RoKVxuXG4gIHZhciB2YWwgPSB0aGlzW29mZnNldF1cbiAgdmFyIG11bCA9IDFcbiAgdmFyIGkgPSAwXG4gIHdoaWxlICgrK2kgPCBieXRlTGVuZ3RoICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgdmFsICs9IHRoaXNbb2Zmc2V0ICsgaV0gKiBtdWxcbiAgfVxuICBtdWwgKj0gMHg4MFxuXG4gIGlmICh2YWwgPj0gbXVsKSB2YWwgLT0gTWF0aC5wb3coMiwgOCAqIGJ5dGVMZW5ndGgpXG5cbiAgcmV0dXJuIHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnRCRSA9IGZ1bmN0aW9uIHJlYWRJbnRCRSAob2Zmc2V0LCBieXRlTGVuZ3RoLCBub0Fzc2VydCkge1xuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGJ5dGVMZW5ndGggPSBieXRlTGVuZ3RoIHwgMFxuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIGJ5dGVMZW5ndGgsIHRoaXMubGVuZ3RoKVxuXG4gIHZhciBpID0gYnl0ZUxlbmd0aFxuICB2YXIgbXVsID0gMVxuICB2YXIgdmFsID0gdGhpc1tvZmZzZXQgKyAtLWldXG4gIHdoaWxlIChpID4gMCAmJiAobXVsICo9IDB4MTAwKSkge1xuICAgIHZhbCArPSB0aGlzW29mZnNldCArIC0taV0gKiBtdWxcbiAgfVxuICBtdWwgKj0gMHg4MFxuXG4gIGlmICh2YWwgPj0gbXVsKSB2YWwgLT0gTWF0aC5wb3coMiwgOCAqIGJ5dGVMZW5ndGgpXG5cbiAgcmV0dXJuIHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQ4ID0gZnVuY3Rpb24gcmVhZEludDggKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCAxLCB0aGlzLmxlbmd0aClcbiAgaWYgKCEodGhpc1tvZmZzZXRdICYgMHg4MCkpIHJldHVybiAodGhpc1tvZmZzZXRdKVxuICByZXR1cm4gKCgweGZmIC0gdGhpc1tvZmZzZXRdICsgMSkgKiAtMSlcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkSW50MTZMRSA9IGZ1bmN0aW9uIHJlYWRJbnQxNkxFIChvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrT2Zmc2V0KG9mZnNldCwgMiwgdGhpcy5sZW5ndGgpXG4gIHZhciB2YWwgPSB0aGlzW29mZnNldF0gfCAodGhpc1tvZmZzZXQgKyAxXSA8PCA4KVxuICByZXR1cm4gKHZhbCAmIDB4ODAwMCkgPyB2YWwgfCAweEZGRkYwMDAwIDogdmFsXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUucmVhZEludDE2QkUgPSBmdW5jdGlvbiByZWFkSW50MTZCRSAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIDIsIHRoaXMubGVuZ3RoKVxuICB2YXIgdmFsID0gdGhpc1tvZmZzZXQgKyAxXSB8ICh0aGlzW29mZnNldF0gPDwgOClcbiAgcmV0dXJuICh2YWwgJiAweDgwMDApID8gdmFsIHwgMHhGRkZGMDAwMCA6IHZhbFxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQzMkxFID0gZnVuY3Rpb24gcmVhZEludDMyTEUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcblxuICByZXR1cm4gKHRoaXNbb2Zmc2V0XSkgfFxuICAgICh0aGlzW29mZnNldCArIDFdIDw8IDgpIHxcbiAgICAodGhpc1tvZmZzZXQgKyAyXSA8PCAxNikgfFxuICAgICh0aGlzW29mZnNldCArIDNdIDw8IDI0KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRJbnQzMkJFID0gZnVuY3Rpb24gcmVhZEludDMyQkUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcblxuICByZXR1cm4gKHRoaXNbb2Zmc2V0XSA8PCAyNCkgfFxuICAgICh0aGlzW29mZnNldCArIDFdIDw8IDE2KSB8XG4gICAgKHRoaXNbb2Zmc2V0ICsgMl0gPDwgOCkgfFxuICAgICh0aGlzW29mZnNldCArIDNdKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRGbG9hdExFID0gZnVuY3Rpb24gcmVhZEZsb2F0TEUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIHRydWUsIDIzLCA0KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWRGbG9hdEJFID0gZnVuY3Rpb24gcmVhZEZsb2F0QkUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA0LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIGZhbHNlLCAyMywgNClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS5yZWFkRG91YmxlTEUgPSBmdW5jdGlvbiByZWFkRG91YmxlTEUgKG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tPZmZzZXQob2Zmc2V0LCA4LCB0aGlzLmxlbmd0aClcbiAgcmV0dXJuIGllZWU3NTQucmVhZCh0aGlzLCBvZmZzZXQsIHRydWUsIDUyLCA4KVxufVxuXG5CdWZmZXIucHJvdG90eXBlLnJlYWREb3VibGVCRSA9IGZ1bmN0aW9uIHJlYWREb3VibGVCRSAob2Zmc2V0LCBub0Fzc2VydCkge1xuICBpZiAoIW5vQXNzZXJ0KSBjaGVja09mZnNldChvZmZzZXQsIDgsIHRoaXMubGVuZ3RoKVxuICByZXR1cm4gaWVlZTc1NC5yZWFkKHRoaXMsIG9mZnNldCwgZmFsc2UsIDUyLCA4KVxufVxuXG5mdW5jdGlvbiBjaGVja0ludCAoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBleHQsIG1heCwgbWluKSB7XG4gIGlmICghQnVmZmVyLmlzQnVmZmVyKGJ1ZikpIHRocm93IG5ldyBUeXBlRXJyb3IoJ1wiYnVmZmVyXCIgYXJndW1lbnQgbXVzdCBiZSBhIEJ1ZmZlciBpbnN0YW5jZScpXG4gIGlmICh2YWx1ZSA+IG1heCB8fCB2YWx1ZSA8IG1pbikgdGhyb3cgbmV3IFJhbmdlRXJyb3IoJ1widmFsdWVcIiBhcmd1bWVudCBpcyBvdXQgb2YgYm91bmRzJylcbiAgaWYgKG9mZnNldCArIGV4dCA+IGJ1Zi5sZW5ndGgpIHRocm93IG5ldyBSYW5nZUVycm9yKCdJbmRleCBvdXQgb2YgcmFuZ2UnKVxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludExFID0gZnVuY3Rpb24gd3JpdGVVSW50TEUgKHZhbHVlLCBvZmZzZXQsIGJ5dGVMZW5ndGgsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGggfCAwXG4gIGlmICghbm9Bc3NlcnQpIHtcbiAgICB2YXIgbWF4Qnl0ZXMgPSBNYXRoLnBvdygyLCA4ICogYnl0ZUxlbmd0aCkgLSAxXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbWF4Qnl0ZXMsIDApXG4gIH1cblxuICB2YXIgbXVsID0gMVxuICB2YXIgaSA9IDBcbiAgdGhpc1tvZmZzZXRdID0gdmFsdWUgJiAweEZGXG4gIHdoaWxlICgrK2kgPCBieXRlTGVuZ3RoICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgdGhpc1tvZmZzZXQgKyBpXSA9ICh2YWx1ZSAvIG11bCkgJiAweEZGXG4gIH1cblxuICByZXR1cm4gb2Zmc2V0ICsgYnl0ZUxlbmd0aFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludEJFID0gZnVuY3Rpb24gd3JpdGVVSW50QkUgKHZhbHVlLCBvZmZzZXQsIGJ5dGVMZW5ndGgsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgYnl0ZUxlbmd0aCA9IGJ5dGVMZW5ndGggfCAwXG4gIGlmICghbm9Bc3NlcnQpIHtcbiAgICB2YXIgbWF4Qnl0ZXMgPSBNYXRoLnBvdygyLCA4ICogYnl0ZUxlbmd0aCkgLSAxXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbWF4Qnl0ZXMsIDApXG4gIH1cblxuICB2YXIgaSA9IGJ5dGVMZW5ndGggLSAxXG4gIHZhciBtdWwgPSAxXG4gIHRoaXNbb2Zmc2V0ICsgaV0gPSB2YWx1ZSAmIDB4RkZcbiAgd2hpbGUgKC0taSA+PSAwICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgdGhpc1tvZmZzZXQgKyBpXSA9ICh2YWx1ZSAvIG11bCkgJiAweEZGXG4gIH1cblxuICByZXR1cm4gb2Zmc2V0ICsgYnl0ZUxlbmd0aFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlVUludDggPSBmdW5jdGlvbiB3cml0ZVVJbnQ4ICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDEsIDB4ZmYsIDApXG4gIGlmICghQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHZhbHVlID0gTWF0aC5mbG9vcih2YWx1ZSlcbiAgdGhpc1tvZmZzZXRdID0gKHZhbHVlICYgMHhmZilcbiAgcmV0dXJuIG9mZnNldCArIDFcbn1cblxuZnVuY3Rpb24gb2JqZWN0V3JpdGVVSW50MTYgKGJ1ZiwgdmFsdWUsIG9mZnNldCwgbGl0dGxlRW5kaWFuKSB7XG4gIGlmICh2YWx1ZSA8IDApIHZhbHVlID0gMHhmZmZmICsgdmFsdWUgKyAxXG4gIGZvciAodmFyIGkgPSAwLCBqID0gTWF0aC5taW4oYnVmLmxlbmd0aCAtIG9mZnNldCwgMik7IGkgPCBqOyArK2kpIHtcbiAgICBidWZbb2Zmc2V0ICsgaV0gPSAodmFsdWUgJiAoMHhmZiA8PCAoOCAqIChsaXR0bGVFbmRpYW4gPyBpIDogMSAtIGkpKSkpID4+PlxuICAgICAgKGxpdHRsZUVuZGlhbiA/IGkgOiAxIC0gaSkgKiA4XG4gIH1cbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZVVJbnQxNkxFID0gZnVuY3Rpb24gd3JpdGVVSW50MTZMRSAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0IHwgMFxuICBpZiAoIW5vQXNzZXJ0KSBjaGVja0ludCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCAyLCAweGZmZmYsIDApXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIHRoaXNbb2Zmc2V0XSA9ICh2YWx1ZSAmIDB4ZmYpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSA+Pj4gOClcbiAgfSBlbHNlIHtcbiAgICBvYmplY3RXcml0ZVVJbnQxNih0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlKVxuICB9XG4gIHJldHVybiBvZmZzZXQgKyAyXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVVSW50MTZCRSA9IGZ1bmN0aW9uIHdyaXRlVUludDE2QkUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMiwgMHhmZmZmLCAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgPj4+IDgpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSAmIDB4ZmYpXG4gIH0gZWxzZSB7XG4gICAgb2JqZWN0V3JpdGVVSW50MTYodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDJcbn1cblxuZnVuY3Rpb24gb2JqZWN0V3JpdGVVSW50MzIgKGJ1ZiwgdmFsdWUsIG9mZnNldCwgbGl0dGxlRW5kaWFuKSB7XG4gIGlmICh2YWx1ZSA8IDApIHZhbHVlID0gMHhmZmZmZmZmZiArIHZhbHVlICsgMVxuICBmb3IgKHZhciBpID0gMCwgaiA9IE1hdGgubWluKGJ1Zi5sZW5ndGggLSBvZmZzZXQsIDQpOyBpIDwgajsgKytpKSB7XG4gICAgYnVmW29mZnNldCArIGldID0gKHZhbHVlID4+PiAobGl0dGxlRW5kaWFuID8gaSA6IDMgLSBpKSAqIDgpICYgMHhmZlxuICB9XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVVSW50MzJMRSA9IGZ1bmN0aW9uIHdyaXRlVUludDMyTEUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgNCwgMHhmZmZmZmZmZiwgMClcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgdGhpc1tvZmZzZXQgKyAzXSA9ICh2YWx1ZSA+Pj4gMjQpXG4gICAgdGhpc1tvZmZzZXQgKyAyXSA9ICh2YWx1ZSA+Pj4gMTYpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSA+Pj4gOClcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgJiAweGZmKVxuICB9IGVsc2Uge1xuICAgIG9iamVjdFdyaXRlVUludDMyKHRoaXMsIHZhbHVlLCBvZmZzZXQsIHRydWUpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDRcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZVVJbnQzMkJFID0gZnVuY3Rpb24gd3JpdGVVSW50MzJCRSAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0IHwgMFxuICBpZiAoIW5vQXNzZXJ0KSBjaGVja0ludCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCA0LCAweGZmZmZmZmZmLCAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgPj4+IDI0KVxuICAgIHRoaXNbb2Zmc2V0ICsgMV0gPSAodmFsdWUgPj4+IDE2KVxuICAgIHRoaXNbb2Zmc2V0ICsgMl0gPSAodmFsdWUgPj4+IDgpXG4gICAgdGhpc1tvZmZzZXQgKyAzXSA9ICh2YWx1ZSAmIDB4ZmYpXG4gIH0gZWxzZSB7XG4gICAgb2JqZWN0V3JpdGVVSW50MzIodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDRcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludExFID0gZnVuY3Rpb24gd3JpdGVJbnRMRSAodmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0IHwgMFxuICBpZiAoIW5vQXNzZXJ0KSB7XG4gICAgdmFyIGxpbWl0ID0gTWF0aC5wb3coMiwgOCAqIGJ5dGVMZW5ndGggLSAxKVxuXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbGltaXQgLSAxLCAtbGltaXQpXG4gIH1cblxuICB2YXIgaSA9IDBcbiAgdmFyIG11bCA9IDFcbiAgdmFyIHN1YiA9IDBcbiAgdGhpc1tvZmZzZXRdID0gdmFsdWUgJiAweEZGXG4gIHdoaWxlICgrK2kgPCBieXRlTGVuZ3RoICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgaWYgKHZhbHVlIDwgMCAmJiBzdWIgPT09IDAgJiYgdGhpc1tvZmZzZXQgKyBpIC0gMV0gIT09IDApIHtcbiAgICAgIHN1YiA9IDFcbiAgICB9XG4gICAgdGhpc1tvZmZzZXQgKyBpXSA9ICgodmFsdWUgLyBtdWwpID4+IDApIC0gc3ViICYgMHhGRlxuICB9XG5cbiAgcmV0dXJuIG9mZnNldCArIGJ5dGVMZW5ndGhcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludEJFID0gZnVuY3Rpb24gd3JpdGVJbnRCRSAodmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbm9Bc3NlcnQpIHtcbiAgdmFsdWUgPSArdmFsdWVcbiAgb2Zmc2V0ID0gb2Zmc2V0IHwgMFxuICBpZiAoIW5vQXNzZXJ0KSB7XG4gICAgdmFyIGxpbWl0ID0gTWF0aC5wb3coMiwgOCAqIGJ5dGVMZW5ndGggLSAxKVxuXG4gICAgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgYnl0ZUxlbmd0aCwgbGltaXQgLSAxLCAtbGltaXQpXG4gIH1cblxuICB2YXIgaSA9IGJ5dGVMZW5ndGggLSAxXG4gIHZhciBtdWwgPSAxXG4gIHZhciBzdWIgPSAwXG4gIHRoaXNbb2Zmc2V0ICsgaV0gPSB2YWx1ZSAmIDB4RkZcbiAgd2hpbGUgKC0taSA+PSAwICYmIChtdWwgKj0gMHgxMDApKSB7XG4gICAgaWYgKHZhbHVlIDwgMCAmJiBzdWIgPT09IDAgJiYgdGhpc1tvZmZzZXQgKyBpICsgMV0gIT09IDApIHtcbiAgICAgIHN1YiA9IDFcbiAgICB9XG4gICAgdGhpc1tvZmZzZXQgKyBpXSA9ICgodmFsdWUgLyBtdWwpID4+IDApIC0gc3ViICYgMHhGRlxuICB9XG5cbiAgcmV0dXJuIG9mZnNldCArIGJ5dGVMZW5ndGhcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludDggPSBmdW5jdGlvbiB3cml0ZUludDggKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMSwgMHg3ZiwgLTB4ODApXG4gIGlmICghQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHZhbHVlID0gTWF0aC5mbG9vcih2YWx1ZSlcbiAgaWYgKHZhbHVlIDwgMCkgdmFsdWUgPSAweGZmICsgdmFsdWUgKyAxXG4gIHRoaXNbb2Zmc2V0XSA9ICh2YWx1ZSAmIDB4ZmYpXG4gIHJldHVybiBvZmZzZXQgKyAxXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVJbnQxNkxFID0gZnVuY3Rpb24gd3JpdGVJbnQxNkxFICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDIsIDB4N2ZmZiwgLTB4ODAwMClcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgdGhpc1tvZmZzZXRdID0gKHZhbHVlICYgMHhmZilcbiAgICB0aGlzW29mZnNldCArIDFdID0gKHZhbHVlID4+PiA4KVxuICB9IGVsc2Uge1xuICAgIG9iamVjdFdyaXRlVUludDE2KHRoaXMsIHZhbHVlLCBvZmZzZXQsIHRydWUpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDJcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludDE2QkUgPSBmdW5jdGlvbiB3cml0ZUludDE2QkUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgMiwgMHg3ZmZmLCAtMHg4MDAwKVxuICBpZiAoQnVmZmVyLlRZUEVEX0FSUkFZX1NVUFBPUlQpIHtcbiAgICB0aGlzW29mZnNldF0gPSAodmFsdWUgPj4+IDgpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSAmIDB4ZmYpXG4gIH0gZWxzZSB7XG4gICAgb2JqZWN0V3JpdGVVSW50MTYodGhpcywgdmFsdWUsIG9mZnNldCwgZmFsc2UpXG4gIH1cbiAgcmV0dXJuIG9mZnNldCArIDJcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZUludDMyTEUgPSBmdW5jdGlvbiB3cml0ZUludDMyTEUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHZhbHVlID0gK3ZhbHVlXG4gIG9mZnNldCA9IG9mZnNldCB8IDBcbiAgaWYgKCFub0Fzc2VydCkgY2hlY2tJbnQodGhpcywgdmFsdWUsIG9mZnNldCwgNCwgMHg3ZmZmZmZmZiwgLTB4ODAwMDAwMDApXG4gIGlmIChCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIHRoaXNbb2Zmc2V0XSA9ICh2YWx1ZSAmIDB4ZmYpXG4gICAgdGhpc1tvZmZzZXQgKyAxXSA9ICh2YWx1ZSA+Pj4gOClcbiAgICB0aGlzW29mZnNldCArIDJdID0gKHZhbHVlID4+PiAxNilcbiAgICB0aGlzW29mZnNldCArIDNdID0gKHZhbHVlID4+PiAyNClcbiAgfSBlbHNlIHtcbiAgICBvYmplY3RXcml0ZVVJbnQzMih0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlKVxuICB9XG4gIHJldHVybiBvZmZzZXQgKyA0XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVJbnQzMkJFID0gZnVuY3Rpb24gd3JpdGVJbnQzMkJFICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICB2YWx1ZSA9ICt2YWx1ZVxuICBvZmZzZXQgPSBvZmZzZXQgfCAwXG4gIGlmICghbm9Bc3NlcnQpIGNoZWNrSW50KHRoaXMsIHZhbHVlLCBvZmZzZXQsIDQsIDB4N2ZmZmZmZmYsIC0weDgwMDAwMDAwKVxuICBpZiAodmFsdWUgPCAwKSB2YWx1ZSA9IDB4ZmZmZmZmZmYgKyB2YWx1ZSArIDFcbiAgaWYgKEJ1ZmZlci5UWVBFRF9BUlJBWV9TVVBQT1JUKSB7XG4gICAgdGhpc1tvZmZzZXRdID0gKHZhbHVlID4+PiAyNClcbiAgICB0aGlzW29mZnNldCArIDFdID0gKHZhbHVlID4+PiAxNilcbiAgICB0aGlzW29mZnNldCArIDJdID0gKHZhbHVlID4+PiA4KVxuICAgIHRoaXNbb2Zmc2V0ICsgM10gPSAodmFsdWUgJiAweGZmKVxuICB9IGVsc2Uge1xuICAgIG9iamVjdFdyaXRlVUludDMyKHRoaXMsIHZhbHVlLCBvZmZzZXQsIGZhbHNlKVxuICB9XG4gIHJldHVybiBvZmZzZXQgKyA0XG59XG5cbmZ1bmN0aW9uIGNoZWNrSUVFRTc1NCAoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBleHQsIG1heCwgbWluKSB7XG4gIGlmIChvZmZzZXQgKyBleHQgPiBidWYubGVuZ3RoKSB0aHJvdyBuZXcgUmFuZ2VFcnJvcignSW5kZXggb3V0IG9mIHJhbmdlJylcbiAgaWYgKG9mZnNldCA8IDApIHRocm93IG5ldyBSYW5nZUVycm9yKCdJbmRleCBvdXQgb2YgcmFuZ2UnKVxufVxuXG5mdW5jdGlvbiB3cml0ZUZsb2F0IChidWYsIHZhbHVlLCBvZmZzZXQsIGxpdHRsZUVuZGlhbiwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkge1xuICAgIGNoZWNrSUVFRTc1NChidWYsIHZhbHVlLCBvZmZzZXQsIDQsIDMuNDAyODIzNDY2Mzg1Mjg4NmUrMzgsIC0zLjQwMjgyMzQ2NjM4NTI4ODZlKzM4KVxuICB9XG4gIGllZWU3NTQud3JpdGUoYnVmLCB2YWx1ZSwgb2Zmc2V0LCBsaXR0bGVFbmRpYW4sIDIzLCA0KVxuICByZXR1cm4gb2Zmc2V0ICsgNFxufVxuXG5CdWZmZXIucHJvdG90eXBlLndyaXRlRmxvYXRMRSA9IGZ1bmN0aW9uIHdyaXRlRmxvYXRMRSAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgcmV0dXJuIHdyaXRlRmxvYXQodGhpcywgdmFsdWUsIG9mZnNldCwgdHJ1ZSwgbm9Bc3NlcnQpXG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVGbG9hdEJFID0gZnVuY3Rpb24gd3JpdGVGbG9hdEJFICh2YWx1ZSwgb2Zmc2V0LCBub0Fzc2VydCkge1xuICByZXR1cm4gd3JpdGVGbG9hdCh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCBmYWxzZSwgbm9Bc3NlcnQpXG59XG5cbmZ1bmN0aW9uIHdyaXRlRG91YmxlIChidWYsIHZhbHVlLCBvZmZzZXQsIGxpdHRsZUVuZGlhbiwgbm9Bc3NlcnQpIHtcbiAgaWYgKCFub0Fzc2VydCkge1xuICAgIGNoZWNrSUVFRTc1NChidWYsIHZhbHVlLCBvZmZzZXQsIDgsIDEuNzk3NjkzMTM0ODYyMzE1N0UrMzA4LCAtMS43OTc2OTMxMzQ4NjIzMTU3RSszMDgpXG4gIH1cbiAgaWVlZTc1NC53cml0ZShidWYsIHZhbHVlLCBvZmZzZXQsIGxpdHRsZUVuZGlhbiwgNTIsIDgpXG4gIHJldHVybiBvZmZzZXQgKyA4XG59XG5cbkJ1ZmZlci5wcm90b3R5cGUud3JpdGVEb3VibGVMRSA9IGZ1bmN0aW9uIHdyaXRlRG91YmxlTEUgKHZhbHVlLCBvZmZzZXQsIG5vQXNzZXJ0KSB7XG4gIHJldHVybiB3cml0ZURvdWJsZSh0aGlzLCB2YWx1ZSwgb2Zmc2V0LCB0cnVlLCBub0Fzc2VydClcbn1cblxuQnVmZmVyLnByb3RvdHlwZS53cml0ZURvdWJsZUJFID0gZnVuY3Rpb24gd3JpdGVEb3VibGVCRSAodmFsdWUsIG9mZnNldCwgbm9Bc3NlcnQpIHtcbiAgcmV0dXJuIHdyaXRlRG91YmxlKHRoaXMsIHZhbHVlLCBvZmZzZXQsIGZhbHNlLCBub0Fzc2VydClcbn1cblxuLy8gY29weSh0YXJnZXRCdWZmZXIsIHRhcmdldFN0YXJ0PTAsIHNvdXJjZVN0YXJ0PTAsIHNvdXJjZUVuZD1idWZmZXIubGVuZ3RoKVxuQnVmZmVyLnByb3RvdHlwZS5jb3B5ID0gZnVuY3Rpb24gY29weSAodGFyZ2V0LCB0YXJnZXRTdGFydCwgc3RhcnQsIGVuZCkge1xuICBpZiAoIXN0YXJ0KSBzdGFydCA9IDBcbiAgaWYgKCFlbmQgJiYgZW5kICE9PSAwKSBlbmQgPSB0aGlzLmxlbmd0aFxuICBpZiAodGFyZ2V0U3RhcnQgPj0gdGFyZ2V0Lmxlbmd0aCkgdGFyZ2V0U3RhcnQgPSB0YXJnZXQubGVuZ3RoXG4gIGlmICghdGFyZ2V0U3RhcnQpIHRhcmdldFN0YXJ0ID0gMFxuICBpZiAoZW5kID4gMCAmJiBlbmQgPCBzdGFydCkgZW5kID0gc3RhcnRcblxuICAvLyBDb3B5IDAgYnl0ZXM7IHdlJ3JlIGRvbmVcbiAgaWYgKGVuZCA9PT0gc3RhcnQpIHJldHVybiAwXG4gIGlmICh0YXJnZXQubGVuZ3RoID09PSAwIHx8IHRoaXMubGVuZ3RoID09PSAwKSByZXR1cm4gMFxuXG4gIC8vIEZhdGFsIGVycm9yIGNvbmRpdGlvbnNcbiAgaWYgKHRhcmdldFN0YXJ0IDwgMCkge1xuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCd0YXJnZXRTdGFydCBvdXQgb2YgYm91bmRzJylcbiAgfVxuICBpZiAoc3RhcnQgPCAwIHx8IHN0YXJ0ID49IHRoaXMubGVuZ3RoKSB0aHJvdyBuZXcgUmFuZ2VFcnJvcignc291cmNlU3RhcnQgb3V0IG9mIGJvdW5kcycpXG4gIGlmIChlbmQgPCAwKSB0aHJvdyBuZXcgUmFuZ2VFcnJvcignc291cmNlRW5kIG91dCBvZiBib3VuZHMnKVxuXG4gIC8vIEFyZSB3ZSBvb2I/XG4gIGlmIChlbmQgPiB0aGlzLmxlbmd0aCkgZW5kID0gdGhpcy5sZW5ndGhcbiAgaWYgKHRhcmdldC5sZW5ndGggLSB0YXJnZXRTdGFydCA8IGVuZCAtIHN0YXJ0KSB7XG4gICAgZW5kID0gdGFyZ2V0Lmxlbmd0aCAtIHRhcmdldFN0YXJ0ICsgc3RhcnRcbiAgfVxuXG4gIHZhciBsZW4gPSBlbmQgLSBzdGFydFxuICB2YXIgaVxuXG4gIGlmICh0aGlzID09PSB0YXJnZXQgJiYgc3RhcnQgPCB0YXJnZXRTdGFydCAmJiB0YXJnZXRTdGFydCA8IGVuZCkge1xuICAgIC8vIGRlc2NlbmRpbmcgY29weSBmcm9tIGVuZFxuICAgIGZvciAoaSA9IGxlbiAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICB0YXJnZXRbaSArIHRhcmdldFN0YXJ0XSA9IHRoaXNbaSArIHN0YXJ0XVxuICAgIH1cbiAgfSBlbHNlIGlmIChsZW4gPCAxMDAwIHx8ICFCdWZmZXIuVFlQRURfQVJSQVlfU1VQUE9SVCkge1xuICAgIC8vIGFzY2VuZGluZyBjb3B5IGZyb20gc3RhcnRcbiAgICBmb3IgKGkgPSAwOyBpIDwgbGVuOyArK2kpIHtcbiAgICAgIHRhcmdldFtpICsgdGFyZ2V0U3RhcnRdID0gdGhpc1tpICsgc3RhcnRdXG4gICAgfVxuICB9IGVsc2Uge1xuICAgIFVpbnQ4QXJyYXkucHJvdG90eXBlLnNldC5jYWxsKFxuICAgICAgdGFyZ2V0LFxuICAgICAgdGhpcy5zdWJhcnJheShzdGFydCwgc3RhcnQgKyBsZW4pLFxuICAgICAgdGFyZ2V0U3RhcnRcbiAgICApXG4gIH1cblxuICByZXR1cm4gbGVuXG59XG5cbi8vIFVzYWdlOlxuLy8gICAgYnVmZmVyLmZpbGwobnVtYmVyWywgb2Zmc2V0WywgZW5kXV0pXG4vLyAgICBidWZmZXIuZmlsbChidWZmZXJbLCBvZmZzZXRbLCBlbmRdXSlcbi8vICAgIGJ1ZmZlci5maWxsKHN0cmluZ1ssIG9mZnNldFssIGVuZF1dWywgZW5jb2RpbmddKVxuQnVmZmVyLnByb3RvdHlwZS5maWxsID0gZnVuY3Rpb24gZmlsbCAodmFsLCBzdGFydCwgZW5kLCBlbmNvZGluZykge1xuICAvLyBIYW5kbGUgc3RyaW5nIGNhc2VzOlxuICBpZiAodHlwZW9mIHZhbCA9PT0gJ3N0cmluZycpIHtcbiAgICBpZiAodHlwZW9mIHN0YXJ0ID09PSAnc3RyaW5nJykge1xuICAgICAgZW5jb2RpbmcgPSBzdGFydFxuICAgICAgc3RhcnQgPSAwXG4gICAgICBlbmQgPSB0aGlzLmxlbmd0aFxuICAgIH0gZWxzZSBpZiAodHlwZW9mIGVuZCA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGVuY29kaW5nID0gZW5kXG4gICAgICBlbmQgPSB0aGlzLmxlbmd0aFxuICAgIH1cbiAgICBpZiAodmFsLmxlbmd0aCA9PT0gMSkge1xuICAgICAgdmFyIGNvZGUgPSB2YWwuY2hhckNvZGVBdCgwKVxuICAgICAgaWYgKGNvZGUgPCAyNTYpIHtcbiAgICAgICAgdmFsID0gY29kZVxuICAgICAgfVxuICAgIH1cbiAgICBpZiAoZW5jb2RpbmcgIT09IHVuZGVmaW5lZCAmJiB0eXBlb2YgZW5jb2RpbmcgIT09ICdzdHJpbmcnKSB7XG4gICAgICB0aHJvdyBuZXcgVHlwZUVycm9yKCdlbmNvZGluZyBtdXN0IGJlIGEgc3RyaW5nJylcbiAgICB9XG4gICAgaWYgKHR5cGVvZiBlbmNvZGluZyA9PT0gJ3N0cmluZycgJiYgIUJ1ZmZlci5pc0VuY29kaW5nKGVuY29kaW5nKSkge1xuICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcignVW5rbm93biBlbmNvZGluZzogJyArIGVuY29kaW5nKVxuICAgIH1cbiAgfSBlbHNlIGlmICh0eXBlb2YgdmFsID09PSAnbnVtYmVyJykge1xuICAgIHZhbCA9IHZhbCAmIDI1NVxuICB9XG5cbiAgLy8gSW52YWxpZCByYW5nZXMgYXJlIG5vdCBzZXQgdG8gYSBkZWZhdWx0LCBzbyBjYW4gcmFuZ2UgY2hlY2sgZWFybHkuXG4gIGlmIChzdGFydCA8IDAgfHwgdGhpcy5sZW5ndGggPCBzdGFydCB8fCB0aGlzLmxlbmd0aCA8IGVuZCkge1xuICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdPdXQgb2YgcmFuZ2UgaW5kZXgnKVxuICB9XG5cbiAgaWYgKGVuZCA8PSBzdGFydCkge1xuICAgIHJldHVybiB0aGlzXG4gIH1cblxuICBzdGFydCA9IHN0YXJ0ID4+PiAwXG4gIGVuZCA9IGVuZCA9PT0gdW5kZWZpbmVkID8gdGhpcy5sZW5ndGggOiBlbmQgPj4+IDBcblxuICBpZiAoIXZhbCkgdmFsID0gMFxuXG4gIHZhciBpXG4gIGlmICh0eXBlb2YgdmFsID09PSAnbnVtYmVyJykge1xuICAgIGZvciAoaSA9IHN0YXJ0OyBpIDwgZW5kOyArK2kpIHtcbiAgICAgIHRoaXNbaV0gPSB2YWxcbiAgICB9XG4gIH0gZWxzZSB7XG4gICAgdmFyIGJ5dGVzID0gQnVmZmVyLmlzQnVmZmVyKHZhbClcbiAgICAgID8gdmFsXG4gICAgICA6IHV0ZjhUb0J5dGVzKG5ldyBCdWZmZXIodmFsLCBlbmNvZGluZykudG9TdHJpbmcoKSlcbiAgICB2YXIgbGVuID0gYnl0ZXMubGVuZ3RoXG4gICAgZm9yIChpID0gMDsgaSA8IGVuZCAtIHN0YXJ0OyArK2kpIHtcbiAgICAgIHRoaXNbaSArIHN0YXJ0XSA9IGJ5dGVzW2kgJSBsZW5dXG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRoaXNcbn1cblxuLy8gSEVMUEVSIEZVTkNUSU9OU1xuLy8gPT09PT09PT09PT09PT09PVxuXG52YXIgSU5WQUxJRF9CQVNFNjRfUkUgPSAvW14rXFwvMC05QS1aYS16LV9dL2dcblxuZnVuY3Rpb24gYmFzZTY0Y2xlYW4gKHN0cikge1xuICAvLyBOb2RlIHN0cmlwcyBvdXQgaW52YWxpZCBjaGFyYWN0ZXJzIGxpa2UgXFxuIGFuZCBcXHQgZnJvbSB0aGUgc3RyaW5nLCBiYXNlNjQtanMgZG9lcyBub3RcbiAgc3RyID0gc3RyaW5ndHJpbShzdHIpLnJlcGxhY2UoSU5WQUxJRF9CQVNFNjRfUkUsICcnKVxuICAvLyBOb2RlIGNvbnZlcnRzIHN0cmluZ3Mgd2l0aCBsZW5ndGggPCAyIHRvICcnXG4gIGlmIChzdHIubGVuZ3RoIDwgMikgcmV0dXJuICcnXG4gIC8vIE5vZGUgYWxsb3dzIGZvciBub24tcGFkZGVkIGJhc2U2NCBzdHJpbmdzIChtaXNzaW5nIHRyYWlsaW5nID09PSksIGJhc2U2NC1qcyBkb2VzIG5vdFxuICB3aGlsZSAoc3RyLmxlbmd0aCAlIDQgIT09IDApIHtcbiAgICBzdHIgPSBzdHIgKyAnPSdcbiAgfVxuICByZXR1cm4gc3RyXG59XG5cbmZ1bmN0aW9uIHN0cmluZ3RyaW0gKHN0cikge1xuICBpZiAoc3RyLnRyaW0pIHJldHVybiBzdHIudHJpbSgpXG4gIHJldHVybiBzdHIucmVwbGFjZSgvXlxccyt8XFxzKyQvZywgJycpXG59XG5cbmZ1bmN0aW9uIHRvSGV4IChuKSB7XG4gIGlmIChuIDwgMTYpIHJldHVybiAnMCcgKyBuLnRvU3RyaW5nKDE2KVxuICByZXR1cm4gbi50b1N0cmluZygxNilcbn1cblxuZnVuY3Rpb24gdXRmOFRvQnl0ZXMgKHN0cmluZywgdW5pdHMpIHtcbiAgdW5pdHMgPSB1bml0cyB8fCBJbmZpbml0eVxuICB2YXIgY29kZVBvaW50XG4gIHZhciBsZW5ndGggPSBzdHJpbmcubGVuZ3RoXG4gIHZhciBsZWFkU3Vycm9nYXRlID0gbnVsbFxuICB2YXIgYnl0ZXMgPSBbXVxuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyArK2kpIHtcbiAgICBjb2RlUG9pbnQgPSBzdHJpbmcuY2hhckNvZGVBdChpKVxuXG4gICAgLy8gaXMgc3Vycm9nYXRlIGNvbXBvbmVudFxuICAgIGlmIChjb2RlUG9pbnQgPiAweEQ3RkYgJiYgY29kZVBvaW50IDwgMHhFMDAwKSB7XG4gICAgICAvLyBsYXN0IGNoYXIgd2FzIGEgbGVhZFxuICAgICAgaWYgKCFsZWFkU3Vycm9nYXRlKSB7XG4gICAgICAgIC8vIG5vIGxlYWQgeWV0XG4gICAgICAgIGlmIChjb2RlUG9pbnQgPiAweERCRkYpIHtcbiAgICAgICAgICAvLyB1bmV4cGVjdGVkIHRyYWlsXG4gICAgICAgICAgaWYgKCh1bml0cyAtPSAzKSA+IC0xKSBieXRlcy5wdXNoKDB4RUYsIDB4QkYsIDB4QkQpXG4gICAgICAgICAgY29udGludWVcbiAgICAgICAgfSBlbHNlIGlmIChpICsgMSA9PT0gbGVuZ3RoKSB7XG4gICAgICAgICAgLy8gdW5wYWlyZWQgbGVhZFxuICAgICAgICAgIGlmICgodW5pdHMgLT0gMykgPiAtMSkgYnl0ZXMucHVzaCgweEVGLCAweEJGLCAweEJEKVxuICAgICAgICAgIGNvbnRpbnVlXG4gICAgICAgIH1cblxuICAgICAgICAvLyB2YWxpZCBsZWFkXG4gICAgICAgIGxlYWRTdXJyb2dhdGUgPSBjb2RlUG9pbnRcblxuICAgICAgICBjb250aW51ZVxuICAgICAgfVxuXG4gICAgICAvLyAyIGxlYWRzIGluIGEgcm93XG4gICAgICBpZiAoY29kZVBvaW50IDwgMHhEQzAwKSB7XG4gICAgICAgIGlmICgodW5pdHMgLT0gMykgPiAtMSkgYnl0ZXMucHVzaCgweEVGLCAweEJGLCAweEJEKVxuICAgICAgICBsZWFkU3Vycm9nYXRlID0gY29kZVBvaW50XG4gICAgICAgIGNvbnRpbnVlXG4gICAgICB9XG5cbiAgICAgIC8vIHZhbGlkIHN1cnJvZ2F0ZSBwYWlyXG4gICAgICBjb2RlUG9pbnQgPSAobGVhZFN1cnJvZ2F0ZSAtIDB4RDgwMCA8PCAxMCB8IGNvZGVQb2ludCAtIDB4REMwMCkgKyAweDEwMDAwXG4gICAgfSBlbHNlIGlmIChsZWFkU3Vycm9nYXRlKSB7XG4gICAgICAvLyB2YWxpZCBibXAgY2hhciwgYnV0IGxhc3QgY2hhciB3YXMgYSBsZWFkXG4gICAgICBpZiAoKHVuaXRzIC09IDMpID4gLTEpIGJ5dGVzLnB1c2goMHhFRiwgMHhCRiwgMHhCRClcbiAgICB9XG5cbiAgICBsZWFkU3Vycm9nYXRlID0gbnVsbFxuXG4gICAgLy8gZW5jb2RlIHV0ZjhcbiAgICBpZiAoY29kZVBvaW50IDwgMHg4MCkge1xuICAgICAgaWYgKCh1bml0cyAtPSAxKSA8IDApIGJyZWFrXG4gICAgICBieXRlcy5wdXNoKGNvZGVQb2ludClcbiAgICB9IGVsc2UgaWYgKGNvZGVQb2ludCA8IDB4ODAwKSB7XG4gICAgICBpZiAoKHVuaXRzIC09IDIpIDwgMCkgYnJlYWtcbiAgICAgIGJ5dGVzLnB1c2goXG4gICAgICAgIGNvZGVQb2ludCA+PiAweDYgfCAweEMwLFxuICAgICAgICBjb2RlUG9pbnQgJiAweDNGIHwgMHg4MFxuICAgICAgKVxuICAgIH0gZWxzZSBpZiAoY29kZVBvaW50IDwgMHgxMDAwMCkge1xuICAgICAgaWYgKCh1bml0cyAtPSAzKSA8IDApIGJyZWFrXG4gICAgICBieXRlcy5wdXNoKFxuICAgICAgICBjb2RlUG9pbnQgPj4gMHhDIHwgMHhFMCxcbiAgICAgICAgY29kZVBvaW50ID4+IDB4NiAmIDB4M0YgfCAweDgwLFxuICAgICAgICBjb2RlUG9pbnQgJiAweDNGIHwgMHg4MFxuICAgICAgKVxuICAgIH0gZWxzZSBpZiAoY29kZVBvaW50IDwgMHgxMTAwMDApIHtcbiAgICAgIGlmICgodW5pdHMgLT0gNCkgPCAwKSBicmVha1xuICAgICAgYnl0ZXMucHVzaChcbiAgICAgICAgY29kZVBvaW50ID4+IDB4MTIgfCAweEYwLFxuICAgICAgICBjb2RlUG9pbnQgPj4gMHhDICYgMHgzRiB8IDB4ODAsXG4gICAgICAgIGNvZGVQb2ludCA+PiAweDYgJiAweDNGIHwgMHg4MCxcbiAgICAgICAgY29kZVBvaW50ICYgMHgzRiB8IDB4ODBcbiAgICAgIClcbiAgICB9IGVsc2Uge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbnZhbGlkIGNvZGUgcG9pbnQnKVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBieXRlc1xufVxuXG5mdW5jdGlvbiBhc2NpaVRvQnl0ZXMgKHN0cikge1xuICB2YXIgYnl0ZUFycmF5ID0gW11cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyArK2kpIHtcbiAgICAvLyBOb2RlJ3MgY29kZSBzZWVtcyB0byBiZSBkb2luZyB0aGlzIGFuZCBub3QgJiAweDdGLi5cbiAgICBieXRlQXJyYXkucHVzaChzdHIuY2hhckNvZGVBdChpKSAmIDB4RkYpXG4gIH1cbiAgcmV0dXJuIGJ5dGVBcnJheVxufVxuXG5mdW5jdGlvbiB1dGYxNmxlVG9CeXRlcyAoc3RyLCB1bml0cykge1xuICB2YXIgYywgaGksIGxvXG4gIHZhciBieXRlQXJyYXkgPSBbXVxuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0ci5sZW5ndGg7ICsraSkge1xuICAgIGlmICgodW5pdHMgLT0gMikgPCAwKSBicmVha1xuXG4gICAgYyA9IHN0ci5jaGFyQ29kZUF0KGkpXG4gICAgaGkgPSBjID4+IDhcbiAgICBsbyA9IGMgJSAyNTZcbiAgICBieXRlQXJyYXkucHVzaChsbylcbiAgICBieXRlQXJyYXkucHVzaChoaSlcbiAgfVxuXG4gIHJldHVybiBieXRlQXJyYXlcbn1cblxuZnVuY3Rpb24gYmFzZTY0VG9CeXRlcyAoc3RyKSB7XG4gIHJldHVybiBiYXNlNjQudG9CeXRlQXJyYXkoYmFzZTY0Y2xlYW4oc3RyKSlcbn1cblxuZnVuY3Rpb24gYmxpdEJ1ZmZlciAoc3JjLCBkc3QsIG9mZnNldCwgbGVuZ3RoKSB7XG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyArK2kpIHtcbiAgICBpZiAoKGkgKyBvZmZzZXQgPj0gZHN0Lmxlbmd0aCkgfHwgKGkgPj0gc3JjLmxlbmd0aCkpIGJyZWFrXG4gICAgZHN0W2kgKyBvZmZzZXRdID0gc3JjW2ldXG4gIH1cbiAgcmV0dXJuIGlcbn1cblxuZnVuY3Rpb24gaXNuYW4gKHZhbCkge1xuICByZXR1cm4gdmFsICE9PSB2YWwgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1zZWxmLWNvbXBhcmVcbn1cbiIsIi8qISBpZWVlNzU0LiBCU0QtMy1DbGF1c2UgTGljZW5zZS4gRmVyb3NzIEFib3VraGFkaWplaCA8aHR0cHM6Ly9mZXJvc3Mub3JnL29wZW5zb3VyY2U+ICovXG5leHBvcnRzLnJlYWQgPSBmdW5jdGlvbiAoYnVmZmVyLCBvZmZzZXQsIGlzTEUsIG1MZW4sIG5CeXRlcykge1xuICB2YXIgZSwgbVxuICB2YXIgZUxlbiA9IChuQnl0ZXMgKiA4KSAtIG1MZW4gLSAxXG4gIHZhciBlTWF4ID0gKDEgPDwgZUxlbikgLSAxXG4gIHZhciBlQmlhcyA9IGVNYXggPj4gMVxuICB2YXIgbkJpdHMgPSAtN1xuICB2YXIgaSA9IGlzTEUgPyAobkJ5dGVzIC0gMSkgOiAwXG4gIHZhciBkID0gaXNMRSA/IC0xIDogMVxuICB2YXIgcyA9IGJ1ZmZlcltvZmZzZXQgKyBpXVxuXG4gIGkgKz0gZFxuXG4gIGUgPSBzICYgKCgxIDw8ICgtbkJpdHMpKSAtIDEpXG4gIHMgPj49ICgtbkJpdHMpXG4gIG5CaXRzICs9IGVMZW5cbiAgZm9yICg7IG5CaXRzID4gMDsgZSA9IChlICogMjU2KSArIGJ1ZmZlcltvZmZzZXQgKyBpXSwgaSArPSBkLCBuQml0cyAtPSA4KSB7fVxuXG4gIG0gPSBlICYgKCgxIDw8ICgtbkJpdHMpKSAtIDEpXG4gIGUgPj49ICgtbkJpdHMpXG4gIG5CaXRzICs9IG1MZW5cbiAgZm9yICg7IG5CaXRzID4gMDsgbSA9IChtICogMjU2KSArIGJ1ZmZlcltvZmZzZXQgKyBpXSwgaSArPSBkLCBuQml0cyAtPSA4KSB7fVxuXG4gIGlmIChlID09PSAwKSB7XG4gICAgZSA9IDEgLSBlQmlhc1xuICB9IGVsc2UgaWYgKGUgPT09IGVNYXgpIHtcbiAgICByZXR1cm4gbSA/IE5hTiA6ICgocyA/IC0xIDogMSkgKiBJbmZpbml0eSlcbiAgfSBlbHNlIHtcbiAgICBtID0gbSArIE1hdGgucG93KDIsIG1MZW4pXG4gICAgZSA9IGUgLSBlQmlhc1xuICB9XG4gIHJldHVybiAocyA/IC0xIDogMSkgKiBtICogTWF0aC5wb3coMiwgZSAtIG1MZW4pXG59XG5cbmV4cG9ydHMud3JpdGUgPSBmdW5jdGlvbiAoYnVmZmVyLCB2YWx1ZSwgb2Zmc2V0LCBpc0xFLCBtTGVuLCBuQnl0ZXMpIHtcbiAgdmFyIGUsIG0sIGNcbiAgdmFyIGVMZW4gPSAobkJ5dGVzICogOCkgLSBtTGVuIC0gMVxuICB2YXIgZU1heCA9ICgxIDw8IGVMZW4pIC0gMVxuICB2YXIgZUJpYXMgPSBlTWF4ID4+IDFcbiAgdmFyIHJ0ID0gKG1MZW4gPT09IDIzID8gTWF0aC5wb3coMiwgLTI0KSAtIE1hdGgucG93KDIsIC03NykgOiAwKVxuICB2YXIgaSA9IGlzTEUgPyAwIDogKG5CeXRlcyAtIDEpXG4gIHZhciBkID0gaXNMRSA/IDEgOiAtMVxuICB2YXIgcyA9IHZhbHVlIDwgMCB8fCAodmFsdWUgPT09IDAgJiYgMSAvIHZhbHVlIDwgMCkgPyAxIDogMFxuXG4gIHZhbHVlID0gTWF0aC5hYnModmFsdWUpXG5cbiAgaWYgKGlzTmFOKHZhbHVlKSB8fCB2YWx1ZSA9PT0gSW5maW5pdHkpIHtcbiAgICBtID0gaXNOYU4odmFsdWUpID8gMSA6IDBcbiAgICBlID0gZU1heFxuICB9IGVsc2Uge1xuICAgIGUgPSBNYXRoLmZsb29yKE1hdGgubG9nKHZhbHVlKSAvIE1hdGguTE4yKVxuICAgIGlmICh2YWx1ZSAqIChjID0gTWF0aC5wb3coMiwgLWUpKSA8IDEpIHtcbiAgICAgIGUtLVxuICAgICAgYyAqPSAyXG4gICAgfVxuICAgIGlmIChlICsgZUJpYXMgPj0gMSkge1xuICAgICAgdmFsdWUgKz0gcnQgLyBjXG4gICAgfSBlbHNlIHtcbiAgICAgIHZhbHVlICs9IHJ0ICogTWF0aC5wb3coMiwgMSAtIGVCaWFzKVxuICAgIH1cbiAgICBpZiAodmFsdWUgKiBjID49IDIpIHtcbiAgICAgIGUrK1xuICAgICAgYyAvPSAyXG4gICAgfVxuXG4gICAgaWYgKGUgKyBlQmlhcyA+PSBlTWF4KSB7XG4gICAgICBtID0gMFxuICAgICAgZSA9IGVNYXhcbiAgICB9IGVsc2UgaWYgKGUgKyBlQmlhcyA+PSAxKSB7XG4gICAgICBtID0gKCh2YWx1ZSAqIGMpIC0gMSkgKiBNYXRoLnBvdygyLCBtTGVuKVxuICAgICAgZSA9IGUgKyBlQmlhc1xuICAgIH0gZWxzZSB7XG4gICAgICBtID0gdmFsdWUgKiBNYXRoLnBvdygyLCBlQmlhcyAtIDEpICogTWF0aC5wb3coMiwgbUxlbilcbiAgICAgIGUgPSAwXG4gICAgfVxuICB9XG5cbiAgZm9yICg7IG1MZW4gPj0gODsgYnVmZmVyW29mZnNldCArIGldID0gbSAmIDB4ZmYsIGkgKz0gZCwgbSAvPSAyNTYsIG1MZW4gLT0gOCkge31cblxuICBlID0gKGUgPDwgbUxlbikgfCBtXG4gIGVMZW4gKz0gbUxlblxuICBmb3IgKDsgZUxlbiA+IDA7IGJ1ZmZlcltvZmZzZXQgKyBpXSA9IGUgJiAweGZmLCBpICs9IGQsIGUgLz0gMjU2LCBlTGVuIC09IDgpIHt9XG5cbiAgYnVmZmVyW29mZnNldCArIGkgLSBkXSB8PSBzICogMTI4XG59XG4iLCJ2YXIgdG9TdHJpbmcgPSB7fS50b1N0cmluZztcblxubW9kdWxlLmV4cG9ydHMgPSBBcnJheS5pc0FycmF5IHx8IGZ1bmN0aW9uIChhcnIpIHtcbiAgcmV0dXJuIHRvU3RyaW5nLmNhbGwoYXJyKSA9PSAnW29iamVjdCBBcnJheV0nO1xufTtcbiIsInZhciBnO1xuXG4vLyBUaGlzIHdvcmtzIGluIG5vbi1zdHJpY3QgbW9kZVxuZyA9IChmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXM7XG59KSgpO1xuXG50cnkge1xuXHQvLyBUaGlzIHdvcmtzIGlmIGV2YWwgaXMgYWxsb3dlZCAoc2VlIENTUClcblx0ZyA9IGcgfHwgbmV3IEZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKTtcbn0gY2F0Y2ggKGUpIHtcblx0Ly8gVGhpcyB3b3JrcyBpZiB0aGUgd2luZG93IHJlZmVyZW5jZSBpcyBhdmFpbGFibGVcblx0aWYgKHR5cGVvZiB3aW5kb3cgPT09IFwib2JqZWN0XCIpIGcgPSB3aW5kb3c7XG59XG5cbi8vIGcgY2FuIHN0aWxsIGJlIHVuZGVmaW5lZCwgYnV0IG5vdGhpbmcgdG8gZG8gYWJvdXQgaXQuLi5cbi8vIFdlIHJldHVybiB1bmRlZmluZWQsIGluc3RlYWQgb2Ygbm90aGluZyBoZXJlLCBzbyBpdCdzXG4vLyBlYXNpZXIgdG8gaGFuZGxlIHRoaXMgY2FzZS4gaWYoIWdsb2JhbCkgeyAuLi59XG5cbm1vZHVsZS5leHBvcnRzID0gZztcbiIsImltcG9ydCB7IHV0aWxpdGllcyB9IGZyb20gXCIuL3V0aWxpdGllc1wiO1xuaW1wb3J0IHsgRW5zUmVxdWVzdCB9IGZyb20gXCIuL2Vuc1JlcXVlc3RcIjtcbmltcG9ydCB7IERhdGFNb25pdG9yaW5nUGFyYW1zIH0gZnJvbSBcIi4vZGF0YU1vbml0b3JpbmdQYXJhbXNcIjtcbmltcG9ydCB7IFVSTCBhcyBVcmwgfSBmcm9tICcuL3VybFBvbnlGaWxsJ1xuaW1wb3J0IGJhc2U2NCBmcm9tIFwiYmFzZTY0LXVybFwiO1xuXG4vKipcbiAqIENoZWNrcyBhIHJlcXVlc3QgZm9yIFBJSSBhbmQgYmxvY2tzIHRoZSByZXF1ZXN0LCBtYXNrcyB0aGUgc2Vuc2l0aXZlIGRhdGEsIG9yIHJlZGFjdHMgaXRcbiAqL1xuZXhwb3J0IGNsYXNzIERhdGFHb3Zlcm5hbmNlTWFuYWdlciB7XG4gICAgcHJpdmF0ZSBlbmFibGVkOiBib29sZWFuO1xuICAgIHByaXZhdGUgZGF0YU1vbml0b3JpbmdQYXJhbXM6IERhdGFNb25pdG9yaW5nUGFyYW1zO1xuICAgIHByaXZhdGUgaW5zcGVjdEZvcm1EYXRhOiBib29sZWFuO1xuICAgIHByaXZhdGUgZW52aXJvbm1lbnQ6IGFueTtcbiAgICBwcml2YXRlIGJsb2NrVVJMUGFyYW1zUmVnZXg6IGFueTtcbiAgICBwcml2YXRlIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXM6IERhdGFNb25pdG9yaW5nUGFyYW1zO1xuICAgIHByaXZhdGUgZGF0YU1vbml0b3JpbmdBbGxvd2VkRG9tYWluc1JlZ2V4OiBSZWdFeHA7XG4gICAgcHJpdmF0ZSBtb2RlOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSBtYXNrQ2hhcmFjdGVyOiBzdHJpbmc7XG4gICAgcHJpdmF0ZSByZWFkb25seSBiYXNlNjRQYXR0ZXJuID0gLygoPzpbQS1aYS16MC05Ky9dezR9KSooPzpbQS1aYS16MC05Ky9dezJ9PT18W0EtWmEtejAtOSsvXXszfT0pPykvZztcblxuICAgIC8qKlxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7Ym9vbGVhbn0gZW5hYmxlZCAtIGlmIGRhdGEgZ292ZXJuYW5jZSBpcyBlbmFibGVkXG4gICAgICogQHBhcmFtIHtEYXRhTW9uaXRvcmluZ1BhcmFtc30gZGF0YU1vbml0b3JpbmdQYXJhbXMgLSB0aGUgZGF0YSBnb3Zlcm5hbmNlIGNvbmZpZ3VyYXRpb25cbiAgICAgKiBAcGFyYW0ge2FueX0gZW52aXJvbm1lbnQgLSB0aGUgYWN0aXZlIGVudmlyb25tZW50XG4gICAgICogQHBhcmFtIHtib29sZWFufSBpbnNwZWN0Rm9ybURhdGEgLSBpZiB3ZSdyZSBhbGxvd2luZyBGb3JtRGF0YSBhbmQgVVJMU2VhcmNoUGFyYW1zIHRvIGJlIHByb2Nlc3NlZFxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBlbmFibGVkOiBib29sZWFuLFxuICAgICAgICBkYXRhTW9uaXRvcmluZ1BhcmFtczogRGF0YU1vbml0b3JpbmdQYXJhbXMsXG4gICAgICAgIGVudmlyb25tZW50OiBhbnksXG4gICAgICAgIGluc3BlY3RGb3JtRGF0YTogYm9vbGVhbixcbiAgICApIHtcbiAgICAgICAgdGhpcy5lbmFibGVkID0gZW5hYmxlZDtcbiAgICAgICAgdGhpcy5kYXRhTW9uaXRvcmluZ1BhcmFtcyA9IGRhdGFNb25pdG9yaW5nUGFyYW1zO1xuICAgICAgICB0aGlzLmVudmlyb25tZW50ID0gZW52aXJvbm1lbnQ7XG4gICAgICAgIHRoaXMuaW5zcGVjdEZvcm1EYXRhID0gaW5zcGVjdEZvcm1EYXRhO1xuICAgICAgICB0aGlzLnVwZGF0ZUVudmlyb25tZW50KGVudmlyb25tZW50KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHVibGljIFZhbGlkYXRlcyB0aGUgZGF0YSBtb25pdG9yaW5nIHBhcmFtcyBieSBhc3NpZ25pbmcgZmFsbGJhY2sgdmFsdWVzIGlmIG5lY2Vzc2FyeVxuICAgICAqIEBwYXJhbSB7RGF0YU1vbml0b3JpbmdQYXJhbXN9IGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMgLSB0aGUgYWN0aXZlIGRhdGEgbW9uaXRvcmluZyBwYXJhbXNcbiAgICAgKi9cbiAgICBwdWJsaWMgdmFsaWRhdGVBY3RpdmF0ZU1vbml0b3JpbmcoYWN0aXZlTW9uaXRvcmluZ1BhcmFtczogRGF0YU1vbml0b3JpbmdQYXJhbXMpOiBhbnkge1xuICAgICAgICBpZihhY3RpdmVNb25pdG9yaW5nUGFyYW1zKSB7XG4gICAgICAgICAgICBhY3RpdmVNb25pdG9yaW5nUGFyYW1zLm1vZGUgID0gYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5tb2RlIHx8IFwiYmxvY2tcIjtcbiAgICAgICAgICAgIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMuYmxvY2tVUkxQYXJhbXNSZWdleCAgPSBhY3RpdmVNb25pdG9yaW5nUGFyYW1zLmJsb2NrVVJMUGFyYW1zUmVnZXggfHwgW107XG4gICAgICAgICAgICBhY3RpdmVNb25pdG9yaW5nUGFyYW1zLmRvbWFpbnNBbGxvd2VkICA9IGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMuZG9tYWluc0FsbG93ZWQgfHwgW107XG4gICAgICAgICAgICBhY3RpdmVNb25pdG9yaW5nUGFyYW1zLm1vbml0b3JCYXNlNjREYXRhICA9IGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMubW9uaXRvckJhc2U2NERhdGEgfHwgZmFsc2U7XG4gICAgICAgICAgICBhY3RpdmVNb25pdG9yaW5nUGFyYW1zLm1hc2tDaGFyYWN0ZXIgID0gYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5tYXNrQ2hhcmFjdGVyIHx8IFwiflwiO1xuICAgICAgICAgICAgYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5wcm9jZXNzUmVxdWVzdEJvZHkgID0gYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5wcm9jZXNzUmVxdWVzdEJvZHkgfHwgZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyBHZXR0ZXIgZnVuY3Rpb24gdGhhdCByZXR1cm5zIHRoZSBhY3RpdmUgZGF0YSBtb25pdG9yaW5nIHBhcmFtc1xuICAgICAqL1xuICAgIHB1YmxpYyBnZXRBY3RpdmVNb25pdG9yaW5nUGFyYW1zKCk6IERhdGFNb25pdG9yaW5nUGFyYW1zIHtcbiAgICAgICAgbGV0IGFjdGl2ZU1vbml0b3JpbmdQYXJhbXM7XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHRoaXMuZW52aXJvbm1lbnQgJiZcbiAgICAgICAgICAgIHRoaXMuZW52aXJvbm1lbnQuaGFzT3duUHJvcGVydHkoXCJkYXRhTW9uaXRvcmluZ1wiKVxuICAgICAgICApIHtcbiAgICAgICAgICAgIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMgPSB0aGlzLnZhbGlkYXRlQWN0aXZhdGVNb25pdG9yaW5nKFxuICAgICAgICAgICAgICAgIHRoaXMuZW52aXJvbm1lbnQuZGF0YU1vbml0b3JpbmdcbiAgICAgICAgICAgICk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBhY3RpdmVNb25pdG9yaW5nUGFyYW1zID1cbiAgICAgICAgICAgICAgICB0aGlzLnZhbGlkYXRlQWN0aXZhdGVNb25pdG9yaW5nKHRoaXMuZGF0YU1vbml0b3JpbmdQYXJhbXMpIHx8XG4gICAgICAgICAgICAgICAge307XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyBTZXR0ZXIgZnVuY3Rpb24gdGhhdCB1cGRhdGVzIHRoZSBlbnZpcm9ubWVudCBhdHRhY2hlZCB0byB0aGlzIGNsYXNzLCBhbmQgdXBkYXRlcyB0aGUgcmVsYXRlZCBkYXRhIGF0dGFjaGVkIHRvIHRoaXMgY2xhc3NcbiAgICAgKiBAcGFyYW0ge2FueX0gZW52IC0gdGhlIG5ldyBlbnZpcm9ubWVudFxuICAgICAqL1xuICAgIHB1YmxpYyB1cGRhdGVFbnZpcm9ubWVudChlbnY6IGFueSk6IHZvaWQge1xuICAgICAgICB0aGlzLmVudmlyb25tZW50ID0gZW52O1xuICAgICAgICB0aGlzLmFjdGl2ZU1vbml0b3JpbmdQYXJhbXMgPSB0aGlzLmdldEFjdGl2ZU1vbml0b3JpbmdQYXJhbXMoKTsgLy8gb3ZlcndyaXRlcyB3aGF0IGlzIGluIGNvbnN0cnVjdG9yIGZyb20gZW52aXJvbm1lbnQ7XG5cbiAgICAgICAgY29uc3QgcmVnZXhBcnJheSA9IHRoaXMuZ2V0QmxvY2tVUkxQYXJhbXNSZWdleEFycmF5KFxuICAgICAgICAgICAgdGhpcy5hY3RpdmVNb25pdG9yaW5nUGFyYW1zXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuYmxvY2tVUkxQYXJhbXNSZWdleCA9IHV0aWxpdGllcy5hcnJheVRvUmVnZXgocmVnZXhBcnJheSk7XG4gICAgICAgIHRoaXMuZGF0YU1vbml0b3JpbmdBbGxvd2VkRG9tYWluc1JlZ2V4ID0gdXRpbGl0aWVzLmFycmF5VG9SZWdleCh0aGlzLmFjdGl2ZU1vbml0b3JpbmdQYXJhbXMuZG9tYWluc0FsbG93ZWQpO1xuXG4gICAgICAgIC8vIGluIGNhc2Ugb2YgZW1wdHkgZW52aXJvbm1lbnRcbiAgICAgICAgdGhpcy5tb2RlID0gdGhpcy5hY3RpdmVNb25pdG9yaW5nUGFyYW1zLm1vZGUgfHwgXCJibG9ja1wiO1xuICAgICAgICB0aGlzLm1hc2tDaGFyYWN0ZXIgPSB0aGlzLmFjdGl2ZU1vbml0b3JpbmdQYXJhbXMubWFza0NoYXJhY3RlciB8fCBcIn5cIjtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHVibGljIENoZWNrcyBmb3IgYmFja3dhcmRzIGNvbXBhdGliaWxpdHkgaW4gdGhlIGJsb2NrVVJMUGFyYW1zUmVnZXggaW4gdGhlICBhY3RpdmVNb25pdG9yaW5nUGFyYW1zXG4gICAqIEBwdWJsaWMgQ2hlY2tzIGZvciBiYWNrd2FyZHMgY29tcGF0aWJpbGl0eSBpbiB0aGUgYmxvY2tVUkxQYXJhbXNSZWdleCBpbiB0aGUgIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMgXG4gICAgICogQHB1YmxpYyBDaGVja3MgZm9yIGJhY2t3YXJkcyBjb21wYXRpYmlsaXR5IGluIHRoZSBibG9ja1VSTFBhcmFtc1JlZ2V4IGluIHRoZSAgYWN0aXZlTW9uaXRvcmluZ1BhcmFtc1xuICAgICAqIEBwYXJhbSB7RGF0YU1vbml0b3JpbmdQYXJhbXN9IGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMgLSB0aGUgYWN0aXZlIGRhdGEgbW9uaXRvcmluZyBwYXJhbXMgdG8gYmUgY2hlY2tlZFxuICAgICAqL1xuICAgIHB1YmxpYyBpc0RhdGFQYXR0ZXJuUmVwb3J0ZWQoXG4gICAgICAgIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXM6IERhdGFNb25pdG9yaW5nUGFyYW1zXG4gICAgKTogYm9vbGVhbiB7XG4gICAgICAgIC8vIGNoZWNrcyBpZiBibG9ja1VSTFBhcmFtc1JlZ2V4IGFycmF5IGNvbnRhaW5zIGN1cnJlbnQgY29uZmlnIHdpdGggcmVnZXgga2V5ICsgdmFsdWVcbiAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMuYmxvY2tVUkxQYXJhbXNSZWdleCAhPSBudWxsICYmXG4gICAgICAgICAgICBhY3RpdmVNb25pdG9yaW5nUGFyYW1zLmJsb2NrVVJMUGFyYW1zUmVnZXgubGVuZ3RoID4gMCAmJlxuICAgICAgICAgICAgdHlwZW9mIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMuYmxvY2tVUkxQYXJhbXNSZWdleFswXS5yZWdleCA9PT1cbiAgICAgICAgICAgICAgICBcInN0cmluZ1wiXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyBDaGVja3MgaWYgdGhlIHJlcXVlc3Qgc2hvdWxkIGJlIGFsbG93ZWQgYWNjb3JkaW5nIHRvIHRoZSBkYXRhIGdvdmVybmFuY2UgcnVsZXMgc2V0IGluIHRoZSBjb25maWd1cmF0aW9uXG4gICAgICogQHBhcmFtIHtFbnNSZXF1ZXN0fSByZXF1ZXN0IC0gdGhlIHJlcXVlc3QgYmVpbmcgY2hlY2tlZFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBob3N0TmFtZSAtIHRoZSBob3N0bmFtZSBvZiB0aGUgcmVxdWVzdCBiZWluZyBjaGVja2VkXG4gICAgICogQHBhcmFtIHthbnl9IHJlcXVlc3RCb2R5IC0gdGhlIGJvZHkgb2YgdGhlIHJlcXVlc3RcbiAgICAgKi9cbiAgICBwdWJsaWMgYWxsb3dSZXF1ZXN0KFxuICAgICAgICByZXF1ZXN0OiBFbnNSZXF1ZXN0LFxuICAgICAgICBob3N0TmFtZTogc3RyaW5nLFxuICAgICAgICByZXF1ZXN0Qm9keT86IGFueVxuICAgICk6IGJvb2xlYW4gfCB1bmRlZmluZWR7XG4gICAgICAgIC8vIE5PVEU6OiBIb3N0bmFtZSBzaG91bGQgcGFyYW0gc2hvdWxkIGJlIHJlZmFjdG9yZWQuICBUaGlzIHNob3VsZCBiZSBkZXJpdmVkIGZyb20gdGhlIFVSTCBpbiB0aGUgcmVxdWVzdFxuICAgICAgICAvLyBpdCBpcyBjdXJyZW50bHkgYmVpbmcgcGFzc2VkIGluIHNvIHRoYXQgd2UgZG9uJ3QgcGFyc2UgdGhlIGhvc3RuYW1lIG11bHRpcGxlIHRpbWVzXG5cbiAgICAgICAgbGV0IGRhdGFNb25pdG9yaW5nQ2hlY2tQYXNzZXM6IGJvb2xlYW4gfCB1bmRlZmluZWQ7XG4gICAgICAgIGNvbnN0IHVybDogc3RyaW5nID0gcmVxdWVzdC5kZXN0aW5hdGlvbjtcblxuICAgICAgICBpZiAodGhpcy5lbmFibGVkKSB7XG4gICAgICAgICAgICBkYXRhTW9uaXRvcmluZ0NoZWNrUGFzc2VzID0gdHJ1ZTtcblxuICAgICAgICAgICAgY29uc3QgZGVjb2RlZFVSTFN0cmluZyA9IHV0aWxpdGllcy5kZWNvZGVVcmlDb21wb25lbnQodXJsKTtcblxuICAgICAgICAgICAgaWYgKHRoaXMuYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5tb25pdG9yQmFzZTY0RGF0YSkge1xuICAgICAgICAgICAgICAgIGRhdGFNb25pdG9yaW5nQ2hlY2tQYXNzZXMgPSAhdGhpcy5jaGVja0ZvckJhc2U2NERhdGEoXG4gICAgICAgICAgICAgICAgICAgIHVybCxcbiAgICAgICAgICAgICAgICAgICAgcmVxdWVzdFxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChkYXRhTW9uaXRvcmluZ0NoZWNrUGFzc2VzKSB7XG4gICAgICAgICAgICAgICAgZGF0YU1vbml0b3JpbmdDaGVja1Bhc3NlcyA9ICF0aGlzLmJsb2NrVVJMUGFyYW1zUmVnZXgudGVzdChcbiAgICAgICAgICAgICAgICAgICAgZGVjb2RlZFVSTFN0cmluZ1xuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLmFjdGl2ZU1vbml0b3JpbmdQYXJhbXMucHJvY2Vzc1JlcXVlc3RCb2R5KSB7XG4gICAgICAgICAgICAgICAgaWYgKHJlcXVlc3RCb2R5KSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBib2R5ID0gcmVxdWVzdEJvZHk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChib2R5IGluc3RhbmNlb2YgRm9ybURhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmluc3BlY3RGb3JtRGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvZHkgPSB0aGlzLmZvcm1EYXRhVG9TdHJpbmcoYm9keSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIENhbid0IHByb2Nlc3MgRm9ybURhdGEgb2JqZWN0cyBvbiBJRVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJvZHkgPSBcIlwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbnNwZWN0Rm9ybURhdGEgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvZHkgaW5zdGFuY2VvZiBVUkxTZWFyY2hQYXJhbXNcbiAgICAgICAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBib2R5ID0gYm9keS50b1N0cmluZygpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5tb25pdG9yQmFzZTY0RGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YU1vbml0b3JpbmdDaGVja1Bhc3NlcyA9ICF0aGlzLmNoZWNrRm9yQmFzZTY0RGF0YShcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib2R5LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVlc3RcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YU1vbml0b3JpbmdDaGVja1Bhc3Nlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YU1vbml0b3JpbmdDaGVja1Bhc3NlcyA9ICF0aGlzLmJsb2NrVVJMUGFyYW1zUmVnZXgudGVzdChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBib2R5XG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maW5kTWF0Y2hlZERhdGFQYXR0ZXJucyhcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5ibG9ja1VSTFBhcmFtc1JlZ2V4LFxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWVzdCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGJvZHlcbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICBkYXRhTW9uaXRvcmluZ0NoZWNrUGFzc2VzID09PSBmYWxzZSAmJlxuICAgICAgICAgICAgICAgIHRoaXMuYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5kb21haW5zQWxsb3dlZC5sZW5ndGggPiAwXG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICBkYXRhTW9uaXRvcmluZ0NoZWNrUGFzc2VzID0gdGhpcy5kYXRhTW9uaXRvcmluZ0FsbG93ZWREb21haW5zUmVnZXgudGVzdChcbiAgICAgICAgICAgICAgICAgICAgaG9zdE5hbWVcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAoIWRhdGFNb25pdG9yaW5nQ2hlY2tQYXNzZXMpIHtcbiAgICAgICAgICAgICAgICByZXF1ZXN0LmFkZFJlYXNvbihcIkRBVEFfTU9OSVRPUklOR1wiKTtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5pc0RhdGFQYXR0ZXJuUmVwb3J0ZWQodGhpcy5hY3RpdmVNb25pdG9yaW5nUGFyYW1zKSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpbmRNYXRjaGVkRGF0YVBhdHRlcm5zKFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hY3RpdmVNb25pdG9yaW5nUGFyYW1zLmJsb2NrVVJMUGFyYW1zUmVnZXgsXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0LFxuICAgICAgICAgICAgICAgICAgICAgICAgZGVjb2RlZFVSTFN0cmluZ1xuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBkYXRhTW9uaXRvcmluZ0NoZWNrUGFzc2VzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgR2V0dGVyIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyBhIG1hc2tlZCB2ZXJzaW9uIG9mIGEgdXJsXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVybCAtIHRoZSB1cmwgd2UncmUgY2hlY2tpbmdcbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0TWFza2VkVXJsKHVybDogc3RyaW5nKSB7XG4gICAgICAgIGlmICh1cmwpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmVkaXRRdWVyeVN0cmluZyhcbiAgICAgICAgICAgICAgICB1cmwsXG4gICAgICAgICAgICAgICAgdXRpbGl0aWVzLnJlcGVhdFN0cmluZyh0aGlzLm1hc2tDaGFyYWN0ZXIsIDEwKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdXJsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgR2V0dGVyIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyBhIHJlZGFjdGVkIHZlcnNpb24gb2YgYSB1cmxcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsIC0gdGhlIHVybCB3ZSdyZSBjaGVja2luZ1xuICAgICAqL1xuICAgIHB1YmxpYyBnZXRSZWRhY3RlZFVybCh1cmw6IHN0cmluZykge1xuICAgICAgICBpZiAodXJsKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5lZGl0UXVlcnlTdHJpbmcodXJsLCBcIlwiKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdXJsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgR2V0dGVyIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyBhIG1hc2tlZCB2ZXJzaW9uIG9mIGEgcmVxdWVzdCBib2R5XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGRhdGEgLSB0aGUgcmVxdWVzdCBib2R5IHdlJ3JlIGNoZWNraW5nXG4gICAgICovXG4gICAgcHVibGljIGdldE1hc2tlZERhdGEoZGF0YTogc3RyaW5nfCBudWxsIHwgdW5kZWZpbmVkKTogYW55IHsgLy8gY2Fubm90IHVzZSB0eXBlIG9iamVjdFxuICAgICAgICBpZiAoZGF0YSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZWRpdERhdGEoXG4gICAgICAgICAgICAgICAgZGF0YSxcbiAgICAgICAgICAgICAgICB1dGlsaXRpZXMucmVwZWF0U3RyaW5nKHRoaXMubWFza0NoYXJhY3RlciwgMTApXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBkYXRhO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgR2V0dGVyIGZ1bmN0aW9uIHRoYXQgcmV0dXJucyBhIHJlZGFjdGVkIHZlcnNpb24gb2YgYSByZXF1ZXN0IGJvZHlcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZGF0YSAtIHRoZSByZXF1ZXN0IGJvZHkgd2UncmUgY2hlY2tpbmdcbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0UmVkYWN0ZWREYXRhKGRhdGE6IHN0cmluZ3wgbnVsbCB8IHVuZGVmaW5lZCk6IGFueSB7IC8vIGNhbm5vdCB1c2UgdHlwZSBvYmplY3RcbiAgICAgICAgaWYgKGRhdGEpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmVkaXREYXRhKGRhdGEsIFwiXCIpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBkYXRhO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgQ2hlY2tzIGlmIGEgcmVxdWVzdCBib2R5IHNob3VsZCBiZSBtYXNrZWQgb3Igbm90XG4gICAgICogQHBhcmFtIHtFbnNSZXF1ZXN0fSByZXF1ZXN0IC0gdGhlIHJlcXVlc3Qgd2UncmUgY2hlY2tpbmdcbiAgICAgKi9cbiAgICBwdWJsaWMgc2hvdWxkTWFza0RhdGEocmVxdWVzdDogRW5zUmVxdWVzdCk6IGJvb2xlYW4ge1xuICAgICAgICBsZXQgc2hvdWxkTWFzayA9IGZhbHNlO1xuXG4gICAgICAgIGlmICh0aGlzLm1vZGUgPT09IFwibWFza1wiICYmIHJlcXVlc3QuaGFzUmVhc29uKFwiREFUQV9NT05JVE9SSU5HXCIpKSB7XG4gICAgICAgICAgICBzaG91bGRNYXNrID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gc2hvdWxkTWFzaztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHVibGljIENoZWNrcyBpZiBhIHJlcXVlc3QgYm9keSBzaG91bGQgYmUgcmVkYWN0ZWQgb3Igbm90XG4gICAgICogQHBhcmFtIHtFbnNSZXF1ZXN0fSByZXF1ZXN0IC0gdGhlIHJlcXVlc3Qgd2UncmUgY2hlY2tpbmdcbiAgICAgKi9cbiAgICBwdWJsaWMgc2hvdWxkUmVkYWN0RGF0YShyZXF1ZXN0OiBFbnNSZXF1ZXN0KTogYm9vbGVhbiB7XG4gICAgICAgIGxldCBzaG91bGRSZWRhY3QgPSBmYWxzZTtcblxuICAgICAgICBpZiAodGhpcy5tb2RlID09PSBcInJlZGFjdFwiICYmIHJlcXVlc3QuaGFzUmVhc29uKFwiREFUQV9NT05JVE9SSU5HXCIpKSB7XG4gICAgICAgICAgICBzaG91bGRSZWRhY3QgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzaG91bGRSZWRhY3Q7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyBDaGVja3MgaWYgYSByZXF1ZXN0IHNob3VsZCBiZSBtYXNrZWQgb3Igbm90XG4gICAgICogQHBhcmFtIHtFbnNSZXF1ZXN0fSByZXF1ZXN0IC0gdGhlIHJlcXVlc3Qgd2UncmUgY2hlY2tpbmdcbiAgICAgKi9cbiAgICBwdWJsaWMgc2hvdWxkTWFza1VybChyZXF1ZXN0OiBFbnNSZXF1ZXN0KTogYm9vbGVhbiB7XG4gICAgICAgIGxldCBzaG91bGRNYXNrID0gZmFsc2U7XG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgdGhpcy5tb2RlID09PSBcIm1hc2tcIiAmJlxuICAgICAgICAgICAgIXJlcXVlc3QuaGFzUmVhc29uKFwiV2hpdGVsaXN0XCIpICYmXG4gICAgICAgICAgICAhcmVxdWVzdC5oYXNSZWFzb24oXCJCbGFja2xpc3RcIikgJiZcbiAgICAgICAgICAgIHJlcXVlc3QuaGFzUmVhc29uKFwiREFUQV9NT05JVE9SSU5HXCIpXG4gICAgICAgICkge1xuICAgICAgICAgICAgc2hvdWxkTWFzayA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHNob3VsZE1hc2s7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyBDaGVja3MgaWYgYSByZXF1ZXN0IHNob3VsZCBiZSByZWRhY3RlZCBvciBub3RcbiAgICAgKiBAcGFyYW0ge0Vuc1JlcXVlc3R9IHJlcXVlc3QgLSB0aGUgcmVxdWVzdCB3ZSdyZSBjaGVja2luZ1xuICAgICAqL1xuICAgIHB1YmxpYyBzaG91bGRSZWRhY3RVcmwocmVxdWVzdDogRW5zUmVxdWVzdCk6IGJvb2xlYW4ge1xuICAgICAgICBsZXQgc2hvdWxkUmVkYWN0ID0gZmFsc2U7XG5cbiAgICAgICAgaWYgKFxuICAgICAgICAgICAgdGhpcy5tb2RlID09PSBcInJlZGFjdFwiICYmXG4gICAgICAgICAgICAhcmVxdWVzdC5oYXNSZWFzb24oXCJXaGl0ZWxpc3RcIikgJiZcbiAgICAgICAgICAgICFyZXF1ZXN0Lmhhc1JlYXNvbihcIkJsYWNrbGlzdFwiKSAmJlxuICAgICAgICAgICAgcmVxdWVzdC5oYXNSZWFzb24oXCJEQVRBX01PTklUT1JJTkdcIilcbiAgICAgICAgKSB7XG4gICAgICAgICAgICBzaG91bGRSZWRhY3QgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzaG91bGRSZWRhY3Q7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgRWRpdHMgdGhlIHBhdGggaW4gdGhlIHJlcXVlc3RcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcGF0aCAtIHRoZSBwYXRoIG9mIHRoZSByZXF1ZXN0IGJlaW5nIGNoZWNrZWRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcmVwbGFjZW1lbnRTdHJpbmcgLSB0aGUgc3RyaW5nIHRvIHJlcGxhY2Ugc2Vuc2l0aXZlIGRhdGFcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IGlzUmVkYWN0ZWQgLSB3aGV0aGVyIG9yIG5vdCBzZW5zaXRpdmUgZGF0YSBzaG91bGQgYmUgcmVkYWN0ZWRcbiAgICAgKi9cbiAgICBwcml2YXRlIGVkaXRQYXRoKFxuICAgICAgICBwYXRoOiBzdHJpbmcsXG4gICAgICAgIHJlcGxhY2VtZW50U3RyaW5nOiBzdHJpbmcsXG4gICAgICAgIGlzUmVkYWN0ZWQ6IGJvb2xlYW5cbiAgICApOiBzdHJpbmcge1xuICAgICAgICBpZiAocGF0aC5pbmRleE9mKFwiO1wiKSA+IC0xKSB7XG4gICAgICAgICAgICBwYXRoID0gdGhpcy5lZGl0U2VtaUNvbG9uRGVsaW1pdGVkUGF0aChcbiAgICAgICAgICAgICAgICBwYXRoLFxuICAgICAgICAgICAgICAgIHJlcGxhY2VtZW50U3RyaW5nLFxuICAgICAgICAgICAgICAgIGlzUmVkYWN0ZWRcbiAgICAgICAgICAgICk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgcGF0aEFycmF5ID0gcGF0aC5zcGxpdChcIi9cIik7XG5cbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXRoQXJyYXkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICBwYXRoQXJyYXlbaV0gIT09IFwiXCIgJiZcbiAgICAgICAgICAgICAgICB0aGlzLmJsb2NrVVJMUGFyYW1zUmVnZXgudGVzdCAmJlxuICAgICAgICAgICAgICAgIHRoaXMuYmxvY2tVUkxQYXJhbXNSZWdleC50ZXN0KHBhdGhBcnJheVtpXSlcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgIHBhdGhBcnJheVtpXSA9IHBhdGhBcnJheVtpXS5yZXBsYWNlKFxuICAgICAgICAgICAgICAgICAgICB0aGlzLmJsb2NrVVJMUGFyYW1zUmVnZXgsXG4gICAgICAgICAgICAgICAgICAgIHJlcGxhY2VtZW50U3RyaW5nXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICBwYXRoQXJyYXlbaV0gPSB1dGlsaXRpZXMuZW5jb2RlVXJpQ29tcG9uZW50KHBhdGhBcnJheVtpXSk7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubW9kZSA9PT0gXCJyZWRhY3RcIiAmJiBwYXRoQXJyYXlbaV0gPT09IFwiXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gdGhpcyBwcmV2ZW50cyBhIHJlcXVlc3QgZnJvbSBiZWNvbWluZyBzb21ldGhpbmcgbGlrZSBodHRwOi8vd3d3LmZvby5jb20vLy8gKGlmIG11bHRpcGxlIHZhbHVlcyBhcmUgcmVkYWN0ZWQpXG4gICAgICAgICAgICAgICAgICAgIHBhdGhBcnJheS5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICAgICAgICAgIGktLTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2UgaWYgKFxuICAgICAgICAgICAgICAgIHRoaXMuYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5tb25pdG9yQmFzZTY0RGF0YSAmJlxuICAgICAgICAgICAgICAgIHBhdGhBcnJheVtpXS5sZW5ndGggPiA0ICYmXG4gICAgICAgICAgICAgICAgdGhpcy5iYXNlNjRQYXR0ZXJuLnRlc3QocGF0aEFycmF5W2ldKVxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgLy8gZGVjb2RlIGFuZCByZXBsYWNlIGJhc2U2NCBkYXRhXG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZGVjb2RlZFN0cmluZyA9IGJhc2U2NC5kZWNvZGUocGF0aEFycmF5W2ldKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ibG9ja1VSTFBhcmFtc1JlZ2V4LnRlc3QgJiZcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmxvY2tVUkxQYXJhbXNSZWdleC50ZXN0KGRlY29kZWRTdHJpbmcpXG4gICAgICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGF0aEFycmF5W2ldID0gcGF0aEFycmF5W2ldLnJlcGxhY2UoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iYXNlNjRQYXR0ZXJuLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlcGxhY2VtZW50U3RyaW5nXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgcGF0aEFycmF5W2ldID0gdXRpbGl0aWVzLmVuY29kZVVyaUNvbXBvbmVudChwYXRoQXJyYXlbaV0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMubW9kZSA9PT0gXCJyZWRhY3RcIiAmJiBwYXRoQXJyYXlbaV0gPT09IFwiXCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyB0aGlzIHByZXZlbnRzIGEgcmVxdWVzdCBmcm9tIGJlY29taW5nIHNvbWV0aGluZyBsaWtlIGh0dHA6Ly93d3cuZm9vLmNvbS8vLyAoaWYgbXVsdGlwbGUgdmFsdWVzIGFyZSByZWRhY3RlZClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXRoQXJyYXkuc3BsaWNlKGksIDEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGktLTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gU3RyaW5nIG5vdCBwcm9wZXJseSBiYXNlNjQgZW5jb2RlZFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBjb25zdCBuZXdQYXRoID0gcGF0aEFycmF5LmpvaW4oXCIvXCIpO1xuICAgICAgICByZXR1cm4gbmV3UGF0aDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZSBDb252ZXJ0cyBmb3JtRGF0YSB0byBhIHN0cmluZ1xuICAgICAqIEBwYXJhbSB7YW55fSBmb3JtRGF0YSAtIHRoZSBmb3JtRGF0YSB3ZSdyZSBjb252ZXJ0aW5nXG4gICAgICovXG4gICAgcHJpdmF0ZSBmb3JtRGF0YVRvU3RyaW5nKGZvcm1EYXRhOiBhbnkpOiBzdHJpbmcge1xuICAgICAgICBjb25zdCBwYXJhbWV0ZXJzID0gW107XG4gICAgICAgIGxldCBlcywgZSwgcGFpcjtcbiAgICAgICAgZm9yIChlcyA9IGZvcm1EYXRhLmVudHJpZXMoKTsgIShlID0gZXMubmV4dCgpKS5kb25lICYmIChwYWlyID0gZS52YWx1ZSk7ICkge1xuICAgICAgICAgICAgcGFyYW1ldGVycy5wdXNoKHBhaXJbMF0gKyBcIj1cIiArIHBhaXJbMV0pO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBwYXJhbWV0ZXJzLmpvaW4oXCImXCIpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIFByb2Nlc3NlcyBhIEJhc2UgNjQgZW5jb2RlZCBTdHJpbmdcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcXVlcnlTdHJpbmcgLSB0aGUgcXVlcnkgc3RyaW5nIG9mIGEgdXJsXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlcGxhY2VtZW50U3RyaW5nQ2hhcnMgLSB0aGUgY2hhcmFjdGVycyB3ZSdsbCByZXBsYWNlIGFueSBzZW5zaXRpdmUgZGF0YSB3aXRoXG4gICAgICovXG4gICAgcHJpdmF0ZSBlZGl0QmFzZTY0U3RyaW5nKHF1ZXJ5U3RyaW5nOiBhbnksIHJlcGxhY2VtZW50U3RyaW5nQ2hhcnM6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIGxldCBuZXdTdHJpbmdWYWwgPSBxdWVyeVN0cmluZztcbiAgICAgICAgY29uc3QgZm91bmQgPSBxdWVyeVN0cmluZy5tYXRjaCh0aGlzLmJhc2U2NFBhdHRlcm4pO1xuICAgICAgICBmb3IgKGNvbnN0IGkgaW4gZm91bmQpIHtcbiAgICAgICAgICAgIGlmIChmb3VuZFtpXS5sZW5ndGggPCA0KSBjb250aW51ZTtcblxuICAgICAgICAgICAgY29uc3QgZGVjb2RlZFN0cmluZyA9IGJhc2U2NC5kZWNvZGUoZm91bmRbaV0pO1xuICAgICAgICAgICAgaWYgKHRoaXMuYmxvY2tVUkxQYXJhbXNSZWdleC50ZXN0KGRlY29kZWRTdHJpbmcpKSB7XG4gICAgICAgICAgICAgICAgbmV3U3RyaW5nVmFsID0gcXVlcnlTdHJpbmcucmVwbGFjZShmb3VuZFtpXSwgcmVwbGFjZW1lbnRTdHJpbmdDaGFycyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbmV3U3RyaW5nVmFsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIFdyYXBwZXIgZnVuY3Rpb24gdGhhdCBwcm9jZXNzZXMgZGF0YSBpbiBhIHJlcXVlc3RcbiAgICAgKiBAcGFyYW0ge0Zvcm1EYXRhIHwgVVJMU2VhcmNoUGFyYW1zIHwgc3RyaW5nfSBkYXRhIC0gdGhlIGRhdGEgdG8gYmUgcHJvY2Vzc2VkXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlcGxhY2VtZW50U3RyaW5nIC0gdGhlIHN0cmluZyB3ZSdsbCByZXBsYWNlIGFueSBzZW5zaXRpdmUgZGF0YSB3aXRoXG4gICAgICovXG4gICAgcHJpdmF0ZSBlZGl0RGF0YShcbiAgICAgICAgZGF0YTogRm9ybURhdGF8IEZvcm1EYXRhRW50cnlWYWx1ZSB8IFVSTFNlYXJjaFBhcmFtcyB8IHN0cmluZyxcbiAgICAgICAgcmVwbGFjZW1lbnRTdHJpbmc6IHN0cmluZ1xuICAgICk6IEZvcm1EYXRhfCBGaWxlIHwgc3RyaW5nIHtcbiAgICAgICAgaWYgKHRoaXMuYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5wcm9jZXNzUmVxdWVzdEJvZHkpIHtcbiAgICAgICAgICAgIGlmIChkYXRhIGluc3RhbmNlb2YgU3RyaW5nIHx8IHR5cGVvZiBkYXRhID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuYWN0aXZlTW9uaXRvcmluZ1BhcmFtcy5tb25pdG9yQmFzZTY0RGF0YSkge1xuICAgICAgICAgICAgICAgICAgICBkYXRhID0gdGhpcy5lZGl0QmFzZTY0U3RyaW5nKGRhdGEsIHJlcGxhY2VtZW50U3RyaW5nKTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBpZiAodGhpcy5ibG9ja1VSTFBhcmFtc1JlZ2V4LnRlc3QpIHtcbiAgICAgICAgICAgICAgICAgICAgd2hpbGUgKHRoaXMuYmxvY2tVUkxQYXJhbXNSZWdleC50ZXN0KGRhdGEpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyByZXBsYWNlIGFsbCBtYXRjaGVzIHdpdGggd2hpbGUgbG9vcFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY2FuJ3QgdXNlIGcgZmxhZyBpbiBibG9ja1VSTFBhcmFtc1JlZ2V4IGJlY2F1c2UgaXQgbWFrZXMgdGhlIFJlZ0V4cCB0ZXN0IG1ldGhvZCB1bnByZWRpY3RhYmxlXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhID0gZGF0YS5yZXBsYWNlKHRoaXMuYmxvY2tVUkxQYXJhbXNSZWdleCwgcmVwbGFjZW1lbnRTdHJpbmcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmluc3BlY3RGb3JtRGF0YSAmJiBkYXRhIGluc3RhbmNlb2YgRm9ybURhdGEpIHtcbiAgICAgICAgICAgICAgICAvLyBDYW4ndCBwcm9jZXNzIEZvcm1EYXRhIG9iamVjdHMgb24gSUVcbiAgICAgICAgICAgICAgICBjb25zdCBuZXdEYXRhID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgICAgICAgICAgICAgbGV0IGVzLCBlLCBwYWlyO1xuICAgICAgICAgICAgICAgIGZvciAoXG4gICAgICAgICAgICAgICAgICAgIGVzID0gZGF0YS5lbnRyaWVzKCk7XG4gICAgICAgICAgICAgICAgICAgICEoZSA9IGVzLm5leHQoKSkuZG9uZSAmJiAocGFpciA9IGUudmFsdWUpO1xuXG4gICAgICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGtleSA9IHRoaXMuZWRpdERhdGEocGFpclswXSwgcmVwbGFjZW1lbnRTdHJpbmcpIGFzIHN0cmluZztcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLmVkaXREYXRhKHBhaXJbMV0sIHJlcGxhY2VtZW50U3RyaW5nKSBhcyBzdHJpbmc7XG4gICAgICAgICAgICAgICAgICAgIG5ld0RhdGEuYXBwZW5kKGtleSwgdmFsdWUpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGRhdGEgPSBuZXdEYXRhO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmluc3BlY3RGb3JtRGF0YSAmJiBkYXRhIGluc3RhbmNlb2YgVVJMU2VhcmNoUGFyYW1zKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbmV3RGF0YSA9IG5ldyBVUkxTZWFyY2hQYXJhbXMoKTtcbiAgICAgICAgICAgICAgICBsZXQgZXMsIGUsIHBhaXI7XG4gICAgICAgICAgICAgICAgZm9yIChlcyA9IGRhdGEuZW50cmllcygpOyAhKGUgPSBlcy5uZXh0KCkpLmRvbmUgJiYgKHBhaXIgPSBlLnZhbHVlKTsgKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGtleSA9IHRoaXMuZWRpdERhdGEocGFpclswXSwgcmVwbGFjZW1lbnRTdHJpbmcpIGFzIHN0cmluZztcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLmVkaXREYXRhKHBhaXJbMV0sIHJlcGxhY2VtZW50U3RyaW5nKSBhcyBzdHJpbmc7XG4gICAgICAgICAgICAgICAgICAgIG5ld0RhdGEuYXBwZW5kKGtleSwgdmFsdWUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBkYXRhID0gbmV3RGF0YTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBkYXRhO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIENoZWNrcyBhIHF1ZXJ5IHN0cmluZyBmb3IgZGF0YSBnb3Zlcm5hbmNlIHZpb2xhdGlvbnNcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsU3RyaW5nIC0gYSBzdHJpbmcgb2YgdGhlIHVybCBiZWluZyBjaGVja2VkXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlcGxhY2VtZW50U3RyaW5nIC0gdGhlIHN0cmluZyB3ZSdsbCByZXBsYWNlIGFueSBzZW5zaXRpdmUgZGF0YSB3aXRoXG4gICAgICovXG4gICAgcHJpdmF0ZSBlZGl0UXVlcnlTdHJpbmcodXJsU3RyaW5nOiBzdHJpbmcsIHJlcGxhY2VtZW50U3RyaW5nQ2hhcnM6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIGlmICghdGhpcy5lbmFibGVkIHx8ICF1dGlsaXRpZXMuaXNWYWxpZFVybCh1cmxTdHJpbmcpKSByZXR1cm4gdXJsU3RyaW5nO1xuXG4gICAgICAgIGNvbnN0IGlzUmVkYWN0ZWQgPSB0aGlzLm1vZGUgPT09IFwicmVkYWN0XCI7XG4gICAgICAgIGxldCByZXBsYWNlbWVudFN0cmluZyA9IHJlcGxhY2VtZW50U3RyaW5nQ2hhcnM7XG5cbiAgICAgICAgLy8gcHJvdGVjdHMgYWdhaW5zdCBKUyAnJyByZXNvbHZpbmcgdG8gZmFsc3lcbiAgICAgICAgaWYgKHJlcGxhY2VtZW50U3RyaW5nQ2hhcnMgPT09IFwiXCIpIHtcbiAgICAgICAgICAgIHJlcGxhY2VtZW50U3RyaW5nID0gXCJcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIGhhdmUgdG8gdXNlIHRoaXMgYmVmb3JlIGNhbGxpbmcgVVJMIGNvbnN0cnVjdG9yLCBiZWNhdXNlIGl0IGRlbGltaXRzIHN0cmljdGx5IGJhc2VkIG9uICYgY2hhcmFjdGVyLFxuICAgICAgICAvLyB3aGljaCBoYXMgdGhlIHNpZGUgZWZmZWN0IG9mIGJyZWFraW5nIHVwIGEgdXJsIGluIGEgcXVlcnkgc3RyaW5nXG4gICAgICAgIFxuICAgICAgICBsZXQgcXVlcnlTdHJpbmcgPSB1cmxTdHJpbmcuc3BsaXQoXCI/XCIpWzFdO1xuXG4gICAgICAgIGNvbnN0IHVybCA9IG5ldyBVcmwodXJsU3RyaW5nKTtcblxuICAgICAgICAvLyBjaGVjayB1cmwgcGF0aCBmb3Igc2Vuc2l0aXZlIGRhdGFcbiAgICAgICAgdXJsLnBhdGhuYW1lID0gdGhpcy5lZGl0UGF0aCh1cmwucGF0aG5hbWUsIHJlcGxhY2VtZW50U3RyaW5nLCBpc1JlZGFjdGVkKTtcblxuICAgICAgICAvL3ZhbGlkYXRlIHF1ZXJ5IHN0cmluZyBhbmQgY2hlY2sgaXQgZm9yIHNlbnNpdGl2ZSBkYXRhXG4gICAgICAgIGlmICh0eXBlb2YgcXVlcnlTdHJpbmcgPT09IFwic3RyaW5nXCIgJiYgcXVlcnlTdHJpbmcgIT09IFwiXCIpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmFjdGl2ZU1vbml0b3JpbmdQYXJhbXMubW9uaXRvckJhc2U2NERhdGEpIHtcbiAgICAgICAgICAgICAgICBxdWVyeVN0cmluZyA9IHRoaXMuZWRpdEJhc2U2NFN0cmluZyhxdWVyeVN0cmluZywgcmVwbGFjZW1lbnRTdHJpbmcpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjb25zdCBzZWFyY2hQYXJhbXMgPSBxdWVyeVN0cmluZy5zcGxpdChcIiZcIik7XG4gICAgICAgICAgICBpZiAoc2VhcmNoUGFyYW1zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICB1cmwuc2VhcmNoID0gdGhpcy5lZGl0U2VhcmNoUGFyYW1zKFxuICAgICAgICAgICAgICAgICAgICBzZWFyY2hQYXJhbXMsXG4gICAgICAgICAgICAgICAgICAgIHJlcGxhY2VtZW50U3RyaW5nLFxuICAgICAgICAgICAgICAgICAgICBpc1JlZGFjdGVkXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICBpZiAoaXNSZWRhY3RlZCAmJiB1cmwuc2VhcmNoID09PSBcIlwiKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIHRoZSBlbnRpcmUgcXVlcnkgc3RyaW5nIGlzIHJlZGFjdGVkLCByZW1vdmUgdGhlIHF1ZXN0aW9uIG1hcmsgZnJvbSB0aGUgdXJsXG4gICAgICAgICAgICAgICAgICAgIHVybC5ocmVmID0gdXJsLmhyZWYucmVwbGFjZShcIj9cIiwgXCJcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHVybC5ocmVmO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIEdldHRlciBmdW5jdGlvbiB0aGF0IHJldHVybnMgYW4gYXJyYXkgdGhhdCBjb250YWlucyBhbGwgdGhlIHJlZ2V4ZXMgd2UncmUgY2hlY2tpbmcgYWdhaW5zdFxuICAgICAqIEBwYXJhbSB7RGF0YU1vbml0b3JpbmdQYXJhbXN9IGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMgLSB0aGUgYWN0aXZlIG1vbml0b3JpbmcgcGFyYW1zXG4gICAgICovXG4gICAgcHJpdmF0ZSBnZXRCbG9ja1VSTFBhcmFtc1JlZ2V4QXJyYXkoXG4gICAgICAgIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXM6IERhdGFNb25pdG9yaW5nUGFyYW1zXG4gICAgKTogQXJyYXk8UmVnRXhwPiB7XG4gICAgICAgIGNvbnN0IHJlZ2V4QXJyYXkgPSBbXTtcbiAgICAgICAgLy8gYmFja3dhcmRzIGNvbXBhdGliaWxpdHkgY2hlY2tcbiAgICAgICAgaWYgKCF0aGlzLmlzRGF0YVBhdHRlcm5SZXBvcnRlZChhY3RpdmVNb25pdG9yaW5nUGFyYW1zKSkge1xuICAgICAgICAgICAgcmV0dXJuIGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMuYmxvY2tVUkxQYXJhbXNSZWdleCB8fCBbXTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGZvciAoXG4gICAgICAgICAgICAgICAgbGV0IGkgPSAwO1xuICAgICAgICAgICAgICAgIGkgPCBhY3RpdmVNb25pdG9yaW5nUGFyYW1zLmJsb2NrVVJMUGFyYW1zUmVnZXgubGVuZ3RoO1xuICAgICAgICAgICAgICAgIGkrK1xuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgcmVnZXhBcnJheS5wdXNoKGFjdGl2ZU1vbml0b3JpbmdQYXJhbXMuYmxvY2tVUkxQYXJhbXNSZWdleFtpXS5yZWdleCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHJlZ2V4QXJyYXk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgQXR0YWNoZXMgZGF0YSBwYXR0ZXJucyB0byBhIHJlcXVlc3QgdGhhdCdzIGJsb2NrZWQgZm9yIHJlcG9ydGluZyBwdXJwb3Nlc1xuICAgICAqIEBwYXJhbSB7QXJyYXk8YW55Pn0gYmxvY2tVUkxQYXJhbXNSZWdleCAtIHRoZSBhcnJheSBvZiByZWd1bGFyIGV4cHJlc3Npb25zIHdlJ3JlIGNoZWNraW5nIGFnYWluc3RcbiAgICAgKiBAcGFyYW0ge0Vuc1JlcXVlc3R9IHJlcXVlc3QgLSB0aGUgYmxvY2tlZCByZXF1ZXN0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGRlY29kZWRVUkxTdHJpbmcgLSBhIGRlY29kZWQgdmVyc2lvbiBvZiB0aGUgdXJsIHN0cmluZ1xuICAgICAqL1xuICAgIHByaXZhdGUgZmluZE1hdGNoZWREYXRhUGF0dGVybnMoXG4gICAgICAgIGJsb2NrVVJMUGFyYW1zUmVnZXg6IEFycmF5PGFueT4sXG4gICAgICAgIHJlcXVlc3Q6IEVuc1JlcXVlc3QsXG4gICAgICAgIGRlY29kZWRVUkxTdHJpbmc6IHN0cmluZ1xuICAgICk6IHZvaWQge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGJsb2NrVVJMUGFyYW1zUmVnZXgubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGNvbnN0IHJlZ2V4ID0gbmV3IFJlZ0V4cChibG9ja1VSTFBhcmFtc1JlZ2V4W2ldLnJlZ2V4KTtcbiAgICAgICAgICAgIGlmIChyZWdleC50ZXN0ICYmIHJlZ2V4LnRlc3QoZGVjb2RlZFVSTFN0cmluZykpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBtYXRjaGVkRGF0YVBhdHRlcm4gPSBibG9ja1VSTFBhcmFtc1JlZ2V4W2ldLm5hbWU7XG4gICAgICAgICAgICAgICAgcmVxdWVzdC5kYXRhUGF0dGVybnMucHVzaChtYXRjaGVkRGF0YVBhdHRlcm4pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgQ2hlY2tzIGlmIGEgdXJsIGNvbnRhaW5zIGJhc2UgNjQgZW5jb2RlZCBkYXRhXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVybCAtIHRoZSB1cmwgd2UncmUgY2hlY2tpbmdcbiAgICAgKiBAcGFyYW0ge0Vuc1JlcXVlc3R9IHJlcXVlc3QgLSB0aGUgcmVxdWVzdCBvYmplY3QgdGhhdCB0aGUgdXJsIGlzIGFzc29jaWF0ZWQgd2l0aFxuICAgICAqL1xuICAgIHByaXZhdGUgY2hlY2tGb3JCYXNlNjREYXRhKHVybDogc3RyaW5nLCByZXF1ZXN0OiBFbnNSZXF1ZXN0KTogYm9vbGVhbiB7XG4gICAgICAgIGNvbnN0IGZvdW5kID0gdXJsLm1hdGNoKHRoaXMuYmFzZTY0UGF0dGVybik7XG4gICAgICAgIGZvciAoY29uc3QgaSBpbiBmb3VuZCkge1xuICAgICAgICAgICAgaWYgKGZvdW5kW2ldLmxlbmd0aCA8IDQpIGNvbnRpbnVlO1xuXG4gICAgICAgICAgICBjb25zdCBkZWNvZGVkU3RyaW5nID0gYmFzZTY0LmRlY29kZShmb3VuZFtpXSk7XG4gICAgICAgICAgICBpZiAodGhpcy5ibG9ja1VSTFBhcmFtc1JlZ2V4LnRlc3QoZGVjb2RlZFN0cmluZykpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmZpbmRNYXRjaGVkRGF0YVBhdHRlcm5zKFxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZU1vbml0b3JpbmdQYXJhbXMuYmxvY2tVUkxQYXJhbXNSZWdleCxcbiAgICAgICAgICAgICAgICAgICAgcmVxdWVzdCxcbiAgICAgICAgICAgICAgICAgICAgZGVjb2RlZFN0cmluZ1xuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgcmVxdWVzdC5kYXRhUGF0dGVybnMucHVzaChcImJhc2U2NFwiKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZSBlZGl0cyB0aGUgcGF0aCBvZiBhIHJlcXVlc3QgdGhhdCdzIGRlbGltaXRlZCB3aXRoIHNlbWkgY29sb25zXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHBhdGggLSB0aGUgdXJsIHBhdGggdG8gYmUgcHJvY2Vzc2VkXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlcGxhY2VtZW50U3RyaW5nIC0gdGhlIHN0cmluZyB3ZSdsbCByZXBsYWNlIGFueSBtYXRjaGVzIHdpdGhcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IGlzUmVkYWN0ZWQgLSB3aGV0aGVyIG9yIG5vdCB3ZSdyZSBpbiByZWRhY3Rpb24gbW9kZVxuICAgICAqL1xuICAgIHByaXZhdGUgZWRpdFNlbWlDb2xvbkRlbGltaXRlZFBhdGgoXG4gICAgICAgIHBhdGg6IHN0cmluZyxcbiAgICAgICAgcmVwbGFjZW1lbnRTdHJpbmc6IHN0cmluZyxcbiAgICAgICAgaXNSZWRhY3RlZDogYm9vbGVhblxuICAgICk6IHN0cmluZyB7XG4gICAgICAgIC8vIHNwbGl0IHVwIGVhY2ggcGF0aCBlbnRyeSBpbnRvIGFuIGFycmF5LCBjYW4gaGF2ZSBub24gc2VtaS1jb2xvbiBkZWxpbWl0ZWQga2V5IHZhbHVlIHBhaXJzXG4gICAgICAgIGxldCBwYXRoRW50cmllc0FycmF5OiBBcnJheTxzdHJpbmc+ID0gcGF0aC5zcGxpdChcIi9cIik7XG5cbiAgICAgICAgLy8gcmVtb3ZlIGVtcHR5IHZhbHVlcyBmcm9tIHBhdGhFbnRyaWVzQXJyYXlcbiAgICAgICAgcGF0aEVudHJpZXNBcnJheSA9IHBhdGhFbnRyaWVzQXJyYXkuZmlsdGVyKGZ1bmN0aW9uIChwYXJhbSkge1xuICAgICAgICAgICAgcmV0dXJuIHR5cGVvZiBwYXJhbSA9PT0gXCJzdHJpbmdcIiAmJiBwYXJhbSAhPT0gXCJcIjtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgbGV0IG5ld1BhdGggPSBcIlwiO1xuXG4gICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzO1xuXG4gICAgICAgIHV0aWxpdGllcy5lYWNoKHBhdGhFbnRyaWVzQXJyYXksIGZ1bmN0aW9uIChwYXRoRW50cnksIGluZGV4KSB7XG4gICAgICAgICAgICAvLyB3aXRoaW4gZWFjaCBwYXRoIGVudHJ5LCBzcGxpdCBpbnRvIGtleS12YWx1ZSBwYWlyc1xuICAgICAgICAgICAgY29uc3QgcGF0aEtleVZhbHVlQXJyYXkgPSBwYXRoRW50cnkuc3BsaXQoXCI7XCIpO1xuICAgICAgICAgICAgdXRpbGl0aWVzLmVhY2gocGF0aEtleVZhbHVlQXJyYXksIGZ1bmN0aW9uIChwYXJhbSwgaW5kZXgpIHtcbiAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICBjb25zdCBwYXJhbUFycmF5ID0gcGFyYW0uc3BsaXQoXCI9XCIpO1xuICAgICAgICAgICAgICAgIGxldCBrZXkgPSBwYXJhbUFycmF5WzBdO1xuICAgICAgICAgICAgICAgIGNvbnN0IHVuRGVjb2RlZFVyaUNvbXBvbmVudCA9IHBhcmFtQXJyYXlbMV0gfHwgXCJcIjtcbiAgICAgICAgICAgICAgICBsZXQgaXNCbG9ja2VkUGFyYW1zID0gdW5kZWZpbmVkOyAvLyBldmFsdWF0ZXMgdG8gdHJ1ZSBpZiBwYXJhbSBzaG91bGQgYmUgYmxvY2tlZCwgd2lsbCB1c2UgdGhpcyB0byByZW1vdmUgcXVlcnkgcGFyYW1zIGZvciByZWRhY3RlZCBVcmxzO1xuICAgICAgICAgICAgICAgIGxldCB2YWx1ZSA9IHV0aWxpdGllcy5kZWNvZGVVcmlDb21wb25lbnQodW5EZWNvZGVkVXJpQ29tcG9uZW50KTtcbiAgICAgICAgICAgICAgICBsZXQgbmV3S2V5VmFsdWUgPSBcIlwiO1xuICAgICAgICAgICAgICAgIC8vIGNoZWNrIGtleSBvZiBwYXRoIHBhcmFtXG4gICAgICAgICAgICAgICAgaWYgKFxuICAgICAgICAgICAgICAgICAgICB0eXBlb2Yga2V5ID09PSBcInN0cmluZ1wiICYmXG4gICAgICAgICAgICAgICAgICAgIGtleSAhPT0gXCJcIiAmJlxuICAgICAgICAgICAgICAgICAgICB0aGF0LmJsb2NrVVJMUGFyYW1zUmVnZXgudGVzdFxuICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICB3aGlsZSAodGhhdC5ibG9ja1VSTFBhcmFtc1JlZ2V4LnRlc3Qoa2V5KSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaXNCbG9ja2VkUGFyYW1zID0gaXNSZWRhY3RlZCA/IHRydWUgOiBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlcGxhY2UgYWxsIG1hdGNoZXMgd2l0aCB3aGlsZSBsb29wXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBjYW4ndCB1c2UgZyBmbGFnIGluIGJsb2NrVVJMUGFyYW1zUmVnZXggYmVjYXVzZSBpdCBtYWtlcyB0aGUgUmVnRXhwIHRlc3QgbWV0aG9kIHVucHJlZGljdGFibGVcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleSA9IGtleS5yZXBsYWNlKHRoYXQuYmxvY2tVUkxQYXJhbXNSZWdleCwgcmVwbGFjZW1lbnRTdHJpbmcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIC8vIGNoZWNrIHZhbHVlIG9mIHBhdGggcGFyYW1cbiAgICAgICAgICAgICAgICBpZiAoXG4gICAgICAgICAgICAgICAgICAgIHR5cGVvZiB2YWx1ZSA9PT0gXCJzdHJpbmdcIiAmJlxuICAgICAgICAgICAgICAgICAgICB2YWx1ZSAhPT0gXCJcIiAmJlxuICAgICAgICAgICAgICAgICAgICB0aGF0LmJsb2NrVVJMUGFyYW1zUmVnZXgudGVzdFxuICAgICAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgICAgICB3aGlsZSAodGhhdC5ibG9ja1VSTFBhcmFtc1JlZ2V4LnRlc3QodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyByZXBsYWNlIGFsbCBtYXRjaGVzIHdpdGggd2hpbGUgbG9vcFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY2FuJ3QgdXNlIGcgZmxhZyBpbiBibG9ja1VSTFBhcmFtc1JlZ2V4IGJlY2F1c2UgaXQgbWFrZXMgdGhlIFJlZ0V4cCB0ZXN0IG1ldGhvZCB1bnByZWRpY3RhYmxlXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0Jsb2NrZWRQYXJhbXMgPSBpc1JlZGFjdGVkID8gdHJ1ZSA6IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSB2YWx1ZS5yZXBsYWNlKHRoYXQuYmxvY2tVUkxQYXJhbXNSZWdleCwgcmVwbGFjZW1lbnRTdHJpbmcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBrZXkgPT09IFwic3RyaW5nXCIgJiYgdHlwZW9mIHZhbHVlID09PSBcInN0cmluZ1wiKSB7IC8vIGFkZCByZXN1bHRzIGJhY2sgdG8gbmV3UGF0aCBhZnRlciB0eXBlIHZhbGlkYXRpb24sIGZvciBhY3R1YWwgcGF0aCBwYXJhbWV0ZXJzXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlID0gdXRpbGl0aWVzLmVuY29kZVVyaUNvbXBvbmVudCh2YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChpc0Jsb2NrZWRQYXJhbXMgJiYgaXNSZWRhY3RlZCkgeyAvLyBTaW5nbGUgcGF0aCB2YWx1ZXMgd2l0aG91dCBhIGtleSB2YWx1ZSBwYWlyIGdldCBibG9ja2VkIGFzIHdlbGxcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ld0tleVZhbHVlICs9IFwiXCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodmFsdWUgPT09IFwiXCIgJiYgIWlzQmxvY2tlZFBhcmFtcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgbmV3S2V5VmFsdWUgKz0ga2V5O1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgbmV3S2V5VmFsdWUgKz0ga2V5ICsgXCI9XCIgKyB2YWx1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmKG5ld0tleVZhbHVlID09PSBcIlwiKXtcbiAgICAgICAgICAgICAgICAgICAgcGF0aEtleVZhbHVlQXJyYXkuc3BsaWNlKGluZGV4LCAxKTsgLy9SZW1vdmUgYXNzb2NpYXRlZCB2YWx1ZSBmcm9tIGFycmF5IGlmIG5vdCB2YWxpZFxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHBhdGhLZXlWYWx1ZUFycmF5W2luZGV4XSA9IG5ld0tleVZhbHVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBwYXRoRW50cmllc0FycmF5W2luZGV4XSA9IHBhdGhLZXlWYWx1ZUFycmF5LmpvaW4oXCI7XCIpO1xuXG4gICAgICAgIH0pO1xuXG4gICAgICAgIG5ld1BhdGggKz0gcGF0aEVudHJpZXNBcnJheS5qb2luKFwiL1wiKTtcblxuICAgICAgICByZXR1cm4gbmV3UGF0aDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZSBFZGl0cyB0aGUgcXVlcnkgc3RyaW5nIG9mIGEgdXJsXG4gICAgICogQHBhcmFtIHtBcnJheTxzdHJpbmc+fSBzZWFyY2hQYXJhbXMgLSBhbiBhcnJheSBjb250YWluaW5nIGFsbCB0aGUgc2VhcmNoIHBhcmFtc1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfSByZXBsYWNlbWVudFN0cmluZyAtIHRoZSBzdHJpbmcgd2UnbGwgcmVwbGFjZSBhbnkgbWF0Y2hlcyB3aXRoXG4gICAgICogQHBhcmFtIHtib29sZWFufSBpc1JlZGFjdGVkIC0gd2hldGhlciBvciBub3Qgd2UncmUgaW4gcmVkYWN0aW9uIG1vZGVcbiAgICAgKi9cbiAgICBwcml2YXRlIGVkaXRTZWFyY2hQYXJhbXMoXG4gICAgICAgIHNlYXJjaFBhcmFtczogQXJyYXk8c3RyaW5nPixcbiAgICAgICAgcmVwbGFjZW1lbnRTdHJpbmc6IHN0cmluZyxcbiAgICAgICAgaXNSZWRhY3RlZDogYm9vbGVhblxuICAgICk6IHN0cmluZyB7XG4gICAgICAgIC8vZmlsdGVyIGZvciBtdWx0aXBsZSBwYXJhbXNcbiAgICAgICAgbGV0IG5ld1F1ZXJ5U3RyaW5nID0gXCJcIjtcbiAgICAgICAgc2VhcmNoUGFyYW1zID0gc2VhcmNoUGFyYW1zLmZpbHRlcihmdW5jdGlvbiAocGFyYW0pIHtcbiAgICAgICAgICAgIHJldHVybiB0eXBlb2YgcGFyYW0gPT09IFwic3RyaW5nXCIgJiYgcGFyYW0gIT09IFwiXCI7XG4gICAgICAgIH0pO1xuICAgICAgICBjb25zdCB0aGF0ID0gdGhpcztcblxuICAgICAgICB1dGlsaXRpZXMuZWFjaChzZWFyY2hQYXJhbXMsIGZ1bmN0aW9uIChwYXJhbSkge1xuICAgICAgICAgICAgY29uc3QgcGFyYW1BcnJheSA9IHBhcmFtLnNwbGl0KFwiPVwiKTtcbiAgICAgICAgICAgIGxldCBrZXkgPSBwYXJhbUFycmF5WzBdO1xuICAgICAgICAgICAgY29uc3QgdW5EZWNvZGVkVXJpQ29tcG9uZW50ID0gcGFyYW1BcnJheVsxXSB8fCBcIlwiO1xuICAgICAgICAgICAgbGV0IGlzQmxvY2tlZFBhcmFtczsgLy8gZXZhbHVhdGVzIHRvIHRydWUgaWYgcGFyYW0gc2hvdWxkIGJlIGJsb2NrZWQsIHdpbGwgdXNlIHRoaXMgdG8gcmVtb3ZlIHF1ZXJ5IHBhcmFtcyBmb3IgcmVkYWN0ZWQgVXJscztcbiAgICAgICAgICAgIGxldCB2YWx1ZSA9IHV0aWxpdGllcy5kZWNvZGVVcmlDb21wb25lbnQodW5EZWNvZGVkVXJpQ29tcG9uZW50KTtcbiAgICAgICAgICAgIC8vIGNoZWNrIGtleSBvZiBzZWFyY2ggcGFyYW1cbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICB0eXBlb2Yga2V5ID09PSBcInN0cmluZ1wiICYmXG4gICAgICAgICAgICAgICAga2V5ICE9PSBcIlwiICYmXG4gICAgICAgICAgICAgICAgdGhhdC5ibG9ja1VSTFBhcmFtc1JlZ2V4LnRlc3RcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgIHdoaWxlICh0aGF0LmJsb2NrVVJMUGFyYW1zUmVnZXgudGVzdChrZXkpKSB7XG4gICAgICAgICAgICAgICAgICAgIGlzQmxvY2tlZFBhcmFtcyA9IGlzUmVkYWN0ZWQgPyB0cnVlIDogZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIC8vIHJlcGxhY2UgYWxsIG1hdGNoZXMgd2l0aCB3aGlsZSBsb29wXG4gICAgICAgICAgICAgICAgICAgIC8vIGNhbid0IHVzZSBnIGZsYWcgaW4gYmxvY2tVUkxQYXJhbXNSZWdleCBiZWNhdXNlIGl0IG1ha2VzIHRoZSBSZWdFeHAgdGVzdCBtZXRob2QgdW5wcmVkaWN0YWJsZVxuICAgICAgICAgICAgICAgICAgICBrZXkgPSBrZXkucmVwbGFjZShcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoYXQuYmxvY2tVUkxQYXJhbXNSZWdleCxcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcGxhY2VtZW50U3RyaW5nXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBjaGVjayB2YWx1ZSBvZiBzZWFyY2ggcGFyYW1cbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICB0eXBlb2YgdmFsdWUgPT09IFwic3RyaW5nXCIgJiZcbiAgICAgICAgICAgICAgICB2YWx1ZSAhPT0gXCJcIiAmJlxuICAgICAgICAgICAgICAgIHRoYXQuYmxvY2tVUkxQYXJhbXNSZWdleC50ZXN0XG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICB3aGlsZSAodGhhdC5ibG9ja1VSTFBhcmFtc1JlZ2V4LnRlc3QodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIHJlcGxhY2UgYWxsIG1hdGNoZXMgd2l0aCB3aGlsZSBsb29wXG4gICAgICAgICAgICAgICAgICAgIC8vIGNhbid0IHVzZSBnIGZsYWcgaW4gYmxvY2tVUkxQYXJhbXNSZWdleCBiZWNhdXNlIGl0IG1ha2VzIHRoZSBSZWdFeHAgdGVzdCBtZXRob2QgdW5wcmVkaWN0YWJsZVxuICAgICAgICAgICAgICAgICAgICBpc0Jsb2NrZWRQYXJhbXMgPSBpc1JlZGFjdGVkID8gdHJ1ZSA6IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLnJlcGxhY2UodGhhdC5ibG9ja1VSTFBhcmFtc1JlZ2V4LCByZXBsYWNlbWVudFN0cmluZyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBhZGQgcmVzdWx0cyBiYWNrIHRvIG5ld1F1ZXJ5U3RyaW5nIGFmdGVyIHR5cGUgdmFsaWRhdGlvblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBrZXkgPT09IFwic3RyaW5nXCIgJiYgdHlwZW9mIHZhbHVlID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICAgICAgdmFsdWUgPSB1dGlsaXRpZXMuZW5jb2RlVXJpQ29tcG9uZW50KHZhbHVlKTtcbiAgICAgICAgICAgICAgICBpZiAoaXNCbG9ja2VkUGFyYW1zICYmIGlzUmVkYWN0ZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgbmV3UXVlcnlTdHJpbmcgKz0gXCJcIjtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZhbHVlID09PSBcIlwiICYmICFpc0Jsb2NrZWRQYXJhbXMpIHtcbiAgICAgICAgICAgICAgICAgICAgbmV3UXVlcnlTdHJpbmcgKz0ga2V5ICsgXCImXCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbmV3UXVlcnlTdHJpbmcgKz0ga2V5ICsgXCI9XCIgKyB2YWx1ZSArIFwiJlwiO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gcmVtb3ZlICYgYXQgZW5kIG9mIHF1ZXJ5IHN0cmluZ1xuICAgICAgICBuZXdRdWVyeVN0cmluZyA9IG5ld1F1ZXJ5U3RyaW5nLnNsaWNlKDAsIC0xKTtcbiAgICAgICAgcmV0dXJuIG5ld1F1ZXJ5U3RyaW5nO1xuICAgIH1cblxufVxuIiwiZXhwb3J0IGNsYXNzIEVuc1JlcXVlc3Qge1xuICBwdWJsaWMgdHlwZTogc3RyaW5nO1xuICBwdWJsaWMgc3RhcnQ6IG51bWJlciA9ICtuZXcgRGF0ZSgpO1xuICBwdWJsaWMgZW5kID0gLTE7XG4gIHB1YmxpYyBzb3VyY2U6IHN0cmluZyA9IG51bGw7XG4gIHB1YmxpYyBzdGF0dXM6IHN0cmluZyA9IG51bGw7XG4gIHB1YmxpYyByZWFzb25zOiBzdHJpbmdbXSA9IFtdO1xuICBwdWJsaWMgZGF0YVBhdHRlcm5zOiBzdHJpbmdbXSA9IFtdO1xuICBwdWJsaWMgbGlzdDogc3RyaW5nW10gPSBbXTtcbiAgcHVibGljIGlkOiBudW1iZXI7XG5cbiAgY29uc3RydWN0b3IocHVibGljIGRlc3RpbmF0aW9uIDogc3RyaW5nKSB7XG4gIH1cblxuICAvKipcbiAgICogQHB1YmxpYyBTZXR0ZXIgZnVuY3Rpb24gdGhhdCBhdHRhY2hlcyBhIGJsb2NrIHJlYXNvbiB0byB0aGUgRW5zUmVxdWVzdCBcbiAgICogQHBhcmFtIHtzdHJpbmd9IHJlYXNvbiAtIHRoZSByZWFzb24gdGhlIHJlcXVlc3Qgd2FzIGJsb2NrZWRcbiAgICovXG4gIHB1YmxpYyBhZGRSZWFzb24ocmVhc29uOiBzdHJpbmcpIDogdm9pZCB7XG4gICAgICB0aGlzLnJlYXNvbnMucHVzaChyZWFzb24pO1xuICB9XG5cbiAgLyoqXG4gICAqIEBwdWJsaWMgQ2hlY2tzIGlmIHRoZSByZXF1ZXN0IGNvbnRhaW5zIGEgc3BlY2lmaWVkIGJsb2NrIHJlYXNvblxuICAgKiBAcGFyYW0ge3N0cmluZ30gcmVhc29uIC0gcmVhc29uIGJlaW5nIGNoZWNrZWRcbiAgICovXG4gIHB1YmxpYyBoYXNSZWFzb24ocmVhc29uOiBzdHJpbmcpIDogYm9vbGVhbiB7XG4gICAgICBpZiAodGhpcy5yZWFzb25zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICByZXR1cm4gbmV3IFJlZ0V4cCh0aGlzLnJlYXNvbnMuam9pbihcInxcIikpLnRlc3QocmVhc29uKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBAcHVibGljIEdldHRlciBmdW5jdGlvbiB0byByZXR1cm4gdGhlIG51bWJlciBvZiBibG9ja2VkIHJlYXNvbnMgYXR0YWNoZWQgdG8gdGhlIEVuc1JlcXVlc3RcbiAgICovXG4gIHB1YmxpYyBudW1SZWFzb25zKCkgOiBudW1iZXIge1xuICAgICAgcmV0dXJuIHRoaXMucmVhc29ucy5sZW5ndGg7XG4gIH1cbn1cbiIsImV4cG9ydCAqIGZyb20gJy4vdXJsUHJvY2Vzc29yJztcbmV4cG9ydCAqIGZyb20gJy4vdXRpbGl0aWVzJztcbmV4cG9ydCAqIGZyb20gJy4vZW5zUmVxdWVzdCc7XG5leHBvcnQgKiBmcm9tICcuL3VybFBvbnlGaWxsJzsiLCJpbXBvcnQgeyB1dGlsaXRpZXMgfSBmcm9tIFwiLi91dGlsaXRpZXNcIjtcbmltcG9ydCB7IFBhdGhGaWx0ZXJNYW5hZ2VyIH0gZnJvbSBcIi4vcGF0aEZpbHRlck1hbmFnZXJcIjtcbmltcG9ydCB7IEVuc1JlcXVlc3QgfSBmcm9tIFwiLi9lbnNSZXF1ZXN0XCI7XG5cbi8qKiBcbiAqIExpc3QgUHJvY2Vzc29yIC0gcHJvY2Vzc2VzIHJlcXVlc3RzIG9uIHRoZSB0YWdzIHRoYXQgYXJlIHNldCBpbiB0aGUgYWN0aXZlIGVudmlyb25tZW50XG4gKi9cbmV4cG9ydCBjbGFzcyBMaXN0UHJvY2Vzc29yIHtcblxuICAgIHByaXZhdGUgYWN0aXZlTGlzdCA6IGFueTtcbiAgICBwcml2YXRlIGxpc3RSZWdleCA9IG5ldyBSZWdFeHAoXCJhXlwiKTtcbiAgICBwcml2YXRlIHBhdGhGaWx0ZXJNYW5hZ2VyIDogUGF0aEZpbHRlck1hbmFnZXI7XG4gICAgcHJpdmF0ZSB1c2VzQWxsb3dsaXN0ID0gdHJ1ZTtcbiAgICBwcml2YXRlIHVzZXNQYXRoRmlsdGVyID0gZmFsc2U7XG4gICAgcHJpdmF0ZSBpc0luaXRpYWxpemVkID0gZmFsc2U7XG4gICAgcHJpdmF0ZSByZWFkb25seSBkZWZhdWx0Q2F0ZWdvcnkgPSBcImRlZmF1bHRcIjtcblxuICAgIC8qKiBcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0ge2FueX0gZW52aXJvbm1lbnQgLSB0aGUgYWN0aXZlIGVudmlyb25tZW50IGN1cnJlbnRseSBiZWluZyB1c2VkIGluIHRoZSBnYXRld2F5XG4gICAgICogQHBhcmFtIHthbnl9IGdhdGV3YXlEYXRhIC0gYSBkYXRhIG9iamVjdCByZWZlcnJpbmcgdG8gdGhlIHVzZXIncyBlbnNDbGllbnRDb25maWdcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzIC0gY29va2llIHZhbHVlcyBzZXQgaW4gdGhlIGFjdGl2ZSBlbnZpcm9ubWVudCBwYXNzZWQgaW4gYXMgYW4gb2JqZWN0XG4gICAgICogQHBhcmFtIHtBcnJheTxhbnk+fSBhbGxvd2VkVXJscyAtIGFsbG93ZWQgdXJscyBwYXNzZWQgaW4gZnJvbSB0YWcgYWNjZXNzIG1hbmFnZXJcbiAgICAgKiBAcGFyYW0ge0FycmF5PGFueT59IGJsb2NrZWRVcmxzIC0gYmxvY2tlZCB1cmxzIHBhc3NlZCBpbiBmcm9tIHRhZyBhY2Nlc3MgbWFuYWdlclxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGVudmlyb25tZW50OiBhbnksXG4gICAgICAgIHByaXZhdGUgZ2F0ZXdheURhdGE6IGFueSxcbiAgICAgICAgcHJpdmF0ZSBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXM6IG9iamVjdCxcbiAgICAgICAgcHJpdmF0ZSBhbGxvd2VkVXJsczogQXJyYXk8YW55PiA9IFtdLFxuICAgICAgICBwcml2YXRlIGJsb2NrZWRVcmxzOiBBcnJheTxhbnk+ID0gW11cbiAgICApIHtcbiAgICAgICAgdGhpcy5idWlsZExpc3RSZWdleChlbnZpcm9ubWVudCwgYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzLCBhbGxvd2VkVXJscywgYmxvY2tlZFVybHMpO1xuICAgICAgICAvLyB0aGlzLnVzZXNQYXRoRmlsdGVyIHdpbGwgZ2V0IHNldCBhZnRlciB3ZSBnbyB0aHJ1IHRoZSBsaXN0IGFuZCBjaGVjayBpZiBhbnkgb2YgdGhlIHRhZ3MgYXJlIHVzaW5nIGEgcGF0aCBmaWx0ZXJcbiAgICAgICAgaWYgKHRoaXMudXNlc1BhdGhGaWx0ZXIpIHtcbiAgICAgICAgICAgIHRoaXMucGF0aEZpbHRlck1hbmFnZXIgPSBuZXcgUGF0aEZpbHRlck1hbmFnZXIodGhpcy5nZXRMaXN0KCksIHRoaXMuc2hvdWxkQWRkVG9MaXN0UmVnZXgsIHRoaXMuZW52aXJvbm1lbnQsIHRoaXMuZ2F0ZXdheURhdGEsIHRoaXMuYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzLCB0aGlzLmFsbG93ZWRVcmxzLCB0aGlzLmJsb2NrZWRVcmxzKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmlzSW5pdGlhbGl6ZWQgPSB0cnVlO1xuICAgIH1cblxuICAgIC8qIFxuICAgICAgICBQVUJMSUMgTUVUSE9EU1xuICAgICovXG5cbiAgICAvKipcbiAgICAgKiBAcHVibGljIHByb2Nlc3NlcyBhIHJlcXVlc3QgYW5kIGRldGVybWluZXMgd2hldGhlciBvciBub3QgaXQgc2hvdWxkIGJlIGFsbG93ZWQgYmFzZWQgb24gdGhlIGFsbG93IG9yIGJsb2NrIGxpc3RcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gaG9zdE5hbWUgLSB0aGUgbmFtZSBvZiB0aGUgaG9zdCBpbiB0aGUgdXJsIGJlaW5nIHJlcXVlc3RlZFxuICAgICAqIEBwYXJhbSB7RW5zUmVxdWVzdH0gcmVxdWVzdCAtIHRoZSByZXF1ZXN0IHRoYXQgd2lsbCBiZSBwcm9jZXNzZWRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcGF0aE5hbWUgLSB0aGUgcGF0aCBvZiB0aGUgdXJsIGJlaW5nIHJlcXVlc3RlZFxuICAgICAqIEByZXR1cm5zIHtib29sZWFufSBpZiB0aGUgcmVxdWVzdCBpcyBhbGxvd2VkIG9yIGJsb2NrZWRcbiAgICAgKi9cbiAgICBwdWJsaWMgYWxsb3dSZXF1ZXN0ID0gKGhvc3ROYW1lOiBzdHJpbmcsIHJlcXVlc3Q6IEVuc1JlcXVlc3QsIHBhdGhOYW1lOiBzdHJpbmcpIDogYm9vbGVhbiA9PiB7XG5cbiAgICAgICAgY29uc3QgbGlzdFJlZ0V4ID0gdGhpcy5nZXRMaXN0UmVnZXgoKTtcbiAgICAgICAgbGV0IHRhcmdldExpc3RDaGVja1Bhc3NlcyA9IGZhbHNlO1xuICAgICAgICAvLyBibG9ja2xpc3RcbiAgICAgICAgaWYgKHRoaXMudXNlc0FsbG93bGlzdCA9PT0gZmFsc2UpIHtcblxuICAgICAgICAgICAgaWYgKGxpc3RSZWdFeC50ZXN0ICYmICFsaXN0UmVnRXgudGVzdChob3N0TmFtZSkpIHtcblxuICAgICAgICAgICAgICAgIHRhcmdldExpc3RDaGVja1Bhc3NlcyA9IHRydWU7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuICAgICAgICAvLyBhbGxvd2xpc3RcbiAgICAgICAgZWxzZSBpZiAobGlzdFJlZ0V4LnRlc3QgJiYgbGlzdFJlZ0V4LnRlc3QoaG9zdE5hbWUpKSB7XG4gICAgICAgICAgICB0YXJnZXRMaXN0Q2hlY2tQYXNzZXMgPSB0cnVlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMucGF0aEZpbHRlck1hbmFnZXIgIT0gbnVsbCkge1xuICAgICAgICAgICAgdGFyZ2V0TGlzdENoZWNrUGFzc2VzID0gdGhpcy5wYXRoRmlsdGVyTWFuYWdlci5hbGxvd1JlcXVlc3QoaG9zdE5hbWUsIHBhdGhOYW1lLCB0YXJnZXRMaXN0Q2hlY2tQYXNzZXMpO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGFyZ2V0TGlzdENoZWNrUGFzc2VzKSB7XG4gICAgICAgICAgICByZXF1ZXN0LmFkZFJlYXNvbih0aGlzLnVzZXNBbGxvd2xpc3QgPyBcIldoaXRlbGlzdFwiIDogXCJCbGFja2xpc3RcIik7XG4gICAgICAgICAgICBjb25zdCBtYXRjaGVkQ2F0ZWdvcmllcyA9IHRoaXMuZmluZE1hdGNoZWRDYXRlZ29yaWVzKGhvc3ROYW1lKTtcbiAgICAgICAgICAgIHJlcXVlc3QubGlzdCA9IG1hdGNoZWRDYXRlZ29yaWVzO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0YXJnZXRMaXN0Q2hlY2tQYXNzZXM7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyB1cGRhdGVzIHRoZSBhY3RpdmUgbGlzdCBiZWluZyBlbmZvcmNlZCBpbiB0aGUgbGlzdCBwcm9jZXNzb3JcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzIC0gYW4gb2JqZWN0IGNvbnRhaW5pbmcgdGhlIGNvb2tpZSB2YWx1ZXMgb2YgdGhlIGFjdGl2ZSBlbnZpcm9ubWVudFxuICAgICAqIEBwYXJhbSB7QXJyYXk8YW55Pn0gYWxsb3dlZFVybHMgLSBhbiBvYmplY3QgZnJvbSB0aGUgdGFnIG1hbmFnZXIgY29udGFpbmluZyB0aGUgYWxsb3dlZCBvciBibG9ja2VkIHRhZ3MgYmFzZWQgb24gYWxsb3cgb3IgYmxvY2tsaXN0IG1vZGVcbiAgICAgKiBAcGFyYW0ge0FycmF5PGFueT59IGJsb2NrZWRVcmxzIC0gYW4gb2JqZWN0IGZyb20gdGhlIHRhZyBtYW5hZ2VyIGNvbnRhaW5pbmcgdGhlIGFsbG93ZWQgb3IgYmxvY2tlZCB0YWdzIGJhc2VkIG9uIGFsbG93IG9yIGJsb2NrbGlzdCBtb2RlXG4gICAgICovXG4gICAgcHVibGljIHVwZGF0ZUxpc3RWYWx1ZXMgPSAoYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzOiBvYmplY3QsIGFsbG93ZWRVcmxzOiBBcnJheTxhbnk+LCBibG9ja2VkVXJsczogQXJyYXk8YW55PikgOiB2b2lkID0+IHtcbiAgICAgICAgdGhpcy5idWlsZExpc3RSZWdleCh0aGlzLmVudmlyb25tZW50LCBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXMsIGFsbG93ZWRVcmxzLCBibG9ja2VkVXJscyk7XG4gICAgICAgIGlmICh0aGlzLnVzZXNQYXRoRmlsdGVyICYmIHRoaXMucGF0aEZpbHRlck1hbmFnZXIgPT0gbnVsbCkge1xuICAgICAgICAgICAgdGhpcy5wYXRoRmlsdGVyTWFuYWdlciA9IG5ldyBQYXRoRmlsdGVyTWFuYWdlcih0aGlzLmdldExpc3QoKSwgdGhpcy5zaG91bGRBZGRUb0xpc3RSZWdleCwgdGhpcy5lbnZpcm9ubWVudCwgdGhpcy5nYXRld2F5RGF0YSwgdGhpcy5hY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXMsIHRoaXMuYWxsb3dlZFVybHMsIHRoaXMuYmxvY2tlZFVybHMpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLnBhdGhGaWx0ZXJNYW5hZ2VyICE9IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMucGF0aEZpbHRlck1hbmFnZXIudXBkYXRlKGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllcywgYWxsb3dlZFVybHMsIGJsb2NrZWRVcmxzKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgdXBkYXRlcyB0aGUgYWN0aXZlIGVudmlyb25tZW50IGluIHRoZSBsaXN0IHByb2Nlc3NvclxuICAgICAqIEBwYXJhbSB7YW55fSBuZXdFbnYgLSB0aGUgbmV3IGVudmlyb25tZW50IHdoaWNoIHdpbGwgYmVjb21lIGFjdGl2ZSBpbiB0aGUgbGlzdCBwcm9jZXNzb3JcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzIC0gYW4gb2JqZWN0IGNvbnRhaW5pbmcgdGhlIGNvb2tpZSB2YWx1ZXMgb2YgdGhlIGFjdGl2ZSBlbnZpcm9ubWVudFxuICAgICAqIEBwYXJhbSB7QXJyYXk8YW55Pn0gYWxsb3dlZFVybHMgLSBhbiBvYmplY3QgZnJvbSB0aGUgdGFnIG1hbmFnZXIgY29udGFpbmluZyB0aGUgYWxsb3dlZCBvciBibG9ja2VkIHRhZ3MgYmFzZWQgb24gYWxsb3cgb3IgYmxvY2tsaXN0IG1vZGVcbiAgICAgKiBAcGFyYW0ge0FycmF5PGFueT59IGJsb2NrZWRVcmxzIC0gYW4gb2JqZWN0IGZyb20gdGhlIHRhZyBtYW5hZ2VyIGNvbnRhaW5pbmcgdGhlIGFsbG93ZWQgb3IgYmxvY2tlZCB0YWdzIGJhc2VkIG9uIGFsbG93IG9yIGJsb2NrbGlzdCBtb2RlXG4gICAgICovXG4gICAgcHVibGljIHVwZGF0ZUVudmlyb25tZW50ID0gKG5ld0VudjogYW55LCBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXM6IG9iamVjdCwgYWxsb3dlZFVybHM6IEFycmF5PGFueT4sIGJsb2NrZWRVcmxzOiBBcnJheTxhbnk+KSA6IHZvaWQgPT4ge1xuICAgICAgICB0aGlzLmVudmlyb25tZW50ID0gbmV3RW52O1xuICAgICAgICB0aGlzLnVwZGF0ZUxpc3RWYWx1ZXMoYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzLCBhbGxvd2VkVXJscywgYmxvY2tlZFVybHMpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgcmV0dXJucyB0aGUgcmVndWxhciBleHByZXNzaW9uIHRoYXQgaXMgYmVpbmcgZW5mb3JjZWQgaW4gdGhlIGdhdGV3YXlcbiAgICAgKiBAcmV0dXJucyB7UmVnRXhwfSB0aGUgcmVndWxhciBleHByZXNzaW9uIHRoYXQgaXMgYmVpbmcgZW5mb3JjZWQgaW4gdGhlIGdhdGV3YXlcbiAgICAgKi9cbiAgICBwdWJsaWMgZ2V0TGlzdFJlZ2V4ID0gKCkgOiBSZWdFeHAgPT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5saXN0UmVnZXg7XG4gICAgfVxuICAgIFxuICAgIC8qIFxuICAgICAgICBQUklWQVRFIE1FVEhPRFNcbiAgICAqL1xuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgcmV0dXJucyB0aGUgYWN0aXZlIGFsbG93IG9yIGJsb2NrIGxpc3QgYmVpbmcgZW5mb3JjZWRcbiAgICAgKiBAcmV0dXJucyB7YW55fSB0aGUgYWN0aXZlIGFsbG93IG9yIGJsb2NrIGxpc3QgYmVpbmcgZW5mb3JjZWRcbiAgICAgKi9cbiAgICBwcml2YXRlIGdldExpc3QgPSAoKSA6IGFueSA9PiB7XG4gICAgICAgIHJldHVybiB0aGlzLmFjdGl2ZUxpc3Q7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgYnVpbGRzIHVwIHRoZSByZWd1bGFyIGV4cHJlc3Npb24gYmFzZWQgb24gdGhlIGVudmlyb25tZW50IHRoYXQncyBwYXNzZWQgaW5cbiAgICAgKiBAcGFyYW0ge2FueX0gZW52aXJvbm1lbnQgLSBlbnZpcm9ubWVudCB3aGljaCBjb250YWlucyB0aGUgYWxsb3cgLyBibG9jayBsaXN0XG4gICAgICogQHBhcmFtIHtvYmplY3R9IGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllcyAtIGFuIG9iamVjdCBjb250YWluaW5nIHRoZSBjb29raWUgdmFsdWVzIG9mIHRoZSBhY3RpdmUgZW52aXJvbm1lbnRcbiAgICAgKiBAcGFyYW0ge0FycmF5PGFueT59IGFsbG93ZWRVcmxzIC0gYW4gb2JqZWN0IGZyb20gdGhlIHRhZyBtYW5hZ2VyIGNvbnRhaW5pbmcgdGhlIGFsbG93ZWQgb3IgYmxvY2tlZCB0YWdzIGJhc2VkIG9uIGFsbG93IG9yIGJsb2NrbGlzdCBtb2RlXG4gICAgICogQHBhcmFtIHtBcnJheTxhbnk+fSBibG9ja2VkVXJscyAtIGFuIG9iamVjdCBmcm9tIHRoZSB0YWcgbWFuYWdlciBjb250YWluaW5nIHRoZSBhbGxvd2VkIG9yIGJsb2NrZWQgdGFncyBiYXNlZCBvbiBhbGxvdyBvciBibG9ja2xpc3QgbW9kZVxuICAgICAqL1xuICAgIHByaXZhdGUgYnVpbGRMaXN0UmVnZXggPSAoZW52aXJvbm1lbnQ6IGFueSwgYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzOiBvYmplY3QsIGFsbG93ZWRVcmxzOiBBcnJheTxhbnk+LCBibG9ja2VkVXJsczogQXJyYXk8YW55PikgOiB2b2lkID0+IHtcblxuICAgICAgICB0aGlzLnNldFVzZXNBbGxvd2xpc3QoZW52aXJvbm1lbnQpO1xuICAgICAgICB0aGlzLnNldEFjdGl2ZUxpc3QoZW52aXJvbm1lbnQpO1xuXG4gICAgICAgIGNvbnN0IGxpc3QgPSB0aGlzLmdldExpc3QoKTtcbiAgICAgICAgbGV0IHJlZyA9IFtdO1xuXG4gICAgICAgIGZvciAoY29uc3QgY2F0ZWdvcnkgaW4gbGlzdCkge1xuICAgICAgICAgICAgY29uc3QgY29va2llVmFsdWUgPSBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXNbY2F0ZWdvcnldO1xuICAgICAgICAgICAgaWYgKHRoaXMuc2hvdWxkQWRkVG9MaXN0UmVnZXgoY2F0ZWdvcnksIGNvb2tpZVZhbHVlKSkge1xuICAgICAgICAgICAgICAgIHJlZyA9IHJlZy5jb25jYXQodGhpcy5nZXRPYmplY3RWYWxzKGxpc3RbY2F0ZWdvcnldLCBcInRhZ1wiKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy51c2VzQWxsb3dsaXN0KSB7XG4gICAgICAgICAgICBpZiAoYWxsb3dlZFVybHMgJiYgYWxsb3dlZFVybHMubGVuZ3RoID4gMCl7XG4gICAgICAgICAgICAgICAgcmVnID0gcmVnLmNvbmNhdCh0aGlzLmdldE9iamVjdFZhbHMoYWxsb3dlZFVybHMsXCJ0YWdcIikpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGJsb2NrZWRVcmxzICYmIGJsb2NrZWRVcmxzLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgICAgIHJlZyA9IHJlZy5maWx0ZXIoKGVsZW1lbnQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0T2JqZWN0VmFscyhibG9ja2VkVXJscyxcInRhZ1wiKS5pbmRleE9mKGVsZW1lbnQpID09PSAtMTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAvLyBibG9jayBsaXN0XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgaWYgKGJsb2NrZWRVcmxzICYmIGJsb2NrZWRVcmxzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICByZWcgPSByZWcuY29uY2F0KHRoaXMuZ2V0T2JqZWN0VmFscyhibG9ja2VkVXJscyxcInRhZ1wiKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoYWxsb3dlZFVybHMgJiYgYWxsb3dlZFVybHMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIHJlZyA9IHJlZy5maWx0ZXIoKGVsZW1lbnQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0T2JqZWN0VmFscyhhbGxvd2VkVXJscyxcInRhZ1wiKS5pbmRleE9mKGVsZW1lbnQpID09PSAtMTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IGZsYWdzID0gKHRoaXMudXNlc0FsbG93bGlzdCkgPyBcImlcIiA6IHVuZGVmaW5lZDtcblxuICAgICAgICB0aGlzLmxpc3RSZWdleCA9IHV0aWxpdGllcy5hcnJheVRvUmVnZXgocmVnLCBmbGFncyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgYSBmdW5jdGlvbiB0aGF0IGRldGVybWluZXMgaWYgYSBjYXRlZ29yeSBuZWVkcyB0byBiZSBhZGRlZCB0byB0aGUgYWN0aXZlIGxpc3RcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gY2F0ZWdvcnkgLSB0aGUgbmFtZSBvZiB0aGUgdGFnIGNhdGVnb3J5IHRvIGJlIGNoZWNrZWRcbiAgICAgKiBAcGFyYW0ge3N0cmluZyB8IHVuZGVmaW5lZH0gY29va2llVmFsdWUgLSB2YWx1ZSBvZiB0aGUgY29va2llIHRoYXQncyBwYXNzZWQgaW5cbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gaWYgdGhlIGNhdGVnb3J5IHNob3VsZCBiZSBhZGRlZCB0byB0aGUgbGlzdCByZWdleCBvciBub3RcbiAgICAgKi9cbiAgICBwcml2YXRlIHNob3VsZEFkZFRvTGlzdFJlZ2V4ID0gKGNhdGVnb3J5OiBzdHJpbmcsIGNvb2tpZVZhbHVlOiBzdHJpbmcgfCB1bmRlZmluZWQpIDogYm9vbGVhbiA9PiB7XG4gICAgICAgIGxldCBzaG91bGRBZGRUb0xpc3RSZWdleCA9IGZhbHNlO1xuXG4gICAgICAgIGlmIChjYXRlZ29yeSA9PT0gdGhpcy5kZWZhdWx0Q2F0ZWdvcnkpIHtcbiAgICAgICAgICAgIC8vaWYgb3VyIGRlZmF1bHQgY2F0ZWdvcnkgYXV0b21hdGljYWxseSBpbmNsdWRlcyB0aGVzZSBpdGVtc1xuICAgICAgICAgICAgc2hvdWxkQWRkVG9MaXN0UmVnZXggPSB0cnVlO1xuICAgICAgICAgICAgcmV0dXJuIHNob3VsZEFkZFRvTGlzdFJlZ2V4O1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gdXNlcyBhbGxvd2xpc3RcbiAgICAgICAgaWYgKHRoaXMudXNlc0FsbG93bGlzdCkge1xuICAgICAgICAgICAgLy8gY29va2llIGlzIHNldCB0byAxIC0tIGFkZCB0byBsaXN0XG4gICAgICAgICAgICBpZiAoY29va2llVmFsdWUgPT09IFwiMVwiKSB7XG4gICAgICAgICAgICAgICAgc2hvdWxkQWRkVG9MaXN0UmVnZXggPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gY29va2llIGlzIG5vdCAwLCBpZiBjb29raWUgaXMgMCB3ZSBhc3N1bWUgdGhlIHVzZXIgaGFzIHNldCBpdCBhbmQgZG9uJ3QgbG9vayB0byB0aGUgb3B0IHZhbCwgb3RoZXJ3aXNlIGNoZWNrIHRoZSBlbnZpcm9ubWVudCBkZWZhdWx0IG9wdCB2YWxcbiAgICAgICAgICAgIGVsc2UgaWYgKCghY29va2llVmFsdWUgfHwgY29va2llVmFsdWUgIT09IFwiMFwiKSAmJiB0aGlzLmlzRW52aXJvbm1lbnRDYXRlZ29yeUVuYWJsZWQoY2F0ZWdvcnksIDEpKSB7XG4gICAgICAgICAgICAgICAgc2hvdWxkQWRkVG9MaXN0UmVnZXggPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC8vIHVzZXMgYmxvY2tsaXN0XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgaWYgKGNvb2tpZVZhbHVlID09PSBcIjBcIikge1xuICAgICAgICAgICAgICAgIHNob3VsZEFkZFRvTGlzdFJlZ2V4ID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKCghY29va2llVmFsdWUgfHwgY29va2llVmFsdWUgIT09IFwiMVwiKSAmJiB0aGlzLmlzRW52aXJvbm1lbnRDYXRlZ29yeUVuYWJsZWQoY2F0ZWdvcnksIDApKSB7XG4gICAgICAgICAgICAgICAgc2hvdWxkQWRkVG9MaXN0UmVnZXggPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHNob3VsZEFkZFRvTGlzdFJlZ2V4O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIGhlbHBlciBmdW5jdGlvbiB0aGF0IGRldGVybWluZXMgaWYgYW4gZW52aXJvbm1lbnQgY2F0ZWdvcnkgaXMgZW5hYmxlZCBvciBkaXNhYmxlZCBcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gY2F0ZWdvcnkgLSB0aGUgbmFtZSBvZiB0aGUgY2F0ZWdvcnkgYmVpbmcgY2hlY2tlZFxuICAgICAqIEBwYXJhbSB7MSB8IDB9IHZhbCAtIG51bWVyaWMgdmFsdWVzIHRvIGNoZWNrIGlmIHRoZSBjYXRlZ29yeSBpcyBlbmFibGVkIG9yIGRpc2FibGVkLCByZXNwZWN0aXZlbHlcbiAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn0gaWYgdGhlIHNwZWNpZmllZCBjYXRlZ29yeSBpcyBlbmFibGVkIGluIHRoZSBhY3RpdmUgZW52aXJvbm1lbnRcbiAgICAgKi9cbiAgICBwcml2YXRlIGlzRW52aXJvbm1lbnRDYXRlZ29yeUVuYWJsZWQgPSAoY2F0ZWdvcnk6IHN0cmluZywgdmFsOiAxIHwgMCkgOiBib29sZWFuID0+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZW52aXJvbm1lbnQgJiYgdGhpcy5lbnZpcm9ubWVudC5vcHRWYWxzW2NhdGVnb3J5XSA9PT0gdmFsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIGhlbHBlciBmdW5jdGlvbiB0aGF0IHJldHVybnMgYXJyYXkgY29udGFpbmluZyB2YWx1ZXMgZm9yIGEgc3BlY2lmaWVkIGtleSBpbiBhbiBhcnJheSBvZiBvYmplY3RzXG4gICAgICogQHBhcmFtIHtBcnJheTxSZWNvcmQ8c3RyaW5nLCB1bmtub3duPj59IGFycmF5T2ZPYmplY3RzIC0gdGhlIGFycmF5IG9mIG9iamVjdHMgYmVpbmcgY2hlY2tlZFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBrZXkgLSB0aGUgcHJvcGVydHkgd2UncmUgbG9va2luZyBmb3IgaW4gdGhlIGFycmF5IG9mIG9iamVjdHNcbiAgICAgKiBAcmV0dXJucyB7QXJyYXk8UmVjb3JkPHN0cmluZywgdW5rbm93bj4+fSBhbiBhcnJheSB3aGljaCBjb250YWlucyBhbiBvYmplY3Qgb2YgdmFsdWVzIHRoYXQgY29ycmVzcG9uZCB0byB0aGUga2V5IGluc2lkZSB0aGUgYXJyYXlPZk9iamVjdHNcbiAgICAgKi9cbiAgICBwcml2YXRlIGdldE9iamVjdFZhbHMgPSAoYXJyYXlPZk9iamVjdHM6IEFycmF5PFJlY29yZDxzdHJpbmcsIHVua25vd24+Piwga2V5OiBzdHJpbmcpIDogQXJyYXk8UmVjb3JkPHN0cmluZywgdW5rbm93bj4+ID0+IHtcbiAgICAgICAgY29uc3QgYXJyYXlPZk9iamVjdFZhbHMgPSBbXTtcblxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFycmF5T2ZPYmplY3RzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBjb25zdCB2YWwgPSBhcnJheU9mT2JqZWN0c1tpXVtrZXldO1xuXG4gICAgICAgICAgICBpZiAodmFsICE9PSBcIlwiICYmIHZhbCAhPT0gdW5kZWZpbmVkICYmIHZhbCAhPSBudWxsKSB7XG4gICAgICAgICAgICAgICAgYXJyYXlPZk9iamVjdFZhbHMucHVzaCh2YWwpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvL2NoZWNrIGZvciBwYXRoIGZpbHRlcnNcbiAgICAgICAgICAgIGlmIChhcnJheU9mT2JqZWN0c1tcImZpbHRlclwiXSkge1xuICAgICAgICAgICAgICAgIHRoaXMudXNlc1BhdGhGaWx0ZXIgPSB0cnVlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gYXJyYXlPZk9iamVjdFZhbHM7XG4gICAgfVxuICAgIFxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIHNldHMgdGhlIHVzZXNBbGxvd2xpc3QgcHJvcGVydHkgaW5zaWRlIHRoZSBsaXN0IHByb2Nlc3NvciBjbGFzc1xuICAgICAqIEBwYXJhbSB7YW55fSBlbnZpcm9ubWVudCAtIHRoZSBhY3RpdmUgZW52aXJvbm1lbnRcbiAgICAgKi9cbiAgICBwcml2YXRlIHNldFVzZXNBbGxvd2xpc3QgPSAoZW52aXJvbm1lbnQ6IGFueSkgOiB2b2lkID0+IHtcbiAgICAgICAgdGhpcy51c2VzQWxsb3dsaXN0ID0gdXRpbGl0aWVzLmlzV2hpdGVsaXN0KHRoaXMuZ2F0ZXdheURhdGEsIGVudmlyb25tZW50KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZSBzZXRzIHRoZSBhY3RpdmUgbGlzdCBpbnNpZGUgdGhlIGxpc3QgcHJvY2Vzc29yIGNsYXNzXG4gICAgICogQHBhcmFtIHthbnl9IGVudmlyb25tZW50IC0gdGhlIGFjdGl2ZSBlbnZpcm9ubWVudFxuICAgICAqL1xuICAgIHByaXZhdGUgc2V0QWN0aXZlTGlzdCA9IChlbnZpcm9ubWVudDogYW55KSA6IHZvaWQgPT4ge1xuXG4gICAgICAgIGlmICh0aGlzLnVzZXNBbGxvd2xpc3QpIHtcbiAgICAgICAgICAgIGlmIChlbnZpcm9ubWVudCAmJiBlbnZpcm9ubWVudC5oYXNPd25Qcm9wZXJ0eShcIndoaXRlbGlzdFwiKSkge1xuICAgICAgICAgICAgICAgIHRoaXMuYWN0aXZlTGlzdCA9IGVudmlyb25tZW50LndoaXRlbGlzdDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5hY3RpdmVMaXN0ID0gdGhpcy5nYXRld2F5RGF0YS53aGl0ZWxpc3QgfHwge307XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoZW52aXJvbm1lbnQgJiYgZW52aXJvbm1lbnQuaGFzT3duUHJvcGVydHkoXCJibGFja2xpc3RcIikpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZUxpc3QgPSBlbnZpcm9ubWVudC5ibGFja2xpc3Q7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuYWN0aXZlTGlzdCA9IHRoaXMuZ2F0ZXdheURhdGEuYmxhY2tsaXN0IHx8IHt9O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLy8gY3JlYXRlIGFuIGVtcHR5IGRlZmF1bHQgY2F0ZWdvcnkgaWYgaXQgZG9lcyBub3QgZXhpc3RcbiAgICAgICAgaWYgKCF0aGlzLmFjdGl2ZUxpc3QuaGFzT3duUHJvcGVydHkodGhpcy5kZWZhdWx0Q2F0ZWdvcnkpICYmIHRoaXMudXNlc0FsbG93bGlzdCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgdGhpcy5hY3RpdmVMaXN0W3RoaXMuZGVmYXVsdENhdGVnb3J5XSA9IFtdO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMudXNlc0FsbG93bGlzdCAmJiAhdGhpcy5pc0luaXRpYWxpemVkKSB7XG4gICAgICAgICAgICBjb25zdCBuZXh1c0RvbWFpbiA9IHRoaXMuZ2F0ZXdheURhdGEuaW5mby5uZXh1cyB8fCBcIm5leHVzLmVuc2lnaHRlbi5jb21cIjtcbiAgICAgICAgICAgIHRoaXMuYWN0aXZlTGlzdFt0aGlzLmRlZmF1bHRDYXRlZ29yeV0ucHVzaCh7IHRhZzogbG9jYXRpb24uaG9zdG5hbWUgfSk7XG4gICAgICAgICAgICB0aGlzLmFjdGl2ZUxpc3RbdGhpcy5kZWZhdWx0Q2F0ZWdvcnldLnB1c2goeyB0YWc6IG5leHVzRG9tYWluIH0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgZnVuY3Rpb24gdGhhdCBmaW5kcyB0aGUgY2F0ZWdvcmllcyBhIGJsb2NrZWQgcmVxdWVzdCBiZWxvbmdzIHRvIHNvIGl0IGNhbiBiZSBhdHRhY2hlZCB0byB0aGUgZW5zUmVxdWVzdFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBob3N0TmFtZSAtIGhvc3QgbmFtZSBvZiBibG9ja2VkIHJlcXVlc3QgdXJsXG4gICAgICogQHJldHVybnMge0FycmF5PHN0cmluZz59IHRoZSBjYXRlZ29yaWVzIHRoYXQgd2VyZSBtYXRjaGVkXG4gICAgICovXG4gICAgcHJpdmF0ZSBmaW5kTWF0Y2hlZENhdGVnb3JpZXMgPSAoaG9zdE5hbWU6IHN0cmluZykgOiBBcnJheTxzdHJpbmc+ID0+IHtcbiAgICAgICAgY29uc3QgbWF0Y2hlZENhdGVnb3JpZXMgPSBbXTtcbiAgICAgICAgY29uc3QgY2F0ZWdvcnlMaXN0ID0gdGhpcy5nZXRMaXN0KCk7XG5cbiAgICAgICAgZm9yIChjb25zdCBjYXRlZ29yeSBpbiBjYXRlZ29yeUxpc3QpIHtcbiAgICAgICAgICAgIC8vIGNoZWNrIHJlcSBhZ2FpbnN0IGV2ZXJ5IHRhZyBpbiBlYWNoIGNhdGVnb3J5XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNhdGVnb3J5TGlzdFtjYXRlZ29yeV0ubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICBjb25zdCB0YWcgPSBjYXRlZ29yeUxpc3RbY2F0ZWdvcnldW2ldLnRhZztcbiAgICAgICAgICAgICAgICAvLyBmaW5kIGNhdGVnb3JpZXMgd2hlcmUgdGhlIGJsb2NrZWQgdGFnIGV4aXN0c1xuICAgICAgICAgICAgICAgIGlmIChob3N0TmFtZS5tYXRjaCh0YWcpKSB7IFxuICAgICAgICAgICAgICAgICAgICBtYXRjaGVkQ2F0ZWdvcmllcy5wdXNoKGNhdGVnb3J5KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAobWF0Y2hlZENhdGVnb3JpZXMubGVuZ3RoID09PSAwKSB7IC8vIGlmIHJlcSBpc24ndCBibG9ja2VkIGZyb20gb3B0LWluL29wdC1vdXRcbiAgICAgICAgICAgIG1hdGNoZWRDYXRlZ29yaWVzLnB1c2godGhpcy51c2VzQWxsb3dsaXN0ID8gXCJXaGl0ZWxpc3RcIiA6IFwiQmxhY2tsaXN0XCIpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtYXRjaGVkQ2F0ZWdvcmllcztcbiAgICB9XG59XG4iLCJpbXBvcnQgeyB1dGlsaXRpZXMgfSBmcm9tIFwiLi91dGlsaXRpZXNcIjtcblxuLyoqXG4gKiBDcmVhdGUgYSBwYXRoIGZpbHRlciBtYW5hZ2VyIG9iamVjdCB0byBkZXRlcm1pbmUsIHdoZW4gcGF0aCBmaWx0ZXJzIGFyZSBkZWZpbmVkLCBpZiBhIHJlcXVlc3Qgc2hvdWxkIGJlXG4gKiBhbGxvd2VkIG9yIHJlamVjdGVkLlxuICovXG5leHBvcnQgY2xhc3MgUGF0aEZpbHRlck1hbmFnZXIge1xuICAgIHByaXZhdGUgdGFnc1dpdGhGaWx0ZXJzUmVnRXg6IFJlZ0V4cDtcbiAgICBwcml2YXRlIHRhZ3NXaXRoQWxsb3dQYXRoczogQXJyYXk8YW55PjtcbiAgICBwcml2YXRlIHRhZ3NXaXRoQmxvY2tQYXRoczogQXJyYXk8YW55PjtcblxuICAgIC8qKlxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBhY3RpdmVMaXN0IC0gVGhlIGFjdGl2ZSBsaXN0IG9mIHRhZyBjYXRlZ29yaWVzXG4gICAgICogQHBhcmFtIHtGdW5jdGlvbn0gc2hvdWxkQWRkQ2F0ZWdvcnlUb0xpc3QgLSBhIGZ1bmN0aW9uIHRoYXQsIGdpdmVuIGEgY2F0ZWdvcnkgbmFtZSwgaW5kaWNhdGVzIHdoZXRoZXIgaXQgc2hvdWxkIGJlIGluY2x1ZGVkIGluIHRoZSBsaXN0IG9mIHBhdGggZmlsdGVyaW5nIGNhdGVnb3JpZXMgY29uc2lkZXJlZCBoZXJlXG4gICAgICogQHBhcmFtIHthbnl9IGVudmlyb25tZW50IC0gZW52aXJvbm1lbnQgb2JqZWN0IHJlZmVyZW5jZVxuICAgICAqIEBwYXJhbSB7YW55fSBnYXRld2F5RGF0YSAtIGdhdGV3YXkgZGF0YSwgaW5jbHVkaW5nIGZlYXR1cmUgdG9nZ2xlc1xuICAgICAqIEBwYXJhbSB7QXJyYXk8YW55Pn0gYWxsb3dlZFRhZ3NJbkxvY2FsU3RvcmFnZSAtIGFuIGFycmF5IGNvbnRhaW5pbmcgYWxsIHRoZSB0YWdzIHRoYXQgYXJlIG1hcmtlZCBhbGxvd2VkIGluIGxvY2FsIHN0b3JhZ2VcbiAgICAgKiBAcGFyYW0ge0FycmF5PGFueT59IGJsb2NrZWRUYWdzSW5Mb2NhbFN0b3JhZ2UgLSBhbiBhcnJheSBjb250YWluaW5nIGFsbCB0aGUgdGFncyB0aGF0IGFyZSBtYXJrZWQgYmxvY2tlZCBpbiBsb2NhbCBzdG9yYWdlXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgYWN0aXZlTGlzdDogc3RyaW5nLFxuICAgICAgICBwcml2YXRlIHNob3VsZEFkZENhdGVnb3J5VG9MaXN0OiBGdW5jdGlvbixcbiAgICAgICAgcHJpdmF0ZSBlbnZpcm9ubWVudDogYW55LFxuICAgICAgICBwcml2YXRlIGdhdGV3YXlEYXRhOiBhbnksXG4gICAgICAgIHByaXZhdGUgYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzOiBhbnksXG4gICAgICAgIHByaXZhdGUgYWxsb3dlZFRhZ3NJbkxvY2FsU3RvcmFnZTogQXJyYXk8YW55PixcbiAgICAgICAgcHJpdmF0ZSBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlOiBBcnJheTxhbnk+XG4gICAgKSB7XG4gICAgICAgIHRoaXMuc2V0R2xvYmFscyhcbiAgICAgICAgICAgIGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllcyxcbiAgICAgICAgICAgIGFsbG93ZWRUYWdzSW5Mb2NhbFN0b3JhZ2UsXG4gICAgICAgICAgICBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyBVcGRhdGUgdGhlIGludGVybmFsIHJlZ2V4IHdoaWNoIGNhcHR1cmVzIHRoZSBhbGxvd2VkL2Jsb2NrZWQgdGFnc1xuICAgICAqIEBwYXJhbSB7YW55fSBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXMgLSBjb29raWUgdmFsdWVzIHNldCBmb3IgZWFjaCBjYXRlZ29yeSBpbiB0aGUgYWN0aXZlIGVudmlyb25tZW50IHBhc3NlZCBpbiBhcyBhbiBvYmplY3RcbiAgICAgKiBAcGFyYW0ge0FycmF5PGFueT59IGFsbG93ZWRUYWdzSW5Mb2NhbFN0b3JhZ2UgLSBhbiBhcnJheSBjb250YWluaW5nIGFsbCB0aGUgdGFncyB0aGF0IGFyZSBtYXJrZWQgYWxsb3dlZCBpbiBsb2NhbCBzdG9yYWdlXG4gICAgICogQHBhcmFtIHtBcnJheTxhbnk+fSBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlIC0gYW4gYXJyYXkgY29udGFpbmluZyBhbGwgdGhlIHRhZ3MgdGhhdCBhcmUgbWFya2VkIGJsb2NrZWQgaW4gbG9jYWwgc3RvcmFnZVxuICAgICAqL1xuICAgIHB1YmxpYyB1cGRhdGUgPSAoXG4gICAgICAgIGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllczogYW55LFxuICAgICAgICBhbGxvd2VkVGFnc0luTG9jYWxTdG9yYWdlOiBBcnJheTxhbnk+LFxuICAgICAgICBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlOiBBcnJheTxhbnk+XG4gICAgKTogdm9pZCA9PiB7XG4gICAgICAgIHRoaXMuc2V0R2xvYmFscyhcbiAgICAgICAgICAgIGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllcyxcbiAgICAgICAgICAgIGFsbG93ZWRUYWdzSW5Mb2NhbFN0b3JhZ2UsXG4gICAgICAgICAgICBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlXG4gICAgICAgICk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgRGV0ZXJtaW5lIHdoZXRoZXIgdG8gYWxsb3cgYSByZXF1ZXN0IHRvIGEgZG9tYWluL3BhdGguXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGRvbWFpbiAtIGRvbWFpbiB0aGUgcmVxdWVzdCBpcyBiZWluZyBtYWRlIHRvXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHBhdGggLSBwYXRoIHRoZSByZXF1ZXN0IGlzIGJlaW5nIG1hZGUgZm9yXG4gICAgICogQHBhcmFtIHtib29sZWFufSB0YXJnZXRMaXN0Q2hlY2tQYXNzZXMgLSB3aGV0aGVyIHRoZSB0YXJnZXRMaXN0IGNoZWNrIHBhc3NlcyBvciBub3RcbiAgICAgKiBAcmV0dXJuIHtib29sZWFufSAtIHRydWUgPSBhbGxvdyB0aGUgcmVxdWVzdDsgZmFsc2UgPSByZWplY3QgaXRcbiAgICAgKi9cbiAgICBwdWJsaWMgYWxsb3dSZXF1ZXN0ID0gKFxuICAgICAgICBkb21haW46IHN0cmluZyxcbiAgICAgICAgcGF0aDogc3RyaW5nLFxuICAgICAgICB0YXJnZXRMaXN0Q2hlY2tQYXNzZXM6IGJvb2xlYW5cbiAgICApOiBib29sZWFuID0+IHtcbiAgICAgICAgbGV0IGFsbG93UmVxdWVzdCA9IHRhcmdldExpc3RDaGVja1Bhc3NlcztcblxuICAgICAgICBpZiAocGF0aCAhPT0gXCJcIiAmJiB0aGlzLnRhZ3NXaXRoRmlsdGVyc1JlZ0V4LnRlc3QoZG9tYWluKSkge1xuICAgICAgICAgICAgZm9yIChcbiAgICAgICAgICAgICAgICBsZXQgdGFnc1dpdGhBbGxvd1BhdGhzQ291bnQgPSAwO1xuICAgICAgICAgICAgICAgIHRhZ3NXaXRoQWxsb3dQYXRoc0NvdW50IDwgdGhpcy50YWdzV2l0aEFsbG93UGF0aHMubGVuZ3RoO1xuICAgICAgICAgICAgICAgIHRhZ3NXaXRoQWxsb3dQYXRoc0NvdW50KytcbiAgICAgICAgICAgICkge1xuICAgICAgICAgICAgICAgIGNvbnN0IGFsbG93UGF0aFJlZ0V4ID0gbmV3IFJlZ0V4cChcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50YWdzV2l0aEFsbG93UGF0aHNbdGFnc1dpdGhBbGxvd1BhdGhzQ291bnRdW1widGFnXCJdXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICBpZiAoYWxsb3dQYXRoUmVnRXgudGVzdChkb21haW4pKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBhbGxvd1JlZ0FycmF5OiBhbnkgPSBbXTtcblxuICAgICAgICAgICAgICAgICAgICBhbGxvd1JlZ0FycmF5ID0gYWxsb3dSZWdBcnJheS5jb25jYXQoXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhZ3NXaXRoQWxsb3dQYXRoc1t0YWdzV2l0aEFsbG93UGF0aHNDb3VudF1bXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwYXRoc1wiXG4gICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGFsbG93UmVnID0gdXRpbGl0aWVzLmFycmF5VG9SZWdleChhbGxvd1JlZ0FycmF5KTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGFsbG93UmVnLnRlc3QocGF0aCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFsbG93UmVxdWVzdCA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gYWxsb3dSZXF1ZXN0O1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgYWxsb3dSZXF1ZXN0ID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZvciAoXG4gICAgICAgICAgICAgICAgbGV0IHRhZ3NXaXRoQmxvY2tQYXRoc0NvdW50ID0gMDtcbiAgICAgICAgICAgICAgICB0YWdzV2l0aEJsb2NrUGF0aHNDb3VudCA8IHRoaXMudGFnc1dpdGhCbG9ja1BhdGhzLmxlbmd0aDtcbiAgICAgICAgICAgICAgICB0YWdzV2l0aEJsb2NrUGF0aHNDb3VudCsrXG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICBjb25zdCBkb21haW5SZWdFeCA9IG5ldyBSZWdFeHAoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudGFnc1dpdGhCbG9ja1BhdGhzW3RhZ3NXaXRoQmxvY2tQYXRoc0NvdW50XVtcInRhZ1wiXVxuICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgICAgICBpZiAoZG9tYWluUmVnRXgudGVzdChkb21haW4pKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBibG9ja1JlZ0FycmF5OiBhbnkgPSBbXTtcblxuICAgICAgICAgICAgICAgICAgICBibG9ja1JlZ0FycmF5ID0gYmxvY2tSZWdBcnJheS5jb25jYXQoXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRhZ3NXaXRoQmxvY2tQYXRoc1t0YWdzV2l0aEJsb2NrUGF0aHNDb3VudF1bXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgXCJwYXRoc1wiXG4gICAgICAgICAgICAgICAgICAgICAgICBdXG4gICAgICAgICAgICAgICAgICAgICk7XG5cbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYmxvY2tSZWcgPSB1dGlsaXRpZXMuYXJyYXlUb1JlZ2V4KGJsb2NrUmVnQXJyYXkpO1xuXG4gICAgICAgICAgICAgICAgICAgIGlmIChibG9ja1JlZy50ZXN0KHBhdGgpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxvd1JlcXVlc3QgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhbGxvd1JlcXVlc3Q7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxvd1JlcXVlc3QgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBhbGxvd1JlcXVlc3Q7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIEJ1aWxkIGxpc3RzIG9mIHBhdGggZmlsdGVyc1xuICAgICAqIEBwYXJhbSB7YW55fSBhY3RpdmVMaXN0IC0gbGlzdCBvZiBjYXRlZ29yaWVzLCBlYWNoIHdpdGggdGFnIG9iamVjdHMgLSB0aGUgbGlzdCB0aGF0IHdpbGwgYmUgZXZhbHVhdGVkIGZvciBwYXRoIGZpbHRlcnNcbiAgICAgKiBAcGFyYW0ge2FueX0gYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzIC0gY29va2llIHZhbHVlcyBzZXQgZm9yIGVhY2ggY2F0ZWdvcnkgaW4gdGhlIGFjdGl2ZSBlbnZpcm9ubWVudCBwYXNzZWQgaW4gYXMgYW4gb2JqZWN0XG4gICAgICogQHBhcmFtIHtBcnJheTxhbnk+fSBhbGxvd2VkVGFnc0luTG9jYWxTdG9yYWdlIC0gYW4gYXJyYXkgY29udGFpbmluZyBhbGwgdGhlIHRhZ3MgdGhhdCBhcmUgbWFya2VkIGFsbG93ZWQgaW4gbG9jYWwgc3RvcmFnZVxuICAgICAqIEBwYXJhbSB7QXJyYXk8YW55Pn0gYmxvY2tlZFRhZ3NJbkxvY2FsU3RvcmFnZSAtIGFuIGFycmF5IGNvbnRhaW5pbmcgYWxsIHRoZSB0YWdzIHRoYXQgYXJlIG1hcmtlZCBibG9ja2VkIGluIGxvY2FsIHN0b3JhZ2VcbiAgICAgKiBAcmV0dXJuIHsqfSBBbiBvYmplY3QgY29udGFpbmluZyB0aGUgdXBkYXRlZCBSZWdFeCBvZiBhbGwgdGFncyB3aXRoIGZpbHRlcnMsIGFuZCB1cGRhdGVkIGxpc3RzIG9mIHRhZ3Mgd2l0aCBhbGxvdy9ibG9jayBwYXRoIGZpbHRlcnM6XG4gICAgICovXG4gICAgcHJpdmF0ZSBidWlsZFBhdGhGaWx0ZXJMaXN0ID0gKFxuICAgICAgICBhY3RpdmVMaXN0OiBhbnksXG4gICAgICAgIGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllczogYW55LFxuICAgICAgICBhbGxvd2VkVGFnc0luTG9jYWxTdG9yYWdlOiBBcnJheTxhbnk+LFxuICAgICAgICBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlOiBBcnJheTxhbnk+XG4gICAgKTogYW55ID0+IHtcbiAgICAgICAgbGV0IHRhZ3NXaXRoRmlsdGVycyA9IFtdOyAvLyBBIGxpc3Qgb2YgYWxsIHRhZyBvYmplY3RzIHdpdGggcGF0aCBmaWx0ZXJzIGRlZmluZWRcbiAgICAgICAgbGV0IHRhZ3NXaXRoQWxsb3dQYXRocyA9IFtdO1xuICAgICAgICBsZXQgdGFnc1dpdGhCbG9ja1BhdGhzID0gW107XG4gICAgICAgIGNvbnN0IHRhZ3NPbmx5RnVuY3Rpb24gPSAoaXRlbTogYW55KTogYW55ID0+IHtcbiAgICAgICAgICAgIHJldHVybiBpdGVtW1widGFnXCJdO1xuICAgICAgICB9O1xuICAgICAgICBjb25zdCB0YWdzV2l0aFBhdGhzRnVuY3Rpb24gPSAoaXRlbTogYW55KTogYW55ID0+IHtcbiAgICAgICAgICAgIHJldHVybiB7IHRhZzogaXRlbVtcInRhZ1wiXSwgcGF0aHM6IGl0ZW1bXCJwYXRoc1wiXSB9O1xuICAgICAgICB9O1xuICAgICAgICBjb25zdCB1cGRhdGVUYWdMaXN0c0Z1bmN0aW9uID0gKHNvdXJjZUxpc3Q6IEFycmF5PGFueT4pOiB2b2lkID0+IHtcbiAgICAgICAgICAgIHRhZ3NXaXRoRmlsdGVycy5wdXNoLmFwcGx5KFxuICAgICAgICAgICAgICAgIHRhZ3NXaXRoRmlsdGVycyxcbiAgICAgICAgICAgICAgICB0aGlzLmV4dHJhY3RUYWdzV2l0aEZpbHRlcnMoc291cmNlTGlzdCwgdGFnc09ubHlGdW5jdGlvbilcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICB0YWdzV2l0aEJsb2NrUGF0aHMucHVzaC5hcHBseShcbiAgICAgICAgICAgICAgICB0YWdzV2l0aEJsb2NrUGF0aHMsXG4gICAgICAgICAgICAgICAgdGhpcy5leHRyYWN0VGFnc1dpdGhGaWx0ZXJzKFxuICAgICAgICAgICAgICAgICAgICBzb3VyY2VMaXN0LFxuICAgICAgICAgICAgICAgICAgICB0YWdzV2l0aFBhdGhzRnVuY3Rpb24sXG4gICAgICAgICAgICAgICAgICAgIFwiYmxvY2tQYXRoc1wiXG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIHRhZ3NXaXRoQWxsb3dQYXRocy5wdXNoLmFwcGx5KFxuICAgICAgICAgICAgICAgIHRhZ3NXaXRoQWxsb3dQYXRocyxcbiAgICAgICAgICAgICAgICB0aGlzLmV4dHJhY3RUYWdzV2l0aEZpbHRlcnMoXG4gICAgICAgICAgICAgICAgICAgIHNvdXJjZUxpc3QsXG4gICAgICAgICAgICAgICAgICAgIHRhZ3NXaXRoUGF0aHNGdW5jdGlvbixcbiAgICAgICAgICAgICAgICAgICAgXCJhbGxvd1BhdGhzXCJcbiAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICApO1xuICAgICAgICB9O1xuXG4gICAgICAgIGZvciAoY29uc3QgY2F0ZWdvcnkgaW4gYWN0aXZlTGlzdCkge1xuICAgICAgICAgICAgY29uc3QgY29va2llVmFsdWUgPSBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXNbY2F0ZWdvcnldO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICBpZiAodGhpcy5zaG91bGRBZGRDYXRlZ29yeVRvTGlzdChjYXRlZ29yeSwgY29va2llVmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgdXBkYXRlVGFnTGlzdHNGdW5jdGlvbihhY3RpdmVMaXN0W2NhdGVnb3J5XSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICBhbGxvd2VkVGFnc0luTG9jYWxTdG9yYWdlICYmXG4gICAgICAgICAgICAgICAgYWxsb3dlZFRhZ3NJbkxvY2FsU3RvcmFnZS5sZW5ndGggPiAwXG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICB1cGRhdGVUYWdMaXN0c0Z1bmN0aW9uKGFsbG93ZWRUYWdzSW5Mb2NhbFN0b3JhZ2UpO1xuICAgICAgICAgICAgICAgIGlmICh0YWdzV2l0aEZpbHRlcnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICB0YWdzV2l0aEZpbHRlcnMgPSB0aGlzLnBydW5lVGFnTGlzdEZyb21Mb2NhbFN0b3JhZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxvd2VkVGFnc0luTG9jYWxTdG9yYWdlLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGFnc1dpdGhGaWx0ZXJzLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHJ1ZVxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICB0YWdzV2l0aEFsbG93UGF0aHMgPSB0aGlzLnBydW5lVGFnTGlzdEZyb21Mb2NhbFN0b3JhZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICBhbGxvd2VkVGFnc0luTG9jYWxTdG9yYWdlLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGFnc1dpdGhBbGxvd1BhdGhzLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIFwidGFnXCJcbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlICYmXG4gICAgICAgICAgICAgICAgYmxvY2tlZFRhZ3NJbkxvY2FsU3RvcmFnZS5sZW5ndGggPiAwXG4gICAgICAgICAgICApIHtcbiAgICAgICAgICAgICAgICB1cGRhdGVUYWdMaXN0c0Z1bmN0aW9uKGJsb2NrZWRUYWdzSW5Mb2NhbFN0b3JhZ2UpO1xuICAgICAgICAgICAgICAgIGlmICh0YWdzV2l0aEZpbHRlcnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICB0YWdzV2l0aEZpbHRlcnMgPSB0aGlzLnBydW5lVGFnTGlzdEZyb21Mb2NhbFN0b3JhZ2UoXG4gICAgICAgICAgICAgICAgICAgICAgICBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlLFxuICAgICAgICAgICAgICAgICAgICAgICAgdGFnc1dpdGhGaWx0ZXJzLFxuICAgICAgICAgICAgICAgICAgICAgICAgZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgdGFnc1dpdGhCbG9ja1BhdGhzID0gdGhpcy5wcnVuZVRhZ0xpc3RGcm9tTG9jYWxTdG9yYWdlKFxuICAgICAgICAgICAgICAgICAgICAgICAgYmxvY2tlZFRhZ3NJbkxvY2FsU3RvcmFnZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhZ3NXaXRoQmxvY2tQYXRocyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICAgICAgXCJ0YWdcIlxuICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICByZWdFeDogdXRpbGl0aWVzLmFycmF5VG9SZWdleCh0YWdzV2l0aEZpbHRlcnMpLFxuICAgICAgICAgICAgYWxsb3dQYXRoVGFnczogdGFnc1dpdGhBbGxvd1BhdGhzLFxuICAgICAgICAgICAgYmxvY2tQYXRoVGFnczogdGFnc1dpdGhCbG9ja1BhdGhzLFxuICAgICAgICB9O1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZSBSZXR1cm4gYSBsaXN0IHBydW5lZCBiYXNlZCBvbiBlbnRyaWVzIGluIHRoZSBnaXZlbiBsb2NhbCBzdG9yYWdlIGxpc3RcbiAgICAgKiBAcGFyYW0ge2FycmF5fSBsb2NhbFN0b3JhZ2VMaXN0IC0gVGhlIGxpc3QgdG8gYmFzZSB0YWcgcHJ1bmluZyBvbiAoYWxsb3cgb3IgYmxvY2spXG4gICAgICogQHBhcmFtIHthcnJheX0gbGlzdFRvUHJ1bmUgLSBUaGUgbGlzdCB0byByZW1vdmUgdGFncyBmcm9tXG4gICAgICogQHBhcmFtIHtib29sZWFufSB3aGl0ZWxpc3RDb25kaXRpb24gLSBXaGV0aGVyIHRvIHBydW5lIGlmIHdoaXRlbGlzdCBpcyBlbmFibGVkIG9yIGRpc2FibGVkXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IGNvbXBhcmlzb25GaWVsZCAtIElmIHRoZXJlIGlzIGEgZmllbGQgZm9yIGVhY2ggdmFsdWUgdG8gdXNlIGZvciBjb21wYXJpc29uLCBvciBqdXN0IHRoZSB2YWx1ZSBpdHNlbGYgKGlmIHVuZGVmaW5lZClcbiAgICAgKiBAcmV0dXJuIHthcnJheX0gLSBUaGUgcHJ1bmVkIGxpc3RcbiAgICAgKi9cbiAgICBwcml2YXRlIHBydW5lVGFnTGlzdEZyb21Mb2NhbFN0b3JhZ2UgPSAoXG4gICAgICAgIGxvY2FsU3RvcmFnZUxpc3Q6IEFycmF5PGFueT4sXG4gICAgICAgIGxpc3RUb1BydW5lOiBBcnJheTxhbnk+LFxuICAgICAgICB3aGl0ZWxpc3RDb25kaXRpb246IGJvb2xlYW4sXG4gICAgICAgIGNvbXBhcmlzb25GaWVsZD86IHN0cmluZ1xuICAgICk6IEFycmF5PGFueT4gPT4ge1xuICAgICAgICBpZiAoXG4gICAgICAgICAgICB1dGlsaXRpZXMuaXNXaGl0ZWxpc3QodGhpcy5nYXRld2F5RGF0YSwgdGhpcy5lbnZpcm9ubWVudCkgPT09XG4gICAgICAgICAgICB3aGl0ZWxpc3RDb25kaXRpb25cbiAgICAgICAgKSB7XG4gICAgICAgICAgICBjb25zdCBsb2NhbFRhZ3NXaXRoTm9GaWx0ZXJzID0gdGhpcy5jaGVja0ZvckZpbHRlcnNUb1JlbW92ZShcbiAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2VMaXN0XG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgbG9jYWxUYWdzV2l0aE5vRmlsdGVycy5mb3JFYWNoKChyZW1vdmVJZlByZXNlbnQpID0+IHtcbiAgICAgICAgICAgICAgICBsaXN0VG9QcnVuZSA9IGxpc3RUb1BydW5lLmZpbHRlcigoaXRlbTogYW55KTogYm9vbGVhbiA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChjb21wYXJpc29uRmllbGQgIT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgaXRlbSA9IGl0ZW1bY29tcGFyaXNvbkZpZWxkXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gaXRlbSAhPT0gcmVtb3ZlSWZQcmVzZW50W1widGFnXCJdO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGxpc3RUb1BydW5lO1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZSBCdWlsZCBhIGxpc3Qgb2YgbG9jYWwgYWxsb3cvYmxvY2sgdGFncyB3aXRob3V0IGFueSBmaWx0ZXJzIGRlZmluZWQuXG4gICAgICogQHBhcmFtIHtBcnJheTxhbnk+fSBsb2NhbFN0b3JhZ2VVUkxzIC0gTGlzdCBvZiBhbnkgVVJMc1xuICAgICAqIEByZXR1cm4ge0FycmF5PGFueT59IGxvY2FsIHN0b3JhZ2UgdGFncyB3aXRoIG5vIGZpbHRlcnNcbiAgICAgKi9cbiAgICBwcml2YXRlIGNoZWNrRm9yRmlsdGVyc1RvUmVtb3ZlID0gKFxuICAgICAgICBsb2NhbFN0b3JhZ2VVUkxzOiBBcnJheTxhbnk+XG4gICAgKTogQXJyYXk8YW55PiA9PiB7XG4gICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2VVUkxzLmZpbHRlcigoaXRlbTogYW55KTogYm9vbGVhbiA9PiB7XG4gICAgICAgICAgICByZXR1cm4gaXRlbS5maWx0ZXIgPT09IHVuZGVmaW5lZDtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIFVwZGF0ZSB0aGUgZ2xvYmFsIGFsbG93ZWQvYmxvY2tlZCB0YWdzIGFuZCB0aGUgcmVnZXggYXNzb2NpYXRlZCB3aXRoIHRoZW1cbiAgICAgKiBAcGFyYW0ge2FueX0gYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzIC0gY29va2llIHZhbHVlcyBzZXQgZm9yIGVhY2ggY2F0ZWdvcnkgaW4gdGhlIGFjdGl2ZSBlbnZpcm9ubWVudCBwYXNzZWQgaW4gYXMgYW4gb2JqZWN0XG4gICAgICogQHBhcmFtIHtBcnJheTxhbnk+fSBhbGxvd2VkVGFnc0luTG9jYWxTdG9yYWdlIC0gYW4gYXJyYXkgY29udGFpbmluZyBhbGwgdGhlIHRhZ3MgdGhhdCBhcmUgbWFya2VkIGFsbG93ZWQgaW4gbG9jYWwgc3RvcmFnZVxuICAgICAqIEBwYXJhbSB7QXJyYXk8YW55Pn0gYmxvY2tlZFRhZ3NJbkxvY2FsU3RvcmFnZSAtIGFuIGFycmF5IGNvbnRhaW5pbmcgYWxsIHRoZSB0YWdzIHRoYXQgYXJlIG1hcmtlZCBibG9ja2VkIGluIGxvY2FsIHN0b3JhZ2VcbiAgICAgKi9cbiAgICBwcml2YXRlIHNldEdsb2JhbHMgPSAoXG4gICAgICAgIGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllczogYW55LFxuICAgICAgICBhbGxvd2VkVGFnc0luTG9jYWxTdG9yYWdlOiBBcnJheTxhbnk+LFxuICAgICAgICBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlOiBBcnJheTxhbnk+XG4gICAgKTogdm9pZCA9PiB7XG4gICAgICAgIHRoaXMuYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzID0gYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzO1xuICAgICAgICAvLyBSZXNldCBnbG9iYWwgYWxsb3cvYmxvY2sgcGF0aHMgbGlzdHNcbiAgICAgICAgY29uc3QgdXBkYXRlZEdsb2JhbHMgPSB0aGlzLmJ1aWxkUGF0aEZpbHRlckxpc3QoXG4gICAgICAgICAgICB0aGlzLmFjdGl2ZUxpc3QsXG4gICAgICAgICAgICB0aGlzLmFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllcyxcbiAgICAgICAgICAgIGFsbG93ZWRUYWdzSW5Mb2NhbFN0b3JhZ2UsXG4gICAgICAgICAgICBibG9ja2VkVGFnc0luTG9jYWxTdG9yYWdlXG4gICAgICAgICk7XG5cbiAgICAgICAgdGhpcy50YWdzV2l0aEFsbG93UGF0aHMgPSB1cGRhdGVkR2xvYmFscy5hbGxvd1BhdGhUYWdzO1xuICAgICAgICB0aGlzLnRhZ3NXaXRoQmxvY2tQYXRocyA9IHVwZGF0ZWRHbG9iYWxzLmJsb2NrUGF0aFRhZ3M7XG4gICAgICAgIHRoaXMudGFnc1dpdGhGaWx0ZXJzUmVnRXggPSB1cGRhdGVkR2xvYmFscy5yZWdFeDtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgR2l2ZW4gYSBzb3VyY2UgbGlzdCBvZiB0YWcgb2JqZWN0LCBhIGZ1bmN0aW9uIGRlZmluaW5nIHdoYXQgdG8gZXh0cmFjdCwgYW5kIGFuIG9wdGlvbmFsIGZpbHRlciB0eXBlLFxuICAgICAqIHJldHVybiBhIGxpc3Qgb2YgdGFncyBbd2l0aCB0aGF0IGZpbHRlciB0eXBlXS5cbiAgICAgKiBAcGFyYW0ge2FycmF5fSB0YWdMaXN0IC0gVGhlIHNvdXJjZSB0YWcgbGlzdCB0byBldmFsdWF0ZS5cbiAgICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBleHRyYWN0RGF0YUZ1bmN0aW9uIC0gR2l2ZW4gYSB0YWcgb2JqZWN0IGZyb20gdGhlIHNvdXJjZSBsaXN0LCByZXR1cm4gdGhlIGRlc2lyZWQgdGFnIHN0cnVjdHVyZS5cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZmlsdGVyVHlwZSAtIE9wdGlvbmFsIC0gSWYgdW5kZWZpbmVkLCB3aWxsIGdldCBhbGwgdGFncyB3aXRoIGRlZmluZWQgZmlsdGVyczsgaWYgc2V0IHRvIFwiYWxsb3dlZFBhdGhzXCIgb3IgXCJibG9ja2VkUGF0aHNcIiwgd2lsbCBvbmx5IHJldHVybiB0YWdzIHdpdGggZmlsdGVycyBvZiB0aGF0IHR5cGUuXG4gICAgICogQHJldHVybiB7YXJyYXl9IFRoZSBsaXN0IG9mIG1hdGNoaW5nIHRhZ3MgZXh0cmFjdGVkIGZyb20gdGhlIHNvdXJjZSB0YWdMaXN0LlxuICAgICAqL1xuICAgIHByaXZhdGUgZXh0cmFjdFRhZ3NXaXRoRmlsdGVycyhcbiAgICAgICAgdGFnTGlzdDogQXJyYXk8YW55PixcbiAgICAgICAgZXh0cmFjdERhdGFGdW5jdGlvbjogYW55LFxuICAgICAgICBmaWx0ZXJUeXBlPzogc3RyaW5nXG4gICAgKTogQXJyYXk8YW55PiB7XG4gICAgICAgIGxldCBmaWx0ZXJGdW5jdGlvbiA9IChpdGVtOiBhbnkpOiBib29sZWFuID0+IHtcbiAgICAgICAgICAgIHJldHVybiBpdGVtW1wiZmlsdGVyXCJdICE9PSB1bmRlZmluZWQ7XG4gICAgICAgIH07XG4gICAgICAgIGlmIChmaWx0ZXJUeXBlICE9IG51bGwpIHtcbiAgICAgICAgICAgIGZpbHRlckZ1bmN0aW9uID0gKGl0ZW06IGFueSk6IGJvb2xlYW4gPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBpdGVtW1wiZmlsdGVyXCJdID09PSBmaWx0ZXJUeXBlO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGFnTGlzdC5maWx0ZXIoZmlsdGVyRnVuY3Rpb24pLm1hcChleHRyYWN0RGF0YUZ1bmN0aW9uKTtcbiAgICB9XG59XG4iLCJpbXBvcnQgeyBFbnNSZXF1ZXN0IH0gZnJvbSBcIi4vZW5zUmVxdWVzdFwiO1xuXG4vKipcbiAqIEFsbG93cyBvciBibG9ja3MgcmVxdWVzdHMgYmFzZWQgb24gaXRzIHByb3RvY29sXG4gKi9cbmV4cG9ydCBjbGFzcyBQcm90b01hbmFnZXIge1xuICAgIHByaXZhdGUgc3RhdGljIHJlYWRvbmx5IGFsbG93ZWRQcm90b2NvbHMgPSB7XG4gICAgICAgIFwiYWJvdXQ6XCI6IHtcbiAgICAgICAgICAgIGJsb2NrUmVhc29uOiBcIkFCT1VUX1BST1RPQ09MXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJibG9iOlwiOiB7XG4gICAgICAgICAgICBibG9ja1JlYXNvbjogXCJCTE9CX1BST1RPQ09MXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJjaHJvbWUtZXh0ZW5zaW9uOlwiOiB7XG4gICAgICAgICAgICBhZGRpdGlvbmFsQWxsb3dDb25kaXRpb25zOiBmdW5jdGlvbihmZWF0dXJlVG9nZ2xlcykge1xuICAgICAgICAgICAgICAgIHJldHVybiAhZmVhdHVyZVRvZ2dsZXMuYmxvY2tFeHRlbnNpb25zO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGJsb2NrUmVhc29uOiBcIkNIUk9NRV9CUk9XU0VSX0VYVEVOU0lPTlwiXG4gICAgICAgIH0sXG4gICAgICAgIFwiZGF0YTpcIjoge1xuICAgICAgICAgICAgYWRkaXRpb25hbEFsbG93Q29uZGl0aW9uczogZnVuY3Rpb24oZmVhdHVyZVRvZ2dsZXMsIHJlcXVlc3QpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAocmVxdWVzdC5kZXN0aW5hdGlvbi5pbmNsdWRlcyhcImRhdGE6aW1hZ2VcIikpIHx8XG4gICAgICAgICAgLy8gcmlnaHQgbm93LCBpdCBhcHBlYXJzIHRoYXQgZGlzYWJsZURhdGFQcm90b2NvbEphdmFTY3JpcHRSZXF1ZXN0cyBhY3R1YWxseSBlbmFibGVzIHRob3NlIHJlcXVlc3RzIHdoZW4gc2V0IHRvIHRydWVcbiAgICAgICAgICAocmVxdWVzdC50eXBlID09PSBcInNjcmlwdFwiICYmIGZlYXR1cmVUb2dnbGVzLmRpc2FibGVEYXRhUHJvdG9jb2xKYXZhc2NyaXB0UmVxdWVzdHMpXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBibG9ja1JlYXNvbjogXCJEQVRBX1BST1RPQ09MXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJoYmxvYjpcIjoge1xuICAgICAgICAgICAgYmxvY2tSZWFzb246IFwiSEJMT0JfUFJPVE9DT0xcIlxuICAgICAgICB9LFxuICAgICAgICBcIm1vei1leHRlbnNpb246XCI6IHtcbiAgICAgICAgICAgIGFkZGl0aW9uYWxBbGxvd0NvbmRpdGlvbnM6IGZ1bmN0aW9uKGZlYXR1cmVUb2dnbGVzKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICFmZWF0dXJlVG9nZ2xlcy5ibG9ja0V4dGVuc2lvbnM7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYmxvY2tSZWFzb246IFwiTU9aX0JST1dTRVJfRVhURU5TSU9OXCJcbiAgICAgICAgfSxcbiAgICAgICAgXCJzYWZhcmktZXh0ZW5zaW9uOlwiOiB7XG4gICAgICAgICAgICBhZGRpdGlvbmFsQWxsb3dDb25kaXRpb25zOiBmdW5jdGlvbihmZWF0dXJlVG9nZ2xlcykge1xuICAgICAgICAgICAgICAgIHJldHVybiAhZmVhdHVyZVRvZ2dsZXMuYmxvY2tFeHRlbnNpb25zO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGJsb2NrUmVhc29uOiBcIlNBRkFSSV9CUk9XU0VSX0VYVEVOU0lPTlwiXG4gICAgICAgIH0sXG4gICAgICAgIFwidGVsOlwiOiB7XG4gICAgICAgICAgICBibG9ja1JlYXNvbjogXCJURUxfUFJPVE9DT0xcIlxuICAgICAgICB9XG4gICAgfTtcblxuICAgIHByaXZhdGUgc3RhdGljIHJlYWRvbmx5IHByb3RvUmVnZXggPSBuZXcgUmVnRXhwKE9iamVjdC5rZXlzKFByb3RvTWFuYWdlci5hbGxvd2VkUHJvdG9jb2xzKS5qb2luKFwifFwiKSwgXCJpXCIpO1xuICAgIHByaXZhdGUgc3RhdGljIHJlYWRvbmx5IHJlcXVlc3RCeXBhc3NSZWdleCAgPSAgbmV3IFJlZ0V4cChcImNocm9tZS1leHRlbnNpb246fHNhZmFyaS1leHRlbnNpb246fG1vei1leHRlbnNpb246XCIsIFwiaVwiKTtcblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgQ2hlY2tzIGlmIGEgcmVxdWVzdCBzaG91bGQgYmUgYWxsb3dlZCBvciBibG9ja2VkIGJhc2VkIG9uIGl0cyBwcm90b2NvbFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwcm90b1N0cmluZyAtIHRoZSBwcm90b2NvbCB3ZSdyZSBjaGVja2luZ1xuICAgICAqIEBwYXJhbSB7RW5zUmVxdWVzdH0gcmVxdWVzdCAtIHRoZSByZXF1ZXN0IHdlJ3JlIGNoZWNraW5nXG4gICAgICogQHBhcmFtIHthbnl9IGZlYXR1cmVUb2dnbGVzIC0gdGhlIGZlYXR1cmVUb2dnbGVzIHNldCBpbiB0aGUgZW5zQ2xpZW50Q29uZmlnXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBhbGxvd1Byb3RvY29sQW5kQnlwYXNzUHJvY2Vzc2luZyhwcm90b1N0cmluZzogc3RyaW5nLCByZXF1ZXN0OiBFbnNSZXF1ZXN0LCBmZWF0dXJlVG9nZ2xlczogYW55KSA6IGJvb2xlYW4ge1xuICAgICAgICBjb25zdCBhbGxvd2VkIDogYm9vbGVhbiA9IHRoaXMucHJvdG9SZWdleC50ZXN0KHByb3RvU3RyaW5nKSAmJiBcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hbGxvd2VkUHJvdG9jb2xzW3Byb3RvU3RyaW5nXSAhPT0gdW5kZWZpbmVkICYmXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICghdGhpcy5hbGxvd2VkUHJvdG9jb2xzW3Byb3RvU3RyaW5nXS5hZGRpdGlvbmFsQWxsb3dDb25kaXRpb25zIHx8IFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hbGxvd2VkUHJvdG9jb2xzW3Byb3RvU3RyaW5nXS5hZGRpdGlvbmFsQWxsb3dDb25kaXRpb25zKGZlYXR1cmVUb2dnbGVzLCByZXF1ZXN0KSk7XG4gICAgICAgIGlmKCFhbGxvd2VkKXtcbiAgICAgICAgICAgIHRoaXMuYWRkQmxvY2tSZWFzb25Ub1JlcXVlc3QocHJvdG9TdHJpbmcsIHJlcXVlc3QpO1xuICAgICAgICB9XG4gICAgICAgIFxuICAgICAgICByZXR1cm4gYWxsb3dlZDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHVibGljIENoZWNrcyBpZiBhIGJsb2NrZWQgcmVxdWVzdCBzaG91bGQgYmUgYnlwYXNzZWQgYmFzZWQgb24gaXRzIHByb3RvY29sXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHByb3RvU3RyaW5nIC0gdGhlIHByb3RvY29sIHdlJ3JlIGNoZWNraW5nXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyBpc0Jsb2NrZWRSZXF1ZXN0QnlwYXNzZWQocHJvdG9TdHJpbmcgOiBzdHJpbmcpIDogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlcXVlc3RCeXBhc3NSZWdleC50ZXN0KHByb3RvU3RyaW5nKTtcbiAgICB9XG5cblxuICAgIC8qKlxuICAgICAqIEBwcml2YXRlIEFkZHMgYSBibG9jayByZWFzb24gdG8gYSByZXF1ZXN0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHByb3RvU3RyaW5nIC0gdGhlIHByb3RvY29sIHdlJ3JlIGNoZWNraW5nXG4gICAgICogQHBhcmFtIHtFbnNSZXF1ZXN0fSByZXF1ZXN0IC0gdGhlIHJlcXVlc3Qgd2UncmUgY2hlY2tpbmdcbiAgICAgKi9cbiAgICBwcml2YXRlIHN0YXRpYyBhZGRCbG9ja1JlYXNvblRvUmVxdWVzdChwcm90b1N0cmluZzogc3RyaW5nLCByZXF1ZXN0OiBFbnNSZXF1ZXN0KSA6IHZvaWQge1xuICAgICAgICBpZihyZXF1ZXN0ICYmIHR5cGVvZiByZXF1ZXN0LmFkZFJlYXNvbiA9PT0gXCJmdW5jdGlvblwiIFxuICAgICAgICAgICAgJiYgdGhpcy5hbGxvd2VkUHJvdG9jb2xzW3Byb3RvU3RyaW5nXSAhPT0gdW5kZWZpbmVkKXtcbiAgICAgICAgICAgIGNvbnN0IGJsb2NrUmVhc29uIDogc3RyaW5nID0gdGhpcy5hbGxvd2VkUHJvdG9jb2xzW3Byb3RvU3RyaW5nXS5ibG9ja1JlYXNvbjtcbiAgICAgICAgICAgIHJlcXVlc3QuYWRkUmVhc29uKGJsb2NrUmVhc29uKTtcbiAgICAgICAgfVxuICAgIH0gXG59XG4iLCJpbXBvcnQgeyB1dGlsaXRpZXMgfSBmcm9tIFwiLi91dGlsaXRpZXNcIjtcbmltcG9ydCB7IEVuc1JlcXVlc3QgfSBmcm9tIFwiLi9lbnNSZXF1ZXN0XCI7XG5pbXBvcnQgeyBVUkwgYXMgVXJsIH0gZnJvbSBcIi4vdXJsUG9ueUZpbGxcIjtcblxuZW51bSBzc2xFbmZvcmNlbWVudCB7XG4gIEJsb2NrID0gXCJCbG9ja1wiLFxuICBSZXdyaXRlID0gXCJSZXdyaXRlXCJcbn1cblxuLyoqXG4gKiBQcm9jZXNzZXMgcmVxdWVzdHMgYmFzZWQgb24gdGhlaXIgcHJvdG9jb2xcbiAqL1xuZXhwb3J0IGNsYXNzIFNTTE1hbmFnZXIge1xuICAgIC8vd291bGQgYmUgZ29vZCB0byBtYWtlIGdhdGV3YXlEYXRhIGFuIG9iamVjdCBpbnN0ZWFkIG9mIGFueSBcbiAgICAvL2FuZCBpbmNsdWRlIGEgZ2F0ZXdheURhdGEgaW50ZXJmYWNlXG4gICAgcHJpdmF0ZSBnYXRld2F5RGF0YTogYW55O1xuICAgIHByaXZhdGUgZW52aXJvbm1lbnQ6IGFueTtcbiAgICBwcml2YXRlIHNzbEVuZm9yY2VtZW50TW9kZTogc3NsRW5mb3JjZW1lbnQ7XG4gICAgcHJpdmF0ZSByZWFkb25seSBpbnNlY3VyZVByb3RvY29sUmVnZXggPSAoL2h0dHA6fHdzOi9pKTtcblxuICAgIC8qKlxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBwYXJhbSB7YW55fSBnYXRld2F5RGF0YSAtIHRoZSBhY3RpdmUgZW5zQ2xpZW50Q29uZmlnIGluIHRoZSBnYXRld2F5XG4gICAgICogQHBhcmFtIHthbnl9IGVudmlyb25tZW50IC0gdGhlIGFjdGl2ZSBlbnZpcm9ubWVudCBpbiB0aGUgZ2F0ZXdheVxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBnYXRld2F5RGF0YTogYW55LFxuICAgICAgICBlbnZpcm9ubWVudDogYW55LFxuICAgICkge1xuICAgICAgICB0aGlzLmdhdGV3YXlEYXRhID0gZ2F0ZXdheURhdGE7XG4gICAgICAgIHRoaXMuZW52aXJvbm1lbnQgPSBlbnZpcm9ubWVudDtcbiAgICAgICAgLy8gZmFsbCBiYWNrIHRvIGJsb2NrIHNzbEVuZm9yY2VtZW50TW9kZSBmb3IgYmFja3dhcmRzIGNvbXBhdGliaWxpdHlcbiAgICAgICAgdGhpcy5zc2xFbmZvcmNlbWVudE1vZGUgPSB0aGlzLmZhbGxiYWNrT3JSZXR1cm5FbmZvcmNlbWVudE1vZGUoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTZXR0ZXIgZnVuY3Rpb24gdGhhdCB1cGRhdGVzIHRoZSBlbnZpcm9ubWVudCBpbiB0aGUgU1NMIE1hbmFnZXJcbiAgICAgKiBAcGFyYW0ge2FueX0gZW52IC0gdGhlIG5ldyBlbnZpcm9ubWVudFxuICAgICAqL1xuICAgIHB1YmxpYyB1cGRhdGVFbnZpcm9ubWVudChlbnY6IGFueSk6IHZvaWQge1xuICAgICAgICB0aGlzLmVudmlyb25tZW50ID0gZW52O1xuICAgICAgICB0aGlzLnNzbEVuZm9yY2VtZW50TW9kZSA9IHRoaXMuZmFsbGJhY2tPclJldHVybkVuZm9yY2VtZW50TW9kZSgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgQ2hlY2tzIHdoZXRoZXIgb3Igbm90IGEgcmVxdWVzdCBzaG91bGQgYmUgYWxsb3dlZCBiYXNlZCBvbiBpdHMgcHJvdG9jb2xcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbG9jYXRpb25Qcm90b2NvbCAtIHRoZSBwcm90b2NvbCBvZiB3aGVyZSB0aGUgcmVxdWVzdCBjYW1lIGZyb21cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gcHJvdG9jb2wgLSB0aGUgcHJvdG9jb2wgb2YgdGhlIHJlcXVlc3Qgd2UncmUgY2hlY2tpbmdcbiAgICAgKiBAcGFyYW0ge0Vuc1JlcXVlc3R9IHJlcXVlc3QgLSB0aGUgcmVxdWVzdCBvYmplY3Qgd2UncmUgY2hlY2tpbmcgYWdhaW5zdFxuICAgICAqL1xuICAgIHB1YmxpYyBhbGxvd1JlcXVlc3QobG9jYXRpb25Qcm90b2NvbDogc3RyaW5nLCBwcm90b2NvbDogc3RyaW5nLCByZXF1ZXN0OiBFbnNSZXF1ZXN0KTogYm9vbGVhbiB7XG4gICAgXG4gICAgICAgIGxldCBTU0xDaGVja1Bhc3NlcyA9IHRydWU7XG4gICAgICAgIGlmKHV0aWxpdGllcy5nZXRFbnZpcm9ubWVudE92ZXJyaWRlT3JHbG9iYWxEZWZhdWx0KHRoaXMuZ2F0ZXdheURhdGEuZmVhdHVyZVRvZ2dsZXMsIHRoaXMuZW52aXJvbm1lbnQsIFwicmVxdWlyZVNTTFwiKSl7XG4gICAgICAgICAgICBpZiAoKGxvY2F0aW9uUHJvdG9jb2wgPT09IFwiaHR0cHM6XCIpICYmIHRoaXMuaW5zZWN1cmVQcm90b2NvbFJlZ2V4LnRlc3QocHJvdG9jb2wpKSB7XG4gICAgICAgICAgICAgICAgU1NMQ2hlY2tQYXNzZXMgPSBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKCFTU0xDaGVja1Bhc3Nlcykge1xuICAgICAgICAgICAgICAgIHJlcXVlc3QuYWRkUmVhc29uKFwiU1NMXCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBTU0xDaGVja1Bhc3NlcztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHVibGljIENoZWNrcyB3aGV0aGVyIG9yIG5vdCB3ZSBzaG91bGQgYmUgcmV3cml0aW5nIHRoZSBwcm90b2NvbCBiYXNlZCBvbiBTU0wgZW5mb3JjZW1lbnQgbW9kZVxuICAgICAqIEBwYXJhbSB7RW5zUmVxdWVzdH0gcmVxdWVzdCAtIHRoZSByZXF1ZXN0IHdlJ3JlIGNoZWNraW5nXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlZmVycmVyIC0gdGhlIHdlYiBsb2NhdGlvbiB3aGVyZSB0aGUgcmVxdWVzdCBvcmlnaW5hdGVkXG4gICAgICovXG4gICAgcHVibGljIHNob3VsZFJld3JpdGVQcm90b2NvbChyZXF1ZXN0OiBFbnNSZXF1ZXN0LCByZWZlcnJlcjogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgICAgIGNvbnN0IHByb3RvY29sID0gbmV3IFVybChyZXF1ZXN0LmRlc3RpbmF0aW9uLCByZWZlcnJlcikucHJvdG9jb2w7XG5cbiAgICAgICAgbGV0IHNob3VsZFJld3JpdGUgPSBmYWxzZTtcblxuICAgICAgICBpZiAodGhpcy5pbnNlY3VyZVByb3RvY29sUmVnZXgudGVzdChwcm90b2NvbCkpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnNzbEVuZm9yY2VtZW50TW9kZSA9PT0gc3NsRW5mb3JjZW1lbnQuUmV3cml0ZSAmJiAhcmVxdWVzdC5oYXNSZWFzb24oXCJXaGl0ZWxpc3RcIikgJiYgIXJlcXVlc3QuaGFzUmVhc29uKFwiQmxhY2tsaXN0XCIpICYmIHJlcXVlc3QuaGFzUmVhc29uKFwiU1NMXCIpKSB7XG4gICAgICAgICAgICAgICAgc2hvdWxkUmV3cml0ZSA9IHRydWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gc2hvdWxkUmV3cml0ZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHVibGljIFJld3JpdGVzIHRoZSBwcm90b2NvbCBvZiB0aGUgcmVxdWVzdFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1cmxTdHJpbmcgLSB0aGUgdXJsIHdlJ3JlIGNoZWNraW5nIGFzIGEgc3RyaW5nXG4gICAgICovXG4gICAgcHVibGljIHJld3JpdGVQcm90b2NvbCh1cmxTdHJpbmc6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIHVybFN0cmluZyA9IHVybFN0cmluZy5yZXBsYWNlKC9eaHR0cDpcXC9cXC8vaSwgXCJodHRwczovL1wiKTtcbiAgICAgICAgdXJsU3RyaW5nID0gdXJsU3RyaW5nLnJlcGxhY2UoL153czpcXC9cXC8vaSwgXCJ3c3M6Ly9cIik7XG4gIFx0ICAgIHJldHVybiB1cmxTdHJpbmc7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgU2V0cyB0aGUgZW5mb3JjZW1lbnQgbW9kZSBpbiB0aGUgU1NMIE1hbmFnZXIgb3IgZmFsbHMgYmFjayB0byBibG9jayBtb2RlIGJ5IGRlZmF1bHRcbiAgICAgKi9cbiAgICBwcml2YXRlIGZhbGxiYWNrT3JSZXR1cm5FbmZvcmNlbWVudE1vZGUoKTogc3NsRW5mb3JjZW1lbnQge1xuICAgICAgICBpZiAoIXRoaXMuZ2F0ZXdheURhdGEuc3NsQ29uZmlnICYmICghdGhpcy5lbnZpcm9ubWVudCB8fCAhdGhpcy5lbnZpcm9ubWVudC5zc2xFbmZvcmNlbWVudE1vZGUpKSB7XG4gICAgICAgICAgICByZXR1cm4gc3NsRW5mb3JjZW1lbnQuQmxvY2s7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdXRpbGl0aWVzLmdldEVudmlyb25tZW50T3ZlcnJpZGVPckdsb2JhbERlZmF1bHQodGhpcy5nYXRld2F5RGF0YS5zc2xDb25maWcsIHRoaXMuZW52aXJvbm1lbnQsIFwic3NsRW5mb3JjZW1lbnRNb2RlXCIpO1xuICAgICAgICB9XG4gICAgfVxufVxuIiwiLyogQW55IGNvcHlyaWdodCBpcyBkZWRpY2F0ZWQgdG8gdGhlIFB1YmxpYyBEb21haW4uXG4gKiBodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9wdWJsaWNkb21haW4vemVyby8xLjAvICovXG5cbmV4cG9ydCBmdW5jdGlvbiBVUkxqc0ltcGwoKXtcblxuICAgIHZhciByZWxhdGl2ZSA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgcmVsYXRpdmVbXCJmdHBcIl0gPSAyMTtcbiAgICByZWxhdGl2ZVtcImZpbGVcIl0gPSAwO1xuICAgIHJlbGF0aXZlW1wiZ29waGVyXCJdID0gNzA7XG4gICAgcmVsYXRpdmVbXCJodHRwXCJdID0gODA7XG4gICAgcmVsYXRpdmVbXCJodHRwc1wiXSA9IDQ0MztcbiAgICByZWxhdGl2ZVtcIndzXCJdID0gODA7XG4gICAgcmVsYXRpdmVbXCJ3c3NcIl0gPSA0NDM7XG5cbiAgICB2YXIgcmVsYXRpdmVQYXRoRG90TWFwcGluZyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgcmVsYXRpdmVQYXRoRG90TWFwcGluZ1tcIiUyZVwiXSA9IFwiLlwiO1xuICAgIHJlbGF0aXZlUGF0aERvdE1hcHBpbmdbXCIuJTJlXCJdID0gXCIuLlwiO1xuICAgIHJlbGF0aXZlUGF0aERvdE1hcHBpbmdbXCIlMmUuXCJdID0gXCIuLlwiO1xuICAgIHJlbGF0aXZlUGF0aERvdE1hcHBpbmdbXCIlMmUlMmVcIl0gPSBcIi4uXCI7XG5cbiAgICBmdW5jdGlvbiBpc1JlbGF0aXZlU2NoZW1lKHNjaGVtZSkge1xuICAgICAgICByZXR1cm4gcmVsYXRpdmVbc2NoZW1lXSAhPT0gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIGludmFsaWQoKSB7XG4gICAgICAgIGNsZWFyLmNhbGwodGhpcyk7XG4gICAgICAgIHRoaXMuX2lzSW52YWxpZCA9IHRydWU7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gSUROQVRvQVNDSUkoaCkge1xuICAgICAgICBpZiAoXCJcIiA9PT0gaCkge1xuICAgICAgICAgICAgaW52YWxpZC5jYWxsKHRoaXMpO1xuICAgICAgICB9XG4gICAgICAgIC8vIFhYWFxuICAgICAgICByZXR1cm4gaC50b0xvd2VyQ2FzZSgpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHBlcmNlbnRFc2NhcGUoYykge1xuICAgICAgICB2YXIgdW5pY29kZSA9IGMuY2hhckNvZGVBdCgwKTtcbiAgICAgICAgaWYgKHVuaWNvZGUgPiAweDIwICYmXG4gICAgICAgIHVuaWNvZGUgPCAweDdGICYmXG4gICAgICAgIC8vIFwiICMgPCA+ID8gYFxuICAgICAgICBbMHgyMiwgMHgyMywgMHgzQywgMHgzRSwgMHgzRiwgMHg2MF0uaW5kZXhPZih1bmljb2RlKSA9PT0gLTFcbiAgICAgICAgKSB7XG4gICAgICAgICAgICByZXR1cm4gYztcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGMpO1xuICAgIH1cblxuICAgIGZ1bmN0aW9uIHBlcmNlbnRFc2NhcGVRdWVyeShjKSB7XG4gICAgICAgIC8vIFhYWCBUaGlzIGFjdHVhbGx5IG5lZWRzIHRvIGVuY29kZSBjIHVzaW5nIGVuY29kaW5nIGFuZCB0aGVuXG4gICAgICAgIC8vIGNvbnZlcnQgdGhlIGJ5dGVzIG9uZS1ieS1vbmUuXG5cbiAgICAgICAgdmFyIHVuaWNvZGUgPSBjLmNoYXJDb2RlQXQoMCk7XG4gICAgICAgIGlmICh1bmljb2RlID4gMHgyMCAmJlxuICAgICAgICB1bmljb2RlIDwgMHg3RiAmJlxuICAgICAgICAvLyBcIiAjIDwgPiBgIChkbyBub3QgZXNjYXBlICc/JylcbiAgICAgICAgWzB4MjIsIDB4MjMsIDB4M0MsIDB4M0UsIDB4NjBdLmluZGV4T2YodW5pY29kZSkgPT09IC0xXG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuIGM7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGVuY29kZVVSSUNvbXBvbmVudChjKTtcbiAgICB9XG5cbiAgICB2YXIgRU9GID0gdW5kZWZpbmVkLFxuICAgICAgICBBTFBIQSA9IC9bYS16QS1aXS8sXG4gICAgICAgIEFMUEhBTlVNRVJJQyA9IC9bYS16QS1aMC05XFwrXFwtXFwuXS87XG5cbiAgICAvKipcbiAgICAgKiBAcGFyYW0geyFzdHJpbmd9IGlucHV0XG4gICAgICogQHBhcmFtIHs/c3RyaW5nPX0gc3RhdGVPdmVycmlkZVxuICAgICAqIEBwYXJhbSB7KFVSTHxzdHJpbmcpPX0gYmFzZVxuICAgICAqL1xuICAgIGZ1bmN0aW9uIHBhcnNlKGlucHV0LCBzdGF0ZU92ZXJyaWRlLCBiYXNlKSB7XG4gICAgICAgIGZ1bmN0aW9uIGVycihtZXNzYWdlKSB7XG4gICAgICAgICAgICBlcnJvcnMucHVzaChtZXNzYWdlKTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgc3RhdGUgPSBzdGF0ZU92ZXJyaWRlIHx8IFwic2NoZW1lIHN0YXJ0XCIsXG4gICAgICAgICAgICBjdXJzb3IgPSAwLFxuICAgICAgICAgICAgYnVmZmVyID0gXCJcIixcbiAgICAgICAgICAgIHNlZW5BdCA9IGZhbHNlLFxuICAgICAgICAgICAgc2VlbkJyYWNrZXQgPSBmYWxzZSxcbiAgICAgICAgICAgIGVycm9ycyA9IFtdO1xuXG4gICAgICAgIGxvb3A6IHdoaWxlICgoaW5wdXRbY3Vyc29yIC0gMV0gIT09IEVPRiB8fCBjdXJzb3IgPT09IDApICYmICF0aGlzLl9pc0ludmFsaWQpIHtcbiAgICAgICAgICAgIHZhciBjID0gaW5wdXRbY3Vyc29yXTtcbiAgICAgICAgICAgIHN3aXRjaCAoc3RhdGUpIHtcbiAgICAgICAgICAgIGNhc2UgXCJzY2hlbWUgc3RhcnRcIjpcbiAgICAgICAgICAgICAgICBpZiAoYyAmJiBBTFBIQS50ZXN0KGMpKSB7XG4gICAgICAgICAgICAgICAgICAgIGJ1ZmZlciArPSBjLnRvTG93ZXJDYXNlKCk7IC8vIEFTQ0lJLXNhZmVcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcInNjaGVtZVwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIXN0YXRlT3ZlcnJpZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgYnVmZmVyID0gXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcIm5vIHNjaGVtZVwiO1xuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBlcnIoXCJJbnZhbGlkIHNjaGVtZS5cIik7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrIGxvb3A7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIFwic2NoZW1lXCI6XG4gICAgICAgICAgICAgICAgaWYgKGMgJiYgQUxQSEFOVU1FUklDLnRlc3QoYykpIHtcbiAgICAgICAgICAgICAgICAgICAgYnVmZmVyICs9IGMudG9Mb3dlckNhc2UoKTsgLy8gQVNDSUktc2FmZVxuICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChcIjpcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9zY2hlbWUgPSBidWZmZXI7XG4gICAgICAgICAgICAgICAgICAgIGJ1ZmZlciA9IFwiXCI7XG4gICAgICAgICAgICAgICAgICAgIGlmIChzdGF0ZU92ZXJyaWRlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhayBsb29wO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChpc1JlbGF0aXZlU2NoZW1lKHRoaXMuX3NjaGVtZSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX2lzUmVsYXRpdmUgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChcImZpbGVcIiA9PT0gdGhpcy5fc2NoZW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwicmVsYXRpdmVcIjtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLl9pc1JlbGF0aXZlICYmIGJhc2UgJiYgYmFzZS5fc2NoZW1lID09PSB0aGlzLl9zY2hlbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRlID0gXCJyZWxhdGl2ZSBvciBhdXRob3JpdHlcIjtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLl9pc1JlbGF0aXZlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwiYXV0aG9yaXR5IGZpcnN0IHNsYXNoXCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwic2NoZW1lIGRhdGFcIjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoIXN0YXRlT3ZlcnJpZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgYnVmZmVyID0gXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgY3Vyc29yID0gMDtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcIm5vIHNjaGVtZVwiO1xuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKEVPRiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICBicmVhayBsb29wO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGVycihcIkNvZGUgcG9pbnQgbm90IGFsbG93ZWQgaW4gc2NoZW1lOiBcIiArIGMpO1xuICAgICAgICAgICAgICAgICAgICBicmVhayBsb29wO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBcInNjaGVtZSBkYXRhXCI6XG4gICAgICAgICAgICAgICAgaWYgKFwiP1wiID09PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3F1ZXJ5ID0gXCI/XCI7XG4gICAgICAgICAgICAgICAgICAgIHN0YXRlID0gXCJxdWVyeVwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoXCIjXCIgPT09IGMpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fZnJhZ21lbnQgPSBcIiNcIjtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcImZyYWdtZW50XCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAvLyBYWFggZXJyb3IgaGFuZGxpbmdcbiAgICAgICAgICAgICAgICAgICAgaWYgKEVPRiAhPT0gYyAmJiBcIlxcdFwiICE9PSBjICYmIFwiXFxuXCIgIT09IGMgJiYgXCJcXHJcIiAhPT0gYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fc2NoZW1lRGF0YSArPSBwZXJjZW50RXNjYXBlKGMpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIFwibm8gc2NoZW1lXCI6XG4gICAgICAgICAgICAgICAgaWYgKCFiYXNlIHx8ICEoaXNSZWxhdGl2ZVNjaGVtZShiYXNlLl9zY2hlbWUpKSkge1xuICAgICAgICAgICAgICAgICAgICBlcnIoXCJNaXNzaW5nIHNjaGVtZS5cIik7XG4gICAgICAgICAgICAgICAgICAgIGludmFsaWQuY2FsbCh0aGlzKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwicmVsYXRpdmVcIjtcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIFwicmVsYXRpdmUgb3IgYXV0aG9yaXR5XCI6XG4gICAgICAgICAgICAgICAgaWYgKFwiL1wiID09PSBjICYmIFwiL1wiID09PSBpbnB1dFtjdXJzb3IrMV0pIHtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcImF1dGhvcml0eSBpZ25vcmUgc2xhc2hlc1wiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGVycihcIkV4cGVjdGVkIC8sIGdvdDogXCIgKyBjKTtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcInJlbGF0aXZlXCI7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBcInJlbGF0aXZlXCI6XG4gICAgICAgICAgICAgICAgdGhpcy5faXNSZWxhdGl2ZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgaWYgKFwiZmlsZVwiICE9PSB0aGlzLl9zY2hlbWUpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3NjaGVtZSA9IGJhc2UuX3NjaGVtZTtcbiAgICAgICAgICAgICAgICBpZiAoRU9GID09PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2hvc3QgPSBiYXNlLl9ob3N0O1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9wb3J0ID0gYmFzZS5fcG9ydDtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fcGF0aCA9IGJhc2UuX3BhdGguc2xpY2UoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fcXVlcnkgPSBiYXNlLl9xdWVyeTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdXNlcm5hbWUgPSBiYXNlLl91c2VybmFtZTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fcGFzc3dvcmQgPSBiYXNlLl9wYXNzd29yZDtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWsgbG9vcDtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFwiL1wiID09PSBjIHx8IFwiXFxcXFwiID09PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChcIlxcXFxcIiA9PT0gYylcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycihcIlxcXFwgaXMgYW4gaW52YWxpZCBjb2RlIHBvaW50LlwiKTtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcInJlbGF0aXZlIHNsYXNoXCI7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChcIj9cIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9ob3N0ID0gYmFzZS5faG9zdDtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fcG9ydCA9IGJhc2UuX3BvcnQ7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3BhdGggPSBiYXNlLl9wYXRoLnNsaWNlKCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3F1ZXJ5ID0gXCI/XCI7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3VzZXJuYW1lID0gYmFzZS5fdXNlcm5hbWU7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3Bhc3N3b3JkID0gYmFzZS5fcGFzc3dvcmQ7XG4gICAgICAgICAgICAgICAgICAgIHN0YXRlID0gXCJxdWVyeVwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoXCIjXCIgPT09IGMpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5faG9zdCA9IGJhc2UuX2hvc3Q7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3BvcnQgPSBiYXNlLl9wb3J0O1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9wYXRoID0gYmFzZS5fcGF0aC5zbGljZSgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9xdWVyeSA9IGJhc2UuX3F1ZXJ5O1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9mcmFnbWVudCA9IFwiI1wiO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl91c2VybmFtZSA9IGJhc2UuX3VzZXJuYW1lO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9wYXNzd29yZCA9IGJhc2UuX3Bhc3N3b3JkO1xuICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwiZnJhZ21lbnRcIjtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB2YXIgbmV4dEMgPSBpbnB1dFtjdXJzb3IrMV07XG4gICAgICAgICAgICAgICAgICAgIHZhciBuZXh0TmV4dEMgPSBpbnB1dFtjdXJzb3IrMl07XG4gICAgICAgICAgICAgICAgICAgIGlmIChcbiAgICAgICAgICAgICAgICAgICAgICAgIFwiZmlsZVwiICE9PSB0aGlzLl9zY2hlbWUgfHwgIUFMUEhBLnRlc3QoYykgfHxcbiAgICAgICAgICAgICAgICAobmV4dEMgIT09IFwiOlwiICYmIG5leHRDICE9PSBcInxcIikgfHxcbiAgICAgICAgICAgICAgICAoRU9GICE9PSBuZXh0TmV4dEMgJiYgXCIvXCIgIT09IG5leHROZXh0QyAmJiBcIlxcXFxcIiAhPT0gbmV4dE5leHRDICYmIFwiP1wiICE9PSBuZXh0TmV4dEMgJiYgXCIjXCIgIT09IG5leHROZXh0QykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX2hvc3QgPSBiYXNlLl9ob3N0O1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fcG9ydCA9IGJhc2UuX3BvcnQ7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl91c2VybmFtZSA9IGJhc2UuX3VzZXJuYW1lO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fcGFzc3dvcmQgPSBiYXNlLl9wYXNzd29yZDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3BhdGggPSBiYXNlLl9wYXRoLnNsaWNlKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9wYXRoLnBvcCgpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHN0YXRlID0gXCJyZWxhdGl2ZSBwYXRoXCI7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBcInJlbGF0aXZlIHNsYXNoXCI6XG4gICAgICAgICAgICAgICAgaWYgKFwiL1wiID09PSBjIHx8IFwiXFxcXFwiID09PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChcIlxcXFxcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyKFwiXFxcXCBpcyBhbiBpbnZhbGlkIGNvZGUgcG9pbnQuXCIpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChcImZpbGVcIiA9PT0gdGhpcy5fc2NoZW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwiZmlsZSBob3N0XCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwiYXV0aG9yaXR5IGlnbm9yZSBzbGFzaGVzXCI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAoXCJmaWxlXCIgIT09IHRoaXMuX3NjaGVtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5faG9zdCA9IGJhc2UuX2hvc3Q7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9wb3J0ID0gYmFzZS5fcG9ydDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3VzZXJuYW1lID0gYmFzZS5fdXNlcm5hbWU7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9wYXNzd29yZCA9IGJhc2UuX3Bhc3N3b3JkO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHN0YXRlID0gXCJyZWxhdGl2ZSBwYXRoXCI7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBcImF1dGhvcml0eSBmaXJzdCBzbGFzaFwiOlxuICAgICAgICAgICAgICAgIGlmIChcIi9cIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwiYXV0aG9yaXR5IHNlY29uZCBzbGFzaFwiO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGVycihcIkV4cGVjdGVkICcvJywgZ290OiBcIiArIGMpO1xuICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwiYXV0aG9yaXR5IGlnbm9yZSBzbGFzaGVzXCI7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBcImF1dGhvcml0eSBzZWNvbmQgc2xhc2hcIjpcbiAgICAgICAgICAgICAgICBzdGF0ZSA9IFwiYXV0aG9yaXR5IGlnbm9yZSBzbGFzaGVzXCI7XG4gICAgICAgICAgICAgICAgaWYgKFwiL1wiICE9PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIGVycihcIkV4cGVjdGVkICcvJywgZ290OiBcIiArIGMpO1xuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgXCJhdXRob3JpdHkgaWdub3JlIHNsYXNoZXNcIjpcbiAgICAgICAgICAgICAgICBpZiAoXCIvXCIgIT09IGMgJiYgXCJcXFxcXCIgIT09IGMpIHtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcImF1dGhvcml0eVwiO1xuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBlcnIoXCJFeHBlY3RlZCBhdXRob3JpdHksIGdvdDogXCIgKyBjKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgXCJhdXRob3JpdHlcIjpcbiAgICAgICAgICAgICAgICBpZiAoXCJAXCIgPT09IGMpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlZW5BdCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyKFwiQCBhbHJlYWR5IHNlZW4uXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgYnVmZmVyICs9IFwiJTQwXCI7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgc2VlbkF0ID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBidWZmZXIubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBjcCA9IGJ1ZmZlcltpXTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChcIlxcdFwiID09PSBjcCB8fCBcIlxcblwiID09PSBjcCB8fCBcIlxcclwiID09PSBjcCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycihcIkludmFsaWQgd2hpdGVzcGFjZSBpbiBhdXRob3JpdHkuXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gWFhYIGNoZWNrIFVSTCBjb2RlIHBvaW50c1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKFwiOlwiID09PSBjcCAmJiBudWxsID09PSB0aGlzLl9wYXNzd29yZCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3Bhc3N3b3JkID0gXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB0ZW1wQyA9IHBlcmNlbnRFc2NhcGUoY3ApO1xuICAgICAgICAgICAgICAgICAgICAgICAgKG51bGwgIT09IHRoaXMuX3Bhc3N3b3JkKSA/IHRoaXMuX3Bhc3N3b3JkICs9IHRlbXBDIDogdGhpcy5fdXNlcm5hbWUgKz0gdGVtcEM7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgYnVmZmVyID0gXCJcIjtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKEVPRiA9PT0gYyB8fCBcIi9cIiA9PT0gYyB8fCBcIlxcXFxcIiA9PT0gYyB8fCBcIj9cIiA9PT0gYyB8fCBcIiNcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICBjdXJzb3IgLT0gYnVmZmVyLmxlbmd0aDtcbiAgICAgICAgICAgICAgICAgICAgYnVmZmVyID0gXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcImhvc3RcIjtcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgYnVmZmVyICs9IGM7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIFwiZmlsZSBob3N0XCI6XG4gICAgICAgICAgICAgICAgaWYgKEVPRiA9PT0gYyB8fCBcIi9cIiA9PT0gYyB8fCBcIlxcXFxcIiA9PT0gYyB8fCBcIj9cIiA9PT0gYyB8fCBcIiNcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICBpZiAoYnVmZmVyLmxlbmd0aCA9PT0gMiAmJiBBTFBIQS50ZXN0KGJ1ZmZlclswXSkgJiYgKGJ1ZmZlclsxXSA9PT0gXCI6XCIgfHwgYnVmZmVyWzFdID09PSBcInxcIikpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRlID0gXCJyZWxhdGl2ZSBwYXRoXCI7XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoYnVmZmVyLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcInJlbGF0aXZlIHBhdGggc3RhcnRcIjtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX2hvc3QgPSBJRE5BVG9BU0NJSS5jYWxsKHRoaXMsIGJ1ZmZlcik7XG4gICAgICAgICAgICAgICAgICAgICAgICBidWZmZXIgPSBcIlwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdGUgPSBcInJlbGF0aXZlIHBhdGggc3RhcnRcIjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFwiXFx0XCIgPT09IGMgfHwgXCJcXG5cIiA9PT0gYyB8fCBcIlxcclwiID09PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIGVycihcIkludmFsaWQgd2hpdGVzcGFjZSBpbiBmaWxlIGhvc3QuXCIpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGJ1ZmZlciArPSBjO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBcImhvc3RcIjpcbiAgICAgICAgICAgIGNhc2UgXCJob3N0bmFtZVwiOlxuICAgICAgICAgICAgICAgIGlmIChcIjpcIiA9PT0gYyAmJiAhc2VlbkJyYWNrZXQpIHtcbiAgICAgICAgICAgICAgICAvLyBYWFggaG9zdCBwYXJzaW5nXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2hvc3QgPSBJRE5BVG9BU0NJSS5jYWxsKHRoaXMsIGJ1ZmZlcik7XG4gICAgICAgICAgICAgICAgICAgIGJ1ZmZlciA9IFwiXCI7XG4gICAgICAgICAgICAgICAgICAgIHN0YXRlID0gXCJwb3J0XCI7XG4gICAgICAgICAgICAgICAgICAgIGlmIChcImhvc3RuYW1lXCIgPT09IHN0YXRlT3ZlcnJpZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrIGxvb3A7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKEVPRiA9PT0gYyB8fCBcIi9cIiA9PT0gYyB8fCBcIlxcXFxcIiA9PT0gYyB8fCBcIj9cIiA9PT0gYyB8fCBcIiNcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9ob3N0ID0gSUROQVRvQVNDSUkuY2FsbCh0aGlzLCBidWZmZXIpO1xuICAgICAgICAgICAgICAgICAgICBpZih0aGlzLl9ob3N0ID09PSBcIlwiKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRocm93IEVycm9yKFwiVW5hYmxlIHRvIGZpbmQgaG9zdFwiKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBidWZmZXIgPSBcIlwiO1xuICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwicmVsYXRpdmUgcGF0aCBzdGFydFwiO1xuICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdGVPdmVycmlkZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWsgbG9vcDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFwiXFx0XCIgIT09IGMgJiYgXCJcXG5cIiAhPT0gYyAmJiBcIlxcclwiICE9PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChcIltcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgc2VlbkJyYWNrZXQgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFwiXVwiID09PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzZWVuQnJhY2tldCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGJ1ZmZlciArPSBjO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGVycihcIkludmFsaWQgY29kZSBwb2ludCBpbiBob3N0L2hvc3RuYW1lOiBcIiArIGMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBicmVhaztcblxuICAgICAgICAgICAgY2FzZSBcInBvcnRcIjpcbiAgICAgICAgICAgICAgICBpZiAoL1swLTldLy50ZXN0KGMpKSB7XG4gICAgICAgICAgICAgICAgICAgIGJ1ZmZlciArPSBjO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoRU9GID09PSBjIHx8IFwiL1wiID09PSBjIHx8IFwiXFxcXFwiID09PSBjIHx8IFwiP1wiID09PSBjIHx8IFwiI1wiID09PSBjIHx8IHN0YXRlT3ZlcnJpZGUpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKFwiXCIgIT09IGJ1ZmZlcikge1xuICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHRlbXAgPSBwYXJzZUludChidWZmZXIsIDEwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0ZW1wICE9PSByZWxhdGl2ZVt0aGlzLl9zY2hlbWVdKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fcG9ydCA9IHRlbXAgKyBcIlwiO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgYnVmZmVyID0gXCJcIjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdGVPdmVycmlkZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWsgbG9vcDtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwicmVsYXRpdmUgcGF0aCBzdGFydFwiO1xuICAgICAgICAgICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFwiXFx0XCIgPT09IGMgfHwgXCJcXG5cIiA9PT0gYyB8fCBcIlxcclwiID09PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIGVycihcIkludmFsaWQgY29kZSBwb2ludCBpbiBwb3J0OiBcIiArIGMpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGludmFsaWQuY2FsbCh0aGlzKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgXCJyZWxhdGl2ZSBwYXRoIHN0YXJ0XCI6XG4gICAgICAgICAgICAgICAgaWYgKFwiXFxcXFwiID09PSBjKVxuICAgICAgICAgICAgICAgICAgICBlcnIoXCInXFxcXCcgbm90IGFsbG93ZWQgaW4gcGF0aC5cIik7XG4gICAgICAgICAgICAgICAgc3RhdGUgPSBcInJlbGF0aXZlIHBhdGhcIjtcbiAgICAgICAgICAgICAgICBpZiAoXCIvXCIgIT09IGMgJiYgXCJcXFxcXCIgIT09IGMpIHtcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIFwicmVsYXRpdmUgcGF0aFwiOlxuICAgICAgICAgICAgICAgIGlmIChFT0YgPT09IGMgfHwgXCIvXCIgPT09IGMgfHwgXCJcXFxcXCIgPT09IGMgfHwgKCFzdGF0ZU92ZXJyaWRlICYmIChcIj9cIiA9PT0gYyB8fCBcIiNcIiA9PT0gYykpKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChcIlxcXFxcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZXJyKFwiXFxcXCBub3QgYWxsb3dlZCBpbiByZWxhdGl2ZSBwYXRoLlwiKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB2YXIgdG1wO1xuICAgICAgICAgICAgICAgICAgICBpZiAodG1wID0gcmVsYXRpdmVQYXRoRG90TWFwcGluZ1tidWZmZXIudG9Mb3dlckNhc2UoKV0pIHsgIC8vIGVzbGludC1kaXNhYmxlLWxpbmUgIG5vLWNvbmQtYXNzaWduXG4gICAgICAgICAgICAgICAgICAgICAgICBidWZmZXIgPSB0bXA7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKFwiLi5cIiA9PT0gYnVmZmVyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9wYXRoLnBvcCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKFwiL1wiICE9PSBjICYmIFwiXFxcXFwiICE9PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fcGF0aC5wdXNoKFwiXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKFwiLlwiID09PSBidWZmZXIgJiYgXCIvXCIgIT09IGMgJiYgXCJcXFxcXCIgIT09IGMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3BhdGgucHVzaChcIlwiKTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChcIi5cIiAhPT0gYnVmZmVyKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoXCJmaWxlXCIgPT09IHRoaXMuX3NjaGVtZSAmJiB0aGlzLl9wYXRoLmxlbmd0aCA9PT0gMCAmJiBidWZmZXIubGVuZ3RoID09PSAyICYmIEFMUEhBLnRlc3QoYnVmZmVyWzBdKSAmJiBidWZmZXJbMV0gPT09IFwifFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnVmZmVyID0gYnVmZmVyWzBdICsgXCI6XCI7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9wYXRoLnB1c2goYnVmZmVyKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBidWZmZXIgPSBcIlwiO1xuICAgICAgICAgICAgICAgICAgICBpZiAoXCI/XCIgPT09IGMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3F1ZXJ5ID0gXCI/XCI7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwicXVlcnlcIjtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChcIiNcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fZnJhZ21lbnQgPSBcIiNcIjtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXRlID0gXCJmcmFnbWVudFwiO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChcIlxcdFwiICE9PSBjICYmIFwiXFxuXCIgIT09IGMgJiYgXCJcXHJcIiAhPT0gYykge1xuICAgICAgICAgICAgICAgICAgICBidWZmZXIgKz0gcGVyY2VudEVzY2FwZShjKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgXCJxdWVyeVwiOlxuICAgICAgICAgICAgICAgIGlmICghc3RhdGVPdmVycmlkZSAmJiBcIiNcIiA9PT0gYykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9mcmFnbWVudCA9IFwiI1wiO1xuICAgICAgICAgICAgICAgICAgICBzdGF0ZSA9IFwiZnJhZ21lbnRcIjtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKEVPRiAhPT0gYyAmJiBcIlxcdFwiICE9PSBjICYmIFwiXFxuXCIgIT09IGMgJiYgXCJcXHJcIiAhPT0gYykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9xdWVyeSArPSBwZXJjZW50RXNjYXBlUXVlcnkoYyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIFwiZnJhZ21lbnRcIjpcbiAgICAgICAgICAgICAgICBpZiAoRU9GICE9PSBjICYmIFwiXFx0XCIgIT09IGMgJiYgXCJcXG5cIiAhPT0gYyAmJiBcIlxcclwiICE9PSBjKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2ZyYWdtZW50ICs9IGM7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjdXJzb3IrKztcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGNsZWFyKCkge1xuICAgICAgICB0aGlzLl9zY2hlbWUgPSBcIlwiO1xuICAgICAgICB0aGlzLl9zY2hlbWVEYXRhID0gXCJcIjtcbiAgICAgICAgdGhpcy5fdXNlcm5hbWUgPSBcIlwiO1xuICAgICAgICB0aGlzLl9wYXNzd29yZCA9IG51bGw7XG4gICAgICAgIHRoaXMuX2hvc3QgPSBcIlwiO1xuICAgICAgICB0aGlzLl9wb3J0ID0gXCJcIjtcbiAgICAgICAgdGhpcy5fcGF0aCA9IFtdO1xuICAgICAgICB0aGlzLl9xdWVyeSA9IFwiXCI7XG4gICAgICAgIHRoaXMuX2ZyYWdtZW50ID0gXCJcIjtcbiAgICAgICAgdGhpcy5faXNJbnZhbGlkID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2lzUmVsYXRpdmUgPSBmYWxzZTtcbiAgICB9XG5cbiAgICAvLyBEb2VzIG5vdCBwcm9jZXNzIGRvbWFpbiBuYW1lcyBvciBJUCBhZGRyZXNzZXMuXG4gICAgLy8gRG9lcyBub3QgaGFuZGxlIGVuY29kaW5nIGZvciB0aGUgcXVlcnkgcGFyYW1ldGVyLlxuICAgIC8qKlxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqIEBleHRlbmRzIHtVUkx9XG4gICAgICogQHBhcmFtIHshc3RyaW5nfSB1cmxcbiAgICAgKiBAcGFyYW0geyhVUkx8c3RyaW5nfExvY2F0aW9uKT19IGJhc2VcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBqVVJMKHVybCwgYmFzZT8gLyogLCBlbmNvZGluZyAqLykge1xuICAgICAgICBpZiAoYmFzZSAhPT0gdW5kZWZpbmVkICYmICEoYmFzZSBpbnN0YW5jZW9mIGpVUkwpKVxuICAgICAgICAgICAgYmFzZSA9IG5ldyBqVVJMKFN0cmluZyhiYXNlKSk7XG5cbiAgICAgICAgdGhpcy5fdXJsID0gXCJcIiArIHVybDtcbiAgICAgICAgY2xlYXIuY2FsbCh0aGlzKTtcblxuICAgICAgICB2YXIgaW5wdXQgPSB0aGlzLl91cmwucmVwbGFjZSgvXlsgXFx0XFxyXFxuXFxmXSt8WyBcXHRcXHJcXG5cXGZdKyQvZywgXCJcIik7XG4gICAgICAgIC8vIGVuY29kaW5nID0gZW5jb2RpbmcgfHwgJ3V0Zi04J1xuXG4gICAgICAgIHBhcnNlLmNhbGwodGhpcywgaW5wdXQsIG51bGwsIGJhc2UpO1xuICAgIH1cblxuICAgIGpVUkwucHJvdG90eXBlID0ge1xuICAgICAgICB0b1N0cmluZzogZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5ocmVmO1xuICAgICAgICB9LFxuICAgICAgICBnZXQgaHJlZigpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLl9pc0ludmFsaWQpXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX3VybDtcblxuICAgICAgICAgICAgdmFyIGF1dGhvcml0eSA9IFwiXCI7XG4gICAgICAgICAgICBpZiAoXCJcIiAhPT0gdGhpcy5fdXNlcm5hbWUgfHwgbnVsbCAhPT0gdGhpcy5fcGFzc3dvcmQpIHtcbiAgICAgICAgICAgICAgICBhdXRob3JpdHkgPSB0aGlzLl91c2VybmFtZSArXG4gICAgICAgICAgICAgICAgKG51bGwgIT09IHRoaXMuX3Bhc3N3b3JkID8gXCI6XCIgKyB0aGlzLl9wYXNzd29yZCA6IFwiXCIpICsgXCJAXCI7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiB0aGlzLnByb3RvY29sICtcbiAgICAgICAgICAgICh0aGlzLl9pc1JlbGF0aXZlID8gXCIvL1wiICsgYXV0aG9yaXR5ICsgdGhpcy5ob3N0IDogXCJcIikgK1xuICAgICAgICAgICAgdGhpcy5wYXRobmFtZSArIHRoaXMuX3F1ZXJ5ICsgdGhpcy5fZnJhZ21lbnQ7XG4gICAgICAgIH0sXG4gICAgICAgIHNldCBocmVmKGhyZWYpIHtcbiAgICAgICAgICAgIGNsZWFyLmNhbGwodGhpcyk7XG4gICAgICAgICAgICBwYXJzZS5jYWxsKHRoaXMsIGhyZWYpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldCBwcm90b2NvbCgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9zY2hlbWUgKyBcIjpcIjtcbiAgICAgICAgfSxcbiAgICAgICAgc2V0IHByb3RvY29sKHByb3RvY29sKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5faXNJbnZhbGlkKVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIHBhcnNlLmNhbGwodGhpcywgcHJvdG9jb2wgKyBcIjpcIiwgXCJzY2hlbWUgc3RhcnRcIik7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZ2V0IGhvc3QoKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5faXNJbnZhbGlkID8gXCJcIiA6IHRoaXMuX3BvcnQgP1xuICAgICAgICAgICAgICAgIHRoaXMuX2hvc3QgKyBcIjpcIiArIHRoaXMuX3BvcnQgOiB0aGlzLl9ob3N0O1xuICAgICAgICB9LFxuICAgICAgICBzZXQgaG9zdChob3N0KSB7XG4gICAgICAgICAgICBpZiAodGhpcy5faXNJbnZhbGlkIHx8ICF0aGlzLl9pc1JlbGF0aXZlKVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIHBhcnNlLmNhbGwodGhpcywgaG9zdCwgXCJob3N0XCIpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldCBob3N0bmFtZSgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9ob3N0O1xuICAgICAgICB9LFxuICAgICAgICBzZXQgaG9zdG5hbWUoaG9zdG5hbWUpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLl9pc0ludmFsaWQgfHwgIXRoaXMuX2lzUmVsYXRpdmUpXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgcGFyc2UuY2FsbCh0aGlzLCBob3N0bmFtZSwgXCJob3N0bmFtZVwiKTtcbiAgICAgICAgfSxcblxuICAgICAgICBnZXQgcG9ydCgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9wb3J0O1xuICAgICAgICB9LFxuICAgICAgICBzZXQgcG9ydChwb3J0KSB7XG4gICAgICAgICAgICBpZiAodGhpcy5faXNJbnZhbGlkIHx8ICF0aGlzLl9pc1JlbGF0aXZlKVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIHBhcnNlLmNhbGwodGhpcywgcG9ydCwgXCJwb3J0XCIpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldCBwYXRobmFtZSgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9pc0ludmFsaWQgPyBcIlwiIDogdGhpcy5faXNSZWxhdGl2ZSA/XG4gICAgICAgICAgICAgICAgXCIvXCIgKyB0aGlzLl9wYXRoLmpvaW4oXCIvXCIpIDogdGhpcy5fc2NoZW1lRGF0YTtcbiAgICAgICAgfSxcbiAgICAgICAgc2V0IHBhdGhuYW1lKHBhdGhuYW1lKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5faXNJbnZhbGlkIHx8ICF0aGlzLl9pc1JlbGF0aXZlKVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIHRoaXMuX3BhdGggPSBbXTtcbiAgICAgICAgICAgIHBhcnNlLmNhbGwodGhpcywgcGF0aG5hbWUsIFwicmVsYXRpdmUgcGF0aCBzdGFydFwiKTtcbiAgICAgICAgfSxcblxuICAgICAgICBnZXQgc2VhcmNoKCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2lzSW52YWxpZCB8fCAhdGhpcy5fcXVlcnkgfHwgXCI/XCIgPT09IHRoaXMuX3F1ZXJ5ID9cbiAgICAgICAgICAgICAgICBcIlwiIDogdGhpcy5fcXVlcnk7XG4gICAgICAgIH0sXG4gICAgICAgIHNldCBzZWFyY2goc2VhcmNoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5faXNJbnZhbGlkIHx8ICF0aGlzLl9pc1JlbGF0aXZlKVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIHRoaXMuX3F1ZXJ5ID0gXCI/XCI7XG4gICAgICAgICAgICBpZiAoXCI/XCIgPT09IHNlYXJjaFswXSlcbiAgICAgICAgICAgICAgICBzZWFyY2ggPSBzZWFyY2guc2xpY2UoMSk7XG4gICAgICAgICAgICBwYXJzZS5jYWxsKHRoaXMsIHNlYXJjaCwgXCJxdWVyeVwiKTtcbiAgICAgICAgfSxcblxuICAgICAgICBnZXQgaGFzaCgpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9pc0ludmFsaWQgfHwgIXRoaXMuX2ZyYWdtZW50IHx8IFwiI1wiID09PSB0aGlzLl9mcmFnbWVudCA/XG4gICAgICAgICAgICAgICAgXCJcIiA6IHRoaXMuX2ZyYWdtZW50O1xuICAgICAgICB9LFxuICAgICAgICBzZXQgaGFzaChoYXNoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5faXNJbnZhbGlkKVxuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIGlmKCFoYXNoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fZnJhZ21lbnQgPSBcIlwiO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH0gXG4gICAgICAgICAgICB0aGlzLl9mcmFnbWVudCA9IFwiI1wiO1xuICAgICAgICAgICAgaWYgKFwiI1wiID09PSBoYXNoWzBdKVxuICAgICAgICAgICAgICAgIGhhc2ggPSBoYXNoLnNsaWNlKDEpO1xuICAgICAgICAgICAgcGFyc2UuY2FsbCh0aGlzLCBoYXNoLCBcImZyYWdtZW50XCIpO1xuICAgICAgICB9LFxuXG4gICAgICAgIGdldCBvcmlnaW4oKSB7XG4gICAgICAgICAgICB2YXIgaG9zdDtcbiAgICAgICAgICAgIGlmICh0aGlzLl9pc0ludmFsaWQgfHwgIXRoaXMuX3NjaGVtZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiBcIlwiO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gamF2YXNjcmlwdDogR2Vja28gcmV0dXJucyBTdHJpbmcoXCJcIiksIFdlYktpdC9CbGluayBTdHJpbmcoXCJudWxsXCIpXG4gICAgICAgICAgICAvLyBHZWNrbyB0aHJvd3MgZXJyb3IgZm9yIFwiZGF0YTovL1wiXG4gICAgICAgICAgICAvLyBkYXRhOiBHZWNrbyByZXR1cm5zIFwiXCIsIEJsaW5rIHJldHVybnMgXCJkYXRhOi8vXCIsIFdlYktpdCByZXR1cm5zIFwibnVsbFwiXG4gICAgICAgICAgICAvLyBHZWNrbyByZXR1cm5zIFN0cmluZyhcIlwiKSBmb3IgZmlsZTogbWFpbHRvOlxuICAgICAgICAgICAgLy8gV2ViS2l0L0JsaW5rIHJldHVybnMgU3RyaW5nKFwiU0NIRU1FOi8vXCIpIGZvciBmaWxlOiBtYWlsdG86XG4gICAgICAgICAgICBzd2l0Y2ggKHRoaXMuX3NjaGVtZSkge1xuICAgICAgICAgICAgY2FzZSBcImRhdGFcIjpcbiAgICAgICAgICAgIGNhc2UgXCJmaWxlXCI6XG4gICAgICAgICAgICBjYXNlIFwiamF2YXNjcmlwdFwiOlxuICAgICAgICAgICAgY2FzZSBcIm1haWx0b1wiOlxuICAgICAgICAgICAgICAgIHJldHVybiBcIm51bGxcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGhvc3QgPSB0aGlzLmhvc3Q7XG4gICAgICAgICAgICBpZiAoIWhvc3QpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB0aGlzLl9zY2hlbWUgKyBcIjovL1wiICsgaG9zdDtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICAvLyBDb3B5IG92ZXIgdGhlIHN0YXRpYyBtZXRob2RzXG4gICAgdmFyIE9yaWdpbmFsVVJMID0gc2VsZi5VUkw7XG4gICAgaWYgKE9yaWdpbmFsVVJMKSB7XG4gICAgICAgIGpVUkxbXCJjcmVhdGVPYmplY3RVUkxcIl0gPSBmdW5jdGlvbihibG9iKSB7XG4gICAgICAgIC8vIElFIGV4dGVuc2lvbiBhbGxvd3MgYSBzZWNvbmQgb3B0aW9uYWwgb3B0aW9ucyBhcmd1bWVudC5cbiAgICAgICAgLy8gaHR0cDovL21zZG4ubWljcm9zb2Z0LmNvbS9lbi11cy9saWJyYXJ5L2llL2hoNzcyMzAyKHY9dnMuODUpLmFzcHhcbiAgICAgICAgICAgIHJldHVybiBPcmlnaW5hbFVSTC5jcmVhdGVPYmplY3RVUkwuYXBwbHkoT3JpZ2luYWxVUkwsIGFyZ3VtZW50cyk7XG4gICAgICAgIH07XG4gICAgICAgIGpVUkxbXCJyZXZva2VPYmplY3RVUkxcIl0gPSBmdW5jdGlvbih1cmwpIHtcbiAgICAgICAgICAgIE9yaWdpbmFsVVJMLnJldm9rZU9iamVjdFVSTCh1cmwpO1xuICAgICAgICB9O1xuICAgIH1cbiAgXG4gICAgcmV0dXJuIGpVUkw7XG59XG4gIFxuZXhwb3J0IGNvbnN0IFVSTCA9IFVSTGpzSW1wbCgpO1xuICBcbi8qKlxuICAgKiBIZWxwZXIgdG8gZmVhdHVyZSBkZXRlY3QgYSB3b3JraW5nIG5hdGl2ZSBVUkwgaW1wbGVtZW50YXRpb25cbiAgICogQHJldHVybiB7Ym9vbH1cbiAgICovXG5leHBvcnQgZnVuY3Rpb24gaGFzTmF0aXZlVVJMKCkgeyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lICBAdHlwZXNjcmlwdC1lc2xpbnQvZXhwbGljaXQtbW9kdWxlLWJvdW5kYXJ5LXR5cGVzXG4gICAgdmFyIGhhc1dvcmtpbmdVcmwgPSBmYWxzZTtcbiAgXG4gICAgdHJ5IHtcbiAgICAgICAgdmFyIHUgPSBuZXcgc2VsZi5VUkwoXCJiXCIsIFwiaHR0cDovL2FcIik7XG4gICAgICAgIHUucGF0aG5hbWUgPSBcImMlMjBkXCI7XG4gICAgICAgIGhhc1dvcmtpbmdVcmwgPSB1LmhyZWYgPT09IFwiaHR0cDovL2EvYyUyMGRcIjtcbiAgICB9IGNhdGNoKGUpIHtcbiAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgfVxuICBcbiAgICByZXR1cm4gaGFzV29ya2luZ1VybDtcbn0iLCJpbXBvcnQgeyBFbnNSZXF1ZXN0IH0gZnJvbSBcIi4vZW5zUmVxdWVzdFwiO1xuaW1wb3J0IHsgdXRpbGl0aWVzIH0gZnJvbSBcIi4vdXRpbGl0aWVzXCI7XG5pbXBvcnQgeyBTU0xNYW5hZ2VyIH0gZnJvbSBcIi4vc3NsTWFuYWdlclwiO1xuaW1wb3J0IHsgRGF0YUdvdmVybmFuY2VNYW5hZ2VyIH0gZnJvbSBcIi4vZGF0YUdvdmVybmFuY2VNYW5hZ2VyXCI7XG5pbXBvcnQgeyBQcm90b01hbmFnZXIgfSBmcm9tIFwiLi9wcm90b01hbmFnZXJcIjtcbmltcG9ydCB7IExpc3RQcm9jZXNzb3IgfSBmcm9tIFwiLi9saXN0UHJvY2Vzc29yXCI7XG5pbXBvcnQgeyBVUkwgYXMgVXJsIH0gZnJvbSBcIi4vdXJsUG9ueUZpbGxcIjtcblxuLyoqXG4gKiBIYW5kbGVzIHVybCBwcm9jZXNzaW5nIGluIGFuIGFnbm9zdGljIGVudmlyb25tZW50XG4gKi9cbmV4cG9ydCBjbGFzcyBVcmxQcm9jZXNzb3Ige1xuICAgIHByaXZhdGUgc3NsTWFuYWdlcjtcbiAgICBwcml2YXRlIGRhdGFHb3Zlcm5hbmNlTWFuYWdlcjtcbiAgICBwcml2YXRlIHByb3RvTWFuYWdlcjtcbiAgICBwcml2YXRlIGxpc3RQcm9jZXNzb3I7XG5cbiAgICAvKipcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKiBAcGFyYW0ge2FueX0gZ2F0ZXdheURhdGEgLSB0aGUgZ2F0ZXdheURhdGEgc2V0IGluIHRoZSBlbnNDbGllbnRDb25maWdcbiAgICAgKiBAcGFyYW0ge2FueX0gZW52aXJvbm1lbnQgLSB0aGUgYWN0aXZlIGVudmlyb25tZW50IGluIHRoZSBnYXRld2F5IFxuICAgICAqIEBwYXJhbSB7YW55fSBoaXN0b3J5RW50cnlSZXBvcnRlciAtIHJlZmVyZW5jZSB0byB0aGUgaGlzdG9yeSBlbnRyeSByZXBvcnRlclxuICAgICAqIEBwYXJhbSB7YW55fSBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXMgLSBjb29raWUgdmFsdWVzIHNldCBpbiB0aGUgYWN0aXZlIGVudmlyb25tZW50IHBhc3NlZCBpbiBhcyBhbiBvYmplY3RcbiAgICAgKiBAcGFyYW0ge0FycmF5PGFueT59IGFsbG93ZWRVcmxzIC0gYWxsb3dlZCB1cmxzIHBhc3NlZCBpbiBmcm9tIHRhZyBhY2Nlc3MgbWFuYWdlclxuICAgICAqIEBwYXJhbSB7QXJyYXk8YW55Pn0gYmxvY2tlZFVybHMgLSBibG9ja2VkIHVybHMgcGFzc2VkIGluIGZyb20gdGFnIGFjY2VzcyBtYW5hZ2VyXG4gICAgICogQHBhcmFtIHtib29sZWFufSBpbnNwZWN0Rm9ybURhdGEgLSBpZiB3ZSdyZSBhbGxvd2luZyBGb3JtRGF0YSBhbmQgVVJMU2VhcmNoUGFyYW1zIHRvIGJlIHByb2Nlc3NlZFxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcm90ZWN0ZWQgZ2F0ZXdheURhdGE6IGFueSxcbiAgICAgICAgcHJvdGVjdGVkIGVudmlyb25tZW50OiBhbnksXG4gICAgICAgIHByaXZhdGUgaGlzdG9yeUVudHJ5UmVwb3J0ZXI6IGFueSxcbiAgICAgICAgcHJpdmF0ZSBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXM6IEFycmF5PGFueT4gPSBbXSxcbiAgICAgICAgcHJpdmF0ZSBhbGxvd2VkVXJsczogQXJyYXk8YW55PiA9IFtdLFxuICAgICAgICBwcml2YXRlIGJsb2NrZWRVcmxzOiBBcnJheTxhbnk+ID0gW10sXG4gICAgICAgIHByaXZhdGUgaW5zcGVjdEZvcm1EYXRhOiBib29sZWFuLFxuICAgICkge1xuICAgICAgICB0aGlzLnNzbE1hbmFnZXIgPSBuZXcgU1NMTWFuYWdlcihnYXRld2F5RGF0YSwgZW52aXJvbm1lbnQpO1xuICAgICAgICB0aGlzLmRhdGFHb3Zlcm5hbmNlTWFuYWdlciA9IG5ldyBEYXRhR292ZXJuYW5jZU1hbmFnZXIoXG4gICAgICAgICAgICBnYXRld2F5RGF0YS5mZWF0dXJlVG9nZ2xlcy5lbmFibGVEYXRhTW9uaXRvcmluZyxcbiAgICAgICAgICAgIGdhdGV3YXlEYXRhLmRhdGFNb25pdG9yaW5nLFxuICAgICAgICAgICAgZW52aXJvbm1lbnQsXG4gICAgICAgICAgICBpbnNwZWN0Rm9ybURhdGEsXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMucHJvdG9NYW5hZ2VyID0gUHJvdG9NYW5hZ2VyO1xuICAgICAgICB0aGlzLmxpc3RQcm9jZXNzb3IgPSBuZXcgTGlzdFByb2Nlc3NvcihcbiAgICAgICAgICAgIGVudmlyb25tZW50LFxuICAgICAgICAgICAgZ2F0ZXdheURhdGEsXG4gICAgICAgICAgICB0aGlzLmFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllcyxcbiAgICAgICAgICAgIHRoaXMuYWxsb3dlZFVybHMsXG4gICAgICAgICAgICB0aGlzLmJsb2NrZWRVcmxzLFxuICAgICAgICApO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgU2V0dGVyIGZ1bmN0aW9uIHdoaWNoIHVwZGF0ZXMgdGhlIGVudmlyb25tZW50IGF0dGFjaGVkIHRvIHRoaXMgY2xhc3NcbiAgICAgKiBAcGFyYW0ge2FueX0gbmV3RW52aXJvbm1lbnQgLSB0aGUgbmV3IGVudmlyb25tZW50XG4gICAgICogQHBhcmFtIHthbnl9IGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllcyAtIGNvb2tpZSB2YWx1ZXMgc2V0IGluIHRoZSBhY3RpdmUgZW52aXJvbm1lbnQgcGFzc2VkIGluIGFzIGFuIG9iamVjdFxuICAgICAqIEBwYXJhbSB7QXJyYXk8YW55Pn0gYWxsb3dlZFVybHMgLSBhbGxvd2VkIHVybHMgcGFzc2VkIGluIGZyb20gdGFnIGFjY2VzcyBtYW5hZ2VyXG4gICAgICogQHBhcmFtIHtBcnJheTxhbnk+fSBibG9ja2VkVXJscyAtIGJsb2NrZWQgdXJscyBwYXNzZWQgaW4gZnJvbSB0YWcgYWNjZXNzIG1hbmFnZXJcbiAgICAgKi9cbiAgICBwdWJsaWMgdXBkYXRlRW52aXJvbm1lbnQgPSAobmV3RW52aXJvbm1lbnQ6IGFueSwgYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzOiBhbnksIGFsbG93ZWRVcmxzOiBBcnJheTxhbnk+LCBibG9ja2VkVXJsczogQXJyYXk8YW55Pik6IHZvaWQgPT4ge1xuICAgICAgICB0aGlzLmVudmlyb25tZW50ID0gbmV3RW52aXJvbm1lbnQ7XG4gICAgICAgIHRoaXMuZGF0YUdvdmVybmFuY2VNYW5hZ2VyLnVwZGF0ZUVudmlyb25tZW50KG5ld0Vudmlyb25tZW50KTtcbiAgICAgICAgdGhpcy5zc2xNYW5hZ2VyLnVwZGF0ZUVudmlyb25tZW50KG5ld0Vudmlyb25tZW50KTtcbiAgICAgICAgdGhpcy5saXN0UHJvY2Vzc29yLnVwZGF0ZUVudmlyb25tZW50KG5ld0Vudmlyb25tZW50LCBhY3RpdmVFbnZpcm9ubWVudENhdGVnb3JpZXMsIGFsbG93ZWRVcmxzLCBibG9ja2VkVXJscyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyBQdWJsaWNhbGx5IGV4cG9zZWQgZnVuY3Rpb24gdG8gdXBkYXRlIHRoZSBsaXN0IHZhbHVlcyBpbiB0aGUgbGlzdCBwcm9jZXNzb3JcbiAgICAgKiBAcGFyYW0ge2FueX0gYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzIC0gY29va2llIHZhbHVlcyBzZXQgaW4gdGhlIGFjdGl2ZSBlbnZpcm9ubWVudCBwYXNzZWQgaW4gYXMgYW4gb2JqZWN0XG4gICAgICogQHBhcmFtIHtBcnJheTxhbnk+fSBhbGxvd2VkVXJscyAtIGFsbG93ZWQgdXJscyBwYXNzZWQgaW4gZnJvbSB0YWcgYWNjZXNzIG1hbmFnZXJcbiAgICAgKiBAcGFyYW0ge0FycmF5PGFueT59IGJsb2NrZWRVcmxzIC0gYmxvY2tlZCB1cmxzIHBhc3NlZCBpbiBmcm9tIHRhZyBhY2Nlc3MgbWFuYWdlclxuICAgICAqL1xuICAgIHB1YmxpYyB1cGRhdGVMaXN0VmFsdWVzID0gKGFjdGl2ZUVudmlyb25tZW50Q2F0ZWdvcmllczogYW55LCBhbGxvd2VkVXJsczogQXJyYXk8YW55PiwgYmxvY2tlZFVybHM6IEFycmF5PGFueT4pOiB2b2lkID0+IHtcbiAgICAgICAgdGhpcy5saXN0UHJvY2Vzc29yLnVwZGF0ZUxpc3RWYWx1ZXMoYWN0aXZlRW52aXJvbm1lbnRDYXRlZ29yaWVzLCBhbGxvd2VkVXJscywgYmxvY2tlZFVybHMpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgUHJvY2Vzc2VzIHVybHMgLSBjYWxscyBvdXIgaW50ZXJuYWwgcHJvY2Vzc2luZyBmdW5jdGlvblxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1cmwgLSB0aGUgdXJsIHRvIGJlIHByb2Nlc3NlZFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSByZXF1ZXN0VHlwZSAtIHRoZSB0eXBlIG9mIHJlcXVlc3QgYmVpbmcgcHJvY2Vzc2VkXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHNvdXJjZSAtIHdoZXJlIHRoZSByZXF1ZXN0IG9yaWdpbmF0ZWQgZnJvbVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSByZWZlcnJlciAtIHRoZSB3ZWIgbG9jYXRpb24gd2hlcmUgdGhlIHJlcXVlc3Qgb3JpZ2luYXRlZFxuICAgICAqIEBwYXJhbSB7RnVuY3Rpb259IG9uVXJsTW9kaWZpZWRGbiAtIGEgY2FsbGJhY2sgdGhhdCBpcyBpbnZva2VkIGlmIHRoZSB1cmwgaXMgbW9kaWZpZWRcbiAgICAgKi9cbiAgICBwdWJsaWMgcHJvY2Vzc1VSTCh1cmw6IHN0cmluZywgcmVxdWVzdFR5cGU6IHN0cmluZywgc291cmNlOiBzdHJpbmcsIHJlZmVycmVyOiBzdHJpbmcsIG9uVXJsTW9kaWZpZWRGbj86IEZ1bmN0aW9uKTogc3RyaW5nIHtcbiAgICAgICAgY29uc3Qgbm9kZSA9IHt9O1xuICAgICAgICByZXR1cm4gdGhpcy5wcm9jZXNzVVJMSW50ZXJuYWwodXJsLCBudWxsLCByZXF1ZXN0VHlwZSwgc291cmNlLCBub2RlLCByZWZlcnJlciwgb25VcmxNb2RpZmllZEZuLCAocmVxdWVzdElkKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmhpc3RvcnlFbnRyeVJlcG9ydGVyLnJlc291cmNlQ29tcGxldGUobm9kZSwgcmVxdWVzdElkLCBcImFsbG93ZWRcIik7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgUHJvY2Vzc2VzIHVybHMgYW5kIHRoZSBwb3N0IGJvZGllcyBhdHRhY2hlZCB0byB0aGVtIC0gY2FsbHMgb3VyIGludGVybmFsIHByb2Nlc3NpbmcgZnVuY3Rpb25cbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsIC0gdGhlIHVybCB0byBiZSBwcm9jZXNzZWRcbiAgICAgKiBAcGFyYW0ge2FueX0gcmVxdWVzdEJvZHkgLSB0aGUgcG9zdCBib2R5IGF0dGFjaGVkIHRvIHRoZSByZXF1ZXN0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlcXVlc3RUeXBlIC0gdGhlIHR5cGUgb2YgcmVxdWVzdCBiZWluZyBwcm9jZXNzZWRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc291cmNlIC0gd2hlcmUgdGhlIHJlcXVlc3Qgb3JpZ2luYXRlZCBmcm9tXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlZmVycmVyIC0gdGhlIHdlYiBsb2NhdGlvbiB3aGVyZSB0aGUgcmVxdWVzdCBvcmlnaW5hdGVkXG4gICAgICogQHBhcmFtIHtGdW5jdGlvbn0gb25VcmxNb2RpZmllZEZuIC0gYSBjYWxsYmFjayB0aGF0IGlzIGludm9rZWQgaWYgdGhlIHVybCBpcyBtb2RpZmllZFxuICAgICAqL1xuICAgIHB1YmxpYyBwcm9jZXNzVVJMQW5kQm9keSh1cmw6IHN0cmluZywgcmVxdWVzdEJvZHk6IGFueSwgcmVxdWVzdFR5cGU6IHN0cmluZywgc291cmNlOiBzdHJpbmcsIHJlZmVycmVyOiBzdHJpbmcsIG9uVXJsTW9kaWZpZWRGbj86IEZ1bmN0aW9uKTogdm9pZCB7XG4gICAgICAgIGNvbnN0IG5vZGUgPSB7fTtcbiAgICAgICAgdGhpcy5wcm9jZXNzVVJMSW50ZXJuYWwodXJsLCByZXF1ZXN0Qm9keSwgcmVxdWVzdFR5cGUsIHNvdXJjZSwgbm9kZSwgcmVmZXJyZXIsIG9uVXJsTW9kaWZpZWRGbiwgKHJlcXVlc3RJZCkgPT4ge1xuICAgICAgICAgICAgdGhpcy5oaXN0b3J5RW50cnlSZXBvcnRlci5yZXNvdXJjZUNvbXBsZXRlKG5vZGUsIHJlcXVlc3RJZCwgXCJhbGxvd2VkXCIpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHVibGljIENoZWNrcyBpZiBhIHVybCBpcyB2b2lkXG4gICAgICogQHBhcmFtIHtzdHJpbmcgfCB1bmRlZmluZWQgfSB1cmwgLSB0aGUgdXJsIHdlJ3JlIGNoZWNraW5nXG4gICAgICovXG4gICAgcHVibGljIGlzVm9pZFVSTCh1cmw6IHN0cmluZyB8IHVuZGVmaW5lZCk6IGJvb2xlYW4ge1xuICAgICAgICBsZXQgdm9pZFVSTCA9IFwiamF2YXNjcmlwdDpcIjsgLy8gY292ZXJzIGJvdGggLy9qYXZhc2NyaXB0OjsgYW5kIGphdmFzY3JpcHQ6dm9pZCgwKVxuICAgICAgICByZXR1cm4gdHlwZW9mICh1cmwpICE9PSBcInN0cmluZ1wiIHx8IHVybCA9PT0gXCJcIiB8fCB1cmwuaW5kZXhPZih2b2lkVVJMKSAhPT0gLTE7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHB1YmxpYyBDaGVja3MgaWYgdGhlIHVybCBpcyB2YWxpZCBhbmQgY2FuIGJlIGNvcnJlY3RseSBwYXJzZWRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsIC0gdGhlIHVybCB3ZSdyZSBjaGVja2luZ1xuICAgICAqIEBwYXJhbSB7c3RyaW5nfSByZWZlcnJlciAtIHRoZSB3ZWIgbG9jYXRpb24gd2hlcmUgdGhlIHJlcXVlc3Qgb3JpZ2luYXRlZFxuICAgICAqL1xuICAgIHB1YmxpYyBpc1ZhbGlkVVJMKHVybDogc3RyaW5nLCByZWZlcnJlcjogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgICAgIC8vIGlmIGludmFsaWQgdXJsLCBmYWlsIGdyYWNlZnVsbHksIHJldHVybiBmYWxzZVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgbmV3IFVybCh1cmwsIHJlZmVycmVyKTtcbiAgICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwdWJsaWMgQXR0YWNoZXMgYW4gZXZlbnQgaGFuZGxlciB3aGljaCBjYWxscyByZXNvdXJjZUNvbXBsZXRlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHRpbWVTdGFtcCAtIHRoZSB0aW1lc3RhbXAgb2YgdGhlIHJlcXVlc3QgcmVwcmVzZW50ZWQgaW4gdW5peCBjb2RlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHN0YXR1cyAtIHRoZSBzdGF0dXMgb2YgdGhlIHJlcXVlc3RcbiAgICAgKi9cbiAgICBwdWJsaWMgY3JlYXRlRXZlbnRIYW5kbGVyKHRpbWVTdGFtcDogbnVtYmVyLCBzdGF0dXM6IHN0cmluZyk6IEZ1bmN0aW9uIHtcbiAgICAgICAgcmV0dXJuICgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuaGlzdG9yeUVudHJ5UmVwb3J0ZXIucmVzb3VyY2VDb21wbGV0ZSh0aGlzLCB0aW1lU3RhbXAsIHN0YXR1cyk7XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByb3RlY3RlZCBPdXIgaW50ZXJuYWwgZnVuY3Rpb24gd2hpY2ggY3JlYXRlcyB0aGUgRW5zUmVxdWVzdCBvYmplY3QgYW5kIGludm9rZXMgdGhlIHByb2Nlc3NpbmdcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsIC0gdGhlIHVybCB0byBiZSBwcm9jZXNzZWRcbiAgICAgKiBAcGFyYW0ge2FueX0gcmVxdWVzdEJvZHkgLSB0aGUgcG9zdCBib2R5IGF0dGFjaGVkIHRvIHRoZSByZXF1ZXN0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlcXVlc3RUeXBlIC0gdGhlIHR5cGUgb2YgcmVxdWVzdCBiZWluZyBwcm9jZXNzZWRcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gc291cmNlIC0gd2hlcmUgdGhlIHJlcXVlc3Qgb3JpZ2luYXRlZCBmcm9tXG4gICAgICogQHBhcmFtIHtOb2RlfSBub2RlIC0gcmVmZXJlbmNlIHRvIHRoZSBlbGVtZW50IG5vZGUgYXNzb2NpYXRlZCB3aXRoIHRoZSByZXF1ZXN0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlZmVycmVyIC0gdGhlIHdlYiBsb2NhdGlvbiB3aGVyZSB0aGUgcmVxdWVzdCBvcmlnaW5hdGVkXG4gICAgICogQHBhcmFtIHtGdW5jdGlvbn0gb25VcmxNb2RpZmllZEZuIC0gYSBjYWxsYmFjayB0aGF0IGlzIGludm9rZWQgaWYgdGhlIHVybCBpcyBtb2RpZmllZFxuICAgICAqIEBwYXJhbSB7RnVuY3Rpb259IG9uQWxsb3dlZGZuIC0gYSBjYWxsYmFjayB0aGF0IGlzIGludm9rZWQgaWYgdGhlIHVybCBpcyBhbGxvd2VkXG4gICAgICovXG4gICAgcHJvdGVjdGVkIHByb2Nlc3NVUkxJbnRlcm5hbCh1cmwsIHJlcXVlc3RCb2R5LCByZXF1ZXN0VHlwZSwgc291cmNlLCBub2RlLCByZWZlcnJlciwgb25VcmxNb2RpZmllZEZuLCBvbkFsbG93ZWRmbik6IHN0cmluZyB7XG4gICAgICAgIGxldCBwcm9jZXNzZWRVcmwgPSB1cmw7XG4gICAgICAgIGlmICh0aGlzLmlzVm9pZFVSTChwcm9jZXNzZWRVcmwpIHx8ICF0aGlzLmlzVmFsaWRVUkwocHJvY2Vzc2VkVXJsLCByZWZlcnJlcikpIHtcbiAgICAgICAgICAgIHJldHVybiBwcm9jZXNzZWRVcmw7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgdGltZVN0YW1wID0gK25ldyBEYXRlKCk7XG4gICAgICAgIGNvbnN0IHJlcXVlc3QgPSBuZXcgRW5zUmVxdWVzdChcIlwiKTtcbiAgICAgICAgcmVxdWVzdC5pZCA9IHRpbWVTdGFtcDtcbiAgICAgICAgcmVxdWVzdC5zb3VyY2UgPSBzb3VyY2U7XG4gICAgICAgIHJlcXVlc3QudHlwZSA9IHJlcXVlc3RUeXBlO1xuICAgICAgICByZXF1ZXN0LmRlc3RpbmF0aW9uID0gbmV3IFVybChwcm9jZXNzZWRVcmwsIHJlZmVycmVyKS5ocmVmO1xuXG4gICAgICAgIGlmICghdGhpcy5vbkJlZm9yZShyZXF1ZXN0LCByZXF1ZXN0Qm9keSwgbm9kZSwgcmVmZXJyZXIpKSB7XG4gICAgICAgICAgICBjb25zdCBtb2RpZmllZFVybCA9IHRoaXMuZ2V0QmxvY2tlZFNyY1N0cmluZyhyZXF1ZXN0LnR5cGUsIHJlcXVlc3QpO1xuICAgICAgICAgICAgY29uc3QgbW9kaWZpZWRSZXF1ZXN0Qm9keSA9IHRoaXMuZ2V0QmxvY2tlZFJlcXVlc3RCb2R5KHJlcXVlc3QsIHJlcXVlc3RCb2R5KTtcbiAgICAgICAgICAgIGlmICh1dGlsaXRpZXMuZ2V0RW52aXJvbm1lbnRPdmVycmlkZU9yR2xvYmFsRGVmYXVsdCh0aGlzLmdhdGV3YXlEYXRhLmZlYXR1cmVUb2dnbGVzLCB0aGlzLmVudmlyb25tZW50LCBcImVuYWJsZUZpbHRlcmluZ1wiKSkge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2Ygb25VcmxNb2RpZmllZEZuID09PSBcImZ1bmN0aW9uXCIgJiYgb25VcmxNb2RpZmllZEZuICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgb25VcmxNb2RpZmllZEZuKG1vZGlmaWVkVXJsLCBtb2RpZmllZFJlcXVlc3RCb2R5KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcHJvY2Vzc2VkVXJsID0gbW9kaWZpZWRVcmw7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnNlbmRCbG9ja1JlcG9ydChyZXF1ZXN0LCBub2RlLCByZWZlcnJlcik7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICBvbkFsbG93ZWRmbihyZXF1ZXN0LmlkKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcHJvY2Vzc2VkVXJsO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEBwcm90ZWN0ZWQgRGV0ZXJtaW5lcyB0aGUgc3RhdHVzIG9mIGEgYmxvY2tlZCByZXF1ZXN0IGFuZCBpbnZva2VzIHJlc291cmNlQ29tcGxldGUgdG8gc2VuZCBhIGJsb2NrZWQgcmVwb3J0XG4gICAgICogQHBhcmFtIHtFbnNSZXF1ZXN0fSByZXF1ZXN0IC0gdGhlIHJlcXVlc3QgYmVpbmcgY2hlY2tlZFxuICAgICAqIEBwYXJhbSB7Tm9kZX0gbm9kZSAtIHRoZSBlbGVtZW50IG5vZGUgYXNzb2NpYXRlZCB3aXRoIHRoZSByZXF1ZXN0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlZmVycmVyIC0gdGhlIHdlYiBsb2NhdGlvbiB3aGVyZSB0aGUgcmVxdWVzdCBvcmlnaW5hdGVkXG4gICAgICovXG4gICAgcHJvdGVjdGVkIHNlbmRCbG9ja1JlcG9ydChyZXF1ZXN0OiBFbnNSZXF1ZXN0LCBub2RlOiBOb2RlLCByZWZlcnJlcjogc3RyaW5nKTogdm9pZCB7XG4gICAgICAgIGxldCBzdGF0dXMgPSBcImJsb2NrZWRcIjtcblxuICAgICAgICBpZiAodGhpcy5kYXRhR292ZXJuYW5jZU1hbmFnZXIuc2hvdWxkTWFza1VybChyZXF1ZXN0KSkge1xuICAgICAgICAgICAgc3RhdHVzID0gXCJtYXNrZWRcIjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLmRhdGFHb3Zlcm5hbmNlTWFuYWdlci5zaG91bGRSZWRhY3RVcmwocmVxdWVzdCkpIHtcbiAgICAgICAgICAgIHN0YXR1cyA9IFwicmVkYWN0XCI7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5zc2xNYW5hZ2VyLnNob3VsZFJld3JpdGVQcm90b2NvbChyZXF1ZXN0LCByZWZlcnJlcikpIHtcbiAgICAgICAgICAgIHN0YXR1cyA9IFwicmV3cml0ZVwiO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5oaXN0b3J5RW50cnlSZXBvcnRlci5yZXNvdXJjZUNvbXBsZXRlKG5vZGUsIHJlcXVlc3QuaWQsIHN0YXR1cyk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgSW52b2tlcyB0aGUgaGlzdG9yeSBlbnRyeSByZXBvcnRlciB0byBjcmVhdGUgYW4gaW5pdGlhbCBlbnRyeSBmb3IgdGhlIHJlcXVlc3QsIHRoZW4gY2FsbHMgYWxsb3dSZXF1ZXN0IHRvIGtpY2sgb2ZmIHByb2Nlc3NpbmdcbiAgICAgKiBAcGFyYW0ge0Vuc1JlcXVlc3R9IHJlcXVlc3QgLSB0aGUgcmVxdWVzdCB0byBiZSBwcm9jZXNzZWRcbiAgICAgKiBAcGFyYW0ge2FueX0gcmVxdWVzdEJvZHkgLSB0aGUgcG9zdCBib2R5IGFzc29jaWF0ZWQgd2l0aCB0aGUgcmVxdWVzdFxuICAgICAqIEBwYXJhbSB7Tm9kZX0gbm9kZSAtIHRoZSBlbGVtZW50IG5vZGUgYXNzb2NpYXRlZCB3aXRoIHRoZSByZXF1ZXN0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlZmVycmVyIC0gdGhlIHdlYiBsb2NhdGlvbiB3aGVyZSB0aGUgcmVxdWVzdCBvcmlnaW5hdGVkXG4gICAgICovXG4gICAgcHJpdmF0ZSBvbkJlZm9yZShyZXF1ZXN0OiBFbnNSZXF1ZXN0LCByZXF1ZXN0Qm9keTogYW55LCBub2RlOiBOb2RlLCByZWZlcnJlcjogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgICAgIC8vY2hlY2tzIHRvIHNlZSBpZiB0aGUgaGlzdG9yeSBvYmplY3QgaGFzIGF3YXJlbmVzcyBvZiB0aGUgbmV3IG5vZGUsIGlmIG5vdCBpdCBjcmVhdGVzIGEgbmV3IGhpc3RvcnkgZW50cnkgYW5kIHN0b3JlcyBpcyBhcyB0aGUgbGFzdCByZXF1ZXN0XG4gICAgICAgIHRoaXMuaGlzdG9yeUVudHJ5UmVwb3J0ZXIuY3JlYXRlSGlzdG9yeUVudHJ5KHJlcXVlc3QsIG5vZGUpO1xuICAgICAgICByZXR1cm4gdGhpcy5hbGxvd1JlcXVlc3QocmVxdWVzdCwgcmVxdWVzdEJvZHksIHJlZmVycmVyKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZSBJbnZva2VzIG91ciBtYW5hZ2VyIGNsYXNzZXMgdG8gY2hlY2sgaWYgYSByZXF1ZXN0IHNob3VsZCBiZSBhbGxvd2VkIG9yIG5vdFxuICAgICAqIEBwYXJhbSB7RW5zUmVxdWVzdH0gcmVxdWVzdCAtIHRoZSByZXF1ZXN0IHRvIGJlIHByb2Nlc3NlZFxuICAgICAqIEBwYXJhbSB7YW55fSByZXF1ZXN0Qm9keSAtIHRoZSBwb3N0IGJvZHkgYXNzb2NpYXRlZCB3aXRoIHRoZSByZXF1ZXN0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHJlZmVycmVyIC0gdGhlIHdlYiBsb2NhdGlvbiB3aGVyZSB0aGUgcmVxdWVzdCBvcmlnaW5hdGVkXG4gICAgICovXG4gICAgcHJpdmF0ZSBhbGxvd1JlcXVlc3QocmVxdWVzdDogRW5zUmVxdWVzdCwgcmVxdWVzdEJvZHk6IGFueSwgcmVmZXJyZXI6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgICAgICBsZXQgc2hvdWxkQWxsb3dSZXF1ZXN0ID0gdHJ1ZTtcbiAgICAgICAgY29uc3QgdXJsID0gcmVxdWVzdC5kZXN0aW5hdGlvbixcbiAgICAgICAgICAgIHVybFRvUGFyc2UgPSBuZXcgVXJsKHVybCwgcmVmZXJyZXIpLFxuICAgICAgICAgICAgbG9jYXRpb25Qcm90b2NvbCA9IG5ldyBVcmwocmVmZXJyZXIpLnByb3RvY29sLFxuICAgICAgICAgICAgaG9zdE5hbWUgPSB1cmxUb1BhcnNlLmhvc3RuYW1lLFxuICAgICAgICAgICAgcGF0aE5hbWUgPSB1cmxUb1BhcnNlLnBhdGhuYW1lLFxuICAgICAgICAgICAgcHJvdG9jb2wgPSB1cmxUb1BhcnNlLnByb3RvY29sO1xuICAgICAgICAvLyBhdXRvIGFsbG93IGRhdGEgcHJvdG9jb2xzIG9mIGltYWdlcyB0byBwcm90ZWN0IGFnYWluc3QgWFNTXG4gICAgICAgIC8vIGNhbiBhbGxvdyBkYXRhIHByb3RvY29scyBmb3IgamF2YXNjcmlwdCBpZiBtb25pdG9yaW5nIGlzIGRpc2FibGVkXG4gICAgICAgIGNvbnN0IHByb3RvY29sQWxsb3dlZCA9IHRoaXMucHJvdG9NYW5hZ2VyLmFsbG93UHJvdG9jb2xBbmRCeXBhc3NQcm9jZXNzaW5nKHByb3RvY29sLCByZXF1ZXN0LCB0aGlzLmdhdGV3YXlEYXRhLmZlYXR1cmVUb2dnbGVzKTtcbiAgICAgICAgaWYgKHByb3RvY29sQWxsb3dlZCkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5wcm90b01hbmFnZXIuaXNCbG9ja2VkUmVxdWVzdEJ5cGFzc2VkKHByb3RvY29sKSkge1xuICAgICAgICAgICAgLy8gY3VycmVudGx5LCB3ZSdyZSBieXBhc3NpbmcgZnVydGhlciBwcm9jZXNzaW5nIG9uIGJsb2NrZWQgYnJvd3NlciBleHRlbnNpb24gcHJvdG9jb2xzXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgbGV0IFNTTENoZWNrUGFzc2VzID0gdHJ1ZTtcbiAgICAgICAgaWYgKHVybCAhPT0gXCJcIikge1xuICAgICAgICAgICAgU1NMQ2hlY2tQYXNzZXMgPSB0aGlzLnNzbE1hbmFnZXIuYWxsb3dSZXF1ZXN0KGxvY2F0aW9uUHJvdG9jb2wsIHByb3RvY29sLCByZXF1ZXN0KTtcbiAgICAgICAgfVxuICAgICAgICAvL2NoZWNrcyB0byBpZiB0aGUgaG9zdCBpcyBpbiB0aGUgd2hpdGVsaXN0IGFuZCB0aGF0IGlzIHVzaW5nIEhUVFAgaWYgcmVxdWlyZWRcbiAgICAgICAgbGV0IHRhcmdldExpc3RDaGVja1Bhc3NlcyA9IHRydWU7XG4gICAgICAgIC8vY2hlY2tzIG1ha2Ugc3VyZSBub3QgaWxsZWdhbCBwYXJhbXMgYXJlIGJlaW5nIHBhc3NlZFxuICAgICAgICBsZXQgZGF0YU1vbml0b3JpbmdDaGVja1Bhc3NlcyA9IHRydWU7XG5cbiAgICAgICAgZGF0YU1vbml0b3JpbmdDaGVja1Bhc3NlcyA9IHRoaXMuZGF0YUdvdmVybmFuY2VNYW5hZ2VyLmFsbG93UmVxdWVzdChyZXF1ZXN0LCBob3N0TmFtZSwgcmVxdWVzdEJvZHkpO1xuICAgICAgICBpZiAodGhpcy5lbnZpcm9ubWVudCkge1xuICAgICAgICAgICAgdGFyZ2V0TGlzdENoZWNrUGFzc2VzID0gdGhpcy5saXN0UHJvY2Vzc29yLmFsbG93UmVxdWVzdChob3N0TmFtZSwgcmVxdWVzdCwgcGF0aE5hbWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRhcmdldExpc3RDaGVja1Bhc3NlcyA9PT0gZmFsc2UgfHxcbiAgICAgICAgICAgIFNTTENoZWNrUGFzc2VzID09PSBmYWxzZSB8fFxuICAgICAgICAgICAgZGF0YU1vbml0b3JpbmdDaGVja1Bhc3NlcyA9PT0gZmFsc2UpIHtcblxuICAgICAgICAgICAgc2hvdWxkQWxsb3dSZXF1ZXN0ID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHNob3VsZEFsbG93UmVxdWVzdDtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAcHJpdmF0ZSBSZXRyaWV2ZXMgdGhlIGJsb2NrZWQgc3JjIHN0cmluZyBmb3IgYSByZXF1ZXN0IGJhc2VkIG9uIGl0cyB0YWdOYW1lIGFuZCBydWxlcyBzZXQgaW4gdGhlIGZlYXR1cmVUb2dnbGVzXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHRhZ05hbWUgLSB0aGUgdHlwZSBvZiB0YWcgYmVpbmcgY2hlY2tlZFxuICAgICAqIEBwYXJhbSB7RW5zUmVxdWVzdH0gcmVxdWVzdCAtIHRoZSByZXF1ZXN0IGJlaW5nIGNoZWNrZWRcbiAgICAgKi9cbiAgICBwcml2YXRlIGdldEJsb2NrZWRTcmNTdHJpbmcodGFnTmFtZTogc3RyaW5nLCByZXF1ZXN0OiBFbnNSZXF1ZXN0KTogc3RyaW5nIHtcbiAgICAgICAgbGV0IGJsb2NrZWRTcmNTdHJpbmcgPSByZXF1ZXN0LmRlc3RpbmF0aW9uO1xuICAgICAgICBsZXQgZ2V0UGVyVGFnQmxvY2tTdHJpbmcgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAodGFnTmFtZSA9PT0gXCJpbWdcIiB8fCB0YWdOYW1lID09PSBcImltYWdlXCIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gXCJcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBcIi8vamF2YXNjcmlwdDo7XCI7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgaWYgKHRoaXMuc3NsTWFuYWdlci5zaG91bGRSZXdyaXRlUHJvdG9jb2wocmVxdWVzdCkpIHtcbiAgICAgICAgICAgIGJsb2NrZWRTcmNTdHJpbmcgPSB0aGlzLnNzbE1hbmFnZXIucmV3cml0ZVByb3RvY29sKHJlcXVlc3QuZGVzdGluYXRpb24pO1xuICAgICAgICAgICAgaWYgKHJlcXVlc3QucmVhc29ucy5sZW5ndGggPT09IDEpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gYmxvY2tlZFNyY1N0cmluZztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0Lmhhc1JlYXNvbihcIlNTTFwiKSkge1xuICAgICAgICAgICAgcmV0dXJuIGdldFBlclRhZ0Jsb2NrU3RyaW5nKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5kYXRhR292ZXJuYW5jZU1hbmFnZXIuc2hvdWxkTWFza1VybChyZXF1ZXN0KSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGF0YUdvdmVybmFuY2VNYW5hZ2VyLmdldE1hc2tlZFVybChibG9ja2VkU3JjU3RyaW5nKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmRhdGFHb3Zlcm5hbmNlTWFuYWdlci5zaG91bGRSZWRhY3RVcmwocmVxdWVzdCkpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRhdGFHb3Zlcm5hbmNlTWFuYWdlci5nZXRSZWRhY3RlZFVybChibG9ja2VkU3JjU3RyaW5nKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBnZXRQZXJUYWdCbG9ja1N0cmluZygpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQHByaXZhdGUgUmV0cmlldmVzIHRoZSBibG9ja2VkIHJlcXVlc3QgYm9keSBhbmQgY2hlY2tzIGFnYWluc3Qgb3VyIGRhdGEgZ292ZXJuYW5jZSBtYW5hZ2VyXG4gICAgICogQHBhcmFtIHtFbnNSZXF1ZXN0fSByZXF1ZXN0IC0gdGhlIHJlcXVlc3QgdG8gYmUgY2hlY2tlZFxuICAgICAqIEBwYXJhbSB7YW55fSByZXF1ZXN0Qm9keSAtIHRoZSBwb3N0IGJvZHkgYXNzb2NpYXRlZCB3aXRoIHRoZSByZXF1ZXN0XG4gICAgICovXG4gICAgcHJpdmF0ZSBnZXRCbG9ja2VkUmVxdWVzdEJvZHkocmVxdWVzdDogRW5zUmVxdWVzdCwgcmVxdWVzdEJvZHk6IGFueSk6IHN0cmluZyB7XG4gICAgICAgIGlmICh0aGlzLmRhdGFHb3Zlcm5hbmNlTWFuYWdlci5zaG91bGRNYXNrRGF0YShyZXF1ZXN0KSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGF0YUdvdmVybmFuY2VNYW5hZ2VyLmdldE1hc2tlZERhdGEocmVxdWVzdEJvZHkpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHRoaXMuZGF0YUdvdmVybmFuY2VNYW5hZ2VyLnNob3VsZFJlZGFjdERhdGEocmVxdWVzdCkpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRhdGFHb3Zlcm5hbmNlTWFuYWdlci5nZXRSZWRhY3RlZERhdGEocmVxdWVzdEJvZHkpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIHJlcXVlc3RCb2R5O1xuICAgICAgICB9XG4gICAgfVxufTtcbiIsImltcG9ydCB7IFVSTCBhcyBVcmwgfSBmcm9tICcuL3VybFBvbnlGaWxsJztcblxuZXhwb3J0IGNvbnN0IHV0aWxpdGllcyA9IChmdW5jdGlvbiAoKSB7XG5cbiAgICByZXR1cm4ge1xuICAgICAgICAvKipcbiAgICAgICAgICogam9pbnMgYW4gYXJyYXkgb2Ygc3RyaW5ncyBpbiB0byBhIFJlZ0V4cC4gaWYgdGhlIGFycmF5IGlzIGVtcHR5IHJldHVybnMgYW4gdW4tbWF0Y2hhYmxlIFJlZ0V4cFxuICAgICAgICAgKiBAcGFyYW0gYXJyXG4gICAgICAgICAqIEBwYXJhbSBmbGFncyAtIEZsYWdzIHRvIGFkZCB0byByZWdleFxuICAgICAgICAgKiBAcmV0dXJucyB7UmVnRXhwfVxuICAgICAgICAgKi9cbiAgICAgICAgLy8gc2V0dGluZyB1bmRlZmluZWQgYXMgZGVmYXVsdCB2YWx1ZSBwcmV2ZW50cyBUUyBlcnJvcnMgd2hlbiB3ZSBjYWxsIHRoaXMgZnVuY3Rpb24gd2l0aCBvbmUgYXJndW1lbnRcbiAgICAgICAgYXJyYXlUb1JlZ2V4OiBmdW5jdGlvbiAoYXJyLCBmbGFncyA9IHVuZGVmaW5lZCk6IFJlZ0V4cCB7XG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShhcnIpKSB7XG4gICAgICAgICAgICAgICAgaWYgKGFyciAmJiBhcnIubGVuZ3RoID4gMCAmJiBhcnIuam9pbikge1xuXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgUmVnRXhwKGFyci5qb2luKFwifFwiKSwgZmxhZ3MpO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgUmVnRXhwKFwiYV5cIiwgZmxhZ3MpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0sXG5cbiAgICAgICAgZWFjaDogZnVuY3Rpb24gKGxpc3QsIGZuLCBjb250ZXh0Pyk6IHZvaWQge1xuICAgICAgICAgICAgaWYgKGxpc3QgPT09IHVuZGVmaW5lZCB8fCAhKFwibGVuZ3RoXCIgaW4gbGlzdCkpIHtcbiAgICAgICAgICAgICAgICBsaXN0ID0ge1xuICAgICAgICAgICAgICAgICAgICBsZW5ndGg6IDBcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgZm4uY2FsbChjb250ZXh0LCBsaXN0W2ldLCBpLCBsaXN0KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpc1VyaUVuY29kZWQ6IGZ1bmN0aW9uKHVyaSk6IGJvb2xlYW4gfCBzdHJpbmcge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiB1cmkgPT09IFwic3RyaW5nXCIpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdXJpICE9PSBkZWNvZGVVUklDb21wb25lbnQodXJpKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfSxcblxuICAgICAgICBpc1ZhbGlkVXJsOiBmdW5jdGlvbihzdHJpbmcpOiBib29sZWFuIHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgbmV3IFVybChzdHJpbmcpO1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfSBjYXRjaCAoXykge1xuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcblxuICAgICAgICBpc1doaXRlbGlzdDogZnVuY3Rpb24gKGdhdGV3YXlEYXRhLCBlbnZpcm9ubWVudCk6IGJvb2xlYW4ge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0RW52aXJvbm1lbnRPdmVycmlkZU9yR2xvYmFsRGVmYXVsdChnYXRld2F5RGF0YS5mZWF0dXJlVG9nZ2xlcywgZW52aXJvbm1lbnQsIFwidXNlc1doaXRlbGlzdFwiKTtcbiAgICAgICAgfSxcblxuICAgICAgICBkZWNvZGVVcmlDb21wb25lbnQ6IGZ1bmN0aW9uKHN0cikge1xuICAgICAgICAgICAgdmFyIGRlY29kZWRVcmkgPSBzdHI7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2Ygc3RyID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICAgICAgICAgIHdoaWxlICh0aGlzLmlzVXJpRW5jb2RlZChkZWNvZGVkVXJpKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGVjb2RlZFVyaSA9IGRlY29kZVVSSUNvbXBvbmVudChkZWNvZGVkVXJpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gY2F0Y2goZSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihcIlVSSTogXCIgKyBzdHIgKyBcIi4gQ291bGQgbm90IGJlIGRlY29kZWQgcHJvcGVybHkuIE9yaWdpbmFsIHN0cmluZyB3YXMgcmV0dXJuZWQuXCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGRlY29kZWRVcmk7XG4gICAgICAgIH0sXG5cbiAgICAgICAgZW5jb2RlVXJpQ29tcG9uZW50OiBmdW5jdGlvbihzdHIpOiBhbnkge1xuICAgICAgICAgICAgdmFyIGVuY29kZWRVcmkgPSBzdHI7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2Ygc3RyID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICAgICAgICAgIGVuY29kZWRVcmkgPSBlbmNvZGVVUklDb21wb25lbnQoc3RyKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGNhdGNoKGUpIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oXCJVUkk6IFwiICsgc3RyICsgXCIuIENvdWxkIG5vdCBiZSBlbmNvZGVkIHByb3Blcmx5LiBPcmlnaW5hbCBzdHJpbmcgd2FzIHJldHVybmVkLlwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBlbmNvZGVkVXJpO1xuICAgICAgICB9LFxuXG4gICAgICAgIHJlcGVhdFN0cmluZzogZnVuY3Rpb24ocywgbik6IHN0cmluZyB7XG4gICAgICAgICAgICB2YXIgYT1bXSxpPTA7XG5cbiAgICAgICAgICAgIGZvcig7aTxuOylcbiAgICAgICAgICAgICAgICBhW2krK109cztcblxuICAgICAgICAgICAgcmV0dXJuIGEuam9pbihcIlwiKTtcbiAgICAgICAgfSxcblxuICAgICAgICBnZXRFbnZpcm9ubWVudE92ZXJyaWRlT3JHbG9iYWxEZWZhdWx0OiBmdW5jdGlvbihmZWF0dXJlVG9nZ2xlcywgZW52aXJvbm1lbnQsIHByb3BlcnR5KTogYW55IHtcbiAgICAgICAgICAgIGlmIChlbnZpcm9ubWVudCAmJiBlbnZpcm9ubWVudC5oYXNPd25Qcm9wZXJ0eShwcm9wZXJ0eSkpIHsgLy8gdHNpbnQtZGlzYWJsZS1saW5lIG5vLXByb3RvdHlwZS1idWlsdGluc1xuICAgICAgICAgICAgICAgIHJldHVybiBlbnZpcm9ubWVudFtwcm9wZXJ0eV07XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBmZWF0dXJlVG9nZ2xlc1twcm9wZXJ0eV07XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgfTtcbn0pKCk7XG4iLCJ2YXIgZztcblxuLy8gVGhpcyB3b3JrcyBpbiBub24tc3RyaWN0IG1vZGVcbmcgPSAoZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzO1xufSkoKTtcblxudHJ5IHtcblx0Ly8gVGhpcyB3b3JrcyBpZiBldmFsIGlzIGFsbG93ZWQgKHNlZSBDU1ApXG5cdGcgPSBnIHx8IG5ldyBGdW5jdGlvbihcInJldHVybiB0aGlzXCIpKCk7XG59IGNhdGNoIChlKSB7XG5cdC8vIFRoaXMgd29ya3MgaWYgdGhlIHdpbmRvdyByZWZlcmVuY2UgaXMgYXZhaWxhYmxlXG5cdGlmICh0eXBlb2Ygd2luZG93ID09PSBcIm9iamVjdFwiKSBnID0gd2luZG93O1xufVxuXG4vLyBnIGNhbiBzdGlsbCBiZSB1bmRlZmluZWQsIGJ1dCBub3RoaW5nIHRvIGRvIGFib3V0IGl0Li4uXG4vLyBXZSByZXR1cm4gdW5kZWZpbmVkLCBpbnN0ZWFkIG9mIG5vdGhpbmcgaGVyZSwgc28gaXQnc1xuLy8gZWFzaWVyIHRvIGhhbmRsZSB0aGlzIGNhc2UuIGlmKCFnbG9iYWwpIHsgLi4ufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGc7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuXHRpZiAoIW1vZHVsZS53ZWJwYWNrUG9seWZpbGwpIHtcblx0XHRtb2R1bGUuZGVwcmVjYXRlID0gZnVuY3Rpb24oKSB7fTtcblx0XHRtb2R1bGUucGF0aHMgPSBbXTtcblx0XHQvLyBtb2R1bGUucGFyZW50ID0gdW5kZWZpbmVkIGJ5IGRlZmF1bHRcblx0XHRpZiAoIW1vZHVsZS5jaGlsZHJlbikgbW9kdWxlLmNoaWxkcmVuID0gW107XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG1vZHVsZSwgXCJsb2FkZWRcIiwge1xuXHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcblx0XHRcdGdldDogZnVuY3Rpb24oKSB7XG5cdFx0XHRcdHJldHVybiBtb2R1bGUubDtcblx0XHRcdH1cblx0XHR9KTtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobW9kdWxlLCBcImlkXCIsIHtcblx0XHRcdGVudW1lcmFibGU6IHRydWUsXG5cdFx0XHRnZXQ6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gbW9kdWxlLmk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0bW9kdWxlLndlYnBhY2tQb2x5ZmlsbCA9IDE7XG5cdH1cblx0cmV0dXJuIG1vZHVsZTtcbn07XG4iLCJzZWxmID0ge307XG5sb2NhdGlvbiA9IHtcbiAgaG9zdG5hbWU6IFwibmV4dXMuZW5zaWdodGVuLmNvbVwiXG59O1xubW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKCcuL25vZGVfbW9kdWxlcy9hdG9iL25vZGUtYXRvYicpLmF0b2I7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4vbm9kZV9tb2R1bGVzL3VybC1wcm9jZXNzb3IvbGliL2luZGV4JykuVXJsUHJvY2Vzc29yOyJdLCJzb3VyY2VSb290IjoiIn0=
//# sourceMappingURL=privacyUrlProcessor.js.map