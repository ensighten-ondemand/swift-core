//
//  EnsightenObjCMessage.h
//  EnsightenSwift
//
//  Created by Miles Alden on 9/9/16.
//  Copyright © 2017 Ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

@interface ESWObjcMessage : NSObject

- (id _Nullable)sendObjcMessage:(id _Nullable )target message:(NSString *_Nullable)messageName args:(NSArray *_Nullable)args isClassMethod:(BOOL)isClass;
- (id _Nullable)ESWValueForKey:(NSString * _Nullable)key onTarget:(id _Nullable)target error:(__autoreleasing NSError *_Nullable* _Nullable )error;
- (id _Nullable)ESWPerformSelector:(NSString * _Nullable)sel onTarget:(id _Nullable)target error:(__autoreleasing NSError *_Nullable*_Nullable)error;
- (id _Nullable)ESWGetIvar:(NSString *_Nullable)key onTarget:(id _Nullable)target error:(__autoreleasing NSError *_Nullable*_Nullable)error;
- (id _Nullable)ESWSetValue:(id _Nullable)value forKey:(NSString * _Nullable)key onTarget:(id _Nullable)target error:(__autoreleasing NSError *_Nullable*_Nullable)error;
- (id _Nullable)ESWSetValue:(id _Nullable)value forIvar:(NSString * _Nullable)key onTarget:(id _Nullable)target error:(__autoreleasing NSError *_Nullable*_Nullable)error;
- (BOOL)ESWIsValidIvar:(NSString *_Nullable)key onTarget:(id _Nullable)target;
- (id _Nullable)ESWGetInvocationReturnValue:(id _Nullable)inv error:(__autoreleasing NSError *_Nullable*_Nullable)error;

+ (nullable id)objcNilWrapper:(inout id _Nullable )anything;
+ (NSString *_Nullable)isAnyNil:(void *_Nullable)ptr;
//NSValue *ptrToValue (void *val);

@end
