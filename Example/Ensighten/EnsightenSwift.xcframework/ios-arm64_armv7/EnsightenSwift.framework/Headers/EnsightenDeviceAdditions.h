//
//  UIDevice+EnsightenDeviceAdditions.h
//  EnsightenSwift
//
//  Created by Miles Alden on 11/1/16.
//  Copyright © 2017 Ensighten. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (EnsightenDeviceAdditions)

+ (NSString *)ESWModel;
+ (NSString *)ESWDeviceName;
+ (NSString *)ESWMachineType;
+ (NSString *)ESWMemSize;
+ (NSString *)ESWCpuType;

@end
