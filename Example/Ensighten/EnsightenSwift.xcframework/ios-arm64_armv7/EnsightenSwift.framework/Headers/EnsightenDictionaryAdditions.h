//
//  NSDictionary+SettingsDictionaries.h
//  Ensighten
//
//  Created by Miles Alden on 8/7/12.
//
//

#import <Foundation/Foundation.h>

#define NSTNNoKey @"-111"

@interface NSDictionary (SettingsDictionaries)

- (BOOL)ESWContainsKey: (NSString *)key;


@end
