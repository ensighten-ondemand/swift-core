//
//  NSDate+EnsightenDateAdditions.h
//  EnsightenSwift
//
//  Created by Miles Alden on 11/1/16.
//  Copyright © 2017 Ensighten. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (EnsightenDateAdditions)

+ (NSString *)ESWHour;
+ (NSString *)ESWMinute;
+ (NSString *)ESWSecond;
+ (NSString *)ESWDay;
+ (NSString *)ESWWeekday;
+ (NSString *)ESWWeekdayGregorian;
+ (NSInteger)ESWDaysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;
@end
