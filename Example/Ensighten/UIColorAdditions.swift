//
//  UIColorAdditions.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 5/28/19.
//  Copyright © 2019 Ensighten. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var orangePrimary: UIColor {
        return #colorLiteral(red: 1, green: 0.6549019608, blue: 0.1490196078, alpha: 1)
    }
    
    class var orangeDark: UIColor {
        return #colorLiteral(red: 0.937254902, green: 0.4235294118, blue: 0, alpha: 1)
    }
    
    class var bluePrimary: UIColor {
        return #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
    }
}
