//
//  DoubleAdditions.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 6/22/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import Foundation

extension Double {
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return (self * multiplier).rounded() / multiplier
    }
}
