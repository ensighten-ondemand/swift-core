//
//  LargeTextThemeUIManager.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 5/28/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import EnsightenSwift

class LargeTextThemeUIManager: EnsightenPrivacyUIManager {
    
    // Modal
    func privacyModalTitleTextFont() -> UIFont {
        return .systemFont(ofSize: 30)
    }
    
    func privacyModalDescriptionTextFont() -> UIFont {
        return .systemFont(ofSize: 20)
    }
    
    // Categories
    func privacyModalCategoryTitleTextFont() -> UIFont {
        return .systemFont(ofSize: 24)
    }
    
    func privacyModalCategoryDescriptionTextFont() -> UIFont {
        return .systemFont(ofSize: 20)
    }
    
    func privacyModalSwitchOnTintColor() -> UIColor {
        return .bluePrimary
    }
    
    // Save button
    func privacyModalSaveButtonTextFont() -> UIFont {
        return .systemFont(ofSize: 20)
    }
    
    func privacyModalSaveButtonTextColor() -> UIColor {
        return .bluePrimary
    }
    
    func privacyModalSaveButtonCornerRadius() -> CGFloat {
        return 5
    }
    
    func privacyModalSaveButtonBorderWidth() -> CGFloat {
        return 1
    }
    
    func privacyModalSaveButtonBorderColor() -> UIColor {
        return .bluePrimary
    }
    
    // Cancel Button
    func privacyModalCancelButtonTextFont() -> UIFont {
        return .systemFont(ofSize: 20)
    }
    
    func privacyModalCancelButtonCornerRadius() -> CGFloat {
        return 5
    }
    
    func privacyModalCancelButtonBorderColor() -> UIColor {
        return .bluePrimary
    }
    
    func privacyModalCancelButtonTextColor() -> UIColor {
        return .bluePrimary
    }
    
    
    func privacyModalCancelButtonBorderWidth() -> CGFloat {
        return 1
    }
    
}

