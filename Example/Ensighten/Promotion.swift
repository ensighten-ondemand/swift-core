//
//  Promotion.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class Promotion: Item {
    
    var largeImage: UIImage?
    
    init(promotionInfo: [String : Any]) {
        super.init(info: promotionInfo)
        if let largeImageName = promotionInfo["largeImage"] as? String {
            largeImage = UIImage(named: largeImageName)
        }
    }
}
