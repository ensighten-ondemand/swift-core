//
//  ThemeManager.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 5/28/19.
//  Copyright © 2019 Ensighten. All rights reserved.
//

import UIKit
import EnsightenSwift

class ThemeManager: NSObject {
    
    static let shared = ThemeManager()
    private let key = "CurrentTheme"
    
    var currentTheme: Theme = .custom
    
    func clearCustomViews() {
        Ensighten.privacyManager.setCustomBanner(banner: nil)
        Ensighten.privacyManager.setCustomModal(modal: nil)
    }
    
    func setCurrentTheme(_ theme: Theme) {
        currentTheme = theme
        switch theme {
        case .dark:
            clearCustomViews()
            Ensighten.privacyManager.privacyUIManager = DarkThemeUIManager()
        case .largeText:
            clearCustomViews()
            Ensighten.privacyManager.privacyUIManager = LargeTextThemeUIManager()
        case .custom:
            let banner: CustomPrivacyBanner = .fromNib()
            let modal: CustomPrivacyModal = .fromNib()
            Ensighten.privacyManager.setCustomBanner(banner: banner)
            Ensighten.privacyManager.setCustomModal(modal: modal)
        default:
            clearCustomViews()
            Ensighten.privacyManager.privacyUIManager = nil
            Ensighten.privacyManager.darkPrivacyUIManager = DarkThemeUIManager()
        }
    }
    
    
}

enum Theme: String {
    case dark = "Dark"
    case largeText = "Large Text"
    case standard = "Default"
    case custom = "Custom"
}
