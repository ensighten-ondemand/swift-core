//
//  CMPPreferences.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 4/26/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class CMPPreferences: NSObject {
    
    static var preferences: [[String : String]] = [
        ["key" : "IABConsent_CMPPresent", "name" : "CMP Present"],
        ["key" : "IABConsent_SubjectToGDPR", "name" : "Subject to GDPR"],
        ["key" : "IABConsent_ConsentString", "name" : "Consent String"],
        ["key" : "IABConsent_ParsedPurposeConsents", "name" : "Parsed Purpose Consents"],
        ["key" : "IABConsent_ParsedVendorConsents", "name" : "Parsed Vendor Consents"]
    ]

}
