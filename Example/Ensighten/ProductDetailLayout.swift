//
//  ProductDetailLayout.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 5/1/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class ProductDetailLayout: UICollectionViewFlowLayout {
    override func prepare() {
        super.prepare()
        self.scrollDirection = UICollectionView.ScrollDirection.horizontal
        sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20)
        minimumInteritemSpacing = 20.0
        minimumLineSpacing = 20.0
        let itemHeight = collectionViewContentSize.height - 30
        let itemWidth  = itemHeight
        itemSize = CGSize(width: itemWidth, height: itemHeight)
        headerReferenceSize = CGSize.zero
    }
}
