//
//  CustomPrivacyModal.swift
//  Ensighten_Example
//
//  Created by Bradley Slayter on 10/25/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import EnsightenSwift

class CustomPrivacyModal: UIView, EnsightenPrivacyModal {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var modalTitleLabel: UILabel!
    
    var cookies = [ESWPrivacyCookie]()
    
    /**
     Called when `Ensighten.privacyManager.openModal()` is called
     */
    func openPrivacyModal(translation: ESWPrivacyTranslation?, cookies: [ESWPrivacyCookie]) {
        let privacyCellNib = UINib(nibName: "CustomPrivacyCell",
                                   bundle: nil)
        tableView.register(privacyCellNib,
                           forCellReuseIdentifier: "PrivacyCellIdentifier")
        
        self.cookies = cookies
        
        let window = UIApplication.shared.keyWindow!
        let bounds = window.bounds
        let modalFrame = CGRect(x: 0, y: 0, width: bounds.width - 50, height: bounds.height - 200)
        self.frame = modalFrame
        self.center = window.center
        window.addSubview(self)
        
        if let consentTitle = translation?.consentTitle {
            modalTitleLabel.text = consentTitle
        }
        if let saveTitle = translation?.save {
            saveButton.setTitle(saveTitle.uppercased(), for: .normal)
        }
        if let cancelTitle = translation?.cancel {
            cancelButton.setTitle(cancelTitle.uppercased(), for: .normal)
        }
        
        tableView.reloadData()
    }
    
    /**
     Called when `Ensighten.privacyManager.closeModal()` is called
     */
    func closePrivacyModal() {
        self.removeFromSuperview()
    }

    /**
     On press of  "Cancel`
     */
    @IBAction func cancelAction(_ sender: Any) {
        Ensighten.privacyManager.closeModal()
    }
    
    /**
     On press of "Save"
     */
    @IBAction func saveAction(_ sender: Any) {
        Ensighten.privacyManager.setCookiesFor(cookies: self.cookies) { (_) in
            Ensighten.privacyManager.updatePreferences()
            Ensighten.privacyManager.closeModal()
        }
    }
}

extension CustomPrivacyModal: UITableViewDelegate {
    
}

extension CustomPrivacyModal: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cookies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrivacyCellIdentifier", for: indexPath) as! CustomPrivacyCell
        let cookie = cookies[indexPath.row]
        cell.delegate = self
        cell.populateWith(cookie: cookie)
        return cell
    }
    
}

extension CustomPrivacyModal: PrivacyCellDelegate {
    func privacyModalCell(cell: CustomPrivacyCell, didToggleCookie cookieSwitch: UISwitch) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        
        // Update the cookie in the array
        cookies[indexPath.row].value = cookieSwitch.isOn ? "1" : "0"
    }
}
