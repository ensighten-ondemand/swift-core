//
//  ProductDetailViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import EnsightenSwift

class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var addToCartButton: UIButton!
    
    var product: Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayProductInformation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Ensighten.trackPageView(page: "Product Detail Page View",
                                data: getProductInfo())
    }
    
    func displayProductInformation() {
        nameLabel.text = product?.name
        priceLabel.text = "$\(product?.price ?? 0)"
        descriptionTextView.text = product?.productDescription
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func addItemToCart(_ sender: UIButton) {
        guard let item = product else {
            return
        }
        Utility.shared.addItemToCart(item: item)
        showAddToCartSuccessAlert()
        Ensighten.trackEvent(event: "Add to Cart",
                             data: getProductInfo())
    }
    
    func showAddToCartSuccessAlert() {
        let alert = UIAlertController(title: "Success!", message: "\(product?.name ?? "This item") has been added to your cart", preferredStyle: .alert)
        let continueAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        let gotoCartAction = UIAlertAction(title: "Go to Cart", style: .default) { (action) in
            self.showCart()
        }
        alert.addAction(continueAction)
        alert.addAction(gotoCartAction)
        present(alert, animated: true, completion: nil)
    }
    
    func showCart() {
        if let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart") as? CartViewController {
            if let root = navigationController?.viewControllers.first {
                navigationController?.setViewControllers([root, cartVC], animated: true)
            }
        }
    }
    
    private func getProductInfo() -> [String : Any] {
        let productInfo: [String : Any] = [
            "productId": product?.id ?? "",
            "productName": product?.name ?? "",
            "productPrice": product?.price ?? 0
        ]
        return productInfo
    }

}

extension ProductDetailViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product?.images.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductDetailCell", for: indexPath) as! ProductDetailCell
        if let image = product?.images[indexPath.item] {
            cell.thumbnailView.image = image
        }
        return cell
    }
}


