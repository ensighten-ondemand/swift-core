//
//  CartViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import EnsightenSwift

class CartViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var emptyCartView: UIView!
    @IBOutlet weak var cartTotalView: UIView!
    @IBOutlet weak var totalPriceLabel: UILabel!


    var cartItems = [Item]()
    let checkoutTracker = CheckoutTracker()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableView.automaticDimension
        title = "Cart"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshCart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let items = cartItems.map { (item) -> [String : Any] in
            return [
                "id": item.id ?? "",
                "name": item.name ?? "",
                "price": item.price
            ] as [String : Any]
        }
        let cartDetails: [String : Any] = [
            // The list of items in the cart
            "items": items
        ]
        Ensighten.trackPageView(page: "Cart Page View",
                                data: cartDetails)
    }

    
    @IBAction func purchaseItems(_ sender: UIButton) {
        if cartItems.count <= 0 {
            navigationController?.popToRootViewController(animated: true)
            return
        }
        
        var promotions = [Promotion]()
        for item in cartItems {
            if let thePromotion = item as? Promotion {
                promotions.append(thePromotion)
            }
        }
        if promotions.count > 0 {
            let items = promotions.map { (item) -> [String : Any] in
                return [
                    "id": item.id ?? "",
                    "name": item.name ?? "",
                    "price": item.price
                    ] as [String : Any]
            }
            Ensighten.trackConversion(conversion: "Promotion Checked Out",
                                      data: items)
        }
        checkoutTracker.trackCheckoutWith(items: cartItems)
        
        if let confirmationVC = storyboard?.instantiateViewController(withIdentifier: "Confirmation") as? ConfirmationViewController {
            navigationController?.pushViewController(confirmationVC, animated: true)
        }
    }
    
    func refreshCart() {
        cartItems = Utility.shared.fetchCartItems()
        var totalPrice: Double = 0
        for item in cartItems {
            let itemCount = Utility.shared.getCartItemCount(item: item)
            totalPrice += Double(itemCount) * item.price
        }
        totalPriceLabel.text = "$\(totalPrice.roundToDecimal(2))"
        tableView.reloadData()
    }
    
}

extension CartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        emptyCartView.isHidden = cartItems.count > 0
        cartTotalView.isHidden = cartItems.count <= 0
        purchaseButton.setTitle(cartItems.count > 0 ? "Checkout" : "Go to Store", for: .normal)
        return cartItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
        let item = cartItems[indexPath.row]
        let itemCount = Utility.shared.getCartItemCount(item: item)
        cell.countLabel.text = "\(itemCount)"
        cell.priceLabel.text = "$\((item.price * Double(itemCount)).roundToDecimal(2))"
        cell.nameLabel.text = item.name
        cell.thumbnailView.image = item.thumbnail
        cell.delegate = self
        cell.item = item
        return cell
    }
}

extension CartViewController: CartCellDelegate {
    
    func cartCellDidUpdateCountFor(item: Item) {
        refreshCart()
        let count = Utility.shared.getCartItemCount(item: item)
        var itemInfo: [String : Any] = [
            "itemId": item.id ?? "",
            "itemName": item.name ?? "",
            "itemPrice": item.price
        ]
        if count > 0 {
            itemInfo["itemQuantity"] = count
            Ensighten.trackEvent(event: "Item Quantity Changed",
                                 data: itemInfo)
        } else {
            Ensighten.trackEvent(event: "Item Removed",
                                 data: itemInfo)
        }
    }
}

