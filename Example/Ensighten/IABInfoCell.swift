//
//  IABInfoCell.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 5/6/19.
//  Copyright © 2019 Ensighten. All rights reserved.
//

import UIKit

class IABInfoCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
}
