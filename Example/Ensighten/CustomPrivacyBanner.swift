//
//  CustomPrivacyBanner.swift
//  Ensighten_Example
//
//  Created by Bradley Slayter on 10/24/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import EnsightenSwift

class CustomPrivacyBanner: UIView, EnsightenPrivacyBanner {

    @IBOutlet weak var consentLabel: UILabel!
    @IBOutlet weak var consentButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    /**
     Called when `Ensighten.privacyManager.openBanner()` is called
     */
    func openPrivacyBanner(translation: ESWPrivacyTranslation?) {
        var message = "Placeholder"
        var settingsTitle = "Cookie Settings"
        var cancelTitle = "Cancel"
        
        if let theMessage = translation?.notificationBannerContent {
            message = theMessage
                .replacingOccurrences(of: "{{", with: "")
                .replacingOccurrences(of: "}}", with: "")
            if let theSettingsTitle = theMessage.slice(from: "{{", to: "}}") {
                settingsTitle = theSettingsTitle
            }
        }
        if let theCancelTitle = translation?.cancel {
            cancelTitle = theCancelTitle
        }
        
        consentLabel.text = message
        consentButton.setTitle(settingsTitle, for: .normal)
        closeButton.setTitle(cancelTitle, for: .normal)
        
        let window = UIApplication.shared.keyWindow!
        let bounds = window.bounds
        let bannerFrame = CGRect(x: 0, y: bounds.height - 225, width: bounds.width, height: 225)
        self.frame = bannerFrame
        window.addSubview(self)
    }
    
    /**
     Called when `Ensighten.privacyManager.closeBanner()` is called
     */
    func closePrivacyBanner() {
        self.removeFromSuperview()
    }
    
    /**
     On press of "Consent Settings"
     */
    @IBAction func consentAction(_ sender: Any) {
        Ensighten.privacyManager.closeBanner()
        Ensighten.privacyManager.openModal()
    }
    
    /**
     On press of "Close"
     */
    @IBAction func closeButtonAction(_ sender: Any) {
        Ensighten.privacyManager.closeBanner()
    }
}
