//
//  Item.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class Item: NSObject {
    var id: String?
    var name: String?
    var thumbnail: UIImage?
    var price: Double = 0

    init(info: [String : Any]) {
        name = info["name"] as? String
        id = info["id"] as? String
        price = info["price"] as? Double ?? 0
        if let primaryImageName = info["thumbnail"] as? String {
            thumbnail = UIImage(named: primaryImageName)
        }
    }
}
