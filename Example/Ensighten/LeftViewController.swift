//
//  LeftViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 8/13/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import SafariServices

class LeftViewController: UITableViewController {

    @IBOutlet weak var toolsIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearsSelectionOnViewWillAppear = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        toolsIcon.tintColor = UIColor(red: 96/255, green: 96/255, blue: 96/255, alpha: 1)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            if let cartVC = storyboard?.instantiateViewController(withIdentifier: "Cart") as? CartViewController {
                displayViewControllerUsingNavigation(viewController: cartVC)
            }
        case 2:
            displayUrl(url: "https://www.ensighten.com/products/enterprise-tag-management/mobile/")
        case 3:
            if let privacyVC = storyboard?.instantiateViewController(withIdentifier: "Privacy") as? PrivacyViewController {
                displayViewControllerUsingNavigation(viewController: privacyVC)
            }
        case 4:
            if let toolsVC = storyboard?.instantiateViewController(withIdentifier: "Tools") as? ToolsViewController {
                displayViewControllerUsingNavigation(viewController: toolsVC)
            }
        case 5:
            displayUrl(url: "https://www.ensighten.com/company/contact-us/")
        case 6:
            if let pushTokenVC = storyboard?.instantiateViewController(withIdentifier: "PushToken") as? PushTokenViewController {
                displayViewControllerUsingNavigation(viewController: pushTokenVC)
            }
        default: break
            
        }
        drawerController?.hide(animated: true)

    }
    
    func displayUrl(url: String) {
        if let contactUsURL = URL(string: url) {
            let safariVC = SFSafariViewController(url: contactUsURL)
            present(safariVC, animated: true, completion: nil)
        }
    }
    
    func displayViewControllerUsingNavigation(viewController: UIViewController) {
        (drawerController?.centerViewController as? UINavigationController)?.pushViewController(viewController, animated: true)
    }

}
