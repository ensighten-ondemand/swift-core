//
//  PrivacyViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 4/26/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import EnsightenSwift

class PrivacyViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
        
        navigationItem.title = "Privacy Settings"
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            displayCookiePolicy()
        case 1:
            displayPrivacySettings()
        case 2:
            displayIABPreferences()
        case 3:
            displayThemeSelection()
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func displayCookiePolicy() {
        Ensighten.privacyManager.openBanner()
    }
    
    func displayPrivacySettings() {
        Ensighten.privacyManager.openModal()
    }
    
    func displayIABPreferences() {
        guard let iabVC = storyboard?.instantiateViewController(withIdentifier: "IAB") else {
            return
        }
        navigationController?.pushViewController(iabVC, animated: true)
    }
    
    func displayThemeSelection() {
        guard let themeSelectionVC = storyboard?.instantiateViewController(withIdentifier: "ThemeSelection") else {
            return
        }
        navigationController?.pushViewController(themeSelectionVC, animated: true)
    }

    
}
