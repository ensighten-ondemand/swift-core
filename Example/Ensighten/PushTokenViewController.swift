//
//  PushTokenViewController.swift
//  Ensighten_Example
//
//  Created by Ron Park on 9/4/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import UIKit

class PushTokenViewController: UIViewController {
    
    @IBOutlet private weak var tokenLabel: UILabel?
    
    private var pushToken: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if
            let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let pushToken = appDelegate.getPushToken()
        {
            self.pushToken = pushToken
            tokenLabel?.text = pushToken
        } else {
            tokenLabel?.text = "UNAVAILABLE"
        }
    }
    
    @IBAction private func copyPushToken() {
        guard let pushToken = pushToken else { return }
        UIPasteboard.general.string = pushToken
        
        let alert = UIAlertController(title: "Push Token Copied To Clipboard", message: "", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default)
        alert.addAction(ok)
        present(alert, animated: true)
    }
    
}
