//
//  IABViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 4/26/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class IABViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "IAB Preferences"
    }

}

extension IABViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CMPPreferences.preferences.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IABCell", for: indexPath) as! IABInfoCell
        let preference = CMPPreferences.preferences[indexPath.row]
        if let key = preference["key"] {
            if key == "IABConsent_CMPPresent" {
                cell.valueLabel.text = UserDefaults.standard.bool(forKey: key).description
            } else {
                cell.valueLabel.text = UserDefaults.standard.string(forKey: key) ?? "Not Available"
            }
        }
        cell.nameLabel.text = preference["name"]
        return cell
    }

}
