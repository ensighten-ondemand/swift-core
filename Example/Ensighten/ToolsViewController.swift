//
//  ToolsViewController.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 10/3/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit

class ToolsViewController: UITableViewController {

    @IBOutlet weak var crashIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        clearsSelectionOnViewWillAppear = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        crashIcon.tintColor = UIColor(red: 96/255, green: 96/255, blue: 96/255, alpha: 1)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            displayDeepLinkAlert()
        case 1:
            displayCrashAlert()
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func displayCrashAlert() {
        let crashTitle = "Crash App"
        let crashMessage = "This will crash the app. The crash report will be tracked on the next launch of the app after the crash. "
        let alert = UIAlertController(title: crashTitle, message: crashMessage, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let crashAction = UIAlertAction(title: "Crash", style: .destructive) {[weak self] (action) in
            self?.crashApp()
        }
        alert.addAction(cancelAction)
        alert.addAction(crashAction)
        present(alert, animated: true, completion: nil)
    }
    
    func displayDeepLinkAlert() {
        let deepLinkTitle = "Open App via Deep Link"
        let deepLinkMessage = """
                            In order to open the app via deep link, please go to safari and enter:
                            "ensightendemo://store"

                            Click "Open Safari"  to automatically copy the deep link url to clipboard and open Safari
                            """
        let alert = UIAlertController(title: deepLinkTitle, message: deepLinkMessage, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let openSafariAction = UIAlertAction(title: "Open Safari", style: .default) {[weak self] (action) in
            self?.openSafari()
        }
        alert.addAction(cancelAction)
        alert.addAction(openSafariAction)
        present(alert, animated: true, completion: nil)
    }

    func openSafari() {
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = "ensightendemo://store"
        if let safariUrl = URL(string: "https://ensighten.com") {
            UIApplication.shared.open(safariUrl, options: [:], completionHandler: nil)
            navigationController?.popViewController(animated: true)
        }
    }
    
    func crashApp() {
        let array = NSArray()
        let element = array.object(at: 4)
        print("\(element as! String)")
    }
}
