//
//  UIVIewAdditions.swift
//  Ensighten_Example
//
//  Created by Bradley Slayter on 10/24/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
