//
//  PromotionView.swift
//  Ensighten_Example
//
//  Created by Subash Luitel on 2/28/18.
//  Copyright © 2018 Ensighten. All rights reserved.
//

import UIKit
import ScalingCarousel

class PromotionView: UICollectionReusableView {
    
    @IBOutlet weak var collectionView: ScalingCarouselView!
    fileprivate var promotions = [Promotion]()
    fileprivate var promotionCellIdentifier = "PromotionCell"
    var delegate: PromotionViewDelegate?
    
    func updatePromotions() {
        promotions = Utility.shared.fetchAllPromotions()
        collectionView.reloadData()
        collectionView.layoutIfNeeded()
    }
}

extension PromotionView: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promotions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: promotionCellIdentifier, for: indexPath) as! PromotionCell
        let promotion = promotions[indexPath.item]
        cell.thumbnailView.image = promotion.thumbnail
        cell.priceLabel.text = "$\(promotion.price)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let promotion = promotions[indexPath.item]
        delegate?.promotionViewDidSelect(promotion: promotion)
    }
}

extension PromotionView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        collectionView.didScroll()
    }
}

protocol PromotionViewDelegate {
    func promotionViewDidSelect(promotion: Promotion)
}

